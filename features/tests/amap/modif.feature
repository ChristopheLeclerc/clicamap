# language: fr
Fonctionnalité: Vérifier la mise à jour d'une AMAP

  @database
  Plan du Scénario: Tester tous les messages, profils admin et super-admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/amap"
    Et que je remplis "search_amap[region]" avec "84"
    Et que je suis "Modifier l'AMAP"
    Et que je suis "Les informations générales"
    Alors je devrais être sur "/amap/informations_generales_edition/1"
    Et que je remplis "amap_edition[nom]" avec "<amap_nom>"
    Et que je remplis "amap_edition[adresseAdminVille]" avec "<cp>"
    Et que je remplis "amap_edition[email]" avec "<email_public>"
    Et que je remplis "amap_edition[amapEtudiante]" avec "<etudiante>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | mail                          | amap_nom      | cp                            | email_public             | message                                                           | etudiante |
      | admin.aura@test.amap-aura.org |               | 69007, LYON 7e Arrondissement | amaptest@tutanota.com    | Cette valeur ne doit pas être vide.                               | 0         |
      | admin.aura@test.amap-aura.org | Nom de l'AMAP | 69007                         | amaptest@tutanota.com    | Le champ \"Code postal, Ville\" est mal formaté.                  | 0         |
      | admin.aura@test.amap-aura.org | Nom de l'AMAP | Lyon                          | amaptest@tutanota.com    | Le champ \"Code postal, Ville\" est mal formaté.                  | 0         |
      | admin.aura@test.amap-aura.org | Nom de l'AMAP |                               | amaptest@tutanota.com    | Le champ \"Code postal, Ville administrative\" est requis.        | 0         |
      | admin.aura@test.amap-aura.org | Nom de l'AMAP | 69007, LYON 7e Arrondissement | amaptest                 | Cette valeur n'est pas une adresse email valide.                  | 0         |
      | admin.aura@test.amap-aura.org |               |                               | amaptest@tutanota.com    | Le champ \"Code postal, Ville administrative\" est requis.        | 0         |
      | admin.aura@test.amap-aura.org | Nom de l'AMAP | 69007, LYON 7e Arrondissement | amaptest@tutanota.com    | L'AMAP a été modifiée avec succès !                               | 0         |
      | admin@test.amap-aura.org      |               | 69007, LYON 7e Arrondissement | amaptest@tutanota.com    | Cette valeur ne doit pas être vide.                               | 0         |
      | admin@test.amap-aura.org      | Nom de l'AMAP | 69007                         | amaptest@tutanota.com    | Le champ \"Code postal, Ville\" est mal formaté.                  | 0         |
      | admin@test.amap-aura.org      | Nom de l'AMAP | Lyon                          | amaptest@tutanota.com    | Le champ \"Code postal, Ville\" est mal formaté.                  | 0         |
      | admin@test.amap-aura.org      | Nom de l'AMAP |                               | amaptest@tutanota.com    | Le champ \"Code postal, Ville administrative\" est requis.        | 0         |
      | admin@test.amap-aura.org      | Nom de l'AMAP | 69007, LYON 7e Arrondissement | amaptest                 | Cette valeur n'est pas une adresse email valide.                   | 0         |
      | admin@test.amap-aura.org      |               |                               | amaptest@tutanota.com    | Le champ \"Code postal, Ville administrative\" est requis.        | 0         |
      | admin@test.amap-aura.org      | Nom de l'AMAP | 69007, LYON 7e Arrondissement | amaptest@tutanota.com    | L'AMAP a été modifiée avec succès !                               | 0         |

  @database
  Plan du Scénario: Tester les messages d'erreur profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/amap"
    Et que je suis "Modifier l'AMAP"
    Et que je suis "Les informations générales"
    Alors je devrais être sur "/amap/informations_generales_edition/1"
    Et que je remplis "amap_edition[nom]" avec "<amap_nom>"
    Et que je remplis "amap_edition[adresseAdminVille]" avec "<cp>"
    Et que je remplis "amap_edition[email]" avec "<email_public>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | amap_nom      | cp                            | email_public          | message                                                           |
      |               | 69007, LYON 7e Arrondissement | amaptest@tutanota.com | Cette valeur ne doit pas être vide.                               |
      | Nom de l'AMAP | 69007                         | amaptest@tutanota.com | Le champ \"Code postal, Ville\" est mal formaté.                  |
      | Nom de l'AMAP | Lyon                          | amaptest@tutanota.com | Le champ \"Code postal, Ville\" est mal formaté.                  |
      | Nom de l'AMAP |                               | amaptest@tutanota.com | Le champ \"Code postal, Ville administrative\" est requis.        |
      | Nom de l'AMAP | 69007, LYON 7e Arrondissement | amaptest              | Cette valeur n'est pas une adresse email valide.                  |
      |               |                               | amaptest@tutanota.com | Le champ \"Code postal, Ville administrative\" est requis.        |
      | Nom de l'AMAP | 69007, LYON 7e Arrondissement | amaptest@tutanota.com | L'AMAP a été modifiée avec succès !                               |
