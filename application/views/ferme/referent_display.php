<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\Amapien[] $referents */
/** @var \PsrLib\ORM\Entity\Amap[] $amapAccessibles */
?>

<script>
  $(document).ready(function () {
    $('select[name="ferme_ref_produit[amap]"]').on('change', function () {
      this.form.submit();
    })
  })
</script>

<?php
echo '<h3>Référents produits de la ferme</h3>';

echo '<h5><a href="'.site_url('ferme').'">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.ucfirst($ferme->getNom()).'</a> <i class="glyphicon glyphicon-menu-right"></i> Référents produits</h5>';

echo '<div class="alert alert-info">Si vous voulez ajouter un référent produit à la ferme, utilisez les menus déroulant ci-dessous. Il convient de choisir l\'AMAP puis l\'amapien à qui vous donnez ce rôle.<br/>Pour créer un amapien au sein d\'une AMAP : <a href="'.site_url('amapien').'">gestion des amapiens</a></div>';

?>

<form method="POST" action="">

    <div class="row" style="margin-bottom:5px;">
        <div class="col-md-12">
            <div class="input-group">

<span class="input-group-btn">
<button class="btn btn-primary" type="button">Amap</button>
</span>

                <?= render_form_select_widget($form['amap']); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="input-group">

<span class="input-group-btn">
<button class="btn btn-primary" type="button">Amapien</button>
</span>

                <?= render_form_select_widget($form['amapien']); ?>

            </div>
        </div>
            <div class="form-group col-sm-12">
                <input type="submit" value="Ajouter" class="btn btn-success pull-right"/>
                <?php if (count($amapAccessibles) > 1): ?>
                    <a href="<?= site_url('ferme/referent_display/'.$ferme->getId()); ?>"
                       class="btn btn-primary pull-left">Changer l'AMAP</a>
                <?php endif; ?>
            </div>
    </div>

</form>


<?php if (count($referents) > 0): ?>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-inverse">
            <tr>
                <th>AMAP</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Email</th>
                <th class="text-right">Outils</th>
            </tr>
            </thead>


            <tbody>
            <?php
            /** @var \PsrLib\ORM\Entity\Amapien $referent */
            foreach ($referents as $referent) {
                echo '<tr>';
                echo '<th scope="row">';
                echo strtoupper($referent->getAmap());
                echo '</th>';

                echo '<td>';
                echo strtoupper($referent->getNom());
                echo '</td>';
                echo '<td>';
                echo ucfirst($referent->getPrenom());
                echo '</td>';

                echo '<td>';
                echo '<a href="mailto:'.$referent->getEmail().'">'.$referent->getEmail().'</a>';
                echo '</td>';

                echo '<td class="text-right">';

                echo '<a href="#" data-toggle="modal" data-target="#modal_remove_ref_'.$referent->getId().'" ><i class="glyphicon glyphicon-remove"></i></a> '; ?>

                <!-- Modal remove -->
                <div class="modal fade text-left"
                     id="modal_remove_ref_<?= $referent->getId(); ?>" tabindex="-1"
                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;
                                </button>
                                <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer
                                    le droit référent produit
                                    de <?= strtoupper($referent->getNom()).' '.ucfirst($referent->getPrenom()); ?>
                                    ?</h5>
                            </div>
                            <div class="modal-body">
                                <!-- <div class="alert alert-danger">...</div> -->
                                <p class="text-right">
                                    <a href="<?= site_url('ferme/remove_referent_produit/'.$ferme->getId().'/'.$referent->getId()); ?>"
                                       class="btn btn-danger btn-sm">OUI</a>
                                    <a href="#" class="btn btn-success btn-sm"
                                       data-dismiss="modal">NON</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                </td>
                </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>

<?php endif; ?>
