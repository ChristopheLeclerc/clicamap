<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;

/**
 * @extends EntityRepository<Contrat>
 */
class ContratRepository extends EntityRepository
{
    /**
     * @return Contrat[]
     */
    public function search(SearchContratSigneState $state): array
    {
        $mc = $state->getMc();
        if (null === $mc) {
            return [];
        }

        return $this
            ->createQueryBuilder('c')
            ->leftJoin('c.amapien', 'a')
            ->addSelect('a')
            ->where('c.modeleContrat = :mc')
            ->setParameter('mc', $mc)
            ->orderBy('a.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Contrat[]
     */
    public function getWithDateMc(ModeleContrat $modeleContrat, ModeleContratDate $date): array
    {
        return $this
            ->createQueryBuilder('c')
            ->addSelect('a')
            ->leftJoin('c.amapien', 'a')
            ->leftJoin('c.cellules', 'cc')
            ->where('c.modeleContrat = :mc')
            ->setParameter('mc', $modeleContrat)
            ->andWhere('cc.modeleContratDate = :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult()
        ;
    }
}
