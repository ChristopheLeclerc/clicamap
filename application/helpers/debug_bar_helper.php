<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!function_exists('render_debug_bar_header')) {
    function render_debug_bar_header()
    {
        if ('production' === getenv('CI_ENV')) {
            return '';
        }

        $renderer = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\DebugBar\JavascriptRenderer::class)
        ;
        $renderer->setIncludeVendors(false);

        $cssFile = \PsrLib\ProjectLocation::PROJECT_ROOT.'public/debugbar.css';
        if (!file_exists($cssFile)) {
            $renderer->dumpCssAssets($cssFile);
        }

        $jsFile = \PsrLib\ProjectLocation::PROJECT_ROOT.'public/debugbar.js';
        if (!file_exists($jsFile)) {
            $renderer->dumpJsAssets($jsFile);
        }

        $out = '<link rel="stylesheet" href="/debugbar.css" />';
        $out .= '<script type="text/javascript" src="/debugbar.js"></script>';

        return $out;
    }
}

if (!function_exists('render_debug_bar')) {
    function render_debug_bar()
    {
        if ('production' === getenv('CI_ENV')) {
            return '';
        }

        $renderer = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\DebugBar\JavascriptRenderer::class)
        ;

        return $renderer->render();
    }
}
