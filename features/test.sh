#!/bin/bash

# wait for composer end installing
if [ ! -f ../vendor/autoload.php ];
then
  echo -n "Initialisation en cours";
  while [ ! -f ../vendor/autoload.php ];
  do
    echo -n '.'
    sleep 5;
  done
  echo ''
fi;

docker-compose -f ../docker/docker-compose.yml exec server php vendor/behat/behat/bin/behat --lang=fr --strict --no-snippets  "$*"
