<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * AkContratCellule.
 *
 * @ORM\Table(name="ak_contrat_cellule", indexes={@ORM\Index(name="IND_CONTRAT_CELLULE_ID", columns={"c_c_id"}), @ORM\Index(name="IND_CONTRAT_CELLULE_FK_CONTRAT_DATE", columns={"c_c_fk_modele_contrat_date_id"})})
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\ContratCelluleRepository")
 */
class ContratCellule
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="c_c_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizardContract"})
     */
    private $id;

    /**
     * @var null|float|int
     *
     * @ORM\Column(name="c_c_quantite", type="float", precision=10, scale=0, nullable=true)
     * @Groups({"wizardContract"})
     */
    private $quantite;

    /**
     * @var ?Contrat
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Contrat", inversedBy="cellules")
     * @ORM\JoinColumn(name="c_c_fk_contrat_id", referencedColumnName="c_id")
     */
    private $contrat;

    /**
     * @var ?ModeleContratProduit
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\ModeleContratProduit")
     * @ORM\JoinColumn(name="c_c_fk_modele_contrat_produit_id", referencedColumnName="mc_pro_id")
     * @Groups({"wizardContract"})
     */
    private $modeleContratProduit;

    /**
     * @var ?ModeleContratDate
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\ModeleContratDate")
     * @ORM\JoinColumn(name="c_c_fk_modele_contrat_date_id", referencedColumnName="mc_d_id")
     * @Groups({"wizardContract"})
     */
    private $modeleContratDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?float
    {
        return $this->quantite;
    }

    public function setQuantite(?float $quantite): ContratCellule
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getContrat(): ?Contrat
    {
        return $this->contrat;
    }

    public function setContrat(?Contrat $contrat): ContratCellule
    {
        $this->contrat = $contrat;

        return $this;
    }

    public function getModeleContratProduit(): ?ModeleContratProduit
    {
        return $this->modeleContratProduit;
    }

    public function setModeleContratProduit(?ModeleContratProduit $modeleContratProduit): ContratCellule
    {
        $this->modeleContratProduit = $modeleContratProduit;

        return $this;
    }

    public function getModeleContratDate(): ?ModeleContratDate
    {
        return $this->modeleContratDate;
    }

    public function setModeleContratDate(?ModeleContratDate $modeleContratDate): ContratCellule
    {
        $this->modeleContratDate = $modeleContratDate;

        return $this;
    }
}
