<?php

namespace PsrLib\Services\Security\Checkers;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\UserWithFermes;
use PsrLib\Services\Security\SecurityChecker;

class ContractSignedCheck extends AbstractCheck
{
    public function isGrantedContractSignedList(): bool
    {
        // Les paysans et les AMAPS peuvent lister les contrats signés
        if ($this->getCurrentUser() instanceof Amap
            || $this->getCurrentUser() instanceof UserWithFermes) {
            return true;
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            if ($this->getCurrentUser()->isAdmin() || $this->getCurrentUser()->isRefProduit()) {
                return true;
            }
        }

        return false;
    }

    public function isGrantedContractSignedDisplay(Contrat $contrat): bool
    {
        $currentUser = $this->getCurrentUser();

        if ($currentUser instanceof Amap) {
            // Les amaps ont accès au contrat signé pour lesquels ils ont accès au contrat vierge
            return $contrat->getModeleContrat()->getLivraisonLieu()->getAmap() === $currentUser;
        }

        if ($currentUser instanceof UserWithFermes) {
            return $currentUser->getFermes()->contains($contrat->getModeleContrat()->getFerme());
        }

        if ($currentUser instanceof Amapien) {
            // Les amapiens peuvent visualiser leurs contrats
            if ($contrat->getAmapien()->getId() === $currentUser->getId()) {
                return true;
            }

            // Les super admin peuvent visulaliser tous les contrats signés
            if ($currentUser->isSuperAdmin()) {
                return true;
            }

            // Les autres admin aux accès aux contrats pour lesquels ils ont accès au contrat vierge correspondant
            if ($currentUser->isAdmin() || $currentUser->isRefProduit()) { // Référent produit
                return $this
                    ->securitychecker
                    ->isGranted(
                        SecurityChecker::ACTION_CONTRACT_MODEL_DISPLAY,
                        $contrat->getModeleContrat()
                    )
                ;
            }
        }

        return false;
    }

    public function isGrantedContractSignedExportPdf(Contrat $contrat): bool
    {
        if (2 === $contrat->getModeleContrat()->getVersion() && null !== $contrat->getPdf()) {
            return $this
                ->securitychecker
                ->isGranted(
                    SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY,
                    $contrat
                )
            ;
        }

        return false;
    }

    public function isGrantedContractSignedEdit(Contrat $contrat): bool
    {
        return $this->securitychecker->isGranted(SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE, $contrat->getModeleContrat());
    }

    public function isGrantedContractSignedEditForce(Contrat $contrat): bool
    {
        return $this->securitychecker->isGranted(SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE, $contrat->getModeleContrat());
    }

    public function isGrantedContractSignedDisplayOwn(): bool
    {
        if ($this->getCurrentUser() instanceof Amapien) {
            // Seulement pour les non admins
            if ($this->getCurrentUser()->isAdmin()) {
                return false;
            }

            return true;
        }

        return false;
    }

    public function isGrantedContractSignedMoveDate(Contrat $contrat): bool
    {
        // Seuls les contats en v2 peuvent etre déplacés
        $mc = $contrat->getModeleContrat();
        if (2 === $mc->getVersion()) {
            // La date de modification doit etre dépassée
            $now = new Carbon();
            if ($now->greaterThan($mc->getForclusion())) {
                // Les amapiens peuvent déplacer les contrats dates dans certains cas
                if ($this->getCurrentUser() instanceof Amapien
                    && $contrat->getAmapien()->getId() === $this->getCurrentUser()->getId()) {
                    return ($mc->getProduitsIdentiqueAmapien() && $mc->getProduitsIdentiquePaysan())
                        && ($mc->getAmapienPermissionDeplacementLivraison() || $mc->getAmapienPermissionReportLivraison());
                }

                // Seuls les référents produits & les amaps peuvent déplacer les livraisons
                if ($this->getCurrentUser() instanceof Amapien
                    || $this->getCurrentUser() instanceof Amap) {
                    return $this->securitychecker->isGranted(SecurityChecker::ACTION_CONTRACT_MODEL_DISPLAY, $mc);
                }
            }
        }

        return false;
    }

    public function isGrantedContractSignedListArchived(): bool
    {
        return $this->getCurrentUser() instanceof UserWithFermes;
    }
}
