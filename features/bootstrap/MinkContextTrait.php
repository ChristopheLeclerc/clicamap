<?php

use Behat\Behat\Hook\Scope\BeforeScenarioScope;

trait MinkContextTrait
{
    /** @var \Behat\MinkExtension\Context\MinkContext */
    private $minkContext;

    /** @BeforeScenario */
    public function gatherContexts(BeforeScenarioScope $scope)
    {
        $environment = $scope->getEnvironment();

        $this->minkContext = $environment->getContext('MinkOverrideContext');
    }
}
