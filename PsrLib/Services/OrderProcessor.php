<?php

namespace PsrLib\Services;

use PsrLib\ORM\Entity\ContratCellule;

class OrderProcessor
{
    /**
     * Compte le nombre de livraisons au contrat.
     *
     * @param ContratCellule[] $commandes
     */
    public function countNbLivraisons(array $commandes): int
    {
        // Compte le nombre de livraisons par date
        $livCount = [];
        foreach ($commandes as $commande) {
            $dateId = $commande->getModeleContratDate()->getId();
            if (!array_key_exists($dateId, $livCount)) {
                $livCount[$dateId] = 0;
            }

            $livCount[$dateId] += (float) $commande->getQuantite();
        }

        // Compte le nombre de dates avec au moins une livraisons
        $datesWithLiv = 0;
        foreach ($livCount as $count) {
            if ($count > 0) {
                ++$datesWithLiv;
            }
        }

        return $datesWithLiv;
    }

    /**
     * Test si toutes les dates qui ont des livraisons sont identiques.
     *
     * @param ContratCellule[] $inputCommandes
     */
    public function areLivSame(array $inputCommandes): bool
    {
        // Groupe les commandes par dates
        $commandDates = [];
        foreach ($inputCommandes as $commande) {
            $dateId = $commande->getModeleContratDate()->getId();
            if (!(array_key_exists($dateId, $commandDates))) {
                $commandDates[$dateId] = [];
            }
            $commandDates[$dateId][] = $commande;
        }

        // Référence de la première ligne contenant des commandes
        $commandRefDateId = null;
        foreach ($commandDates as $dateId => $commandes) {
            $nbLiv = array_reduce($commandes, function ($count, ContratCellule $c) {
                return $count + (float) $c->getQuantite();
            }, 0);

            // La ligne ne contiens pas de livraisons. Elle est ignorée
            if (0.0 === $nbLiv) {
                continue;
            }

            // La ligne est la première qui contiens des livraison. Elle est sauvegardée comme référence
            if (null === $commandRefDateId) {
                $commandRefDateId = $dateId;

                continue;
            }

            // Compare la ligne courante avec la ligne de référence
            foreach ($commandes as $i => $commande) {
                if (!isset($commandDates[$commandRefDateId][$i])) { // Pour les contrats avec des nombres de produits différents
                    if ('0' !== $commande->getQuantite()) {
                        return false;
                    }
                } elseif ($commandDates[$commandRefDateId][$i]->getQuantite() !== $commande->getQuantite()) {
                    return false;
                }
            }
        }

        return true;
    }
}
