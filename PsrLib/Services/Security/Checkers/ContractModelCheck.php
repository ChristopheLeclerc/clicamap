<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Security\Checkers;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\UserWithFermes;
use PsrLib\ORM\Repository\FermeRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\Security\SecurityChecker;

class ContractModelCheck extends AbstractCheck
{
    /**
     * @var ModeleContratRepository
     */
    private $modelContratRepository;

    /**
     * @var FermeRepository
     */
    private $fermeRepository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->modelContratRepository = $em->getRepository(ModeleContrat::class);
        $this->fermeRepository = $em->getRepository(Ferme::class);
    }

    public function isGrantedContractModelList(): bool
    {
        // Les AMAPS peuvent lister les modèles de contrat
        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        // Les roles suivants peuvent lister les modèles de contrats
        if ($this->getCurrentUser() instanceof Amapien) {
            if ($this->getCurrentUser()->isAdmin() || $this->getCurrentUser()->isRefProduit()) {
                return true;
            }
        }

        return false;
    }

    public function isGrantedContractModelListValidate(): bool
    {
        return $this->getCurrentUser() instanceof UserWithFermes;
    }

    public function isGrantedContractModelDisplay(ModeleContrat $mc): bool
    {
        $amap = $mc->getLivraisonLieu()->getAmap();
        // Une AMAP peut afficher les contrats qui lui sont liée
        if ($this->getCurrentUser() instanceof Amap) {
            return $amap->getId() === $this->getCurrentUser()->getId();
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            // Les super admin peuvent modifier tous les contrats vierge
            if ($this->getCurrentUser()->isAdmin()) {
                return $this->securitychecker->isGranted(SecurityChecker::ACTION_AMAP_DISPLAY, $amap);
            }

            // Les référents produits ont accès aux contrats vierges de leurs fermes
            if ($this->getCurrentUser()->isRefProduit()) {
                return $this->getCurrentUser()->getRefProdFermes()->contains($mc->getFerme());
            }
        }

        // Les paysans peuvent visualiser les contrats auxquels ils sont attachés
        if ($this->getCurrentUser() instanceof Paysan) {
            return $mc->getFerme()->getId() === $this->getCurrentUser()->getFerme()->getId();
        }

        return false;
    }

    public function isGrantedContractModelSubscribeForce(ModeleContrat $mc): bool
    {
        if ($this->getCurrentUser() instanceof Amap) {
            return $this->isGrantedContractModelSubscribe($mc);
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            // Les référents ne peuvent forcer les contrats que pour les fermes dont ils sont référents
            $forceRef = false;
            if ($this->getCurrentUser()->isRefProduit()) {
                foreach ($this->getCurrentUser()->getRefProdFermes() as $ferme) {
                    if ($ferme->getId() === $mc->getFerme()->getId()) {
                        $forceRef = true;
                    }
                }
            }

            // Les rôles suivant peuvent s'inscrire aux contrats vierges si ils ont accès à l'amap correspondante
            if ($this->getCurrentUser()->isAdminRegion()
                || $this->getCurrentUser()->isAdminDepartment()
                || $this->getCurrentUser()->isAdminReseaux()
                || $forceRef) {
                if (!$this->securitychecker->isGranted(SecurityChecker::ACTION_AMAP_DISPLAY, $mc->getLivraisonLieu()->getAmap())) {
                    return false;
                }

                // La date de dernière livraison ne doit pas etre dépassée
                return $this->isLastDateExcedeed($mc);
            }
        }

        return false;
    }

    public function isGrantedContractModelSubscribe(ModeleContrat $mc): bool
    {
        // On ne peut pas s'inscrire à un contrat qui n'est pas en v2
        if (2 !== (int) $mc->getVersion()) {
            return false;
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            // L'amapien ne doit pas etre en liste d'attente
            if ($this->getCurrentUser()->getFermeAttentes()->contains($mc->getFerme())) {
                return false;
            }

            // Les amapiens peuvent s'inscrire si la date n'est pas dépassée
            $now = Carbon::now()->subDay();
            if ($now->greaterThan($mc->getForclusion())) {
                return false;
            }

            // Verifie que le contrat est dans la liste des contrats que l'on peut souscrire
            $contractsSubscribable = $this
                ->modelContratRepository
                ->getSubscribableContractForAmapien($this->getCurrentUser(), false)
            ;
            foreach ($contractsSubscribable as $contractSubscribable) {
                if ($contractSubscribable->getId() === $mc->getId()) {
                    return true;
                }
            }

            return false;
        }

        if ($this->getCurrentUser() instanceof Amap) {
            return $this->isLastDateExcedeed($mc);
        }

        return false;
    }

    public function isGrantedContractModelEdit(ModeleContrat $mc): bool
    {
        // Vérifie que le l'amap & la ferme sont cohérents
        $mcAmap = $mc->getLivraisonLieu()->getAmap();

        $fermesAmap = $this
            ->fermeRepository
            ->getFromAmap($mcAmap)
        ;
        if (!in_array($mc->getFerme(), $fermesAmap, true)) {
            return false;
        }

        // On peut uniquement supprimer les contrats en création
        if ((ModeleContrat::ETAT_CREATION === $mc->getEtat() || ModeleContrat::ETAT_REFUS === $mc->getEtat())
            && $this->isGrantedContractModelDisplay($mc)) {
            return true;
        }

        return false;
    }

    public function isGrantedContractModelValidate(ModeleContrat $mc): bool
    {
        // On peut uniquement valider les contrats en validation paysan
        if (($this->getCurrentUser() instanceof Paysan)
            && $this->isGrantedContractModelDisplay($mc)) {
            return true;
        }

        return false;
    }

    public function isGrantedContractModelPreviewPdf(ModeleContrat $mc): bool
    {
        // Un paysan peut pré visualiser le contrat qu'il peut valider
        if (($this->getCurrentUser() instanceof Paysan)
            && $this->isGrantedContractModelValidate($mc)) {
            return true;
        }

        // Les autres peuvent pré visualiser les contrats en statut "validation paysan" ou "création" ou "refus"
        if ((in_array($mc->getEtat(), [ModeleContrat::ETAT_CREATION, ModeleContrat::ETAT_REFUS, ModeleContrat::ETAT_VALIDATION_PAYSAN], true))
            && $this->isGrantedContractModelDisplay($mc)) {
            return true;
        }

        return false;
    }

    public function isGrantedContractModelRemove(ModeleContrat $mc): bool
    {
        // Le contrat doit etre visualisable
        if ($this->isGrantedContractModelDisplay($mc)) {
            return $mc->getContrats()->isEmpty();
        }

        return false;
    }

    public function isGrantedContractModelPaymentSummary(ModeleContrat $mc): bool
    {
        // Le contrat doit etre actif et en v2
        if (2 === $mc->getVersion() && ModeleContrat::ETAT_VALIDE === $mc->getEtat()) {
            // Le contrat doit etre visualisable
            return $this->isGrantedContractModelDisplay($mc);
        }

        return false;
    }

    public function isGrantedContractModelMoveDate(ModeleContrat $mc): bool
    {
        return ($this->getCurrentUser() instanceof Amapien || $this->getCurrentUser() instanceof Amap) && $this->isGrantedContractModelDisplay($mc)
        ;
    }

    private function isLastDateExcedeed(ModeleContrat $modeleContrat): bool
    {
        $now = new Carbon();
        $lastDate = $modeleContrat->getLastDate();

        return null !== $lastDate && $now->lessThanOrEqualTo($lastDate->getDateLivraison());
    }
}
