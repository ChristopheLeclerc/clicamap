<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ak_amapien_annee_adhesion")
 */
class AmapienAnneeAdhesion
{
    /**
     * @var ?int
     * @ORM\Column(type="integer", name="a_aa_id")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", name="a_aa_annee")
     */
    private $annee;

    /**
     * @var null|Amapien
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="anneeAdhesions")
     * @ORM\JoinColumn(name="a_aa_fk_amapien_id", referencedColumnName="a_id")
     */
    private $amapien;

    /**
     * AmapienAnneeAdhesion constructor.
     */
    public function __construct(int $annee, Amapien $amapien)
    {
        $this->annee = $annee;
        $this->amapien = $amapien;
    }

    public function __toString()
    {
        return (string) $this->annee;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): AmapienAnneeAdhesion
    {
        $this->annee = $annee;

        return $this;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    /**
     * @param Amapien $amapien
     */
    public function setAmapien(Amapien $amapien = null): AmapienAnneeAdhesion
    {
        $this->amapien = $amapien;

        return $this;
    }
}
