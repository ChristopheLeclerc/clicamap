<?php

namespace PsrLib\Services\ServiceFactories;

use PsrLib\ProjectLocation;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

class SessionFactory
{
    public function create(): Session
    {
        $fileSessionHandler = new NativeFileSessionHandler(ProjectLocation::PROJECT_ROOT.'/application/writable/session');
        $sessionStorage = new NativeSessionStorage([], $fileSessionHandler);

        $session = new Session($sessionStorage);
        $session->start();

        return $session;
    }
}
