# language: fr
Fonctionnalité: Ajouter un nouveau réseau

  @database
  Plan du Scénario: Tester les messages d'erreur
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand je suis sur "/reseau"
    Et que je remplis "reseau" avec "<nom_reseau>"
    Et que je presse "Créer le réseau"
    Alors je devrais voir "<message>"
    Et je devrais être sur "/reseau/creation"
    Et je devrais voir "<nom_reseau>"

    Exemples:
      | nom_reseau | message                            |
      |            | Le champ \"Réseau\" est requis.    |
      | EugèneLand | Le réseau a été créé avec succès ! |
