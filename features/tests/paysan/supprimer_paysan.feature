#language: fr
Fonctionnalité: suppression d'un paysan

  @database
  Plan du Scénario: suppression d'un paysan en tant qu'administrateur
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/paysan"
    Et que je remplis "search_paysan[region]" avec "84"
    Et que je suis "Supprimer le profil"
    Et je devrais voir "Voulez-vous vraiment supprimer le profil de \"PAYSAN NOM Paysan prénom \""
    Et que je suis "supprimer le profil du paysan"
    Et je devrais voir "le paysan a été supprimé avec succès"
    Et je ne devrais pas voir "PAYSAN NOM Paysan prénom"

    Exemples:
      | mail                           |
      | admin.rhone@test.amap-aura.org |
      | admin.aura@test.amap-aura.org  |
