# language: fr
Fonctionnalité: Je dois voir les liens du menu Clic'AMAP dès ma connexion quel que soit mon profil

  Plan du Scénario: J'ai accès au lien de l'accueil
    Etant donné que je suis authentifié comme "<type>" avec le mail "<mail>"
    Et que je suis sur "/evenement"
    Lorsque je suis "Clic'AMAP"
    Et que je suis "Les événements"
    Alors je devrais être sur "/evenement"
    Lorsque je suis "Clic'AMAP"
    Et que je suis "Documentation"
    Alors je devrais être sur "/documentation"
    Lorsque je suis "Clic'AMAP"
    Alors je devrais voir le lien "Clic'AMAP c'est quoi" vers "https://amap-aura.org/categorie/clicamap/"

    Exemples:
      | type    | mail                          |
      | amapien | amapien@test.amap-aura.org    |
      | amapien | amapienref@test.amap-aura.org |
      | amapien | amap@test.amap-aura.org       |
      | amapien | admin@test.amap-aura.org      |
      | amapien | admin.aura@test.amap-aura.org |
      | paysan  | paysan@test.amap-aura.org     |
