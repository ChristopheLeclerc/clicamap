<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var array $logos */
?>
<?php if (!empty($logos)) { ?>
    <div class="slick-logo-reseaux">

        <?php foreach ($logos as $logo) { ?>
            <div>
                <div class="logo-reseau">
                    <a href="<?= prep_url($logo['url']); ?>" target="_blank" >
                        <img class="img-responsive" src="<?= base_url($logo['file']); ?>" />
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>

<script>
    $('.slick-logo-reseaux')
      .not('.slick-initialized')
      .slick({
        dots: true,
        speed: 300,
        slidesToShow: <?php if (count($logos) <= 4) {
    echo count($logos);
} else {
    echo '4';
} ?>,
      });
</script>

<?php } ?>
