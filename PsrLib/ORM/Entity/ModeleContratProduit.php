<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * AkModeleContratProduit.
 *
 * @ORM\Table(name="ak_modele_contrat_produit")
 * @ORM\Entity
 */
class ModeleContratProduit
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="mc_pro_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *  @Groups({"wizard", "wizardContract"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mc_pro_prix", type="string", length=255, nullable=false)
     *  @Groups({"wizard"})
     */
    private $prix;

    /**
     * @var FermeProduit | null
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\FermeProduit")
     * @ORM\JoinColumn(name="mc_pro_fk_ferme_produit_id", referencedColumnName="f_pro_id", onDelete="SET NULL")
     *  @Groups({"wizard"})
     */
    private $fermeProduit;

    /**
     * @var ModeleContrat | null
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\ModeleContrat", inversedBy="produits")
     * @ORM\JoinColumn(name="mc_pro_fk_modele_contrat_id", referencedColumnName="mc_id")
     */
    private $modeleContrat;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_pro_nom", type="string", length=255, nullable=true)
     *  @Groups({"wizard"})
     */
    private $nom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_pro_conditionnement", type="string", length=255, nullable=true)
     *  @Groups({"wizard"})
     */
    private $conditionnement;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_pro_regul_pds", type="boolean", nullable=true)
     *  @Groups({"wizard"})
     */
    private $regulPds = false;

    /**
     * @var ArrayCollection<ModeleContratProduitExclure>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ModeleContratProduitExclure", mappedBy="modeleContratProduit", orphanRemoval=true, cascade="persist")
     * @Groups({"wizard"})
     */
    private $exclusions;

    /**
     * @var TypeProduction | null
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\TypeProduction")
     * @ORM\JoinColumn(referencedColumnName="tp_id")
     * @Groups({"wizard"})
     */
    private $typeProduction;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->exclusions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ModeleContratProduit
    {
        $this->id = $id;

        return $this;
    }

    public function getPrix(): string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): ModeleContratProduit
    {
        $this->prix = $prix;

        return $this;
    }

    public function getFermeProduit(): ?FermeProduit
    {
        return $this->fermeProduit;
    }

    public function setFermeProduit(?FermeProduit $fermeProduit): ModeleContratProduit
    {
        $this->fermeProduit = $fermeProduit;

        return $this;
    }

    public function getModeleContrat(): ?ModeleContrat
    {
        return $this->modeleContrat;
    }

    public function setModeleContrat(?ModeleContrat $modeleContrat): ModeleContratProduit
    {
        $this->modeleContrat = $modeleContrat;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): ModeleContratProduit
    {
        $this->nom = $nom;

        return $this;
    }

    public function getConditionnement(): ?string
    {
        return $this->conditionnement;
    }

    public function setConditionnement(?string $conditionnement): ModeleContratProduit
    {
        $this->conditionnement = $conditionnement;

        return $this;
    }

    public function getRegulPds(): ?bool
    {
        return $this->regulPds;
    }

    public function setRegulPds(?bool $regulPds): ModeleContratProduit
    {
        $this->regulPds = $regulPds;

        return $this;
    }

    /**
     * Add exclusion.
     *
     * @return ModeleContratProduit
     */
    public function addExclusion(ModeleContratProduitExclure $exclusion)
    {
        $exclusion->setModeleContratProduit($this);
        $this->exclusions[] = $exclusion;

        return $this;
    }

    /**
     * Remove exclusion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeExclusion(ModeleContratProduitExclure $exclusion)
    {
        return $this->exclusions->removeElement($exclusion);
    }

    /**
     * Get exclusions.
     *
     * @return Collection
     */
    public function getExclusions()
    {
        return $this->exclusions;
    }

    public function getTypeProduction(): ?TypeProduction
    {
        return $this->typeProduction;
    }

    public function setTypeProduction(?TypeProduction $typeProduction): ModeleContratProduit
    {
        $this->typeProduction = $typeProduction;

        return $this;
    }
}
