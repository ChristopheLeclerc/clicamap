<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\ORM\Repository\AmapCommentaireRepository;
use PsrLib\Validator\UniqueEntity;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AmapRepository")
 * @ORM\Table(name="ak_amap")
 * @UniqueEntity(fields={"email"})
 */
class Amap extends BaseUser
{
    public const ETAT_CREATION = 'creation';
    public const ETAT_ATTENTE = 'attente';
    public const ETAT_FONCIONNEMENT = 'fonctionnement';
    public const ETATS = [
        self::ETAT_CREATION,
        self::ETAT_ATTENTE,
        self::ETAT_FONCIONNEMENT,
    ];

    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(name="amap_id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var null|string
     * @ORM\Column(name="amap_email_public", length=255, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_nom", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $nom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_adresse_admin_lib_adresse", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $adresseAdmin;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_adresse_admin_lib_adresse_nom", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $adresseAdminNom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_date_creation", type="string", length=4, nullable=true)
     * @Assert\Length(min="4", max="4", exactMessage="L'année doivent avoir exactement 4 caractères")
     * @Assert\Type(type="digit", message="L'année doit être constituée de chiffres")
     */
    private $anneeCreation;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_nb_adherents", type="string", length=55, nullable=true)
     * @Assert\Type(type="digit", message="Le nombre d'adhérents doit etre constitué de chiffres")
     * @Assert\Length(max="55")
     */
    private $nbAdherents;

    /**
     * @var null|Amapien
     *
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="amapAsRefReseau")
     * @ORM\JoinColumn(name="amap_fk_contact_referent_reseau_id", referencedColumnName="a_id", onDelete="SET NULL")
     */
    private $amapienRefReseau;

    /**
     * @var null|Amapien
     *
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="amapAsRefReseauSec")
     * @ORM\JoinColumn(name="amap_fk_contact_referent_reseau_secondaire_id", referencedColumnName="a_id", onDelete="SET NULL")
     */
    private $amapienRefReseauSecondaire;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_etat", type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @var bool | null
     *
     * @ORM\Column(name="possede_assurance", type="boolean", nullable=true)
     * @Assert\NotNull(message="Le champ Assurance réseau est requis")
     */
    private $possedeAssurance;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_uuid", type="string", length=255, nullable=true)
     */
    private $uuid;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="amap_etudiante", type="boolean", nullable=true)
     * @Assert\NotNull(message="Le champ AMAP étudiante est requis")
     */
    private $amapEtudiante;

    /**
     * @var bool
     *
     * @ORM\Column(name="amap_email_invalid", type="boolean", nullable=false)
     */
    private $emailInvalid = false;

    /**
     * @var ArrayCollection<AmapLivraisonLieu>
     * @ORM\OneToMany(targetEntity="AmapLivraisonLieu", mappedBy="amap", cascade={"persist", "remove"})
     */
    private $livraisonLieux;

    /**
     * @var ArrayCollection<AmapAnneeAdhesion>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\AmapAnneeAdhesion", mappedBy="amap", cascade="PERSIST", orphanRemoval=true)
     */
    private $anneeAdhesions;

    /**
     * @var ArrayCollection<AmapCommentaire>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\AmapCommentaire", mappedBy="amap", orphanRemoval=true)
     */
    private $comments;

    /**
     * @var ArrayCollection<TypeProduction>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\TypeProduction")
     * @JoinTable(name="ak_amap_type_production_propose",
     *      joinColumns={@JoinColumn(name="amap_tpp_fk_amap_id", referencedColumnName="amap_id")},
     *      inverseJoinColumns={@JoinColumn(name="amap_tpp_fk_type_production_id", referencedColumnName="tp_id")}
     * )
     */
    private $tpPropose;

    /**
     * @var ArrayCollection<TypeProduction>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\TypeProduction")
     * @JoinTable(name="ak_amap_type_production_recherche",
     *      joinColumns={@JoinColumn(name="amap_tpr_fk_amap_id", referencedColumnName="amap_id")},
     *      inverseJoinColumns={@JoinColumn(name="amap_tpr_fk_type_production_id", referencedColumnName="tp_id")}
     * )
     */
    private $tpRecherche;

    /**
     * @var ArrayCollection<Amapien>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\Amapien", mappedBy="amap", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $amapiens;

    /**
     * @var ArrayCollection<AmapCollectif>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\AmapCollectif", mappedBy="amap", orphanRemoval=true)
     */
    private $collectifs;

    /**
     * @var null|Ville
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ville", fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="v_id")
     */
    private $adresseAdminVille;

    /**
     * @var null|bool
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull(message="Le champ Association de fait est requis.")
     */
    private $associationDeFait;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="14", max="14", exactMessage="Le SIRET doit faire exactement 14 caractères.")
     * @Assert\Luhn(message="Le SIRET est invalide.")
     */
    private $siret;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="~^W\d{9}$~", message="Le numéro RNA doit être composé de W suivi de 9 chiffres.")
     */
    private $rna;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->livraisonLieux = new ArrayCollection();
        $this->anneeAdhesions = new ArrayCollection();
        $this->tpPropose = new ArrayCollection();
        $this->amapiens = new ArrayCollection();
        $this->tpRecherche = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
    }

    public function __toString()
    {
        return (string) $this->nom;
    }

    /**
     * @return AmapCommentaire[]
     */
    public function getCommentsOrdered()
    {
        return $this
            ->comments
            ->matching(AmapCommentaireRepository::criteriaOrderDate())
            ->toArray()
        ;
    }

    /**
     * Verifie si l'amap est active.
     */
    public function estActif(): bool
    {
        return true; // Une AMAP est toujours active
    }

    public function getRegion(): ?Region
    {
        if (0 === $this->livraisonLieux->count()) {
            return null;
        }

        return $this->livraisonLieux[0]->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        if (0 === $this->livraisonLieux->count()) {
            return null;
        }

        return $this->livraisonLieux[0]->getDepartement();
    }

    /**
     * @return ?int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): Amap
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Amap
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Add livraisonLieux.
     *
     * @return Amap
     */
    public function addLivraisonLieux(AmapLivraisonLieu $livraisonLieux)
    {
        $this->livraisonLieux[] = $livraisonLieux;

        return $this;
    }

    /**
     * Remove livraisonLieux.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeLivraisonLieux(AmapLivraisonLieu $livraisonLieux)
    {
        return $this->livraisonLieux->removeElement($livraisonLieux);
    }

    /**
     * @return ArrayCollection<AmapLivraisonLieu>
     */
    public function getLivraisonLieux()
    {
        return $this->livraisonLieux;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Amap
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): Amap
    {
        $this->password = $password;

        return $this;
    }

    public function getAmapienRefReseau(): ?Amapien
    {
        return $this->amapienRefReseau;
    }

    public function setAmapienRefReseau(?Amapien $amapienRefReseau): Amap
    {
        $this->amapienRefReseau = $amapienRefReseau;

        return $this;
    }

    public function getAmapienRefReseauSecondaire(): ?Amapien
    {
        return $this->amapienRefReseauSecondaire;
    }

    public function setAmapienRefReseauSecondaire(?Amapien $amapienRefReseauSecondaire): Amap
    {
        $this->amapienRefReseauSecondaire = $amapienRefReseauSecondaire;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): Amap
    {
        $this->etat = $etat;

        return $this;
    }

    public function getAmapEtudiante(): ?bool
    {
        return $this->amapEtudiante;
    }

    public function setAmapEtudiante(?bool $amapEtudiante): Amap
    {
        $this->amapEtudiante = $amapEtudiante;

        return $this;
    }

    /**
     * Add Comments.
     *
     * @return Amap
     */
    public function addComment(AmapCommentaire $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove Comments.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeComment(AmapCommentaire $comments)
    {
        return $this->comments->removeElement($comments);
    }

    /**
     * @return ArrayCollection<AmapCommentaire>
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function getAnneeCreation(): ?string
    {
        return $this->anneeCreation;
    }

    public function setAnneeCreation(?string $anneeCreation): Amap
    {
        $this->anneeCreation = $anneeCreation;

        return $this;
    }

    public function getNbAdherents(): ?string
    {
        return $this->nbAdherents;
    }

    public function setNbAdherents(?string $nbAdherents): Amap
    {
        $this->nbAdherents = $nbAdherents;

        return $this;
    }

    /**
     * Add tppPropose.
     *
     * @return Amap
     */
    public function addTpPropose(TypeProduction $tppPropose)
    {
        $this->tpPropose[] = $tppPropose;

        return $this;
    }

    /**
     * Remove tppPropose.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTpPropose(TypeProduction $tppPropose)
    {
        return $this->tpPropose->removeElement($tppPropose);
    }

    /**
     * Get tppPropose.
     *
     * @return Collection
     */
    public function getTpPropose()
    {
        return $this->tpPropose;
    }

    /**
     * @return TypeProduction[]
     */
    public function getTpProposeOrdered()
    {
        return $this
            ->tpPropose
            ->matching(\PsrLib\ORM\Repository\TypeProductionRepository::criteriaOrderId())
            ->toArray()
        ;
    }

    /**
     * Add tpRecherche.
     *
     * @return Amap
     */
    public function addTpRecherche(TypeProduction $tpRecherche)
    {
        $this->tpRecherche[] = $tpRecherche;

        return $this;
    }

    /**
     * Remove tpRecherche.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTpRecherche(TypeProduction $tpRecherche)
    {
        return $this->tpRecherche->removeElement($tpRecherche);
    }

    /**
     * Get tpRecherche.
     *
     * @return Collection
     */
    public function getTpRecherche()
    {
        return $this->tpRecherche;
    }

    /**
     * Get tpRecherche.
     *
     * @return TypeProduction[]
     */
    public function getTpRechercheOrdered()
    {
        return $this->tpRecherche
            ->matching(\PsrLib\ORM\Repository\TypeProductionRepository::criteriaOrderId())
            ->toArray()
        ;
    }

    public function getAdresseAdmin(): ?string
    {
        return $this->adresseAdmin;
    }

    public function setAdresseAdmin(?string $adresseAdmin): Amap
    {
        $this->adresseAdmin = $adresseAdmin;

        return $this;
    }

    /**
     * Add amapien.
     *
     * @return Amap
     */
    public function addAmapien(Amapien $amapien)
    {
        $this->amapiens[] = $amapien;
        $amapien->setAmap($this);

        return $this;
    }

    /**
     * Remove amapien.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAmapien(Amapien $amapien)
    {
        $amapien->setAmap(null);

        return $this->amapiens->removeElement($amapien);
    }

    /**
     * Get amapiens.
     *
     * @return Collection
     */
    public function getAmapiens()
    {
        return $this->amapiens;
    }

    /**
     * Get amapiens gestionnaires.
     *
     * @return ArrayCollection
     */
    public function getAmapiensGestionnaires()
    {
        return $this->amapiens
            ->matching(\PsrLib\ORM\Repository\AmapienRepository::criteriaGestionaire())
        ;
    }

    /**
     * Add anneeAdhesion.
     *
     * @return Amap
     */
    public function addAnneeAdhesion(AmapAnneeAdhesion $anneeAdhesion)
    {
        $this->anneeAdhesions[] = $anneeAdhesion;

        return $this;
    }

    /**
     * Remove anneeAdhesion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAnneeAdhesion(AmapAnneeAdhesion $anneeAdhesion)
    {
        return $this->anneeAdhesions->removeElement($anneeAdhesion);
    }

    /**
     * Get anneeAdhesions.
     *
     * @return Collection
     */
    public function getAnneeAdhesions()
    {
        return $this->anneeAdhesions;
    }

    /**
     * @return AmapAnneeAdhesion[]
     */
    public function getAnneeAdhesionsOrdered()
    {
        $adhesions = $this->anneeAdhesions->toArray();
        usort($adhesions, function (AmapAnneeAdhesion $a, AmapAnneeAdhesion $b) {
            return $b->getAnnee() - $a->getAnnee();
        });

        return $adhesions;
    }

    /**
     * Add collectif.
     *
     * @return Amap
     */
    public function addCollectif(AmapCollectif $collectif)
    {
        $this->collectifs[] = $collectif;

        return $this;
    }

    /**
     * Remove collectif.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCollectif(AmapCollectif $collectif)
    {
        return $this->collectifs->removeElement($collectif);
    }

    /**
     * Get collectifs.
     *
     * @return Collection
     */
    public function getCollectifs()
    {
        return $this->collectifs;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Amap
    {
        $this->url = $url;

        return $this;
    }

    public function isEmailInvalid(): bool
    {
        return $this->emailInvalid;
    }

    public function setEmailInvalid(bool $emailInvalid): Amap
    {
        $this->emailInvalid = $emailInvalid;

        return $this;
    }

    public function isPossedeAssurance(): ?bool
    {
        return $this->possedeAssurance;
    }

    public function setPossedeAssurance(?bool $possedeAssurance): Amap
    {
        $this->possedeAssurance = $possedeAssurance;

        return $this;
    }

    public function getAdresseAdminVille(): ?Ville
    {
        return $this->adresseAdminVille;
    }

    public function setAdresseAdminVille(?Ville $adresseAdminVille): Amap
    {
        $this->adresseAdminVille = $adresseAdminVille;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function getAssociationDeFait(): ?bool
    {
        return $this->associationDeFait;
    }

    public function setAssociationDeFait(?bool $associationDeFait): Amap
    {
        $this->associationDeFait = $associationDeFait;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): Amap
    {
        $this->siret = $siret;

        return $this;
    }

    public function getRna(): ?string
    {
        return $this->rna;
    }

    public function setRna(?string $rna): Amap
    {
        $this->rna = $rna;

        return $this;
    }

    public function getAdresseAdminNom(): ?string
    {
        return $this->adresseAdminNom;
    }

    public function setAdresseAdminNom(?string $adresseAdminNom): Amap
    {
        $this->adresseAdminNom = $adresseAdminNom;

        return $this;
    }
}
