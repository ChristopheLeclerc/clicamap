<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('libraries/htmlpurifier-4.10.0')
    ->exclude('libraries/tcpdf')
    ->exclude('third_party')
    ->exclude('logs')
    ->exclude('writable')
    ->in(__DIR__ . '/application')
    ->in(__DIR__ . '/features/bootstrap')
    ->in(__DIR__ . '/PsrLib')
    ->in(__DIR__ . '/tests')
;

return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR1' => true,
        '@PSR2' => true,
        '@PhpCsFixer' => true,
        '@PHP56Migration' => true,
        '@PHP70Migration' => true,
        '@PHP71Migration' => true,
        'array_syntax' => ['syntax' => 'short'],
        'no_alternative_syntax' => false,
        'no_short_echo_tag' => false,
        'phpdoc_to_comment' => false,
        'echo_tag_syntax' => ['format' => 'short'],
    ])
    ->setFinder($finder)
;
