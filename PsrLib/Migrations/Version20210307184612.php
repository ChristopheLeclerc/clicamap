<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184612 extends AbstractMigration
{
    private $existingAdmins = [];

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        $existingAdmins = $this
            ->connection
            ->executeQuery('SELECT a.a_id FROM ak_amapien a
                JOIN ak_super_administrateur asa ON asa.sup_adm_fk_amapien_id = a.a_id
            ;')
            ->fetchAll()
        ;

        $this->existingAdmins = array_column($existingAdmins, 'a_id');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_amapien ADD super_admin TINYINT(1) NOT NULL');
        //$this->addSql('DROP TABLE ak_super_administrateur'); TODO

        foreach ($this->existingAdmins as $admin) {
            $this->addSql('UPDATE ak_amapien SET super_admin=1 WHERE a_id=:admin', ['admin' => $admin]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
