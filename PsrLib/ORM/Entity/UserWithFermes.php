<?php

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\Collection;

interface UserWithFermes
{
    /**
     * @return Collection
     */
    public function getFermes();
}
