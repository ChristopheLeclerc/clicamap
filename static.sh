#!/bin/sh

set -xe

vendor/bin/php-cs-fixer fix --dry-run -vv
vendor/bin/twigcs templates
php -d memory_limit=4G vendor/bin/phpstan analyse PsrLib
vendor/bin/doctrine orm:validate-schema --skip-sync
php public/index.php command lintTwig
