<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \Symfony\Component\Form\FormView $form */
?>

<script type="text/javascript">

  /*ALERTES & AUTOCOMPLÉTION CP / VILLE */
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 2500);

  });

</script>

<?php

if (get_current_connected_user() instanceof Amap) {
    $retour = 'Gestion de mon AMAP';
} else {
    $retour = 'Gestion des AMAP';
}

echo '<h3>Le collectif de l\'AMAP</h3>';

echo '<h5><a href="'.site_url('amap').'">'.$retour.'</a> <i class="glyphicon glyphicon-menu-right"></i> '.$amap->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Collectif</h5>';

echo '<div class="alert alert-info">Les rôles de président, trésorier et secrétaire ne peuvent-être attribués qu\'une fois. En revanche, un rôle précisé par vos soins avec la mention "Autre" peut-être attribué à plusieurs personnes. En outre, une personne peut avoir plusieurs  rôles.</div>';

?>

<div class="row">
    <div class="col-md-12">

        <form method="POST" action="<?= site_url('amap/collectif/'.$amap->getId()); ?>" class="clearfix">

            <div class="well well-sm">

                <div class="row">
                    <div class="col-md-12">
                        <?= render_form_global_errors($form); ?>

                        <div class="form-group col-md-4">
                            <label for="profil" class="control-label">Profil : </label>
                            <?= render_form_select_widget($form['profil']); ?>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="precision" class="control-label">Précision : </label>
                            <?= render_form_text_widget($form['precision']); ?>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="amapien_id" class="control-label">Amapien : </label>
                            <?= render_form_select_widget($form['amapien']); ?>
                        </div>

                    </div>
                </div>

            </div>

            <div class="form-group pull-right">

                <input type="submit" value="Valider" class="pull-right btn btn-success clearfix"
                       style="margin-left:5px;"/>
                <a href="<?= site_url('amap'); ?>" class="pull-right btn btn-default">Annuler</a>

            </div>

        </form>

    </div>
</div>

<?php

$collectifs = $amap->getCollectifs();
if (!$collectifs->isEmpty()) {
    ?>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-inverse">
            <tr>
                <th>Profil</th>
                <th>Précision</th>
                <th>Email</th>
                <th class="text-right">Outils</th>
            </tr>
            </thead>

            <tbody>

            <?php

            // Pour régler l'orde d'affichage ----------------------------------------
            // D'abord le président --------------------------------------------------
            /** @var \PsrLib\ORM\Entity\AmapCollectif $coll */
            foreach ($collectifs as $coll) {
                if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_PRESIDENT == $coll->getProfil()) {
                    echo '<tr>';
                    echo '<th scope="row">Président</th>';
                    echo '<td>';
                    if ($coll->getPrecision()) {
                        echo ucfirst(mb_strtolower($coll->getPrecision()));
                    }
                    // else
                    //   echo 'Aucune';
                    echo '</td>';
                    echo '<td><a href="mailto:'.$coll->getAmapien()->getEmail().'">'.$coll->getAmapien()->getEmail().'</a></td>';
                    echo '<td class="text-right"><a title="Retirer le rôle" href="#" data-toggle="modal" data-target="#modal_remove_'.$coll->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Retirer le rôle"></i></a>&nbsp;</td>';
                    echo '</tr>';
                }

                echo '<div class="modal fade text-left" id="modal_remove_'.$coll->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
                echo '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">';
                echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
                echo '<h4 class="modal-title" id="myModalLabel">AMAP "'.$amap->getNom().'"</h4>';
                echo '</div>';
                echo '<div class="modal-body">';
                echo '<h5>Voulez-vous vraiment retirer ce rôle à "'.$coll->getAmapien()->getNom().' '.$coll->getAmapien()->getPrenom().'" ?</h5>';
                echo '<p class="text-right">';
                echo '<a href="'.site_url('amap/collectif_remove/'.$coll->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
                echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
                echo '</p>';
                echo '</div></div></div></div>';
            }
    // Ensuite le secrétaire -------------------------------------------------
    foreach ($collectifs as $coll) {
        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_SECRETAIRE == $coll->getProfil()) {
            echo '<tr>';
            echo '<th scope="row">Secrétaire</th>';
            echo '<td>';
            if ($coll->getPrecision()) {
                echo ucfirst(mb_strtolower($coll->getPrecision()));
            }
            // else
            //   echo 'Aucune';
            echo '</td>';
            echo '<td><a href="mailto:'.$coll->getAmapien()->getEmail().'">'.$coll->getAmapien()->getEmail().'</a></td>';
            echo '<td class="text-right"><a title="Retirer le rôle" href="#" data-toggle="modal" data-target="#modal_remove_'.$coll->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Retirer le rôle"></i></a>&nbsp;</td>';
            echo '</tr>';
        }

        echo '<div class="modal fade text-left" id="modal_remove_'.$coll->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        echo '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">';
        echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        echo '<h4 class="modal-title" id="myModalLabel">AMAP "'.$amap->getNom().'"</h4>';
        echo '</div>';
        echo '<div class="modal-body">';
        echo '<h5>Voulez-vous vraiment retirer ce rôle à "'.$coll->getAmapien()->getNom().' '.$coll->getAmapien()->getPrenom().'" ?</h5>';
        echo '<p class="text-right">';
        echo '<a href="'.site_url('amap/collectif_remove/'.$coll->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
        echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
        echo '</p>';
        echo '</div></div></div></div>';
    }

    // Ensuite le secrétaire -------------------------------------------------
    foreach ($collectifs as $coll) {
        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_TRESORIER == $coll->getProfil()) {
            echo '<tr>';
            echo '<th scope="row">Trésorier</th>';
            echo '<td>';
            if ($coll->getPrecision()) {
                echo ucfirst(mb_strtolower($coll->getPrecision()));
            }
            // else
            //   echo 'Aucune';
            echo '</td>';
            echo '<td><a href="mailto:'.$coll->getAmapien()->getEmail().'">'.$coll->getAmapien()->getEmail().'</a></td>';
            echo '<td class="text-right"><a title="Retirer le rôle" href="#" data-toggle="modal" data-target="#modal_remove_'.$coll->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Retirer le rôle"></i></a>&nbsp;</td>';
            echo '</tr>';
        }

        echo '<div class="modal fade text-left" id="modal_remove_'.$coll->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        echo '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">';
        echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        echo '<h4 class="modal-title" id="myModalLabel">AMAP "'.$amap->getNom().'"</h4>';
        echo '</div>';
        echo '<div class="modal-body">';
        echo '<h5>Voulez-vous vraiment retirer ce rôle à "'.$coll->getAmapien()->getNom().' '.$coll->getAmapien()->getPrenom().'" ?</h5>';
        echo '<p class="text-right">';
        echo '<a href="'.site_url('amap/collectif_remove/'.$coll->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
        echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
        echo '</p>';
        echo '</div></div></div></div>';
    }

    // Puis les autres rôles -------------------------------------------------
    foreach ($collectifs as $coll) {
        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_AUTRE == $coll->getProfil()) {
            echo '<tr>';
            echo '<th scope="row">Autre</th>';
            echo '<td>';
            if ($coll->getPrecision()) {
                echo ucfirst(mb_strtolower($coll->getPrecision()));
            }
            // else
            //   echo 'Aucune';
            echo '</td>';
            echo '<td><a href="mailto:'.$coll->getAmapien()->getEmail().'">'.$coll->getAmapien()->getEmail().'</a></td>';
            echo '<td class="text-right"><a title="Retirer le rôle" href="#" data-toggle="modal" data-target="#modal_remove_'.$coll->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Retirer le rôle"></i></a>&nbsp;</td>';
            echo '</tr>';
        }

        echo '<div class="modal fade text-left" id="modal_remove_'.$coll->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        echo '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">';
        echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        echo '<h4 class="modal-title" id="myModalLabel">AMAP "'.$amap->getNom().'"</h4>';
        echo '</div>';
        echo '<div class="modal-body">';
        echo '<h5>Voulez-vous vraiment retirer ce rôle à "'.$coll->getAmapien()->getNom().' '.$coll->getAmapien()->getPrenom().'" ?</h5>';
        echo '<p class="text-right">';
        echo '<a href="'.site_url('amap/collectif_remove/'.$coll->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
        echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
        echo '</p>';
        echo '</div></div></div></div>';
    } ?>

            </tbody>
        </table>
    </div>

    <?php
} else {
        echo '<div class="alert alert-warning">Aucun rôle n\'a encore été défini au sein du collectif de l\'AMAP.</div>';
    }

?>
