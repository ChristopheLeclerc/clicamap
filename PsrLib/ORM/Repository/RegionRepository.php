<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use LogicException;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;

/**
 * @extends EntityRepository<Region>
 */
class RegionRepository extends EntityRepository
{
    /**
     * @var array
     */
    private $cachedUserRegionMap = [];

    /**
     * @return Region[]
     */
    public function findAllOrdered(): array
    {
        return $this
            ->createQueryBuilder('r')
            ->orderBy('r.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Region[]
     */
    public function getAdminRegionForAmapien(Amapien $amapien): array
    {
        if ($amapien->isAdminRegion()) {
            return $amapien->getAdminRegions()->toArray();
        }

        return [];
    }

    /**
     * @return Region[]
     */
    public function findWithLogo()
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.logo IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get accessible region for user. Memory cache result to avoid recurring database requests.
     *
     * @return Region[]
     */
    public function getRegionForUserCached(BaseUser $user)
    {
        $userId = $user->getId();
        if (isset($this->cachedUserRegionMap[$userId])) {
            return $this->cachedUserRegionMap[$userId];
        }
        if ($user instanceof Amapien && $user->isSuperAdmin()) {
            $regions = $this->findAllOrdered();
        } elseif ($user instanceof Amapien && $user->isAdminRegion()) {
            $regions = $user->getAdminRegions()->toArray();
        } elseif ($user instanceof Amapien && $user->isAdminDepartment()) {
            $regions = $user->getAdminDepartments()->map(function (Departement $departement) {
                return $departement->getRegion();
            })->toArray();
        } elseif ($user instanceof Amap) {
            $regions = $this->findAllOrdered();
        } else {
            throw new LogicException('Invalid user : '.$user->getEmail());
        }

        $this->cachedUserRegionMap[$userId] = $regions;

        return $regions;
    }
}
