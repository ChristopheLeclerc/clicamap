<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\ORM\Entity\Embeddable;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/** @Embeddable */
class Period
{
    /**
     * @var ?Carbon
     * @ORM\Column(type="carbon")
     * @Assert\NotBlank
     */
    private $startAt;

    /**
     * @var ?Carbon
     * @ORM\Column(type="carbon")
     * @Assert\NotBlank
     */
    private $endAt;

    /**
     * @Assert\Callback
     *
     * @param mixed $payload
     */
    public function validateDates(ExecutionContextInterface $context, $payload): void
    {
        $start = $this->getStartAt();
        $end = $this->getEndAt();

        if (null === $start || null === $end) {
            return;
        }

        if ($start->gt($end)) {
            $context
                ->buildViolation('La date de fin doit être après la date de debut.')
                ->addViolation()
            ;
        }
    }

    public static function buildFromDates(Carbon $startAt, Carbon $endAt): self
    {
        $period = new self();
        $period->setStartAt($startAt);
        $period->setEndAt($endAt);

        return $period;
    }

    public function getCarbonPeriod(): ?CarbonPeriod
    {
        if (null === $this->getStartAt() || null === $this->getEndAt()) {
            return null;
        }

        return new CarbonPeriod($this->getStartAt(), $this->getEndAt());
    }

    public function getStartAt(): ?Carbon
    {
        return $this->startAt;
    }

    public function setStartAt(?Carbon $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?Carbon
    {
        return $this->endAt;
    }

    public function setEndAt(?Carbon $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }
}
