<?php

namespace Test\Services\Security;

use Carbon\Carbon;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class DistributionCheckAmapienRegisterTest extends AbstractSecurityTest
{
    public function testRegisterAsPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        $distribution = $this->prophesize(AmapDistribution::class)->reveal();
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution));
    }

    public function testRegisterAsAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        $distribution = $this->prophesize(AmapDistribution::class)->reveal();
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution));
    }

    public function testRegisterAsAmapienValid()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            10,
            new Carbon('2000-01-01')
        );
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->mockAuthenticatedUser($amapien);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution));
    }

    public function testRegisterAsAmapienInvalidAmap()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            10,
            new Carbon('2000-01-01')
        );
        $amapien->setAmap(new Amap());
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->mockAuthenticatedUser($amapien);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution));
    }

    public function testRegisterAsAmapienAlreadyRegistered()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            10,
            new Carbon('2000-01-01')
        );
        $distribution->addAmapien($amapien);
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->mockAuthenticatedUser($amapien);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution));
    }

    public function testRegisterAsAmapienAfterStart()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            10,
            new Carbon('2000-01-01')
        );
        Carbon::setTestNow('2000-01-01 12:00:00');

        $this->mockAuthenticatedUser($amapien);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution));
    }

    public function testRegisterAsAmapienFull()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            3,
            new Carbon('2000-01-01')
        );
        $distribution->addAmapien(new Amapien());
        $distribution->addAmapien(new Amapien());
        $distribution->addAmapien(new Amapien());
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->mockAuthenticatedUser($amapien);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution));
    }

    private function buildDistribution(Amapien $amapien, Time $heureDebut, Time $heureFin, int $nbPersonnes, Carbon $date)
    {
        return new AmapDistribution(
            AmapDistributionDetail::create($heureDebut, $heureFin, $nbPersonnes, 'tache'),
            $amapien->getAmap()->getLivraisonLieux()->first(),
            $date
        );
    }

    private function buildAmapien()
    {
        $amapien = new Amapien();
        $ll = new AmapLivraisonLieu();
        $amap = new Amap();
        $amap->addLivraisonLieux($ll);
        $ll->setAmap($amap);
        $amapien->setAmap($amap);

        return $amapien;
    }
}
