<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
/** @var \Symfony\Component\Form\FormView $form */
?>
<script type="text/javascript">

  /* ALERTES & AUTOCOMPLÉTION CP / VILLE */
  $(document).ready(function () {

  $('input[name="ferme_paysan[paysan]"]').autocomplete({
    source: <?= '"'.site_url('ajax/get_paysan_suggestion"'); ?>,
    dataType: "json",
    minLength: 3,
    appendTo: "#container"
  });

  });

</script>

<?php
echo '<h3>Paysans de la ferme</h3>';

echo '<h5><a href="'.site_url('ferme').'">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.ucfirst($ferme->getNom()).'</a> <i class="glyphicon glyphicon-menu-right"></i> Paysans</h5>';
?>
<div class="alert alert-info">Si vous voulez ajouter un paysan à la ferme, utilisez le champ ci-dessous <strong>en vous
        aidant des suggestions proposées en cours de frappe</strong>. Dans le champ d'ajout, il convient de commencer
    par le nom. L'ajout se fera via l'email afin d'éviter tout problème en cas d\'homonyme.
</div>

<form method="POST" action="">

    <div class="row">
        <div class="form-group col-sm-12">
            <?= render_form_text_widget($form['paysan']); ?>
            <div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" value="Ajouter" class="btn btn-success pull-right"/>
        </div>
    </div>
</form>


<div class="table-responsive">
    <table class="table">
        <thead class="thead-inverse">
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Email</th>
            <th class="text-right">Outils</th>
        </tr>
        </thead>

        <?php

        echo '<tbody>';

        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        foreach ($ferme->getPaysans() as $paysan) {
            echo '<tr>';
            echo '<th scope="row">';
            echo strtoupper($paysan->getNom());
            echo '</th>';

            echo '<td>';
            echo ucfirst($paysan->getPrenom());
            echo '</td>';

            echo '<td>';
            echo '<a href="mailto:'.$paysan->getEmail().'">'.$paysan->getEmail().'</a>';
            echo '</td>';

            echo '<td class="text-right">';

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DISPLAY_PROFIL, $paysan)) {
                echo '<a title="Détails du profil" href="'.site_url('paysan/display/'.$paysan->getId()).'"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="Détails du profil"></i></a>&nbsp;';
            }

            echo '<a title="Retirer le paysan" href="#" data-toggle="modal" data-target="#modal_remove_ref_'.$paysan->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Retirer le paysan"></i></a> '; ?>

            <!-- Modal remove -->
            <div class="modal fade text-left" id="modal_remove_ref_<?= $paysan->getId(); ?>" tabindex="-1"
                 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h5 class="modal-title" id="myModalLabel">
                                <?= 'Voulez-vous vraiment retirer '.strtoupper($paysan->getNom()).' '.ucfirst($paysan->getPrenom()).' de "'.$ferme->getNom().'" ?'; ?>
                            </h5>
                        </div>
                        <div class="modal-body">
                            <p class="text-right">
                                <a href="<?= site_url('ferme/remove_paysan/'.$paysan->getId()); ?>"
                                   class="btn btn-danger btn-sm">OUI</a>
                                <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <?php
// echo '<a href="'.site_url('amapien/key/'.$amapien->a_id).'"><i class="glyphicon glyphicon-lock"></i></a>';
            echo '</td>';
            echo '</tr>';
        }

        echo '</tbody>';
        echo '</table>';
        echo '</div>';

        ?>
