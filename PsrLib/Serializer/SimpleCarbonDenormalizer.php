<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Carbon\Carbon;
use PsrLib\Exception\AdhesionDecodeException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class SimpleCarbonDenormalizer implements DenormalizerInterface
{
    public function denormalize($data, $type, $format = null, array $context = [])
    {
        if (!is_string($data) || 1 !== preg_match('~\d{2}/\d{2}/\d{4}~', $data)) {
            throw new AdhesionDecodeException('Impossible de convertir en date '.$data);
        }

        return Carbon::createFromFormat('d/m/Y', $data)->startOfDay();
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return Carbon::class === $type;
    }
}
