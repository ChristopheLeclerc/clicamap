<?php

namespace PsrLib\Services\Security\Checkers;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class PaysanCheck extends AbstractCheck
{
    public function isGrantedPaysanDisplayProfil(Paysan $paysan): bool
    {
        // Un paysan peut éditer son propre profil
        if ($this->getCurrentUser() instanceof Paysan) {
            return $paysan->getId() === $this->getCurrentUser()->getId();
        }

        if ($this->getCurrentUser() instanceof FermeRegroupement) {
            $paysanFerme = $paysan->getFerme();

            return $paysanFerme->inRegroupement() && $paysanFerme->getRegroupement() === $this->getCurrentUser();
        }

        return $this->editProfilExternal($paysan);
    }

    public function editProfilExternal(Paysan $paysan): bool
    {
        if ($this->getCurrentUser() instanceof Amapien) {
            // Les rôles suivant peuvent éditer tous les paysans
            if ($this->getCurrentUser()->isAdmin()) { // Admin de réseau
                return true;
            }

            // Le référent produit peut éditer les paysans des fermes dont il est référent
            if ($this->getCurrentUser()->isRefProduit()) {
                /** @var Ferme $ferme */
                foreach ($this->getCurrentUser()->getRefProdFermes() as $ferme) {
                    if ($ferme->getPaysans()->contains($paysan)) {
                        return true;
                    }
                }

                return false;
            }
        }

        // L'amap peut éditer tous les paysans
        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        return false;
    }

    public function isGrantedPaysanCreate(): bool
    {
        if ($this->getCurrentUser() instanceof Amapien) {
            return $this->getCurrentUser()->isAdmin() || $this->getCurrentUser()->isRefProduit();
        }

        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        if ($this->getCurrentUser() instanceof FermeRegroupement) {
            return true;
        }

        return false;
    }

    public function isGrantedPaysanDownloadList(): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && (
                $this->getCurrentUser()->isSuperAdmin()
                || $this->getCurrentUser()->isAdminRegion()
                || $this->getCurrentUser()->isAdminDepartment()
            );
    }

    public function isGrantedPaysanDownloadListRegroupement(): bool
    {
        return $this->getCurrentUser() instanceof FermeRegroupement;
    }

    public function isGrantedPaysanEditComment(Paysan $paysan): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isAdminOf($paysan)
        ;
    }

    public function isGrantedPaysanChangeAdhesion(Paysan $paysan): bool
    {
        // Seul les amapiens peuvent changer les années d'adhésion des paysans
        if ($this->getCurrentUser() instanceof Amapien) {
            return $this->editProfilExternal($paysan);
        }

        return false;
    }

    public function isGrantedPaysanChangeCr(Paysan $paysan): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isAdminOf($paysan)
            ;
    }
}
