<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AkPaysanCadreReseau.
 *
 * @ORM\Table(name="ak_paysan_cadre_reseau")
 * @ORM\Entity
 */
class PaysanCadreReseau
{
    /**
     * @var null|int
     *
     * @ORM\Column(name="pay_cr_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="pay_cr_grpe_W", type="boolean")
     */
    private $payCrGrpeW = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="pay_cr_CA", type="boolean")
     */
    private $payCrCa = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="pay_cr_visite_dem", type="boolean")
     */
    private $payCrVisiteDem = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="pay_cr_fiche_loc_exp", type="boolean")
     */
    private $payCrFicheLocExp = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="pay_cr_cagn_sol", type="boolean")
     */
    private $payCrCagnSol = false;

    /**
     * @var Paysan
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Paysan", inversedBy="cadreReseau")
     * @ORM\JoinColumn(name="pay_cr_fk_pay_id", referencedColumnName="pay_id", nullable=false)
     */
    private $paysan;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isPayCrGrpeW(): bool
    {
        return $this->payCrGrpeW;
    }

    public function setPayCrGrpeW(bool $payCrGrpeW): PaysanCadreReseau
    {
        $this->payCrGrpeW = $payCrGrpeW;

        return $this;
    }

    public function isPayCrCa(): bool
    {
        return $this->payCrCa;
    }

    public function setPayCrCa(bool $payCrCa): PaysanCadreReseau
    {
        $this->payCrCa = $payCrCa;

        return $this;
    }

    public function isPayCrVisiteDem(): bool
    {
        return $this->payCrVisiteDem;
    }

    public function setPayCrVisiteDem(bool $payCrVisiteDem): PaysanCadreReseau
    {
        $this->payCrVisiteDem = $payCrVisiteDem;

        return $this;
    }

    public function isPayCrFicheLocExp(): bool
    {
        return $this->payCrFicheLocExp;
    }

    public function setPayCrFicheLocExp(bool $payCrFicheLocExp): PaysanCadreReseau
    {
        $this->payCrFicheLocExp = $payCrFicheLocExp;

        return $this;
    }

    public function isPayCrCagnSol(): bool
    {
        return $this->payCrCagnSol;
    }

    public function setPayCrCagnSol(bool $payCrCagnSol): PaysanCadreReseau
    {
        $this->payCrCagnSol = $payCrCagnSol;

        return $this;
    }

    public function getPaysan(): Paysan
    {
        return $this->paysan;
    }

    public function setPaysan(Paysan $paysan): PaysanCadreReseau
    {
        $this->paysan = $paysan;

        return $this;
    }
}
