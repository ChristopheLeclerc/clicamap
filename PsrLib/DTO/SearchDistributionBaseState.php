<?php

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Validator\Constraints as Assert;

class SearchDistributionBaseState
{
    /**
     * @Assert\NotNull()
     *
     * @var ?AmapLivraisonLieu
     */
    private $livLieu;

    /**
     * @var ?Period
     * @Assert\NotNull()
     * @Assert\Valid
     */
    private $period;

    public function getLivLieu(): ?AmapLivraisonLieu
    {
        return $this->livLieu;
    }

    public function setLivLieu(?AmapLivraisonLieu $livLieu): SearchDistributionBaseState
    {
        $this->livLieu = $livLieu;

        return $this;
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): SearchDistributionBaseState
    {
        $this->period = $period;

        return $this;
    }
}
