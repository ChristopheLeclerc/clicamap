#language: fr
Fonctionnalité: Inscription d'un amapien  à une distribution en tant qu'amap

  @database
  Scénario: Inscription d'un amapien à une distribution en tant qu'amap
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion de la distrib'AMAP"
    Et que je devrais voir "Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions"
    Et que je remplis le datepicker "search_distribution_amap[period]" avec "01/01/2030 au 01/12/2030"
    Et que je suis "Gestion des inscriptions"
    Quand je sélectionne "2" depuis "amap_distribution_amap_amapien_inscription_amapiens"
    Et que je presse "Ajouter un ou plusieurs amapiens"
    Alors je devrais voir "Amapiens ajoutés avec succès"
    Et l'élément ".table-datatable" devrait contenir "prenom amapien"
    Et un mail avec le sujet "Votre inscription à la Distrib'AMAP du 01/01/2030" devrait avoir été envoyé à "amapien@test.amap-aura.org"

  @database
  Scénario: Inscription de plus amapiens que de place à une distribution en tant qu'amap
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion de la distrib'AMAP"
    Et que je remplis le datepicker "search_distribution_amap[period]" avec "01/01/2030 au 01/12/2030"
    Et que je suis "Gestion des inscriptions"
    Quand je sélectionne "2" depuis "amap_distribution_amap_amapien_inscription_amapiens"
    Quand je sélectionne une autre option "3" depuis "amap_distribution_amap_amapien_inscription_amapiens"
    Et que je presse "Ajouter un ou plusieurs amapiens"
    Alors je devrais voir "Pas assez de places disponibles pour ajouter autant d'amapiens à cette distribution."
    Et l'élément ".table-datatable" ne devrait pas contenir "prenom amapien"
    Et l'élément ".table-datatable" ne devrait pas contenir "Prénom amapien référent"
