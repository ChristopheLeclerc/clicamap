<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amapien $amapien */
$currentUser = get_current_connected_user();
?>

<script type="text/javascript">

  /*ALERTES*/
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 2500);

  });

</script>

<?php
if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_OWN_PROFILE, $amapien)) {
    echo '<h3>Mon profil</h3>';
} else {
    echo '<h3>Détails du profil</h3>';
    echo '<h5><a href="'.site_url('amapien').'">Gestion des amapiens</a> <i class="glyphicon glyphicon-menu-right"></i> '.$amapien->getPrenom().' '.$amapien->getNom().'</h5>';
}
?>

<p class="text-right">
<?php
    if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien)) {
        if (\PsrLib\ORM\Entity\Amapien::ETAT_ACTIF === $amapien->getEtat()) {
            $icon = 'pause';
            $texte = 'Désactiver le profil';
        } else {
            $icon = 'play';
            $texte = 'Activer le profil';
        }
        echo '<a href="'.site_url('amapien/de_activate/'.$amapien->getId().'/'.$amapien->getEtat()).'" class="btn btn-primary"><span class="glyphicon glyphicon-'.$icon.'">&nbsp;</span>'.$texte.'</a>&nbsp;';
    }

    $editionUrl = is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_OWN_PROFILE, $amapien)
    ? site_url('amapien/informations_generales_edition_perso/'.$amapien->getId())
    : site_url('amapien/informations_generales_edition/'.$amapien->getId())
    ;
    echo '<a href="'.$editionUrl.'" class="btn btn-success"><span class="glyphicon glyphicon-edit">&nbsp;</span>Éditer le profil</a>';
    ?>
</p>

<div class="table-responsive well">
    <h3 style="font-size:20px;"><?= $amapien->GetNom().' '.$amapien->GetPrenom(); ?></h3>
    <table class="table">

        <tbody>

        <?php

        echo '<tr>';
        echo '<th>État du profil</th>';
        echo '<td>'.ucfirst($amapien->getEtat()).'</td>';
        echo '</tr>';

        $sup_adm = $amapien->isSuperAdmin();
        $adm_res = $amapien->isAdminReseaux();
        $gest_amap = $amapien->isGestionnaire();
        $ref_pro = $amapien->isRefProduit();
        $ref_res = $amapien->isRefAmap();
        $ref_res_sec = $amapien->isRefAmapSec();
        if ($sup_adm || $adm_res || $gest_amap || $ref_res || $ref_res_sec || $ref_pro) {
            echo '<tr>';
            echo '<th>Rôle</th>';
            echo '<td>';

            if ($sup_adm) {
                echo '<div><strong>Super Administrateur</strong>.</div>';
            }

            if ($adm_res) {
                echo '<div><strong>Administrateur réseau</strong> de : ';
                echo implode(' ; ', $amapien->getAdminReseaux()->toArray());
                echo '.</div>';
            }

            if ($gest_amap) {
                echo '<div><strong>Correspondant Clic\'AMAP</strong> de ';
                echo $amapien->getAmap();
                echo '.</div>';
            }

            if ($ref_res) {
                echo '<div><strong>Correspondant réseau AMAP</strong> de '.$amapien->getAmapAsRefReseau().'.</div>';
            }

            if ($ref_res_sec) {
                echo '<div><strong>Second Correspondant réseau AMAP</strong> de '.$amapien->getAmapAsRefReseauSec().'.</div>';
            }

            if ($ref_pro) {
                echo '<div><strong>Référent produit</strong> auprès de :<ul><li>';
                echo implode('</li><li>', $amapien->getRefProdFermes()->toArray());
                echo '</li></ul></div>';
            }

            echo '</td>';
            echo '</tr>';
        }

        echo '<tr>';
        echo '<th>Email</th>';
        echo '<td><a href="mailto:'.$amapien->getEmail().'">'.$amapien->getEmail().'</a></td>';
        echo '</tr>';

        if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_USER_CHANGE_PASSWORD, $amapien->getId())) {
            echo '<tr>';
            echo '<th>Mot de passe</th>';
            echo '<td><a href="'.site_url('portail/password/'.$amapien->getId().'/a').'">Changer mon Mot de passe</a></td>';
            echo '</tr>';
        }

        if ($amapien->getNumTel1()) {
            echo '<tr>';
            echo '<th>Numéro de téléphone n°1</th>';
            echo '<td>'.$amapien->getNumTel1().'</td>';
            echo '</tr>';
        }

        if ($amapien->getNumTel2()) {
            echo '<tr>';
            echo '<th>Numéro de téléphone n°2</th>';
            echo '<td>'.$amapien->getNumTel2().'</td>';
            echo '</tr>';
        }

        if (null !== $amapien->getVille()) {
            echo '<tr>';
            echo '<th>Adresse</th>';
            echo '<td>';
            if ($amapien->getLibAdr1()) {
                echo ucfirst($amapien->getLibAdr1());
            } elseif ($amapien->getLibAdr2()) {
                echo ucfirst($amapien->getLibAdr2());
            }

            echo ', '.$amapien->getVille()->getCpString().' '.strtoupper($amapien->getVille()->getNom());
            echo '</td>';
            echo '</tr>';
        }

        $adhesions = $amapien->getAnneeAdhesionsOrdered();
        if (count($adhesions) > 0) {
            echo '<tr>';
            echo '<th>Année(s) d\'adhésion :</th>';

            echo '<td>';

            echo implode(' ; ', $adhesions);

            echo '</td>';
            echo '</tr>';
        }

        if (!$amapien->getFermeAttentes()->isEmpty()) {
            echo '<tr>';
            echo '<th>Liste d\'attente</th>';

            echo '<td>';

            echo implode(' ; ', $amapien->getFermeAttentes()->toArray());

            echo '</td>';
            echo '</tr>';
        }

        echo '<tr>';
        echo '<th>J\'autorise Clic\'AMAP à mettre à jour mes coordonnées sur les listes de diffusion email du réseau auquel j\'appartiens</th>';
        echo '<td>';
        echo true === $amapien->getNewsletter() ? 'Oui' : 'Non';
        echo '</td>';
        echo '</tr>';

        $adhesionInfo = $amapien->getAdhesionInfo();
        if (null !== $adhesionInfo->getNomReseau()) {
            echo '<tr>';
            echo '<th>Nom du réseau</th>';

            echo '<td>';

            echo $adhesionInfo->getNomReseau();

            echo '</td>';
            echo '</tr>';
        }
        if (null !== $adhesionInfo->getSite()) {
            echo '<tr>';
            echo '<th>Site internet</th>';

            echo '<td>';

            echo $adhesionInfo->getSite();

            echo '</td>';
            echo '</tr>';
        }
        if (null !== $adhesionInfo->getSiret()) {
            echo '<tr>';
            echo '<th>SIRET</th>';

            echo '<td>';

            echo $adhesionInfo->getSiret();

            echo '</td>';
            echo '</tr>';
        }
        if (null !== $adhesionInfo->getRna()) {
            echo '<tr>';
            echo '<th>RNA</th>';

            echo '<td>';

            echo $adhesionInfo->getRna();

            echo '</td>';
            echo '</tr>';
        }
        if (null !== $adhesionInfo->getVilleSignature()) {
            echo '<tr>';
            echo '<th>Ville mentionnée dans la signature (Fait à)</th>';

            echo '<td>';

            echo $adhesionInfo->getVilleSignature();

            echo '</td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>
