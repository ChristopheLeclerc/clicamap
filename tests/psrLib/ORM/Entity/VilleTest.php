<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ville;

/**
 * @internal
 * @coversNothing
 */
class VilleTest extends TestCase
{
    /**
     * @dataProvider villeStringProvider
     *
     * @param null|int $input
     */
    public function testVilleString(int $input, string $expected)
    {
        $departement = $this->prophesize(Departement::class);
        $ville = new Ville(0, $input, 'test', $departement->reveal());
        self::assertSame($expected, $ville->getCpString());
    }

    public function villeStringProvider()
    {
        return [
            [20247, '20247'],
            [1400, '01400'],
        ];
    }
}
