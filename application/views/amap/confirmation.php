<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
?>

<h3>Validation pour enregistrement</h3>

<h5><a href="<?= site_url('amap'); ?>">Gestion des AMAP</a> <i class="glyphicon glyphicon-menu-right"></i>
    Création d'une AMAP <i class="glyphicon glyphicon-menu-right"></i> Validation pour enregistrement</h5>

<!--<div class="alert alert-info">Après sauvegarde de l'AMAP, un mot de passe sera généré et transmis par email à l'adresse public de l'AMAP ainsi qu'au gestionnaire créé.</div>-->

<form method="POST" action="confirmation">

    <input type="hidden" name="step_conf" value="1"/>


    <div class="table-responsive">
        <table class="table">

            <tbody>

            <?php

            echo '<tr>';
            echo '<th>Nom :</th>';
            echo '<td>'.$amap->getNom().'</td>';
            echo '</tr>';

            echo '<tr>';

            echo '<th>Adresse de livraison :</th>';

            echo '<td>';

            /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $ll */
            $ll = $amap->getLivraisonLieux()->first();
            echo '<div class="alert alert-success"><strong>Lieu de livraison : </strong>';
            echo ucfirst($ll->getNom()).' - ';
            echo ucfirst($ll->getAdresse()).', '.$ll->getVille()->getCpString().' '.$ll->getVille()->getNom().' GPS Lat : '.$ll->getGpsLatitude().'. GPS Long : '.$ll->getGpsLongitude().'.';

            echo '<ul>';
            /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $llHoraire */
            $llHoraire = $ll->getLivraisonHoraires()->first();
            echo '<li><strong>Horaire de livraison : </strong>';
            echo ucfirst($llHoraire->getSaison()).', ';
            echo ucfirst($llHoraire->getJour()).' : '.$llHoraire->getHeureDebut().' - '.$llHoraire->getHeureDebut().'. ';
            echo '</li>';

            echo '</ul>';

            echo '</div>';

            echo '</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Adresse administrative -> Adresse :</th>';
            echo '<td>';

            echo $amap->getAdresseAdmin();

            echo sprintf(' %s', $amap->getAdresseAdminVille());

            echo '</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Email public :</th>';
            echo '<td>'.$amap->getEmail().'</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Site web ou blog :</th>';
            echo '<td>'.$amap->getUrl().'</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Date de création :</th>';
            echo '<td>'.$amap->getAnneeCreation().'</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Nombre d\'adhérents :</th>';
            echo '<td>'.$amap->getNbAdherents().'</td>';
            echo '</tr>';

            if (!$amap->getAnneeAdhesions()->isEmpty()) {
                echo '<tr>';
                echo '<th>Années d\'adhésion :</th>';
                echo '<td>';

                echo implode(' | ', $amap->getAnneeAdhesions()->toArray());

                echo '</td>';
                echo '</tr>';
            }

            if (!$amap->getTpPropose()->isEmpty()) {
                echo '<tr>';
                echo '<th>Produits proposés :</th>';
                echo '<td>';

                echo implode(' | ', $amap->getTpPropose()->toArray());

                echo '</td>';
                echo '</tr>';
            }

            if (!$amap->getTpRecherche()->isEmpty()) {
                echo '<tr>';
                echo '<th>Produits recherchés :</th>';
                echo '<td>';

                echo implode(' | ', $amap->getTpRecherche()->toArray());

                echo '</td>';
                echo '</tr>';
            }

            /** @var \PsrLib\ORM\Entity\Amapien $gest */
            $gest = $amap->getAmapiens()->first();
            echo '<tr>';
            echo '<th>Correspondant Clic\'AMAP -> Prénom / Nom :</th>';
            echo '<td>'.ucfirst($gest->getPrenom()).' '.strtoupper($gest->getNom()).'</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Correspondant Clic\'AMAP -> Email :</th>';
            echo '<td>'.$gest->getEmail().'</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Correspondant Clic\'AMAP -> Adresse :</th>';
            echo '<td>';

            echo $gest->getLibAdr1();
            echo ' '.$gest->getVille();

            echo '</td>';
            echo '</tr>';

            if (\PsrLib\ORM\Entity\Amap::ETAT_CREATION == $amap->getEtat()) {
                $etat = 'En création';
            }

            if (\PsrLib\ORM\Entity\Amap::ETAT_FONCIONNEMENT == $amap->getEtat()) {
                $etat = 'En fonctionnement';
            }

            if (\PsrLib\ORM\Entity\Amap::ETAT_ATTENTE == $amap->getEtat()) {
                $etat = 'En attente';
            }

            echo '<tr>';
            echo '<th>État :</th>';
            echo '<td>'.$etat.'</td>';
            echo '</tr>';

            echo '<tr>';
            echo '<th>Assurance :</th>';
            if ($amap->isPossedeAssurance()) {
                echo '<td>Oui</td>';
            } else {
                echo '<td>Non</td>';
            }
            echo '</tr>';

            echo '<tr>';
            echo '<th>Etudiante :</th>';
            if ($amap->getAmapEtudiante()) {
                echo '<td>Oui</td>';
            } else {
                echo '<td>Non</td>';
            }
            echo '</tr>';

            ?>


            </tbody>
        </table>
    </div>

    <div class="form-group">
        <input type="submit" value="Sauvegarder" class="pull-right btn btn-success" style="margin-left:5px;"/>

</form>

<a href="/amap/gestionnaire_creation" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
<a href="<?= site_url('amap'); ?>" class="pull-right btn btn-default">Annuler</a>
