<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\ORM\Entity\Files\Logo;

/**
 * AkReseau.
 *
 * @ORM\Table(name="ak_reseau", uniqueConstraints={@ORM\UniqueConstraint(name="res_nom", columns={"res_nom"})})
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\ReseauRepository")
 */
class Reseau implements EntityWithLogoInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="res_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="res_nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="res_reseau_url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var ArrayCollection<Amapien>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="adminReseaux")
     * @JoinTable(name="ak_reseau_administrateur",
     *      joinColumns={@JoinColumn(name="res_adm_fk_reseau_id", referencedColumnName="res_id")},
     *      inverseJoinColumns={@JoinColumn(name="res_adm_fk_amapien_id", referencedColumnName="a_id")}
     * )
     */
    private $admins;

    /**
     * @var ArrayCollection<Ville>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Ville")
     * @JoinTable(name="ak_reseau_villes",
     *      joinColumns={@JoinColumn(name="res_v_fk_reseau_id", referencedColumnName="res_id")},
     *      inverseJoinColumns={@JoinColumn(name="res_v_fk_ville_id", referencedColumnName="v_id")}
     * )
     */
    private $villes;

    /**
     * @var null|Logo
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Files\Logo", cascade={"ALL"}, orphanRemoval=true)
     */
    private $logo;

    public function __construct()
    {
        $this->admins = new ArrayCollection();
        $this->villes = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getNom();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Reseau
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Add admin.
     */
    public function addAdmin(Amapien $admin): Reseau
    {
        $this->admins[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdmin(Amapien $admin): bool
    {
        return $this->admins->removeElement($admin);
    }

    /**
     * Get admins.
     *
     * @return ArrayCollection<Amapien>
     */
    public function getAdmins(): ArrayCollection
    {
        return $this->admins;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Reseau
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Add ville.
     */
    public function addVille(Ville $ville): Reseau
    {
        $this->villes[] = $ville;

        return $this;
    }

    /**
     * Remove ville.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeVille(Ville $ville): bool
    {
        return $this->villes->removeElement($ville);
    }

    /**
     * Get villes.
     *
     * @return ArrayCollection<Ville>
     */
    public function getVilles(): ArrayCollection
    {
        return $this->villes;
    }

    public function getLogo(): ?Logo
    {
        return $this->logo;
    }

    public function setLogo(?Logo $logo): Reseau
    {
        $this->logo = $logo;

        return $this;
    }
}
