# language: fr
Fonctionnalité: Modifier une ferme

  @database
  Plan du Scénario: Tester les messages d'erreur avec le profil AMAPien référent
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapienref@test.amap-aura.org"
    Quand je suis sur "/ferme"
    Et que je suis "Modifier la ferme"
    Lorsque je remplis "ferme[nom]" avec "<ferme_nom>"
    Et que je remplis "ferme[siret]" avec "<ferme_siret>"
    Et que je remplis "ferme[delaiModifContrat]" avec "<delai>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | ferme_nom  | ferme_siret     | delai | message                                                    |
      | Ferme test | 11111111111111  |       | Cette valeur ne doit pas être vide.                        |
      | Ferme test |                 | 2     | Cette valeur ne doit pas être vide.                        |
      |            | 11111111111111  | 2     | Cette valeur ne doit pas être vide.                        |
      | Ferme test | 1               | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | Ferme test | 111111111111111 | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | Ferme test | 22222222222222  | 2     | Ce siret existe déjà dans la base de donnée.               |
      | Ferme test | 11111111111111  | 2     | La ferme a été modifiée avec succès !                      |

  @database
  Plan du Scénario: Tester les messages avec le profil paysan
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Quand je suis sur "/ferme/home_paysan"
    Et que je suis "Modifier la ferme"
    Lorsque je remplis "ferme[nom]" avec "<ferme_nom>"
    Et que je remplis "ferme[siret]" avec "<ferme_siret>"
    Et que je remplis "ferme[delaiModifContrat]" avec "<delai>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | ferme_nom         | ferme_siret     | delai | message                                                    |
      | Ferme test        | 11111111111111  |       | Cette valeur ne doit pas être vide.                        |
      | Ferme test        |                 | 2     | Cette valeur ne doit pas être vide.                        |
      |                   | 11111111111111  | 2     | Cette valeur ne doit pas être vide.                        |
      | Ferme test        | 1               | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | Ferme test        | 111111111111111 | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | Ferme test        | 22222222222222  | 2     | Ce siret existe déjà dans la base de donnée.               |
      | Ferme paysan test | 11111111111111  | 2     | La ferme a été modifiée avec succès !                      |

  @database
  Plan du Scénario: Tester les messages avec les profils Réseau, super admin et AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/ferme"
    Et que je remplis "search_ferme[region]" avec "84"
    Et que je suis "Modifier la ferme"
    Lorsque je remplis "ferme[nom]" avec "<ferme_nom>"
    Et que je remplis "ferme[siret]" avec "<ferme_siret>"
    Et que je remplis "ferme[delaiModifContrat]" avec "<delai>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | mail                          | ferme_nom  | ferme_siret     | delai | message                                                    |
      | admin@test.amap-aura.org      | Ferme test | 11111111111111  |       | Cette valeur ne doit pas être vide.                        |
      | admin@test.amap-aura.org      | Ferme test |                 | 2     | Cette valeur ne doit pas être vide.                        |
      | admin@test.amap-aura.org      |            | 11111111111111  | 2     | Cette valeur ne doit pas être vide.                        |
      | admin@test.amap-aura.org      | Ferme test | 1               | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | admin@test.amap-aura.org      | Ferme test | 111111111111111 | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | admin@test.amap-aura.org      | Ferme test | 22222222222222  | 2     | Ce siret existe déjà dans la base de donnée.               |
      | admin@test.amap-aura.org      | Ferme bis  | 11111111111111  | 2     | La ferme a été modifiée avec succès !                      |
      | admin.aura@test.amap-aura.org | Ferme test | 11111111111111  |       | Cette valeur ne doit pas être vide.                        |
      | admin.aura@test.amap-aura.org | Ferme test |                 | 2     | Cette valeur ne doit pas être vide.                        |
      | admin.aura@test.amap-aura.org |            | 11111111111111  | 2     | Cette valeur ne doit pas être vide.                        |
      | admin.aura@test.amap-aura.org | Ferme test | 1               | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | admin.aura@test.amap-aura.org | Ferme test | 111111111111111 | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | admin.aura@test.amap-aura.org | Ferme test | 22222222222222  | 2     | Ce siret existe déjà dans la base de donnée.               |
      | admin.aura@test.amap-aura.org | Ferme ter  | 11111111111111  | 2     | La ferme a été modifiée avec succès !                      |
      | amap@test.amap-aura.org       | Ferme test | 11111111111111  |       | Cette valeur ne doit pas être vide.                        |
      | amap@test.amap-aura.org       | Ferme test |                 | 2     | Cette valeur ne doit pas être vide.                        |
      | amap@test.amap-aura.org       |            | 11111111111111  | 2     | Cette valeur ne doit pas être vide.                        |
      | amap@test.amap-aura.org       | Ferme test | 1               | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | amap@test.amap-aura.org       | Ferme test | 111111111111111 | 2     | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | amap@test.amap-aura.org       | Ferme test | 22222222222222  | 2     | Ce siret existe déjà dans la base de donnée.               |
      | amap@test.amap-aura.org       | Ferme un   | 11111111111111  | 2     | La ferme a été modifiée avec succès !                      |

  @database
  Plan du Scénario: Modifier le regroupement à l'édition d'une ferme pour les super admin et les admins région
    Etant donné que je suis authentifié comme "amapien" avec le mail "<email>"
    Quand je suis sur "/ferme/informations_generales/1"
    Et que je remplis "ferme[regroupement]" avec "1"
    Et que je presse "Sauvegarder"
    Alors je vais sur "ferme_regroupement"
    Et je devrais voir "ferme" sur la ligne contenant "regroupement@test.amap-aura.org"

    Exemples:
      | email                         |
      | admin@test.amap-aura.org      |
      | admin.aura@test.amap-aura.org |

  Plan du Scénario: Masquer le champ regroupement pour les utilisateurs non admin
    Etant donné que je suis authentifié comme "<status>" avec le mail "<email>"
    Quand je suis sur "/ferme/informations_generales/1"
    Alors je ne devrais pas voir "Regroupement"

    Exemples:
      | status  | email                          |
      | amapien | admin.rhone@test.amap-aura.org |
      | amapien | amapienref@test.amap-aura.org  |
      | amapien | amap@test.amap-aura.org        |
      | paysan  | paysan@test.amap-aura.org      |
