<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use PsrLib\DTO\Time;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Embeddable
 */
class AmapDistributionDetail
{
    /**
     * @var ?Time
     * @ORM\Column(type="time_custom")
     * @Assert\NotNull
     * @Assert\Valid
     */
    private $heureDebut;

    /**
     * @var ?Time
     * @ORM\Column(type="time_custom")
     * @Assert\NotNull
     * @Assert\Valid
     */
    private $heureFin;

    /**
     * @var ?int
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     * @Assert\Range(min="1")
     */
    private $nbPersonnes;

    /**
     * @var ?string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private $tache;

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context): void
    {
        if (null === $this->getHeureDebut() || null === $this->getHeureFin()) {
            return;
        }

        if (
            $this->getHeureDebut()->getHour() > $this->getHeureFin()->getHour()
            || (
                $this->getHeureDebut()->getHour() === $this->getHeureFin()->getHour()
                && $this->getHeureDebut()->getMinute() >= $this->getHeureFin()->getMinute()
            )
        ) {
            $context
                ->buildViolation('L\'heure de début doit être avant l\'heure de fin')
                ->atPath('heureDebut')
                ->addViolation()
            ;
        }
    }

    public static function create(Time $heureDebut, Time $heureFin, int $nbPersonnes, string $tache): self
    {
        $detail = new self();
        $detail->setHeureDebut($heureDebut);
        $detail->setHeureFin($heureFin);
        $detail->setNbPersonnes($nbPersonnes);
        $detail->setTache($tache);

        return $detail;
    }

    public function getHeureDebut(): ?Time
    {
        return $this->heureDebut;
    }

    public function setHeureDebut(?Time $heureDebut): AmapDistributionDetail
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getHeureFin(): ?Time
    {
        return $this->heureFin;
    }

    public function setHeureFin(?Time $heureFin): AmapDistributionDetail
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    public function getNbPersonnes(): ?int
    {
        return $this->nbPersonnes;
    }

    public function setNbPersonnes(?int $nbPersonnes): AmapDistributionDetail
    {
        $this->nbPersonnes = $nbPersonnes;

        return $this;
    }

    public function getTache(): ?string
    {
        return $this->tache;
    }

    public function setTache(?string $tache): AmapDistributionDetail
    {
        $this->tache = $tache;

        return $this;
    }
}
