<?php

use DI\Annotation\Inject;

class Command extends AppController
{
    /**
     * @Inject
     *
     * @var Symfony\Component\Finder\Finder
     */
    private $finder;

    /**
     * Light twig linter command. Used on CI.
     */
    public function lintTwig()
    {
        if (PHP_SAPI !== 'cli') {
            $this->exit_error();
        }

        echo 'Linting twig templates'.PHP_EOL;

        $templates = $this
            ->finder
            ->files()
            ->in(\PsrLib\ProjectLocation::PROJECT_ROOT.'/templates')
            ->name('*.html.twig')
        ;
        echo $templates->count().' templates found'.PHP_EOL;

        foreach ($templates as $template) {
            echo 'Linting '.$template->getRealPath().' : ';

            try {
                $this->twig->load($template->getRelativePathname());
            } catch (\Exception $e) {
                echo PHP_EOL.$e;

                exit(EXIT_ERROR);
            }

            echo 'ok'.PHP_EOL;
        }
    }
}
