<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['pre_system'][] = [
    'function' => 'initSentry',
    'filename' => 'sentry.php',
    'filepath' => 'hooks',
];

$hook['pre_system'][] = [
    'class' => 'CleanInput',
    'function' => 'cleanAllInput',
    'filename' => 'CleanInput.php',
    'filepath' => 'hooks',
];

$hook['pre_controller'][] = [
    'class' => 'MaintenanceHook',
    'function' => 'redirectOnMaintenanceEnabled',
    'filename' => 'MaintenanceHook.php',
    'filepath' => 'hooks',
];
