<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use DI\Annotation\Inject;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Evenement extends AppController
{
    protected $data;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\EntityBuilder\EvenementBuilder
     */
    private $evenementBuilder;

    /**
     * Evenement constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['ev_id' => null,
            'ev_info_dep' => [],
            'ev_info_res' => [],
            'ev_info_amap' => [],
            'ev_info_ferme' => [],
            'ev_info_tp' => [],
            'evenement_all' => [],
            'dep' => null,
            'res' => null,
            'amap' => null,
            'ferme' => null,
            'type_prod' => [],
            'alerte_pj' => null,
            'alerte_img' => null, ];

        $this->load->helper('array');
        $this->load->helper('email');
        $this->load->helper('string');
        $this->load->helper('text');

        $this->load->library('email');
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    public function index()
    {
        $this->accueil();
    }

    public function accueil()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_LIST);

        $currentUser = $this->getUser();

        $this->data = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Evenement::class)
            ->getEvementForUser($currentUser)
        ;

        $this->loadViewWithTemplate(
            'evenement/display_all',
            $this->data
        );
    }

    /**
     * @param $ev_id
     */
    public function display($ev_id)
    {
        $evenement = $this->findOrExit(\PsrLib\ORM\Entity\Evenement::class, $ev_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_DISPLAY, $evenement);

        $this->loadViewWithTemplate(
            'evenement/display',
            [
                'evenement' => $evenement,
            ]
        );
    }

    /**
     * @param null $ev_id
     */
    public function add_1($ev_id = null)
    {
        /** @var \PsrLib\ORM\Repository\RegionRepository $regionRepo */
        $regionRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Region::class);
        /** @var \PsrLib\ORM\Repository\DepartementRepository $departementRepo */
        $departementRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Departement::class);
        /** @var \PsrLib\ORM\Repository\TypeProductionRepository $tpRepo */
        $tpRepo = $this->em->getRepository(\PsrLib\ORM\Entity\TypeProduction::class);
        /** @var \PsrLib\ORM\Repository\FermeRepository $fermeRepo */
        $fermeRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Ferme::class);
        /** @var \PsrLib\ORM\Repository\AmapRepository $amapRepo */
        $amapRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class);
        /** @var \PsrLib\ORM\Repository\ReseauRepository $reseauRepo */
        $reseauRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Reseau::class);
        $currentUser = $this->getUser();

        if ($ev_id) {
            $this->data['ev_id'] = $ev_id;
            /** @var \PsrLib\ORM\Entity\Evenement $evenement */
            $evenement = $this->findOrExit(\PsrLib\ORM\Entity\Evenement::class, $ev_id);
            $this->data['ev'] = $evenement;

            // Security
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_EDIT, $this->data['ev']);

            if (empty($_POST)) {
                /** @var \PsrLib\ORM\Entity\Departement[] $ev_info_dep */
                $ev_info_dep = $evenement->getRelDepartements()->toArray();
                if ($ev_info_dep) {
                    $this->data['ev_info_dep'] = $ev_info_dep;
                }

                /** @var \PsrLib\ORM\Entity\Reseau[] $ev_info_res */
                $ev_info_res = $evenement->getRelReseau()->toArray();
                if ($ev_info_res) {
                    $this->data['ev_info_res'] = $ev_info_res;
                }

                /** @var \PsrLib\ORM\Entity\Amap[] $ev_info_amap */
                $ev_info_amap = $evenement->getRelAmaps()->toArray();
                if ($ev_info_amap) {
                    $this->data['ev_info_amap'] = $ev_info_amap;
                }

                /** @var \PsrLib\ORM\Entity\Ferme[] $ev_info_ferme */
                $ev_info_ferme = $evenement->getRelFerme()->toArray();
                if ($ev_info_ferme) {
                    $this->data['ev_info_ferme'] = $ev_info_ferme;
                }

                /** @var \PsrLib\ORM\Entity\TypeProduction[] $ev_info_tp */
                $ev_info_tp = $evenement->getRelTypeProduction()->toArray();
                if ($ev_info_tp) {
                    $this->data['ev_info_tp'] = $ev_info_tp;
                }
            }
        } else {
            // Security check
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_CREATE);
        }

        $this->data['region_all'] = [];
        $this->data['departement_all'] = [];
        $this->data['dep_res'] = [];
        $this->data['amap_array'] = [];
        $this->data['ferme_array'] = [];

        // BOUTON DÉSÉLECTIONNER
        if ($this->input->post('reg_clear')) {
            $_POST['reg'] = [];
        }
        if ($this->input->post('dep_clear')) {
            $_POST['dep'] = [];
        }
        if ($this->input->post('res_clear')) {
            $_POST['res'] = [];
        }
        if ($this->input->post('amap_clear')) {
            $_POST['amap'] = [];
        }
        if ($this->input->post('ferme_clear')) {
            $_POST['ferme'] = [];
        }
        if ($this->input->post('tp_clear')) {
            $_POST['tp'] = [];
        }

        // SESSIONS + POST -------------------------------------------------------
        // SI SUPER ADMIN
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isSuperAdmin()) {
            $this->data['region_all'] = $regionRepo->findAllOrdered();
        }
        // SI RÉGION
        elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdminRegion()) {
            $this->data['departement_all'] = $departementRepo->findByMultipleRegion($currentUser->getAdminRegions()->toArray());
        }
        // SI DEPARTEMENT
        elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdminDepartment()) {
            $this->data['departement_all'] = $currentUser->getAdminDepartments();
        }
        // SI RÉSEAU
        elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdminReseaux()) {
            $this->data['dep_res'] = $currentUser->getAdminReseaux();
        }
        // SI AMAP
        elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
            $this->data['amap_ferme_all'] = $fermeRepo->getFromAmap($currentUser);
        }
        // SI RÉFÉRENT PRODUIT
        elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
            $this->data['amap_ferme_all'] = $currentUser->getRefProdFermes();
        }
        // -----------------------------------------------------------------------
        if (!empty($this->input->post('reg'))) {
            $departements = [];
            foreach ($this->input->post('reg') as $reg) {
                $departements = array_merge($departementRepo->findBy([
                    'region' => $this->em->getReference(\PsrLib\ORM\Entity\Region::class, $reg),
                ]), $departements);
            }
            $this->data['departement_all'] = $departements;
            $this->data['type_prod'] = $tpRepo->findAllNoParent();
        }

        if (!empty($this->input->post('dep')) || !empty($ev_info_dep)) {
            $this->data['type_prod'] = $tpRepo->findAllNoParent();
            // AFFICHAGE DES RÉSEAUX
            // Tableaux dans 1 tableau !
            $reseaux = [];
            // CRÉATION
            if (!empty($this->input->post('dep'))) {
                foreach ($this->input->post('dep') as $dep) {
                    $reseaux[] = $reseauRepo->findByDepartement($this->em->getReference(\PsrLib\ORM\Entity\Departement::class, $dep));
                }
            } // ÉDITION
            elseif (isset($ev_info_dep)) {
                foreach ($ev_info_dep as $dep) {
                    $reseaux[] = $reseauRepo->findByDepartement($dep);
                }
            }
            // CRÉATION & ÉDITION
            foreach ($reseaux as $reseau) {
                foreach ($reseau as $res) {
                    $this->data['dep_res'][] = $res;
                }
            }
        }

        // AFFICHAGE DES AMAP ----------------------------------------------------
        // SI RÉSEAU(X) CHOISI(S) ------------------------------------------------
        if (!empty($this->input->post('res')) || !empty($ev_info_res)) {
            $this->data['type_prod'] = $tpRepo->findAllNoParent();

            // Création
            if (!empty($this->input->post('res'))) {
                foreach ($this->input->post('res') as $res) {
                    $this->data['amap_array'][] = $amapRepo->findByReseau($this->em->getReference(\PsrLib\ORM\Entity\Reseau::class, $res));
                }
            } // Édition
            elseif (isset($ev_info_res)) {
                foreach ($ev_info_res as $res) {
                    $this->data['amap_array'][] = $amapRepo->findByReseau($res);
                }
            }
        } // SINON, AMAP DU/DES DÉPARTEMENT(S) -------------------------------------
        elseif (!empty($this->input->post('dep')) || !empty($ev_info_dep)) {
            if (!empty($this->input->post('dep'))) {
                foreach ($this->input->post('dep') as $dep) {
                    $this->data['amap_array'][] = $amapRepo->findByDepartement($this->em->getReference(\PsrLib\ORM\Entity\Departement::class, $dep));
                }
            }
            if (isset($ev_info_dep)) {
                foreach ($ev_info_dep as $dep) {
                    $this->data['amap_array'][] = $amapRepo->findByDepartement($dep);
                }
            }
        } // Sinon, AMAP DE LA/DES RÉGIONS -----------------------------------------
        elseif (!empty($this->input->post('reg'))) {
            foreach ($this->input->post('reg') as $reg) {
                $this->data['amap_array'][] = $amapRepo->findByRegion($this->em->getReference(\PsrLib\ORM\Entity\Region::class, $reg));
            }
        }

        // AFFIHAGES DES FERMES --------------------------------------------------
        // SI AMAP -> FERME DE l'AMAP --------------------------------------------
        // SI RÉSEAU(X) CHOISI(S) ------------------------------------------------
        if (!empty($this->input->post('res')) || !empty($ev_info_res)) {
            if (!empty($this->input->post('res'))) {
                foreach ($this->input->post('res') as $res) {
                    $this->data['ferme_array'][] = $fermeRepo->findByReseau($this->em->getReference(\PsrLib\ORM\Entity\Reseau::class, $res));
                }
            } elseif (isset($ev_info_res)) {
                foreach ($ev_info_res as $res) {
                    $this->data['ferme_array'][] = $fermeRepo->findByReseau($res);
                }
            }
        } // SINON, FERME(S) DU/DES DÉPARTEMENT(S) ---------------------------------
        elseif (!empty($this->input->post('dep')) || !empty($ev_info_dep)) {
            if (!empty($this->input->post('dep'))) {
                foreach ($this->input->post('dep') as $dep) {
                    $this->data['ferme_array'][] = $fermeRepo->findByDepartement($this->em->getReference(\PsrLib\ORM\Entity\Departement::class, $dep));
                }
            } elseif (isset($ev_info_dep)) {
                foreach ($ev_info_dep as $dep) {
                    $this->data['ferme_array'][] = $fermeRepo->findByDepartement($dep);
                }
            }
        } // Sinon, FERME(S) DE LA/DES RÉGIONS -------------------------------------
        elseif (!empty($this->input->post('reg'))) {
            if (!empty($this->input->post('reg'))) {
                foreach ($this->input->post('reg') as $reg) {
                    $this->data['ferme_array'][] = $fermeRepo->findByRegion($this->em->getReference(\PsrLib\ORM\Entity\Region::class, $reg));
                }
            }
        }

        if ($this->input->post('etape_validee')) {
            if ($ev_id) {
                // TRAITEMENT DES VARIABLES VIDES ------------------------------------
                if (empty($this->input->post('dep')) || $this->appfunction->array_vide($this->input->post('dep'))) {
                    $dep = null;
                } else {
                    $dep = $this->input->post('dep');
                }

                if (empty($this->input->post('res')) || $this->appfunction->array_vide($this->input->post('res'))) {
                    $res = null;
                } else {
                    $res = $this->input->post('res');
                }

                if (empty($this->input->post('amap')) || $this->appfunction->array_vide($this->input->post('amap'))) {
                    $amap = null;
                } else {
                    $amap = $this->input->post('amap');
                }

                if (empty($this->input->post('ferme')) || $this->appfunction->array_vide($this->input->post('ferme'))) {
                    $ferme = null;
                } else {
                    $ferme = $this->input->post('ferme');
                }

                if (empty($this->input->post('tp')) || $this->appfunction->array_vide($this->input->post('tp'))) {
                    $tp = null;
                } else {
                    $tp = $this->input->post('tp');
                }

                if (empty($this->input->post('ferme_id'))) {
                    $ferme_id = null;
                } else {
                    $ferme_id = $this->input->post('ferme_id');
                }

                $this->evenementBuilder->bindEvementRelations(
                    $evenement,
                    $this->input->post('amapiens'),
                    $this->input->post('reseau'),
                    $ferme_id,
                    $dep,
                    $res,
                    $amap,
                    $ferme,
                    $tp,
                    $this->getUser()
                );
                $this->em->flush();

                $_POST = [];
                $this->data['edit'] = 1;
                $this->accueil();
            } else {
                $this->loadViewWithTemplate('evenement/add_2', $this->data);
            }
        } else {
            // Sort
            sort($this->data['departement_all'], SORT_STRING);
            $this->loadViewWithTemplate('evenement/add_1', $this->data);
        }
    }

    public function add_2($ev_id = null)
    {
        if ($ev_id) {
            /** @var \PsrLib\ORM\Entity\Evenement $evenement */
            $evenement = $this->findOrExit(\PsrLib\ORM\Entity\Evenement::class, $ev_id);
            $this->data['ev_id'] = $ev_id;
            $this->data['ev'] = $evenement;

            // Security
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_EDIT, $evenement);
        } else {
            // Security
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_CREATE);
            $this->data['ev'] = null;
        }

        $this->form_validation->set_rules('evt_nom', '"Nom"', 'required');
        $this->form_validation->set_rules('date_deb', '"Date de début"', 'date_deb_format_check');
        $this->form_validation->set_rules('date_fin', '"Date de fin"', 'date_fin_format_check');
        $this->form_validation->set_rules('hor_deb', '"Heure de début"', 'hor_deb_check');
        $this->form_validation->set_rules('hor_fin', '"Heure de fin"', 'hor_fin_check');
        $this->form_validation->set_rules('evt_txt', '"Description"', 'required');

        if ($this->form_validation->run()) {
            // Édition
            if ($ev_id) {
                if (empty($this->input->post('evt_txt'))) {
                    $txt = null;
                } else {
                    $txt = $this->input->post('evt_txt');
                }

                if (empty($this->input->post('url'))) {
                    $url = null;
                } else {
                    $url = $this->input->post('url');
                }

                $evenement
                    ->setNom($this->input->post('evt_nom'))
                    ->setEvTxt($txt)
                    ->setUrl($url)
                    ->setHorDeb($this->input->post('hor_deb'))
                    ->setHorFin($this->input->post('hor_fin'))
                    ->setDateDeb('' === $this->input->post('date_deb') ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $this->input->post('date_deb')))
                    ->setDateFin('' === $this->input->post('date_fin') ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $this->input->post('date_fin')))
                ;
                $this->em->flush();

                $_POST = [];
                $this->accueil();
            } // Création
            else {
                if ($this->input->post('etape_infos')) {
                    $this->add_3();
                } else {
                    $page = $this->load->view('evenement/add_2', $this->data, true);
                    $this->load->view('template', ['page' => $page, 'header_evenements' => 1]);
                }
            }
        } else {
            $page = $this->load->view('evenement/add_2', $this->data, true);
            $this->load->view('template', ['page' => $page, 'header_evenements' => 1]);
        }
    }

    public function add_3()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_CREATE);

        $form = $this->formFactory->create(\PsrLib\Form\EvenementAttachmentType::class);
        $form->handleRequest($this->request);
        if ($this->input->post('etape_pj') && $form->isSubmitted() && $form->isValid()) {
            $img = null;
            if (null !== $form->get('img')->getData()) {
                $img = $this
                    ->uploader
                    ->uploadFile($form->get('img')->getData(), \PsrLib\ORM\Entity\Files\EvenementImg::class)
                ;
            }

            $pj = null;
            if (null !== $form->get('pj')->getData()) {
                $pj = $this
                    ->uploader
                    ->uploadFile($form->get('pj')->getData(), \PsrLib\ORM\Entity\Files\EvenementPj::class)
                ;
            }

            // TRAITEMENT DES VARIABLES VIDES ------------------------------------
            if (empty($this->input->post('dep')) || $this->appfunction->array_vide($this->input->post('dep'))) {
                $dep = null;
            } else {
                $dep = $this->input->post('dep');
            }

            if (empty($this->input->post('res')) || $this->appfunction->array_vide($this->input->post('res'))) {
                $res = null;
            } else {
                $res = $this->input->post('res');
            }

            if (empty($this->input->post('amap')) || $this->appfunction->array_vide($this->input->post('amap'))) {
                $amap = null;
            } else {
                $amap = $this->input->post('amap');
            }

            if (empty($this->input->post('ferme')) || $this->appfunction->array_vide($this->input->post('ferme'))) {
                $ferme = null;
            } else {
                $ferme = $this->input->post('ferme');
            }

            if (empty($this->input->post('tp')) || $this->appfunction->array_vide($this->input->post('tp'))) {
                $tp = null;
            } else {
                $tp = $this->input->post('tp');
            }

            if (empty($this->input->post('evt_txt'))) {
                $txt = null;
            } else {
                $txt = $this->input->post('evt_txt');
            }

            if (empty($this->input->post('url'))) {
                $url = null;
            } else {
                $url = $this->input->post('url');
            }

            if (empty($this->input->post('ferme_id'))) {
                $ferme_id = null;
            } else {
                $ferme_id = $this->input->post('ferme_id');
            }

            $evenement = $this->evenementBuilder->buildEvenement(
                $this->input->post('evt_nom'),
                $this->input->post('date_deb'),
                $this->input->post('date_fin'),
                $this->input->post('hor_deb'),
                $this->input->post('hor_fin'),
                $txt,
                $url,
                $img,
                $pj,
                $this->input->post('amapiens'),
                $this->input->post('reseau'),
                $ferme_id,
                $dep,
                $res,
                $amap,
                $ferme,
                $tp,
                $this->getUser()
            );
            $this->em->persist($evenement);
            $this->em->flush();
            $this->data['create'] = 1;
            $this->accueil();
        } else {
            $this->data['form'] = $form->createView();
            $page = $this->load->view('evenement/add_3', $this->data, true);
            $this->load->view('template', ['page' => $page, 'header_evenements' => 1]);
        }
    }

    /**
     * @param $ev_id
     */
    public function remove($ev_id)
    {
        // Security
        /** @var \PsrLib\ORM\Entity\Evenement $evenment */
        $evenment = $this->findOrExit(\PsrLib\ORM\Entity\Evenement::class, $ev_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_EDIT, $evenment);

        $this->em->remove($evenment);
        $this->em->flush();

        $this->data['remove'] = 1;
        $this->accueil();
    }
}
