<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme[] $fermes */

// LISTE DES FERMES
if (count($fermes) > 0) {
    ?>

<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-inverse">
        <tr>
            <th>Nom</th>
            <th>Paysan(s)</th>
            <th class="text-right">Outils</th>
        </tr>
        </thead>

        <tbody>

        <?php

        foreach ($fermes as $ferme) {
            echo '<tr>';

            echo '<th scope="row">';
            echo ucfirst($ferme->getNom());
            echo '</th>';

            // PAYSANS
            echo '<td>';

            $virgule = 0;
            /** @var \PsrLib\ORM\Entity\Paysan $paysan */
            foreach ($ferme->getPaysans() as $paysan) {
                if (0 !== $virgule) {
                    echo ' ; ';
                }

                echo '<a href="'.site_url('paysan/display/'.$paysan->getId()).'">'.$paysan->getPrenom().' '.strtoupper($paysan->getNom()).'</a>';

                ++$virgule;
            }

            echo '</td>';

            echo '<td class="text-right">';
            echo '<a title="Détails de la ferme" href="'.site_url('ferme/display/'.$ferme->getId()).'"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="Détails de la ferme"></i></a>&nbsp;';

            echo '<a title="Modifier la ferme" href="'.site_url('ferme/informations_generales/'.$ferme->getId()).'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier la ferme"></i></a>&nbsp;';

            echo '<a title="Paysans de la ferme" href="'.site_url('ferme/paysan_display/'.$ferme->getId()).'"><i class="glyphicon glyphicon-user" data-toggle="tooltip" title="Paysans de la ferme"></i></a>&nbsp;';

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REF_PRODUITS, $ferme)) {
                echo '<a title="Référents produits de la ferme" href="'.site_url('ferme/referent_display/'.$ferme->getId().'/0').'"><i class="glyphicon glyphicon-education" data-toggle="tooltip" title="Référents produits de la ferme"></i></a>&nbsp;';
            }

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_TYPE_PRODUCTION_MANAGE, $ferme)) {
                echo '<a title="Types de production" href="'.site_url('ferme/type_production_display/'.$ferme->getId()).'"><i class="glyphicon glyphicon-certificate" data-toggle="tooltip" title="Types de production"></i></a>&nbsp;';
            }

            echo '<a title="Produits proposés" href="'.site_url('produit/accueil/'.$ferme->getId()).'"><i class="glyphicon glyphicon-apple" data-toggle="tooltip" title="Produits proposés"></i></a>&nbsp;';

            echo '<a title="Calendrier des livraisons" href="'.site_url('ferme/calendrier/'.$ferme->getId()).'"><i class="glyphicon glyphicon-calendar" data-toggle="tooltip" title="Calendrier des livraisons"></i></a>&nbsp;';

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REMOVE)) {
                echo '<a title="Supprimer la ferme" href="#" data-toggle="modal" data-target="#modal_'.$ferme->getId().'"><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer la ferme"></i></a>&nbsp;';
            } ?>

        <!-- Modal -->
        <div class="modal fade text-left" id="modal_<?= $ferme->getId(); ?>" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer
                            "<?= ucfirst($ferme->getNom()); ?>" ?</h5>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger">En supprimant la ferme, vous supprimerez tous ses contrats ainsi
                            que ses produits et les droits "référents produits" qui lui sont liés. Les paysans seront
                            conservés dans la base de données.
                        </div>
                        <p class="text-right">
                            <a href="<?= site_url('ferme/remove_ferme/'.$ferme->getId()); ?>"
                               class="btn btn-danger btn-sm">OUI</a>
                            <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        </td>
        <tr>

        <?php
        } // endforeach fermes
        ?>

        </tbody>
    </table>
</div>
<?php
} // endif
?>
