#language: fr
Fonctionnalité: Liste des creneaux disponibles en tant qu'amapien

  Scénario: Affichage d'une liste vide
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Quand je suis "Mes distrib'AMAP"
    Alors je devrais voir "Cette page vous permet de gérer vos inscriptions distrib'AMAP. "
    Et je devrais voir "Votre AMAP n'a pas encore réalisé de créneaux de distribution"

  Scénario: Recherche selon une date
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Quand je suis "Mes distrib'AMAP"
    Et que je remplis le datepicker "search_distribution_amapien[period]" avec "01/01/2030 au 01/12/2030"
    Et je ne devrais pas voir "Votre AMAP n'a pas encore réalisé de créneaux de distribution"
    Et je devrais voir "01/01/2030"
    Et je devrais voir "test"
    Et je devrais voir "17:00"
    Et je devrais voir "18:00"
