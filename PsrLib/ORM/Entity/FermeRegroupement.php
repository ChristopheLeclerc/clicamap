<?php

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\FermeRegroupementRepository")
 * @ORM\Table(name="ak_ferme_regroupement")
 * @UniqueEntity(fields={"email"})
 */
class FermeRegroupement extends BaseUser implements UserWithFermes
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var null|string
     * @ORM\Column(length=255, nullable=true)
     * @Assert\Email()
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private $email;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     */
    private $adresseAdmin;

    /**
     * @var Ville | null
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ville")
     * @ORM\JoinColumn(referencedColumnName="v_id")
     * @Assert\NotNull
     */
    private $ville;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $emailInvalid = false;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private $nom;

    /**
     * @var ArrayCollection<Ferme>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\Ferme", mappedBy="regroupement")
     */
    private $fermes;

    /**
     * @var string |  null
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(min="14", max="14", exactMessage="Le SIRET doit faire exactement 14 caractères.")
     * @Assert\Luhn(message="Le SIRET est invalide.")
     */
    private $siret;

    public function __construct()
    {
        $this->fermes = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getNom();
    }

    public function estActif(): bool
    {
        return true;
    }

    public function getRegion(): ?Region
    {
        return $this->getVille()->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return $this->getVille()->getDepartement();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): FermeRegroupement
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): FermeRegroupement
    {
        $this->password = $password;

        return $this;
    }

    public function getAdresseAdmin(): ?string
    {
        return $this->adresseAdmin;
    }

    public function setAdresseAdmin(?string $adresseAdmin): FermeRegroupement
    {
        $this->adresseAdmin = $adresseAdmin;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): FermeRegroupement
    {
        $this->ville = $ville;

        return $this;
    }

    public function isEmailInvalid(): bool
    {
        return $this->emailInvalid;
    }

    public function setEmailInvalid(bool $emailInvalid): FermeRegroupement
    {
        $this->emailInvalid = $emailInvalid;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): FermeRegroupement
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Add ferme.
     *
     * @return FermeRegroupement
     */
    public function addFerme(Ferme $ferme)
    {
        $this->fermes[] = $ferme;
        $ferme->setRegroupement($this);

        return $this;
    }

    /**
     * Remove ferme.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeFerme(Ferme $ferme)
    {
        $ferme->setRegroupement(null);

        return $this->fermes->removeElement($ferme);
    }

    /**
     * Get fermes.
     */
    public function getFermes()
    {
        return $this->fermes;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): FermeRegroupement
    {
        $this->siret = $siret;

        return $this;
    }
}
