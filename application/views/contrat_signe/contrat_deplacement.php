<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @param mixed $cellules
 * @param mixed $produit
 * @param mixed $date
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
/** @var \PsrLib\ORM\Entity\Contrat $contrat */
/** @var \PsrLib\ORM\Entity\ContratCellule[] $cellules */
/** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $produits */
/** @var \PsrLib\ORM\Entity\ModeleContratDate[] $datesSrc */
/** @var \PsrLib\ORM\Entity\ModeleContratDate[] $datesDst */
/** @var \PsrLib\ORM\Entity\ModeleContratProduitExclure[] $exclusions */
/** @var string $errorMessage */
/** @var string $returnUrl */

/**
 * @param \PsrLib\ORM\Entity\ContratCellule[]     $cellules
 * @param \PsrLib\ORM\Entity\ModeleContratProduit $produit
 * @param \PsrLib\ORM\Entity\ModeleContratDate    $date
 */
function getCelluleQuantite($cellules, $produit, $date)
{
    foreach ($cellules as $cellule) {
        if ($cellule->getModeleContratDate()->getId() === $date->getId()
            && $cellule->getModeleContratProduit()->getId() === $produit->getId()) {
            return $cellule->getQuantite();
        }
    }

    return null;
}

/**
 * @param \PsrLib\ORM\Entity\ContratCellule[]       $cellules
 * @param \PsrLib\ORM\Entity\ModeleContratProduit[] $produits
 * @param \PsrLib\ORM\Entity\ModeleContratDate      $date
 */
function getCelluleArray($cellules, $produits, $date)
{
    $res = [];
    foreach ($produits as $produit) {
        $res[$produit->getId()] = getCelluleQuantite($cellules, $produit, $date);
    }

    return $res;
}

/**
 * @param \PsrLib\ORM\Entity\ModeleContratProduitExclure[] $excusions
 */
function normalize_exclusions($excusions)
{
    return array_map(function (PsrLib\ORM\Entity\ModeleContratProduitExclure $exclusion) {
        return [
            'date_id' => $exclusion->getModeleContratDate()->getId(),
            'produit_id' => $exclusion->getModeleContratProduit()->getId(),
        ];
    }, $excusions);
}

?>

<h3>Déplacer des dates de livraison</h3>

<form method="post">

    <div class="alert alert-info">
        Séléctionnez la date pour laquelle vous souhaitez déplacer la livraison. <br />
        Sélectionnez ensuite les dates sur lesquelles vous souhaitez déplacer les livraisons. <br />
        <em>Attention</em> : les quantités que vous selectionnez s'ajoutent aux quantités déjà prévues dans votre contrat. <br />
    </div>

    <?php if (null !== $errorMessage): ?>
        <div class="alert alert-danger">
            <p><?=$errorMessage; ?></p>
        </div>
    <?php endif; ?>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Sélectionnez la date que vous souhaitez déplacer</label>
                    <select name="date_deplacement_src" class="form-control">
                        <option value=""></option>
                        <?php foreach ($datesSrc as $date) :?>
                            <option
                                value="<?=$date->getId(); ?>"
                                data-cellules='<?=json_encode(getCelluleArray($cellules, $produits, $date)); ?>'
                            >
                                <?=date_format_french($date->getDateLivraison()); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <?php foreach ($produits as $produit) : ?>
                                <th class="text-center">
                                    <?=$produit->getNom(); ?>
                                    <br />
                                    <?=$produit->getConditionnement(); ?>
                                    <br />
                                    <?=$produit->getPrix().' €'; ?>
                                </th>
                            <?php endforeach; ?>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                Livraisons prévues au contrat
                            </td>
                            <?php foreach ($produits as $produit) : ?>
                                <td>
                                    <input
                                            type="number"
                                            class="form-control prod_prevu"
                                            min="0"
                                            disabled
                                            value=""
                                            data-produit-id="<?=$produit->getId(); ?>"
                                    />
                                </td>
                            <?php endforeach; ?>
                            <td></td>
                        </tr>
                        <tr style="display: none" id="row-template">
                            <td>
                                <select name="" class="form-control date-deplacement-dst">
                                    <option value=""></option>
                                    <?php foreach ($datesDst as $date) :?>
                                        <option
                                                value="<?=$date->getId(); ?>"
                                        >
                                            <?=date_format_french($date->getDateLivraison()); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <?php foreach ($produits as $produit) : ?>
                                <td>
                                    <input
                                        type="number"
                                        class="form-control input-prod"
                                        min="0"
                                        data-produit-id="<?=$produit->getId(); ?>"
                                        step="<?php if ($mc->getAmapienPermissionDeplacementLivraison() && 'entier' === $mc->getAmapienPermissionDeplacementLivraison()) {
    echo '1';
} else {
    echo '0.5';
} ?>"
                                    />
                                </td>
                            <?php endforeach; ?>
                            <td><button class="btn btn-danger btn-remove"><span class="glyphicon glyphicon-minus"></span></button> </td>
                        </tr>
                        <tr id="row-sum">
                            <td>
                                Total : <br />
                                Doit correspondre au nombre de livraisons prévues au contrat
                            </td>
                            <?php foreach ($produits as $produit) : ?>
                                <td>
                                    <input
                                        type="number"
                                        class="form-control input-sum"
                                        min="0"
                                        data-produit-id="<?=$produit->getId(); ?>"
                                        disabled
                                    />
                                </td>
                            <?php endforeach; ?>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

            </div>

        </div>

        <div class="row">
            <div class="col-sm-11">
                <p class="pull-right" style="margin-top: 0.5em">Ajouter une nouvelle date de report.</p>
            </div>
            <div class="col-sm-1">
                <button class="btn btn-success" id="btn-plus"><span class="glyphicon glyphicon-plus"></span> </button>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <a class="btn btn-default" href="<?=site_url($returnUrl); ?>">Retour</a>
                <button class="btn btn-success" type="submit" id="btn-send" disabled>Envoyer</button>
            </div>
        </div>

</form>

<script>
    $(document).ready(function () {

      $('select[name="date_deplacement_src"]').on('change', function () {
        var cellules = $(this).find(':selected').data('cellules');
        $('.prod_prevu')
          .each(function () {
            var produidId = $(this).data('produit-id');
            var count = cellules[produidId] === null ? 0 : cellules[produidId];
            $(this).val(count);
          });
        updateSendButton();
      })
    });

    var rowCount = 0;
    $('#btn-plus').on('click', function(e) {
        e.preventDefault();

        var newRow = $('#row-template').clone();
        newRow.show();
        var select = newRow.find('select');
        select.attr('name', 'date_deplacement_dst[' + rowCount + ']');
        newRow
          .find('input')
          .each(function () {
            var produitId = $(this).data('produit-id');
            $(this).attr('name', 'produit_deplacement[' + rowCount + '][' + produitId + ']');
          });

        $(newRow).insertBefore('#row-sum');

        rowCount++;
    });

    function updateSum() {
      $('.input-sum')
        .each(function () {
          var productId = $(this).data('produit-id');

          var sum = 0;
          $('.input-prod[data-produit-id=' + productId + ']')
            .each(function() {
              var val =  parseFloat($(this).val());
              if(!isNaN(val)) {
                sum += val;
              }
            });
          $(this).val(sum);
        })
    }


    $('table').on('change', '.input-prod', function () {
      updateSum();
      updateSendButton();
    });

    $('table').on('click', '.btn-remove', function (e) {
      e.preventDefault();

      $(this).parents('tr').remove();
      updateSum();
      updateSendButton();
    });
    $('table').on('change', '.date-deplacement-dst', function () {
      updateSum();
      updateSendButton();
    });

    // Manage excluded dates
    var exclusions = JSON.parse("<?=addslashes(json_encode(normalize_exclusions($exclusions))); ?>");
    $('table').on('change', '.date-deplacement-dst', function () {
      var dateId = parseInt($(this).find(':selected').val());

      $(this)
        .parents('tr')
        .find('input')
        .each(function() {
          $(this).prop('disabled', false);

          var productId = parseInt($(this).data('produit-id'));

          var toDisable = false;
          exclusions
            .forEach(function (exclusion) {
              if(parseInt(exclusion.date_id) === dateId
                && parseInt(exclusion.produit_id) === productId) {
                toDisable = true;
              }
          });

          if(toDisable === true) {
            $(this).prop('disabled', true);
            $(this).val(null);
          }
        });

      updateSum();
    });

    function isSendButtonEnabled() {
      // A date must be selected
      if($('select[name="date_deplacement_src"]').val() === "") {
        return false;
      }


      var enabled = true;
      $('.prod_prevu')
        .each(function () {
          var produitId = $(this).data('produit-id');

          // All sums must be equal to selected
          if($('.input-sum[data-produit-id=' + produitId + ']').val() !== $(this).val()) {
            enabled = false;
          }

          // All lines must be equals
          var prod = $('.input-prod[data-produit-id=' + produitId + ']').map(function(idx, elem) {
            return $(elem).val();
          }).get();
          prod.shift(); // Remove template row
          if(!prod.every(function (p) {return p === prod[0]})) {
            enabled = false;
          }

          // A date must be selected
          var dstDates = $('.date-deplacement-dst option:selected').map(function(idx, elem) {
            return $(elem).val();
          }).get();
          dstDates.shift(); // Remove template row
          if(!dstDates.every(function (d) {return d !== ''})) {
            enabled = false;
          }

        });

      return enabled;
    }
    function updateSendButton() {
      if(isSendButtonEnabled()) {
        $('#btn-send').attr('disabled', false);
      } else {
        $('#btn-send').attr('disabled', true);
      }

    }
</script>
