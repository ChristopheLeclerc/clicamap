<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapAbsence;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class AmapAbsenceTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var Amap
     */
    private $amap;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
        $this->amap = $this->prophesize(Amap::class)->reveal();
    }

    public function testDebutBeforeFin()
    {
        $absence = new AmapAbsence($this->amap);
        $absence->setAbsenceDebut(new Carbon('2000-01-01'));
        $absence->setAbsenceFin(new Carbon('2000-01-10'));

        $violations = $this->validator->validateProperty($absence, 'absenceFin');
        self::assertEmpty($violations);
    }

    public function testDebutEqualFin()
    {
        $absence = new AmapAbsence($this->amap);
        $absence->setAbsenceDebut(new Carbon('2000-01-01'));
        $absence->setAbsenceFin(new Carbon('2000-01-01'));

        $violations = $this->validator->validateProperty($absence, 'absenceFin');
        self::assertCount(1, $violations);
        self::assertSame('La date de début doit être avant la date de fin', $violations[0]->getMessage());
    }

    public function testDebutAfterFin()
    {
        $absence = new AmapAbsence($this->amap);
        $absence->setAbsenceDebut(new Carbon('2000-01-10'));
        $absence->setAbsenceFin(new Carbon('2000-01-01'));

        $violations = $this->validator->validateProperty($absence, 'absenceFin');
        self::assertCount(1, $violations);
        self::assertSame('La date de début doit être avant la date de fin', $violations[0]->getMessage());
    }
}
