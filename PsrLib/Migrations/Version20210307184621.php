<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184621 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $tpps = $this
            ->connection
            ->executeQuery('SELECT tp_id, tp_categorie_lib_lien FROM ak_type_production')
            ->fetchAll()
        ;

        $this->addSql('DELETE FROM ak_ferme_produit WHERE ak_ferme_produit.f_pro_fk_ferme_id NOT IN (SELECT f_id FROM ak_ferme);');
        $this->addSql('ALTER TABLE ak_ferme_produit ADD type_production_id INT DEFAULT NULL, CHANGE f_pro_fk_ferme_id f_pro_fk_ferme_id BIGINT(0) NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_produit ADD CONSTRAINT FK_244CA784708B3AB7 FOREIGN KEY (f_pro_fk_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_ferme_produit ADD CONSTRAINT FK_244CA7849C2BA34A FOREIGN KEY (type_production_id) REFERENCES ak_type_production (tp_id)');

        foreach ($tpps as $tpp) {
            $this->addSql('UPDATE ak_ferme_produit SET type_production_id=:id WHERE f_pro_fk_type_production_lib_lien=:lien', [
                'id' => $tpp['tp_id'],
                'lien' => $tpp['tp_categorie_lib_lien'],
            ]);
        }

        $this->addSql('ALTER TABLE ak_ferme_produit DROP f_pro_fk_type_production_lib_lien');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
