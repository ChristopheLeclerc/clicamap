<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Formulaire mot de passe perdu */
if (isset($print_lost_password_form) && true === $print_lost_password_form) {
    ?>

    <div class="modal" tabindex="-1" role="dialog" id="lostpassword-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <?= form_open('', ['class' => 'form-horizontal']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mot de passe oublié</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="statut" class="col-sm-2 control-label">Vous êtes</label>
                        <div class="col-sm-10  text-left">
                            <div class="radio">
                                <label>
                                    <?= form_radio('lostpassword_statut', 'amapien'); ?> Un(e) administrateur(trice)
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <?= form_radio('lostpassword_statut', 'amapien', 'amapien' === set_value('lostpassword_statut')); ?> Un(e) amapien(ne)
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <?= form_radio('lostpassword_statut', 'paysan', 'paysan' === set_value('lostpassword_statut')); ?> Un(e) paysan(ne)
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <?= form_radio('lostpassword_statut', 'regroupement', 'regroupement' === set_value('lostpassword_statut')); ?> Un regroupement
                                </label>
                            </div>
                            <?= form_error('lostpassword_statut'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lostpassword_email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <?= form_input('lostpassword_email', set_value('lostpassword_email'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('lostpassword_email'); ?>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                </div>
                <?= form_close(); ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script>
      $('#lostpassword-modal')
        .modal({
          show: true
        })
        // Redirect to connexion page on close
        .on('hidden.bs.modal', function () {
          window.location.href = '/portail/connexion';
        })
    </script>
    <?php
}
?>
