<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Repository\AmapLivraisonLieuRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\Assert\Assert;

class ModelContratForm1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Amap $amap */
        $amap = $options['amap'];
        Assert::isInstanceOf($amap, Amap::class);

        $builder
            ->add('nom', TextType::class, [
                'attr' => ['placeholder' => 'Penser à donner un nom unique en indiquant par exemple la période du contrat, le numéro de contrat, la saison, etc...'],
            ])
            ->add('filiere', TextareaType::class, [
                'attr' => ['placeholder' => 'Exemples : Légumes, Fruits, Produits laitiers (vache), ...'],
            ])
            ->add('specificite', TextareaType::class, [
                'attr' => ['placeholder' => 'Vous pouvez inscrire des informations qui seront visibles par l\'amapien.ne lors de la saisie de son contrat.
Exemples : rapporter les boites d’œufs, notre paysan est en démarche vers une agriculture biologique, ...'],
            ])
            ->add('livraisonLieu', EntityType::class, [
                'class' => AmapLivraisonLieu::class,
                'query_builder' => function (AmapLivraisonLieuRepository $amapLivraisonLieuRepository) use ($options) {
                    return $amapLivraisonLieuRepository->qbByAmap($options['amap']);
                },
                'placeholder' => 'Sélectionner',
                'required' => false,
                'data' => $options['defaultLl'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'defaultLl' => null,
            'data_class' => ModeleContrat::class,
            'validation_groups' => ['form1'],
        ]);
    }
}
