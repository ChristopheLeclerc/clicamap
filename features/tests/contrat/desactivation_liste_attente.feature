# language: fr
Fonctionnalité: Comme amapien, je ne doit pas pouvoir souscrire un contrat si je suis sur liste d'attente

  Scénario: Souscription possible si pas en liste d'attente
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Quand je vais sur "/contrat_signe/contrat_own_new/"
    Alors je devrais voir "Souscrire"
    Et je ne devrais pas voir "Liste d'attente"

  @database
  Scénario: Souscription impossible en cas de liste d'attente
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis sur "/amapien"
    Et que je suis "Liste d'attente" sur la ligne contenant "amapien@test.amap-aura.org"
    Et que je remplis "amapien_liste_attente[fermeAttentes][]" avec "1"
    Et que je presse "Sauvegarder"
    Quand je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Et que je vais sur "/contrat_signe/contrat_own_new/"
    Et je ne devrais pas voir "Souscrire"
    Et je devrais voir "Liste d'attente"

  Scénario: Forcer la souscription possible si pas en liste d'attente
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je vais sur "/contrat_signe/contrat_new_force/1"
    Alors le selecteur "amapien" devrait contenir l'option "amapien@test.amap-aura.org"

  @database
  Scénario: Forcer souscription impossible si désactité
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis sur "/amapien"
    Et que je suis "Liste d'attente" sur la ligne contenant "amapien@test.amap-aura.org"
    Et que je remplis "amapien_liste_attente[fermeAttentes][]" avec "1"
    Et que je presse "Sauvegarder"
    Quand  je vais sur "/contrat_signe/contrat_new_force/1"
    Alors le selecteur "amapien" ne devrait pas contenir l'option "amapien@test.amap-aura.org"
