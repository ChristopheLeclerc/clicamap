# language: fr
Fonctionnalité: Ajouter une ferme

  @database
  Plan du Scénario: Tester les messages d'erreur, tous les profils
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/ferme"
    Et que je suis "Créer une nouvelle ferme"
    Alors je devrais être sur "/ferme/informations_generales_creation"
    Lorsque je remplis "ferme[nom]" avec "<nom_ferme>"
    Et que je remplis "ferme[siret]" avec "<siret_ferme>"
    Et que je remplis "ferme[delaiModifContrat]" avec "<delai_modif_contrat>"
    Et que je remplis "f_autocomplete" avec "<adresse_ferme>"
    Et que je clique sur l'élément "<list_item>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | mail                          | nom_ferme | siret_ferme    | delai_modif_contrat | adresse_ferme             | list_item             | message                                                    |
      | admin@test.amap-aura.org      |           | 12345678901234 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | admin@test.amap-aura.org      | Ferme     |                | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | admin@test.amap-aura.org      | Ferme     | 1              | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | admin@test.amap-aura.org      | Ferme     | 12345678901234 |                     | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | admin@test.amap-aura.org      | Ferme     | 12345678901234 | 2                   |                           |                       | Cette valeur ne doit pas être vide.                        |
      | admin@test.amap-aura.org      | Ferme     | 11111111111111 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Ce SIRET existe déjà dans la base de donnée                |
      | admin.aura@test.amap-aura.org | Ferme     |                | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | admin.aura@test.amap-aura.org | Ferme     | 1              | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | admin.aura@test.amap-aura.org | Ferme     | 12345678901234 |                     | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | admin.aura@test.amap-aura.org | Ferme     | 12345678901234 | 2                   |                           |                       | Cette valeur ne doit pas être vide.                        |
      | admin.aura@test.amap-aura.org | Ferme     | 11111111111111 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Ce SIRET existe déjà dans la base de donnée                |
      | amap@test.amap-aura.org       |           | 12345678901234 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | amap@test.amap-aura.org       | Ferme     |                | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | amap@test.amap-aura.org       | Ferme     | 1              | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Le champ \"SIRET\" doit contenir exactement 14 caractères. |
      | amap@test.amap-aura.org       | Ferme     | 12345678901234 |                     | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Cette valeur ne doit pas être vide.                        |
      | amap@test.amap-aura.org       | Ferme     | 12345678901234 | 2                   |                           |                       | Cette valeur ne doit pas être vide.                        |
      | amap@test.amap-aura.org       | Ferme     | 11111111111111 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Ce SIRET existe déjà dans la base de donnée                |

  @database
  Plan du Scénario: Intégration réussie de la ferme, tous profils amapiens
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Et que je suis sur "/ferme"
    Et que je suis "Créer une nouvelle ferme"
    Et que je devrais être sur "/ferme/informations_generales_creation"
    Lorsque je remplis "ferme[nom]" avec "<nom_ferme>"
    Et que je remplis "ferme[siret]" avec "<siret_ferme>"
    Et que je remplis "ferme[delaiModifContrat]" avec "<delai_modif_contrat>"
    Et que je remplis "f_autocomplete" avec "<adresse_ferme>"
    Et que je clique sur l'élément "<list_item>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"
    Et que je devrais être sur "/paysan/informations_generales_creation"
    Exemples:
      | mail                          | nom_ferme              | siret_ferme    | delai_modif_contrat | adresse_ferme             | list_item             | message                                                                                        |
      | admin@test.amap-aura.org      | FermeTest Super-Admin  | 12345678901234 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans. |
      | admin.aura@test.amap-aura.org | FermeTest Admin Réseau | 12345678901235 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans. |
      | amap@test.amap-aura.org       | FermeTest AMAP         | 12345678901235 | 2                   | 58 rue Raulin, 69007 Lyon | ul.ui-autocomplete li | Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans. |

  @database
  Scénario: Intégration réussie de la ferme, comme regroupement
    Etant donné que je suis authentifié comme "regroupement" avec le mail "regroupement@test.amap-aura.org"
    Et que je suis sur "/ferme"
    Et que je suis "Créer une nouvelle ferme"
    Et que je devrais être sur "/ferme/informations_generales_creation"
    Lorsque je remplis "ferme[nom]" avec "FermeTest"
    Et que je remplis "ferme[siret]" avec ""
    Et que je remplis "ferme[delaiModifContrat]" avec "2"
    Et que je remplis "f_autocomplete" avec "58 rue Raulin, 69007 Lyon"
    Et que je clique sur l'élément "ul.ui-autocomplete li"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans."
    Et que je devrais être sur "/paysan/informations_generales_creation"
    Et je suis sur "/ferme"
    Alors je devrais voir "FermeTest"

  @database
  Plan du Scénario: Ajouter le regroupement à l'édition d'une ferme pour les super admin et les admins région
    Etant donné que je suis authentifié comme "amapien" avec le mail "<email>"
    Quand je suis sur "/ferme"
    Et que je suis "Créer une nouvelle ferme"
    Alors je devrais voir "Regroupement"

    Exemples:
      | email                         |
      | admin@test.amap-aura.org      |
      | admin.aura@test.amap-aura.org |

  Plan du Scénario: Masquer le champ regroupement pour les utilisateurs non admin
    Etant donné que je suis authentifié comme "<status>" avec le mail "<email>"
    Quand je suis sur "/ferme/informations_generales/1"
    Alors je ne devrais pas voir "Regroupement"

    Exemples:
      | status  | email                          |
      | amapien | admin.rhone@test.amap-aura.org |
      | amapien | amapienref@test.amap-aura.org  |
      | amapien | amap@test.amap-aura.org        |
      | paysan  | paysan@test.amap-aura.org      |

  @database
  Scénario: Retour sur l'ajout d'une nouvelle ferme après j'ajout d'une première
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/ferme/informations_generales_creation"
    Et que je remplis "ferme[nom]" avec " FermeTest"
    Et que je remplis "ferme[siret]" avec "12345678901234"
    Et que je remplis "ferme[delaiModifContrat]" avec "2"
    Et que je remplis "f_autocomplete" avec "58 rue Raulin, 69007 Lyon>"
    Et que je clique sur l'élément "ul.ui-autocomplete li"
    Et que je presse "Sauvegarder"
    Lorsque je suis sur "/ferme/informations_generales_creation"
    Alors le champ "ferme[nom]" devrait contenir ""
