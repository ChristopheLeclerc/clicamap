<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amapien[] $adminRegions */
/** @var \PsrLib\ORM\Entity\Amapien[] $adminDepartements */
/** @var \Symfony\Component\Form\FormView $form */
?>

<script type="text/javascript">

  /*ALERTES*/
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 3000);

  });

</script>

<h3>Contacter un administrateur</h3>

<?php if (count($adminRegions) > 0 || count($adminDepartements) > 0): ?>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-inverse">
            <tr>
                <th>Statut</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Téléphone 1</th>
                <th>Téléphone 2</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <?php

            echo '<tbody>';

            if (count($adminRegions) > 0) {
                foreach ($adminRegions as $amapien) {
                    echo '<tr>';

                    echo '<td>Régional</td>';

                    echo '<td>';
                    echo strtoupper($amapien->getNom());
                    echo '</td>';

                    echo '<td>';
                    echo ucfirst($amapien->getPrenom());
                    echo '</td>';

                    echo '<td>';
                    if ($amapien->getNumTel1()) {
                        echo ucfirst($amapien->getNumTel1());
                    } else {
                        echo 'Aucun';
                    }
                    echo '</td>';

                    echo '<td>';
                    if ($amapien->getNumTel2()) {
                        echo ucfirst($amapien->getNumTel2());
                    } else {
                        echo 'Aucun';
                    }
                    echo '</td>';

                    echo '<td class="text-right">';

                    echo '<a href="#" data-toggle="modal" data-target="#modal_envoi_'.$amapien->getId().'" ><button type=button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Email</button></a>'; ?>

                    <div class="modal fade text-left" id="modal_envoi_<?= $amapien->getId(); ?>" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Envoyer un email
                                        à <?= strtoupper($amapien->getNom()).' '.ucfirst($amapien->getPrenom()); ?></h5>
                                </div>
                                <div class="modal-body">

                                    <form method="POST" action="">

                                        <?= render_form_hidden_widget($form['targetId'], $amapien->getId()); ?>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">

                                                    <span class="input-group-btn">
                                                    <button class="btn btn-primary" type="button">Objet</button>
                                                    </span>

                                                    <div class="form-group">
                                                        <?= render_form_text_widget($form['title']); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    <?= render_form_textarea_widget($form['content']); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="submit" value="Envoyer"
                                                           class="pull-right btn btn-success"/>
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                    </td>
                    </tr>

                    <?php
                }
            }

            if (count($adminDepartements) > 0) {
                foreach ($adminDepartements as $amapien) {
                    echo '<tr>';

                    echo '<td>Départemental</td>';

                    echo '<td>';
                    echo strtoupper($amapien->getNom());
                    echo '</td>';

                    echo '<td>';
                    echo ucfirst($amapien->getPrenom());
                    echo '</td>';

                    echo '<td>';
                    if ($amapien->getNumTel1()) {
                        echo ucfirst($amapien->getNumTel1());
                    } else {
                        echo 'Aucun';
                    }
                    echo '</td>';

                    echo '<td>';
                    if ($amapien->getNumTel2()) {
                        echo ucfirst($amapien->getNumTel2());
                    } else {
                        echo 'Aucun';
                    }
                    echo '</td>';

                    echo '<td class="text-right">';

                    echo '<a href="#" data-toggle="modal" data-target="#modal_envoi_'.$amapien->getId().'" ><button type=button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Email</button></a>'; ?>

                    <div class="modal fade text-left" id="modal_envoi_<?= $amapien->getId(); ?>" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Envoyer un email
                                        à <?= strtoupper($amapien->getNom()).' '.ucfirst($amapien->getPrenom()); ?></h5>
                                </div>
                                <div class="modal-body">

                                    <form method="POST" action="">

                                        <?= render_form_hidden_widget($form['targetId']); ?>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">

                                                    <span class="input-group-btn">
                                                    <button class="btn btn-primary" type="button">Objet</button>
                                                    </span>

                                                    <div class="form-group">
                                                        <?= render_form_text_widget($form['title']); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <?= render_form_textarea_widget($form['content']); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="submit" value="Envoyer"
                                                               class="pull-right btn btn-success"/>
                                                    </div>
                                                </div>
                                            </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                    </td>
                    </tr>

                    <?php
                }
            } ?>

            </tbody>
        </table>
    </div>

<?php else: ?>
    <div class="alert alert-warning">Aucun contact.</div>
<?php endif; ?>
