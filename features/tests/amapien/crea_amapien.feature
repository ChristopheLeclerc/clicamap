# language: fr
Fonctionnalité: Création d'un AMAPien depuis différents profils

  @database
  Scénario: Créer un amapien depuis une amap sans amapien
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/amapien"
    Et que je suis "supprimer amapien"
    Et que je devrais voir "L'amapien a été supprimé(e) avec succès"
    Et que je suis "supprimer amapien"
    Et que je devrais voir "L'amapien a été supprimé(e) avec succès"
    Alors je ne devrais pas voir "amapien@test.amap-aura.org"
    Et je ne devrais pas voir "amapienref@test.amap-aura.org"
    Et je suis "Créer un nouvel amapien"
    Et je devrais être sur "/amapien/informations_generales_creation/1"

  @database
  Plan du Scénario: Tester les messages d'erreurs avec les profils réseau et super-admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/amapien"
    Et que je remplis "search_amapien[region]" avec "84"
    Et que je remplis "search_amapien[amap]" avec "1"
    Et que je suis "Créer un nouvel amapien"
    Et que je remplis "amapien_creation[prenom]" avec "<contenu_prenom>"
    Et que je remplis "amapien_creation[nom]" avec "<contenu_nom>"
    Et que je remplis "amapien_creation[email]" avec "<contenu_mail>"
    Et que je remplis "amapien_creation[newsletter]" avec "1"
    Et que je remplis "amapien_creation[needWaiting]" avec "0"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | mail                          | contenu_prenom | contenu_nom | contenu_mail               | message                                                                  |
      | admin@test.amap-aura.org      |                | Martin      | test@test.amap-aura.org    | Cette valeur ne doit pas être vide.                                      |
      | admin@test.amap-aura.org      | Eugène         |             | test@test.amap-aura.org    | Cette valeur ne doit pas être vide.                                      |
      | admin@test.amap-aura.org      | Eugène         | Martin      |                            | Cette valeur ne doit pas être vide.                                      |
      | admin@test.amap-aura.org      | Eugène         | Martin      | amapien@test.amap-aura.org | Cet email existe déjà dans la base de donnée.                            |
      | admin.aura@test.amap-aura.org |                | Martin      | test@amap-aura.org         | Cette valeur ne doit pas être vide.                                      |
      | admin.aura@test.amap-aura.org | Eugène         |             | test@amap-aura.org         | Cette valeur ne doit pas être vide.                                      |
      | admin.aura@test.amap-aura.org | Eugène         | Martin      |                            | Cette valeur ne doit pas être vide.                                      |
      | admin.aura@test.amap-aura.org | Eugène         | Martin      | amapien@test.amap-aura.org | Cet email existe déjà dans la base de donnée.                            |

  @database
  Plan du Scénario: Tester le message de réussite avec les profils réseau et super-admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/amapien"
    Et que je remplis "search_amapien[region]" avec "84"
    Et que je remplis "search_amapien[amap]" avec "1"
    Et que je suis "Créer un nouvel amapien"
    Et que je remplis "amapien_creation[prenom]" avec "Eugène"
    Et que je remplis "amapien_creation[nom]" avec "Martin"
    Et que je remplis "amapien_creation[email]" avec "<contenu_mail>"
    Et que je remplis "amapien_creation[newsletter]" avec "1"
    Et que je remplis "amapien_creation[needWaiting]" avec "0"
    Et que je presse "Sauvegarder"
    Alors je devrais être sur "/amapien"
    Et je devrais voir "<message>"
    Et je devrais voir "<contenu_mail>"


    Exemples:
      | mail                          | contenu_mail            | message                                   |
      | admin@test.amap-aura.org      | test@test.amap-aura.org | L'amapien(ne) a été créé(e) avec succès ! |
      | admin.aura@test.amap-aura.org | test@amap-aura.org      | L'amapien(ne) a été créé(e) avec succès ! |


  @database
  Plan du Scénario: Tester les messages d'erreur avec le profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/amapien"
    Et que je suis "Créer un nouvel amapien"
    Et que je remplis "amapien_creation[prenom]" avec "<contenu_prenom>"
    Et que je remplis "amapien_creation[nom]" avec "<contenu_nom>"
    Et que je remplis "amapien_creation[email]" avec "<contenu_mail>"
    Et que je remplis "amapien_creation[newsletter]" avec "1"
    Et que je remplis "amapien_creation[needWaiting]" avec "0"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | contenu_prenom | contenu_nom | contenu_mail               | message                                                                  |
      |                | Martin      | test@test.amap-aura.org    | Cette valeur ne doit pas être vide.                                      |
      | Eugène         |             | test@test.amap-aura.org    | Cette valeur ne doit pas être vide.                                      |
      | Eugène         | Martin      |                            | Cette valeur ne doit pas être vide.                                      |
      | Eugène         | Martin      | amapien@test.amap-aura.org | Cet email existe déjà dans la base de donnée.                            |

  @database
  Scénario: Tester le message de réussite avec le profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/amapien"
    Et que je suis "Créer un nouvel amapien"
    Et que je remplis "amapien_creation[prenom]" avec "Eugène"
    Et que je remplis "amapien_creation[nom]" avec "Martin"
    Et que je remplis "amapien_creation[email]" avec "eugene@test.amap-aura.org"
    Et que je remplis "amapien_creation[newsletter]" avec "1"
    Et que je remplis "amapien_creation[needWaiting]" avec "0"
    Et que je presse "Sauvegarder"
    Alors je devrais être sur "/amapien"
    Et je devrais voir "L'amapien(ne) a été créé(e) avec succès !"
    Et je devrais voir "eugene@test.amap-aura.org"
