<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $livraisonHoraire */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
/** @var \PsrLib\ORM\Entity\Amapien $amap */
?>

<h3>Modifier les horaires de livraison</h3>

<?php

if (get_current_connected_user() instanceof \PsrLib\ORM\Entity\Amap) {
    $retour = 'Gestion de mon AMAP';
} else {
    $retour = 'Gestion des AMAP';
}

echo '<h5><a href="'.site_url('amap').'">'.$retour.'</a> <i class="glyphicon glyphicon-menu-right"></i> '.mb_strtoupper($amap->getNom()).' <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('amap/livraison_lieu/'.$amap->getId()).'">Les lieux de livraison </a> <i class="glyphicon glyphicon-menu-right"></i> '.$livraisonLieu->getNom().'</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('amap/livraison_horaire/'.$livraisonLieu->getId()).'"> Les horaires de livraison</a> <i class="glyphicon glyphicon-menu-right"></i> L\''.$livraisonHoraire->getSaison().', le '.$livraisonHoraire->getJour().' : '.$livraisonHoraire->getHeureDebut().' - '.$livraisonHoraire->getHeureFin().'</h5>';

// echo '<div class="alert alert-info">Ajoutez des lieux de livraison ...</div>';

?>

<div class="row">
    <div class="col-md-12" id="livraison_lieu">

        <!-- ----------------------------------------------------------------------- -->
        <!-- AJOUTER UN LIEU -->
        <form method="POST"
              action="<?= site_url('amap/livraison_horaire_edit/'.$livraisonHoraire->getId()); ?>"
              class="clearfix">

            <div class="entry">

                <div class="well well-sm">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">

                                <div class="col-md-3">
                                    <label for="liv_saison" class="control-label">Saison : </label>
                                    <?= render_form_select_widget($form['saison']); ?>
                                </div>

                                <div class="col-md-3">
                                    <label for="liv_jour" class="control-label">Jour de livraison : </label>
                                    <?= render_form_select_widget($form['jour']); ?>
                                </div>

                                <div class="col-md-3">
                                    <label for="liv_heure_deb" class="control-label">Heure de début : </label>
                                    <?= render_form_text_widget($form['heureDebut']); ?>
                                </div>

                                <div class="col-md-3">
                                    <label for="liv_heure_fin" class="control-label">Heure de fin : </label>
                                    <?= render_form_text_widget($form['heureFin']); ?>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-group pull-right">

                <input type="submit" value="Valider" class="pull-right btn btn-success clearfix"
                       style="margin-left:5px;"/>
                <a href="<?= site_url('amap/livraison_horaire/'.$livraisonLieu->getId()); ?>"
                   class="pull-right btn btn-default">Annuler</a>

            </div>

        </form>

    </div>
</div>
