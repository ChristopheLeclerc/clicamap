<?php

function modal_email($paysans)
{
    $emails = array_map(function (PsrLib\ORM\Entity\Paysan $paysan) {
        return $paysan->getEmail();
    }, $paysans);
    $emailsString = implode(PHP_EOL, $emails);

    return <<< EOL
        <div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les paysans de cette
                            liste
                                ?</h4>
                    </div>
                    <div class="modal-body">
                        <h5>Pour envoyer un email à tous ces paysans , vous devez :</h5>
                        <ul>
                            <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit
    +
    Copier sur la zone bleue ci dessous
    </li>
                            <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
                            <li>Faire nouveau message</li>
                            <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit
                                +
                                Coller dans la liste des destinataires du message.
                            </li>
                        </ul>

                        <h5>Email :</h5>
                        <form>
                            <div class="form-group">
                            <textarea class="form-control" rows="5">{$emailsString}</textarea>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
EOL;
}
