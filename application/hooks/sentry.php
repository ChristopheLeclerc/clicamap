<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Sentry\Integration\RequestIntegration;

function addUserInfoToScope(Sentry\State\Scope $scope)
{
    $userInfo = [];

    if (!isset($_SESSION) || !is_array($_SESSION)) {
        return;
    }

    if (array_key_exists('email', $_SESSION)) {
        $userInfo['email'] = $_SESSION['email'];
    }
    if (array_key_exists('statut', $_SESSION)) {
        $userInfo['statut'] = $_SESSION['statut'];
    }
    if (array_key_exists('ID', $_SESSION)) {
        $userInfo['id'] = $_SESSION['ID'];
    }

    $scope->setUser($userInfo);
}

function sentryCapture($e)
{
    // Configure sentry
    Sentry\configureScope(function (Sentry\State\Scope $scope): void {
        $scope->setLevel(Sentry\Severity::fatal());

        addUserInfoToScope($scope);
    });

    // Capture exception on sentry
    Sentry\captureException($e);
}

function initSentry()
{
    Sentry\init([
        'dsn' => $_ENV['SENTRY_DSN'],
        'environment' => $_ENV['SENTRY_ENV'],
        'default_integrations' => false, // Disable default integration to avoid CodeIgniter core functions override
        'integrations' => [new RequestIntegration()], // Add request informations
    ]);

    // Replace default exception handler with custom one to pass exception to sentry
    set_exception_handler(function ($e) {
        sentryCapture($e);

        // Call default exception handler
        _exception_handler($e);
    });

    // Replace default error handler
    set_error_handler(function ($severity, $message, $filepath, $line) {
        if (0 !== error_reporting()) { // Non silent error
            // Capture exception on sentry
            $e = new ErrorException($message, 0, $severity, $filepath, $line);
            sentryCapture($e);
        }

        // Call default error handler
        _error_handler($severity, $message, $filepath, $line);
    });
}
