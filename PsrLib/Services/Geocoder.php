<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Exception;
use Geocoder\Formatter\StringFormatter;
use Geocoder\Model\Address;
use Geocoder\Provider\Cache\ProviderCache;
use Geocoder\Provider\Nominatim\Nominatim;
use Geocoder\Query\GeocodeQuery;
use Geocoder\StatefulGeocoder;
use Http\Client\Curl\Client;
use PsrLib\ProjectLocation;
use Symfony\Component\Cache\Simple\FilesystemCache;

class Geocoder
{
    public const NOMINATIM_ROOT = 'https://nominatim.openstreetmap.org';
    public const NOMINATIM_AGENT = 'clicamap';

    /**
     * @var StatefulGeocoder
     */
    private $geocoder;

    public function __construct()
    {
        $cacheDuration = 3600 * 24; // 1 jour
        $provider = new ProviderCache(
            new Nominatim(new Client(), self::NOMINATIM_ROOT, self::NOMINATIM_AGENT),
            new FilesystemCache('geocoder', 0, ProjectLocation::PROJECT_ROOT.'/application/writable/cache/geocoder/'),
            $cacheDuration
        );

        $this->geocoder = new StatefulGeocoder($provider, 'fr');
        $this->geocoder->setLimit(200);
    }

    public function geocode_suggestions(string $query): array
    {
        $formatter = new StringFormatter();

        $geocodeQuery = GeocodeQuery::create($query)
            ->withData('countrycodes', 'FR')
        ;

        try {
            /** @var Address[] $addresses */
            $addresses = $this->geocoder->geocodeQuery($geocodeQuery);
        } catch (Exception $e) {
            return [];
        }

        $res = [];
        foreach ($addresses as $address) {
            $coordinates = $address->getCoordinates();
            if (
                null !== $coordinates
                && null !== $address->getPostalCode()
            ) {
                $res[] = [
                    'label' => $formatter->format($address, '%n %S, %z %L'),
                    'value' => [
                        'cp' => $address->getPostalCode(),
                        'ville' => $address->getLocality(),
                        'lat' => $coordinates->getLatitude(),
                        'long' => $coordinates->getLongitude(),
                        'address' => $formatter->format($address, '%n %S'),
                    ],
                ];
            }
        }

        return $res;
    }
}
