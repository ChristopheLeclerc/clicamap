# language: fr
Fonctionnalité: Comme administrateur, je devrais pouvoir éditer un document

  Contexte:
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Et que je remplis "nom" avec "Nom du nouveau document"
    Et que je remplis "lien" avec "http://www.amap-aura.org"
    Et que je coche "Page d'accueil (utilisateur non connecté)"
    Et que je presse "Sauvegarder"

  @database
  Scénario: Edition du document
    Etant donné je suis sur "/documentation/list_documents"
    Quand je suis "Modifier le document"
    Et que je remplis "nom" avec "Nom modifié"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "Document correctement modifié"
    Et je devrais voir "Nom modifié"

  @database
  Scénario: Annulation édition du document
    Etant donné je suis sur "/documentation/list_documents"
    Quand je suis "Modifier le document"
    Et que je remplis "nom" avec "Nom modifié"
    Et que je suis "Annuler"
    Et je devrais voir "Nom du nouveau document"

