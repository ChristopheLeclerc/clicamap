<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
?>

<h3>Produits proposés / recherchés</h3>

<h5><a href="<?= site_url('amap'); ?>">Gestion des AMAP</a> <i class="glyphicon glyphicon-menu-right"></i>
    Création d'une AMAP <i class="glyphicon glyphicon-menu-right"></i>
    Étape 4</h5>

<form method="POST" action="produits_creation">

    <input type="hidden" name="step_prd" value="1"/>


    <div class="well">
        <div class="row">
            <div class="col-md-12">
                <?= render_form_hidden_widget($form['hidden']); ?>

                <div class="form-group col-md-6">
                    <label for="produits_proposes" class="control-label">Produits proposés (liste à choix multiples ;
                        maintenez Shift ou Ctrl enfoncés pour choisir plusieurs produits) :</label>
                    <?= render_form_multiselect_widget($form['tpPropose']); ?>
                </div>

                <div class="form-group col-md-6">
                    <label for="produits_recherches" class="control-label">Produits recherchés (liste à choix multiples
                        ; maintenez Shift ou Ctrl enfoncés pour choisir plusieurs produits) :</label>
                    <?= render_form_multiselect_widget($form['tpRecherche']); ?>
                </div>

            </div>
        </div>
    </div>

    <div class="form-group">
        <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>
</form>

<a href="<?= site_url('/amap/livraison_horaire_creation'); ?>" class="pull-right btn btn-default" style="margin-left:5px;">Étape
    précédente</a>
<a href="<?= site_url('amap'); ?>" class="pull-right btn btn-default">Annuler</a>
