<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Reseau;

class SearchAmapienState extends SearchAmapienAmapState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    /**
     * @var null|Reseau
     */
    private $reseau;

    /**
     * @var bool | null
     */
    private $noAmap = false;

    public function isEmpty(): bool
    {
        return parent::isEmpty()
            && null === $this->getRegion()
            && null === $this->getDepartement()
            && null === $this->getReseau()
            && false === $this->getNoAmap()
        ;
    }

    public function getReseau(): ?Reseau
    {
        return $this->reseau;
    }

    public function setReseau(?Reseau $reseau): SearchAmapienState
    {
        $this->reseau = $reseau;

        return $this;
    }

    public function getNoAmap(): ?bool
    {
        return $this->noAmap;
    }

    public function setNoAmap(?bool $noAmap): SearchAmapienState
    {
        $this->noAmap = $noAmap;

        return $this;
    }
}
