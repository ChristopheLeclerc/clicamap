<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $searchForm */
/** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
?>

<h3>Gestion des contrats archivés</h3>
<form method="GET" action="" class="js-form-search">
    <div class="well">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-primary"
                                    type="button">AMAP</button>
                            </span>
                    <?= render_form_select_widget($searchForm['amap']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-primary"
                                    type="button">Contrat vierge</button>
                            </span>
                    <?= render_form_select_widget($searchForm['mc']); ?>

                </div>
            </div>
        </div>
    </div>
</form>

<?php if (!empty($contrats)): ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-datatable">
                <thead>
                <tr>
                    <th><b>Nom</b></th>
                    <th>Prénom</th>
                    <th>Date de création</th>
                    <th>Date de modification</th>
                    <th>Outils</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($contrats as $contrat): ?>
                        <tr>
                            <td>
                                <b><?=$contrat->getAmapien()->getNom(); ?></b>
                            </td>
                            <td>
                                <?=$contrat->getAmapien()->getPrenom(); ?>
                            </td>
                            <td>
                                <?=$contrat->getDateCreation()->format('Y-m-d'); ?>
                            </td>
                            <td>
                                <?php if (null !== $contrat->getDateModification()): ?>
                                    <?=$contrat->getDateModification()->format('Y-m-d'); ?>
                                <?php else: ?>
                                    Le contrat n'a pas encore été modifié
                                <?php endif; ?>
                            </td>
                            <td class="text-right">
                                <a title="Télécharger le contrat" href="<?=site_url('contrat_signe/telecharger_contrat/'.$contrat->getId()); ?>">
                                    <i class="glyphicon glyphicon-list-alt" data-toggle="tooltip" title="Télécharger le contrat"></i>
                                </a>
                                <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EXPORT_PDF, $contrat)) : ?>
                                    <a title="Télécharger le contrat" href='<?=site_url('/contrat_signe/contrat_pdf/'.$contrat->getId()); ?>'>
					<i class="glyphicon glyphicon-file" data-toggle="tooltip" title="Télécharger le contrat"></i>
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
<?php endif; ?>
