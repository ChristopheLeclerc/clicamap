# language: fr
Fonctionnalité: Comme administrateur, je devrais pouvoir ajouter un document

  Scénario: : Accès par le menu comme admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/list_documents"
    Quand je suis "Créer un nouveau document"
    Alors je devrais voir "Nom du document"
    Et je devrais être sur "/documentation/form"

  Scénario: : Vérification des champs obligatoires
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Quand je presse "Sauvegarder"
    Alors je devrais voir "Le champ \"Nom\" est requis."
    Alors je devrais voir "Le champ \"Lien\" est requis."

  Scénario: : Vérificaion de la validité de l'adresse mail
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Quand je remplis "nom" avec "test"
    Quand je remplis "lien" avec "lien_invalide"
    Quand je presse "Sauvegarder"
    Alors je devrais voir "Le champ \"Lien\" doit contenir une URL valide."

  Scénario: Vérification selection type d'utilisateur
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Quand je remplis "nom" avec "test"
    Et que je remplis "lien" avec "http://www.amap-aura.org"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "Merci de selectionner au moins un type d'utilisateur"

  @database
  Scénario: Ajout d'un document
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Quand je remplis "nom" avec "Nom du nouveau document"
    Et que je remplis "lien" avec "http://www.amap-aura.org"
    Et que je coche "Page d'accueil (utilisateur non connecté)"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "Document correctement ajouté"
    Et je devrais voir "Nom du nouveau document"
