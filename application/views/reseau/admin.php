<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Reseau $reseau */
?>

<script type="text/javascript">

  /*ALERTES & AUTOCOMPLÉTION CP / VILLE */
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 2500);

    /*$(function(){
      $("#amapien").autocomplete({
        source: <?= '"'.site_url('ajax/get_amapien_suggestion'.$reseau->getId().'"'); ?>,
    dataType: "json",
    minLength : 2,
    appendTo: "#container"
  });
});*/

    $(function () {
      $("#amapien").autocomplete({
        source: <?= '"'.site_url('ajax/get_amapien_suggestion"'); ?>,
        dataType: "json",
        minLength: 2,
        appendTo: "#container"
      });
    });

  });

</script>

<?php
echo '<h3>Les Administrateurs de mon réseau</h3>';

echo '<h5><a href="'.site_url('reseau').'">Mes réseaux</a> <i class="glyphicon glyphicon-menu-right"></i> '.$reseau->getNom().'</h5>';

echo '<div class="alert alert-info">';
echo 'Si vous voulez ajouter un administrateur au réseau, utilisez le champ ci-dessous <strong>en vous aidant des suggestions proposées en cours de frappe</strong>.&nbsp;';
// echo 'L\'amapien choisi pour être administrateur doit faire partie du réseau par l\'adresse de livraison de son AMAP.';
echo 'Dans le champ d\'ajout, il convient de commencer par le nom. L\'email de l\'amapien est mis entre parenthèses en cas d\'homonyme. Pour ensuite retirer les droits d\'administration à l\'amapien, il suffit de cliquer sur l\'amapien à supprimer.';
echo '</div>';

echo '<form method="POST" action="'.site_url('reseau/admin_ajout/'.$reseau->getId()).'">';

echo '<div class="row">';
echo '<div class="form-group col-sm-12">';
echo '<input type="text" class="form-control" id="amapien" name="amapien" value="" placeholder="Exemple : Dupont Jean"/>';
echo '<div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>';
echo form_error('amapien');
echo '</div>';
echo '<div class="form-group col-sm-12">';
echo '<input type="submit" value="Ajouter" class="btn btn-success pull-right"/>';
echo '</div>';
echo '</div>';
echo '</form>';

/** @var \PsrLib\ORM\Entity\Amapien $res_adm */
$currentUser = get_current_connected_user();
foreach ($reseau->getAdmins() as $res_adm) {
    if ($res_adm->getId() != $currentUser->getId()) {
        echo '<a href="#" data-toggle="modal" data-target="#modal_'.$res_adm->getId().'" ><button class="btn btn-primary btn-sm" data-toggle="tooltip" title="Retirer les droits à l\'amapien">'.mb_strtoupper($res_adm->getNom()).' '.ucfirst($res_adm->getPrenom()).' <i class="glyphicon glyphicon-remove"></i></button></a>&nbsp;';
    } else {
        echo '<button class="btn btn-default btn-sm" disabled >'.mb_strtoupper($res_adm->getNom()).' '.ucfirst($res_adm->getPrenom()).'</button>&nbsp;';
    }

    echo '<div class="modal fade text-left" id="modal_'.$res_adm->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    echo '<div class="modal-dialog">';
    echo '<div class="modal-content">';
    echo '<div class="modal-header">';
    echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    echo '<h4 class="modal-title" id="myModalLabel">Réseau "'.$reseau->getNom().'"</h4>';
    echo '</div>';
    echo '<div class="modal-body">';
    echo '<h5>Voulez-vous vraiment retirer "'.$res_adm->getNom().' '.$res_adm->getPrenom().'" du réseau ?</h5>';
    echo '<p class="text-right">';
    echo '<a href="'.site_url('reseau/admin_remove/'.$reseau->getId().'/'.$res_adm->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
    echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
    echo '</p>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}

?>
