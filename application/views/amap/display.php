<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \PsrLib\ORM\Entity\Ferme[] $amapFermes */
$currentUser = get_current_connected_user();

if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
    $retour = 'Gestion de mon AMAP';
} else {
    $retour = 'Gestion des AMAP';
}

echo '<h3>Détails de l\'AMAP</h3>';
echo '<h5><a href="'.site_url('amap').'">'.$retour.'</a> <i class="glyphicon glyphicon-menu-right"></i> '.$amap->getNom();

// COMMENTAIRE DATÉ ------------------------------------------------------------
if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_COMMENT, $amap)) {
    echo '<a href="#"><i class="glyphicon glyphicon-pencil pull-right" data-toggle="modal" data-target="#modal_commentaire" title="Écrire un commentaire daté" style="font-size:18px;"></i></a>&nbsp;';

    echo '<p class="clearfix">&nbsp;</p>'; ?>
    <div class="modal fade" id="modal_commentaire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Écrire un commentaire daté.</h4>
                </div>
                <div class="modal-body">
                    <?= '<form method="POST" action="'.site_url('amap/amap_commentaire_add/'.$amap->getId()).'">'; ?>
                    <div class="form-group clearfix">
                        <textarea class="form-control" rows="8" name="amap_commentaire"></textarea>
                        <input type="submit" value="Valider" class="pull-right btn btn-success clearfix"
                               style="margin:5px;"/>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php

    foreach ($amap->getCommentsOrdered() as $com) {
        echo '<div class="alert alert-warning clearfix">';
        echo '<a title="Supprimer le commentaire" href="'.site_url('amap/amap_commentaire_remove/'.$com->getId()).'" ><i class="glyphicon glyphicon-remove pull-right" data-toggle="tooltip" title="Supprimer le commentaire"></i></a>&nbsp;';
        echo '<h5><strong>Le '.date_format_french($com->getDate()).'</strong></h5>';
        echo '<p style="line-height:24px;">'.nl2br($com->getComment()).'</p>';
        echo '</div>';
    }
}

echo '</h5>';

echo '<div class="table-responsive well">';
echo '<h3 style="font-size:20px;">'.$amap->getNom().'</strong></h3>';
echo '<table class="table">';

echo '<tbody>';

if (\PsrLib\ORM\Entity\Amap::ETAT_CREATION === $amap->getEtat()) {
    $etat = 'en création';
} elseif (\PsrLib\ORM\Entity\Amap::ETAT_FONCIONNEMENT === $amap->getEtat()) {
    $etat = 'en fonctionnement';
} elseif (\PsrLib\ORM\Entity\Amap::ETAT_ATTENTE === $amap->getEtat()) {
    $etat = 'en attente';
}

echo '<tr>';
echo '<th>État de l\'AMAP :</th>';
echo '<td>'.ucfirst($etat).'</td>';
echo '</tr>';

echo '<tr>';
echo '<th>Association de fait :</th>';
echo '<td>'.($amap->getAssociationDeFait() ? 'Oui' : 'Non').'</td>';
echo '</tr>';

if (!$amap->getAssociationDeFait()) {
    echo '<tr>';
    echo '<th>SIRET :</th>';
    echo '<td>'.$amap->getSiret().'</td>';
    echo '</tr>';

    echo '<tr>';
    echo '<th>RNA : </th>';
    echo '<td>'.$amap->getRna().'</td>';
    echo '</tr>';
}

if (null !== $amap->getEmail()) {
    echo '<tr>';
    echo '<th>Email contact public :</th>';
    echo '<td><a href="mailto:'.$amap->getEmail().'">'.$amap->getEmail().'</a></td>';
    echo '</tr>';
} elseif (null !== $amap->getAmapienRefReseau()) {
    $ref = $amap->getAmapienRefReseau();
    echo '<tr>';
    echo '<th>Email contact public :</th>';
    echo '<td>';
    echo '<div class="mark">';
    echo '<a href="mailto:'.$ref->getEmail().'">'.$ref->getEmail().'</a>';
    echo ' (email du référent réseau).';
    echo '</div>';
    echo '</td>';
    echo '</tr>';
}

if (null !== $amap->getUrl()) {
    echo '<tr>';
    echo '<th>Site web :</th>';
    echo '<td><a href="'.$amap->getUrl().'" target="_blank">'.$amap->getUrl().'</a></td>';
    echo '</tr>';
}

if (null !== $amap->getAnneeCreation()) {
    echo '<tr>';
    echo '<th>Date de création :</th>';
    echo '<td>'.$amap->getAnneeCreation().'</td>';
    echo '</tr>';
}

if (null !== $amap->getNbAdherents() && '' !== $amap->getNbAdherents()) {
    echo '<tr>';
    echo '<th>Nombre d\'adhérents :</th>';
    echo '<td>'.$amap->getNbAdherents().'</td>';
    echo '</tr>';
}

if (!$amap->getTpPropose()->isEmpty()) {
    echo '<tr>';
    echo '<th>Produits proposés :</th>';
    echo '<td>';

    echo implode(' | ', $amap->getTpPropose()->matching(\PsrLib\ORM\Repository\TypeProductionRepository::criteriaOrderId())->toArray());

    echo '</td>';
    echo '</tr>';
}

if (!$amap->getTpRecherche()->isEmpty()) {
    echo '<tr>';
    echo '<th>Produits récherchés :</th>';
    echo '<td>';

    echo implode(' | ', $amap->getTpRecherche()->matching(\PsrLib\ORM\Repository\TypeProductionRepository::criteriaOrderId())->toArray());

    echo '</td>';
    echo '</tr>';
}

/** @var \Doctrine\Common\Collections\ArrayCollection<\PsrLib\ORM\Entity\AmapLivraisonLieu> $lls */
$lls = $amap->getLivraisonLieux();
if (!$lls->isEmpty()) {
    echo '<tr>';

    if ($lls->count() > 1) {
        echo '<th>Adresses de livraison :</th>';
    } else {
        echo '<th>Adresse de livraison :</th>';
    }

    echo '<td>';

    /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $liv_lieu */
    foreach ($lls as $liv_lieu) {
        echo '<div class="alert alert-success">';
        echo '<strong>';
        echo ucfirst($liv_lieu->getNom()).' - ';
        echo ucfirst($liv_lieu->getAdresse()).' '.$liv_lieu->getVille()->getCpString().' '.mb_strtoupper($liv_lieu->getVille()->getNom());
        echo '</strong>';

        $llhs = $liv_lieu->getLivraisonHoraires();
        if (!$llhs->isEmpty()) {
            echo '<ul>';

            /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $liv_hor */
            foreach ($llhs as $liv_hor) {
                if ($liv_hor->getSaison() && $liv_hor->getJour() && $liv_hor->getHeureDebut() && $liv_hor->getHeureFin()) {
                    echo '<li>';
                    if (\PsrLib\ORM\Entity\AmapLivraisonHoraire::SAISON_ETE == $liv_hor->getSaison()) {
                        echo 'Été';
                    }
                    if (\PsrLib\ORM\Entity\AmapLivraisonHoraire::SAISON_HIVER == $liv_hor->getSaison()) {
                        echo 'Hiver';
                    }
                    echo ' -> ';
                    echo ucfirst($liv_hor->getJour()).' : <strong>'.$liv_hor->getHeureDebut().' - '.$liv_hor->getHeureFin();
                    echo '</strong></li>';
                }
            }

            echo '</ul>';
        }

        echo '</div>';
    }

    echo '</td></tr>';

    if (!$lls->isEmpty() && null !== $lls->first()->getGpsLatitude() && null !== $lls->first()->getGpsLongitude()) {
        echo '<tr>';
        echo '<th>Coordonnées GPS :</th>';
        echo '<td><a href="'.site_url('map/amap_display/'.$amap->getId()).'"><i class="glyphicon glyphicon-map-marker"></i>&nbsp;Visualiser le ou les adresse(s) de livraison sur une carte</a>&nbsp;';
        echo '</td></tr>';
    }

    if (null !== $amap->getAdresseAdminNom() && '' !== $amap->getAdresseAdminNom()) {
        echo '<tr>';
        echo '<th>Nom du lieu de l\'adresse administrative</th>';
        echo '<td>'.$amap->getAdresseAdminNom().'</td>';
        echo '</tr>';
    }

    if (null !== $amap->getAdresseAdmin() && '' !== $amap->getAdresseAdmin()) {
        echo '<tr>';
        echo '<th>Adresse Administrative :</th>';
        echo '<td>'.ucfirst($amap->getAdresseAdmin()).',<br/>'.$amap->getAdresseAdminVille()->getCpString().' '.mb_strtoupper($amap->getAdresseAdminVille()->getNom());
        echo '</tr>';
    }

    /** @var \Doctrine\Common\Collections\ArrayCollection<\PsrLib\ORM\Entity\Amapien> $gestionnaires */
    $gestionnaires = $amap->getAmapiens()->matching(\PsrLib\ORM\Repository\AmapienRepository::criteriaGestionaire());
    if (!$gestionnaires->isEmpty()) {
        $i = 1;

        echo '<tr>';
        echo '<th>Correspondant(s) Clic\'AMAP :</th>';
        echo '<td>';

        /** @var \PsrLib\ORM\Entity\Amapien $gestionnaire */
        foreach ($gestionnaires as $gestionnaire) {
            echo ucfirst($gestionnaire->getPrenom()).' '.mb_strtoupper($gestionnaire->getNom()).'<br/>';

            if ($gestionnaire->getNumTel1() && $gestionnaire->getNumTel2()) {
                echo $gestionnaire->getNumTel1().' | '.$gestionnaire->getNumTel2().'<br/>';
            } else {
                if (null !== $gestionnaire->getNumTel1()) {
                    echo $gestionnaire->getNumTel1().'<br/>';
                }

                if (null !== $gestionnaire->getNumTel2()) {
                    echo $gestionnaire->getNumTel2().'<br/>';
                }
            }

            echo '<a href="mailto:'.$gestionnaire->getEmail().'">'.$gestionnaire->getEmail().'</a><br/>';

            if ($i < $gestionnaires->count()) {
                echo '<br/>';
            }

            ++$i;
        }

        echo '</td>';
        echo '</tr>';
    }

    $refReseau = $amap->getAmapienRefReseau();
    if (null !== $refReseau) {
        echo '<tr>';
        echo '<th>Correspondant réseau AMAP :</th>';
        echo '<td>'.ucfirst($refReseau->getPrenom()).' '.mb_strtoupper($refReseau->getNom()).'<br/>';
        echo $refReseau->getNumTel1().'<br/>';
        echo '<a href="mailto:'.$refReseau->getEmail().'">'.$refReseau->getEmail().'</a>';

        $refReseauVille = $refReseau->getVille();
        if (null !== $refReseauVille) {
            echo '<br/><br/>';
            if ($refReseau->getLibAdr1()) {
                echo $refReseau->getLibAdr1().',<br/>';
            } elseif ($refReseau->getLibAdr2()) {
                echo $refReseau->getLibAdr2().',<br/>';
            }

            echo $refReseauVille->getCpString().' '.$refReseauVille->getNom().'</td>';
            echo '</tr>';
        }
    }

    $refReseauSec = $amap->getAmapienRefReseauSecondaire();
    if (null !== $refReseauSec) {
        echo '<tr>';
        echo '<th>Second correspondant réseau AMAP :</th>';
        echo '<td>'.ucfirst($refReseauSec->getPrenom()).' '.mb_strtoupper($refReseauSec->getNom()).'<br/>';
        echo $refReseauSec->getNumTel1().'<br/>';
        echo '<a href="mailto:'.$refReseauSec->getEmail().'">'.$refReseauSec->getEmail().'</a>';

        $refReseauSecVille = $refReseauSec->getVille();
        if (null !== $refReseauSecVille) {
            echo '<br/><br/>';
            if ($refReseauSec->getLibAdr1()) {
                echo $refReseauSec->getLibAdr1().',<br/>';
            } elseif ($refReseauSec->getLibAdr2()) {
                echo $refReseauSec->getLibAdr2().',<br/>';
            }

            echo $refReseauSecVille->getCpString().' '.$refReseauSecVille->getNom().'</td>';
            echo '</tr>';
        }
    }

    $annee_adhesion = $amap->getAnneeAdhesionsOrdered();
    if (count($annee_adhesion) > 0) {
        echo '<tr>';
        echo '<th>Année(s) d\'adhésion :</th>';

        echo '<td>';

        $i = 1;
        $pipe = ' | ';

        /** @var \PsrLib\ORM\Entity\AmapAnneeAdhesion $aa */
        foreach ($annee_adhesion as $aa) {
            echo $aa->getAnnee();

            if ($i < count($annee_adhesion)) {
                echo $pipe;
            }

            ++$i;
        }

        echo '</td>';
        echo '</tr>';
    }

    echo '</tbody>';
    echo '</table>';
    echo '</div>';
}

// COLLECTIF -------------------------------------------------------------------
if ($amap->getCollectifs()->count() > 0) {
    ?>

    <div class="modal fade" id="modal_collectif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les membres du collectif
                        ?</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Copier sur la zone bleue ci dessous
                        </li>
                        <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
                        <li>Faire nouveau message</li>
                        <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Coller dans la liste des destinataires du message.
                        </li>
                    </ul>
                    <form>
                        <div class="form-group">
              <textarea class="form-control" rows="8"><?php $nbr_membre = $amap->getCollectifs()->count();
    $retour = 1;
    /** @var \PsrLib\ORM\Entity\AmapCollectif $coll */
    foreach ($amap->getCollectifs() as $coll) {
        if (null !== $coll->getAmapien()->getEmail()) {
            echo $coll->getAmapien()->getEmail();
            if ($retour < $nbr_membre) {
                echo "\n";
            }
        }
        ++$retour;
    } ?></textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive well">

        <p><a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#modal_collectif"
              data-dismiss="modal" aria-hidden="true" style="margin-left:5px;"><i
                        class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>
            <a href="<?= site_url('amap/telecharger_collectif/'.$amap->getId()); ?>"
               class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-download"></i> Télécharger la
                liste</a></p>

        <h3 style="font-size:20px;">Collectif</h3>
        <table class="table table-hover">
            <thead class="thead-inverse">
            <tr>
                <th>Profil</th>
                <th>Précision</th>
                <th>Nom Prénom</th>
                <th>Téléphone</th>
                <th class="text-right">Email</th>
            </tr>
            </thead>

            <tbody>

            <?php

            // Pour régler l'orde d'affichage ----------------------------------------
            // D'abord le président --------------------------------------------------
            /** @var \PsrLib\ORM\Entity\AmapCollectif $coll */
            foreach ($amap->getCollectifs() as $coll) {
                if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_PRESIDENT == $coll->getProfil()) {
                    $amapien = $coll->getAmapien();
                    echo '<tr>';
                    echo '<th scope="row">Président</th>';
                    echo '<td>';
                    if (null !== $coll->getPrecision()) {
                        echo ucfirst(mb_strtolower($coll->getPrecision()));
                    } else {
                        echo 'Aucune';
                    }
                    echo '</td>';
                    echo '<td>';
                    echo '<a href="'.site_url('amapien/informations_generales/'.$coll->getId()).'" target="_blank">'.ucfirst($amapien->getPrenom()).' '.mb_strtoupper($amapien->getNom()).'</a><br/>';
                    echo '</td>';
                    echo '<td>';
                    if ($amapien->getNumTel1() && $amapien->getNumTel2()) {
                        echo $amapien->getNumTel1().' | '.$amapien->getNumTel2().'<br/>';
                    } else {
                        if ($amapien->getNumTel1()) {
                            echo $amapien->getNumTel1().'<br/>';
                        }
                        if ($amapien->getNumTel2()) {
                            echo $amapien->getNumTel2().'<br/>';
                        }
                    }
                    echo '</td>';
                    echo '<td class="text-right"><a href="mailto:'.$amapien->getEmail().'">'.$amapien->getEmail().'</a></td>';
                }
            }
    // Ensuite le secrétaire -------------------------------------------------
    foreach ($amap->getCollectifs() as $coll) {
        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_SECRETAIRE == $coll->getProfil()) {
            $amapien = $coll->getAmapien();
            echo '<tr>';
            echo '<th scope="row">Secrétaire</th>';
            echo '<td>';
            if (null !== $coll->getPrecision()) {
                echo ucfirst(mb_strtolower($coll->getPrecision()));
            } else {
                echo 'Aucune';
            }
            echo '</td>';
            echo '<td>';
            echo '<a href="'.site_url('amapien/informations_generales/'.$amapien->getId()).'" target="_blank">'.ucfirst($amapien->getPrenom()).' '.mb_strtoupper($amapien->getNom()).'</a><br/>';
            echo '</td>';
            echo '<td>';
            if ($amapien->getNumTel1() && $amapien->getNumTel2()) {
                echo $amapien->getNumTel1().' | '.$amapien->getNumTel2().'<br/>';
            } else {
                if ($amapien->getNumTel1()) {
                    echo $amapien->getNumTel1().'<br/>';
                }

                if ($amapien->getNumTel2()) {
                    echo $amapien->getNumTel2().'<br/>';
                }
            }
            echo '</td>';
            echo '<td class="text-right"><a href="mailto:'.$amapien->getEmail().'">'.$amapien->getEmail().'</a></td>';
        }
    }

    // Ensuite le trésorier -------------------------------------------------
    /** @var \PsrLib\ORM\Entity\AmapCollectif $coll */
    foreach ($amap->getCollectifs() as $coll) {
        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_TRESORIER == $coll->getProfil()) {
            $amapien = $coll->getAmapien();
            echo '<tr>';
            echo '<th scope="row">Trésorier</th>';
            echo '<td>';
            if (null !== $coll->getPrecision()) {
                echo ucfirst(mb_strtolower($coll->getPrecision()));
            } else {
                echo 'Aucune';
            }
            echo '</td>';
            echo '<td>';
            echo '<a href="'.site_url('amapien/informations_generales/'.$amapien->getId()).'" target="_blank">'.ucfirst($amapien->getPrenom()).' '.mb_strtoupper($amapien->getNom()).'</a><br/>';
            echo '</td>';
            echo '<td>';
            if ($amapien->getNumTel1() && $amapien->getNumTel2()) {
                echo $amapien->getNumTel1().' | '.$amapien->getNumTel2().'<br/>';
            } else {
                if ($amapien->getNumTel1()) {
                    echo $amapien->getNumTel1().'<br/>';
                }

                if ($amapien->getNumTel2()) {
                    echo $amapien->getNumTel2().'<br/>';
                }
            }
            echo '</td>';
            echo '<td class="text-right"><a href="mailto:'.$amapien->getEmail().'">'.$amapien->getEmail().'</a></td>';
        }
    }

    // Puis les autres rôles -------------------------------------------------
    /** @var \PsrLib\ORM\Entity\AmapCollectif $coll */
    foreach ($amap->getCollectifs() as $coll) {
        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_AUTRE == $coll->getProfil()) {
            $amapien = $coll->getAmapien();
            echo '<tr>';
            echo '<th scope="row">Autre</th>';
            echo '<td>';
            if (null !== $coll->getPrecision()) {
                echo ucfirst(mb_strtolower($coll->getPrecision()));
            } else {
                echo 'Aucune';
            }
            echo '</td>';
            echo '<td>';
            echo '<a href="'.site_url('amapien/informations_generales/'.$amapien->getId()).'" target="_blank">'.ucfirst($amapien->getPrenom()).' '.mb_strtoupper($amapien->getNom()).'</a><br/>';
            echo '</td>';
            echo '<td>';
            if ($amapien->getNumTel1() && $amapien->getNumTel2()) {
                echo $amapien->getNumTel1().' | '.$amapien->getNumTel2().'<br/>';
            } else {
                if ($amapien->getNumTel1()) {
                    echo $amapien->getNumTel1().'<br/>';
                }

                if ($amapien->getNumTel2()) {
                    echo $amapien->getNumTel2().'<br/>';
                }
            }
            echo '</td>';
            echo '<td class="text-right"><a href="mailto:'.$amapien->getEmail().'">'.$amapien->getEmail().'</a></td>';
        }
    } ?>

            </tbody>
        </table>
    </div>

    <?php
}

// FERMES ----------------------------------------------------------------------
if (count($amapFermes) > 0) {
    ?>

    <div class="modal fade" id="modal_paysans" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les paysans ?</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Copier sur la zone bleue ci dessous
                        </li>
                        <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
                        <li>Faire nouveau message</li>
                        <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Coller dans la liste des destinataires du message.
                        </li>
                    </ul>
                    <form>
                        <div class="form-group">
                            <?php
                            $emails = [];
    foreach ($amapFermes as $ferme) {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        foreach ($ferme->getPaysans() as $paysan) {
            $emails[] = $paysan->getEmail();
        }
    }

    $emails = array_unique($emails);

    $emails_nbr = count($emails);
    $retour = 1; ?>
                            <textarea class="form-control" rows="8"><?php foreach ($emails as $email) {
        echo $email;
        if ($retour < $emails_nbr) {
            echo "\n";
        }
        ++$retour;
    } ?>
              </textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_referents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les référents produit
                        ?</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Copier sur la zone bleue ci dessous
                        </li>
                        <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
                        <li>Faire nouveau message</li>
                        <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Coller dans la liste des destinataires du message.
                        </li>
                    </ul>
                    <form>
                        <div class="form-group">
                            <?php
                            $emails = [];
    /** @var \PsrLib\ORM\Entity\Amapien $amapien */
    foreach ($amap->getAmapiens() as $amapien) {
        if (!$amapien->getRefProdFermes()->isEmpty()) {
            $emails[] = $amapien->getEmail();
        }
    }

    $emails = array_unique($emails);

    $emails_nbr = count($emails);
    $retour = 1; ?>
                            <textarea class="form-control" rows="8"><?php foreach ($emails as $email) {
        echo $email;
        if ($retour < $emails_nbr) {
            echo "\n";
        }
        ++$retour;
    } ?>
              </textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive well">

        <p>
            <a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#modal_referents"
               data-dismiss="modal" aria-hidden="true" style="margin-left:5px;"><i
                        class="glyphicon glyphicon-envelope"></i> Emails des référents</a>
            <a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#modal_paysans"
               data-dismiss="modal" aria-hidden="true" style="margin-left:5px;"><i
                        class="glyphicon glyphicon-envelope"></i> Emails des paysans</a>

            <a href="<?= site_url('amap/telecharger_referents_produit/'.$amap->getId()); ?>"
               class="btn btn-primary btn-sm pull-right" style="margin-left:5px;"><i
                        class="glyphicon glyphicon-download"></i> Liste des référents</a>
            <a href="<?= site_url('amap/telecharger_paysans/'.$amap->getId()); ?>"
               class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-download"></i> Liste des paysans</a>
        </p>

        <h3 style="font-size:20px;">Fermes / Référents produits</h3>
        <table class="table table-hover">
            <thead class="thead-inverse">
            <tr>
                <th>Ferme(s)</th>
                <th>Paysan(s)</th>
                <th>Référent(s) produit</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <?php

            echo '<tbody>';

    foreach ($amapFermes as $ferme) {
        echo '<tr>';

        echo '<th scope="row">';
        echo ucfirst(strtolower($ferme->getNom()));
        echo '</th>';

        echo '<td>';
        $virgule = 0;
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        foreach ($ferme->getPaysans() as $paysan) {
            $virgule = 0;
            echo '<a href="'.site_url('paysan/informations_generales/'.$paysan->getId()).'" target="_blank">'.strtoupper($paysan->getNom()).' '.ucfirst(strtolower($paysan->getPrenom())).'</a>';
            ++$virgule;
            if ($virgule < $ferme->getPaysans()->count()) {
                echo ' ; ';
            }
        }

        echo '</td>';

        echo '<td>';
        $virgule = 0;
        /** @var \PsrLib\ORM\Entity\Amapien $f_ref */
        foreach ($ferme->getAmapienRefs() as $f_ref) {
            if ($f_ref->getAmap()->getId() === $amap->getId()) {
                $nbr_f_ref = $virgule;
                $virgule = 0;

                echo '<a href="'.site_url('amapien/informations_generales_edition/'.$f_ref->getId()).'" target="_blank">'.strtoupper($f_ref->getNom()).' '.ucfirst(strtolower($f_ref->getPrenom())).'</a>';
                ++$virgule;
                if ($virgule < $ferme->getAmapienRefs()->count()) {
                    echo ' ; ';
                }
            }
        }

        echo '<td class="text-right">';

        $pay_emails = [];
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        foreach ($ferme->getPaysans() as $paysan) {
            $pay_emails[] = $paysan->getEmail();
        }
        array_unique($pay_emails);

        $ref_emails = [];
        foreach ($ferme->getAmapienRefs() as $ferme_referent) {
            if ($ferme_referent->getAmap() === $amap) {
                $ref_emails[] = $ferme_referent->getEmail();
            }
        }
        array_unique($ref_emails);

        echo '<a href="#" data-toggle="modal" data-target="#modal_pay_envoi_'.$ferme->getId().'" ><button type=button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Paysans</button></a>';

        echo '<a href="#" data-toggle="modal" data-target="#modal_ref_envoi_'.$ferme->getId().'" style="margin-left:5px;"><button type=button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Référents</button></a>'; ?>

                <div class="modal fade text-left" id="modal_pay_envoi_<?= $ferme->getId(); ?>" tabindex="-1"
                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les paysans
                                    ?</h4>
                            </div>
                            <div class="modal-body">
                                <ul>
                                    <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant
                                        clic droit + Copier sur la zone bleue ci dessous
                                    </li>
                                    <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook,
                                        ...)
                                    </li>
                                    <li>Faire nouveau message</li>
                                    <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant
                                        clic droit + Coller dans la liste des destinataires du message.
                                    </li>
                                </ul>
                                <form>
                                    <div class="form-group">
            <textarea class="form-control" rows="8"><?php $nbr_membre = count($pay_emails);
        $retour = 1;
        foreach ($pay_emails as $email) {
            if ($email) {
                echo $email;
                if ($retour < $nbr_membre) {
                    echo "\n";
                }
            }
            ++$retour;
        } ?></textarea>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade text-left" id="modal_ref_envoi_<?= $ferme->getId(); ?>" tabindex="-1"
                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les référents
                                    produits ?</h4>
                            </div>
                            <div class="modal-body">
                                <ul>
                                    <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant
                                        clic droit + Copier sur la zone bleue ci dessous
                                    </li>
                                    <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook,
                                        ...)
                                    </li>
                                    <li>Faire nouveau message</li>
                                    <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant
                                        clic droit + Coller dans la liste des destinataires du message.
                                    </li>
                                </ul>
                                <form>
                                    <div class="form-group">
            <textarea class="form-control" rows="8"><?php $nbr_membre = count($ref_emails);
        $retour = 1;
        foreach ($ref_emails as $email) {
            if ($email) {
                echo $email;
                if ($retour < $nbr_membre) {
                    echo "\n";
                }
            }
            ++$retour;
        } ?></textarea>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                </td>
                </tr>

                <?php
    } ?>

            </tbody>
        </table>
    </div>

    <?php
}

?>
