<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use CI_Config;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amapien;

class ContactCPSelecteur
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $mailDefaut;

    public function __construct(CI_Config $config, EntityManagerInterface $em)
    {
        $this->em = $em;

        $this->mailDefaut = $config->item('contact_default_mail');
    }

    /**
     * Choisi le mail de contact depuis le code postal.
     *
     * @return string[]
     */
    public function choisirContactDepuisCP(string $cp): array
    {
        $amapienRepo = $this
            ->em
            ->getRepository(Amapien::class)
        ;

        $depAmapiens = $amapienRepo
            ->getAdminDepFromCP($cp)
        ;
        if (!empty($depAmapiens)) {
            return $this->extraireEmailAmapien($depAmapiens);
        }

        $regAmapien = $amapienRepo
            ->getAdminRegFromCP($cp)
        ;
        if (!empty($regAmapien)) {
            return $this->extraireEmailAmapien($regAmapien);
        }

        $resAmapien = $amapienRepo
            ->getAdminResFromCP($cp)
        ;
        if (!empty($resAmapien)) {
            return $this->extraireEmailAmapien($resAmapien);
        }

        return [$this->mailDefaut];
    }

    /**
     * @param Amapien[] $amapiens
     *
     * @return null[]|string[]
     */
    private function extraireEmailAmapien($amapiens)
    {
        return array_map(function (Amapien $amapien) {
            return $amapien->getEmail();
        }, $amapiens);
    }
}
