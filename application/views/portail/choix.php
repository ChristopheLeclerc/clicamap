<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
echo '<h1>'.SITE_NAME.'</h1>';

echo form_open('portail/choix');

if ($ref_prod_choix) {
    echo '<div class="alert alert-warning">Vous êtes référent produit de plusieurs fermes, laquelle voulez-vous choisir pour cette session ?</div>'; ?>

    <div class="well">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group">
                    <label for="ref_prod_index" class="control-label">Fermes</label>
                    <select class="form-control" id="ref_prod_index" name="ref_prod_index">

                        <?php

                        // Index du tableau de portail_model->get_referent_produit()
                        $index = 0;

    foreach ($ref_prod_choix as $ferme) {
        echo '<option value="'.$index.'">'.$ferme->f_nom.'</option>';

        ++$index;
    } ?>

                    </select>
                </div>

            </div>
        </div>
    </div>

    <?php
} ?>

<button type="submit" class="btn btn-success">Valider</button>

</form>
