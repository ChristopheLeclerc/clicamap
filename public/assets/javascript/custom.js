/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

function pageAmapDistributionAjout()
{
  if($('body.page_amap_distribution_ajout').length === 0) {
    return;
  }

  function hideFieldOnChange(e) {
    const $row = $('input[name="amap_distribution_ajout[period][endAt]"]').parents('.form-group');
    if(e.target.value === "REP_AUCUNE") {
      $row.hide();
    } else {
      $row.show();
    }
  }

  $('select[name="amap_distribution_ajout[repetition]"]').on('change', hideFieldOnChange).trigger('change');
}

$(document).ready(function () {

  // form search
  $('form.js-form-search').on('change', 'select', function () {
    this.form.submit();
  })
  $('form.js-form-search input[type="checkbox"]').on('change', function () {
    this.form.submit();
  })

  // Js select all
  $('.js-select-all').click(function () {
    if ($(this).prop('checked') === false) {
      $('.js-auto-selectable').prop('checked', false).trigger('change');
    } else {
      $('.js-auto-selectable').prop('checked', true).trigger('change');
    }
  })
  $('.js-auto-selectable').click(function () {
    $('.js-select-all').prop('checked', false);
  })

  // Js bind
  $('input[type="checkbox"].js-binder').change(function () {
    var $this = $(this);
    var target = $this.attr('data-target-name');
    $('input[type="checkbox"][name="' + target + '"]').prop('checked', $this.prop('checked'));
  })

  // Modal
  var submitValid = false;
  $('button.js-trigger-modal').click(function (e) {
    var $this = $(this);

    function getData(label, def) {
      var res = $this.attr(label);
      if (res === null) {
        return def;
      }
      return res;
    }

    if (submitValid) {
      submitValid = false;
      return;
    }

    e.preventDefault();
    bootbox.confirm({
      title: getData('data-title', null),
      message: getData('data-message', 'Message'),
      buttons: {
        confirm: {
          label: getData('data-confirm-label', 'Confirmer'),
          className: getData('data-confirm-class', null)
        },
        cancel: {
          label: getData('data-cancel-label', 'Annuler'),
          className: getData('data-cancel-class', null)
        }
      },
      locale: 'fr',
      callback: function (result) {
        if (result) {
          submitValid = true; // Use global submit valid to keep trace of submit button used
          $this.trigger('click');
          return;
        }
        submitValid = false;
      }
    })
  });

  $(".admin_cp_ville_autocomplete").autocomplete({
    source: '/ajax/get_cp_ville_suggestion',
    dataType: "json",
    minLength: 4,
    appendTo: "#container_admin"
  });

  // Init summernote
  $('.summernote').summernote({
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['table', ['table']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['insert', ['link']],
    ],
  });
  $('.summernote-singleline').summernote({
    height: 25,
    minHeight: 25,
    maxHeight: 25,
    toolbar: [
      ['font', ['bold', 'underline', 'clear']],
    ]
  })


  // Form amap creation and edition
  function hideOnFieldChange(e) {
    if(e.target.value === "1") {
      $('.js-hide-association').hide();
    } else {
      $('.js-hide-association').show();
    }
  }
  $('.js-form-amap input[name="amap_edition[associationDeFait]"]').on('change', hideOnFieldChange);
  $('.js-form-amap input[name="amap_edition[associationDeFait]"][checked]').trigger('change');

  $('.js-form-amap input[name="amap_creation[associationDeFait]"]').on('change', hideOnFieldChange);
  $('.js-form-amap input[name="amap_creation[associationDeFait]"][checked]').trigger('change');

  // Init jquery mask
  $('.js-input-mask-phone').mask('00.00.00.00.00');

  // Init select 2
  $('.js-select2').select2();

  // Init flatpickr
  if(typeof flatpickr !== "undefined") {
    flatpickr(".flatpickr_range", {
      mode: "range",
      locale: "fr",
      dateFormat: "d/m/Y",
      // Auto submit on search form
      onClose: function(selectedDates, dateStr, instance) {
        const $parentSearchForm = $(instance.input).parents('.js-form-search');
        if($parentSearchForm.length === 0) {
          return;
        }

        $parentSearchForm.submit();
      },
    });
  }


  pageAmapDistributionAjout();
});
