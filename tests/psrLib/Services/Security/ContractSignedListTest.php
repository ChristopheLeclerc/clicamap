<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class ContractSignedListTest extends AbstractSecurityTest
{
    public function testAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_LIST));
    }

    public function testPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_LIST));
    }

    public function testFermeRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_LIST));
    }
}
