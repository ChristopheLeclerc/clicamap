<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\DTO\SearchContratViergeState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\UserWithFermes;

/**
 * @extends EntityRepository<ModeleContrat>
 */
class ModeleContratRepository extends EntityRepository
{
    /**
     * @return ModeleContrat[]
     */
    public function search(SearchContratViergeState $contratViergeState)
    {
        if (null === $contratViergeState->getAmap()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('ll.amap = :amap')
            ->setParameter('amap', $contratViergeState->getAmap())
            ->andWhere('mc.etat in (:states)')
            ->setParameter('states', [ModeleContrat::ETAT_CREATION, ModeleContrat::ETAT_VALIDATION_PAYSAN, ModeleContrat::ETAT_REFUS])
            ->andWhere('mc.version = 2')
        ;

        if (null !== $contratViergeState->getFerme()) {
            $qb
                ->andWhere('mc.ferme = :ferme')
                ->setParameter('ferme', $contratViergeState->getFerme())
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ModeleContrat[]
     */
    public function findToValidateForUserWithFermes(UserWithFermes $user, Amap $amap = null)
    {
        $qb = $this
            ->createQueryBuilder('mc')
            ->where('mc.ferme IN (:fermes)')
            ->setParameter('fermes', $user->getFermes()->toArray())
            ->andWhere('mc.etat = :etat')
            ->setParameter('etat', ModeleContrat::ETAT_VALIDATION_PAYSAN)
        ;

        if (null !== $amap) {
            $qb
                ->leftJoin('mc.livraisonLieu', 'll')
                ->andWhere('ll.amap = :amap')
                ->setParameter('amap', $amap)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ModeleContrat[]
     */
    public function findValidatedByFermeAmap(Ferme $ferme, Amap $amap)
    {
        return $this->findValidatedByFermesAmap([$ferme], $amap);
    }

    /**
     * @var Ferme[]
     *
     * @param mixed $fermes
     *
     * @return ModeleContrat[]
     */
    public function findValidatedByFermesAmap($fermes, Amap $amap)
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('mc.ferme IN (:fermes)')
            ->setParameter('fermes', $fermes)
            ->andWhere('mc.etat IN (:state)')
            ->setParameter('state', [ModeleContrat::ETAT_VALIDE, ModeleContrat::ETAT_ACTIF])
            ->orderBy('mc.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ModeleContrat[]
     */
    public function getSubscribableContractForAmapien(Amapien $amapien, bool $excludeSigned = true): array
    {
        $qb = $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->leftJoin('ll.amap', 'amap')
            ->where(':amapien MEMBER OF amap.amapiens')
            ->setParameter('amapien', $amapien)
            ->andWhere('mc.etat = :etat')
            ->setParameter('etat', ModeleContrat::ETAT_VALIDE)
        ;

        if ($excludeSigned) {
            $qb
                ->andWhere('mc NOT IN (SELECT umc FROM PsrLib\ORM\Entity\ModeleContrat umc LEFT JOIN PsrLib\ORM\Entity\Contrat c WITH c.modeleContrat = umc WHERE c.amapien = :amapien)')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ModeleContrat[]
     */
    public function getFromAmapDate(Amap $amap, string $date)
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.dates', 'dates')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('dates.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ModeleContrat[]
     */
    public function getFromFermeDate(Ferme $ferme, string $date): array
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.dates', 'dates')
            ->where('dates.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ModeleContrat[]
     */
    public function getFromAmapienDate(Amapien $amapien, string $date)
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.contrats', 'c')
            ->leftJoin('mc.dates', 'dates')
            ->where('dates.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('c.amapien = :amapien')
            ->setParameter('amapien', $amapien)
            ->getQuery()
            ->getResult()
    ;
    }
}
