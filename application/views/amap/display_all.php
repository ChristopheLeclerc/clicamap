<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap[] $amaps */
/** @var int $gps */
/** @var mixed $map */
/** @var \Symfony\Component\Form\FormView $searchForm */
?>

<script type="text/javascript">

  /*ALERTES*/
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 3000);

  });

</script>

<h3>Gestion des AMAP</h3>

<form method="GET" id="mdr" action="/amap" class="js-form-search">
    <div class="well">
        <div class="row">
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-btn">
                    <button class="btn btn-primary"
                            type="button">Région</button>
                    </span>
                    <?= render_form_select_widget($searchForm['region']); ?>

                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Département</button>
                    </span>
                    <?= render_form_select_widget($searchForm['departement']); ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Réseau</button>
                    </span>
                    <?= render_form_select_widget($searchForm['reseau']); ?>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <div class="input-group">

            <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Années d'adhésion</button>
            </span>
                    <?= render_form_select_widget($searchForm['adhesion']); ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">

            <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Type</button>
            </span>

                    <?= render_form_select_widget($searchForm['etudiante']); ?>

                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">

            <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Mot clé</button>
            </span>
                    <?= render_form_text_widget($searchForm['keyword']); ?>
                </div>
            </div>
        </div>


        <div class="row">

            <div class="form-group col-md-4" style="margin-top:5px; margin-bottom: 0;">
                <?= render_form_checkbox_widget($searchForm['carto']); ?>
	    </div>
	    <div class="form-group col-md-4" style="margin-top:5px; margin-bottom: 0;">
                <?= render_form_checkbox_widget($searchForm['insurance']); ?>
            </div>
        </div>

	<div class="row text-right">
	    <a href="<?= site_url('amap?reset_search'); ?>" class="btn btn-primary" style="margin-right:5px;"><i class="glyphicon glyphicon-refresh"></i> Réinitialiser les critères de recherche</a>
	    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Générer la liste</button>
	</div>
		    
    </div><!-- well -->

</form>


    <a href="<?= site_url('amap/informations_generales_creation'); ?>" class="btn btn-success"><i
                class="glyphicon glyphicon-plus"></i> Créer une nouvelle AMAP</a>

    <div class="dropdown d-inline-block">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuDownload" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Télécharger
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu mt-0" aria-labelledby="dropdownMenuDownload">
            <li><a href="<?= site_url('amap/telecharger_amap_synthese?'.http_build_query($this->input->get())); ?>">Télécharger la synthèse</a></li>
            <li><a href="<?= site_url('amap/telecharger_amap_complet?'.http_build_query($this->input->get())); ?>">Télécharger le fichier complet</a></li>
        </ul>
    </div>

    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_email" data-dismiss="modal"
       aria-hidden="true"><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>

    <?php if (null !== $map): ?>
        <?php if ($gps > 0): ?>
            <?= $map['js']; ?>
            <div class="alert alert-success"><?= $gps; ?> coordonnées GPS disponibles</div>
            <div id="map" style="width:100%; height:500px"></div>
            <br/>
        <?php else: ?>
            <div class="alert alert-success">Aucunes coordonnées GPS disponibles</div>
        <?php endif; ?>
    <?php endif; ?>

<!-- MODAL EMAIL -->
<div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à toutes les AMAP de cette liste
                    ?</h4>
            </div>
            <div class="modal-body">
                <h5>Pour envoyer un email à toutes ces AMAP , vous devez :</h5>
                <ul>
                    <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                        Copier sur la zone bleue ci dessous
                    </li>
                    <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
                    <li>Faire nouveau message</li>
                    <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                        Coller dans la liste des destinataires du message.
                    </li>
                </ul>

                <h5>Contact Public :</h5>
                <form>
                    <div class="form-group">
                        <?php
                        $emails = array_map(function (PsrLib\ORM\Entity\Amap $amap) {
                            return $amap->getEmail();
                        }, $amaps);
                        ?>
                            <textarea class="form-control" rows="5"><?=implode(PHP_EOL, $emails); ?></textarea>
                    </div>
                </form>

                <h5>Correspondant réseau :</h5>
                <form>
                    <div class="form-group">
                            <?php
                            $emails = array_map(function (PsrLib\ORM\Entity\Amap $amap) {
                                $ref = $amap->getAmapienRefReseau();
                                if (null === $ref) {
                                    return null;
                                }

                                return $ref->getEmail();
                            }, $amaps);
                            $emails = array_filter($emails, function (?string $input) {
                                return null !== $input;
                            });
                            ?>
                           <textarea class="form-control"
                                     rows="5"><?=implode(PHP_EOL, $emails); ?></textarea>
                    </div>
                </form>

                <h5>Second correspondant réseau :</h5>
                <form>
                    <div class="form-group">
                        <?php
                        $emails = array_map(function (PsrLib\ORM\Entity\Amap $amap) {
                            $ref = $amap->getAmapienRefReseauSecondaire();
                            if (null === $ref) {
                                return null;
                            }

                            return $ref->getEmail();
                        }, $amaps);
                        $emails = array_filter($emails, function (?string $input) {
                            return null !== $input;
                        });
                        ?>
                            <textarea class="form-control"
                                      rows="5"><?=implode(PHP_EOL, $emails); ?></textarea>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<?php
$nb_amap = count($amaps);
if (1 == $nb_amap) {
    echo '<div class="alert alert-success">1 résultat pour cette recherche';
} else {
    echo '<div class="alert alert-success">'.$nb_amap.' résultats pour cette recherche';
}
echo '</div>';
?>

<?php include 'display_all_table.php'; ?>
