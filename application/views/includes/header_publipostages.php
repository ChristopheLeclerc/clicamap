<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?php $this->load->view('includes/header_z_common_ressources'); ?>
    <style type="text/css">

        a:focus {
            outline: 0 !important
        }

        .entry:not(:first-of-type) {
            margin-top: 10px;
        }

        .glyphicon {
            font-size: 12px;
        }
    </style>

    <script type="text/javascript">

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });

      $(function () {
        $('.datepicker').datepicker();
      });

      function reg_check_all() {
        $("#regions input:checkbox").attr('checked', true);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function reg_uncheck_all() {
        $('#reg_clear').val(1);
        $('#dep_clear').val(1);
        $('#res_clear').val(1);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function dep_check_all() {
        $("#departements input:checkbox").attr('checked', true);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function dep_uncheck_all() {
        $('#dep_clear').val(1);
        $('#res_clear').val(1);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function res_check_all() {
        $("#reseaux input:checkbox").attr('checked', true);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function res_uncheck_all() {
        $('#res_clear').val(1);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function tp_check_all() {
        $("#tp_all input:checkbox").attr('checked', true);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function tp_uncheck_all() {
        $('#tp_clear').val(1);
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

    </script>
</head>
<body>
