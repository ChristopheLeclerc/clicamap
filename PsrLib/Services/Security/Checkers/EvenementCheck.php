<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Security\Checkers;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Repository\EvenementRepository;

class EvenementCheck extends AbstractCheck
{
    /**
     * @var EvenementRepository
     */
    private $evenementRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->evenementRepo = $em->getRepository(Evenement::class);
    }

    public function isGrantedEvenementCreate(): bool
    {
        // Les amaps peuvent créer des évènements
        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        return $this->getCurrentUser() instanceof Amapien
           && $this->getCurrentUser()->isAdmin()
           ;
    }

    public function isGrantedEvenementEdit(Evenement $evenement): bool
    {
        // Les amaps peuvent éditer leurs évènements
        if ($this->getCurrentUser() instanceof Amap) {
            $evenementAmap = $evenement->getAmap();

            return null !== $evenementAmap && $evenementAmap->getId() === $this->getCurrentUser()->getId();
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            // Le super admin peut éditer tous les évènements
            if ($this->getCurrentUser()->isSuperAdmin()) {
                return true;
            }

            // Les amapiens peuvent éditer leurs évènements
            if ($this->getCurrentUser()->isAdmin()) {
                return null !== $evenement->getAmapien()
                    && $evenement->getAmapien()->getId() === $this->getCurrentUser()->getId()
                ;
            }
        }

        return false;
    }

    public function isGrantedEvenementDisplay(Evenement $evenement): bool
    {
        // L'utilisateur doit etre connecté
        if (null === $this->getCurrentUser()) {
            return false;
        }

        // On peut afficher l'évènement si on peut l'éditer
        // Optimisation pour éviter des requetes dans les cas simples
        if ($this->isGrantedEvenementEdit($evenement)) {
            return true;
        }

        // On peut afficher l'évènement si il est dans la liste des évènements affichable...
        $evenements = $this
            ->evenementRepo
            ->getEvementForUser($this->getCurrentUser())
        ;
        // @var \PsrLib\ORM\Entity\Evenement $evenement
        foreach ($evenements['evenement_all'] as $evenementFromDb) {
            if ($evenement->getId() === $evenementFromDb->getId()) {
                return true;
            }
        }

        return false;
    }

    public function isGrantedEvenementList(): bool
    {
        // Tous les utilisateurs connectés
        return null !== $this->getCurrentUser();
    }
}
