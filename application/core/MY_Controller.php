<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use DI\Annotation\Inject;
use PsrLib\Services\Authentification;
use PsrLib\Services\PhpDiContrainerSingleton;
use PsrLib\Services\Uploader;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class AppController.
 */
class AppController extends CI_Controller
{
    /**
     * @var Appfunction
     */
    public $appfunction;

    /**
     * @var MY_Form_validation
     */
    public $form_validation;

    /**
     * @var CI_Session
     */
    public $session;

    /**
     * @var MY_Input
     */
    public $input;

    /**
     * @var Redirect
     */
    public $redirect;

    /**
     * @var CI_URI
     */
    public $uri;

    /**
     * @Inject
     *
     * @var Uploader
     */
    public $uploader;

    /**
     * @Inject
     *
     * @var Authentification
     */
    public $authentification;

    /**
     * @Inject
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * @Inject
     *
     * @var \Symfony\Component\Form\FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Security\SecurityChecker
     */
    protected $securitychecker;

    /**
     * @Inject
     *
     * @var \Twig\Environment
     */
    protected $twig;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var Di\Container
     */
    protected $container;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     *
     * @Inject
     */
    protected $sfSession;

    /**
     * AppController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->container = PhpDiContrainerSingleton::getContainer();

        // The framework doesn't let us control how the controller is created, so
        // we can't use the container to create the controller
        // So we ask the container to inject dependencies
        $this->container->injectOn($this);

        $this->load->library('appfunction');
        $this->load->library('form_validation');
        $this->load->library('Redirect');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        $this->request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    }

    /**
     * Get current connected user.
     */
    public function getUser(): ?PsrLib\ORM\Entity\BaseUser
    {
        return $this->authentification->getCurrentUser();
    }

    public function exit_error()
    {
        $this->sfSession->invalidate();
        $this->sfSession->getFlashBag()->set(
            'error_permission',
            'Vous n\'avez pas les accès suffisant pour consulter cette page, veuillez vous reconnecter.'
        );
        redirect('/');
    }

    /**
     * find existing entity on doctrine or exit.
     *
     * @param mixed $id
     *
     * @return object
     */
    public function findOrExit(string $class, $id)
    {
        if (null === $id) {
            $this->exit_error();
        }
        $elt = $this->em->find($class, $id);
        if (null === $elt) {
            $this->exit_error();
        }

        return $elt;
    }

    public function denyUnlessRequestMethod(string $method)
    {
        if ($this->request->getMethod() !== $method) {
            $this->exit_error();
        }
    }

    /**
     * @param string $action
     * @param $subject
     */
    public function denyAccessUnlessGranted($action, $subject = null)
    {
        $access = $this->securitychecker->isGranted($action, $subject);

        if (false === $access) {
            $this->exit_error();
        }
    }

    /**
     * @param string $action
     */
    public function denyAccessUnlessGrantedMultiple($action, array $subjects)
    {
        if (0 === count($subjects)) {
            $this->exit_error();
        }
        foreach ($subjects as $subject) {
            $access = $this->securitychecker->isGranted($action, $subject);

            if (false === $access) {
                $this->exit_error();
            }
        }
    }

    /**
     * Load given view with template.
     */
    protected function loadViewWithTemplate(string $view, array $data = [], string $template = 'template', array $templateRenderOptions = [])
    {
        $page = $this->load->view($view, $data, true);

        $options = array_merge(
            ['page' => $page],
            $templateRenderOptions
        );
        $this->load->view($template, $options);
    }

    /**
     * Redirect user to stored get param. Store in other case.
     */
    protected function redirectFromStoredGetParamIfExist(string $key)
    {
        $sessionKey = sprintf('stored_get_params_%s', $key);

        $getParams = $this->input->get();
        if (null === $getParams || 0 === count($getParams)) {
            if ($this->sfSession->has($sessionKey)) {
                // bind flash data
                foreach ($this->sfSession->getFlashBag()->all() as $flashKey => $flashMessage) {
                    $this->sfSession->getFlashBag()->set($flashKey, $flashMessage);
                }

                redirect(
                    sprintf(
                        '%s?%s',
                        $this->uri->uri_string(),
                        http_build_query($this->sfSession->get($sessionKey))
                    )
                );
            }

            return;
        }

        $this->sfSession->set($sessionKey, $getParams);
    }

    protected function twigDisplay(string $template, array $data = [])
    {
        $twigArgs = array_merge([
            'app' => [
                'currentUser' => $this->getUser(),
                'request' => $this->request,
                'siteName' => SITE_NAME,
            ],
        ], $data);

        // Render on variable to avoid header already send error
        $rendered = $this
            ->twig
            ->render($template, $twigArgs)
        ;
        echo $rendered;
    }

    /**
     * Add flash message on current session.
     *
     * @param string       $type
     * @param array|string $messages
     */
    protected function addFlash($type, $messages): void
    {
        $this->sfSession->getFlashBag()->set($type, $messages);
    }
}
