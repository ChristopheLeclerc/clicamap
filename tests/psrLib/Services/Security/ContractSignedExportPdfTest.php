<?php

namespace Test\Services\Security;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Files\ContratPdf;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class ContractSignedExportPdfTest extends AbstractSecurityTest
{
    /**
     * @dataProvider exportPdfProvider
     */
    public function testExportPdf(int $version, bool $withPdf, bool $mcPermission, bool $result)
    {
        $contract = new Contrat();
        $mc = new ModeleContrat();
        $mc->setVersion($version);

        $contract->setModeleContrat($mc);
        if ($withPdf) {
            $contract->setPdf($this->prophesize(ContratPdf::class)->reveal());
        }

        $this->securityChecker->mockAction(SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $mcPermission);

        self::assertSame($result, $this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_EXPORT_PDF, $contract));
    }

    public function exportPdfProvider()
    {
        return [
            [1, false, false, false],
            [2, false, false, false],
            [1, true, true, false],
            [2, true, false, false],
            [2, false, true, false],
            [2, true, true, true],
        ];
    }
}
