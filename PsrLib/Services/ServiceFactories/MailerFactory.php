<?php

namespace PsrLib\Services\ServiceFactories;

use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mailer\Transport\Dsn;

class MailerFactory
{
    public function create(): MailerInterface
    {
        $dsn = new Dsn(
            getenv('PROTOCOL'),
            getenv('SMTP_HOST'),
            getenv('SMTP_USER'),
            getenv('SMTP_PASS'),
            (int) getenv('SMTP_PORT')
        );

        $transportFactory = new Transport([new Transport\Smtp\EsmtpTransportFactory()]);
        $transport = $transportFactory->fromDsnObject($dsn);

        return new Mailer($transport);
    }
}
