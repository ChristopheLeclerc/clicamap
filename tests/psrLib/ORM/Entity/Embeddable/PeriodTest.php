<?php

namespace Test\ORM\Entity\Embeddable;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class PeriodTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testCarbonPeriodNull()
    {
        $period = new Period();
        self::assertNull($period->getCarbonPeriod());

        $period->setStartAt(new Carbon());
        self::assertNull($period->getCarbonPeriod());

        $period = new Period();
        $period->setEndAt(new Carbon());
        self::assertNull($period->getCarbonPeriod());
    }

    public function testCarbonPeriodValid()
    {
        $start = Carbon::create(1900, 01, 01);
        $end = Carbon::create(1900, 01, 20);
        $period = new Period();
        $period
            ->setStartAt($start)
            ->setEndAt($end)
        ;
        $carbonPeriod = $period->getCarbonPeriod();
        self::assertTrue($start->equalTo($carbonPeriod->getStartDate()));
        self::assertTrue($end->equalTo($carbonPeriod->getEndDate()));

        $violations = $this->validator->validate($period);
        self::assertCount(0, $violations);
    }

    public function testDateEquals()
    {
        $start = Carbon::create(1900, 01, 01);
        $end = Carbon::create(1900, 01, 01);
        $period = new Period();
        $period
            ->setStartAt($start)
            ->setEndAt($end)
        ;

        /** @var ConstraintViolation[] $violations */
        $violations = $this->validator->validate($period);
        self::assertEmpty($violations);
    }

    public function testDateEndBeforeStart()
    {
        $start = Carbon::create(1900, 01, 10);
        $end = Carbon::create(1900, 01, 01);
        $period = new Period();
        $period
            ->setStartAt($start)
            ->setEndAt($end)
        ;

        /** @var ConstraintViolation[] $violations */
        $violations = $this->validator->validate($period);
        self::assertCount(1, $violations);
        self::assertSame('La date de fin doit être après la date de debut.', $violations[0]->getMessageTemplate());
    }
}
