<?php

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use PsrLib\Services\PhpDiContrainerSingleton;

/**
 * @template T
 */
abstract class ServiceEntityRepository extends EntityRepository
{
    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);

        // Self inject service as we do not control repository instanciation by doctrine
        $di = PhpDiContrainerSingleton::getContainer();
        $di->injectOn($this);
    }

    /**
     * Refresh sql connection.
     * Use on long-running tasks before request to avoid "MySQL server has gone away" errors.
     *
     * @ref https://stackoverflow.com/a/32648896
     */
    public function refreshConnection(): void
    {
        $em = $this->getEntityManager();
        if (false === $em->getConnection()->ping()) {
            $em->getConnection()->close();
            $em->getConnection()->connect();
        }
    }
}
