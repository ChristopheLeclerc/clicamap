<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use PsrLib\ORM\Entity\Document;

class DocumentPermissionFormatter
{
    /**
     * Concatene toutes les permissions dans une chaine de caractere.
     */
    public function formatPermission(Document $document): string
    {
        $res = [];
        if ($document->isPermissionAnonyme()) {
            $res[] = 'Accueil';
        }
        if ($document->isPermissionAdmin()) {
            $res[] = 'Administrateur réseau';
        }
        if ($document->isPermissionPaysan()) {
            $res[] = 'Paysan';
        }
        if ($document->isPermissionAmap()) {
            $res[] = 'AMAP';
        }
        if ($document->isPermissionAmapienref()) {
            $res[] = 'AMAPien référent';
        }
        if ($document->isPermissionAmapien()) {
            $res[] = 'AMAPien';
        }

        return implode(',', $res);
    }
}
