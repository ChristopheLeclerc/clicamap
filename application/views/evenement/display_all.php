<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
$currentUser = get_current_connected_user();

if (null !== $currentUser) {
    echo '<div class="alert alert-success">Bonjour';

    // INFO AMAPIEN
    if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien || $currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
        echo ' <strong>'.ucfirst(strtolower($currentUser->getPrenom())).' '.mb_strtoupper($currentUser->getNom()).'</strong>';
    }

    echo ', bienvenue sur <strong>'.SITE_NAME.' !</strong> Vous êtes actuellement connecté en tant ';

    // SUPER ADMIN
    if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isSuperAdmin()) {
        echo 'que <strong>Super Administrateur</strong>. Vous avez tous les droits.';
    } // ADMINISTRATEURS RÉSEAUX
    elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdminReseaux()) {
        echo 'qu\'<strong>Administrateur Réseau</strong>. Vous avez tous les droits sur la région : <strong>';

        echo $currentUser->getAdminRegions()->first();

        echo '</strong>.';
    } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdminDepartment()) {
        echo 'qu\'<strong>Administrateur Réseau</strong>. Vous avez tous les droits sur le(s) département(s) : <strong>';

        echo implode(' ; ', $currentUser->getAdminDepartments()->toArray());

        echo '</strong>.';
    } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdminReseaux()) {
        echo 'qu\'<strong>Administrateur Réseau</strong>. Vous avez tous les droits sur le(s) réseau(x) : <strong>';

        echo implode(' ; ', $currentUser->getAdminReseaux()->toArray());

        echo '</strong>.';
    } // GESTIONNAIRE
    elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
        echo 'qu\'<strong>AMAP</strong> : <strong>';

        echo $currentUser;

        echo '</strong>.';
    } // RÉFÉRENT PRODUIT AVEC N FERME
    elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
        echo 'que <strong>Référent produit</strong> auprès de : <strong>';

        echo implode(' ; ', $currentUser->getRefProdFermes()->toArray()).'.';
        echo '</strong>';
    } // INFOS AMAPIEN AVEC AMAP
    elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->hasAmap()) {
        echo 'qu\'<strong>amapien</strong> de : <strong>';

        echo $currentUser->getAmap();

        echo '</strong>.';
    } // INFOS AMAPIEN SANS AMAP
    elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && !$currentUser->hasAmap()) {
        echo 'qu\'utilisateur sans AMAP ni privilège.';
    } // INFOS PAYSAN
    elseif ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
        echo 'que <strong>paysan</strong> de la ferme <strong>';

        echo $currentUser->getFerme();

        echo '</strong>.';
    } elseif ($currentUser instanceof \PsrLib\ORM\Entity\FermeRegroupement) {
        echo 'que <strong>regroupement</strong>';
    }

    echo '</div>';
}

echo '<h3>Les événements</h3>';

if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_CREATE)) {
    echo '<p><a href="'.site_url('evenement/add_1').'" class="btn btn-success" style="margin-right:5px;"><i class="glyphicon glyphicon-plus"></i> Créer un nouvel événement</a></p>';
}

if ($evenement_all) {
    ?>

<div class="table-responsive">
    <table id="event" class="table table-hover table-datatable" data-order='[[ 2, "desc" ]]' data-paging="false" data-info="false">
        <thead class="thead-inverse">
        <tr>
            <th>Nom</th>
            <th>Créateur</th>
            <th>Date et heure de début</th>
            <th>Date et heure de fin</th>
            <th class="text-right">Outils</th>
        </tr>
        </thead>

        <?php

        echo '<tbody>';

    /** @var \PsrLib\ORM\Entity\Evenement $ev */
    foreach ($evenement_all as $ev) {
        echo '<tr>';
        echo '<th scope="row">';

        echo '<a href="'.site_url('evenement/display/'.$ev->getId()).'">';
        if (strlen($ev->getNom()) > 65) {
            echo substr($ev->getNom(), 0, 65).'...';
        } else {
            echo $ev->getNom();
        }
        echo '</a>';
        echo '</th>';

        echo '<td>';

        foreach ($createur as $c) {
            if ($c[0] == $ev->getId()) {
                if ('amap' == $c[1]) {
                    echo 'AMAP ('.$c[2].')';
                } elseif ('adm_reg' == $c[1]) {
                    echo 'Administrateur région';
                } elseif ('adm_dep' == $c[1]) {
                    echo 'Administrateur département';
                } elseif ('adm_res' == $c[1]) {
                    echo 'Administrateur réseau';
                } elseif ('sup_adm' == $c[1]) {
                    echo 'Super administrateur';
                } else {
                    echo 'Référent produit';
                }
            }
        }

        echo '</td>';

        if ($ev->getDateDeb()) {
            echo '<td data-order="'.$ev->getTimestampDeb().'">';
            echo date_format_french($ev->getDateDeb()).'&nbsp;'.$ev->getHorDeb();
        } else {
            echo '<td data-order="0">';
        }
        echo '</td>';

        if ($ev->getDateFin()) {
            echo '<td data-order="'.$ev->getTimestampFin().'">';
            echo date_format_french($ev->getDateFin()).'&nbsp;'.$ev->getHorFin();
        } else {
            echo '<td data-order="0">';
        }
        echo '</td>';

        echo '<td class="text-right">';

        echo '<a title="Détails de l\'événement" href="'.site_url('evenement/display/'.$ev->getId()).'"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="Détails de l\'événement"></i></a>&nbsp;';

        if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_EVENEMENT_EDIT, $ev)) {
            echo '<a title="Modifier l\'événement" href="#" data-toggle="modal" data-target="#modal_edit_'.$ev->getId().'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier l\'événement"></i></a>&nbsp;';
            echo '<a title="Supprimer l\'événement" href="#" data-toggle="modal" data-target="#modal_remove_'.$ev->getId().'"><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer l\'événement"></i></a>&nbsp;';
        } ?>

            <!-- Modal edit -->
            <div class="modal fade text-left" id="modal_edit_<?= $ev->getId(); ?>" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h5 class="modal-title" id="myModalLabel">Modifier l'événement
                                "<?= ucfirst($ev->getNom()); ?>" ?</h5>
                        </div>
                        <div class="modal-body">
                            <p class="text-right">
                                <?php
                                echo '<p><a href="'.site_url('evenement/add_1/'.$ev->getId()).'"><i class="glyphicon glyphicon-info-sign"></i> Qui informer ?</a></p>';
        echo '<p><a href="'.site_url('evenement/add_2/'.$ev->getId()).'"><i class="glyphicon glyphicon-info-sign"></i> Les informations générales</a></p>'; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal remove -->
            <div class="modal fade text-left" id="modal_remove_<?= $ev->getId(); ?>" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer l'événement
                                "<?= strtoupper($ev->getNom()); ?>" ?</h5>
                        </div>
                        <div class="modal-body">
                            <p class="text-right">
                                <a href="<?= site_url('evenement/remove/'.$ev->getId()); ?>"
                                   class="btn btn-danger btn-sm">OUI</a>
                                <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            </td>
            </tr>

            <?php
    }

    echo '</tbody>';
    echo '</table>';
    echo '</div>';
} else {
    echo '<div class="alert alert-warning">Aucun événement n\'est présent dans votre agenda.</div>';
}

        ?>
