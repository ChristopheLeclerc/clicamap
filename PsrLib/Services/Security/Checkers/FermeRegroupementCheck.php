<?php

namespace PsrLib\Services\Security\Checkers;

use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\FermeRegroupement;

class FermeRegroupementCheck extends AbstractCheck
{
    public function isGrantedList(): bool
    {
        $currentUser = $this->getCurrentUser();

        return $currentUser instanceof Amapien
            && ($currentUser->isAdminRegion() || $currentUser->isSuperAdmin())
        ;
    }

    public function isGrantedProfil(): bool
    {
        return $this->getCurrentUser() instanceof FermeRegroupement;
    }
}
