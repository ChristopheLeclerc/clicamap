<?php

namespace PsrLib\Services\EntityBuilder;

use PsrLib\DTO\AmapDistributionDetailMassEdit;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;

class AmapDistributionDetailMassEditDtoBuilder
{
    /**
     * Build distribution detail with common attributes from multiple distributions.
     *
     * @param AmapDistribution[] $distributions
     */
    public function buildFromMultipleDistributions(array $distributions): AmapDistributionDetailMassEdit
    {
        if (0 === count($distributions)) {
            throw new \LogicException('Need at least one distirbution');
        }

        $outDetail = new AmapDistributionDetail();
        $outDetail
            ->setTache($distributions[0]->getDetail()->getTache())
            ->setNbPersonnes($distributions[0]->getDetail()->getNbPersonnes())
            ->setHeureDebut(clone $distributions[0]->getDetail()->getHeureDebut())
            ->setHeureFin(clone $distributions[0]->getDetail()->getHeureFin())
        ;
        $outLL = $distributions[0]->getAmapLivraisonLieu();
        foreach ($distributions as $distribution) {
            $detail = $distribution->getDetail();
            if (null !== $outDetail->getTache() && $outDetail->getTache() !== $detail->getTache()) {
                $outDetail->setTache(null);
            }
            if (null !== $outDetail->getNbPersonnes() && $outDetail->getNbPersonnes() !== $detail->getNbPersonnes()) {
                $outDetail->setNbPersonnes(null);
            }
            if (null !== $outDetail->getHeureDebut() && $outDetail->getHeureDebut() != $detail->getHeureDebut()) {
                $outDetail->setHeureDebut(null);
            }
            if (null !== $outDetail->getHeureFin() && $outDetail->getHeureFin() != $detail->getHeureFin()) {
                $outDetail->setHeureFin(null);
            }
            if (null !== $outLL && $distribution->getAmapLivraisonLieu() !== $outLL) {
                $outLL = null;
            }
        }

        return new AmapDistributionDetailMassEdit($outDetail, $outLL);
    }
}
