<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Embeddable()
 */
class AmapienAdhesionInfo
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var null|string
     * @Assert\NotBlank(groups={"adhesion_info"})
     * @Assert\Length(max="255",groups={"adhesion_info"})
     */
    private $nomReseau;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="14", max="14", exactMessage="Le SIRET doit faire exactement 14 caractères.", groups={"adhesion_info"})
     * @Assert\Luhn(message="Le SIRET est invalide.", groups={"adhesion_info"})
     */
    private $siret;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="~^W\d{9}$~", message="Le numéro RNA doit être composé de W suivi de 9 chiffres.", groups={"adhesion_info"})
     */
    private $rna;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"adhesion_info"})
     */
    private $villeSignature;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $site;

    public function getNomReseau(): ?string
    {
        return $this->nomReseau;
    }

    public function setNomReseau(?string $nomReseau): AmapienAdhesionInfo
    {
        $this->nomReseau = $nomReseau;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): AmapienAdhesionInfo
    {
        $this->siret = $siret;

        return $this;
    }

    public function getRna(): ?string
    {
        return $this->rna;
    }

    public function setRna(?string $rna): AmapienAdhesionInfo
    {
        $this->rna = $rna;

        return $this;
    }

    public function getVilleSignature(): ?string
    {
        return $this->villeSignature;
    }

    public function setVilleSignature(?string $villeSignature): AmapienAdhesionInfo
    {
        $this->villeSignature = $villeSignature;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(?string $site): AmapienAdhesionInfo
    {
        $this->site = $site;

        return $this;
    }
}
