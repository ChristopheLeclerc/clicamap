<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Produit.
 */
class Produit extends AppController
{
    /**
     * @var Excel
     */
    public $excel;
    protected $data;

    /**
     * Produit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['create' => null,
            'edit' => null,
            'remove' => null,
            'ferme' => [],
            'produit' => [],
            'produit_all' => [], ];

        $this->load->library('excel');
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    /**
     * Afficher les produits d'une ferme sélectionnée.
     *
     * @param null $ferme_id
     */
    public function accueil($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $this->loadViewWithTemplate('produit/display_all', [
            'ferme' => $ferme,
        ]);
    }

    /**
     * Créer / éditer un produit.
     *
     * @param null  $produit_id
     * @param mixed $ferme_id
     */
    public function informations($ferme_id, $produit_id = null)
    {
        if (null === $produit_id) {
            /** @var \PsrLib\ORM\Entity\Ferme $ferme */
            $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
            $produit = new \PsrLib\ORM\Entity\FermeProduit();
            $produit->setFerme($ferme);
        } else {
            /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
            $produit = $this->findOrExit(\PsrLib\ORM\Entity\FermeProduit::class, $produit_id);
            $ferme = $produit->getFerme();
        }

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $form = $this->formFactory->create(\PsrLib\Form\FermeProduitType::class, $produit);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (null === $produit->getId()) {
                $this->em->persist($produit);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Produit modifié avec succès'
            );

            redirect('/produit/accueil/'.$ferme->getId());
        }

        $this->loadViewWithTemplate('produit/informations', [
            'ferme' => $ferme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Télécharger les produits d'une ferme.
     *
     * @param mixed $ferme_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function download($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        // Onglet 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Produits');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => ['horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT], ];

        $this->excel->getActiveSheet()->setCellValue('A3', 'Type de production');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Conditionnement');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Prix');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Régularisation de poids ?');

        $this->excel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);

        $i = 4;
        /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
        foreach ($ferme->getProduits() as $produit) {
            $this->excel->getActiveSheet()->setCellValue('A'.$i, ucfirst($produit->getTypeProduction()->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($produit->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, $produit->getConditionnement());
            $this->excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('D'.$i, $produit->getPrix());
            $this->excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('E'.$i, $produit->isRegulPds() ? 'Oui' : 'Non');
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A1', $time);

        $nom_fichier = $this->appfunction->caracteres_speciaux($ferme->getNom()).'_produits.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * Supprimer un produit.
     *
     * @param $produit_id
     */
    public function remove($produit_id)
    {
        /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
        $produit = $this->findOrExit(\PsrLib\ORM\Entity\FermeProduit::class, $produit_id);
        $ferme = $produit->getFerme();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_PRODUCT_DELETE, $ferme);

        $this->em->remove($produit);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Produit supprimé avec succès'
        );

        redirect('/produit/accueil/'.$ferme->getId());
    }
}
