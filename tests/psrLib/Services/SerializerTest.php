<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Services;

use Carbon\Carbon;
use Money\Money;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\AdhesionValueAmapAmapien;
use PsrLib\ORM\Entity\AdhesionValueAmapien;
use PsrLib\ORM\Entity\AdhesionValueFerme;
use Symfony\Component\Serializer\SerializerInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class SerializerTest extends ContainerAwareTestCase
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function setUp()
    {
        parent::setUp();
        $this->serializer = self::$container->get(SerializerInterface::class);
    }

    public function testSupportDecoding()
    {
        self::assertTrue($this->serializer->supportsDecoding('xls'));
    }

    public function testDecodeXls()
    {
        $decoded = $this->serializer->decode(__DIR__.'/../../data/SerializerTest/data1.xls', 'xls');
        self::assertSame([
            [
                'Header 1' => 'Data 1',
                'Header 2' => 'Data 2',
            ], [
                'Header 1' => '02/01/1900',
                'Header 2' => '5.5',
            ],
        ], $decoded);
    }

    public function testDeserializeImportAdherentAmap()
    {
        /** @var AdhesionValueAmap[] $deserialized */
        $deserialized = $this->serializer->deserialize(
            __DIR__.'/../../data/SerializerTest/Import-adhérent_AMAP_v3.xls',
            AdhesionValueAmap::class.'[]',
            'xls',
            ['disable_type_enforcement' => true]
        );

        self::assertCount(2, $deserialized);

        self::assertSame(2020, $deserialized[0]->getYear());
        self::assertSame('14A0103', $deserialized[0]->getVoucherNumber());
        self::assertSame('Nom de l’AMAP 1', $deserialized[0]->getPayer());
        self::assertTrue(Money::EUR(500)->equals($deserialized[0]->getAmount()));
        self::assertTrue(Money::EUR(100)->equals($deserialized[0]->getAmountMembership()));
        self::assertTrue(Money::EUR(200)->equals($deserialized[0]->getAmountDonation()));
        self::assertTrue(Money::EUR(300)->equals($deserialized[0]->getAmountInsurance()));
        self::assertTrue((new Carbon('2014-03-14'))->isSameAs('Y-m-d', $deserialized[0]->getPaymentDate()));
        self::assertSame('chèque', $deserialized[0]->getPaymentType());
        self::assertTrue((new Carbon('2014-03-15'))->isSameAs('Y-m-d', $deserialized[0]->getProcessingDate()));
        self::assertNull($deserialized[0]->getFreeField1());
        self::assertSame('free2', $deserialized[0]->getFreeField2());
        self::assertSame('123', $deserialized[0]->getAmapId());

        self::assertSame(2020, $deserialized[1]->getYear());
        self::assertSame('14A0104', $deserialized[1]->getVoucherNumber());
        self::assertSame('Nom de l’AMAP 2', $deserialized[1]->getPayer());
        self::assertTrue(Money::EUR(512)->equals($deserialized[1]->getAmount()));
        self::assertTrue(Money::EUR(400)->equals($deserialized[1]->getAmountMembership()));
        self::assertTrue(Money::EUR(500)->equals($deserialized[1]->getAmountDonation()));
        self::assertTrue(Money::EUR(600)->equals($deserialized[1]->getAmountInsurance()));
        self::assertTrue((new Carbon('2014-04-22'))->isSameAs('Y-m-d', $deserialized[1]->getPaymentDate()));
        self::assertSame('chèque', $deserialized[1]->getPaymentType());
        self::assertTrue((new Carbon('2014-03-15'))->isSameAs('Y-m-d', $deserialized[1]->getProcessingDate()));
        self::assertNull($deserialized[1]->getFreeField1());
        self::assertNull($deserialized[1]->getFreeField2());
        self::assertSame('123', $deserialized[1]->getAmapId());
    }

    public function testDeserializeImportAdherentFerme()
    {
        /** @var AdhesionValueFerme[] $deserialized */
        $deserialized = $this->serializer->deserialize(
            __DIR__.'/../../data/SerializerTest/Import-adhérent_Paysan_V3.xls',
            AdhesionValueFerme::class.'[]',
            'xls',
            ['disable_type_enforcement' => true]
        );

        self::assertCount(1, $deserialized);

        self::assertSame(2020, $deserialized[0]->getYear());
        self::assertSame('14P0101', $deserialized[0]->getVoucherNumber());
        self::assertSame('Nom Prénom', $deserialized[0]->getPayer());
        self::assertTrue(Money::EUR(1000)->equals($deserialized[0]->getAmount()));
        self::assertTrue(Money::EUR(1000)->equals($deserialized[0]->getAmountMembership()));
        self::assertNull($deserialized[0]->getAmountDonation());
        self::assertNull($deserialized[0]->getAmountInsurance());
        self::assertTrue((new Carbon('2014-02-18'))->isSameAs('Y-m-d', $deserialized[0]->getPaymentDate()));
        self::assertSame('Chèque', $deserialized[0]->getPaymentType());
        self::assertTrue((new Carbon('2014-02-19'))->isSameAs('Y-m-d', $deserialized[0]->getProcessingDate()));
        self::assertNull($deserialized[0]->getFreeField1());
        self::assertSame('free2', $deserialized[0]->getFreeField2());
        self::assertSame('123', $deserialized[0]->getSiret());
    }

    public function testDeserializeImportAdherentAmapien()
    {
        /** @var AdhesionValueAmapien[] $deserialized */
        $deserialized = $this->serializer->deserialize(
            __DIR__.'/../../data/SerializerTest/Import-adhérent_AMAPien_v3.xls',
            AdhesionValueAmapien::class.'[]',
            'xls',
            ['disable_type_enforcement' => true]
        );

        self::assertCount(1, $deserialized);

        self::assertSame(2020, $deserialized[0]->getYear());
        self::assertSame('14P0101', $deserialized[0]->getVoucherNumber());
        self::assertSame('Nom Prénom', $deserialized[0]->getPayer());
        self::assertTrue(Money::EUR(1000)->equals($deserialized[0]->getAmount()));
        self::assertTrue(Money::EUR(1100)->equals($deserialized[0]->getAmountMembership()));
        self::assertTrue(Money::EUR(1200)->equals($deserialized[0]->getAmountDonation()));
        self::assertNull($deserialized[0]->getAmountInsurance());
        self::assertTrue((new Carbon('2014-02-18'))->isSameAs('Y-m-d', $deserialized[0]->getPaymentDate()));
        self::assertSame('Chèque', $deserialized[0]->getPaymentType());
        self::assertTrue((new Carbon('2014-02-19'))->isSameAs('Y-m-d', $deserialized[0]->getProcessingDate()));
        self::assertNull($deserialized[0]->getFreeField1());
        self::assertSame('free2', $deserialized[0]->getFreeField2());
        self::assertSame('amapien@test.amap-aura.org', $deserialized[0]->getEmailAmapien());
    }

    public function testDeserializeImportAdherentAmapAmapien()
    {
        /** @var AdhesionValueAmapAmapien[] $deserialized */
        $deserialized = $this->serializer->deserialize(
            __DIR__.'/../../data/SerializerTest/Import_adhesion_amap_amapien.xls',
            AdhesionValueAmapAmapien::class.'[]',
            'xls',
            ['disable_type_enforcement' => true]
        );

        self::assertCount(1, $deserialized);

        self::assertSame(2020, $deserialized[0]->getYear());
        self::assertSame('14A0103', $deserialized[0]->getVoucherNumber());
        self::assertSame('Nom de l’AMAPien', $deserialized[0]->getPayer());
        self::assertTrue(Money::EUR(1000)->equals($deserialized[0]->getAmount()));
        self::assertTrue(Money::EUR(1100)->equals($deserialized[0]->getAmountMembership()));
        self::assertTrue(Money::EUR(1200)->equals($deserialized[0]->getAmountDonation()));
        self::assertTrue((new Carbon('2021-01-21'))->isSameAs('Y-m-d', $deserialized[0]->getPaymentDate()));
        self::assertSame('bank', $deserialized[0]->getBank());
        self::assertSame('chèque', $deserialized[0]->getPaymentType());
        self::assertTrue((new Carbon('2021-01-22'))->isSameAs('Y-m-d', $deserialized[0]->getProcessingDate()));
        self::assertNull($deserialized[0]->getFreeField1());
        self::assertSame('free2', $deserialized[0]->getFreeField2());
        self::assertSame('amapien@test.amap-aura.org', $deserialized[0]->getEmailAmapien());
    }
}
