<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Adhesion $adhesion */
$value = $adhesion->getValue();

$creator = $adhesion->getCreator();

$logo = null;
if ($creator instanceof \PsrLib\ORM\Entity\Amapien && $creator->isAdminRegion()) {
    $logo = $creator->getAdminRegions()->first()->getLogo();
}

if ($creator instanceof \PsrLib\ORM\Entity\Amapien && $creator->isAdminDepartment()) {
    $logo = $creator->getAdminDepartments()->first()->getLogo();
}

if ($creator instanceof \PsrLib\ORM\Entity\Amapien) {
    if (null === $creator->getVille()) {
        throw new \PsrLib\Exception\AdhesionGenerationCreatorNoCityExecption();
    }
    $adresse = sprintf(
        '%s %s %s, %s',
        $creator->getLibAdr1(),
        $creator->getLibAdr1Complement(),
        $creator->getVille()->getCpString(),
        $creator->getVille()->getNom()
    );
    $site = $creator->getAdhesionInfo()->getSite();
    $nom = $creator->getAdhesionInfo()->getNomReseau();
    $telephone = $creator->getNumTel1();
    $siret = $creator->getAdhesionInfo()->getSiret();
    $rna = $creator->getAdhesionInfo()->getRna();
    $ville = $creator->getAdhesionInfo()->getVilleSignature();
} elseif ($creator instanceof \PsrLib\ORM\Entity\Amap) {
    $adresse = '';
    $ville = '';
    if (null !== $creator->getAdresseAdminVille()) {
        $adresse = sprintf(
            '%s %s, %s',
            $creator->getAdresseAdmin(),
            $creator->getAdresseAdminVille()->getCpString(),
            $creator->getAdresseAdminVille()->getNom()
        );
        $ville = $creator->getAdresseAdminVille()->getNom();
    }

    $site = $creator->getUrl();
    $nom = $creator->getNom();
    $telephone = '';
    $siret = $creator->getSiret();
    $rna = $creator->getRna();
} else {
    throw new \LogicException('Unsupported user');
}

?>

<html>
    <head>
        <style>
            @page {
                sheet-size: A4;
                margin-top: 25mm;
            }

            h1 {
                font-size: 1.5em;
                border: 2px solid black;
                text-align: center;
            }

            p.margin-left {
                margin-left: 3em;
            }

            p.right {
                text-align: right;
            }

            p {
                margin-top: 0;
                margin-bottom: 1em;
            }

            p.footer {
                border: 2px solid black;
                text-align: center;
                padding: 2em;
                margin-top: 1em;
            }

        </style>

    </head>

    <body>
        <?php if (null !== $logo): ?>
            <img
                src="<?=$logo->getFileFullPath(); ?>"
                style="width: 40mm; height: 40mm"
            />
        <?php endif; ?>
        <table style="width: 100%; margin-top: 1em; margin-bottom: 1em" >
            <tr>
                <td style="width: 49%; vertical-align: top">
                    <?= $nom; ?><br />
                    <?= $adresse; ?><br />
                    <?= $site; ?>
                </td>
                <td style="width: 49%; vertical-align: top; text-align: right">
                    <?= $adhesion->getCreator()->getEmail(); ?> <br />
                    <?= $telephone; ?>
                </td>
            </tr>
        </table>


        <p class="right">Numéro de reçu : <?=$value->getVoucherNumber(); ?></p>

        <h1>RECU <?=$value->getYear(); ?> </h1>

        <p>L'association <b><?= $nom; ?></b> atteste avoir reçue</p>
        <p class="margin-left">la somme de <?=money_format_french($value->getAmount()); ?> €</p>
        <?php
        $payer = $adhesion->getValue()->getPayer();
        ?>
        <?php if ($adhesion instanceof \PsrLib\ORM\Entity\AdhesionAmap): ?>
            <p class="margin-left">de l'Amap <b><?=$payer; ?></b></p>
        <?php endif; ?>
        <?php if ($adhesion instanceof \PsrLib\ORM\Entity\AdhesionAmapien): ?>
            <p class="margin-left">de l'Amapien <b><?=$payer; ?></b></p>
        <?php endif; ?>
        <?php if ($adhesion instanceof \PsrLib\ORM\Entity\AdhesionFerme): ?>
            <p class="margin-left">de la ferme <b><?=$payer; ?></b></p>
        <?php endif; ?>
        <?php if ($adhesion instanceof \PsrLib\ORM\Entity\AdhesionAmapAmapien): ?>
            <p class="margin-left">de l'Amapien <b><?=$payer; ?></b></p>
        <?php endif; ?>
        <p class="margin-left">
            par <?=$value->getPaymentType(); ?>
        </p>
        <p class="margin-left">en date du <?=carbon_format_french($value->getPaymentDate()); ?></p>

        <?php if (null !== $value->getAmountMembership() && !$value->getAmount()->equals($value->getAmountMembership())): ?>
            <p>composée de :</p>
        <?php endif; ?>

        <?php if (null !== $value->getAmountDonation() && $value->getAmountDonation()->isPositive()): ?>
            <p class="margin-left">la somme de <?=money_format_french($value->getAmountDonation()); ?> € au titre de DON de l'année <?=$value->getYear(); ?></p>
        <?php endif; ?>
        <?php if (null !== $value->getAmountInsurance() && $value->getAmountInsurance()->isPositive()): ?>
            <p class="margin-left">la somme de <?=money_format_french($value->getAmountInsurance()); ?> € au titre des frais d'assurance de l'année <?=$value->getYear(); ?></p>
        <?php endif; ?>

        <?php if (null !== $value->getAmountMembership() && $value->getAmountMembership()->isPositive()): ?>
            <p class="margin-left">
                <?php if (!$value->getAmount()->equals($value->getAmountMembership())): ?>
                    la somme de <?=money_format_french($value->getAmountMembership()); ?> €
                <?php endif; ?>
                au titre de cotisation de l'année <?=$value->getYear(); ?>
            &nbsp;<b>Cette cotisation confirme la qualité de membre et ouvre aux droits de vote aux Assemblées Générales.</b></p>
        <?php endif; ?>

        <p class="separator">&nbsp;</p>
        <p><?=$value->getFreeField1(); ?></p>
        <p><?=$value->getFreeField2(); ?></p>

        <p class="separator">&nbsp;</p>

        <p class="right">
            Fait à <?= $ville; ?> <br />
            le <?= carbon_format_french($value->getProcessingDate()); ?>
        </p>

        <p class="footer">
            Association soumise à la loi du 1er juillet 1901 et au décret du 16 août 1901 <br />
            <?= !empty($siret) ? "N° de SIRET : {$siret}" : ''; ?> <?= !empty($rna) ? "- N° déclaration en préfecture : {$rna}" : ''; ?>
        </p>
    </body>
</html>
