<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

// HTML -> HEAD
if (isset($header_portail)) {
    $this->load->view('includes/header_portail.php');
} elseif (isset($header_dates)) {
    $this->load->view('includes/header_dates.php');
} elseif (isset($header_publipostages)) {
    $this->load->view('includes/header_publipostages.php');
} elseif (isset($header_evenements)) {
    $this->load->view('includes/header_evenements.php');
} elseif (isset($header_recherche_amap)) {
    $this->load->view('includes/header_recherche_amap.php');
} else {
    $this->load->view('includes/header.php');
}

echo '<div class="container">';

// ENTÊTE ; BANNIÈRE
echo '<header>';
if (isset($menu_less)) {
    echo '<img src="'.base_url('assets/images/header.jpg').'" alt="Bannière"/>';
}
echo '</header>';

// MENU
if (!isset($menu_less)) {
    $this->load->view('includes/menu.php');
}

echo '<div class="row">';
echo '<div class="col-md-12">';

// CONTENU
echo $page;

echo '</div>'; // row
echo '</div>'; // col-md-12

// PIED DE PAGE
echo '<footer class="text-center">';
if (!isset($menu_less)) {
    echo '<div class="well well-sm">';
    echo '<a href="'.site_url('contact/mentions_legales').'">Mentions légales</a>';
    echo '</div>';
} else {
    echo '<img src="'.base_url('assets/images/region-auvergne-rhone-alpes.png').'" style="margin-top:20px; margin-right:10px; width:260px;"/>';
    echo '<img src="'.base_url('assets/images/logo-grand-lyon-la-metropole.png').'" style="margin-top:49px; margin-right:15px; width:160px;"/>';
    echo '<img src="'.base_url('assets/images/logopsaderpenap3.png').'" style="margin-right:15px; width:260px;"/>';
    echo '<img src="'.base_url('assets/images/Logo_CASDAR_MAA.png').'" style="margin-right:15px; width:150px;"/>';
    echo '<img src="'.base_url('assets/images/Logo_Region_HDF.png').'" style="width:150px;"/>';
}
echo '</footer>';

echo '</div>'; // container

echo render_flash_messages();

echo render_debug_bar();

echo '</body>';
echo '</html>';
