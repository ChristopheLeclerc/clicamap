#language: fr
Fonctionnalité: Désinscription d'un amapien  à une distribution en tant qu'amap

  @database
  Scénario: Désinscription d'un amapien à une distribution en tant qu'amap
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion de la distrib'AMAP"
    Et que je remplis le datepicker "search_distribution_amap[period]" avec "01/01/2030 au 01/12/2030"
    Et que je devrais voir "test"
    Et que je suis "Gestion des inscriptions"
    Quand je sélectionne "2" depuis "amap_distribution_amap_amapien_inscription_amapiens"
    Et que je presse "Ajouter un ou plusieurs amapiens"
    Et que je presse "Désinscrire"
    Alors je devrais voir "Amapiens désinscrit avec succès"
    Et l'élément "table" ne devrait pas contenir "prenom amapien"
    Et un mail avec le sujet "Votre désinscription à la Distrib'AMAP du 01/01/2030" devrait avoir été envoyé à "amapien@test.amap-aura.org"
