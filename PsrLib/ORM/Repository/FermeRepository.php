<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use PsrLib\DTO\SearchFermeState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\Reseau;

/**
 * @extends ServiceEntityRepository<Ferme>
 */
class FermeRepository extends ServiceEntityRepository
{
    /**
     * @return Ferme[]
     */
    public function search(SearchFermeState $searchFermeState): array
    {
        if ($searchFermeState->isEmpty()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('f')
            ->orderBy('f.nom', 'ASC')

            // optimisation
            ->leftJoin('f.ville', 'ville')
            ->addSelect('ville')
            ->leftJoin('ville.departement', 'departement')
            ->addSelect('departement')
            ->leftJoin('departement.region', 'region')
            ->addSelect('region')
            ->leftJoin('f.produits', 'p')
            ->addSelect('p')
            ->leftJoin('f.paysans', 'paysans')
            ->addSelect('paysans')
            ->leftJoin('paysans.cadreReseau', 'cr')
            ->addSelect('cr')
        ;

        if (null !== $searchFermeState->getRegion()) {
            $qb
                ->andWhere('departement.region = :region')
                ->setParameter('region', $searchFermeState->getRegion())
            ;
        }

        if (null !== $searchFermeState->getDepartement()) {
            $qb
                ->andWhere('departement = :departement')
                ->setParameter('departement', $searchFermeState->getDepartement())
            ;
        }

        if (null !== $searchFermeState->getKeyWord()) {
            $qb
                ->andWhere('f.nom LIKE :keyword')
                ->setParameter('keyword', '%'.$searchFermeState->getKeyWord().'%')
            ;
        }

        if (null !== $searchFermeState->getTypeProduction()) {
            $qb
                ->andWhere('p.typeProduction = :tp')
                ->setParameter('tp', $searchFermeState->getTypeProduction())
            ;
        }

        return $qb
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Ferme[]
     */
    public function findByRegion(Region $region)
    {
        return $this
            ->createQueryBuilder('f')
            ->orderBy('f.nom', 'ASC')
            ->leftJoin('f.ville', 'ville')
            ->leftJoin('ville.departement', 'departement')
            ->where('departement.region = :region')
            ->setParameter('region', $region)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Ferme[]
     */
    public function findByDepartement(Departement $departement)
    {
        return $this
            ->createQueryBuilder('f')
            ->orderBy('f.nom', 'ASC')
            ->leftJoin('f.ville', 'ville')
            ->where('ville.departement = :departement')
            ->setParameter('departement', $departement)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Ferme[]
     */
    public function findByReseau(Reseau $reseau)
    {
        return $this
            ->createQueryBuilder('f')
            ->leftJoin('f.ville', 'v')
            ->leftJoin(Reseau::class, 'r', Join::WITH, 'v MEMBER OF r.villes')
            ->where('r = :reseau')
            ->setParameter('reseau', $reseau)
            ->getQuery()
            ->getResult()
        ;
    }

    public function qbFromAmap(Amap $amap): QueryBuilder
    {
        return $this
            ->createQueryBuilder('f')
            ->orderBy('f.nom', 'ASC')
            ->leftJoin('f.amapienRefs', 'amapienRefs')
            ->where('amapienRefs.amap = :amap')
            ->setParameter('amap', $amap)
        ;
    }

    public function qbFromAmapien(Amapien $amapien): QueryBuilder
    {
        $qb = $this
            ->createQueryBuilder('f')
            ->leftJoin('f.ville', 'ville')
            ->addSelect('ville')
            ->leftJoin('ville.departement', 'departement')
            ->addSelect('departement')
            ->leftJoin('departement.region', 'region')
            ->addSelect('region')
            ->orderBy('f.nom', 'ASC')
        ;

        if ($amapien->isSuperAdmin()) {
            return $qb;
        }

        if ($amapien->isAdminRegion()) {
            return $qb
                ->where('region IN (:regions)')
                ->setParameter('regions', $amapien->getAdminRegions()->toArray())
            ;
        }

        if ($amapien->isAdminDepartment()) {
            return $qb
                ->where('departement IN (:departements)')
                ->setParameter('departements', $amapien->getAdminDepartments()->toArray())
            ;
        }

        throw new LogicException('This code should not be reached');
    }

    public function qbAllOrdered(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('f')
            ->orderBy('f.nom', 'ASC')
        ;
    }

    /**
     * @return Ferme[]
     */
    public function getFromAmap(Amap $amap): array
    {
        return $this
            ->qbFromAmap($amap)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return string[]
     */
    public function getAllSIRET()
    {
        $emails = $this
            ->createQueryBuilder('f')
            ->select('f.siret')
            ->where('f.siret IS NOT NULL')
            ->andWhere('f.siret != \'\'')
            ->getQuery()
            ->getArrayResult()
        ;

        return array_column($emails, 'siret');
    }

    public function qbAccessibleForUser(BaseUser $user): QueryBuilder
    {
        if ($user instanceof Amap) {
            return $this->qbAllOrdered();
        }

        if ($user instanceof Amapien && $user->isAdmin()) {
            return $this->qbFromAmapien($user);
        }

        if ($user instanceof Amapien && $user->isRefProduit()) {
            return $this
                ->createQueryBuilder('f')
                ->where(':user MEMBER OF f.amapienRefs')
            ;
        }

        if ($user instanceof FermeRegroupement) {
            return $this
                ->createQueryBuilder('f')
                ->where('f.regroupement = :regroupement')
                ->setParameter('regroupement', $user)
            ;
        }

        throw new \LogicException('Invalid user');
    }
}
