<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use Assert\Assertion;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AmapRepository;
use RuntimeException;

class AdhesionBuilderAmap implements AdhesionBuider
{
    /**
     * @var AmapRepository
     */
    private $amapRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->amapRepo = $em->getRepository(Amap::class);
    }

    /**
     * @param AdhesionValueAmap $value
     * @param Amapien           $creator
     *
     * @return AdhesionAmap
     */
    public function buildFromValue($value, $creator)
    {
        Assertion::isInstanceOf($creator, Amapien::class);
        Assertion::isInstanceOf($value, AdhesionValueAmap::class);

        $amap = $this->amapRepo->findOneBy(['id' => $value->getAmapId()]);
        if (null === $amap) {
            throw new RuntimeException(sprintf('L\'ID %s ne correspond pas à une AMAP', $value->getAmapId()));
        }

        return new AdhesionAmap($amap, $value, $creator);
    }
}
