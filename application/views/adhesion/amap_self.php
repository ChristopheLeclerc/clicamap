<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var PsrLib\ORM\Entity\AdhesionAmap[] $adhesions */
?>

<div class="col-md-12">
    <h3>Mes reçus</h3>
</div>

<div class="col-md-12">
    <table class="table table-datatable" data-order='[[ 1, "desc" ]]'>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Année</th>
            <th>Outils</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($adhesions as $adhesion): ?>
            <tr>
                <th>Reçu année <?= $adhesion->getValue()->getYear(); ?></th>
                <th><?= $adhesion->getValue()->getYear(); ?></th>
                <th>
                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DOWNLOAD, $adhesion)) : ?>
                        <a title="Télécharger le reçu" href="<?=site_url('/adhesion/amap_download/'.$adhesion->getId()); ?>">
                            <i class="glyphicon glyphicon-file" data-toggle="tooltip" title="Télécharger le reçu"></i>
                        </a>
                    <?php endif; ?>
                </th>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
