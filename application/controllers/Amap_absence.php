<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Amap_absence extends AppController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    /**
     * @param $amapId
     */
    public function liste($amapId)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amapId);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_MANAGE_ABS, $amap);
        $absences = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapAbsence::class)
            ->findBy([
                'amap' => $amap,
            ])
        ;

        $this->loadViewWithTemplate('amap_absence/liste', [
            'absences' => $absences,
            'amap' => $amap,
        ]);
    }

    /**
     * @param $amapId
     * @param null $absenceId
     */
    public function form($amapId, $absenceId = null)
    {
        if (null === $absenceId) {
            /** @var \PsrLib\ORM\Entity\Amap $amap */
            $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amapId);
            $absence = new \PsrLib\ORM\Entity\AmapAbsence($amap);
        } else {
            /** @var \PsrLib\ORM\Entity\AmapAbsence $amap */
            $absence = $this->findOrExit(\PsrLib\ORM\Entity\AmapAbsence::class, $absenceId);
        }

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_MANAGE_ABS, $absence->getAmap());

        $form = $this
            ->formFactory
            ->create(\PsrLib\Form\AmapAbsenceType::class, $absence)
        ;

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($absence);
            $this->em->flush();

            return redirect('amap_absence/liste/'.$absence->getAmap()->getId());
        }

        $this->loadViewWithTemplate('amap_absence/form', [
            'absence' => $absence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $absenceId
     */
    public function supprimer($absenceId)
    {
        /** @var \PsrLib\ORM\Entity\AmapAbsence $amap */
        $absence = $this->findOrExit(\PsrLib\ORM\Entity\AmapAbsence::class, $absenceId);
        $amap = $absence->getAmap();

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_MANAGE_ABS, $amap);

        $this->em->remove($absence);
        $this->em->flush();

        return redirect('amap_absence/liste/'.$amap->getId());
    }
}
