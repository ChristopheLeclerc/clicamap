<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class DocumentCheckTest extends AbstractSecurityTest
{
    public function testGetOwnAmapien()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DOCUMENT_GETOWN));
    }

    public function testGetOwnPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DOCUMENT_GETOWN));
    }

    public function testGetOwnAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DOCUMENT_GETOWN));
    }

    public function testGetOwnRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DOCUMENT_GETOWN));
    }
}
