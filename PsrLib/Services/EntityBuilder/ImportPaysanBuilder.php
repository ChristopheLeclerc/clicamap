<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\VilleRepository;

class ImportPaysanBuilder
{
    /**
     * @var VilleRepository
     */
    private $villeRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->villeRepo = $em->getRepository(Ville::class);
    }

    /**
     * @return Paysan[]
     */
    public function buildPaysansFromImport(array $entete, array $valeurs)
    {
        $paysans = [];
        foreach ($valeurs as $valeur) {
            $paysan = new Paysan();
            $paysan->setNewsletter(true);
            $ferme = new Ferme();
            $paysan->setFerme($ferme);

            $payVille = $this->villeRepo->findOneBy([
                'cp' => trim($valeur['J']),
                'nom' => trim($valeur['K']),
            ]);
            $paysan->setVille($payVille);

            $fVille = $this->villeRepo->findOneBy([
                'cp' => trim($valeur['U']),
                'nom' => trim($valeur['V']),
            ]);
            $ferme->setVille($fVille);

            foreach ($valeur as $k => $v) {
                if ($v) {
                    $v = trim($v);

                    // PAYSAN ----------------------------------------------------------
                    // Nom
                    if ('A' == $k) {
                        $paysan->setNom(mb_strtoupper($v));
                    }
                    // Prénom
                    if ('B' == $k) {
                        $paysan->setPrenom(ucfirst($v));
                    }
                    // Email
                    if ('C' == $k) {
                        $paysan->setEmail($v);
                    }
                    // Téléphone 1
                    if ('D' == $k) {
                        $paysan->setNumTel1($v);
                    }
                    // Téléphone 2
                    if ('E' == $k) {
                        $paysan->setNumTel2($v);
                    }
                    // Adresse Site Web ou Blog
                    if ('F' == $k) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0]);
                        }

                        $ferme->setUrl($v);
                    }
                    // Conjoint : nom
                    if ('G' == $k) {
                        $paysan->setConjointNom(mb_strtoupper($v));
                    }
                    // Conjoint : prénom
                    if ('H' == $k) {
                        $paysan->setConjointPrenom(ucfirst($v));
                    }
                    // Adresse du domicile
                    if ('I' == $k) {
                        $paysan->setLibAdr($v);
                    }
                    // Numéro MSA
                    if ('L' == $k) {
                        $paysan->setMsa($v);
                    }
                    // Année début partenariat AMAP
                    if ('M' == $k) {
                        $paysan->setAnneeDebCommercialisationAmap($v);
                    }
                    // Statut
                    if ('N' == $k) {
                        $paysan->setStatut($v);
                    }
                    // Suivi SPG
                    if ('O' == $k) {
                        $paysan->setSpg($v);
                    }
                    // Abonné newsletter
                    if ('Q' == $k) {
                        $v = strtolower($v);
                        $paysan->setNewsletter('oui' === $v);
                    }

                    // FERME -----------------------------------------------------------
                    // Nom
                    if ('R' == $k) {
                        $ferme->setNom($v);
                    }
                    // Siret
                    if ('S' == $k) {
                        $ferme->setSiret($v);
                    }
                    // Adresse
                    if ('T' == $k) {
                        $ferme->setLibAdr($v);
                    }
                    // GPS lat
                    if ('lat' == $k) {
                        $ferme->setGpsLatitude($v);
                    }
                    // GPS long
                    if ('long' == $k) {
                        $ferme->setGpsLongitude($v);
                    }
                }
            }

            $paysans[] = $paysan;
        }

        return $paysans;
    }
}
