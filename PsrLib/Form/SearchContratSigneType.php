<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\Form\Type\ReseauType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\FermeRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchContratSigneType extends AbstractType
{
    /**
     * @var AmapRepository
     */
    private $amapRepo;

    /**
     * @var FermeRepository
     */
    private $fermeRepo;

    /**
     * @var ModeleContratRepository
     */
    private $modeleContratRepo;

    /**
     * @var Amap[]
     */
    private $choicesCache = [];

    public function __construct(EntityManagerInterface $em)
    {
        $this->amapRepo = $em
            ->getRepository(Amap::class)
        ;
        $this->fermeRepo = $em
            ->getRepository(Ferme::class)
        ;
        $this->modeleContratRepo = $em
            ->getRepository(ModeleContrat::class)
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $currentUser = $options['currentUser'];
        $builder
            ->add('reseau', ReseauType::class, [
                'disabled' => true,
            ])
            ->add('amapFerme', AmapFermeType::class, [
                'inherit_data' => true,
                'disabled' => true,
            ])
            ->add('mc', ChoiceType::class, [
                'choices' => [],
                'disabled' => true,
            ])
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($currentUser) {
                $form = $event->getForm();
                $data = $event->getData();

                /** @var SearchBaseRegionDepartementType $parentForm */
                $parentForm = $form
                    ->getConfig()
                    ->getType()
                    ->getParent()
                    ->getInnerType()
                ;
                [$region, $departement] = $parentForm
                    ->getRegionDepartementFromPreSubmitData($form, $data, $currentUser)
                ;

                if (null === $region) {
                    return;
                }

                if (null === $departement) {
                    $choices = $this->amapRepo->findByRegion($region);
                } else {
                    $choices = $this->amapRepo->findByDepartement($departement);
                }

                $this->choicesCache = $choices;

                $form
                    ->add('amapFerme', AmapFermeType::class, [
                        'amap_choices' => $choices,
                        'inherit_data' => true,
                    ])
                ;
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();
                $amapId = $data['amapFerme']['amap'] ?? '';
                $fermeId = $data['amapFerme']['ferme'] ?? '';

                if ('' === $amapId || '' === $fermeId) {
                    return;
                }

                $amap = null;
                foreach ($this->choicesCache as $choice) {
                    if ($choice->getId() === (int) $amapId) {
                        $amap = $choice;
                    }
                }

                if (null === $amap) {
                    return;
                }

                $choiceFermes = $this->fermeRepo->getFromAmap($amap);
                $ferme = null;
                foreach ($choiceFermes as $choiceFerme) {
                    if ($choiceFerme->getId() === (int) $fermeId) {
                        $ferme = $choiceFerme;
                    }
                }

                if (null === $ferme) {
                    return;
                }

                $mcs = $this->modeleContratRepo->findValidatedByFermeAmap($ferme, $amap);
                $form
                    ->add('mc', ChoiceType::class, [
                        'choices' => $mcs,
                        'choice_label' => function (?ModeleContrat $modeleContrat) {
                            return null === $modeleContrat ? null : $modeleContrat->getNom();
                        },
                        'choice_value' => function (?ModeleContrat $modeleContrat) {
                            return null === $modeleContrat ? null : $modeleContrat->getId();
                        },
                        'placeholder' => true,
                        'required' => false,
                    ])
                ;
            })
        ;
    }

    public function getParent()
    {
        return SearchBaseRegionDepartementType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchContratSigneState::class,
            'empty_data' => function (FormInterface $form) {
                $state = new SearchContratSigneState();
                $state->setRegion($form->get('region')->getData());
                $state->setDepartement($form->get('departement')->getData());

                return $state;
            },
        ]);
    }
}
