<?php

namespace Test\Form\Type;

use Carbon\Carbon;
use PsrLib\Form\Type\PeriodFlatpickrType;
use PsrLib\ORM\Entity\Embeddable\Period;
use Test\Form\AbstractFormTest;

/**
 * @internal
 * @coversNothing
 */
class PeriodFlatpickrTypeTest extends AbstractFormTest
{
    public function testReverseTransformPeriodValid()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class);
        $form->submit('01/01/2000 au 02/01/2000');

        self::assertTrue($form->isSynchronized());

        $res = $form->getData();
        self::assertInstanceOf(Period::class, $res);
        self::assertTrue(Carbon::create(2000, 01, 01)->eq($res->getStartAt()));
        self::assertTrue(Carbon::create(2000, 01, 02)->eq($res->getEndAt()));
    }

    /**
     * @dataProvider invalidInputProvider
     *
     * @param mixed $invalidInput
     */
    public function testReverseTransformPeriodInvalid($invalidInput)
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class);
        $form->submit($invalidInput);

        self::assertTrue($form->isSynchronized());
        self::assertNull($form->getData());
    }

    public function invalidInputProvider()
    {
        return [
            ['invalid'],
            ['1/01/2000 au 02/01/2000'],
        ];
    }

    public function testTransformNull()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null);
        $view = $form->createView();

        self::assertSame('', $view->vars['value']);
    }

    public function testTransformStartNull()
    {
        $period = new Period();
        $period->setStartAt(null);
        $period->setEndAt(new Carbon('2000-01-02'));
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null);
        $view = $form->createView();

        self::assertSame('', $view->vars['value']);
    }

    public function testTransformEndNull()
    {
        $period = new Period();
        $period->setStartAt(new Carbon('2000-01-01'));
        $period->setEndAt(null);
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null);
        $view = $form->createView();

        self::assertSame('', $view->vars['value']);
    }

    public function testTransformPeriod()
    {
        $form = $this->formFactory->create(
            PeriodFlatpickrType::class,
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-02')
            )
        );
        $view = $form->createView();

        self::assertSame('01/01/2000 au 02/01/2000', $view->vars['value']);
    }

    public function testDefaultValue()
    {
        Carbon::setTestNow(new Carbon('2000-01-01'));
        $form = $this->formFactory->create(PeriodFlatpickrType::class);
        $form->submit(null);

        self::assertTrue($form->isSynchronized());

        $res = $form->getData();
        self::assertInstanceOf(Period::class, $res);
        self::assertTrue(Carbon::create(2000, 01, 01)->eq($res->getStartAt()));
        self::assertTrue(Carbon::create(2000, 04, 01)->eq($res->getEndAt()));

        Carbon::setTestNow();
    }

    public function testMinDateDefaultNullView()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class);
        $form->submit(null);

        self::assertTrue($form->isSynchronized());

        $view = $form->createView();
        self::assertFalse(isset($view->vars['attr']['data-min-date']));
    }

    public function testMinDateView()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null, [
            'min_date' => new Carbon('2000-01-02'),
        ]);
        $form->submit(null);

        self::assertTrue($form->isSynchronized());

        $view = $form->createView();
        self::assertSame('2000-01-02', $view->vars['attr']['data-min-date']);
    }

    public function testMinDateConstraintBefore()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null, [
            'min_date' => new Carbon('2000-01-02'),
        ]);
        $form->submit('01/01/2000 au 03/01/2000');

        self::assertTrue($form->isSynchronized());
        self::assertFalse($form->isValid());
        self::assertSame('La date de début doit être après la date minimale', $form->getErrors()[0]->getMessage());
    }

    public function testMinDateConstraintSame()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null, [
            'min_date' => new Carbon('2000-01-02'),
        ]);
        $form->submit('02/01/2000 au 03/01/2000');

        self::assertTrue($form->isSynchronized());
        self::assertTrue($form->isValid());
    }

    public function testMinDateConstraintSameDay()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null, [
            'min_date' => (new Carbon('2000-01-02'))->addMinute(),
        ]);
        $form->submit('02/01/2000 au 03/01/2000');

        self::assertTrue($form->isSynchronized());
        self::assertTrue($form->isValid());
    }

    public function testMinDateConstraintAfter()
    {
        $form = $this->formFactory->create(PeriodFlatpickrType::class, null, [
            'min_date' => new Carbon('2000-01-02'),
        ]);
        $form->submit('03/01/2000 au 03/01/2000');

        self::assertTrue($form->isSynchronized());
        self::assertTrue($form->isValid());
    }
}
