<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class ContractSignedListArchivesTest extends AbstractSecurityTest
{
    public function testAmapien()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_LIST_ARCHIVED));
    }

    public function testAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_LIST_ARCHIVED));
    }

    public function testPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_LIST_ARCHIVED));
    }

    public function testRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_LIST_ARCHIVED));
    }
}
