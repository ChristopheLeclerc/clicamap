<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class Captcha
{
    public const expiration = 7200;
    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->helper('captcha');
    }

    public function nouveauCaptcha()
    {
        // Genere le captcha
        $urlLocation = '/uploads/captcha/';
        $captchaPath = \PsrLib\ProjectLocation::PROJECT_ROOT.'/public/'.$urlLocation;
        if (!is_dir($captchaPath) && !mkdir($captchaPath, 0777, true) && !is_dir($captchaPath)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $captchaPath));
        }

        $cap = create_captcha([
            'img_path' => $captchaPath,
            'img_url' => $urlLocation,
            'img_width' => 300,
            'img_height' => 60,
            'font_size' => 32,
            'font_path' => './assets/font/OpenSans-Regular.ttf',
            'expiration' => self::expiration,
            'pool' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        ]);

        // Insere le captcha en BDD
        $data = [
            'captcha_time' => $cap['time'],
            'ip_address' => $this->CI->input->ip_address(),
            'word' => $cap['word'],
        ];
        $query = $this->CI->db->insert_string('captcha', $data);
        $this->CI->db->query($query);

        return $cap;
    }

    /**
     * @param $captcha
     *
     * @return bool
     */
    public function verifCaptcha($captcha)
    {
        $captchaTime = time() - self::expiration;

        // Supprime les anciens captcha
        $this
            ->CI
            ->db
            ->where('captcha_time < ', $captchaTime)
            ->delete('captcha')
        ;

        // Récupère les captchas qui correspondent à la demande de verification
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = [$captcha, $this->CI->input->ip_address(), $captchaTime];
        $query = $this
            ->CI
            ->db
            ->query($sql, $binds)
        ;
        $row = $query->row();

        return '1' === $row->count;
    }
}
