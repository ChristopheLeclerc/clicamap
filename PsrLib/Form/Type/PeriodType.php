<?php

namespace PsrLib\Form\Type;

use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PeriodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startAt', CarbonDateType::class, [
                'label' => 'Période - date de début',
                'required' => false,
            ])
            ->add('endAt', CarbonDateType::class, [
                'label' => 'Période - date de fin',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Period::class,
        ]);
    }
}
