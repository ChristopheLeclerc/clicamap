# language: fr
Fonctionnalité: Comme utilisateur, je doit revenir sur la page "evenement" si je suis connecté au portail

  Scénario: Redirection sans authentifiation
    Etant donné que je suis sur "/portail"
    Alors je devrais être sur "/portail/connexion"

  Scénario: Redirection authentifié
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand je suis sur "/portail"
    Alors je devrais être sur "/evenement"
