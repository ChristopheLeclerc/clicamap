<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        // Fix database file
        $this->connection->executeQuery('UPDATE ak_ville SET v_cp = \'38380\' WHERE v_id=\'38446\'');

        $existingVilles = $this
            ->connection
            ->executeQuery('SELECT a_id, a_code_postal, a_ville FROM ak_amapien')
            ->fetchAll()
        ;

        $this->addSql('ALTER TABLE ak_amapien ADD ville_id VARCHAR(5) DEFAULT NULL, DROP a_code_postal, DROP a_ville');

        foreach ($existingVilles as $existingVille) {
            if (null === $existingVille['a_ville'] || null === $existingVille['a_code_postal']) {
                continue;
            }

            if ('38660' === $existingVille['a_code_postal'] && 'SAINT-HILAIRE' === $existingVille['a_ville']) {
                $existingVille['a_ville'] = 'PLATEAU-DES-PETITES-ROCHES';
            }
            if ('38190' === $existingVille['a_code_postal'] && 'Crolles' === $existingVille['a_ville']) {
                $existingVille['a_code_postal'] = '38920';
            }

            $ville = $this
                ->connection
                ->executeQuery('SELECT v_id FROM ak_ville WHERE v_nom = :nom AND v_cp = :cp', [
                    'nom' => $existingVille['a_ville'],
                    'cp' => $existingVille['a_code_postal'],
                ])
                ->fetchAll()
            ;

            if (1 !== count($ville) || null === $ville[0]['v_id']) {
                throw new Exception(
                    sprintf('Unable to find ville with %s %s', $existingVille['a_code_postal'], $existingVille['a_ville'])
                );
            }
            $this->addSql('UPDATE ak_amapien SET ville_id = :villeId WHERE a_id = :id', [
                'villeId' => $ville[0]['v_id'],
                'id' => $existingVille['a_id'],
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
