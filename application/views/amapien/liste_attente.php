<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\Amapien $amapien */
/** @var \PsrLib\ORM\Entity\Ferme[] $amapFermes */
?>

<script type="text/javascript">
  /*ALERTES*/
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 4500);

  });

</script>

<?php
//// ALERTES
//if ($create) {
//?>
<!--    <div class="modal fade" id="alerte">-->
<!--        <div class="modal-dialog">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
<!--                    <h4 class="modal-title">Information</h4>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
<!--                    <p><i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> L'amapien(ne) a été créé(e) avec-->
<!--                        succès, vous pouvez le mettre sur la liste d'attente des fermes suivantes</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    --><?php
//}

echo '<h3>Liste d\'attente</h3>';

echo '<h5><a href="'.site_url('amapien').'">Gestion des amapiens</a> <i class="glyphicon glyphicon-menu-right"></i> ';

echo mb_strtoupper($amapien->getNom()).' '.ucfirst($amapien->getPrenom()).' <i class="glyphicon glyphicon-menu-right"></i> Liste d\'attente';

?>
</h5>


<div class="alert alert-info">Cocher les fermes où vous souhaitez que l'amapien soit mis en liste d'attente puis sauvegarder</div>

<?php if (count($amapFermes) > 0): ?>
    <form method="POST" action="">

    <div class="table-responsive">
        <table class="table">
            <thead class="thead-inverse">
            <tr>
                <th>Ferme</th>
            </tr>
            </thead>

            <tbody>
                <?php foreach ($form->children['fermeAttentes'] as $field): ?>
                    <tr>
                        <td><?= $field->vars['label']; ?></td>
                        <td>
                            <input
                                type="checkbox"
                                name="<?= $field->vars['full_name']; ?>"
                                value="<?= $field->vars['value']; ?>"
                                <?= $field->vars['checked'] ? 'checked' : null; ?>
                            >
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <button type="submit" class="btn btn-success pull-right">Sauvegarder</button>

    <a href="<?=site_url('amapien'); ?>" class="pull-right btn btn-default">Annuler</a>


    </form>
<?php else: ?>
        <div class="alert alert-warning">Il n'y a pas encore de ferme en lien avec l'AMAP <a href="<?=site_url('ferme'); ?>">Gestion des fermes</a></div>
<?php endif; ?>
