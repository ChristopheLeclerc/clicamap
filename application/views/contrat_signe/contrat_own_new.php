<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat[] $mc_all */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu[] $livLieux */
/** @var bool[] $attente */
?>

<h3>Les nouveaux contrats disponibles</h3>


<table class="table table-datatable" data-paging="false" data-info="false">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Paniers obligatoires</th>
            <th>Choix identiques</th>
            <th>Date de fin de souscription</th>
            <th>Lieu de livraison</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($mc_all as $mc) : ?>
        <tr>
            <td><?=$mc->getNom(); ?></td>
            <td><?=$mc->getNblivPlancher(); ?></td>
            <td><?=print_bool($mc->getProduitsIdentiqueAmapien()); ?></td>
            <td><?=date_format_french($mc->getForclusion()); ?></td>
            <td><?=$livLieux[$mc->getId()]->getNom(); ?></td>
            <td>
                <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE, $mc)): ?>
                    <a href="<?=site_url('contrat_signe/contrat_subscribe_1/'.$mc->getId()); ?>" class="btn btn-success">Souscrire</a>
                <?php elseif ($attente[$mc->getId()]): ?>
                    <a href="#" class="btn btn-default" disabled>Liste d'attente</a>
                <?php else: ?>
                    <a href="#" class="btn btn-default" disabled>Fin de souscription</a>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
