<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
echo '<h3>Informations générales</h3>';

echo '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.$mc->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Déplacer une date de livraison <i class="glyphicon glyphicon-menu-right"></i> Étape 1</h5>';

echo '<form method="POST" action="'.site_url('contrat_signe/move/'.$mc->getId()).'">';
?>

<input type="hidden" name="step_1" value="1"/>

<div class="row">
    <div class="form-group">
        <div class="col-md-12">

            <div class="alert alert-info">
                <p>Cet outil va vous permettre de déplacer une date de livraison, pour tous les adhérents à ce
                    contrat.</p>

                <p>Exemple de cas d'utilisation : un producteur a prévu de livrer ses produits le 20 janvier
                    Il est obligé de décaler au 27 janvier pour une raison quelconque.</p>

                <p>Cet outil permet de déplacer la date, en gardant à l'identique les quantités commandées</p>

                <p>Cet outil affiche la liste des emails des personnes impactées, vous pourrez alors les avertir.</p>
            </div>

        </div>
    </div>
</div>

<div class="form-group">

    <input type="submit" value="Étape suivante" class="pull-right btn btn-success"/>

    <a href="<?= site_url('contrat_signe/accueil'); ?>" class="pull-right btn btn-default">Annuler</a>

</div>

</form>
