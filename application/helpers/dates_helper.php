<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!function_exists('date_format_french')) {
    function date_format_french(DateTimeInterface $input = null)
    {
        if (null === $input) {
            return '';
        }

        return $input->format('d/m/Y');
    }
}

if (!function_exists('date_format_french_hour')) {
    function date_format_french_hour(DateTimeInterface $input = null)
    {
        if (null === $input) {
            return '';
        }

        return $input->format('d/m/Y H:i:s');
    }
}

if (!function_exists('carbon_format_french')) {
    function carbon_format_french(Carbon\Carbon $date)
    {
        if (null === $date) {
            return '';
        }

        return $date->format('d/m/Y');
    }
}
