<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class PaysanCheck extends AbstractSecurityTest
{
    public function testDownloadListRegroupementRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT));
    }

    public function testDownloadListRegroupementAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT));
    }

    public function testPaysanCreationAmapien()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_CREATE));
    }

    public function testPaysanCreationAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_CREATE));
    }

    public function testPaysanCreationFermeRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_CREATE));
    }

    public function testPaysanCreationAmapienAdmin()
    {
        $admin = $this->prophesize(Amapien::class);
        $admin->isAdmin()->willReturn(true);
        $this->mockAuthenticatedUser($admin->reveal());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_CREATE));
    }

    public function testPaysanCreationAmapienRefProd()
    {
        $admin = $this->prophesize(Amapien::class);
        $admin->isAdmin()->willReturn(false);
        $admin->isRefProduit()->willReturn(true);
        $this->mockAuthenticatedUser($admin->reveal());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_CREATE));
    }
}
