<?php

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class StringExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('ucfirst', [$this, 'ucfirst']),
        ];
    }

    public function ucfirst(?string $input): string
    {
        if (null === $input) {
            return '';
        }

        return ucfirst($input);
    }
}
