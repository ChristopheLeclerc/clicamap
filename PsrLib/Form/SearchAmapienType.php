<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\SearchAmapienState;
use PsrLib\Form\Type\AnneeAdhesionGenericType;
use PsrLib\Form\Type\ReseauType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\DepartementRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchAmapienType extends AbstractType
{
    /**
     * @var AmapRepository
     */
    private $amapRepo;

    /**
     * @var DepartementRepository
     */
    private $departementRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->amapRepo = $em
            ->getRepository(Amap::class)
        ;

        $this->departementRepo = $em
            ->getRepository(Departement::class)
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $currentUser = $options['currentUser'];
        $builder
            ->add('reseau', ReseauType::class, [
                'disabled' => true,
            ])
            ->add('adhesion', AnneeAdhesionGenericType::class)
            ->add('amap', ChoiceType::class, [
                'disabled' => true,
            ])
            ->add('keyword', TextType::class, [
                'required' => false,
            ])
            ->add('newsletter', CheckboxType::class, [
                'label' => 'Abonné à la newsletter',
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Actif',
            ])
        ;

        if ($currentUser instanceof Amapien && $currentUser->isSuperAdmin()) {
            $builder
                ->add('noAmap', CheckboxType::class, [
                    'label' => 'Amapien hors AMAP',
                ])
            ;
        }

        // Amap auto select
        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($currentUser) {
                $form = $event->getForm();
                $data = $event->getData();

                /** @var SearchBaseRegionDepartementType $parentForm */
                $parentForm = $form
                    ->getConfig()
                    ->getType()
                    ->getParent()
                    ->getInnerType()
                ;
                [$region, $departement] = $parentForm
                    ->getRegionDepartementFromPreSubmitData($form, $data, $currentUser)
                ;

                if (null === $region) {
                    return;
                }

                if (null === $departement) {
                    $choices = $this->amapRepo->findByRegion($region);
                } else {
                    $choices = $this->amapRepo->findByDepartement($departement);
                }

                $form
                    ->add('amap', ChoiceType::class, [
                        'choices' => $choices,
                        'required' => false,
                        'placeholder' => true,
                        'choice_label' => function (?Amap $amap) {
                            return null === $amap ? '' : $amap->getNom();
                        },
                        'choice_value' => function (?Amap $amap) {
                            return null === $amap ? '' : $amap->getId();
                        },
                    ])
                ;
            })
        ;
    }

    public function getParent()
    {
        return SearchBaseRegionDepartementType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchAmapienState::class,
            'empty_data' => function (FormInterface $form) {
                $state = new SearchAmapienState();
                $state->setRegion($form->get('region')->getData());
                $state->setDepartement($form->get('departement')->getData());

                return $state;
            },
        ]);
    }
}
