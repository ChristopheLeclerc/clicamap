<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AkAmapCollectif.
 *
 * @ORM\Table(name="ak_amap_collectif")
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AmapCollectifRepository")
 * @UniqueEntity(fields={"profil", "precision", "amap", "amapien"}, message="Le rôle est déjà pris dans l'AMAP", errorPath="")
 */
class AmapCollectif
{
    public const PROFIL_PRESIDENT = 'president';
    public const PROFIL_SECRETAIRE = 'secretaire';
    public const PROFIL_TRESORIER = 'tresorier';
    public const PROFIL_AUTRE = 'autre';
    public const PROFILS = [
        self::PROFIL_PRESIDENT,
        self::PROFIL_SECRETAIRE,
        self::PROFIL_TRESORIER,
        self::PROFIL_AUTRE,
    ];

    /**
     * @var ?int
     *
     * @ORM\Column(name="amap_coll_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_coll_profil", type="string", length=150, nullable=true)
     * @Assert\NotBlank
     * @Assert\Choice(choices=AmapCollectif::PROFILS)
     */
    private $profil;

    /**
     * @var null|string
     *
     * @ORM\Column(name="amap_coll_precision", type="string", length=50, nullable=true)
     * @Assert\Length(max="50")
     */
    private $precision;

    /**
     * @var Amap
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amap", inversedBy="collectifs")
     * @ORM\JoinColumn(name="amap_coll_fk_amap_id", referencedColumnName="amap_id", nullable=false)
     * @Assert\NotNull
     */
    private $amap;

    /**
     * @var null|Amapien
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amapien")
     * @ORM\JoinColumn(name="amap_coll_fk_amapien_id", referencedColumnName="a_id")
     * @Assert\NotNull
     */
    private $amapien;

    /**
     * AmapCollectif constructor.
     */
    public function __construct(Amap $amap)
    {
        $this->amap = $amap;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }

    public function setAmap(Amap $amap): AmapCollectif
    {
        $this->amap = $amap;

        return $this;
    }

    public function getProfil(): ?string
    {
        return $this->profil;
    }

    public function setProfil(?string $profil): AmapCollectif
    {
        $this->profil = $profil;

        return $this;
    }

    public function getPrecision(): ?string
    {
        return $this->precision;
    }

    public function setPrecision(?string $precision): AmapCollectif
    {
        $this->precision = $precision;

        return $this;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): AmapCollectif
    {
        $this->amapien = $amapien;

        return $this;
    }
}
