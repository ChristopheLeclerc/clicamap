<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AmapAnneeAdhesion;

/**
 * Bind added file to amap adhesion list.
 */
class AdhesionAmapPostGenerateSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [Events::ADHESION_POST_GENERATE];
    }

    public function adhesionPostGenerate(AdhesionPostGenerateEventArgs $args): void
    {
        $adhesion = $args->getAdhesion();
        if (!($adhesion instanceof AdhesionAmap)) {
            return;
        }

        $amap = $adhesion->getAmap();
        $year = $adhesion->getValue()->getYear();

        $existingAdhesion = $amap
            ->getAnneeAdhesions()
            ->filter(function (AmapAnneeAdhesion $amapAnneeAdhesion) use ($year) {
                return $amapAnneeAdhesion->getAnnee() === $year;
            })
        ;

        if (!$existingAdhesion->isEmpty()) {
            return;
        }

        $newAmapAnneeAdhesion = new AmapAnneeAdhesion($year, $amap);
        $amap->addAnneeAdhesion($newAmapAnneeAdhesion);
    }
}
