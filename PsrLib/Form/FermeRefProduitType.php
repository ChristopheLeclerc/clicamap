<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Repository\AmapienRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FermeRefProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Amap[] $amaps */
        $amaps = $options['amaps'];
        /** @var Ferme $ferme */
        $ferme = $options['ferme'];

        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => $amaps,
                'choice_label' => function (?Amap $amap) {
                    return null === $amap ? '' : $amap->getNom();
                },
                'choice_value' => function (?Amap $amap) {
                    return null === $amap ? null : $amap->getId();
                },
                'required' => true,
                'placeholder' => 1 !== count($amaps),
            ])

        ;

        $builder
            ->get('amap')
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($amaps, $ferme) {
                $form = $event->getForm()->getParent();

                if (1 === count($amaps)) {
                    $this->addAmapienFromAmapField($form, $amaps[0], $ferme);

                    return;
                }

                $form
                    ->add('amapien', ChoiceType::class, [
                        'required' => false,
                        'placeholder' => true,
                        'attr' => ['readonly' => true],
                    ])
                ;
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($ferme) {
                /** @var Amap $amap */
                $amap = $event->getForm()->getData();

                $this->addAmapienFromAmapField($event->getForm()->getParent(), $amap, $ferme);
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
            'amaps' => [],
            'ferme' => null,
        ]);
    }

    private function addAmapienFromAmapField(FormInterface $form, Amap $amap, Ferme $ferme): void
    {
        $form
            ->add('amapien', EntityType::class, [
                'class' => Amapien::class,
                'query_builder' => function (AmapienRepository $amapienRepository) use ($amap, $ferme) {
                    return $amapienRepository->qbNotRefProduitFromAmapFerme($amap, $ferme);
                },
                'required' => false,
                'placeholder' => true,
            ])
        ;
    }
}
