<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Repository\PaysanRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Expression;
use Symfony\Component\Validator\Constraints\NotNull;

class FermePaysanType extends AbstractType
{
    /**
     * @var PaysanRepository
     */
    private $paysanRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->paysanRepo = $em
            ->getRepository(Paysan::class)
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('paysan', EmailType::class, [
                'constraints' => [
                    new NotNull(['message' => 'Le paysan n\'existe pas dans la base de donnée ou le nom est mal formaté.']),
                    new Expression([
                        'expression' => 'value === null or value.getFerme() === null',
                        'message' => 'Le paysan est déjà affilié à une ferme.',
                    ]),
                ],
                'attr' => ['placeholder' => 'Exemple: Dupont Jeant'],
                'required' => false,
            ])
        ;

        $builder
            ->get('paysan')
            ->addModelTransformer(
                new CallbackTransformer(
                    function (Paysan $paysan = null) {
                        if (null === $paysan) {
                            return null;
                        }

                        return $paysan->getEmail();
                    },
                    function (string $email = null) {
                        if (null === $email) {
                            return null;
                        }

                        return $this->paysanRepo->findOneBy(['email' => $email]);
                    }
                )
            )
        ;
    }
}
