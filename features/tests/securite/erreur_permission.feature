# language: fr
Fonctionnalité: Comme utilisateur, je doit retourner sur la page d'accueil en cas de tentative d'accès invalide

  Scénario: Accès invalide
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Quand je vais sur "/amap"
    Alors je devrais être sur "/"
    Et je devrais voir "Vous n'avez pas les accès suffisant pour consulter cette page, veuillez vous reconnecter."
    # Vérifie que l'on est bien déconnecte
    Et je vais sur "/evenement"
    Et je devrais être sur "/"
