#language: fr
Fonctionnalité: suppression dun produit

  @database
  Plan du Scénario: suppression d'un produit en tant que paysan
    Etant donné que je suis authentifié comme "paysan" avec le mail "<mail>"
    Quand  je suis sur "/produit/accueil/1"
    Et que je suis "Supprimer le produit"
    Et     je devrais voir "Voulez-vous vraiment supprimer le produit \"Produit 1\" ?"
    Et     je devrais voir "Etes-vous sûr de vouloir supprimer ce produit ?"
    Et que je suis "confirmer la suppression"
    Et     je devrais voir "Produit supprimé avec succès"
    Et     je ne devrais pas voir "Produit 1"
    Exemples:
      | mail |
      | paysan@test.amap-aura.org |