<?php

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\DTO\AmapDistributionDetailMassEdit;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Repository\AmapLivraisonLieuRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class AmapDistributionEditMasseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?Amap $amap */
        $amap = $options['amap'];
        Assertion::isInstanceOf($amap, Amap::class);

        $builder
            ->add('livraisonLieu', EntityType::class, [
                'label' => 'Lieu de livraison*',
                'class' => AmapLivraisonLieu::class,
                'query_builder' => function (AmapLivraisonLieuRepository $amapLivraisonLieuRepository) use ($amap) {
                    return $amapLivraisonLieuRepository->qbByAmap($amap);
                },
                'required' => false,
                'constraints' => [new NotNull()],
            ])
            ->add('detail', AmapDistributionDetailType::class, [
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'data_class' => AmapDistributionDetailMassEdit::class,
        ]);
    }
}
