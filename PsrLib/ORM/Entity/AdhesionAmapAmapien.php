<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Assert\Assertion;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AdhesionAmapAmapienRepository")
 */
class AdhesionAmapAmapien extends Adhesion
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var ?Amapien
     * @ORM\ManyToOne(targetEntity="Amapien")
     * @ORM\JoinColumn(referencedColumnName="a_id", nullable=true, onDelete="SET NULL")
     */
    protected $amapien;

    /**
     * @var AdhesionValueAmapAmapien
     * @ORM\OneToOne(targetEntity="AdhesionValueAmapAmapien", cascade="ALL")
     * @Assert\Valid()
     * @ORM\JoinColumn(nullable=false)
     */
    protected $value;

    /**
     * @var Amap
     * @ORM\ManyToOne(targetEntity="Amap")
     * @ORM\JoinColumn(referencedColumnName="amap_id", nullable=false)
     * @Assert\Expression("value.getAmapiens().contains(this.getAmapien())", groups={"import"}, message="Erreur d'accès à l'amapien")
     */
    protected $creator;

    /**
     * AdhesionFerme constructor.
     */
    public function __construct(Amapien $amapien, AdhesionValueAmapAmapien $value, Amap $creator)
    {
        $this->amapien = $amapien;
        $this->value = $value;
        $this->creator = $creator;
    }

    /**
     * @return ?Amapien
     */
    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    /**
     * @param ?Amapien $amapien
     */
    public function setAmapien($amapien): AdhesionAmapAmapien
    {
        $this->amapien = $amapien;

        return $this;
    }

    public function getValue(): AdhesionValueAmapAmapien
    {
        return $this->value;
    }

    /**
     * @param AdhesionValueAmapAmapien $value
     *
     * @return AdhesionAmapAmapien
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Amap
     */
    public function getCreator(): BaseUser
    {
        return $this->creator;
    }

    /**
     * @param Amap $creator
     */
    public function setCreator(BaseUser $creator): AdhesionAmapAmapien
    {
        Assertion::isInstanceOf($creator, Amap::class);
        $this->creator = $creator;

        return $this;
    }
}
