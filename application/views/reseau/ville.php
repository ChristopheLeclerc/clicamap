<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Reseau $reseau */
use PsrLib\ORM\Entity\Ville;

?>

<script type="text/javascript">

  /*ALERTES & AUTOCOMPLÉTION CP / VILLE */

  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 2500);

    $(function () {
      $("#code_postal_ville").autocomplete({
        source: <?= '"'.site_url('ajax/get_cp_ville_suggestion"'); ?>,
        dataType: "json",
        minLength: 4,
        appendTo: "#container"
      });
    });

  });

</script>

<?php
echo '<h3>Les villes du réseau</h3>';

echo '<h5><a href="'.site_url('reseau').'">Les réseaux</a> <i class="glyphicon glyphicon-menu-right"></i> '.$reseau->getNom().'</h5>';

echo '<div class="alert alert-info">Si vous voulez ajouter une ville au réseau, utilisez le champ ci-dessous <strong>en vous aidant des suggestions proposées en cours de frappe</strong>. Il convient de commencer par le code postal. Pour en retirer, il suffit de cliquer sur la ville à supprimer.</div>';

echo '<form method="POST" action="'.site_url('reseau/ville_ajout/'.$reseau->getId()).'">';

echo '<div class="row">';
echo '<div class="form-group col-sm-12">';
echo '<input type="text" class="form-control" id="code_postal_ville" name="code_postal_ville" value="" placeholder="Exemple : 69001, LYON 1er Arrondissement"/>';
echo '<div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>';
echo form_error('code_postal_ville');
echo '</div>';
echo '<div class="form-group col-sm-12">';
echo '<input type="submit" value="Ajouter" class="btn btn-success pull-right"/>';
echo '</div>';
echo '</div>';
echo '</form>';

/** @var Ville $ville */
foreach ($reseau->getVilles() as $ville) {
    echo '<a href="#" data-toggle="modal" data-target="#modal_'.$ville->getId().'" ><button class="btn btn-primary btn-sm" data-toggle="tooltip" title="Retirer la ville du réseau">'.$ville->getCpString().', '.$ville->getNom().' <i class="glyphicon glyphicon-remove"></i></button></a>&nbsp;';

    echo '<div class="modal fade text-left" id="modal_'.$ville->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    echo '<div class="modal-dialog">';
    echo '<div class="modal-content">';
    echo '<div class="modal-header">';
    echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    echo '<h4 class="modal-title" id="myModalLabel">Réseau "'.$reseau->getNom().'"</h4>';
    echo '</div>';
    echo '<div class="modal-body">';
    echo '<h5>Voulez-vous vraiment retirer "'.$ville->getNom().'" du réseau ?</h5>';
    echo '<p class="text-right">';
    echo '<a href="'.site_url('reseau/ville_remove/'.$reseau->getId().'/'.$ville->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
    echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
    echo '</p>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}

?>
