# language: fr
Fonctionnalité: Comme utilisateur, je devrait pouvoir ajouter des types de production aux fermes

  Scénario: Accès depuis l'interface
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Quand je suis sur "/ferme/type_production_display/1"
    Et je suis "Ajouter"
    Alors je devrais voir "Ajouter des types de production"
    Et je devrais être sur "/ferme/type_production_creation/1"

  Scénario: Ajout sans fichier
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Quand je suis sur "/ferme/type_production_creation/1"
    Et je presse "Ajouter le type de production"
    Alors je devrais voir "Vous n'avez pas sélectionné de fichier à envoyer."

  Scénario: Ajout fichier mauvais type
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Quand je suis sur "/ferme/type_production_creation/1"
    Et j'attache le fichier "img_50k.jpg" à "ferme_type_production_propose[certification]"
    Et je presse "Ajouter le type de production"
    Alors je devrais voir "Le type de fichier que vous tentez d'envoyer n'est pas autorisé."

  Scénario: Ajout de type de production par défaut
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Quand je suis sur "/ferme/type_production_creation/1"
    Et j'attache le fichier "pdf_11m.pdf" à "ferme_type_production_propose[certification]"
    Et je presse "Ajouter le type de production"
    Alors je devrais voir "Autres / divers"

  Scénario: Ajout multiple du meme type de production
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Quand je suis sur "/ferme/type_production_creation/1"
    Et j'attache le fichier "pdf_11m.pdf" à "ferme_type_production_propose[certification]"
    Et je remplis "ferme_type_production_propose[typeProduction]" avec "19"
    Et je presse "Ajouter le type de production"
    Et que je suis sur "/ferme/type_production_creation/1"
    Et j'attache le fichier "pdf_11m.pdf" à "ferme_type_production_propose[certification]"
    Et je remplis "ferme_type_production_propose[typeProduction]" avec "19"
    Et je presse "Ajouter le type de production"
    Alors je devrais voir "Le produit existe déjà dans la base de donnée."
