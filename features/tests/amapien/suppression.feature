#language: fr
Fonctionnalité: suppression d'un amapien

  @database
  Plan du Scénario: suppression d'un amapien en tant qu'administrateur
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/amapien"
    Et que je remplis "search_amapien[region]" avec "84"
    Et que je suis "Supprimer le compte de l'amapien"
    Et je devrais voir "Voulez-vous vraiment supprimer l'amapien(ne)"
    Et je devrais voir "Attention ! En supprimant l'amapien(ne)"
    Et que je suis "supprimer amapien"
    Et je devrais voir "L'amapien a été supprimé(e) avec succès"
    Et je ne devrais pas voir "amapien@test.amap-aura.org"

    Exemples:
      | mail                           |
      | admin@test.amap-aura.org       |
      | admin.aura@test.amap-aura.org  |
      | admin.rhone@test.amap-aura.org |