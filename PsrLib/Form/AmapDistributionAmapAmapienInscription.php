<?php

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AmapienRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

class AmapDistributionAmapAmapienInscription extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var AmapDistribution $distribution */
        $distribution = $options['distribution'];
        Assertion::isInstanceOf($distribution, AmapDistribution::class);

        $builder
            ->add('amapiens', EntityType::class, [
                'class' => Amapien::class,
                'query_builder' => function (AmapienRepository $amapienRepository) use ($distribution) {
                    return $amapienRepository->qbDistributionAmapienAvailiable($distribution);
                },
                'multiple' => true,
                'expanded' => false,
                'constraints' => [new Count(['min' => 1])],
                'attr' => ['class' => 'js-select2'],
                'choice_label' => function (?Amapien $amapien) {
                    if (null === $amapien) {
                        return '';
                    }

                    return $amapien->getNomCompletInverse();
                },
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($distribution) {
                $amapiens = $event->getData()['amapiens'];
                if (count($amapiens) > $distribution->getPlacesRestantes()) {
                    $event->getForm()->get('amapiens')->addError(new FormError(
                        'Pas assez de places disponibles pour ajouter autant d\'amapiens à cette distribution.'
                    ));
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'distribution' => null,
        ]);
    }
}
