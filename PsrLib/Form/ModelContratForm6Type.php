<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\ModeleContrat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModelContratForm6Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reglementNbMax', NumberType::class)
            ->add('dateReglements', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => ModeleContratDateReglementType::class,
                'by_reference' => false,
            ])
            ->add('reglementType', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices' => ModeleContrat::mode_available,
            ])
            ->add('reglementModalite', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'L\'information inscrite dans ce champ est reprise dans le contrat juridique, vous pouvez y mettre l’ordre du libellé du chèque',
                    'rows' => 6,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'data_class' => ModeleContrat::class,
            'validation_groups' => ['form6'],
        ]);
    }
}
