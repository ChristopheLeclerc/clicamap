<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use PsrLib\ORM\Entity\Files\Logo;
use PsrLib\ORM\Entity\Reseau;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184615 extends AbstractMigration
{
    private $existingLogos = [];

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        $this->existingLogos = $this
            ->connection
            ->executeQuery('SELECT res_id, res_reseau_logo_filename FROM ak_reseau WHERE ak_reseau.res_reseau_logo_filename IS NOT NULL')
            ->fetchAll()
        ;
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_reseau ADD logo_id INT DEFAULT NULL, DROP res_reseau_logo_filename');
    }

    public function postUp(Schema $schema): void
    {
        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        $reseauRepo = $em->getRepository(Reseau::class);
        foreach ($this->existingLogos as $existingLogo) {
            $reseau = $reseauRepo->findOneById($existingLogo['res_id']);
            $logo = new Logo($existingLogo['res_reseau_logo_filename']);
            $reseau->setLogo($logo);

            $em->persist($logo);
        }
        $em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
