<?php

namespace PsrLib\Services\Security\Checkers;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;

class DistributionCheck extends AbstractCheck
{
    public function isGrantedList(): bool
    {
        return $this->getCurrentUser() instanceof Amap;
    }

    public function isGrantedDetail(AmapDistribution $amapDistribution): bool
    {
        $currentUser = $this->getCurrentUser();

        return $currentUser instanceof Amap
              && $amapDistribution->getAmapLivraisonLieu()->getAmap() === $currentUser;
    }

    public function isGrantedDelete(AmapDistribution $amapDistribution): bool
    {
        return $this->getCurrentUser() instanceof Amap
            && $amapDistribution->getAmapLivraisonLieu()->getAmap() === $this->getCurrentUser();
    }

    public function isGrantedAmapienListOwn(): bool
    {
        return $this->getCurrentUser() instanceof Amapien;
    }

    public function isGrantedAmapienRegister(AmapDistribution $amapDistribution): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && (!$amapDistribution->getAmapiens()->contains($this->getCurrentUser()))
            && $this->getCurrentUser()->getAmap() === $amapDistribution->getAmapLivraisonLieu()->getAmap()
            && Carbon::now()->lt($amapDistribution->getDateDebut())
            && $amapDistribution->getAmapiens()->count() < $amapDistribution->getDetail()->getNbPersonnes()
        ;
    }

    public function isGrantedAmapienUnregister(AmapDistribution $amapDistribution): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && ($amapDistribution->getAmapiens()->contains($this->getCurrentUser()))
            && Carbon::now()->lt($amapDistribution->getDateDebut())
        ;
    }
}
