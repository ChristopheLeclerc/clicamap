# language: fr
Fonctionnalité: Envoi du mot de passe d'un regroupement depuis différents profils

  @database
  Plan du Scénario: Envoi du mot de passe du regroupement
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Et que je suis sur "/evenement"
    Quand je suis "Gestion des regroupements"
    Et que je suis "Envoyer un mot de passe au regroupement"
    Et que je presse "Confirmer l'envoi du mot de passe au regroupement"
    Alors je devrais voir "Mot de passe envoyé"
    Et un mail avec le sujet "Bienvenue sur Clic'AMAP" devrait avoir été envoyé à "regroupement@test.amap-aura.org"

    Exemples:
      | mail                          |
      | admin@test.amap-aura.org      |
      | admin.aura@test.amap-aura.org |
