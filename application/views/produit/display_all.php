<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
$currentUser = get_current_connected_user();
?>
<h3>Produits proposés</h3>

<?php

if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
    echo '<h5><a href="/ferme/home_paysan/">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.$ferme->getNom().'</a> <i class="glyphicon glyphicon-menu-right"></i> Produits proposés</h5>';
} else {
    echo '<h5><a href="'.site_url('ferme').'">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.$ferme->getNom().'</a> <i class="glyphicon glyphicon-menu-right"></i> Produits proposés</h5>';
}

$produits = $ferme->getProduits();
if (!$produits->isEmpty()) {
    echo '<p>';
    echo '<a href="'.site_url('produit/informations/'.$ferme->getId()).'" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Ajouter un nouveau produit</a>&nbsp;';
    echo '<a href="'.site_url('produit/download/'.$ferme->getId()).'" class="btn btn-info"><i class="glyphicon glyphicon-download"></i> Télécharger la liste des produits</a>';
    echo '</p>';

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // MISE EN TABLEAU CLASSIQUE DE TOUS LES PRODUITS INCLUS DS UN CONTRAT VIERGE ACTIF
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ?>

<div class="table-responsive">
    <table
            class="table table-hover table-datatable"
            data-order='[[ 0, "asc" ], [ 1, "asc" ]]'
    >
        <thead class="thead-inverse">
        <tr>
            <th>Type de Production</th>
            <th>Nom</th>
            <th>Conditionnement<br/>(Unité de livraison)</th>
            <th>Prix<br/>(Euros)</th>
            <th>Label</th>
            <th>Régularisation<br/>de poids ?</th>
            <th class="text-right">Outils</th>
        </tr>
        </thead>

        <tbody>
        <?php
        /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
        foreach ($produits as $produit) {
            echo '<tr>';

            echo '<th scope="row">';

            echo $produit->getTypeProduction()->getNomCompletParenthese();

            echo '</th>';

            echo '<th scope="row">';
            echo ucfirst($produit->getNom());
            echo '</th>';

            echo '<td>';
            echo ucfirst($produit->getConditionnement());
            echo '</td>';
            echo '<td>';
            echo number_format_french($produit->getPrix());
            echo '</td>';
            echo '<td>';
            echo array_search($produit->getCertification(), \PsrLib\ORM\Entity\FermeProduit::AVAILABLE_CERTIFICATIONS);
            echo '</td>';

            echo '<td>';
            if (true === $produit->isRegulPds()) {
                echo '<input type="checkbox" disabled checked="checked" />';
            } else {
                echo '<input type="checkbox" disabled />';
            }
            echo '</td>';

            echo '<td class="text-right">';

            echo '<a title="Modifier le produit" href="'.site_url('produit/informations/'.$ferme->getId().'/'.$produit->getId()).'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier le produit"></i></a> ';

            if (!empty($ferme) && is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_PRODUCT_DELETE, $ferme)) {
                echo '<a title="Supprimer le produit" href="#" data-toggle="modal" data-target="#modal_'.$produit->getId().'"><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le produit"></i></a>';
            }
            echo '</td>';
            echo '</tr>'; ?>
            <!-- Modal -->
            <div class="modal fade" id="modal_<?= $produit->getId(); ?>" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer le produit
                                "<?= ucfirst($produit->getNom()); ?>" ?</h5>
                        </div>
                        <div class="modal-body">
                            <p class="alert alert-danger">Etes-vous sûr de vouloir supprimer ce produit ?</p>
                            <p class="text-right">
                                <a href="<?= site_url('produit/remove/'.$produit->getId()); ?>"
                                   class="btn btn-danger btn-sm" title="confirmer la suppression">OUI</a>
                                <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }

    echo '</tbody>';
    echo '</table>';
    echo '</div>';
} else {
    echo '<p>';

    echo '<a href="/produit/informations/'.$ferme->getId().'" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Ajouter un nouveau produit</a>&nbsp;';

    echo '<a href="#" class="btn btn-info" disabled ><i class="glyphicon glyphicon-download"></i> Télécharger la liste des produits</a>';
    echo '</p>';
}

        ?>
