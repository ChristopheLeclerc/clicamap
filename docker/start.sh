#!/usr/bin/env bash

chmod -R 777 /var/www/html/application/writable
chmod -R 777 /var/www/html/application/logs
chmod -R 777 /var/www/html/public/uploads

composer install

# load DB
while ! echo exit | nc mariadb 3306; do sleep 1; done # Wait for server initialized
docker/fixtures.sh

# Launch chrome
/opt/google/chrome/chrome --headless --disable-gpu --no-sandbox --window-size="1920,1080" --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222 http://localhost &

# Default entrypoint
apache2-foreground
