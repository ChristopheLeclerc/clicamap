<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class AdhesionValueAmap extends AdhesionValue
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("ID de l’AMAP")
     *
     * @var ?string
     * @Assert\NotBlank(message="L'ID l'AMAP ne peut pas être vide.")
     * @Assert\Type(type="digit", message="L'ID de l'amap est constitué de chiffres")
     */
    private $amapId;

    public function getAmapId(): ?string
    {
        return $this->amapId;
    }

    public function setAmapId(?string $amapId): AdhesionValueAmap
    {
        $this->amapId = $amapId;

        return $this;
    }
}
