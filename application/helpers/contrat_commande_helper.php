<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;

if (!function_exists('render_contrat_commande_form')) {
    /**
     * @return mixed
     */
    function render_contrat_commande_form(
        PsrLib\ORM\Entity\ModeleContrat $contrat,
        string $cancel_url,
        string $validation_url = null,
        string $validation_label = 'Valider',
        array $existingCommand = [],
        string $prevalidationMessage = null
    ) {
        // Charge l'instance
        $CI = &get_instance();
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(\Doctrine\ORM\EntityManagerInterface::class);
        $livExclusions = $em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratProduitExclure::class)
            ->getByContrat($contrat)
        ;
        $ferme = $contrat->getFerme();
        $produits = $contrat->getProduits()->toArray();
        $dates = $contrat->getDates()->toArray();

        $dateInscriptionMin = $ferme->getPremiereDateLivrableAvecDelais(Carbon::now());

        return $CI->load->view(
            'includes/contrat_commande',
            [
                'existingCommand' => $existingCommand,
                'contrat' => $contrat,
                'produits' => $produits,
                'dates' => $dates,
                'cancel_url' => $cancel_url,
                'validation_url' => $validation_url,
                'validation_label' => $validation_label,
                'livExclusions' => $livExclusions,
                'dateInscriptionMin' => $dateInscriptionMin,
                'prevalidationMessage' => $prevalidationMessage,
            ],
            true
        );
    }
}
