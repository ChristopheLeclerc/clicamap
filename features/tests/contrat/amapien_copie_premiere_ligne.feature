# language: fr
Fonctionnalité: Pouvoir copier la premiere ligne lors de la souscription d'un contrat

  @database
  Scénario: En tant qu'amapien je doit pouvoir copier la premiere ligne d'un contrat pour faciliter sa souscription
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Et que je vais sur "/contrat_signe/contrat_own_new"
    Et je devrais voir "contrat 1"
    Quand je suis "Souscrire"
    Et que je remplis "commande[1][1]" avec "1"
    Et que je remplis "commande[1][2]" avec "2"
    Et que je presse "Copier la première ligne partout"
    Alors le champ "commande[2][1]" devrait contenir "1"
    Alors le champ "commande[2][2]" devrait contenir "2"
    Alors le champ "commande[18][1]" devrait contenir "1"
    Alors le champ "commande[18][2]" devrait contenir "2"
