<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
?>

<h3>Entête du contrat</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Création
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 1
</h5>

<form method="POST" action="">

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group">
                    <label for="nom" class="control-label">Nom du contrat :<span class="text-danger"> *</span></label>
                    <?= render_form_text_widget($form['nom']); ?>
                </div>

                <div class="form-group">
                    <label for="filiere" class="control-label">Type de production :<span class="text-danger"> *</span></label>
                    <?= render_form_textarea_widget($form['filiere']); ?>
                </div>

                <div class="form-group">
                    <label for="specificite" class="control-label">Informations complémentaires : </label>
                    <?= render_form_textarea_widget($form['specificite']); ?>
                </div>

                <div class="form-group">
                    <label for="livraison_lieu" class="control-label">Lieu de livraison :<span class="text-danger"> *</span></label>
                    <?= render_form_select_widget($form['livraisonLieu']); ?>

                </div>

            </div>
        </div>
    </div>

    <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>
    <a href="<?= site_url('contrat_vierge'); ?>" class="pull-right btn btn-default">Annuler</a>

</form>
