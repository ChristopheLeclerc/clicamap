<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * AkModeleContratDate.
 *
 * @ORM\Table(name="ak_modele_contrat_date", indexes={@ORM\Index(name="IND_CONTRAT_DATE_DATE", columns={"mc_d_date_livraison"}), @ORM\Index(name="IND_CONTRAT_DATE_ID", columns={"mc_d_id"})})
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\ModeleContratDateRepository")
 */
class ModeleContratDate
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="mc_d_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizard", "wizardContract"})
     */
    private $id;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="mc_d_date_livraison", type="date", nullable=true)
     * @Groups({"wizard"})
     */
    private $dateLivraison;

    /**
     * @var ModeleContrat | null
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\ModeleContrat", inversedBy="dates")
     * @ORM\JoinColumn(name="mc_d_fk_modele_contrat_id", referencedColumnName="mc_id")
     */
    private $modeleContrat;

    /**
     * @var ArrayCollection<ModeleContratProduitExclure>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ModeleContratProduitExclure", mappedBy="modeleContratDate", orphanRemoval=true, cascade="persist")
     */
    private $exclusions;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->exclusions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ModeleContratDate
    {
        $this->id = $id;

        return $this;
    }

    public function getDateLivraison(): ?DateTime
    {
        return $this->dateLivraison;
    }

    public function setDateLivraison(?DateTime $dateLivraison): ModeleContratDate
    {
        $this->dateLivraison = $dateLivraison;

        return $this;
    }

    public function getModeleContrat(): ?ModeleContrat
    {
        return $this->modeleContrat;
    }

    public function setModeleContrat(?ModeleContrat $modeleContrat): ModeleContratDate
    {
        $this->modeleContrat = $modeleContrat;

        return $this;
    }

    /**
     * Add exclusion.
     *
     * @return ModeleContratDate
     */
    public function addExclusion(ModeleContratProduitExclure $exclusion)
    {
        $exclusion->setModeleContratDate($this);
        $this->exclusions[] = $exclusion;

        return $this;
    }

    /**
     * Remove exclusion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeExclusion(ModeleContratProduitExclure $exclusion)
    {
        return $this->exclusions->removeElement($exclusion);
    }

    /**
     * Get exclusions.
     *
     * @return Collection
     */
    public function getExclusions()
    {
        return $this->exclusions;
    }
}
