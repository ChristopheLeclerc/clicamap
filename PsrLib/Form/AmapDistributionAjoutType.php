<?php

namespace PsrLib\Form;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\AmapDistributionAjout;
use PsrLib\Form\Type\PeriodType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Repository\AmapLivraisonLieuRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapDistributionAjoutType extends AbstractType
{
    /**
     * @var AmapLivraisonLieuRepository
     */
    private $amapLLRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->amapLLRepo = $em->getRepository(AmapLivraisonLieu::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?Amap $amap */
        $amap = $options['amap'];
        if (null === $amap) {
            throw new \LogicException('Amap cannot be null');
        }

        $lls = $this->amapLLRepo->qbByAmap($amap)->getQuery()->getResult();

        $builder
            ->add('amapLivraisonLieu', EntityType::class, [
                'label' => 'Lieu de livraison*',
                'class' => AmapLivraisonLieu::class,
                'choices' => $lls,
                'required' => false,
                'data' => 1 === count($lls) ? $lls[0] : null,
            ])
            ->add('detail', AmapDistributionDetailType::class, [
                'label' => false,
            ])
            ->add('period', PeriodType::class, [
                'label' => false,
            ])
            ->add('repetition', ChoiceType::class, [
                'label' => 'Répétition',
                'choices' => AmapDistributionAjout::REPETITIONS,
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                if (AmapDistributionAjout::REP_AUCUNE === $data['repetition']) {
                    $data['period']['endAt'] = $data['period']['startAt'];
                }
                $event->setData($data);
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'data_class' => AmapDistributionAjout::class,
        ]);
    }
}
