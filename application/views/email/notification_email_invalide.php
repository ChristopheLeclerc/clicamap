<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
Bonjour <br />
Un mail a été envoyé à l'adresse de l'utilisateur suivant. Celle-ci semble invalide. Les prochains envois à cet email sont désactivés. <br />
<?php foreach ($users as $user): ?>
    <ul>
        <li>Statut : <?= $user->getStatut(); ?></li>
        <li>Nom : <?= $user->getNom(); ?></li>
        <li>Email : <?= $user->getEmail(); ?></li>
    </ul>
<?php endforeach; ?>
