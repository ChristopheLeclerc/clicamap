<?php

namespace PsrLib\Services\Security;

interface SecurityCheckerInterface
{
    /**
     * @param mixed $subject
     */
    public function isGranted(string $action, $subject = null): bool;
}
