<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ak_amap_recherche_paysan")
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AmapRecherchePaysanRepository")
 */
class AmapRecherchePaysan
{
    /**
     * @var ?int
     * @ORM\Column(name="amap_r_pay_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var DateTime
     * @ORM\Column(name="amap_r_pay_debut", type="date")
     */
    private $debut;

    /**
     * @var DateTime
     * @ORM\Column(name="amap_r_pay_expiration", type="date")
     */
    private $fin;

    /**
     * @var ?Amap
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amap")
     * @ORM\JoinColumn(name="amap_r_pay_fk_amap_id", referencedColumnName="amap_id")
     */
    private $amap;

    public function __construct(Amap $amap)
    {
        $this->amap = $amap;
        $this->debut = Carbon::now();
        $this->fin = $this->debut->clone()->addMonths(6);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDebut(): DateTime
    {
        return $this->debut;
    }

    /**
     * @param DateTime $debut
     */
    public function setDebut($debut): AmapRecherchePaysan
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): DateTime
    {
        return $this->fin;
    }

    /**
     * @param DateTime $fin
     */
    public function setFin($fin): AmapRecherchePaysan
    {
        $this->fin = $fin;

        return $this;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap): AmapRecherchePaysan
    {
        $this->amap = $amap;

        return $this;
    }
}
