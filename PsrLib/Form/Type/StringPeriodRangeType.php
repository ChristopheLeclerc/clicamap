<?php

namespace PsrLib\Form\Type;

use PsrLib\DTO\StringPeriodRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StringPeriodRangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('stringPeriod', ChoiceType::class, [
                'choices' => StringPeriodRange::PERIODS,
                'label' => 'Période',
                'empty_data' => StringPeriodRange::PERIOD_MONTH_CURRENT,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => StringPeriodRange::class,
        ]);
    }
}
