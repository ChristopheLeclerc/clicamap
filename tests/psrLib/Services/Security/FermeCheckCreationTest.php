<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class FermeCheckCreationTest extends AbstractSecurityTest
{
    public function testCreateAmapien()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_CREATE));
    }

    public function testCreateAmapienAdmin()
    {
        $admin = $this->prophesize(Amapien::class);
        $admin->isAdmin()->willReturn(true);
        $this->mockAuthenticatedUser($admin->reveal());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_CREATE));
    }

    public function testCreateFermeRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_CREATE));
    }

    public function testCreateFermeAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_CREATE));
    }
}
