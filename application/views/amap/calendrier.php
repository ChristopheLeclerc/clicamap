<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @param mixed $cellules
 */

/** @var string $calendrier */
/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var array $livraisons */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu[] $livraison_lieu */

/**
 * @param \PsrLib\ORM\Entity\ModeleContrat    $mc
 * @param \PsrLib\ORM\Entity\ContratCellule[] $cellules
 */
function getMcDateFromMcCellules(PsrLib\ORM\Entity\ModeleContrat $mc, $cellules)
{
    foreach ($cellules as $cellule) {
        if ($cellule->getContrat()->getModeleContrat()->getId() === $mc->getId()) {
            return $cellule->getModeleContratDate();
        }
    }

    return null;
}
?>

<h3>Calendrier des livraisons</h3>

<div class="alert alert-info">Une date en vert indique une livraison, cliquer dessus pour voir le détail.</div>

<?php

echo '<div class="table-responsive calendrier">';
echo $calendrier;
echo '</div>';

if (isset($livraisons)) {
    $i = 0;

    foreach ($livraisons as $key => $jour) {
        echo '<div class="modal fade" id="modal_'.$key.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Livraisons du '.$key.'/'.$mois.'/'.$an.'</h4>
    </div>
    <div class="modal-body">';

        foreach ($livraison_lieu as $lieu) {
            echo '<h3 style="font-size:18px;">'.ucfirst($lieu->getNom()).' - '.$lieu->getAdresse().', '.$lieu->getVille()->getCpString().' '.$lieu->getVille()->getNom().'</h3>';

            $mc_nbr = 0;

            if (isset($jour['mc'])) {
                /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
                foreach ($jour['mc'] as $mc) {
                    $mc_quantites = 0;

                    if ($mc->getLivraisonLieu()->getId() == $lieu->getId()) {
                        ++$mc_nbr;

                        echo '<div class="well">';
                        echo '<h4>Contrat : <strong>'.ucfirst($mc->getNom()).'</strong></h4>';
                        echo '<h4>Ferme : <strong>'.ucfirst($mc->getFerme()->getNom()).'</strong></h4>';
                        $mcDate = getMcDateFromMcCellules($mc, $jour['cellules']);
                        if (null !== $mcDate) {
                            echo '<h4>Livraisons '
                                .$mc->getDatePosition($mcDate)
                                .' / '
                                .count($mc->getDates()->toArray()).'</strong></h4>'
                            ;
                        }

                        // Produits liés au MC
                        /** @var \PsrLib\ORM\Entity\ModeleContratProduit $produit */
                        foreach ($mc->getProduits() as $produit) {
                            $quantites = null;
                            // Quantités liées au produit => addition
                            /** @var \PsrLib\ORM\Entity\ContratCellule $quantite */
                            foreach ($jour['cellules'] as $quantite) {
                                if ($produit->getId() == $quantite->getModeleContratProduit()->getId()) {
                                    $quantites += $quantite->getQuantite();
                                }
                            }
                            // Somme
                            if ($quantites) {
                                if (0 == $mc_quantites) {
                                    echo '<h4>Quantités totales à livrer :</h4>';
                                    // Liste quantités totales des produits à livrer
                                    echo '<ul>';
                                }
                                echo '<li>'.ucfirst($produit->getNom()).' ('.$produit->getConditionnement().' / '.$produit->getPrix().' €) : <strong>'.$quantites.'</strong></li>';

                                ++$mc_quantites;
                            }
                        }

                        if ($mc_quantites) {
                            echo '</ul>';

                            // Liste des produits à livrer par AMAPIEN
                            echo '<button class="btn btn-primary btn-xs" data-toggle="collapse" data-target="#amapiens_'.$i.'">Détail par amapien</button>';

                            echo '<div id="amapiens_'.$i.'" class="alert alert-success collapse" style="padding-top:5px;">';

                            /** @var \PsrLib\ORM\Entity\Amapien $amapien */
                            foreach ($amap->getAmapiens() as $amapien) {
                                // Entête
                                $n = 0;

                                /** @var \PsrLib\ORM\Entity\ContratCellule $quantite */
                                foreach ($jour['cellules'] as $quantite) {
                                    $celluleMc = $quantite->getModeleContratProduit()->getModeleContrat();
                                    if ($celluleMc->getId() === $mc->getId() && $quantite->getContrat()->getAmapien()->getId() === $amapien->getId()) {
                                        if (0 == $n) {
                                            echo '<p style="margin-top:10px;"><strong>'.$amapien->getNom().' '.$amapien->getPrenom().'</strong></p>';
                                        }

                                        if (0.0 !== $quantite->getQuantite()) {
                                            echo '<ul>';
                                            echo '<li>'.ucfirst($quantite->getModeleContratProduit()->getNom()).' ('.$quantite->getModeleContratProduit()->getConditionnement().' / '.$quantite->getModeleContratProduit()->getPrix().' €) : <strong>'.$quantite->getQuantite().'</strong></li>';
                                            echo '</ul>';
                                        }

                                        ++$n;
                                    }
                                }
                            }

                            echo '</div>';
                        }

                        if (0 == $mc_quantites) {
                            echo '<div class="alert alert-warning">Il n\'y a pas encore de souscriptions à ce contrat.</div>';
                        }

                        ++$i;

                        echo '</div>';
                    }
                }

                if (0 == $mc_nbr) {
                    echo '<div class="alert alert-warning">Il n\'y a pas encore de commande pour cette date et ce lieu de livraison.</div>';
                }
            } else {
                echo '<div class="alert alert-warning">Il n\'y a pas encore de commande pour cette date.</div>';
            }
        }

        echo '</div>
    </div>
    </div>
    </div>';
    }
}
