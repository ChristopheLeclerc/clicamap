<?php

namespace PsrLib\Services\ServiceFactories;

use DebugBar\Bridge\DoctrineCollector;
use DebugBar\JavascriptRenderer;
use DebugBar\StandardDebugBar;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\ORM\EntityManagerInterface;

class DebugBarFactory
{
    public function create(EntityManagerInterface $em): JavascriptRenderer
    {
        $debugBar = new StandardDebugBar();

        $debugStack = new DebugStack();
        $em->getConnection()->getConfiguration()->setSQLLogger($debugStack);
        $debugBar->addCollector(new DoctrineCollector($debugStack));

        return $debugBar->getJavascriptRenderer();
    }
}
