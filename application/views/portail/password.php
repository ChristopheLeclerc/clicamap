<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<h3>Mot de passe</h3>

<div class="alert alert-info">Sélectionnez un mot de passe efficace et ne le réutilisez pas pour d'autres comptes.
    Utilisez au moins 8 caractères.
</div>

<form method="POST" action="">

    <div class="well">
        <div class="row">
            <div class="col-sm-12">

                <div class="form-group col-sm-6">
                    <input class="form-control" type="password" name="password_1" placeholder="Nouveau mot de passe"
                           value="<?= set_value('password_1'); ?>"/>

                    <?= form_error('password_1'); ?>
                </div>

                <div class="form-group col-sm-6">
                    <input class="form-control" type="password" name="password_2"
                           placeholder="confirmer le nouveau mot de passe"
                           value="<?= set_value('password_2'); ?>"/>
                    <?php
                    if ($alerte_password_2) {
                        echo '<div class="alert alert-danger">'.$alerte_password_2.'</div>';
                    }

                    echo form_error('password_2');
                    ?>
                </div>

            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Modifier le mot de passe</button>

</form>
