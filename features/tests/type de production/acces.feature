# language: fr
Fonctionnalité: Comme utilisateur, je devrais pouvoir accéder à la gestion du type de production d'une ferme

  Scénario: Accès par le menu comme paysan
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Quand je suis "Mon compte"
    Et je suis "Ma ferme"
    Et je suis "Types de production"
    Alors je devrais voir "Types de production"
    Et je devrais être sur "/ferme/type_production_display/1"

  Plan du Scénario: Accès par le menu comme admin
    Etant donné que je suis authentifié comme "<statut>" avec le mail "<email>"
    Quand je suis "Gestionnaire"
    Et je suis "Gestion des fermes"
    Et je sélectionne "AUVERGNE-RHÔNE-ALPES" depuis "search_ferme[region]"
    Et je suis "Types de production"
    Alors je devrais voir "Types de production"
    Et je devrais être sur "/ferme/type_production_display/1"

    Exemples:
      | statut  | email                    |
      | amapien | admin@test.amap-aura.org |
