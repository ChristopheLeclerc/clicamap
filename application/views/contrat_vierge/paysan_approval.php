<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
?>
<h3>Mes contrats vierges</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Mes contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Validation du contrat vierge
</h5>

<div class="row">
    <div class="col-xs-12">
        <?php
        $modeleContrat = $contrat;

        include __DIR__.'/../contrat_signe/_info_modele_contrat.php';
        ?>
    </div>
    <div class="col-xs-12">
        <h4 class="mb-2 mt-1">
            <div class="alert alert-danger">
              Vous vous apprêtez à vous engager dans le cadre d'un partenariat avec des amapiens.<br/>Lisez le contrat ci-dessous, acceptez les conditions puis validez, sinon il ne sera pas pris en compte.
            </div>
        </h4>
    </div>
    <div class="col-xs-12">
        <?= pdf_embedded(site_url('/contrat_vierge/v2_preview_pdf/'.$contrat->getId())); ?>

        <form method="POST">
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input
                            type="checkbox"
                            name="confirm-sign"
                            value="1"
                            <?= set_checkbox('confirm-sign', '1'); ?>
                        > <b>J'accepte les termes du <a href="<?= site_url('/contrat_vierge/v2_preview_pdf/'.$contrat->getId()); ?>" target="_blank"> contrat juridique</a></b>
                    </label>
                </div>
                <?= form_error('confirm-sign'); ?>
                <div class="checkbox">
                    <label>
                        <input
                            type="checkbox"
                            name="confirm-charter"
                            value="1"
                            <?= set_checkbox('confirm-charter', '1'); ?>
                        ><b> Je confirme avoir pris connaissance de la <a href="https://amap-aura.org/la-charte-des-amap/" target="_blank">Charte des AMAP</a></b>
                    </label>
                </div>
                <?= form_error('confirm-charter'); ?>

                <p>Une fois <b>validé</b>, ce contrat juridique reste disponible au format PDF : l'impression du contrat n'est pas nécessaire.</p>

                <button type="submit" class="btn btn-success pull-right" style="margin-left: 10px">Valider</button>
                <a
                    href="#"
                    class="btn btn-danger pull-right"
                    data-toggle="modal"
                    data-target="#alerte"
                >Refuser</a>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="alerte">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Motif du refus</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="<?= site_url('/contrat_vierge/v2_state_paysan_refuse/'.$contrat->getId()); ?>">
                    <div class="form-goup" style="margin-bottom: 10px">
                        <textarea
                            class="form-control"
                            name="refus"
                            placeholder="Ecrire votre message"
                            rows="10"
                            required
                        ></textarea>
                    </div>
                    <div class="container-fluid" style="padding-right: 0">
                        <button class="btn btn-success pull-right" >Envoyer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
