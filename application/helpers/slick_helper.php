<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use PsrLib\ORM\Entity\EntityWithLogoInterface;

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('render_slick_logo_reseaux')) {
    /**
     * Fait le rendu de la bannière avec les logos des réseaux
     * Attention : les fichiers CSS & JS de slick slider doivent etre chargés sur la page.
     */
    function render_slick_logo_reseaux()
    {
        // Récupère les régions avec logo
        $CI = &get_instance();
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(\Doctrine\ORM\EntityManagerInterface::class);

        $entites = array_merge(
            $em->getRepository(\PsrLib\ORM\Entity\Region::class)->findWithLogo(),
            $em->getRepository(\PsrLib\ORM\Entity\Departement::class)->findWithLogo(),
            $em->getRepository(\PsrLib\ORM\Entity\Reseau::class)->findWithLogo()
        );

        // Tri par ordre alphabétique
        usort($entites, function (EntityWithLogoInterface $a, EntityWithLogoInterface $b) {
            return strnatcmp(strtolower($a->getNom()), strtolower($b->getNom()));
        });

        // Formate pour l'impression
        $logos = array_map(function (EntityWithLogoInterface $entite) {
            return [
                'nom' => $entite->getNom(),
                'file' => $entite->getLogo()->getFileRelativePath(),
                'url' => $entite->getUrl(),
            ];
        }, $entites);

        return $CI->load->view('includes/slick_logo_reseaux', ['logos' => $logos], true);
    }
}
