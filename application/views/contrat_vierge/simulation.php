<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
$ferme = $contrat->getFerme();
$livraison_lieu = $contrat->getLivraisonLieu();
/** @var string $validation_error */
?>

<h3>Simulation</h3>

<?php
$modeleContrat = $contrat;

include __DIR__.'/../contrat_signe/_info_modele_contrat.php';
?>

<?php if (null !== $validation_error): ?>
    <div class="col-md-12">
	<div class="alert alert-danger">
            <span><?=$validation_error; ?></span>
	</div>
    </div>
<?php endif; ?>

<div class="well well-sm">
    <?=render_contrat_commande_form($contrat, site_url('/contrat_vierge'), site_url('/'.uri_string()), 'Tester'); ?>
</div>
