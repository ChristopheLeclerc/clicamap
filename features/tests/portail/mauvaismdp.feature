# language: fr
Fonctionnalité: Comme utilisateur, je souhaite me connecter mais je me suis trompé en écrivant mon mot de passe

  Plan du Scénario: Authentification échouée à cause d'un mauvais mot de passe
    Etant donné que je suis sur "/"
    Quand je sélectionne "amapien" depuis "statut"
    Et remplis le texte suivant:
      | email    | <email>  |
      | password | échec    |
      | statut   | <statut> |
    Et je presse "Connexion"
    Alors je devrais voir "<message>"

    Exemples:
      | email                           | statut       | message                                                         |
      | amapien@test.amap-aura.org      | amapien      | L'adresse email et le mot de passe saisis ne correspondent pas. |
      | admin@test.amap-aura.org        | amapien      | L'adresse email et le mot de passe saisis ne correspondent pas. |
      | amapienref@test.amap-aura.org   | amapien      | L'adresse email et le mot de passe saisis ne correspondent pas. |
      | paysan@test.amap-aura.org       | paysan       | L'adresse email et le mot de passe saisis ne correspondent pas. |
      | amap@test.amap-aura.org         | amapien      | L'adresse email et le mot de passe saisis ne correspondent pas. |
      | regroupement@test.amap-aura.org | regroupement | L'adresse email et le mot de passe saisis ne correspondent pas. |
