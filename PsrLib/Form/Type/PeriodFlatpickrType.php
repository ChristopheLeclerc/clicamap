<?php

namespace PsrLib\Form\Type;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PeriodFlatpickrType extends AbstractType
{
    public const REGEX = '~(\d{2})\/(\d{2})\/(\d{4}) au (\d{2})\/(\d{2})\/(\d{4})~';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addModelTransformer(new CallbackTransformer(
                function (?Period $period) {
                    if (null === $period) {
                        return '';
                    }

                    return $this->periodToString($period);
                },
                function ($input) {
                    if (!is_string($input)) {
                        return null;
                    }

                    $matches = [];
                    if (1 !== preg_match(self::REGEX, $input, $matches)) {
                        return null;
                    }

                    return Period::buildFromDates(
                        Carbon::create((int) $matches[3], (int) $matches[2], (int) $matches[1]),
                        Carbon::create((int) $matches[6], (int) $matches[5], (int) $matches[4])
                    );
                }
            ))

            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($options) {
                $form = $event->getForm();
                $data = $form->getData();
                if (null === $data) {
                    return;
                }

                if (null === $options['min_date']) {
                    return;
                }

                Assertion::isInstanceOf($options['min_date'], Carbon::class);

                // endAt is after start garanted by validation
                if ($options['min_date']->clone()->startOfDay()->gt($data->getStartAt())) {
                    $event
                        ->getForm()
                        ->addError(new FormError('La date de début doit être après la date minimale'))
                    ;
                }
            })
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        if ($options['min_date'] instanceof Carbon) {
            $view->vars['attr']['data-min-date'] = $options['min_date']->format('Y-m-d');
        }
    }

    public function getParent(): string
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
            'attr' => [
                'class' => 'flatpickr_range',
            ],
            'empty_data' => $this->periodToString(
                Period::buildFromDates(
                    Carbon::now(),
                    Carbon::now()->addMonths(3)
                )
            ),
            'min_date' => null,
        ]);
    }

    private function periodToString(Period $period): string
    {
        $periodStart = $period->getStartAt();
        $periodEnd = $period->getEndAt();
        if (null === $periodStart || null === $periodEnd) {
            return '';
        }

        return sprintf(
            '%02d/%02d/%04d au %02d/%02d/%04d',
            $periodStart->day,
            $periodStart->month,
            $periodStart->year,
            $periodEnd->day,
            $periodEnd->month,
            $periodEnd->year
        );
    }
}
