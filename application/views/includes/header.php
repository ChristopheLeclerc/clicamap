<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title><?= SITE_NAME; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?= base_url('assets/images/favicon.ico'); ?>" />

    <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.css'); ?>">

    <link href="<?= base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/bootstrap/css/datepicker.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/fontawesome/css/all.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet"/>
    <link href="<?= base_url('assets/plugins/flatpickr/flatpickr.min.css'); ?>" rel="stylesheet"/>

    <link href="<?= base_url('assets/plugins/leaflet/leaflet.css'); ?>" rel="stylesheet"/>
    <script src="<?= base_url('assets/plugins/leaflet/leaflet.js'); ?>"></script>

    <script type="text/javascript" src="<?= base_url('assets/jquery/jquery-2.2.3.min.js'); ?>" rel="stylesheet"></script>
    <script type="text/javascript" src="<?= base_url('assets/jquery/jquery-ui.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/bootstrap/js/bootstrap-datepicker.js'); ?>" rel="stylesheet"></script>
    <script type="text/javascript" src="<?= base_url('assets/bootstrap/js/bootstrap.js'); ?>" rel="stylesheet"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/jquery-debounce/jquery.ba-throttle-debounce.min.js'); ?>" rel="stylesheet"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/jquery-mask/jquery.mask.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/bootbox/bootbox.all.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/select2/js/select2.min.js'); ?>"></script>

    <script type="text/javascript" src="<?= base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
    <link href="<?= base_url('assets/plugins/summernote/summernote.min.css'); ?>" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables/datatables.min.css'); ?>"/>
    <script type="text/javascript" src="<?= base_url('assets/plugins/moment/moment.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/datatables/datatables.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/datatables/datetime-moment.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/datatables/datatables.init.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/uri/URI.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/flatpickr/flatpickr.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/plugins/flatpickr/fr.js'); ?>"></script>


    <script type="text/javascript" src="<?= base_url('assets/javascript/custom.js'); ?>"></script>

    <style type="text/css">

        a:focus {
            outline: 0 !important
        }

    </style>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

      $(function () {
        $('.datepicker').datepicker();
      });
    </script>

    <?= render_debug_bar_header(); ?>

</head>
<body>
