# language: fr
Fonctionnalité: Accès à la fonctionnalité d'import

  Plan du Scénario: Accès non autorisé pour l'amapien
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Quand je suis "Mon compte"
    Alors je devrais voir "Mes reçus"
    Quand je suis "Mes reçus"
    Alors je ne devrais pas voir "Import"

    Exemples:
      | type | mail                            |
      | amapien | amapien@test.amap-aura.org   |

