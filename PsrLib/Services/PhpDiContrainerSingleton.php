<?php

namespace PsrLib\Services;

use CI_Config;
use CI_Input;
use CI_Loader;
use CI_Parser;
use DebugBar\JavascriptRenderer;
use DI\Container;
use DI\ContainerBuilder;
use function DI\factory;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ProjectLocation;
use PsrLib\Services\Security\SecurityCheckerInterface;
use PsrLib\Services\ServiceFactories\DebugBarFactory;
use PsrLib\Services\ServiceFactories\DoctrineFactory;
use PsrLib\Services\ServiceFactories\FormFactoryFactory;
use PsrLib\Services\ServiceFactories\MailerFactory;
use PsrLib\Services\ServiceFactories\SecurityCheckerFactory;
use PsrLib\Services\ServiceFactories\SerializerFactory;
use PsrLib\Services\ServiceFactories\SessionFactory;
use PsrLib\Services\ServiceFactories\TranslatorFactory;
use PsrLib\Services\ServiceFactories\TwigEnvironmentFactory;
use PsrLib\Services\ServiceFactories\ValidatorFactory;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\ExpressionValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

/**
 * Main container class. Used for instanciation of all libraries in PsrLib subfolder.
 * Use Singleton design pattern as used in multiple part of the code.
 */
class PhpDiContrainerSingleton
{
    /**
     * @var null|Container
     */
    private static $container = null;

    public static function getContainer(): Container
    {
        if (null === self::$container) {
            $builder = new ContainerBuilder();
            $builder->addDefinitions([
                FormFactoryInterface::class => factory([FormFactoryFactory::class, 'create']),
                EntityManagerInterface::class => factory([DoctrineFactory::class, 'create']),
                ValidatorInterface::class => factory([ValidatorFactory::class, 'create']),
                SerializerInterface::class => factory([SerializerFactory::class, 'create']),
                Environment::class => factory([TwigEnvironmentFactory::class, 'create']),
                Translator::class => factory([TranslatorFactory::class, 'create']),
                JavascriptRenderer::class => factory([DebugBarFactory::class, 'create']),
                MailerInterface::class => factory([MailerFactory::class, 'create']),
                Session::class => factory([SessionFactory::class, 'create']),
                SecurityCheckerInterface::class => factory([SecurityCheckerFactory::class, 'create']),
                CI_Loader::class => function () {
                    /** @phpstan-ignore-next-line */
                    $CI = &get_instance();

                    return $CI->load;
                },
                CI_Config::class => function () {
                    /** @phpstan-ignore-next-line */
                    $CI = &get_instance();

                    return $CI->config;
                },
                CI_Input::class => function () {
                    /** @phpstan-ignore-next-line */
                    $CI = &get_instance();

                    return $CI->input;
                },
                CI_Parser::class => function () {
                    /** @phpstan-ignore-next-line */
                    $CI = &get_instance();
                    $CI->load->library('parser');

                    return $CI->parser;
                },
                'validator.expression' => new ExpressionValidator(), // Fix for contraint validator factory usage
            ]);
            $builder->useAnnotations(true);

            if ('production' === getenv('CI_ENV')) {
                $basePath = ProjectLocation::PROJECT_ROOT.'/application/writable/cache/di/';
                $proxiesPath = $basePath.'proxies';
                $cachePath = $basePath.'cache';
                $fs = new Filesystem();
                $fs->mkdir([$proxiesPath, $cachePath]);
                $builder->writeProxiesToFile(true, $proxiesPath);
                $builder->enableCompilation($cachePath);
            }
            self::$container = $builder->build();
        }

        return self::$container;
    }

    /**
     * Force container regeneration on next request. Use on unit testing to avoid side effects.
     */
    public static function cleanContainer(): void
    {
        self::$container = null;
    }
}
