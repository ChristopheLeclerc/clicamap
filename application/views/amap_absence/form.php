<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\AmapAbsence $absence */
$amap = $absence->getAmap();
/** @var \Symfony\Component\Form\FormView $form */
?>

<h3>Le planning des absences</h3>

<h5>
    Gestionnaire <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('amap'); ?>">Gestion de mon AMAP </a><i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('amap_absence/liste/'.$amap->getId()); ?>">Le planning des absences </a><i class="glyphicon glyphicon-menu-right"></i>
   Nouvelle absence
</h5>


<?= form_open(''); ?>

    <?= render_form_global_errors($form); ?>
    <div class="form-group">
        <label for="nom" class="col-xs-12">Titre *</label>
        <?= render_form_text_widget($form['titre']); ?>
        <p class="help-block">Ex: Vacances de la Toussaint</p>
    </div>

    <div class="form-group">
        <label for="nom" class="col-xs-12">Date de début</label>
        <?= render_form_datepicker_widget($form['absenceDebut']); ?>
    </div>

    <div class="form-group">
        <label for="nom" class="col-xs-12">Date de fin</label>
        <?= render_form_datepicker_widget($form['absenceFin']); ?>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right">Sauvegarder</button>
            <a href="<?= site_url('amap_absence/liste/'.$amap->getId()); ?>" class="btn btn-default pull-right" style="margin-right:5px;">Annuler</a>
        </div>
    </div>

<?= form_close(); ?>
