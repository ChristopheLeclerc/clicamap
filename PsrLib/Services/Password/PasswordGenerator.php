<?php

namespace PsrLib\Services\Password;

use Assert\Assertion;

class PasswordGenerator
{
    public const POOL = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * @ref https://stackoverflow.com/a/31284266
     *
     * @param mixed $length
     */
    public function generatePassword($length = 8): string
    {
        Assertion::min($length, 1);

        $str = '';
        $max = mb_strlen(self::POOL, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= self::POOL[random_int(0, $max)];
        }

        return $str;
    }
}
