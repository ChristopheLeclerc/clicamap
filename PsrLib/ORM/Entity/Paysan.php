<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Annotation\DraftPropertie;
use PsrLib\Validator\UniqueEntity;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ak_paysan", uniqueConstraints={@ORM\UniqueConstraint(name="pay_mdpreinit_token", columns={"pay_mdpreinit_token"})})
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\PaysanRepository")
 * @UniqueEntity(fields={"email"})
 */
class Paysan extends BaseUser implements UserWithFermes
{
    public const ETAT_ACTIF = 'actif';
    public const ETAT_INACTIF = 'inactif';
    public const ETAT_AVALIABLE = [
        self::ETAT_ACTIF, self::ETAT_INACTIF,
    ];

    public const STATUT_EI = 'ei';
    public const STATUT_GROUPEMENT = 'groupement';

    /**
     * @var int
     *
     * @ORM\Column(name="pay_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_prenom", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $prenom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_nom", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $nom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_conjoint_prenom", type="string", length=255, nullable=true)
     */
    private $conjointPrenom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_conjoint_nom", type="string", length=255, nullable=true)
     */
    private $conjointNom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_email", type="string", length=255, nullable=true)
     * @Assert\Email
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_num_tel_1", type="string", length=50, nullable=true)
     */
    private $numTel1;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_num_tel_2", type="string", length=50, nullable=true)
     */
    private $numTel2;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_lib_adr", type="string", length=255, nullable=true)
     */
    private $libAdr;

    /**
     * @var Ville | null
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ville")
     * @ORM\JoinColumn(referencedColumnName="v_id")
     */
    private $ville;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_annee_deb_commercialisation_amap", type="string", length=4, nullable=true)
     * @Assert\Length(min="4", max="4")
     * @Assert\Type(type="digit")
     */
    private $anneeDebCommercialisationAmap;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="pay_date_installation", type="date", nullable=true)
     */
    private $dateInstallation;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_msa", type="string", length=255, nullable=true)
     */
    private $msa;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_statut", type="string", length=255, nullable=true)
     */
    private $statut;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_spg", type="string", length=4, nullable=true)
     * @Assert\Length(min="4", max="4")
     * @Assert\Type(type="digit")
     */
    private $spg;

    /**
     * @var bool
     *
     * @ORM\Column(name="newsletter", type="boolean", nullable=false)
     */
    private $newsletter = true;

    /**
     * @var string
     *
     * @ORM\Column(name="pay_etat", type="string", length=50, nullable=false)
     */
    private $etat = Paysan::ETAT_INACTIF;

    /**
     * @DraftPropertie()
     *
     * @var null|string
     *
     * @ORM\Column(name="pay_mdpreinit_token", type="string", length=255, nullable=true)
     */
    private $payMdpreinitToken;

    /**
     * @DraftPropertie()
     *
     * @var null|DateTime
     *
     * @ORM\Column(name="pay_mdpreinit_creationdate", type="datetime", nullable=true)
     */
    private $payMdpreinitCreationdate;

    /**
     * @var bool
     *
     * @ORM\Column(name="pay_email_invalid", type="boolean")
     */
    private $emailInvalid = false;

    /**
     * @var null|Ferme
     * @ORM\ManyToOne (targetEntity="PsrLib\ORM\Entity\Ferme", inversedBy="paysans", cascade="PERSIST")
     * @ORM\JoinColumn(referencedColumnName="f_id")
     */
    private $ferme;

    /**
     * @var PaysanCadreReseau | null
     * @ORM\OneToOne(targetEntity="\PsrLib\ORM\Entity\PaysanCadreReseau", mappedBy="paysan", cascade={"remove", "persist"})
     */
    private $cadreReseau;

    /**
     * @var ArrayCollection<PaysanCommentaire>
     * @ORM\OneToMany(targetEntity="PaysanCommentaire",mappedBy="paysan")
     */
    private $commentaires;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_uuid", type="string", length=255, nullable=true)
     */
    private $uuid;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->commentaires = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
    }

    public function __toString()
    {
        return strtoupper($this->getNom()).' '.ucfirst($this->getPrenom());
    }

    public function getFermes()
    {
        return new ArrayCollection([$this->getFerme()]);
    }

    public function getRegion(): ?Region
    {
        return null === $this->getFerme() ? null : $this->getFerme()->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return null === $this->getFerme() ? null : $this->getFerme()->getDepartement();
    }

    /**
     * Verifie si le paysan est actif.
     */
    public function estActif(): bool
    {
        return self::ETAT_ACTIF === $this->getEtat();
    }

    public function getNomCompletInverse(): string
    {
        return $this->getNom().' '.$this->getPrenom();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Paysan
    {
        $this->email = $email;

        return $this;
    }

    public function getEtat(): string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): Paysan
    {
        $this->etat = $etat;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): Paysan
    {
        $this->password = $password;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): Paysan
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Paysan
    {
        $this->nom = $nom;

        return $this;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): Paysan
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getNumTel1(): ?string
    {
        return $this->numTel1;
    }

    public function setNumTel1(?string $numTel1): Paysan
    {
        $this->numTel1 = $numTel1;

        return $this;
    }

    public function getNumTel2(): ?string
    {
        return $this->numTel2;
    }

    public function setNumTel2(?string $numTel2): Paysan
    {
        $this->numTel2 = $numTel2;

        return $this;
    }

    public function getLibAdr(): ?string
    {
        return $this->libAdr;
    }

    public function setLibAdr(?string $libAdr): Paysan
    {
        $this->libAdr = $libAdr;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): Paysan
    {
        $this->ville = $ville;

        return $this;
    }

    public function isNewsletter(): bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): Paysan
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getAnneeDebCommercialisationAmap(): ?string
    {
        return $this->anneeDebCommercialisationAmap;
    }

    public function setAnneeDebCommercialisationAmap(?string $anneeDebCommercialisationAmap): Paysan
    {
        $this->anneeDebCommercialisationAmap = $anneeDebCommercialisationAmap;

        return $this;
    }

    public function getSpg(): ?string
    {
        return $this->spg;
    }

    public function setSpg(?string $spg): Paysan
    {
        $this->spg = $spg;

        return $this;
    }

    public function getCadreReseau(): ?PaysanCadreReseau
    {
        return $this->cadreReseau;
    }

    public function setCadreReseau(?PaysanCadreReseau $cadreReseau): Paysan
    {
        $this->cadreReseau = $cadreReseau;
        if (null !== $cadreReseau) {
            $cadreReseau->setPaysan($this);
        }

        return $this;
    }

    /**
     * Add commentaire.
     *
     * @return Paysan
     */
    public function addCommentaire(PaysanCommentaire $commentaire)
    {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    /**
     * Remove commentaire.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCommentaire(PaysanCommentaire $commentaire)
    {
        return $this->commentaires->removeElement($commentaire);
    }

    /**
     * Get commentaires.
     *
     * @return Collection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    public function getConjointPrenom(): ?string
    {
        return $this->conjointPrenom;
    }

    public function setConjointPrenom(?string $conjointPrenom): Paysan
    {
        $this->conjointPrenom = $conjointPrenom;

        return $this;
    }

    public function getConjointNom(): ?string
    {
        return $this->conjointNom;
    }

    public function setConjointNom(?string $conjointNom): Paysan
    {
        $this->conjointNom = $conjointNom;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): Paysan
    {
        $this->statut = $statut;

        return $this;
    }

    public function getMsa(): ?string
    {
        return $this->msa;
    }

    public function setMsa(?string $msa): Paysan
    {
        $this->msa = $msa;

        return $this;
    }

    public function getDateInstallation(): ?DateTime
    {
        return $this->dateInstallation;
    }

    public function setDateInstallation(?DateTime $dateInstallation): Paysan
    {
        $this->dateInstallation = $dateInstallation;

        return $this;
    }

    public function isEmailInvalid(): bool
    {
        return $this->emailInvalid;
    }

    public function setEmailInvalid(bool $emailInvalid): Paysan
    {
        $this->emailInvalid = $emailInvalid;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUuid()
    {
        return $this->uuid;
    }
}
