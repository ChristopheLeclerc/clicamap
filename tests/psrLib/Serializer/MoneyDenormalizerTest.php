<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Serializer;

use Money\Money;
use PHPUnit\Framework\TestCase;
use PsrLib\Exception\AdhesionDecodeException;
use PsrLib\Serializer\MoneyDenormalizer;

/**
 * @internal
 * @coversNothing
 */
class MoneyDenormalizerTest extends TestCase
{
    /**
     * @var MoneyDenormalizer
     */
    private $denormalizer;

    public function setUp()
    {
        $this->denormalizer = new MoneyDenormalizer();
    }

    /**
     * @dataProvider denormalizeExceptionProvider
     *
     * @param mixed $input
     */
    public function testDenormalizeException($input)
    {
        $this->expectException(AdhesionDecodeException::class);
        $this->denormalizer->denormalize($input, Money::class);
    }

    public function denormalizeExceptionProvider()
    {
        return [
            [null],
            [0],
            [1.12],
            ['abcd'],
            ['123abcd'],
            ['123.12abcd'],
            ['aaa.12'],
        ];
    }

    /**
     * @dataProvider denormalizeProvider
     */
    public function testDenormalize(string $input, Money $expected)
    {
        $denormalized = $this->denormalizer->denormalize($input, Money::class);
        self::assertTrue($expected->equals($denormalized));
    }

    public function denormalizeProvider()
    {
        return [
            ['123', Money::EUR('12300')],
            ['123.45', Money::EUR('12345')],
            ['0.45', Money::EUR('45')],
        ];
    }
}
