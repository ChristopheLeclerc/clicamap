<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use DI\Annotation\Inject;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Paysan.
 */
class Paysan extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordEncoder
     */
    public $passwordEncoder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordGenerator
     */
    public $passwordGenerator;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Exporters\ExcelGeneratorExportPaysan
     */
    public $excelGeneratorExportPaysan;

    /**
     * @var array
     */
    protected $data;

    /**
     * Paysan constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['paysan_all' => null,
            'remove' => null,
            'create' => null,
            'edit' => null,
            'key' => null,
            'key_all' => null,
            'alertes' => null,
            'paysan' => [],
            'paysan_id' => null,
            'reseau_all' => [],
            'ferme' => null, ];

        $this->load->helper('array');
        $this->load->helper('string');

        $this->load->library('email');
        $this->load->library('excel');
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    /**
     * Index.
     */
    public function index()
    {
        $currentUser = $this->getUser();
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
            $this->home_refprod($currentUser);

            return;
        }

        if ($currentUser instanceof \PsrLib\ORM\Entity\FermeRegroupement) {
            $this->home_regroupement($currentUser);

            return;
        }

        $this->accueil();
    }

    /*
     *
     * Accueil / Moteur de recherche.
     *
     * @param bool $off
     */
    public function accueil()
    {
        // Sécurité
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_CREATE);

        $this->redirectFromStoredGetParamIfExist('paysan');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchPaysanType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->search($searchForm->getData())
        ;

        $this->loadViewWithTemplate('paysan/display_all', [
            'paysans' => $paysans,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * Afficher le détail d'une fiche paysan.
     *
     * @param $paysan_id
     */
    public function display($paysan_id)
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DISPLAY_PROFIL, $paysan);

        $this->loadViewWithTemplate('paysan/display', [
            'paysan' => $paysan,
        ]);
    }

    /**
     *  Ajouter un commentaire daté.
     *
     * @param $paysan_id
     */
    public function paysan_commentaire_add($paysan_id)
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_COMMENT, $paysan);

        $commentaire = new \PsrLib\ORM\Entity\PaysanCommentaire($paysan);
        $commentaire->setCommentaire($this->input->post('paysan_commentaire'));
        $this->em->persist($commentaire);
        $this->em->flush();

        redirect('/paysan/display/'.$paysan->getId());
    }

    /**
     * Ajouter un commentaire daté.
     *
     * @param $commentaire_id
     */
    public function paysan_commentaire_remove($commentaire_id)
    {
        /** @var \PsrLib\ORM\Entity\PaysanCommentaire $commentaire */
        $commentaire = $this->findOrExit(\PsrLib\ORM\Entity\PaysanCommentaire::class, $commentaire_id);
        $paysan = $commentaire->getPaysan();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_COMMENT, $paysan);

        $this->em->remove($commentaire);
        $this->em->flush();

        redirect('/paysan/display/'.$paysan->getId());
    }

    /**
     * Ajout / Édition des informations générales.
     *
     * @param null $paysan_id
     */
    public function informations_generales($paysan_id)
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DISPLAY_PROFIL, $paysan);

        $form = $this->formFactory->create(\PsrLib\Form\PaysanType::class, $paysan, [
            'current_user' => $this->getUser(),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($paysan);
            $this->em->flush();

            if ($this->getUser() instanceof \PsrLib\ORM\Entity\Paysan) {
                $this->addFlash(
                    'notice_success',
                    'Le profil a été édité avec succès !'
                );

                redirect('/paysan/display/'.$paysan->getId());
            }
            $this->addFlash(
                'notice_success',
                'Le paysan a été mise à jour avec succès'
            );

            redirect('/paysan');
        }

        $this->loadViewWithTemplate('paysan/informations_generales', [
            'paysan' => $paysan,
            'form' => $form->createView(),
        ]);
    }

    public function informations_generales_creation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_CREATE);

        $paysan = new \PsrLib\ORM\Entity\Paysan();

        $form = $this->formFactory->create(\PsrLib\Form\PaysanCreationType::class, $paysan, [
            'current_user' => $this->getUser(),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($paysan);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le paysan a été créée avec succès. Vous pouvez en rajouter un autre.'
            );

            redirect('/paysan/informations_generales_creation');

            return;
        }

        $this->loadViewWithTemplate('paysan/informations_generales_creation', [
            'paysan' => $paysan,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Recherche d'AMAP.
     *
     * @param null $paysan_id
     * @param null $paysan_annonce_id
     * @param null $edit
     */
    public function recherche_amap($paysan_id = null, $paysan_annonce_id = null, $edit = null)
    {
        $this->loadViewWithTemplate('paysan/recherche_amap');
    }

    /**
     * Télécharger l'ensemble des paysans de la session recherche.
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_paysans()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DOWNLOAD_LIST);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchPaysanType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->search($searchForm->getData())
        ;

        if (0 === count($paysans)) {
            redirect('/paysan');

            return;
        }

        /** @var \PsrLib\DTO\SearchPaysanState $paysanSearchState */
        $paysanSearchState = $searchForm->getData();

        /** @var \PsrLib\ORM\Entity\Paysan[] $paysans */
        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->search($paysanSearchState)
        ;

        $this->excelGeneratorExportPaysan->genererExportPaysans($paysans, $paysanSearchState);
    }

    public function telecharger_paysans_regroupement()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT);
        /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
        $regroupement = $this->getUser();

        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->findByFermeMultiple($regroupement->getFermes()->toArray())
        ;

        $this->excelGeneratorExportPaysan->genererExportPaysans($paysans);
    }

    /**
     * Télécharger.
     *
     * @param $ferme_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_ferme_referents_produit($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        /** @var \PsrLib\ORM\Entity\Amapien[] $ferme_referent_produit_all */
        $ferme_referent_produit_all = $ferme->getAmapienRefs()->toArray();
        $time = 'extrait le '.date('d/m/Y');
        $nb_amapiens = count($ferme_referent_produit_all);

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Référents produits');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'AMAP');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Email');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Téléphone 1');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Téléphone 2');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Adresse');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Ville');

        $this->excel->getActiveSheet()->getStyle('A4:I4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:I4')->getFont()->setBold(true);

        $i = 5;
        foreach ($ferme_referent_produit_all as $amapien) {
            $this->excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($amapien->getAmap()->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, strtoupper($amapien->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, ucfirst($amapien->getPrenom()));
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('D'.$i, $amapien->getEmail());
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('E'.$i, $amapien->getNumTel1(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('F'.$i, $amapien->getNumTel2(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('G'.$i, $amapien->getLibAdr1());
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $ville = $amapien->getVille();
            $this->excel->getActiveSheet()->setCellValueExplicit('H'.$i, null === $ville ? null : $ville->getCpString(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('I'.$i, strtoupper(null === $ville ? null : $ville->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = 'Référents produit';
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_referents_produit_'.date('d_m_Y').'.xls';
        header('Content-Type: application/vnd.ms-excel'); // mime type
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"'); // Envoi du nom au navigateur
        header('Cache-Control: max-age=0'); // PAs de cache

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * Créer un mot de passe pour un paysan et lui envoyer par email.
     *
     * @param $paysan_id
     */
    public function password($paysan_id)
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan);

        $password = $this->passwordGenerator->generatePassword();
        $pass_hash = $this->passwordEncoder->encodePassword($password);

        // EMAIL
        $paysan->setPassword($pass_hash);
        $paysan->setEtat(\PsrLib\ORM\Entity\Paysan::ETAT_ACTIF);
        $this->em->flush();
        $email = $paysan->getEmail();
        $this->email->from(EMAIL, FROM);
        $this->email->to($email);
        $this->email->subject('Bienvenue sur '.SITE_NAME);
        $this->email->message('<p>Bonjour, voici vos identifiants pour vous connecter à '.SITE_NAME.' :<br/>
        Adresse email : '.$email.'<br/>
        Mot de passe : '.$password.'</p>
        <a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a>');

        if ($this->email->send()) {
            $this->addFlash(
                'notice_success',
                'Le mot de passe a été envoyé avec succès !'
            );
        } else {
            $this->addFlash(
                'notice_success',
                'Le mot de passe n\'a pas pu être envoyé.'
            );
        }

        $this->email->clear();
        // VUE
        redirect('/paysan');
    }

    /**
     * Supprimer une fiche paysan.
     *
     * @param $paysan_id
     */
    public function remove_paysan($paysan_id)
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_CHANGE_CR, $paysan);

        $this->em->remove($paysan);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le paysan a été supprimé avec succès'
        );

        redirect('/paysan');
    }

    /**
     * Désactiver une fiche paysan.
     *
     * @param $paysan_id
     * @param $etat
     */
    public function de_activate_paysan($paysan_id)
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan);

        $paysan->setEtat(
            \PsrLib\ORM\Entity\Paysan::ETAT_ACTIF === $paysan->getEtat()
            ? \PsrLib\ORM\Entity\Paysan::ETAT_INACTIF
            : \PsrLib\ORM\Entity\Paysan::ETAT_ACTIF
        );
        $this->em->flush();

        redirect('/paysan');
    }

    private function home_refprod(PsrLib\ORM\Entity\Amapien $refProd)
    {
        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->findByFermeMultiple($refProd->getRefProdFermes()->toArray())
        ;

        $this->loadViewWithTemplate('paysan/display_all_refprod', [
            'paysans' => $paysans,
        ]);
    }

    private function home_regroupement(PsrLib\ORM\Entity\FermeRegroupement $fermeRegroupement)
    {
        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->findByFermeMultiple($fermeRegroupement->getFermes()->toArray())
        ;

        $this->loadViewWithTemplate('paysan/display_all_regroupement', [
            'paysans' => $paysans,
        ]);
    }
}
