<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Region extends AppController
{
    /**
     * Region constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index.
     */
    public function index()
    {
        $this->accueil();
    }

    /**
     * Accueil | Affiche toutes les régions.
     */
    public function accueil()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        $regions = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Region::class)
            ->findAllOrdered()
        ;

        $this->twigDisplay('region/index.html.twig', [
            'region_all' => $regions,
        ]);
    }

    /**
     * Affiche tous les départements d'une région.
     *
     * @param $region_id
     */
    public function departement($region_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        $departements = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Departement::class)
            ->findBy([
                'region' => $region,
            ])
        ;

        $this->twigDisplay('departement/index.html.twig', [
            'region' => $region,
            'departement_all' => $departements,
        ]);
    }

    /**
     * Afficher les Admin d'une région.
     *
     * @param $region_id
     */
    public function reg_admin($region_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        $this->loadViewWithTemplate('region/admin', [
            'region' => $region,
        ]);
    }

    /**
     * Ajout d'un Admin à la région.
     *
     * @param $region_id
     */
    public function add_reg_admin($region_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        $this->form_validation->set_rules('amapien', '"Amapien"', 'required|valid_email|amapien_check', ['required' => 'Le champ ne peut pas être vide.']);
        if ($this->form_validation->run()) {
            /** @var \PsrLib\ORM\Entity\Amapien $amapien */
            $amapien = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                ->findOneBy([
                    'email' => $this->input->post('amapien'),
                ])
            ;
            // L'administrateur ne doit pas déjà exister dans la région
            if ($region->getAdmins()->contains($amapien)) {
                $this->addFlash(
                    'notice_error',
                    'L\'administrateur(trice) "'.$amapien->getEmail().'" existe déjà dans la région.'
                );
                redirect('/region/reg_admin/'.$region->getId());
            }

            $region->addAdmin($amapien);
            $this->em->flush();
            $this->addFlash(
                'notice_success',
                'L\'administrateur a été ajouté avec succès !'
            );
            redirect('/region/reg_admin/'.$region->getId());
        }
        redirect('/region/reg_admin/'.$region->getId());
    }

    /**
     * Suppression d'un Admin d'un réseau.
     *
     * @param $region_id
     * @param $reg_adm_id
     */
    public function remove_reg_admin($region_id, $reg_adm_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        /** @var \PsrLib\ORM\Entity\Amapien $admin */
        $admin = $this->em->getReference(\PsrLib\ORM\Entity\Amapien::class, $reg_adm_id);
        $region->removeAdmin($admin);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'administrateur a été retiré avec succès !'
        );

        redirect('/region/reg_admin/'.$region->getId());
    }

    /**
     * Afficher les Admin du département.
     *
     * @param $dep_id
     */
    public function dep_admin($dep_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Departement $departement */
        $departement = $this->findOrExit(\PsrLib\ORM\Entity\Departement::class, $dep_id);

        $this->twigDisplay('departement/admin.html.twig', [
            'departement' => $departement,
        ]);
    }

    /**
     * Ajout d'un Admin au département.
     *
     * @param $dep_id
     */
    public function add_dep_admin($dep_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Departement $departement */
        $departement = $this->findOrExit(\PsrLib\ORM\Entity\Departement::class, $dep_id);

        $this->form_validation->set_rules('amapien', '"Amapien"', 'required|valid_email|amapien_check', ['required' => 'Le champ ne peut pas être vide.']);
        if ($this->form_validation->run()) {
            /** @var \PsrLib\ORM\Entity\Amapien $amapien */
            $amapien = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                ->findOneBy([
                    'email' => $this->input->post('amapien'),
                ])
            ;

            // L'administrateur ne doit pas déjà exister dans le département
            if ($departement->getAdmins()->contains($amapien)) {
                $this->addFlash(
                    'notice_error',
                    'L\'administrateur(trice) "'.$amapien->getEmail().'" existe déjà dans le département.'
                );
                redirect('/region/dep_admin/'.$departement->getId());
            }

            $departement->addAdmin($amapien);
            $this->em->flush();
            $this->addFlash(
                'notice_success',
                'L\'administrateur a été ajouté avec succès !'
            );
            redirect('/region/dep_admin/'.$departement->getId());
        }

        redirect('/region/dep_admin/'.$departement->getId());
    }

    /**
     * Suppression d'un Admin d'un département.
     *
     * @param $dep_id
     * @param $dep_adm_id
     */
    public function remove_dep_admin($dep_id, $dep_adm_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Departement $departement */
        $departement = $this->findOrExit(\PsrLib\ORM\Entity\Departement::class, $dep_id);

        /** @var \PsrLib\ORM\Entity\Amapien $admin */
        $admin = $this->em->getReference(\PsrLib\ORM\Entity\Amapien::class, $dep_adm_id);
        $departement->removeAdmin($admin);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'administrateur a été retiré avec succès !'
        );

        redirect('/region/dep_admin/'.$departement->getId());
    }

    /**
     * @param $region_id
     */
    public function region_logo($region_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        $form = $this->formFactory->create(\PsrLib\Form\EntityWithLogoType::class, $region);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedImg = $form->get('img')->getData();
            if (null !== $uploadedImg) {
                $logo = $this
                    ->uploader
                    ->uploadFile($uploadedImg, \PsrLib\ORM\Entity\Files\Logo::class)
                ;
                $region->setLogo($logo);
            }
            $this->em->flush();

            redirect('/region');
        }

        $this->twigDisplay('region/logo.html.twig', [
            'region' => $region,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $departement_id
     */
    public function departement_logo($departement_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Departement $departement */
        $departement = $this->findOrExit(\PsrLib\ORM\Entity\Departement::class, $departement_id);

        $form = $this->formFactory->create(\PsrLib\Form\EntityWithLogoType::class, $departement);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedImg = $form->get('img')->getData();
            if (null !== $uploadedImg) {
                $logo = $this
                    ->uploader
                    ->uploadFile($uploadedImg, \PsrLib\ORM\Entity\Files\Logo::class)
                ;
                $departement->setLogo($logo);
            }
            $this->em->flush();

            redirect('/region/departement/'.$departement->getRegion()->getId());
        }

        $this->twigDisplay('departement/logo.html.twig', [
            'departement' => $departement,
            'form' => $form->createView(),
        ]);
    }
}
