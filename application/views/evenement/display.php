<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Evenement $evenement */
$currentUser = get_current_connected_user();

echo '<br/>';

// QUI AI-JE INFORMER ?
if ($currentUser instanceof \PsrLib\ORM\Entity\Amap
    && null !== $evenement->getAmap()
    && $evenement->getAmap()->getId() === $currentUser->getId()) {
    echo '<div class="alert alert-warning col-md-12 clearfix">';

    echo '<div class="col-md-4">';
    echo 'Informer mes amapiens : ';
    if ($evenement->getInfoAmapiens()) {
        echo 'OUI';
    } else {
        echo 'NON';
    }
    echo '</div>';

    echo '<div class="col-md-4">';
    echo 'Informer mon réseau : ';
    if ($evenement->getInfoCreateurReseau()) {
        echo 'OUI';
    } else {
        echo 'NON';
    }
    echo '</div>';

    echo '<div class="col-md-4">';
    echo 'Informer une ferme : ';
    if ($evenement->getInfoFerme()) {
        echo $evenement->getRelFerme()->first();
    } else {
        echo 'NON';
    }
    echo '</div>';

    echo '</div>';
}

if (null !== $evenement->getImg()) {
    echo '<img class="img-rounded float-left col-md-6" src="'.site_url($evenement->getImg()->getFileRelativePath()).'">';
    echo '<div class="float-right col-md-6">';
} else {
    echo '<div class="float-right col-md-12">';
}

if ($evenement->getNom()) {
    echo '<h3>'.$evenement->getNom().'</h3>';
}

if (null !== $evenement->getDateDeb()) {
    echo '<div class="alert alert-info clearfix">';
    if (null !== $evenement->getDateFin()) {
        echo 'Du';
    } else {
        echo 'Le';
    }
    echo ' <strong>'.date_format_french($evenement->getDateDeb()).'</strong>';
    if (null != $evenement->getHorDeb()) {
        echo ' à <strong>'.$evenement->getHorDeb().'</strong>';
    }
    if (null !== $evenement->getDateFin()) {
        echo ' au <strong>'.(null === $evenement->getDateFin() ? null : date_format_french($evenement->getDateFin())).'</strong>';
    }
    if (null != $evenement->getHorFin()) {
        echo ' à <strong>'.$evenement->getHorFin().'</strong>';
    }
    echo '.';
    echo '</div>';
}

if ($evenement->getEvTxt()) {
    echo '<h5 class="text-justify">'.nl2br($evenement->getEvTxt()).'</h5>';
}

if ($evenement->getUrl()) {
    $url = $evenement->getUrl();

    // On retire "http://"
    if ('http://' == substr($evenement->getUrl(), 0, 7)) {
        $url = substr($url, 7);
    }

    // On retire "https://"
    if ('https://' == substr($evenement->getUrl(), 0, 8)) {
        $url = substr($url, 8);
    }

    echo '<p><a href="http://'.$url.'" class="btn btn-primary pull-left" target="_blank">Cliquer ici pour plus d\'info</a></p>';
}

if (null !== $evenement->getPj()) {
    echo '<p><a href="'.site_url($evenement->getPj()->getFileRelativePath()).'" style="margin-left:5px;" class="btn btn-warning pull-left" target="_blank">Télécharger la pièce jointe</a></p>';
}

echo '</div>';
?>
<div class="form-group">
    <a href="<?= site_url('evenement'); ?>" class="pull-right btn btn-success">Retour</a>
</div>
