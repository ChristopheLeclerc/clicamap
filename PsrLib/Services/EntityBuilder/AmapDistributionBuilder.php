<?php

namespace PsrLib\Services\EntityBuilder;

use PsrLib\DTO\AmapDistributionAjout;
use PsrLib\ORM\Entity\AmapDistribution;

class AmapDistributionBuilder
{
    /**
     * @return AmapDistribution[]
     */
    public function buildFromAjoutDTO(AmapDistributionAjout $amapDistributionAjout): array
    {
        $res = [];
        if (AmapDistributionAjout::REP_AUCUNE === $amapDistributionAjout->getRepetition()) {
            $res[] = new AmapDistribution(
                $amapDistributionAjout->getDetail(),
                $amapDistributionAjout->getAmapLivraisonLieu(),
                $amapDistributionAjout->getPeriod()->getStartAt()
            );
        } elseif (AmapDistributionAjout::REP_SEMAINE === $amapDistributionAjout->getRepetition()) {
            $date = $amapDistributionAjout->getPeriod()->getStartAt()->clone();
            $end = $amapDistributionAjout->getPeriod()->getEndAt();
            while ($date->lte($end)) {
                $res[] = new AmapDistribution(
                    $amapDistributionAjout->getDetail(),
                    $amapDistributionAjout->getAmapLivraisonLieu(),
                    $date->clone()
                );
                $date->addWeek();
            }
        } elseif (AmapDistributionAjout::REP_2SEMAINES === $amapDistributionAjout->getRepetition()) {
            $date = $amapDistributionAjout->getPeriod()->getStartAt()->clone();
            $end = $amapDistributionAjout->getPeriod()->getEndAt();
            while ($date->lte($end)) {
                $res[] = new AmapDistribution(
                    $amapDistributionAjout->getDetail(),
                    $amapDistributionAjout->getAmapLivraisonLieu(),
                    $date->clone()
                );
                $date->addWeeks(2);
            }
        } else {
            throw new \LogicException('Unknown repetition');
        }

        return $res;
    }
}
