<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\UserWithFermes;
use PsrLib\ORM\Repository\AmapRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContratViergeChooseAmapPaysan extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var UserWithFermes $currentUser */
        $currentUser = $options['current_user'];
        Assertion::isInstanceOf($currentUser, UserWithFermes::class);

        $builder
            ->add('amap', EntityType::class, [
                'class' => Amap::class,
                'query_builder' => function (AmapRepository $amapRepository) use ($currentUser) {
                    return $amapRepository->qbByFermes($currentUser->getFermes()->toArray());
                },
                'required' => false,
                'placeholder' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'current_user' => null,
            'method' => 'GET',
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
        ]);
    }
}
