#language: fr
Fonctionnalité: Supression d'une distribution

  @database
  Scénario: Supression d'une distribution en tant qu'amap
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion de la distrib'AMAP"
    Et que je devrais voir "Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions"
    Et que je remplis le datepicker "search_distribution_amap[period]" avec "01/01/2030 au 01/12/2030"
    Et que je suis "Gestion des inscriptions"
    Et que je sélectionne "2" depuis "amap_distribution_amap_amapien_inscription_amapiens"
    Et que je presse "Ajouter un ou plusieurs amapiens"
    Et que je suis sur "/distribution"
    Et que je remplis "search_distribution_amap[complete]" avec "1"
    Quand je suis "Supprimer le créneau"
    Et que je presse "Confirmer la suppression du créneau"
    Alors je devrais voir "La distribution a été supprimée avec succès"
    Et un mail avec le sujet "Votre désinscription à la Distrib'AMAP du 01/01/2030" devrait avoir été envoyé à "amapien@test.amap-aura.org"
