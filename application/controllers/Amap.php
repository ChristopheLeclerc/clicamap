<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;
use DI\Annotation\Inject;
use PsrLib\ORM\Entity\Ville;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Amap.
 */
class Amap extends AppController
{
    /**
     * @var \PsrLib\ORM\Repository\AmapRepository
     */
    public $amapRepository;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Exporters\ExcelGeneratorExportAmap
     */
    public $excelgeneratorexportamap;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordEncoder
     */
    public $passwordEncoder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordGenerator
     */
    public $passwordGenerator;

    protected $data;

    /**
     * Amap constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('array');
        $this->load->helper('email');
        $this->load->helper('string');
        $this->load->helper('security');

        $this->load->library('email');
        $this->load->library('excel'); // Export .xls
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        $this->amapRepository = $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class);
    }

    /**
     * Première fonction.
     */
    public function index()
    {
        if ($this->getUser() instanceof \PsrLib\ORM\Entity\Amap) {
            $this->mon_amap($this->getUser()->getId());

            return;
        }
        $this->amap_mdr();
    }

    /**
     * Accueil / Moteur de recherche.
     */
    public function amap_mdr()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_LIST);

        $this->redirectFromStoredGetParamIfExist('amap');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $data = $searchForm->getData();
        $amaps = $this->amapRepository->search($data);

        $gps = 0;
        $map = null;
        if ($searchForm->get('carto')->getData()) {
            [$gps, $map] = $this->cartographie($amaps);
        }

        $this->loadViewWithTemplate('amap/display_all', [
            'amaps' => $amaps,
            'gps' => $gps,
            'map' => $map,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @param $amap_id
     */
    public function mon_amap($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $this->loadViewWithTemplate('amap/display_own', [
            'amap' => $amap,
        ]);
    }

    /**
     * Éditer l'état de l'AMAP.
     *
     * @param $etat
     * @param $amap_id
     */
    public function edit_etat($etat, $amap_id)
    {
        // Sécurité
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_STATE, $amap);

        if (in_array($etat, \PsrLib\ORM\Entity\Amap::ETATS)) {
            $amap->setEtat($etat);
        }
        $this->em->flush();

        $this->redirect->redirectToReferer('/amap');
    }

    /**
     * Afficher les détails d'une AMAP.
     *
     * @param $amap_id
     */
    public function display($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $amapFermes = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->getFromAmap($amap)
        ;

        $this->twigDisplay('amap/display.html.twig', [
            'amap' => $amap,
            'amapFermes' => $amapFermes,
        ]);
    }

    /**
     * Calendrier de livraisons.
     *
     * @param $amap_id
     * @param null $an
     * @param null $mois
     */
    public function calendrier($amap_id, $an = null, $mois = null)
    {
        // Sécurité ------------------------------------------------------------
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        // PRÉFÉRENCES DU CALENDRIER -------------------------------------------
        if ($amap_id) {
            $prefs = ['start_day' => 'monday',
                'day_type' => 'long',
                'show_next_prev' => true,
                'next_prev_url' => site_url('amap/calendrier/'.$amap_id), ]; // Le zéro est pour $date_id

            $prefs['template'] = '
          {table_open}<table class="table"  border="0" cellpadding="0" cellspacing="0">{/table_open}

          {heading_row_start}<tr>{/heading_row_start}

          {heading_previous_cell}<th><a href="{previous_url}"><i class="glyphicon glyphicon-menu-left"></i></a></th>{/heading_previous_cell}
          {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
          {heading_next_cell}<th><a href="{next_url}"><i class="glyphicon glyphicon-menu-right"></i></a></th>{/heading_next_cell}

          {heading_row_end}</tr>{/heading_row_end}

          {week_row_start}<tr>{/week_row_start}
          {week_day_cell}<td>{week_day}</td>{/week_day_cell}
          {week_row_end}</tr>{/week_row_end}

          {cal_row_start}<tr>{/cal_row_start}
          {cal_cell_start}<td>{/cal_cell_start}
          {cal_cell_start_today}<td>{/cal_cell_start_today}
          {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

          {cal_cell_content}<a href="{content}" data-toggle="modal" data-target="#modal_{day}"><button type=button class="btn btn-primary btn-xs">{day}</button></a>{/cal_cell_content}

          {cal_cell_no_content}{day}{/cal_cell_no_content}

          {cal_cell_blank}&nbsp;{/cal_cell_blank}

          {cal_cell_other}{day}{/cal_cel_other}

          {cal_cell_end}</td>{/cal_cell_end}
          {cal_cell_end_today}</td>{/cal_cell_end_today}
          {cal_cell_end_other}</td>{/cal_cell_end_other}
          {cal_row_end}</tr>{/cal_row_end}

          {table_close}</table>{/table_close}
          ';

            $this->load->library('calendar', $prefs);
        }

        if (!$an) {
            $this->data['an'] = date('Y');
        } else {
            $this->data['an'] = $an;
        }
        if (!$mois) {
            $this->data['mois'] = date('m');
        } else {
            $this->data['mois'] = $mois;
        }

        $this->data['liens'] = null;

        $this->data['amap'] = $amap;
        $this->data['livraison_lieu'] = $amap->getLivraisonLieux()->toArray();

        // TOUTES LES DATES DE CONTRATS VIERGES (MC) LIÉS À UNE AMAPIEN --------
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $date_all */
        $date_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratDate::class)
            ->getByAmap($amap)
        ;
        if (count($date_all) > 0) {
            // On dédoublonne les dates ------------------------------------------
            $amap_date_all = [];
            foreach ($date_all as $date) {
                $amap_date_all[] = $date->getDateLivraison()->format('Y-m-d');
            }
            $amap_date_all = array_unique($amap_date_all);

            // Liens du calendrier et infos des Modals ---------------------------
            $this->data['liens'] = [];

            /** @var \PsrLib\ORM\Repository\ModeleContratRepository $modeleContratRepo */
            $modeleContratRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ;

            /** @var \PsrLib\ORM\Repository\ContratCelluleRepository $contratCelluleRepo */
            $contratCelluleRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ContratCellule::class)
            ;

            foreach ($amap_date_all as $a_d) {
                // Année
                $a_d_an = substr($a_d, 0, 4);
                // Mois
                $a_d_mois = substr($a_d, 5, 2);

                // L'année et le mois du contrat correspondent avec l'année et le mois courant du calendrier
                if ($this->data['an'] == $a_d_an
                    && $this->data['mois'] == $a_d_mois
                ) {
                    $jour = substr($a_d, -2);
                    // On enlève le premier zéro du jour s'il y en a un --------------
                    if (0 == substr($jour, 0, 1)) {
                        $jour = substr($jour, 1, 2);
                    }

                    // On place un lien sur le jour du calendrier --------------------
                    $this->data['livraisons'][$jour] = [];

                    $this->data['livraisons'][$jour]['mc'] = $modeleContratRepo
                        ->getFromAmapDate($amap, $a_d)
                    ;
                    $this->data['livraisons'][$jour]['cellules'] = $contratCelluleRepo
                        ->getFromAmapDate($amap, $a_d)
                    ;

                    if (count($this->data['livraisons'][$jour]['cellules']) > 0) {
                        $this->data['liens'][$jour] = '#';
                    }
                }
            }
        }

        $this->data['calendrier'] = $this
            ->calendar
            ->generate($this->data['an'], $this->data['mois'], $this->data['liens'])
        ;

        $this->loadViewWithTemplate('amap/calendrier', $this->data);
    }

    /**
     * Télécharger infos du collectif.
     *
     * @param $amap_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_collectif($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $collectifs = $amap->getCollectifs();

        $time = 'extrait le '.date('d/m/Y');

        $nb_amapiens = $collectifs->count();

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Collectif');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'Profil');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Précision');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Email');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Téléphone 1');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Téléphone 2');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Adresse');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Ville');
        $this->excel->getActiveSheet()->setCellValue('K4', 'Adhérent année');
        $this->excel->getActiveSheet()->setCellValue('L4', 'Abonné newsletter');
        $this->excel->getActiveSheet()->setCellValue('M4', 'État');

        $this->excel->getActiveSheet()->getStyle('A4:M4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:M4')->getFont()->setBold(true);

        $i = 5;
        /** @var \PsrLib\ORM\Entity\AmapCollectif $collectif */
        foreach ($collectifs as $collectif) {
            $amapien = $collectif->getAmapien();
            $annee_adhesion = implode(' | ', $amapien->getAnneeAdhesions()->toArray());

            $this->excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($collectif->getProfil()));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, strtoupper($collectif->getPrecision()));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, strtoupper($amapien->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('C'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('D'.$i, ucfirst($amapien->getPrenom()));
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('E'.$i, $amapien->getEmail());
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('F'.$i, $amapien->getNumTel1(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('G'.$i, $amapien->getNumTel2(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('H'.$i, $amapien->getLibAdr1());
            $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $amapienVille = $amapien->getVille();
            $this->excel->getActiveSheet()->setCellValueExplicit('I'.$i, null === $amapienVille ? null : $amapienVille->getCpString(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('J'.$i, strtoupper(null === $amapienVille ? null : $amapienVille->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('K'.$i, $annee_adhesion);
            $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('L'.$i, ucfirst(true === $amapien->getNewsletter() ? 'oui' : null));
            $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('M'.$i, ucfirst($amapien->getEtat()));
            $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = null;
        $titre = $titre.$amap->getNom().' -> collectif';

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_du collectif_'.date('d_m_Y').'.xls';
        header('Content-Type: application/vnd.ms-excel'); // mime type
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"'); // Envoi du nom au navigateur
        header('Cache-Control: max-age=0'); // PAs de cache

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * Ajouter un commentaire daté.
     *
     * @param $amap_id
     */
    public function amap_commentaire_add($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_STATE, $amap);

        $commentaire = new \PsrLib\ORM\Entity\AmapCommentaire(
            $amap,
            Carbon::now(),
            $this->input->post('amap_commentaire')
        );
        $this->em->persist($commentaire);
        $this->em->flush();

        redirect('/amap/display/'.$amap->getId());
    }

    /**
     *  Ajouter un commentaire daté.
     *
     * @param $amap_id
     * @param $commentaire_id
     */
    public function amap_commentaire_remove($commentaire_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapCommentaire $amap */
        $commentaire = $this->findOrExit(\PsrLib\ORM\Entity\AmapCommentaire::class, $commentaire_id);

        $amap = $commentaire->getAmap();

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_STATE, $amap);

        $this->em->remove($commentaire);
        $this->em->flush();

        redirect('/amap/display/'.$amap->getId());
    }

    /**
     * Télécharger l'ensemble des AMAP.
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_amap_synthese()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $amaps = $this->amapRepository->search($searchForm->getData());
        if (0 === count($amaps)) {
            redirect('/amap');
        }

        // Tous les types de production ------------------------------------------
        /** @var \PsrLib\ORM\Entity\TypeProduction[] $type_production */
        $type_production = $this->em->getRepository(\PsrLib\ORM\Entity\TypeProduction::class)->findAll();
        $time = 'Extrait le '.date('d/m/Y');
        $nb_amap = count($amaps);

        if (1 == $nb_amap) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amap.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Amap');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'ID');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Nombre d\'adhérents');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Email public');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Email correspondant');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Lieu de livraison');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Adresse de livraison');
        $this->excel->getActiveSheet()->setCellValue('H4', 'CP de livraison');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Ville de livraison');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Saison de livraison');
        $this->excel->getActiveSheet()->setCellValue('K4', 'Jours de livraison');
        $this->excel->getActiveSheet()->setCellValue('L4', 'Heure de début de livraison');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Heure de fin de livraison');

        $this->excel->getActiveSheet()->getStyle('A4:CB4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:CB4')->getFont()->setBold(true);

        $i = 5;
        foreach ($amaps as $amap) {
            $this->excel->getActiveSheet()->setCellValue('A'.$i, mb_strtoupper($amap->getId()));
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            // NOM DE L'AMAP -------------------------------------------------------
            $this->excel->getActiveSheet()->setCellValue('B'.$i, mb_strtoupper($amap->getNom()));
            $this->excel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);

            // NOMBRE D'ADHÉRENTS --------------------------------------------------
            $this->excel->getActiveSheet()->setCellValue('C'.$i, mb_strtolower($amap->getNbAdherents()));

            // CONTACTS ------------------------------------------------------------
            // EMAIL de l'AMAP -----------------------------------------------------
            $this->excel->getActiveSheet()->setCellValue('D'.$i, mb_strtolower($amap->getEmail()));

            // EMAIL des CORRESPONDANTS CLIC'AMAP ----------------------------------
            $correspondant_clicamap = $amap->getAmapiens()->matching(\PsrLib\ORM\Repository\AmapienRepository::criteriaGestionaire());
            if ($correspondant_clicamap->count() > 0) {
                $correspondants = null;
                $nb_c = count($correspondant_clicamap);
                $n = 1;
                $retour = ' ; ';

                foreach ($correspondant_clicamap as $c) {
                    $correspondants .= $c->getEmail();
                    if ($n < $nb_c) {
                        $correspondants = $correspondants.$retour;
                    }
                    ++$n;
                }
                $this->excel->getActiveSheet()->setCellValue('D'.$i, mb_strtolower($correspondants));
            } else { // ou celui des CORRESPONDANT RÉSEAUX
                $correspondant_reseau = $amap->getAmapienRefReseau();
                if (null !== $correspondant_reseau) {
                    $this->excel->getActiveSheet()->setCellValue('E'.$i, mb_strtolower($correspondant_reseau->getEmail()));
                } else { // ou celui du PRÉSIDENT
                    $collectif = $amap->getCollectifs();
                    foreach ($collectif as $coll) {
                        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_PRESIDENT == $coll->getProfil()) {
                            $this->excel->getActiveSheet()->setCellValue('E'.$i, mb_strtolower($coll->getAmapien()->getEmail()));
                        }
                    }
                }
            }

            // LIEUX DE LIVRAISON
            $livraison_lieu = $amap->getLivraisonLieux();
            $livraison_lieu_nbr = count($livraison_lieu);

            $j = $i; // 1er ligne de la cellule de l'AMAP
            $n = 1; // Incrémentation
            /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $l_l */
            foreach ($livraison_lieu as $l_l) {
                $this->excel->getActiveSheet()->setCellValue('F'.$i, $l_l->getNom());
                $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

                $this->excel->getActiveSheet()->setCellValue('G'.$i, $l_l->getAdresse());
                $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

                $this->excel->getActiveSheet()->setCellValueExplicit('H'.$i, $l_l->getVille()->getCpString(), PHPExcel_Cell_DataType::TYPE_STRING);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

                $this->excel->getActiveSheet()->setCellValue('I'.$i, $l_l->getVille()->getNom());
                $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

                $livraison_horaire = $l_l->getLivraisonHoraires();
                $livraison_horaire_nbr = count($livraison_horaire);

                $k = $i; // 1er ligne de la cellule du lieu de livraison
                $nn = 1; // Incrémentation

                /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $l_h */
                foreach ($livraison_horaire as $l_h) {
                    if (\PsrLib\ORM\Entity\AmapLivraisonHoraire::SAISON_ETE == $l_h->getSaison()) {
                        $saison = 'Eté';
                    } else {
                        $saison = 'Hiver';
                    }

                    $this->excel->getActiveSheet()->setCellValue('J'.$i, $saison);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

                    $this->excel->getActiveSheet()->setCellValue('K'.$i, $l_h->getJour());
                    $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

                    $this->excel->getActiveSheet()->setCellValue('L'.$i, $l_h->getHeureDebut());
                    $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

                    $this->excel->getActiveSheet()->setCellValue('M'.$i, $l_h->getHeureFin());
                    $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

                    // Cellules lieu de livraison de l'AMAP ----------------------------
                    if ($nn == $livraison_horaire_nbr) {
                        $m = 'E'.$k.':E'.$i;
                        $this->excel->getActiveSheet()->mergeCells($m);
                        $m = 'F'.$k.':F'.$i;
                        $this->excel->getActiveSheet()->mergeCells($m);
                        $m = 'G'.$k.':G'.$i;
                        $this->excel->getActiveSheet()->mergeCells($m);
                        $m = 'H'.$k.':H'.$i;
                        $this->excel->getActiveSheet()->mergeCells($m);
                    } else {
                        ++$i;
                        ++$nn;
                    }
                }

                // Cellule nom / emails de l'AMAP ------------------------------------
                if ($n == $livraison_lieu_nbr) {
                    $m = 'A'.$j.':A'.$i;
                    $this->excel->getActiveSheet()->mergeCells($m);
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $m = 'B'.$j.':B'.$i;
                    $this->excel->getActiveSheet()->mergeCells($m);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $m = 'C'.$j.':C'.$i;
                    $this->excel->getActiveSheet()->mergeCells($m);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $m = 'D'.$j.':D'.$i;
                    $this->excel->getActiveSheet()->mergeCells($m);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $m = 'E'.$j.':E'.$i;
                    $this->excel->getActiveSheet()->mergeCells($m);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                } else {
                    ++$i;
                    ++$n;
                }
            }

            // FILIÈRES ------------------------------------------------------------

            $y = 'M'; // Position du pointeur pour l'entête
            $z = $y; // Entête
            // Pour chaque filière -------------------------------------------------
            // On affiche que les catégories filles --------------------------------
            foreach ($type_production as $tp) {
                if (!$tp->hasChilds()) {
                    $tp_nom = $tp->getNomComplet();

                    // Quatres colonnes par filière
                    $this->excel->getActiveSheet()->setCellValue($y.'4', $tp_nom.' (Référents / Paysans)');
                    ++$z;
                    ++$z;
                    ++$z; // soit $y + 3
                    $m = $y.'4:'.$z.'4';
                    // Merge de l'entête
                    $this->excel->getActiveSheet()->mergeCells($m);

                    // RÉFÉRENTS / PAYSANS ---------------------------------------------
                    $referents_nom_prenom = [];
                    $referents_email = [];
                    $referents_nom_prenom_cell = null;
                    $referents_email_cell = null;

                    $paysans_nom_prenom = [];
                    $paysans_email = [];
                    $paysans_nom_prenom_cell = null;
                    $paysans_email_cell = null;

                    /** @var \PsrLib\ORM\Entity\Amapien[] $ferme_referents */
                    $ferme_referents = $this
                        ->em
                        ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                        ->getRefProduitFromAmapTp($amap, $tp)
                    ;

                    // On fait le tour de toutes les fermes reliées à l'AMAP -----------
                    foreach ($ferme_referents as $amapien) {
                        $referents_nom_prenom[] = mb_strtoupper($amapien->getNom()).' '.ucfirst(strtolower($amapien->getPrenom()));
                        $referents_email[] = $amapien->getEmail();

                        $referents_nom_prenom = array_unique($referents_nom_prenom);
                        $referents_email = array_unique($referents_email);

                        // On liste les paysans dans des tableaux ----------------------
                        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
                        foreach ($amapien->getRefProdFermes() as $ferme) {
                            foreach ($ferme->getPaysans() as $paysan) {
                                $paysans_nom_prenom[] = mb_strtoupper($paysan->getNom()).' '.ucfirst(strtolower($paysan->getPrenom()));
                                $paysans_email[] = $paysan->getEmail();
                            }
                        }
                        $paysans_nom_prenom = array_unique($paysans_nom_prenom);
                        $paysans_email = array_unique($paysans_email);
                    }

                    // RÉFÉRENTS -------------------------------------------------------
                    if ($referents_email) {
                        // On place les valeurs des tableaux dans une variable string ----
                        $nb_r = count($referents_nom_prenom);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($referents_nom_prenom as $referent_nom_prenom) {
                            $referents_nom_prenom_cell .= $referent_nom_prenom;
                            if ($n < $nb_r) {
                                $referents_nom_prenom_cell = $referents_nom_prenom_cell.$retour;
                            }
                            ++$n;
                        }
                        $nb_r = count($referents_email);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($referents_email as $referent_email) {
                            $referents_email_cell .= $referent_email;
                            if ($n < $nb_r) {
                                $referents_email_cell = $referents_email_cell.$retour;
                            }
                            ++$n;
                        }
                    }
                    // Cellules des référents ------------------------------------------
                    $this->excel->getActiveSheet()->setCellValue($y.$j, $referents_nom_prenom_cell);
                    ++$y;

                    $this->excel->getActiveSheet()->setCellValue($y.$j, $referents_email_cell);
                    ++$y;

                    // PAYSANS ---------------------------------------------------------
                    if ($paysans_email) {
                        // On place les valeurs des tableaux dans une variable string ----
                        $nb_r = count($paysans_nom_prenom);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($paysans_nom_prenom as $paysan_nom_prenom) {
                            $paysans_nom_prenom_cell .= $paysan_nom_prenom;
                            if ($n < $nb_r) {
                                $paysans_nom_prenom_cell = $paysans_nom_prenom_cell.$retour;
                            }
                            ++$n;
                        }
                        $nb_r = count($paysans_email);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($paysans_email as $paysan_email) {
                            $paysans_email_cell .= $paysan_email;
                            if ($n < $nb_r) {
                                $paysans_email_cell = $paysans_email_cell.$retour;
                            }
                            ++$n;
                        }
                    }

                    // Cellules des paysans --------------------------------------------
                    $this->excel->getActiveSheet()->setCellValue($y.$j, $paysans_nom_prenom_cell);
                    ++$y;

                    $this->excel->getActiveSheet()->setCellValue($y.$j, $paysans_email_cell);
                    ++$y;
                    // Positionnement de l'entête --------------------------------------
                    $z = $y;
                }
            }

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = 'Amap';
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_amap_'.date('d_m_Y').'.xlsx'; //save our workbook as this file name

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        // dit donne le nom du fichier au navigateur
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"');
        header('Cache-Control: max-age=0'); // Pas de cache

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function telecharger_amap_complet()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $this->load->helper('download');
        $outFileName = $this->excelgeneratorexportamap->genererExportAmapComplet($searchForm->getData());
        force_download(
            sprintf('liste_des_amaps_%s.xls', Carbon::now()->format('Y_m_d')),
            file_get_contents($outFileName),
            true
        );
        unlink($outFileName);
    }

    /**
     * Télécharger.
     *
     * @param $amap_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_referents_produit($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $amap_referent_produit_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->getRefProduitFromAmap($amap)
        ;

        $time = 'extrait le '.date('d/m/Y');

        $nb_amapiens = count($amap_referent_produit_all);

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Référents produits');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Email');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Téléphone 1');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Téléphone 2');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Adresse');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Ville');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Adhérent année');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Abonné newsletter');
        $this->excel->getActiveSheet()->setCellValue('K4', 'État');

        $this->excel->getActiveSheet()->getStyle('A4:K4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:K4')->getFont()->setBold(true);

        $i = 5;
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        foreach ($amap_referent_produit_all as $amapien) {
            $annee_adhesion = implode(' | ', $amapien->getAnneeAdhesions()->toArray());

            $this->excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($amapien->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($amapien->getPrenom()));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, $amapien->getEmail());
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('D'.$i, $amapien->getNumTel1(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('E'.$i, $amapien->getNumTel2(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('F'.$i, $amapien->getLibAdr1());
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $villeAmapien = $amapien->getVille();
            $this->excel->getActiveSheet()->setCellValueExplicit('G'.$i, null === $villeAmapien ? null : $villeAmapien->getCpString(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('H'.$i, strtoupper(null === $villeAmapien ? null : $villeAmapien->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('I'.$i, $annee_adhesion);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('J'.$i, ucfirst(true === $amapien->getNewsletter() ? 'oui' : null));
            $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('K'.$i, ucfirst($amapien->getEtat()));
            $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':K'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = null;
        $titre = $titre.$amap->getNom().' -> référents produit';

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_referents_produit_'.date('d_m_Y').'.xls';
        header('Content-Type: application/vnd.ms-excel'); // mime type
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"'); // Envoi du nom au navigateur
        header('Cache-Control: max-age=0'); // PAs de cache

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * Télécharger l'ensemble des paysans de la session recherche.
     *
     * @param $amap_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_paysans($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        // fermes reliées à l'AMAP par un référent
        /** @var \PsrLib\ORM\Entity\Ferme[] $fermes */
        $fermes = $this->em->getRepository(\PsrLib\ORM\Entity\Ferme::class)->getFromAmap($amap);
        /** @var \PsrLib\ORM\Entity\Paysan[] $paysans */
        $paysans = [];
        foreach ($fermes as $ferme) {
            $paysans = array_merge($paysans, $ferme->getPaysans()->toArray());
        }

        $time = 'Extrait le '.date('d/m/Y');

        $nb_paysans = count($paysans);

        if (1 == $nb_paysans) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_paysans.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Paysans');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Email');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Téléphone 1');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Téléphone 2');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Adresse');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Ville');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Adhérent année');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Suivi SPG');
        $this->excel->getActiveSheet()->setCellValue('K4', 'Abonné newsletter');
        $this->excel->getActiveSheet()->setCellValue('L4', 'Ferme : nom');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Ferme : siret');
        $this->excel->getActiveSheet()->setCellValue('N4', 'Ferme : code postal');
        $this->excel->getActiveSheet()->setCellValue('O4', 'Ferme : ville');

        $this->excel->getActiveSheet()->getStyle('A4:O4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:O4')->getFont()->setBold(true);

        $i = 5;
        foreach ($paysans as $paysan) {
            $annee_adhesion = implode(
                ' | ',
                $paysan->getFerme()->getAnneeAdhesions()->toArray()
            );

            $this->excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($paysan->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($paysan->getPrenom()));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, $paysan->getEmail());
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('D'.$i, $paysan->getNumTel1(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('E'.$i, $paysan->getNumTel2(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('F'.$i, $paysan->getLibAdr());
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $villePaysan = $paysan->getVille();
            $this->excel->getActiveSheet()->setCellValueExplicit('G'.$i, null === $villePaysan ? null : $villePaysan->getCpString(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('H'.$i, strtoupper(null === $villePaysan ? null : $villePaysan->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('I'.$i, $annee_adhesion);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('J'.$i, null === $villePaysan ? null : $villePaysan->getCpString(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('K'.$i, ucfirst($paysan->isNewsletter() ? 'oui' : 'non'));
            $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $ferme = $paysan->getFerme();
            $this->excel->getActiveSheet()->setCellValue('L'.$i, ucfirst($ferme->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('M'.$i, $ferme->getSiret());
            $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('N'.$i, $ferme->getVille()->getCpString());
            $this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('O'.$i, $ferme->getVille()->getNom());
            $this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = $amap->getNom().' -> paysans';

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_paysans_'.date('d_m_Y').'.xls';
        header('Content-Type: application/vnd.ms-excel'); // mime type
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"'); // Envoi du nom au navigateur
        header('Cache-Control: max-age=0'); // PAs de cache

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * @param $amap_id
     */
    public function informations_generales_edition($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapEditionType::class, $amap, [
            'emailRequired' => $this->getUser() instanceof \PsrLib\ORM\Entity\Amap, // L'AMAP ne peut pas supprimer son
            'additionalFields' => $this->securitychecker->isGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'AMAP a été modifiée avec succès !'
            );

            redirect('/amap');
        }

        $this->loadViewWithTemplate('amap/informations_generales_edition', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    public function informations_generales_creation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);

        $wizard = $this->wizard_get_session_data();
        if (false === $wizard) {
            $wizard = new \PsrLib\DTO\AmapCreationWizard();
            $amap = new \PsrLib\ORM\Entity\Amap();
        } else {
            $amap = $wizard->getAmap();
        }

        $form = $this->formFactory->create(\PsrLib\Form\AmapCreationType::class, $amap);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setAmap($amap);
            $this->wizard_store_session_data($wizard);
            redirect('/amap/livraison_lieu_creation');
        }

        $this->loadViewWithTemplate('amap/informations_generales_creation', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    /**
     * LIEUX DE LIVRAISON.
     *
     * @param null $amap_id
     */
    public function livraison_lieu($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonLieuType::class);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $ll */
            $ll = $form->getData();
            $ll->setAmap($amap);
            $this->em->persist($ll);
            $this->em->flush();

            redirect('/amap/livraison_lieu/'.$amap->getId());
        }

        $this->loadViewWithTemplate('amap/livraison_lieu', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    public function livraison_lieu_creation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);

        $wizard = $this->wizard_get_session_data();
        $ll = $wizard->getLivraisonLieu();
        if (null === $ll) {
            $ll = new \PsrLib\ORM\Entity\AmapLivraisonLieu();
        }

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonLieuType::class, $ll);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setLivraisonLieu($ll);
            $this->wizard_store_session_data($wizard);

            redirect('/amap/livraison_horaire_creation');
        }

        $this->loadViewWithTemplate('amap/livraison_lieu_creation', [
            'amap' => $wizard->getAmap(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param null $amap_id
     * @param null $liv_lieu_id
     */
    public function livraison_lieu_edit($liv_lieu_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
        $livraisonLieu = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonLieu::class, $liv_lieu_id);

        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonLieuType::class, $livraisonLieu);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            redirect('/amap/livraison_lieu/'.$amap->getId());
        }

        $this->loadViewWithTemplate('amap/livraison_lieu_edition', [
            'amap' => $amap,
            'livraisonLieu' => $livraisonLieu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param null $amap_id
     * @param null $liv_lieu_id
     */
    public function livraison_lieu_remove($liv_lieu_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
        $livraisonLieu = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonLieu::class, $liv_lieu_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DELIVERY_PLACE_DELETE, $livraisonLieu);

        $amap = $livraisonLieu->getAmap();

        $this->em->remove($livraisonLieu);
        $this->em->flush();

        redirect('/amap/livraison_lieu/'.$amap->getId());
    }

    public function livraison_horaire_creation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);
        $wizard = $this->wizard_get_session_data();

        $llh = $wizard->getLivraisonHoraire();
        if (null === $llh) {
            $llh = new \PsrLib\ORM\Entity\AmapLivraisonHoraire($wizard->getLivraisonLieu());
        }

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonHoraireType::class, $llh);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setLivraisonHoraire($llh);
            $this->wizard_store_session_data($wizard);

            redirect('/amap/produits_creation');

            return;
        }

        $this->loadViewWithTemplate('amap/livraison_horaires_creation', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * HORAIRES DE LIVRAISON.
     *
     * @param null $liv_lieu_id
     */
    public function livraison_horaire($liv_lieu_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
        $livraisonLieu = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonLieu::class, $liv_lieu_id);
        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $llh = new \PsrLib\ORM\Entity\AmapLivraisonHoraire($livraisonLieu);
        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonHoraireType::class, $llh);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($llh);
            $this->em->flush();

            redirect('/amap/livraison_horaire/'.$livraisonLieu->getId());
        }

        $livraisonHoraires = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapLivraisonHoraire::class)
            ->getByLivraisonLieuOrderedByStartHour($livraisonLieu)
        ;

        $this->loadViewWithTemplate('amap/livraison_horaire', [
            'amap' => $amap,
            'livraisonHoraires' => $livraisonHoraires,
            'livraisonLieu' => $livraisonLieu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param null $amap_id
     * @param null $liv_lieu_id
     * @param null $liv_hor_id
     */
    public function livraison_horaire_edit($liv_hor_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $livraisonHoraire */
        $livraisonHoraire = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonHoraire::class, $liv_hor_id);
        $livraisonLieu = $livraisonHoraire->getLivraisonLieu();
        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonHoraireType::class, $livraisonHoraire);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            redirect('/amap/livraison_horaire/'.$livraisonLieu->getId());
        }

        $this->loadViewWithTemplate('amap/livraison_horaire_edition', [
            'amap' => $amap,
            'livraisonLieu' => $livraisonLieu,
            'livraisonHoraire' => $livraisonHoraire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param null $amap_id
     * @param null $liv_lieu_id
     * @param null $liv_hor_id
     */
    public function livraison_horaire_remove($liv_hor_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $livraisonHoraire */
        $livraisonHoraire = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonHoraire::class, $liv_hor_id);
        $livraisonLieu = $livraisonHoraire->getLivraisonLieu();
        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $this->em->remove($livraisonHoraire);
        $this->em->flush();

        redirect('/amap/livraison_horaire/'.$livraisonLieu->getId());
    }

    public function produits_creation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);
        $wizard = $this->wizard_get_session_data();

        $amap = $wizard->getAmap();
        $form = $this->formFactory->create(\PsrLib\Form\AmapProduitType::class, $amap);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setTpPIds($amap->getTpPropose()->map(function (PsrLib\ORM\Entity\TypeProduction $typeProduction) {
                return $typeProduction->getId();
            })->toArray());
            $wizard->setTpRIds($amap->getTpRecherche()->map(function (PsrLib\ORM\Entity\TypeProduction $typeProduction) {
                return $typeProduction->getId();
            })->toArray());

            $this->wizard_store_session_data($wizard);
            redirect('/amap/gestionnaire_creation');
        }

        $this->loadViewWithTemplate('amap/produits_creation', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param mixed $amap_id
     */
    public function produits($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapProduitType::class, $amap);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash('notice_success', 'L\'AMAP a été modifiée avec succès !');
            redirect('/amap/produits/'.$amap->getId());
        }

        $this->loadViewWithTemplate('amap/produits_edition', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    public function recherche_paysan()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_SEARCH_PAYSAN);

        /** @var \PsrLib\ORM\Entity\Amap $currentUser granted by permission $currentUser */
        $currentUser = $this->getUser();

        $form = $this->formFactory->create(\PsrLib\Form\AmapProduitRechercheType::class, $currentUser);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $recherche = new \PsrLib\ORM\Entity\AmapRecherchePaysan($currentUser);
            $this->em->persist($recherche);
            $this->em->flush();

            $this->addFlash('notice_success', 'L\'AMAP a été modifiée avec succès !');

            redirect('/amap');
        }

        $this->loadViewWithTemplate('amap/produits_edition', [
            'amap' => $currentUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Gestionnaire de l'AMAP.
     */
    public function gestionnaire_creation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);
        $wizard = $this->wizard_get_session_data();
        $amapien = $wizard->getGestionnaire();
        if (null === $amapien) {
            $amapien = new \PsrLib\ORM\Entity\Amapien();
            $amapien->setGestionnaire(true);
            $amapien->setEtat(\PsrLib\ORM\Entity\Amapien::ETAT_ACTIF);
        }

        $form = $this->formFactory->create(\PsrLib\Form\AmapCreationGestionnaireType::class, $amapien);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setGestionnaire($amapien);

            $this->wizard_store_session_data($wizard);
            redirect('/amap/confirmation');
        }

        $this->loadViewWithTemplate('amap/gestionnaire_creation', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Confirmation avant sauvegarde des données.
     */
    public function confirmation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CREATE);
        $wizard = $this->wizard_get_session_data();

        // Rebuild amap entity
        $amap = $wizard->getAmap();
        $ll = $wizard->getLivraisonLieu();
        $llh = $wizard->getLivraisonHoraire();

        $llh->setLivraisonLieu($ll);
        $ll->addLivraisonHoraire($llh);

        $ll->setAmap($amap);
        $amap->addLivraisonLieux($ll);

        $gestionnaire = $wizard->getGestionnaire();
        $gestionnaire->setAmap($amap);
        $amap->addAmapien($gestionnaire);

        if ($this->input->post('step_conf')) {
            $this->em->persist($amap);
            $this->em->flush();

            // clean cache data
            $this->wizard_clear_session_data();

            $this->addFlash(
                'notice_success',
                'L\'AMAP a été crée avec succès'
            );

            redirect('/amap');
        }

        $this->loadViewWithTemplate('amap/confirmation', [
            'amap' => $amap,
        ]);
    }

    /**
     * Afficher les gestionnaires de l'AMAP.
     *
     * @param $amap_id
     */
    public function collectif($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $collectif = new \PsrLib\ORM\Entity\AmapCollectif($amap);
        $form = $this->formFactory->create(\PsrLib\Form\AmapCollectifType::class, $collectif);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($collectif);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le rôle a été ajouté avec succès !'
            );

            redirect('/amap/collectif/'.$amap->getId());

            return;
        }

        $this->loadViewWithTemplate('amap/collectif_display', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $amap_id
     * @param $coll_id
     */
    public function collectif_remove($coll_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapCollectif $collectif */
        $collectif = $this->findOrExit(\PsrLib\ORM\Entity\AmapCollectif::class, $coll_id);
        $amap = $collectif->getAmap();

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $this->em->remove($collectif);
        $this->em->flush();

        redirect('/amap/collectif/'.$amap->getId());
    }

    /**
     * Changer le référent réseau.
     *
     * @param $amap_id
     */
    public function referent_edition($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapReferentType::class, null, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ref = $form->get('referent')->getData();
            $amap->setAmapienRefReseau($ref);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le référent réseau a été mis à jour avec succès'
            );

            redirect('/amap');

            return;
        }

        $this->loadViewWithTemplate('amap/referent_edition', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Changer le référent réseau.
     *
     * @param $amap_id
     */
    public function referent_second_edition($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapReferentType::class, null, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ref = $form->get('referent')->getData();
            $amap->setAmapienRefReseauSecondaire($ref);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le référent réseau secondaire a été mis à jour avec succès'
            );

            redirect('/amap');

            return;
        }

        $this->loadViewWithTemplate('amap/referent_second_edition', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    /*
     *
     * Afficher les gestionnaires de l'AMAP.
     *
     * @param $amap_id
     */
    public function gestionnaire_display($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapReferentType::class, null, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $amapien = $form->get('referent')->getData();

            if ($amapien->isGestionnaire()) {
                $this->addFlash(
                    'notice_success',
                    'L\'amapien(ne) est déjà gestionnaire de l\'AMAP.'
                );
                redirect('/amap/gestionnaire_display/'.$amap->getId());

                return;
            }

            $amapien->setGestionnaire(true);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le correspondants Clic\'AMAP a été ajouté avec succès !'
            );

            redirect('/amap/gestionnaire_display/'.$amap->getId());
        }

        $this->loadViewWithTemplate('amap/gestionnaire_display', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Supprimer le droit gestionnaire à un amapien.
     *
     * @param $amap_id
     * @param $amapien_id
     */
    public function gestionnaire_remove($amapien_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $amap = $amapien->getAmap();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        $amapien->setGestionnaire(false);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le correspondants Clic\'AMAP a été supprimés avec succès !'
        );

        redirect('/amap/gestionnaire_display/'.$amap->getId());
    }

    /**
     * Créer un mot de passe pour une AMAP et lui envoyer par email.
     *
     * @param $amap_id
     */
    public function password($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_SEND_PASSWORD, $amap);

        $password = $this->passwordGenerator->generatePassword();
        $pass_hash = $this->passwordEncoder->encodePassword($password);

        $amap->setPassword($pass_hash);
        $amap->setEtat(\PsrLib\ORM\Entity\Amap::ETAT_FONCIONNEMENT);
        $this->em->flush();

        $email = $amap->getEmail();

        $this->email->from(EMAIL, FROM);
        $this->email->to($email);
        $this->email->subject('Bienvenue sur '.SITE_NAME);
        $this->email->message('<p>Bonjour, voici vos identifiants pour vous connecter à '.SITE_NAME.' :<br/>
      Adresse email : '.$email.'<br/>
      Mot de passe : '.$password.'</p>
      <a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a>');

        if ($this->email->send()) {
            $this->addFlash(
                'notice_success',
                'Le mot de passe a été envoyé avec succès !'
            );
        } else {
            $this->addFlash(
                'notice_success',
                'Le mot de passe n\'a pas pu être envoyé.'
            );
        }

        $this->email->clear();

        redirect('/amap');
    }

    /**
     * Page de modification du mot de passe d'une AMAP.
     *
     * @param $amap_id
     */
    public function mot_de_passe($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_PASSWORD, $amap);

        $password1 = $this->input->post('password_1', false);
        $this->form_validation->set_rules('password_1', '"Mot de passe"', 'required|min_length[8]|password_strength_check', [
            'password_strength_check' => 'Mot de passe trop faible',
        ]);
        $this->form_validation->set_rules('password_2', '"Confirmez le mot de passe"', 'required|min_length[8]|password_check['.$password1.']', [
            'password_check' => 'Les deux mot de passe ne correspondent pas',
        ]);
        if ($this->form_validation->run()) {
            // Re initialise le mot de passe et supprime le token
            $amap->setPlainPassword($password1);
            $this->em->flush();

            return redirect('/evenement');
        }

        $page = $this->load->view(
            'amap/mot_de_passe',
            [
                'amap' => $amap,
            ],
            true
        );

        return $this->load->view('template', ['page' => $page]);
    }

    /**
     * Test la sécurité du mot de passe dans la page de protection des AMAPS.
     *
     * @param $amap_id
     */
    public function mot_de_passe_test($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        // Sécurité
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_PASSWORD, $amap);

        generate_zxcvbn_response();
    }

    /**
     * Supprimer une AMAP.
     *
     * @param $amap_id
     */
    public function remove($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        // Sécurité
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_STATE, $amap);

        $this->em->remove($amap);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'AMAP a été supprimée avec succès'
        );

        redirect('/amap');
    }

    private function wizard_store_session_data(PsrLib\DTO\AmapCreationWizard $wizard)
    {
        $amap = $wizard->getAmap();
        if (null !== $amap) {
            $amap->getTpPropose()->clear();
            $amap->getTpRecherche()->clear();
        }
        $this->sfSession->set('amap_wizard', serialize($wizard));
    }

    /**
     * @return false|\PsrLib\DTO\AmapCreationWizard
     */
    private function wizard_get_session_data()
    {
        /** @var false|\PsrLib\DTO\AmapCreationWizard $wizard */
        $wizard = unserialize($this->sfSession->get('amap_wizard'));

        if (false === $wizard) {
            return false;
        }

        $amap = $wizard->getAmap();
        if (null !== $amap) {
            foreach ($wizard->getTpPIds() as $tpPId) {
                $amap->addTpPropose($this->em->find(\PsrLib\ORM\Entity\TypeProduction::class, $tpPId));
            }
            foreach ($wizard->getTpRIds() as $tpPId) {
                $amap->addTpRecherche($this->em->find(\PsrLib\ORM\Entity\TypeProduction::class, $tpPId));
            }

            $amap->setAdresseAdminVille($this->em->find(Ville::class, $amap->getAdresseAdminVille()->getId()));
        }

        $ll = $wizard->getLivraisonLieu();
        if (null !== $ll) {
            $ll->setVille($this->em->find(Ville::class, $ll->getVille()->getId()));
        }

        $gestionnaire = $wizard->getGestionnaire();
        if (null !== $gestionnaire) {
            $gestionnaire->setVille($this->em->find(Ville::class, $gestionnaire->getVille()->getId()));
        }

        return $wizard;
    }

    private function wizard_clear_session_data()
    {
        $this->sfSession->set('amap_wizard', null);
    }

    /**
     * Générer la CARTOGRAPHIE.
     *
     * @var \PsrLib\ORM\Entity\Amap[]
     *
     * @param mixed $amaps
     */
    private function cartographie($amaps)
    {
        $this->load->library('leaflet');

        $gps = 0;
        $map = null;

        /** @var \PsrLib\ORM\Entity\Amap $amap */
        foreach ($amaps as $amap) {
            /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $liv_lieu */
            foreach ($amap->getLivraisonLieux() as $liv_lieu) {
                if ($liv_lieu->getGpsLatitude()) {
                    ++$gps;

                    $config = [
                        'center' => $liv_lieu->getGpsLatitude()
                            .','
                            .$liv_lieu->getGpsLongitude(),
                        'zoom' => 7,
                    ];

                    $this->leaflet->initialize($config);

                    // infos du popup
                    $infos = '<strong>'.$amap->getNom().'</strong>';
                    if ($liv_lieu->getNom()) {
                        $infos = $infos.'<br/>'.$liv_lieu->getNom();
                    }
                    if ($liv_lieu->getAdresse()) {
                        $infos = $infos.'<br/>'.$liv_lieu->getAdresse();
                    }
                    if ($liv_lieu->getVille()->getCpString()) {
                        $infos = $infos.'<br/>'.$liv_lieu->getVille()->getCpString();
                    }
                    if ($liv_lieu->getVille()->getNom()) {
                        $infos = $infos.', '.$liv_lieu->getVille()->getNom();
                    }
                    if ($amap->getEmail()) {
                        $infos = $infos.'<br/>'.$amap->getEmail();
                    }
                    if ($amap->getUrl()) {
                        $infos = $infos.'<br/>'.$amap->getUrl();
                    }

                    // Emplacement du marker
                    $marker = [
                        'latlng' => $liv_lieu->getGpsLatitude().
                            ','.
                            $liv_lieu->getGpsLongitude(), // Marker Location
                        // popup
                        'popupContent' => addslashes($infos), // Popup Content
                    ];

                    $this->leaflet->add_marker($marker);
                }
            }

            if ($gps > 0) {
                $map = $this->leaflet->create_map();
            }
        }

        return [$gps, $map];
    }
}
