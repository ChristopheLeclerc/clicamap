<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<script type="text/javascript">

  /*ALERTES*/
  $(document).ready(function () {

      <?php

      // Erreur de formulaire dans la MODAL
      if ($ID) {
          echo '$(\'#modal_envoi_'.$ID.'\').modal(\'show\');';
      }

      ?>

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 3000);

  });

</script>

<?php
// ALERTES
if ($envoi) {
    ?>
    <div class="modal fade" id="alerte">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">

                    <?php

                    if ($envoi) {
                        echo '<p><i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> Le message été envoyé avec succès !</p>';
                    } ?>

                </div>
            </div>
        </div>
    </div>

    <?php
}

echo '<div class="btn btn-info pull-right" data-toggle="collapse" data-target="#info" style="margin-top:5px;"><i class="glyphicon glyphicon-info-sign"></i> Bulle info</div>';

echo '<h3>Contacter un administrateur réseau</h3>';

echo '<div class="alert alert-info collapse" id="info">Pour prendre contact avec un administrateur réseau, sélectionner d\'abord la région et / ou le département et / ou le réseau local via le moteur de recherche ci-dessous. Choisissez ensuite votre interlocuteur dans la liste générée.</div>';

// CRITÈRES DE RECHERCHES

echo '<form method="POST" action="'.site_url('contact/admin').'">';

echo '<div class="well">';

echo '<div class="row">';

?>

<div class="col-md-4">
    <div class="input-group">

<span class="input-group-btn">
<button class="btn btn-primary" type="button">Région</button>
</span>

        <select class="form-control" id="region_id" name="region_id" onchange="this.form.submit();">

            <?php

            echo '<option value="0"></option>';

            foreach ($region_all as $region) {
                // Selected en Session Recherche
                if ($this->session->recherche_contact_admin && $this->session->recherche_contact_admin['region_id'] == $region->reg_id) {
                    echo '<option value="'.$region->reg_id.'" selected >'.$region->reg_nom.'</option>';
                } // Set_value (utile ?)
                elseif (set_value('region_id') == $region->reg_id) {
                    echo '<option value="'.$region->reg_id.'" selected >'.$region->reg_nom.'</option>';
                } // Affichage normal
                else {
                    echo '<option value="'.$region->reg_id.'">'.$region->reg_nom.'</option>';
                }
            }

            ?>

        </select>

    </div>
</div>

<div class="col-md-4">
    <div class="input-group">

        <?php

        if (set_value('region_id') || $this->session->recherche_contact_admin['region_id']) {
            ?>

        <span class="input-group-btn">
<button class="btn btn-primary" type="button">Département</button>
</span>

        <select class="form-control" id="departement_id" name="departement_id" onchange="this.form.submit();">

            <?php

            echo '<option value="0"></option>';

            if ($departement_all) {
                foreach ($departement_all as $departement) {
                    if ($this->session->recherche_contact_admin && $this->session->recherche_contact_admin['departement_id'] == $departement->dep_id) {
                        echo '<option value="'.$departement->dep_id.'" selected >'.$departement->dep_nom.'</option>';
                    } elseif (set_value('departement_id') == $departement->dep_id) {
                        echo '<option value="'.$departement->dep_id.'" selected >'.$departement->dep_nom.'</option>';
                    } else {
                        echo '<option value="'.$departement->dep_id.'">'.$departement->dep_nom.'</option>';
                    }
                }
            }

            echo '</select>';
        } else {
            echo '<span class="input-group-btn">
<button class="btn btn-primary" type="button" disabled>Département</button>
</span>
    
<select class="form-control" disabled>

<option value="0"></option>

</select>';
        }

            ?>

    </div>
</div>

<?php

echo '<div class="col-md-4">';

echo '<div class="input-group">';

if ($reseau_all) {
    ?>

    <span class="input-group-btn">
<button class="btn btn-primary" type="button">Réseau</button>
</span>

    <!-- En session Admin Réseau ou Gestionnaire AMAP, on bloque le select sur Readonly -->
    <select class="form-control" id="reseau_id" name="reseau_id" onchange="this.form.submit();">

        <?php

        echo '<option value="0"></option>';

    if ($reseau_all) {
        foreach ($reseau_all as $res) {
            if ($this->session->recherche_contact_admin && $this->session->recherche_contact_admin['reseau_id'] == $res->res_id) {
                echo '<option value="'.$res->res_id.'" selected >'.$res->res_nom.'</option>';
            } elseif (set_value('reseau_id') == $res->res_id) {
                echo '<option value="'.$res->res_id.'" selected >'.$res->res_nom.'</option>';
            } else {
                echo '<option value="'.$res->res_id.'">'.$res->res_nom.'</option>';
            }
        }
    } ?>

    </select>

    <?php
} else {
        echo '<span class="input-group-btn">
<button class="btn btn-primary" type="button" disabled>Réseau</button>
</span>
    
<select class="form-control" disabled></select>';
    }

?>

</div>
</div>

</div>

</div>

</form>

<?php

// FIN DES CRITÈRES DE RECHERCHES / LISTE

if ($amapien_all) {
    ?>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-inverse">
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Téléphone 1</th>
                <th>Téléphone 2</th>
                <th>Adresse</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <?php

            echo '<tbody>';

    foreach ($amapien_all as $amapien) {
        echo '<tr>';

        echo '<td>';
        echo strtoupper($amapien->a_nom);
        echo '</td>';

        echo '<td>';
        echo ucfirst($amapien->a_prenom);
        echo '</td>';

        echo '<td>';
        echo ucfirst($amapien->a_num_tel_1);
        echo '</td>';

        echo '<td>';
        echo ucfirst($amapien->a_num_tel_2);
        echo '</td>';

        if ($amapien->a_code_postal && $amapien->a_ville) {
            echo '<td>';
            if ($amapien->a_lib_adr_1) {
                echo ucfirst($amapien->a_lib_adr_1);
            } elseif ($amapien->a_lib_adr_2) {
                echo ucfirst($amapien->a_lib_adr_2);
            }

            echo ', '.$amapien->a_code_postal.' '.strtoupper($amapien->a_ville);
            echo '</td>';
        }

        echo '<td class="text-right">';

        echo '<a href="#" data-toggle="modal" data-target="#modal_envoi_'.$amapien->a_id.'" ><button type=button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Email</button></a>'; ?>

                <div class="modal fade text-left" id="modal_envoi_<?= $amapien->a_id; ?>" tabindex="-1"
                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h5 class="modal-title" id="myModalLabel">Envoyer un email
                                    à <?= strtoupper($amapien->a_nom).' '.ucfirst($amapien->a_prenom); ?></h5>
                            </div>
                            <div class="modal-body">

                                <form method="POST" action="<?= site_url('contact/admin_envoi'); ?>">

                                    <input type="hidden" name="ID" value="<?= $amapien->a_id; ?>"/>

                                    <?php if (!$this->session->amapien && !$this->session->paysan) {
            ?>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
    
<span class="input-group-btn">
<button class="btn btn-primary" type="button">Vous êtes</button>
</span>

                                                    <div class="form-group">
                                                        <select class="form-control" name="statut">
                                                            <option value=""></option>
                                                            <option value="1" <?= set_select('statut', 1); ?>>Un
                                                                amapien
                                                            </option>
                                                            <option value="2" <?= set_select('statut', 2); ?>>Un
                                                                paysan
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?= form_error('statut'); ?>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group">
    
<span class="input-group-btn">
<button class="btn btn-primary" type="button">Votre nom</button>
</span>

                                                    <div class="form-group">
                                                        <input type="text" name="nom" class="form-control"
                                                               value="<?= set_value('nom'); ?>"/>
                                                    </div>

                                                </div>
                                                <?= form_error('nom'); ?>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="input-group">
    
<span class="input-group-btn">
<button class="btn btn-primary" type="button">Votre prénom</button>
</span>

                                                    <div class="form-group">
                                                        <input type="text" name="prenom" class="form-control"
                                                               value="<?= set_value('prenom'); ?>"/>
                                                    </div>

                                                </div>
                                                <?= form_error('prenom'); ?>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
    
<span class="input-group-btn">
<button class="btn btn-primary" type="button">Votre email</button>
</span>

                                                    <div class="form-group">
                                                        <input type="text" name="email" class="form-control"
                                                               value="<?= set_value('email'); ?>"/>
                                                    </div>

                                                </div>
                                                <?= form_error('email'); ?>
                                            </div>
                                        </div>

                                        <?php
        } ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
    
<span class="input-group-btn">
<button class="btn btn-primary" type="button">Objet</button>
</span>

                                                <div class="form-group">
                                                    <input type="text" name="objet" class="form-control"
                                                           value="<?= set_value('objet'); ?>"/>
                                                </div>
                                            </div>
                                            <?= form_error('objet'); ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <textarea class="form-control" name="message"
                                                          rows="3"><?= set_value('message'); ?></textarea>
                                                <?= form_error('message'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="submit" value="Envoyer"
                                                       class="pull-right btn btn-success"/>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                </td>
                </tr>

                <?php
    } ?>

            </tbody>
        </table>
    </div>

    <?php
}

?>
