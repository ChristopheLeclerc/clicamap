<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use DI\Annotation\Inject;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Ajax.
 */
class Ajax extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\Geocoder
     */
    public $geocoder;
    protected $data;

    /**
     * Ajax constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Autocomplétion CP / Ville.
     */
    public function get_cp_ville_suggestion()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AJAX);

        if (!isset($_GET['term'])) {
            return;
        }
        $term = $_GET['term'];
        /** @var \PsrLib\ORM\Entity\Ville[] $villes */
        $villes = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ville::class)
            ->getByCpOrdered($term)
        ;
        $row_set = [];
        foreach ($villes as $ville) {
            $new_row['label'] = $ville->getCpString().', '.$ville->getNom();
            $new_row['value'] = $ville->getCpString().', '.$ville->getNom();
            $row_set[] = $new_row; // construit un tableau
        }
        echo json_encode($row_set);
    }

    /**
     * Autocomplétion amapien
     * url /region/dep_admin/01/84.
     *
     * @param null|mixed $amap_id
     */
    public function get_amapien_suggestion($amap_id = null)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AJAX);
        $amap = null;
        if (null === $amap_id) {
            $currentUser = $this->getUser();
            if (!($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isSuperAdmin())) {
                $this->exit_error();
            }
        } else {
            /** @var \PsrLib\ORM\Entity\Amap $amap */
            $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        }

        if (!isset($_GET['term'])) {
            return;
        }

        $term = $_GET['term'];
        /** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->searchByName($term, $amap)
        ;
        $row_set = [];
        foreach ($amapiens as $amapien) {
            $new_row['label'] = $amapien->getNom().' '.$amapien->getPrenom().' ('.$amapien->getEmail().')';
            $new_row['value'] = $amapien->getEmail();
            $row_set[] = $new_row; //build an array
        }

        echo json_encode($row_set);
    }

    /**
     * Autocomplétion paysan.
     */
    public function get_paysan_suggestion()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AJAX);

        if (!isset($_GET['term'])) {
            return;
        }

        $term = $_GET['term'];

        /** @var \PsrLib\ORM\Entity\Paysan[] $paysans */
        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->searchByName($term)
        ;

        $row_set = [];
        foreach ($paysans as $paysan) {
            $new_row['label'] = $paysan->getNom().' '.$paysan->getPrenom().' ('.$paysan->getEmail().')';
            $new_row['value'] = $paysan->getEmail();
            $row_set[] = $new_row; //build an array
        }

        echo json_encode($row_set);
    }

    /**
     * Autocomplétition des adresses.
     *
     * @return false|string
     */
    public function geocode_get_suggestion()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AJAX);

        $query = $this->input->get('term');
        if (null === $query) {
            return json_encode([]);
        }

        $villeRepo = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ville::class)
        ;

        $suggestions = $this->geocoder->geocode_suggestions($query);

        // Deduplication des labels des suggestions
        $suggestionsDeduplicated = [];
        foreach ($suggestions as $suggestion) {
            if (array_key_exists($suggestion['label'], $suggestionsDeduplicated)) {
                continue;
            }

            $suggestionsDeduplicated[$suggestion['label']] = $suggestion;
        }

        // Filtre les suggestions pour ne garder que celles dont (cp,ville) est compatible avec les inputs
        $transliterator = Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: NFC;', Transliterator::FORWARD);
        $res = [];
        foreach ($suggestionsDeduplicated as $suggestion) {
            $suggestionVilleNormalise = $transliterator->transliterate($suggestion['value']['ville']);
            if (false === $suggestionVilleNormalise) {
                continue;
            }
            /** @var \PsrLib\ORM\Entity\Ville[] $dbVilles */
            $dbVilles = $villeRepo->findBy([
                'cp' => $suggestion['value']['cp'],
            ]);
            foreach ($dbVilles as $dbVille) {
                $dbVilleNormalise = $transliterator->transliterate($dbVille->getNom());
                if (false === $dbVilleNormalise) {
                    continue;
                }
                if (false !== mb_stripos($dbVilleNormalise, $suggestionVilleNormalise)
                    || false !== mb_stripos($suggestionVilleNormalise, $dbVilleNormalise)) {
                    $_suggestion = $suggestion;
                    $_suggestion['label'] = sprintf(
                        '%s (%s %s)',
                        $suggestion['label'],
                        $dbVille->getCpString(),
                        $dbVille->getNom()
                    );
                    $_suggestion['value']['ville'] = $dbVille->getNom();
                    $res[] = $_suggestion;
                }
            }
        }

        echo json_encode($res);
    }
}
