<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
?>

<script type="text/javascript">

  /*AUTOCOMPLÉTION*/
  $(document).ready(function () {

    $(function () {
      $("#gest_cp_ville").autocomplete({
        source: <?= '"'.site_url('ajax/get_cp_ville_suggestion"'); ?>,
        dataType: "json",
        minLength: 4,
        appendTo: "#container"
      });
    });

  });

</script>

<h3>Ajout d'un correspondant Clic'AMAP</h3>

<h5><a href="<?= site_url('amap'); ?>">Gestion des AMAP</a> <i class="glyphicon glyphicon-menu-right"></i>
    Création d'une AMAP <i class="glyphicon glyphicon-menu-right"></i> Étape 6</h5>

<form method="POST" action="/amap/gestionnaire_creation">

    <div class="alert alert-info">Un correspondant Clic'AMAP doit être associé à cette AMAP. L'amapien est créé en
        passant par les champs d'ajout rapide ci-dessous.
    </div>

    <div class="well">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-md-2">
                    <label for="gest_nom" class="control-label">Nom : </label>
                    <?= render_form_text_widget($form['nom']); ?>
                </div>

                <div class="form-group col-md-2">
                    <label for="gest_prenom" class="control-label">Prénom : </label>
                    <?= render_form_text_widget($form['prenom']); ?>
                </div>


                <div class="form-group col-md-2">
                    <label for="gest_email" class="control-label">Email : </label>
                    <?= render_form_text_widget($form['email']); ?>
                </div>

                <div class="form-group col-md-2">
                    <label for="gest_lib_adr_1" class="control-label">Adresse : </label>
                    <?= render_form_text_widget($form['libAdr1']); ?>
                </div>

                <div class="form-group col-md-4">
                    <label for="gest_cp_ville" class="control-label">Code postal, Ville : </label>
                    <?= render_form_text_widget($form['ville']); ?>
                </div>

            </div>
        </div>
    </div>

    <div class="form-group">
        <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>

</form>

<a href="/amap/produits_creation" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
<a href="<?= site_url('amap'); ?>" class="pull-right btn btn-default">Annuler</a>
