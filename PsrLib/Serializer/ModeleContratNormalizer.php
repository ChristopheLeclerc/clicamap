<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\ModeleContratProduitExclure;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ModeleContratNormalizer implements DenormalizerAwareInterface, ContextAwareDenormalizerInterface
{
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        /** @var ModeleContrat $mc */
        $mc = $this->denormalizer->denormalize($data, ModeleContrat::class);

        $fermeId = $data['ferme']['id'] ?? null;
        if (null !== $fermeId) {
            $mc->setFerme($this->em->getReference(Ferme::class, $fermeId));
        }

        $llId = $data['livraisonLieu']['id'] ?? null;
        if (null !== $llId) {
            $mc->setLivraisonLieu($this->em->getReference(AmapLivraisonLieu::class, $llId));
        }

        $mc->getProduits()->clear();
        foreach ($data['produits'] as $produit) {
            /** @var ModeleContratProduit $produitDenormalized */
            $produitDenormalized = $this->denormalizer->denormalize($produit, ModeleContratProduit::class, null, [
                'groups' => 'wizard',
            ]);
            $produitDenormalized->getExclusions()->clear();
            $mc->addProduit($produitDenormalized);

            // Exclusion des produits
            foreach ($produit['exclusions'] as $exclusion) {
                /** @var false|ModeleContratDate $produitDate */
                $produitDate = $mc
                    ->getDates()
                    ->filter(function (ModeleContratDate $date) use ($exclusion) {
                        return Carbon::instance($date->getDateLivraison())->isSameAs('Y-m-d', $exclusion['date']['date']);
                    })
                    ->first()
                ;
                if (false === $produitDate) {
                    continue;
                }
                $exclusionEntity = new ModeleContratProduitExclure();
                $produitDate->addExclusion($exclusionEntity);
                $produitDenormalized->addExclusion($exclusionEntity);
            }
        }

        return $mc;
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return ModeleContrat::class === $type
            && isset($context['groups'])
            && 'wizard' === $context['groups']
        ;
    }
}
