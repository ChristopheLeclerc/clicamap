# language: fr
Fonctionnalité: Vérification de l'envoi des mails aux bonnes adresses

  @database
  Plan du Scénario: Vérification de l'envoi aux bonnes adresses mail
    Etant donné que je suis sur "/portail/contact"
    Lorsque je remplis le texte suivant:
      | contact_nom     | Martin                  |
      | contact_prenom  | Eugène                  |
      | contact_email   | test@test.amap-aura.org |
      | contact_cp      | <contact_cp>            |
      | contact_contenu | Du contenu              |
      | contact_captcha | captchaValid            |
    Et que je presse "Envoyer"
    Alors je devrais voir "Votre message est bien envoyé. Merci et bonne journée"
    Et un mail avec le sujet "Nouveau message du formulaire de contact Clic'AMAP" devrait avoir été envoyé à "<mail_envoye>"

    Exemples:
      | contact_cp | mail_envoye                       |
      | 69000      | admin.rhone@test.amap-aura.org    |
      | 01000      | admin.aura@test.amap-aura.org     |
      | 59000      | admin.reslille@test.amap-aura.org |
      | 59100      | admin.reslille@test.amap-aura.org |
      | 59200      | admin.reslille@test.amap-aura.org |
      | 21000      | clicamap@amap-aura.org            |
