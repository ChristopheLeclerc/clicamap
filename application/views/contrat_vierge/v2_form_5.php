<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
/** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $produits */
/** @var \PsrLib\ORM\Entity\ModeleContratDate[] $dates */
/** @var array $livraisons_exclues */
?>
<h3><?= $contrat->getNom(); ?> : Calendrier des livraisons</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Création
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_2'); ?>">Étape 2</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_3'); ?>">Étape 3</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_4'); ?>">Étape 4</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 5
</h5>

<form method="POST" action="">

    <div class="well well-sm">
        <div class="row">

            <div class="col-xs-12">
                <div class="alert alert-info">
                    Il restera possible de modifier une date de livraison paysan en cas de nécessité (produits non prêts, contrainte producteur, ...).
                </div>
            </div>

            <div class="col-xs-12">

                <table class="table text-center">
                    <tr>
                        <th>

                        </th>
                        <?php

                        foreach ($produits as $produit) {
                            echo "<th class='text-center'>{$produit->getNom()}</th>";
                        } ?>

                    </tr>

                <?php foreach ($dates as $date) {
                            $dateString = $date->getDateLivraison()->format('Y-m-d');
                            echo '<tr>';
                            echo "<td class='text-left'>{$dateString}</td>";
                            foreach ($produits as $produit) {
                                $produitId = $produit->getFermeProduit()->getId();
                                $checkName = "liv[{$dateString}][{$produitId}]";
                                $checked = set_checkbox(
                                    $checkName,
                                    '',
                                    false === $livraisons_exclues[$dateString][$produitId]
                                );
                                echo "<td>
                            <input 
                                type='checkbox'
                                name={$checkName}
                                {$checked}
                            />
                        </td>";
                            }
                            echo '</tr>';
                        }
                ?>

                </table>

                <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>
                <a href="<?= site_url('/contrat_vierge/v2_contrat_form_4'); ?>" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
                <a href="<?= site_url('contrat_vierge'); ?>" class="pull-right btn btn-default">Annuler</a>
            </div>
        </div>
    </div>
</form>

