<?php

namespace PsrLib\Form;

use PsrLib\Form\Type\TimeType;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapDistributionDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tache', TextType::class, [
                'label' => 'Tâche*',
            ])
            ->add('heureDebut', TimeType::class, [
                'label' => 'Heure de début*',
            ])
            ->add('heureFin', TimeType::class, [
                'label' => 'Heure de fin*',
            ])
            ->add('nbPersonnes', IntegerType::class, [
                'label' => 'Nombre de personnes nécessaires*',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AmapDistributionDetail::class,
        ]);
    }
}
