<?php

namespace Test\ORM\Entity;

use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;
use Test\Services\Security\AbstractSecurityTest;

/**
 * @internal
 * @coversNothing
 */
class PaysanDisplayProfilTest extends AbstractSecurityTest
{
    public function testFermeRegroupementValid()
    {
        $regroupement = new FermeRegroupement();
        $ferme = new Ferme();
        $ferme->setRegroupement($regroupement);
        $paysan = new Paysan();
        $paysan->setFerme($ferme);

        $this->mockAuthenticatedUser($regroupement);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_DISPLAY_PROFIL, $paysan));
    }

    public function testFermeRegroupementInvalid()
    {
        $regroupement = new FermeRegroupement();

        $ferme = new Ferme();
        $paysan = new Paysan();
        $paysan->setFerme($ferme);

        $this->mockAuthenticatedUser($regroupement);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_PAYSAN_DISPLAY_PROFIL, $paysan));
    }
}
