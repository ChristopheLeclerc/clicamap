<?php

namespace Test\Services\EntityBuilder;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\Services\EntityBuilder\AmapDistributionDetailMassEditDtoBuilder;

/**
 * @internal
 * @coversNothing
 */
class AmapDistributionDetailMassEditDtoBuilderTest extends TestCase
{
    private $builder;

    public function setUp()
    {
        $this->builder = new AmapDistributionDetailMassEditDtoBuilder();
    }

    public function testEmpty()
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('Need at least one distirbution');

        $this->builder->buildFromMultipleDistributions([]);
    }

    public function testSingle()
    {
        $detail = new AmapDistributionDetail();
        $detail
            ->setTache('tache')
            ->setNbPersonnes(12)
            ->setHeureDebut(new Time(10, 10))
            ->setHeureFin(new Time(15, 10))
        ;
        $distribution = new AmapDistribution(
            $detail,
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            new Carbon()
        );
        $ll = new AmapLivraisonLieu();
        $distribution->setAmapLivraisonLieu($ll);

        $out = $this->builder->buildFromMultipleDistributions([$distribution]);
        self::assertSame('tache', $out->getDetail()->getTache());
        self::assertSame(12, $out->getDetail()->getNbPersonnes());
        self::assertEquals(new Time(10, 10), $out->getDetail()->getHeureDebut());
        self::assertEquals(new Time(15, 10), $out->getDetail()->getHeureFin());
        self::assertSame($ll, $out->getLivraisonLieu());
    }

    public function testMultipleSameValues()
    {
        $ll = new AmapLivraisonLieu();

        $detail1 = new AmapDistributionDetail();
        $detail1
            ->setTache('tache')
            ->setNbPersonnes(12)
            ->setHeureDebut(new Time(10, 10))
            ->setHeureFin(new Time(15, 10))
        ;
        $distribution1 = new AmapDistribution(
            $detail1,
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            new Carbon()
        );
        $distribution1->setAmapLivraisonLieu($ll);
        $detail2 = new AmapDistributionDetail();
        $detail2
            ->setTache('tache')
            ->setNbPersonnes(12)
            ->setHeureDebut(new Time(10, 10))
            ->setHeureFin(new Time(15, 10))
        ;
        $distribution2 = new AmapDistribution(
            $detail2,
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            new Carbon()
        );
        $distribution2->setAmapLivraisonLieu($ll);

        $out = $this->builder->buildFromMultipleDistributions([$distribution1, $distribution2]);
        self::assertSame('tache', $out->getDetail()->getTache());
        self::assertSame(12, $out->getDetail()->getNbPersonnes());
        self::assertEquals(new Time(10, 10), $out->getDetail()->getHeureDebut());
        self::assertEquals(new Time(15, 10), $out->getDetail()->getHeureFin());
        self::assertSame($ll, $out->getLivraisonLieu());
    }

    public function testMultipleDiffrentValues()
    {
        $detail1 = new AmapDistributionDetail();
        $detail1
            ->setTache('tache1')
            ->setNbPersonnes(12)
            ->setHeureDebut(new Time(10, 10))
            ->setHeureFin(new Time(15, 10))
        ;
        $distribution1 = new AmapDistribution(
            $detail1,
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            new Carbon()
        );
        $ll1 = new AmapLivraisonLieu();
        $distribution1->setAmapLivraisonLieu($ll1);

        $detail2 = new AmapDistributionDetail();
        $detail2
            ->setTache('tache2')
            ->setNbPersonnes(13)
            ->setHeureDebut(new Time(10, 11))
            ->setHeureFin(new Time(15, 11))
        ;
        $distribution2 = new AmapDistribution(
            $detail2,
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            new Carbon()
        );
        $ll2 = new AmapLivraisonLieu();
        $distribution1->setAmapLivraisonLieu($ll2);

        $out = $this->builder->buildFromMultipleDistributions([$distribution1, $distribution2]);
        self::assertNull($out->getDetail()->getTache());
        self::assertNull($out->getDetail()->getNbPersonnes());
        self::assertNull($out->getDetail()->getHeureDebut());
        self::assertNull($out->getDetail()->getHeureFin());
        self::assertNull($out->getLivraisonLieu());
    }
}
