<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\Annotation\DraftPropertie;
use PsrLib\Validator\UniqueEntity;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Amapien.
 *
 * @ORM\Table(name="ak_amapien", uniqueConstraints={@ORM\UniqueConstraint(name="a_mdpreinit_token", columns={"a_mdpreinit_token"}), @ORM\UniqueConstraint(name="a_email", columns={"a_email"})})
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AmapienRepository")
 * @UniqueEntity(fields={"email"}, message="Cet email existe déjà dans la base de donnée.")
 */
class Amapien extends BaseUser
{
    public const ETAT_ACTIF = 'actif';
    public const ETAT_INACTIF = 'inactif';
    public const ETAT_AVALIABLE = [
        self::ETAT_ACTIF, self::ETAT_INACTIF,
    ];

    /**
     * @var ?int
     *
     * @ORM\Column(name="a_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizardContract"})
     */
    private $id;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_prenom", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     */
    private $prenom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_nom", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     */
    private $nom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_email", type="string", length=255, nullable=true)
     * @Assert\Email()
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private $email;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_password", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $password;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_num_tel_1", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $numTel1;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_num_tel_2", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $numTel2;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_lib_adr_1", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $libAdr1;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_lib_adr_1_complement", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $libAdr1Complement;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_lib_adr_2", type="string", length=255, nullable=true)
     */
    private $libAdr2;

    /**
     * @var string
     *
     * @ORM\Column(name="a_etat", type="string", length=50, nullable=false)
     */
    private $etat = self::ETAT_INACTIF;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="newsletter", type="boolean", nullable=true)
     */
    private $newsletter;

    /**
     * @DraftPropertie
     *
     * @var null|string
     *
     * @ORM\Column(name="a_mdpreinit_token", type="string", length=255, nullable=true)
     */
    private $aMdpreinitToken;

    /**
     * @DraftPropertie
     *
     * @var null|DateTime
     *
     * @ORM\Column(name="a_mdpreinit_creationdate", type="datetime", nullable=true)
     */
    private $aMdpreinitCreationdate;

    /**
     * @var null|string
     *
     * @ORM\Column(name="a_uuid", type="string", length=255, nullable=true)
     */
    private $uuid;

    /**
     * @var bool
     *
     * @ORM\Column(name="a_email_invalid", type="boolean")
     */
    private $emailInvalid = false;

    /**
     * @var ArrayCollection<Region>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Region", mappedBy="admins")
     * @JoinTable(name="ak_region_administrateur",
     *      joinColumns={@JoinColumn(name="reg_adm_fk_amapien_id", referencedColumnName="a_id")},
     *      inverseJoinColumns={@JoinColumn(name="reg_adm_fk_region_id", referencedColumnName="reg_id")}
     * )
     */
    private $adminRegions;

    /**
     * @var ArrayCollection<Departement>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Departement", mappedBy="admins")
     * @JoinTable(name="ak_departement_administrateur",
     *      joinColumns={@JoinColumn(name="dep_adm_fk_amapien_id", referencedColumnName="a_id")},
     *      inverseJoinColumns={@JoinColumn(name="dep_adm_fk_departement_id", referencedColumnName="dep_id")}
     * )
     */
    private $adminDepartments;

    /**
     * @var ArrayCollection<Reseau>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Reseau", mappedBy="admins")
     * @JoinTable(name="ak_reseau_administrateur",
     *      joinColumns={@JoinColumn(name="res_adm_fk_amapien_id", referencedColumnName="a_id")},
     *      inverseJoinColumns={@JoinColumn(name="res_adm_fk_reseau_id", referencedColumnName="res_id")}
     * )
     */
    private $adminReseaux;

    /**
     * @var null|Amap
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amap", inversedBy="amapiens")
     * @ORM\JoinColumn(referencedColumnName="amap_id", nullable=true, onDelete="SET NULL")
     */
    private $amap;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $gestionnaire = false;

    /**
     * @var ArrayCollection<Ferme>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Ferme", mappedBy="amapienRefs")
     * @JoinTable(
     *     joinColumns={@JoinColumn(referencedColumnName="a_id")},
     *     inverseJoinColumns={@JoinColumn(referencedColumnName="f_id")}
     * )
     */
    private $refProdFermes;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $superAdmin = false;

    /**
     * @var ArrayCollection<AmapienAnneeAdhesion>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\AmapienAnneeAdhesion", mappedBy="amapien", cascade="PERSIST", orphanRemoval=true)
     */
    private $anneeAdhesions;

    /**
     * @var null|Ville
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ville")
     * @ORM\JoinColumn(referencedColumnName="v_id")
     * @Assert\NotBlank(groups={"adhesion_info"})
     */
    private $ville;

    /**
     * @var ArrayCollection<Ferme>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Ferme", inversedBy="amapienAttentes")
     * @JoinTable(
     *     name="ak_ferme_amapien_liste_attente",
     *     joinColumns={@JoinColumn(name="f_a_la_fk_amapien_id", referencedColumnName="a_id")},
     *     inverseJoinColumns={@JoinColumn(name="f_a_la_fk_ferme_id", referencedColumnName="f_id")}
     * )
     */
    private $fermeAttentes;

    /**
     * @var null|Amap
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Amap", mappedBy="amapienRefReseau")
     */
    private $amapAsRefReseau;

    /**
     * @var null|Amap
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Amap", mappedBy="amapienRefReseauSecondaire")
     */
    private $amapAsRefReseauSec;

    /**
     * @var ArrayCollection<Contrat>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\Contrat", mappedBy="amapien", orphanRemoval=true)
     */
    private $contrats;

    /**
     * Additional information to generate network adhesion by this user.
     *
     * @var AmapienAdhesionInfo
     * @Assert\Valid(groups={"adhesion_info"})
     * @ORM\Embedded(class="PsrLib\ORM\Entity\AmapienAdhesionInfo")
     */
    private $adhesionInfo;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->adminRegions = new ArrayCollection();
        $this->adminDepartments = new ArrayCollection();
        $this->adminReseaux = new ArrayCollection();
        $this->refProdFermes = new ArrayCollection();
        $this->fermeAttentes = new ArrayCollection();
        $this->anneeAdhesions = new ArrayCollection();
        $this->contrats = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
        $this->adhesionInfo = new AmapienAdhesionInfo();
    }

    public function __toString()
    {
        return $this->prenom.' '.$this->nom;
    }

    public function getRegion(): ?Region
    {
        if (null === $this->amap) {
            return null;
        }

        return $this->amap->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        if (null === $this->amap) {
            return null;
        }

        return $this->amap->getDepartement();
    }

    public function isAdminRegion(): bool
    {
        return $this->adminRegions->count() > 0;
    }

    public function isAdminDepartment(): bool
    {
        return $this->adminDepartments->count() > 0;
    }

    public function isAdminReseaux(): bool
    {
        return $this->adminReseaux->count() > 0;
    }

    public function isAdmin(): bool
    {
        return $this->isSuperAdmin() // Super admin
            || $this->isAdminDepartment() // Admin de région
            || $this->isAdminRegion() // Admin de département
            || $this->isAdminReseaux()
        ;
    }

    public function isRefProduit(): bool
    {
        return $this->refProdFermes->count() > 0;
    }

    public function isRefAmap(): bool
    {
        return null !== $this->amapAsRefReseau;
    }

    public function isRefAmapSec(): bool
    {
        return null !== $this->amapAsRefReseauSec;
    }

    public function isAdminOf(?LocalisableEntity $entity): bool
    {
        if (null === $entity) {
            return false;
        }

        if ($this->isAdminRegion()) {
            return $this->adminRegions->contains($entity->getRegion());
        }

        if ($this->isAdminDepartment()) {
            return $this->adminDepartments->contains($entity->getDepartement());
        }

        return false;
    }

    public function hasAmap(): bool
    {
        return null !== $this->amap;
    }

    /**
     * Verifie si l'amapien est actif.
     */
    public function estActif(): bool
    {
        return self::ETAT_ACTIF === $this->getEtat();
    }

    public function getAdresse(): string
    {
        $ville = $this->getVille();
        if (null === $ville) {
            return '';
        }

        return $this->getLibAdr1()
            .' '
            .$this->getLibAdr2()
            .' '
            .$ville->getCpString()
            .' '
            .$ville->getNom()
            ;
    }

    public function getNomComplet(): string
    {
        return $this->getPrenom().' '.$this->getNom();
    }

    public function getNomCompletInverse(): string
    {
        return $this->getNom().' '.$this->getPrenom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Amapien
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Add adminRegion.
     *
     * @return Amapien
     */
    public function addAdminRegion(Region $adminRegion)
    {
        $this->adminRegions[] = $adminRegion;
        $adminRegion->addAdmin($this);

        return $this;
    }

    /**
     * Remove adminRegion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdminRegion(Region $adminRegion)
    {
        return $this->adminRegions->removeElement($adminRegion);
    }

    /**
     * Get adminRegions.
     *
     * @return Collection
     */
    public function getAdminRegions()
    {
        return $this->adminRegions;
    }

    /**
     * Add adminDepartment.
     *
     * @return Amapien
     */
    public function addAdminDepartment(Departement $adminDepartment)
    {
        $this->adminDepartments[] = $adminDepartment;
        $adminDepartment->addAdmin($this);

        return $this;
    }

    /**
     * Remove adminDepartment.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdminDepartment(Departement $adminDepartment)
    {
        return $this->adminDepartments->removeElement($adminDepartment);
    }

    /**
     * Get adminDepartments.
     *
     * @return Collection
     */
    public function getAdminDepartments()
    {
        return $this->adminDepartments;
    }

    /**
     * Add adminReseau.
     *
     * @return Amapien
     */
    public function addAdminReseau(Reseau $adminReseau)
    {
        $this->adminReseaux[] = $adminReseau;

        return $this;
    }

    /**
     * Remove adminReseau.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdminReseau(Departement $adminReseau)
    {
        return $this->adminReseaux->removeElement($adminReseau);
    }

    /**
     * Get adminDepartments.
     *
     * @return Collection
     */
    public function getAdminReseaux()
    {
        return $this->adminReseaux;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): Amapien
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Amapien
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEtat(): string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): Amapien
    {
        $this->etat = $etat;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): Amapien
    {
        $this->password = $password;

        return $this;
    }

    public function isSuperAdmin(): bool
    {
        return $this->superAdmin;
    }

    public function setSuperAdmin(bool $superAdmin): Amapien
    {
        $this->superAdmin = $superAdmin;

        return $this;
    }

    /**
     * Add refProdFerme.
     *
     * @return Amapien
     */
    public function addRefProdFerme(Ferme $refProdFerme)
    {
        $this->refProdFermes[] = $refProdFerme;

        return $this;
    }

    /**
     * Remove refProdFerme.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRefProdFerme(Ferme $refProdFerme)
    {
        return $this->refProdFermes->removeElement($refProdFerme);
    }

    /**
     * Get refProdFermes.
     *
     * @return Collection
     */
    public function getRefProdFermes()
    {
        return $this->refProdFermes;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap): Amapien
    {
        $this->amap = $amap;

        return $this;
    }

    public function isGestionnaire(): bool
    {
        return $this->gestionnaire;
    }

    public function setGestionnaire(bool $gestionnaire): Amapien
    {
        $this->gestionnaire = $gestionnaire;

        return $this;
    }

    public function getNumTel1(): ?string
    {
        return $this->numTel1;
    }

    public function setNumTel1(?string $numTel1): Amapien
    {
        $this->numTel1 = $numTel1;

        return $this;
    }

    public function getNumTel2(): ?string
    {
        return $this->numTel2;
    }

    public function setNumTel2(?string $numTel2): Amapien
    {
        $this->numTel2 = $numTel2;

        return $this;
    }

    public function getLibAdr1(): ?string
    {
        return $this->libAdr1;
    }

    public function setLibAdr1(?string $libAdr1): Amapien
    {
        $this->libAdr1 = $libAdr1;

        return $this;
    }

    public function getLibAdr2(): ?string
    {
        return $this->libAdr2;
    }

    public function setLibAdr2(?string $libAdr2): Amapien
    {
        $this->libAdr2 = $libAdr2;

        return $this;
    }

    /**
     * Add anneeAdhesion.
     *
     * @return Amapien
     */
    public function addAnneeAdhesion(AmapienAnneeAdhesion $anneeAdhesion)
    {
        $this->anneeAdhesions[] = $anneeAdhesion;

        return $this;
    }

    /**
     * Remove anneeAdhesion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAnneeAdhesion(AmapienAnneeAdhesion $anneeAdhesion)
    {
        return $this->anneeAdhesions->removeElement($anneeAdhesion);
    }

    /**
     * Get anneeAdhesions.
     *
     * @return Collection
     */
    public function getAnneeAdhesions()
    {
        return $this->anneeAdhesions;
    }

    /**
     * @return AmapienAnneeAdhesion[]
     */
    public function getAnneeAdhesionsOrdered()
    {
        $adhesions = $this->anneeAdhesions->toArray();
        usort($adhesions, function (AmapienAnneeAdhesion $a, AmapienAnneeAdhesion $b) {
            return $b->getAnnee() - $a->getAnnee();
        });

        return $adhesions;
    }

    public function getNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(?bool $newsletter): Amapien
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): Amapien
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Add fermeAttente.
     *
     * @return Amapien
     */
    public function addFermeAttente(Ferme $fermeAttente)
    {
        $this->fermeAttentes[] = $fermeAttente;

        return $this;
    }

    /**
     * Remove fermeAttente.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeFermeAttente(Ferme $fermeAttente)
    {
        return $this->fermeAttentes->removeElement($fermeAttente);
    }

    /**
     * Get fermeAttentes.
     *
     * @return Collection
     */
    public function getFermeAttentes()
    {
        return $this->fermeAttentes;
    }

    public function getAmapAsRefReseau(): ?Amap
    {
        return $this->amapAsRefReseau;
    }

    public function setAmapAsRefReseau(?Amap $amapAsRefReseau): Amapien
    {
        $this->amapAsRefReseau = $amapAsRefReseau;

        return $this;
    }

    public function getAmapAsRefReseauSec(): ?Amap
    {
        return $this->amapAsRefReseauSec;
    }

    public function setAmapAsRefReseauSec(?Amap $amapAsRefReseauSec): Amapien
    {
        $this->amapAsRefReseauSec = $amapAsRefReseauSec;

        return $this;
    }

    public function isEmailInvalid(): bool
    {
        return $this->emailInvalid;
    }

    public function setEmailInvalid(bool $emailInvalid): Amapien
    {
        $this->emailInvalid = $emailInvalid;

        return $this;
    }

    /**
     * Add fermeAttente.
     *
     * @return Amapien
     */
    public function addContrat(Contrat $contrat)
    {
        $this->contrats[] = $contrat;

        return $this;
    }

    /**
     * Remove fermeAttente.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeContrat(Contrat $contrat)
    {
        return $this->contrats->removeElement($contrat);
    }

    /**
     * Get contrats.
     *
     * @return Collection
     */
    public function getContrats()
    {
        return $this->contrats;
    }

    /**
     * Set aMdpreinitToken.
     *
     * @param null|string $aMdpreinitToken
     *
     * @return Amapien
     */
    public function setAMdpreinitToken($aMdpreinitToken = null)
    {
        $this->aMdpreinitToken = $aMdpreinitToken;

        return $this;
    }

    /**
     * Get aMdpreinitToken.
     *
     * @return null|string
     */
    public function getAMdpreinitToken()
    {
        return $this->aMdpreinitToken;
    }

    /**
     * Set aMdpreinitCreationdate.
     *
     * @param null|DateTime $aMdpreinitCreationdate
     *
     * @return Amapien
     */
    public function setAMdpreinitCreationdate($aMdpreinitCreationdate = null)
    {
        $this->aMdpreinitCreationdate = $aMdpreinitCreationdate;

        return $this;
    }

    /**
     * Get aMdpreinitCreationdate.
     *
     * @return null|DateTime
     */
    public function getAMdpreinitCreationdate()
    {
        return $this->aMdpreinitCreationdate;
    }

    /**
     * @return null|string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get emailInvalid.
     *
     * @return bool
     */
    public function getEmailInvalid()
    {
        return $this->emailInvalid;
    }

    /**
     * Get gestionnaire.
     *
     * @return bool
     */
    public function getGestionnaire()
    {
        return $this->gestionnaire;
    }

    /**
     * Get superAdmin.
     *
     * @return bool
     */
    public function getSuperAdmin()
    {
        return $this->superAdmin;
    }

    /**
     * Add adminReseaux.
     *
     * @return Amapien
     */
    public function addAdminReseaux(Reseau $adminReseaux)
    {
        $this->adminReseaux[] = $adminReseaux;

        return $this;
    }

    /**
     * Remove adminReseaux.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdminReseaux(Reseau $adminReseaux)
    {
        return $this->adminReseaux->removeElement($adminReseaux);
    }

    public function getAdhesionInfo(): AmapienAdhesionInfo
    {
        return $this->adhesionInfo;
    }

    public function setAdhesionInfo(AmapienAdhesionInfo $adhesionInfo): Amapien
    {
        $this->adhesionInfo = $adhesionInfo;

        return $this;
    }

    public function getLibAdr1Complement(): ?string
    {
        return $this->libAdr1Complement;
    }

    public function setLibAdr1Complement(?string $libAdr1Complement): Amapien
    {
        $this->libAdr1Complement = $libAdr1Complement;

        return $this;
    }
}
