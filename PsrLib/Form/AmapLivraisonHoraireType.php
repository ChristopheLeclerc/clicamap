<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\AmapLivraisonHoraire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapLivraisonHoraireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('saison', ChoiceType::class, [
                'choices' => [
                    'Été' => AmapLivraisonHoraire::SAISON_ETE,
                    'Hiver' => AmapLivraisonHoraire::SAISON_HIVER,
                ],
            ])
            ->add('jour', ChoiceType::class, [
                'choices' => [
                    'Lundi' => AmapLivraisonHoraire::JOUR_LUNDI,
                    'Mardi' => AmapLivraisonHoraire::JOUR_MARDI,
                    'Mercredi' => AmapLivraisonHoraire::JOUR_MERCREDI,
                    'Jeudi' => AmapLivraisonHoraire::JOUR_JEUDI,
                    'Vendredi' => AmapLivraisonHoraire::JOUR_VENDREDI,
                    'Samedi' => AmapLivraisonHoraire::JOUR_SAMEDI,
                    'Dimanche' => AmapLivraisonHoraire::JOUR_DIMANCHE,
                ],
            ])
            ->add('heureDebut', TextType::class, [
                'attr' => ['placeholder' => 'ex: 16:00'],
            ])
            ->add('heureFin', TextType::class, [
                'attr' => ['placeholder' => 'ex: 18:00'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AmapLivraisonHoraire::class,
        ]);
    }
}
