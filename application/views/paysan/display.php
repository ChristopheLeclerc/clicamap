<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Paysan $paysan */
$currentUser = get_current_connected_user();
$ferme = $paysan->getFerme();

if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
    echo '<h3>Mon profil</h3>';
} else {
    echo '<h3>Détails du profil</h3>';
    echo '<h5><a href="'.site_url('paysan').'">Gestion des paysans</a> <i class="glyphicon glyphicon-menu-right"></i> '.mb_strtoupper($paysan->getNom()).' '.ucfirst($paysan->getPrenom());
}

// COMMENTAIRE DATÉ
if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_COMMENT, $paysan)) {
    echo '<a href="#"><i class="glyphicon glyphicon-pencil pull-right" data-toggle="modal" data-target="#modal_commentaire" title="Écrire un commentaire daté" style="font-size:18px;"></i></a>&nbsp;';

    echo '<p class="clearfix">&nbsp;</p>'; ?>
    <div class="modal fade" id="modal_commentaire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Écrire un commentaire daté.</h4>
                </div>
                <div class="modal-body">
                    <?= '<form method="POST" action="'.site_url('paysan/paysan_commentaire_add/'.$paysan->getId()).'">'; ?>
                    <div class="form-group clearfix">
                        <textarea class="form-control" rows="8" name="paysan_commentaire"></textarea>
                        <input type="submit" value="Valider" class="pull-right btn btn-success clearfix"
                               style="margin:5px;"/>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php

    /** @var \PsrLib\ORM\Entity\PaysanCommentaire $com */
    foreach ($paysan->getCommentaires() as $com) {
        echo '<div class="alert alert-warning clearfix">';
        echo '<a title="Supprimer le commentaire" href="'.site_url('paysan/paysan_commentaire_remove/'.$com->getId()).'" ><i class="glyphicon glyphicon-remove pull-right" data-toggle="tooltip" title="Supprimer le commentaire"></i></a>&nbsp;';
        echo '<h5><strong>Le '.$com->getPayComDate()->format('d/m/Y').'</strong></h5>';
        echo '<p style="line-height:24px;">'.nl2br($com->getCommentaire()).'</p>';
        echo '</div>';
    }
}

echo '</h5>';

    echo '<p class="text-right">';
    if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan)) {
        if (\PsrLib\ORM\Entity\Paysan::ETAT_ACTIF === $paysan->getEtat()) {
            $icon = 'pause';
            $texte = 'Désactiver le profil';
        } else {
            $icon = 'play';
            $texte = 'Activer le profil';
        }
        echo '<a href="'.site_url('paysan/de_activate_paysan/'.$paysan->getId().'/'.$paysan->getEtat()).'" class="btn btn-primary"><span class="glyphicon glyphicon-'.$icon.'">&nbsp;</span>'.$texte.'</a>&nbsp;';
    }

    echo '<a href="'.site_url('paysan/informations_generales/'.$paysan->getId()).'" class="btn btn-success"><span class="glyphicon glyphicon-edit">&nbsp;</span>Éditer le profil</a>&nbsp;';
    echo '</p>';

echo '<div class="table-responsive well">';
echo '<h3 style="font-size:20px;">'.$paysan->GetNom().' '.$paysan->GetPrenom().'</h3>';
echo '<table class="table">';

echo '<tbody>';
echo '<tr>';

echo '<th>État</th>';

if (\PsrLib\ORM\Entity\Paysan::ETAT_ACTIF === $paysan->getEtat()) {
    echo '<td>Actif</td>';
} else {
    echo '<td class="mark">Inactif (la connexion sur '.SITE_NAME.' n\'est plus possible)</td>';
}

if ($paysan->getConjointPrenom() && $paysan->getConjointNom()) {
    echo '<tr>';
    echo '<th>Conjoint</th><td>'.strtoupper($paysan->getConjointNom()).' '.ucfirst($paysan->getConjointPrenom()).'</td>';
    echo '</tr>';
}

echo '<tr>';
echo '<th>Email</th><td><a href="mailto:'.$paysan->getEmail().'">'.$paysan->getEmail().'</a></td>';
echo '</tr>';

if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_USER_CHANGE_PASSWORD, $paysan->getId())) {
    echo '<tr>';
    echo '<th>Mot de passe</th>';
    echo '<td><a href="'.site_url('portail/password/'.$paysan->getId().'/pay').'">Changer mon Mot de passe</a></td>';
    echo '</td>';
}

if (null !== $ferme) {
    echo '<tr>';
    echo '<th>Ferme</th>';
    echo '<td><a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.$ferme->getNom().'</a></td>';
    echo '</tr>';
}

if ($paysan->getNumTel1()) {
    echo '<tr>';
    echo '<th>Numéro de téléphone n°1</th>';
    echo '<td>'.$paysan->getNumTel1().'</td>';
    echo '</tr>';
}

if ($paysan->getNumTel2()) {
    echo '<tr>';
    echo '<th>Numéro de téléphone n°2</th>';
    echo '<td>'.$paysan->getNumTel2().'</td>';
    echo '</tr>';
}

if ($paysan->getLibAdr() && $paysan->getVille()) {
    echo '<tr>';
    echo '<th>Adresse</th>';
    echo '<td>'.ucfirst($paysan->getLibAdr()).', '.$paysan->getVille()->getCpString().' '.strtoupper($paysan->getVille()->getNom()).'</td>';
    echo '</tr>';
}

if (null !== $ferme && !$ferme->getAnneeAdhesions()->isEmpty()) {
    echo '<tr>';
    echo '<th>Année(s) d\'adhésion</th>';

    echo '<td>';

    echo implode('; ', $ferme->getAnneeAdhesions()->toArray());

    echo '</td>';
    echo '</tr>';
}

if ($paysan->getSpg()) {
    echo '<tr>';
    echo '<th>Visite de partenariat</th>';
    echo '<td>'.$paysan->getSpg().'</td>';
    echo '</tr>';
}

if ($paysan->getStatut()) {
    if (\PsrLib\ORM\Entity\Paysan::STATUT_EI === $paysan->getStatut()) {
        $statut = 'Entreprise individuelle';
    } else {
        $statut = 'Groupement';
    }

    echo '<tr>';
    echo '<th>Statut</th>';
    echo '<td>'.$statut.'</td>';
    echo '</tr>';
}

if ($paysan->getMsa()) {
    echo '<tr>';
    echo '<th>Numéro MSA</th>';
    echo '<td>'.$paysan->getMsa().'</td>';
    echo '</tr>';
}

if ($paysan->getAnneeDebCommercialisationAmap()) {
    echo '<tr>';
    echo '<th>Année de début de commercialisation en AMAP</th>';
    echo '<td>'.$paysan->getAnneeDebCommercialisationAmap().'</td>';
    echo '</tr>';
}

if ($paysan->getDateInstallation()) {
    echo '<tr>';
    echo '<th>Date d\'installation</th>';
    echo '<td>'.date_format_french($paysan->getDateInstallation()).'</td>';
    echo '</tr>';
}

echo '<tr>';
echo '<th>Synchronisation de la lettre d\'information</th>';
if (!$paysan->isNewsletter()) {
    echo '<td>Non</td>';
} else {
    echo '<td>Oui</td>';
}
echo '</tr>';

$paysan_cr = $paysan->getCadreReseau();
if (null !== $paysan_cr && is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_CHANGE_CR, $paysan)) {
    echo '<tr>';
    echo '<th>Groupe de travail ?</th>';
    if (!$paysan_cr->isPayCrGrpeW()) {
        echo '<td>Non</td>';
    } else {
        echo '<td>Oui</td>';
    }
    echo '</tr>';

    echo '<tr>';
    echo '<th>CA ?</th>';
    if (!$paysan_cr->isPayCrCa()) {
        echo '<td>Non</td>';
    } else {
        echo '<td>Oui</td>';
    }
    echo '</tr>';

    echo '<tr>';
    echo '<th>Visite démarrage ?</th>';
    if (!$paysan_cr->isPayCrVisiteDem()) {
        echo '<td>Non</td>';
    } else {
        echo '<td>Oui</td>';
    }
    echo '</tr>';

    echo '<tr>';
    echo '<th>Fiche locale d’expérience ?</th>';
    if (!$paysan_cr->isPayCrFicheLocExp()) {
        echo '<td>Non</td>';
    } else {
        echo '<td>Oui</td>';
    }
    echo '</tr>';

    echo '<tr>';
    echo '<th>Cagnotte solidaire ?</th>';
    if (!$paysan_cr->isPayCrCagnSol()) {
        echo '<td>Non</td>';
    } else {
        echo '<td>Oui</td>';
    }
    echo '</tr>';
} ?>

    </tbody>
    </table>
    </div>

<?php

if (null !== $ferme) {
    $referent_produit = $ferme->getAmapienRefs();
    if (!$referent_produit->isEmpty()) {
        ?>

        <div class="table-responsive well">

            <?php
            if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan || ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin())) {
                ?>
                <p><a href="<?= site_url('paysan/telecharger_ferme_referents_produit/'.$ferme->getId()); ?>"
                      class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-download"></i> Télécharger la liste</a></p>
                <?php
            } ?>

            <h3 style="font-size:20px;">Référents produits</h3>
            <table class="table table-hover">
                <thead class="thead-inverse">
                <tr>
                    <th>AMAP</th>
                    <th>Référent produit</th>
                    <th>Email</th>
                    <th>Téléphone</th>
                </tr>
                </thead>

                <?php

                echo '<tbody>';
        /** @var \PsrLib\ORM\Entity\Amapien $ref_prod */
        foreach ($referent_produit as $ref_prod) {
            echo '<tr>';

            echo '<th scope="row">';
            echo ucfirst(strtolower($ref_prod->getAmap()->getNom()));
            echo '</th>';

            echo '<td>';
            if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin()) {
                echo '<a href="'.site_url('amapien/display/'.$ref_prod->getId()).'" target="_blank">';
            }
            echo ucfirst(strtolower($ref_prod->getNom())).' '.ucfirst(strtolower($ref_prod->getPrenom()));
            if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin()) {
                echo '</a>';
            }
            echo '</td>';

            echo '<td>';
            echo '<a href="mailto:'.$ref_prod->getEmail().'">'.strtolower($ref_prod->getEmail()).'</a>';
            echo '</td>';

            echo '<td>';
            if ($ref_prod->getNumTel1() && $ref_prod->getNumTel2()) {
                echo $ref_prod->getNumTel1().' | '.$ref_prod->getNumTel2();
            } else {
                if ($ref_prod->getNumTel1()) {
                    echo $ref_prod->getNumTel1();
                }
                if ($ref_prod->getNumTel2()) {
                    echo $ref_prod->getNumTel2();
                }
            }
            echo '</td>';

            echo '</tr>';
        } ?>

                </tbody>
            </table>
        </div>

        <?php
    }
}
