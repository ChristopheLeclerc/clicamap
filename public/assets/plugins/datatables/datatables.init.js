$(document).ready(function () {
  // Add date column sorting
  $.fn.dataTable.moment( 'DD/MM/YYYY' );

  $('.table-datatable').DataTable({
    searching: false,
    lengthChange: true,
    pagingType: 'numbers',
    "language": {
      "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
    },
    "pageLength": 30,
    "lengthMenu": [[20, 30, 50, 100, 250, -1], [20, 30, 50, 100, 250, "Tous"]],
  });

});
