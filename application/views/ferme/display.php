<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
$currentUser = get_current_connected_user();
?>

<?php
if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
    echo '<h3>Détails de la ferme</h3>';

    echo '<p class="text-right">';
    echo '<a href="'.site_url('ferme/informations_generales/'.$ferme->getId()).'" class="btn btn-success"><span class="glyphicon glyphicon-edit">&nbsp;</span>Éditer la ferme</a>&nbsp;';
    echo '</p">';

    echo '<h5><a href="'.site_url('paysan/display/'.$currentUser->getId()).'">Profil</a> <i class="glyphicon glyphicon-menu-right"></i> '.$ferme->getNom().'</h5>';
} else {
    echo '<h3>Détails de la ferme</h3>';
    echo '<h5><a href="'.site_url('ferme').'">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> '.$ferme->getNom().'</h5>';
}

?>

<div class="table-responsive well">
    <h3 style="font-size:20px;"><?= $ferme->GetNom(); ?></h3>
    <table class="table">

        <tbody>

        <?php

        if ($ferme->getSiret()) {
            echo '<tr>';
            echo '<th>SIRET :</th>';
            echo '<td>'.$ferme->getSiret().'</td>';
        }

        if ($ferme->getDescription()) {
            echo '<tr>';
            echo '<th>Description :</th>';
            echo '<td>'.$ferme->getDescription().'</td>';
        }

        if ($ferme->getDelaiModifContrat()) {
            echo '<tr>';
            echo '<th>Délai de modification contrat :</th>';
            echo '<td>'.$ferme->getDelaiModifContrat().'</td>';
            echo '</tr>';
        }

        if ($ferme->getVille()) {
            echo '<tr>';
            echo '<th>Adresse de la ferme :</th>';
            echo '<td>'.ucfirst($ferme->getLibAdr()).', '.$ferme->getVille()->getCpString().' '.mb_strtoupper($ferme->getVille()->getNom());
            echo '</tr>';
        }

        if ($ferme->getUrl()) {
            echo '<tr>';
            echo '<th>Site web :</th>';
            echo '<td><a href="'.$ferme->getUrl().'" target="_blank">'.$ferme->getUrl().'</a>';
            echo '</tr>';
        }

        if ($ferme->getGpsLatitude() && $ferme->getGpsLongitude()) {
            // ICON CARTO
            echo '<tr>';
            echo '<th>Coordonnées GPS :</th>';
            echo '<td><a href="'.site_url('map/ferme_display/'.$ferme->getId()).'"><i class="glyphicon glyphicon-map-marker"></i>&nbsp;Visualiser la ferme sur une carte</a>&nbsp;';
            echo '</td></tr>';

            // echo '<tr>';
            // echo '<th>Coordonnés GPS :</th>';
            // echo '<td><strong>Latitude</strong> : '.$ferme->f_gps_latitude.'&nbsp;';
            // echo '<strong>Longitude</strong> : '.$ferme->f_gps_longitude;
            // echo '</tr>';
        }

        echo '<tr>';
        echo '<th>Surface de la ferme (ha) :</th>';
        echo '<td>'.$ferme->getSurface().'</td>';
        echo '</tr>';

        echo '<tr>';
        echo '<th>Nombre d\'UTA :</th>';
        echo '<td>'.$ferme->getUta().'</td>';
        echo '</tr>';

        echo '<tr>';
        echo '<th>Année(s) d\'adhésion :</th>';
        echo '<td>'.implode(';', $ferme->getAnneeAdhesionsOrdered()).'</td>';
        echo '</tr>';

        $paysan = $ferme->getPaysans();
        if (!$paysan->isEmpty()) {
            echo '<tr>';
            echo '<th>Paysans :</th>';
            echo '<td>';
            echo implode(' ; ', $paysan->toArray());
            echo '</td>';
            echo '</tr>';
        }

        $refProds = $ferme->getAmapienRefs();
        if (!$refProds->isEmpty()) {
            echo '<tr>';
            echo '<th>Référents produits :</th>';
            echo '<td>';
            $virgule = 0;

            /** @var \PsrLib\ORM\Entity\Amapien $refProd */
            foreach ($refProds as $refProd) {
                if (0 != $virgule) {
                    echo ' <br/> ';
                }

                echo $refProd->getPrenom().' '.strtoupper($refProd->getNom());
                echo ' (';
                echo $refProd->getAmap()->getNom();
                echo ') ';

                ++$virgule;
            }

            echo '</td>';
            echo '</tr>';
        }

        ?>

        </tbody>
    </table>
</div>
