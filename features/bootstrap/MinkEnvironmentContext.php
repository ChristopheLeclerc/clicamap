<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Behat\Behat\Hook\Scope\AfterScenarioScope;

class MinkEnvironmentContext extends \Behat\MinkExtension\Context\RawMinkContext
{
    public const ENV_FILE_OVERRIDE_PATH = __DIR__.'/../../.env.test.override';

    /**
     * @Given je définit la variable environment :variable avec la valeur :valeur
     *
     * @param string $variable
     * @param string $valeur
     */
    public function jeDefinitLaVariableEnvironement($variable, $valeur)
    {
        $fs = new Symfony\Component\Filesystem\Filesystem();
        $fs->appendToFile(
            self::ENV_FILE_OVERRIDE_PATH,
            sprintf('%s=%s'.PHP_EOL, $variable, $valeur)
        );
    }

    /**
     * @AfterScenario
     */
    public static function resetDbAfterScenario(AfterScenarioScope $scope)
    {
        if ($scope->getScenario()->hasTag('environment')) {
            $fs = new Symfony\Component\Filesystem\Filesystem();
            $fs->remove(self::ENV_FILE_OVERRIDE_PATH);
        }
    }
}
