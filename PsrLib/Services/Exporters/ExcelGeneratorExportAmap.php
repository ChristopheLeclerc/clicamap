<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PsrLib\DTO\SearchAmapState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonHoraire;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Repository\AmapienRepository;

class ExcelGeneratorExportAmap
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ?int
     */
    private $maxLivLieu;

    /**
     * @var array
     */
    private $amapRefProdMap = [];

    /**
     * @var TypeProduction[]
     */
    private $typeProductions;

    /**
     * @var array<array<AmapLivraisonLieu>>
     */
    private $amapLls;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function genererExportAmapComplet(SearchAmapState $searchAmapState): string
    {
        /** @var Amap[] $amaps */
        $amaps = $this
            ->em
            ->getRepository(Amap::class)
            ->search($searchAmapState)
        ;
        $this->initCommonVariables($amaps);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Filters
        $sheet->setCellValue('A1', 'Région '.$searchAmapState->getRegion());
        $sheet->setCellValue(
            'A2',
            sprintf('%s résultats (Extrait le %s)', count($amaps), date_format_french(new Carbon()))
        );
        $sheet->getStyle('A1:A2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        // Header
        $this->genererExportAmapCompletHeader($sheet);

        $line = 5;
        foreach ($amaps as $amap) {
            $this->genererExportAmapCompletLigne($sheet, $line, $amap);
            ++$line;
        }

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportAmapComplet_');
        $objWriter = new Xls($spreadsheet);
        $objWriter->save($outputFileName);

        return $outputFileName;
    }

    /**
     * @param Amap[] $amaps
     */
    private function initCommonVariables($amaps): void
    {
        $amapLivraisonLieuRepo = $this
            ->em
            ->getRepository(AmapLivraisonLieu::class)
        ;
        $this->maxLivLieu = 0;
        foreach ($amaps as $amap) {
            // Force loading from database as search request exclude Lls from other departement or region
            /** @var AmapLivraisonLieu[] $amapLls */
            $amapLls = $amapLivraisonLieuRepo->findBy([
                'amap' => $amap,
            ]);
            $this->amapLls[$amap->getId()] = $amapLls;
            $nbLL = count($amapLls);
            if ($nbLL > $this->maxLivLieu) {
                $this->maxLivLieu = $nbLL;
            }
        }

        $this->typeProductions = $this
            ->em
            ->getRepository(TypeProduction::class)
            ->findAllNoParent()
        ;

        /** @var AmapienRepository $amapienRepo */
        $amapienRepo = $this
            ->em
            ->getRepository(Amapien::class)
        ;
        foreach ($amaps as $amap) {
            $this->amapRefProdMap[$amap->getId()] = [];
            foreach ($this->typeProductions as $typeProduction) {
                $this->amapRefProdMap[$amap->getId()][$typeProduction->getId()] = [
                    'amapiens' => [],
                    'paysans' => [],
                ];
            }
            $refProds = $amapienRepo->getRefProduitFromAmap($amap);
            foreach ($refProds as $refProd) {
                /** @var Ferme $ferme */
                foreach ($refProd->getRefProdFermes() as $ferme) {
                    foreach ($ferme->getTypeProductionFromProduits() as $tp) {
                        $this->amapRefProdMap[$amap->getId()][$tp->getId()]['amapiens'][] = $refProd;
                        $this->amapRefProdMap[$amap->getId()][$tp->getId()]['paysans'] = array_merge(
                            $this->amapRefProdMap[$amap->getId()][$tp->getId()]['paysans'],
                            $ferme->getPaysans()->toArray()
                        );
                    }
                }
            }
        }
    }

    private function genererExportAmapCompletHeader(Worksheet $sheet): void
    {
        $borderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'alignment' => [
                'wrapText' => true,
            ],
        ];
        $borderStyleCentered = array_merge_recursive($borderStyle, [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ]);
        $headerMap = [
            'ID',
            'Nom de l\'AMAP',
            'Nombre d\'adhérents',
            'Email public',
            'Site internet',
            'Année de création',
            'Nom & Prénom du correspondant  réseau',
            'adresse mail du correspondant réseau',
            'Numéro de téléhone du correspondant réseau',
            'Adresse postale du correspondant réseau',
            'CP de l\'adresse du correspondant réseau',
            'Ville de l\'adresse du correspondant réseau',
            'Nom & Prénom du correspondant  clic\'AMAP',
            'adresse mail du correspondant clic\'AMAP',
        ];

        for ($i = 0; $i < $this->maxLivLieu; ++$i) {
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $headerMap = array_merge($headerMap, [
                'Nom du lieu de livraison',
                'Adresse de livraison',
                'CP de livraison',
                'Ville de livraison',
                'Jours de livraison',
                'Période de livraison',
                'Heure de début',
                'Heure de fin',
            ]);
        }

        $sheet
            ->getCellByColumnAndRow(count($headerMap) + 1, 3)
            ->setValue('Produits proposés')
        ;
        $sheet
            ->getStyleByColumnAndRow(count($headerMap) + 1, 3)
            ->applyFromArray($borderStyleCentered)
        ;
        $sheet->mergeCellsByColumnAndRow(
            count($headerMap) + 1,
            3,
            count($headerMap) + count($this->typeProductions),
            3
        );
        foreach ($this->typeProductions as $typeProduction) {
            $headerMap[] = $typeProduction->getNomComplet();
        }
        $sheet
            ->getCellByColumnAndRow(count($headerMap) + 1, 3)
            ->setValue('Produits recherchés')
        ;
        $sheet->mergeCellsByColumnAndRow(
            count($headerMap) + 1,
            3,
            count($headerMap) + count($this->typeProductions),
            3
        );
        $sheet
            ->getStyleByColumnAndRow(count($headerMap) + 1, 3)
            ->applyFromArray($borderStyleCentered)
        ;
        foreach ($this->typeProductions as $typeProduction) {
            $headerMap[] = $typeProduction->getNomComplet();
        }

        $sheet
            ->getCellByColumnAndRow(count($headerMap) + 1, 3)
            ->setValue('Adresse administrative de l\'Amap')
        ;
        $sheet
            ->getStyleByColumnAndRow(count($headerMap) + 1, 3)
            ->applyFromArray($borderStyleCentered)
        ;
        $sheet->mergeCellsByColumnAndRow(
            count($headerMap) + 1,
            3,
            count($headerMap) + 4,
            3
        );
        // Workaround border left
        $sheet
            ->getStyleByColumnAndRow(count($headerMap) + 5, 3)
            ->getBorders()
            ->getLeft()
            ->setBorderStyle(Border::BORDER_THIN)
        ;

        $headerMap = array_merge($headerMap, [
            'Nom du lieu de l\'adresse administrative',
            'adresse administrative',
            'CP de l\'adresse administrative',
            'ville de l\'adresse administrative',
            'années d\'adhésion',
            'Type d\'asso (de fait ?)',
            'assurance réseau',
            'AMAP étudiante',
        ]);

        $sheet
            ->getCellByColumnAndRow(count($headerMap) + 1, 3)
            ->setValue('Production livrée par l\'AMAP')
        ;
        $sheet
            ->getStyleByColumnAndRow(count($headerMap) + 1, 3)
            ->applyFromArray($borderStyleCentered)
        ;
        $sheet->mergeCellsByColumnAndRow(
            count($headerMap) + 1,
            3,
            count($headerMap) + count($this->typeProductions) * 4,
            3
        );
        foreach ($this->typeProductions as $typeProduction) {
            $headerMap[] = $typeProduction->getNomComplet().'+ nom & prénom du/des Référents';
            $headerMap[] = $typeProduction->getNomComplet().'+ adresse mail du/des Référents';
            $headerMap[] = $typeProduction->getNomComplet().'+ nom & prénom du/des paysans';
            $headerMap[] = $typeProduction->getNomComplet().'+ adresse mail du/des paysans';
        }
        // Workaround border left
        $sheet
            ->getStyleByColumnAndRow(count($headerMap) + 1, 3)
            ->getBorders()
            ->getLeft()
            ->setBorderStyle(Border::BORDER_THIN)
        ;

        $column = 1;
        foreach ($headerMap as $header) {
            $sheet
                ->getCellByColumnAndRow($column, 4, true)
                ->setValue($header)
            ;
            $sheet
                ->getStyleByColumnAndRow($column, 4)
                ->applyFromArray($borderStyle)
            ;
            ++$column;
        }
        $sheet->getRowDimension(4, true)->setRowHeight(100);
    }

    private function genererExportAmapCompletLigne(Worksheet $sheet, int $line, Amap $amap): void
    {
        $valueMap = [
            $amap->getId(),
            $amap->getNom(),
            $amap->getNbAdherents(),
            $amap->getEmail(),
            $amap->getUrl(),
            $amap->getAnneeCreation(),
        ];
        $refReseau = $amap->getAmapienRefReseau();
        if (null === $refReseau) {
            $valueMap[] = '';
            $valueMap[] = '';
            $valueMap[] = '';
            $valueMap[] = '';
            $valueMap[] = '';
            $valueMap[] = '';
        } else {
            $valueMap[] = $refReseau->getNomCompletInverse();
            $valueMap[] = $refReseau->getEmail();
            $valueMap[] = $refReseau->getNumTel1();
            $valueMap[] = $refReseau->getLibAdr1().' '.$refReseau->getLibAdr2();
            $valueMap[] = null === $refReseau->getVille() ? '' : $refReseau->getVille()->getCpString();
            $valueMap[] = null === $refReseau->getVille() ? '' : $refReseau->getVille()->getNom();
        }

        /** @var Amapien[] $gestionaires */
        $gestionaires = $amap
            ->getAmapiens()
            ->matching(AmapienRepository::criteriaGestionaire())
            ->toArray()
        ;
        $valueMap[] = implode(';', array_map(function (Amapien $amapien) {
            return $amapien->getNomCompletInverse();
        }, $gestionaires));

        $valueMap[] = implode(';', array_map(function (Amapien $amapien) {
            return $amapien->getEmail();
        }, $gestionaires));

        $livLieux = $this->amapLls[$amap->getId()];
        for ($i = 0; $i < $this->maxLivLieu; ++$i) {
            $ll = $livLieux[$i] ?? null;
            if (null === $ll) {
                /** @noinspection SlowArrayOperationsInLoopInspection */
                $valueMap = array_merge($valueMap, [
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                ]);
            } else {
                /** @var AmapLivraisonHoraire[] $horaires */
                $horaires = $ll->getLivraisonHoraires()->toArray();
                $jours = implode(';', array_map(function (AmapLivraisonHoraire $horaire) {
                    return $horaire->getJour();
                }, $horaires));
                $saisons = implode(';', array_map(function (AmapLivraisonHoraire $horaire) {
                    return $horaire->getSaison();
                }, $horaires));
                $heureDebut = implode(';', array_map(function (AmapLivraisonHoraire $horaire) {
                    return $horaire->getHeureDebut();
                }, $horaires));
                $heureFin = implode(';', array_map(function (AmapLivraisonHoraire $horaire) {
                    return $horaire->getHeureFin();
                }, $horaires));
                /** @noinspection SlowArrayOperationsInLoopInspection */
                $valueMap = array_merge($valueMap, [
                    $ll->getNom(),
                    $ll->getAdresse(),
                    $ll->getVille()->getCpString(),
                    $ll->getVille()->getNom(),
                    $jours,
                    $saisons,
                    $heureDebut,
                    $heureFin,
                ]);
            }
        }

        foreach ($this->typeProductions as $typeProduction) {
            if ($amap->getTpPropose()->contains($typeProduction)) {
                $valueMap[] = 'O';
            } else {
                $valueMap[] = '';
            }
        }
        foreach ($this->typeProductions as $typeProduction) {
            if ($amap->getTpRecherche()->contains($typeProduction)) {
                $valueMap[] = 'O';
            } else {
                $valueMap[] = '';
            }
        }

        $valueMap[] = $amap->getAdresseAdminNom();
        $valueMap[] = $amap->getAdresseAdmin();
        $valueMap[] = null === $amap->getAdresseAdminVille() ? '' : $amap->getAdresseAdminVille()->getCpString();
        $valueMap[] = null === $amap->getAdresseAdminVille() ? '' : $amap->getAdresseAdminVille()->getNom();
        $valueMap[] = implode(';', $amap->getAnneeAdhesionsOrdered());
        $valueMap[] = $amap->getAssociationDeFait() ? 'De fait' : 'Déclarée';
        $valueMap[] = $amap->isPossedeAssurance() ? 'O' : '';
        $valueMap[] = $amap->getAmapEtudiante() ? 'O' : '';

        foreach ($this->typeProductions as $typeProduction) {
            $valueMap[] = implode(';', array_map(function (Amapien $amapien) {
                return $amapien->getNomCompletInverse();
            }, array_unique($this->amapRefProdMap[$amap->getId()][$typeProduction->getId()]['amapiens'])));
            $valueMap[] = implode(';', array_map(function (Amapien $amapien) {
                return $amapien->getEmail();
            }, array_unique($this->amapRefProdMap[$amap->getId()][$typeProduction->getId()]['amapiens'])));
            $valueMap[] = implode(';', array_map(function (Paysan $paysan) {
                return $paysan->getNomCompletInverse();
            }, array_unique($this->amapRefProdMap[$amap->getId()][$typeProduction->getId()]['paysans'])));
            $valueMap[] = implode(';', array_map(function (Paysan $paysan) {
                return $paysan->getEmail();
            }, array_unique($this->amapRefProdMap[$amap->getId()][$typeProduction->getId()]['paysans'])));
        }

        $column = 1;
        foreach ($valueMap as $value) {
            $sheet
                ->getCellByColumnAndRow($column, $line, true)
                ->setValue($value)
            ;
            ++$column;
        }
    }
}
