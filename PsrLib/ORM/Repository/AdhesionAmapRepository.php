<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;

class AdhesionAmapRepository extends AdhesionRepository
{
    /**
     * @return AdhesionAmap
     */
    public function search(
        Region $region,
        ?Departement $departement,
        ?int $selectedYear,
        ?bool $selectedIsStudent,
        string $selectedSearchKeyword,
        ?Amapien $creator
    ) {
        $qb = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.value', 'value')
            ->leftJoin('aa.amap', 'amap')
            ->leftJoin('amap.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'v')
            ->leftJoin('v.departement', 'd')
            ->leftJoin('d.region', 'r')
            ->where('r = :region')
            ->setParameter('region', $region)
        ;

        if (null !== $departement) {
            $qb
                ->andWhere('d = :department')
                ->setParameter('department', $departement)
            ;
        }

        if (null !== $selectedYear && $selectedYear > 0) {
            $qb
                ->andWhere('value.year = :year')
                ->setParameter('year', $selectedYear)
            ;
        }

        if (null !== $selectedIsStudent) {
            $qb
                ->andWhere('amap.amapEtudiante = :student')
                ->setParameter('student', $selectedIsStudent)
            ;
        }

        if ('' !== $selectedSearchKeyword) {
            $qb
                ->andWhere('amap.nom LIKE :keyWord')
                ->setParameter('keyWord', '%'.$selectedSearchKeyword.'%')
            ;
        }

        if (null !== $creator) {
            $qb
                ->andWhere('aa.creator = :creator')
                ->setParameter('creator', $creator)
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
