# language: fr
Fonctionnalité: Comme utilisateur, je devrais pouvoir accéder à la gestion des documents

  Scénario: : Accès par le menu comme admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand je suis "Communication"
    Et je suis "Gestion des documents"
    Alors je devrais voir "Gestion des documents"
    Et je devrais être sur "/documentation/list_documents"

  Plan du Scénario: Pas d'accès menu pour les autres utilisateurs
    Etant donné que je suis authentifié comme "<type>" avec le mail "<mail>"
    Quand je suis sur "/"
    Alors je ne devrais pas voir "Communication"
    Exemples:
      | type    | mail                      |
      | paysan  | paysan@test.amap-aura.org |
      | amapien | amap@test.amap-aura.org   |
