<?php

namespace PsrLib\Form\Type;

use PsrLib\DTO\Time;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeType extends AbstractType
{
    public const REGEX = '~^(\d{1,2}):(\d{1,2})$~';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addModelTransformer(new CallbackTransformer(
                function (?Time $time) {
                    return $time;
                },
                function (?string $input) {
                    if (null === $input) {
                        return null;
                    }

                    $matches = [];
                    if (1 !== preg_match(self::REGEX, $input, $matches)) {
                        throw new TransformationFailedException(
                            '',
                            0,
                            null,
                            'Merci d\'utiliser le format HH:MM'
                        );
                    }

                    return new Time((int) $matches[1], (int) $matches[2]);
                }
            ))
        ;
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Time::class,
            'empty_data' => '',
            'required' => true,
            'attr' => ['placeholder' => '00:00'],
        ]);
    }
}
