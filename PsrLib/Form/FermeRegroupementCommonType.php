<?php

namespace PsrLib\Form;

use PsrLib\Form\Type\VilleType;
use PsrLib\ORM\Entity\FermeRegroupement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FermeRegroupementCommonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => false,
            ])
            ->add('siret', TextType::class, [
                'label' => 'SIRET',
                'required' => false,
            ])
            ->add('adresseAdmin', TextType::class, [
                'label' => 'Adresse administrative',
                'required' => false,
            ])
            ->add('ville', VilleType::class, [
                'label' => ' Code Postal, Ville administrative (à remplir à l\'aide des suggestions) :',
                'required' => false,
                'not_null' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FermeRegroupement::class,
        ]);
    }
}
