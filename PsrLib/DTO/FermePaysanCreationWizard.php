<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;

class FermePaysanCreationWizard
{
    /**
     * @var Paysan | null
     */
    private $paysan;

    /**
     * @var Ferme | null
     */
    private $ferme;

    public function getPaysan(): ?Paysan
    {
        return $this->paysan;
    }

    public function setPaysan(?Paysan $paysan): FermePaysanCreationWizard
    {
        $this->paysan = $paysan;

        return $this;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): FermePaysanCreationWizard
    {
        $this->ferme = $ferme;

        return $this;
    }
}
