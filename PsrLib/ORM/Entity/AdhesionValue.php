<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Money\Money;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
class AdhesionValue
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("N° de reçu (année + numéro)")
     *
     * @var ?string
     * @Assert\NotBlank(message="Le numéro de reçus ne peut pas être vide.")
     * @Assert\Length(max=255)
     * @Assert\Regex("~^[a-zA-Z0-9]+$~", message="Le numéro de recu doit contenir uniquement des lettres majuscule ou minuscule et des chiffres.")
     */
    protected $voucherNumber;

    /**
     * @ORM\Column(type="money")
     * @SerializedName("Montant (€)")
     *
     * @var ?Money
     * @Assert\NotNull(message="Le montant ne peut pas être vide.")
     */
    protected $amount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @SerializedName("Année concernée")
     * @Assert\GreaterThan(0, message="L'année doit etre positive")
     * @Assert\NotNull(message="L'année ne peut pas être vide.")
     *
     * @var ?integer
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("Payeur")
     *
     * @var ?string
     * @Assert\NotBlank(message="Le payeur ne peut pas être vide.")
     * @Assert\Length(max=255)
     */
    private $payer;

    /**
     * @ORM\Column(type="money", nullable=true)
     * @SerializedName("Montant adhésion (€)")
     *
     * @var ?Money
     */
    private $amountMembership;

    /**
     * @ORM\Column(type="money", nullable=true)
     * @SerializedName("Montant Don (€)")
     *
     * @var ?Money
     */
    private $amountDonation;

    /**
     * @ORM\Column(type="money", nullable=true)
     * @SerializedName("Montant Assurance (€)")
     *
     * @var ?Money
     */
    private $amountInsurance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("Moyen de paiement Virement/espèce/chèque n° banque")
     *
     * @var ?string
     * @Assert\NotBlank(message="Le moyen de paiement ne peut pas être vide.")
     * @Assert\Length(max=255)
     */
    private $paymentType;

    /**
     * @ORM\Column(type="carbon")
     * @SerializedName("Date de règlement")
     *
     * @var ?Carbon
     * @Assert\NotNull(message="La date de règlement ne peut pas être vide.")
     */
    private $paymentDate;

    /**
     * @ORM\Column(type="carbon")
     * @SerializedName("Traité le")
     *
     * @var ?Carbon
     * @Assert\NotNull(message="La date de traitement ne peut pas être vide.")
     */
    private $processingDate;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @SerializedName("Champ Libre 1")
     *
     * @var ?string
     * @Assert\Length(max=1000, maxMessage="Le champ libre 1 peut faire au maximum 1000 caractères.")
     */
    private $freeField1;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @SerializedName("Champ Libre 2")
     *
     * @var ?string
     * @Assert\Length(max=1000, maxMessage="Le champ libre 2 peut faire au maximum 1000 caractères.")
     */
    private $freeField2;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("Banque")
     * @Assert\Length(max=255, maxMessage="La banque peut faire au maximum 255 caractères.")
     */
    private $bank;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getVoucherNumber(): ?string
    {
        return $this->voucherNumber;
    }

    public function setVoucherNumber(?string $voucherNumber): self
    {
        $this->voucherNumber = $voucherNumber;

        return $this;
    }

    public function getPayer(): ?string
    {
        return $this->payer;
    }

    public function setPayer(?string $payer): self
    {
        $this->payer = $payer;

        return $this;
    }

    public function getAmount(): ?Money
    {
        return $this->amount;
    }

    public function setAmount(?Money $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmountMembership(): ?Money
    {
        return $this->amountMembership;
    }

    public function setAmountMembership(?Money $amountMembership): self
    {
        $this->amountMembership = $amountMembership;

        return $this;
    }

    public function getAmountDonation(): ?Money
    {
        return $this->amountDonation;
    }

    public function setAmountDonation(?Money $amountDonation): self
    {
        $this->amountDonation = $amountDonation;

        return $this;
    }

    public function getAmountInsurance(): ?Money
    {
        return $this->amountInsurance;
    }

    public function setAmountInsurance(?Money $amountInsurance): self
    {
        $this->amountInsurance = $amountInsurance;

        return $this;
    }

    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    public function setPaymentType(?string $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getProcessingDate(): ?Carbon
    {
        return $this->processingDate;
    }

    public function setProcessingDate(?Carbon $processingDate): self
    {
        $this->processingDate = $processingDate;

        return $this;
    }

    public function getFreeField1(): ?string
    {
        return $this->freeField1;
    }

    public function setFreeField1(?string $freeField1): self
    {
        $this->freeField1 = $freeField1;

        return $this;
    }

    public function getFreeField2(): ?string
    {
        return $this->freeField2;
    }

    public function setFreeField2(?string $freeField2): self
    {
        $this->freeField2 = $freeField2;

        return $this;
    }

    public function getBank(): ?string
    {
        return $this->bank;
    }

    public function setBank(?string $bank): AdhesionValue
    {
        $this->bank = $bank;

        return $this;
    }

    public function getPaymentDate(): ?Carbon
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(?Carbon $paymentDate): AdhesionValue
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }
}
