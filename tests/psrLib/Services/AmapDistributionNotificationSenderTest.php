<?php

namespace Test\Services;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Repository\AmapDistributionRepository;
use PsrLib\Services\AmapDistributionNotificationSender;
use PsrLib\Services\Email_Sender;

/**
 * @internal
 * @coversNothing
 */
class AmapDistributionNotificationSenderTest extends TestCase
{
    /**
     * @var AmapDistributionNotificationSender
     */
    private $sender;

    /**
     * @var Prophet
     */
    private $emailSenderProphet;

    /**
     * @var Prophet
     */
    private $amapDistributionRepo;

    public function setUp()
    {
        $this->emailSenderProphet = $this->prophesize(Email_Sender::class);
        $this->amapDistributionRepo = $this->prophesize(AmapDistributionRepository::class);

        $em = $this->prophesize(EntityManagerInterface::class);
        $em->getRepository(AmapDistribution::class)->willReturn($this->amapDistributionRepo->reveal());

        $this->sender = new AmapDistributionNotificationSender(
            $this->emailSenderProphet->reveal(),
            $em->reveal()
        );
    }

    public function testAmapienRappels()
    {
        $amapien11 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amapien12 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getAmapiens()->willReturn(new ArrayCollection([$amapien11, $amapien12]));
        $distribution1->getPlacesRestantes()->willReturn(1);

        $amapien21 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amapien22 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $distribution2 = $this->prophesize(AmapDistribution::class);
        $distribution2->getAmapiens()->willReturn(new ArrayCollection([$amapien21, $amapien22]));
        $distribution2->getPlacesRestantes()->willReturn(1);

        $amapien31 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amapien32 = $this->buildAmapien(Amapien::ETAT_INACTIF);
        $distribution3 = $this->prophesize(AmapDistribution::class);
        $distribution3->getAmapiens()->willReturn(new ArrayCollection([$amapien31, $amapien32]));
        $distribution3->getPlacesRestantes()->willReturn(1);

        $amapien41 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $distribution4 = $this->prophesize(AmapDistribution::class);
        $distribution4->getAmapiens()->willReturn(new ArrayCollection([$amapien41]));
        $distribution4->getPlacesRestantes()->willReturn(0);

        $this->amapDistributionRepo->getDistributionInDaysFromNow(6)->willReturn([$distribution1])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(18)->willReturn([$distribution2, $distribution3, $distribution4])->shouldBeCalled();

        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution1, $amapien11)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution1, $amapien12)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution2, $amapien21)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution2, $amapien22)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution3, $amapien31)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution3, $amapien32)->shouldNotBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution4, $amapien41)->shouldNotBeCalled();

        $this->sender->amapienRappels();
    }

    public function testAmapRelance()
    {
        $amapien11 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amapien12 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amap1 = $this->prophesize(Amap::class);
        $amap1->getId()->willReturn(1);
        $amap1->getAmapiens()->willReturn(new ArrayCollection([$amapien11, $amapien12]));
        $ll1 = new AmapLivraisonLieu();
        $ll1->setAmap($amap1->reveal());
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getAmapiens()->willReturn(new ArrayCollection([$amapien11]));
        $distribution1->getAmapLivraisonLieu()->willReturn($ll1);
        $distribution1->getDateDebut()->willReturn(Carbon::create(2000, 01, 01, 17, 00));
        $distribution1->getPlacesRestantes()->willReturn(1);
        $distribution1Revealed = $distribution1->reveal();

        $distribution2 = $this->prophesize(AmapDistribution::class);
        $distribution2->getAmapiens()->willReturn(new ArrayCollection([]));
        $distribution2->getAmapLivraisonLieu()->willReturn($ll1);
        $distribution2->getDateDebut()->willReturn(Carbon::create(2000, 01, 01, 16, 00));
        $distribution2->getPlacesRestantes()->willReturn(1);
        $distribution2Revealed = $distribution2->reveal();

        $amapien21 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amapien22 = $this->buildAmapien(Amapien::ETAT_INACTIF);
        $amap2 = $this->prophesize(Amap::class);
        $amap2->getId()->willReturn(2);
        $amap2->getAmapiens()->willReturn(new ArrayCollection([$amapien21, $amapien22]));
        $ll2 = new AmapLivraisonLieu();
        $ll2->setAmap($amap2->reveal());
        $distribution3 = $this->prophesize(AmapDistribution::class);
        $distribution3->getAmapiens()->willReturn(new ArrayCollection([]));
        $distribution3->getAmapLivraisonLieu()->willReturn($ll2);
        $distribution3->getDateDebut()->willReturn(Carbon::create(2000, 01, 01, 18, 00));
        $distribution3->getPlacesRestantes()->willReturn(1);
        $distribution3Revealed = $distribution3->reveal();

        $distribution4 = $this->prophesize(AmapDistribution::class);
        $distribution4->getPlacesRestantes()->willReturn(0);
        $distribution4Revealed = $distribution4->reveal();

        $this->amapDistributionRepo->getDistributionInDaysFromNow(18)->willReturn([$distribution1Revealed, $distribution2Revealed, $distribution3Revealed])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(6)->willReturn([$distribution4Revealed])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(1)->willReturn([])->shouldBeCalled();

        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution2Revealed, $distribution1Revealed]),
            Argument::exact([$amapien12]),
            Argument::exact(false)
        )->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution3Revealed]),
            Argument::exact([$amapien21]),
            Argument::exact(false)
        )->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution4Revealed]),
            Argument::any(),
            Argument::any()
        )->shouldNotBeCalled();

        $this->sender->amapRelance();
    }

    public function testAmapRelanceAmapCopie()
    {
        $amapien11 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amapien12 = $this->buildAmapien(Amapien::ETAT_ACTIF);
        $amap1 = $this->prophesize(Amap::class);
        $amap1->getId()->willReturn(1);
        $amap1->getAmapiens()->willReturn(new ArrayCollection([$amapien11, $amapien12]));
        $ll1 = new AmapLivraisonLieu();
        $ll1->setAmap($amap1->reveal());
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getAmapiens()->willReturn(new ArrayCollection([]));
        $distribution1->getAmapLivraisonLieu()->willReturn($ll1);
        $distribution1->getDateDebut()->willReturn(Carbon::create(2000, 01, 01, 17, 00));
        $distribution1->getPlacesRestantes()->willReturn(1);
        $distribution1Revealed = $distribution1->reveal();

        $this->amapDistributionRepo->getDistributionInDaysFromNow(18)->willReturn([])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(6)->willReturn([])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(1)->willReturn([$distribution1Revealed])->shouldBeCalled();

        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution1Revealed]),
            Argument::exact([$amapien11, $amapien12]),
            Argument::exact(true)
        )->shouldBeCalled();

        $this->sender->amapRelance();
    }

    public function testAmapAlerteFin()
    {
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getDate()->willReturn(new Carbon('2000-05-31'));
        $distribution2 = $this->prophesize(AmapDistribution::class);
        $distribution2->getDate()->willReturn(new Carbon('2000-06-30'));
        $distribution3 = $this->prophesize(AmapDistribution::class);
        $distribution3->getDate()->willReturn(new Carbon('2000-08-01'));

        $this->amapDistributionRepo->getLastDistributionByAmap()->wilLReturn([$distribution1, $distribution2, $distribution3]);
        $this->emailSenderProphet->envoyerDistributionAmapAlerteFin($distribution1)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapAlerteFin($distribution2)->shouldBeCalled();

        Carbon::setTestNow(Carbon::create(2000, 05, 01, 1, 00, 00));
        $this->sender->amapAlerteFin();
        Carbon::setTestNow();
    }

    private function buildAmapien(string $etat)
    {
        $amapien = new Amapien();
        $amapien->setEtat($etat);

        return $amapien;
    }
}
