<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;

class ExcelGeneratorDistributionAmap
{
    /**
     * @param AmapDistribution[] $amapDistributions
     */
    public function genererExportDistributionInscrits($amapDistributions): string
    {
        usort($amapDistributions, function (AmapDistribution $d1, AmapDistribution $d2) {
            $tDiff = $d1->getDate()->getTimestamp() - $d2->getDate()->getTimestamp();
            if (0 !== $tDiff) {
                return $tDiff;
            }

            $d1Detail = $d1->getDetail();
            $d2Detail = $d2->getDetail();

            $hsDiff = $d1Detail->getHeureDebut()->getMinutesTotal() - $d2Detail->getHeureDebut()->getMinutesTotal();
            if (0 !== $hsDiff) {
                return $hsDiff;
            }

            return $d1Detail->getHeureFin()->getMinutesTotal() - $d2Detail->getHeureFin()->getMinutesTotal();
        });
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Filters
        $sheet->setCellValue('A1', 'Date');
        $sheet->setCellValue('B1', 'Heure de début');
        $sheet->setCellValue('C1', 'Heure de fin');
        $sheet->setCellValue('D1', 'Tâche');
        $sheet->setCellValue('E1', 'Nom de l\'amapien');
        $sheet->setCellValue('F1', 'Prénom de l\'amapien');
        $sheet->setCellValue('G1', 'Téléphone de l\'amapien');
        $sheet->setCellValue('H1', 'Email de l\'amapien');
        $sheet->getStyle('A1:H1')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        $row = 2;
        foreach ($amapDistributions as $amapDistribution) {
            $distributionAmapiens = $amapDistribution->getAmapiens()->toArray();
            if (0 === count($distributionAmapiens)) {
                $this->amapDistributionInfo($sheet, $row, $amapDistribution);
            } else {
                /** @var Amapien $amapien */
                foreach ($amapDistribution->getAmapiens() as $amapien) {
                    $this->amapDistributionInfo($sheet, $row, $amapDistribution);
                    $sheet->getCellByColumnAndRow(5, $row)->setValue($amapien->getNom());
                    $sheet->getCellByColumnAndRow(6, $row)->setValue($amapien->getPrenom());
                    $sheet->getCellByColumnAndRow(7, $row)->setValue($amapien->getNumTel1());
                    $sheet->getCellByColumnAndRow(8, $row)->setValue($amapien->getEmail());
                    ++$row;
                }
            }
            ++$row;
        }

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportDistributionAmap_');
        $objWriter = new Xlsx($spreadsheet);
        $objWriter->save($outputFileName);

        return $outputFileName;
    }

    private function amapDistributionInfo(Worksheet $sheet, int $row, AmapDistribution $distribution): void
    {
        $sheet->getCellByColumnAndRow(1, $row)->setValue($distribution->getDate()->format('d/m/Y'));
        $sheet->getCellByColumnAndRow(2, $row)->setValue($distribution->getDetail()->getHeureDebut());
        $sheet->getCellByColumnAndRow(3, $row)->setValue($distribution->getDetail()->getHeureFin());
        $sheet->getCellByColumnAndRow(4, $row)->setValue($distribution->getDetail()->getTache());
    }
}
