<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Reseau;

class SearchAmapState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    /**
     * @var null|Reseau
     */
    private $reseau;

    /**
     * @var null|string
     */
    private $adhesion;

    /**
     * @var null|bool
     */
    private $etudiante;

    /**
     * @var bool | null
     */
    private $carto;

    /**
     * @var bool | null
     */
    private $insurance;

    /**
     * @var string | null
     */
    private $keyword;

    public function isEmpty(): bool
    {
        return null === $this->getRegion()
            && null === $this->getDepartement()
            && null === $this->getReseaux()
            && null === $this->getAdhesion()
            && null === $this->getEtudiante()
            && null === $this->getKeyword()
        ;
    }

    public function getReseaux(): ?Reseau
    {
        return $this->reseau;
    }

    public function setReseaux(?Reseau $reseau): SearchAmapState
    {
        $this->reseau = $reseau;

        return $this;
    }

    public function getAdhesion(): ?string
    {
        return $this->adhesion;
    }

    public function setAdhesion(?string $adhesion): SearchAmapState
    {
        $this->adhesion = $adhesion;

        return $this;
    }

    public function getEtudiante(): ?bool
    {
        return $this->etudiante;
    }

    public function setEtudiante(?bool $etudiante): SearchAmapState
    {
        $this->etudiante = $etudiante;

        return $this;
    }

    public function getCarto(): ?bool
    {
        return $this->carto;
    }

    public function setCarto(?bool $carto): SearchAmapState
    {
        $this->carto = $carto;

        return $this;
    }

    public function getInsurance(): ?bool
    {
        return $this->insurance;
    }

    public function setInsurance(?bool $insurance): SearchAmapState
    {
        $this->insurance = $insurance;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): SearchAmapState
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getReseau(): ?Reseau
    {
        return $this->reseau;
    }

    public function setReseau(?Reseau $reseau): SearchAmapState
    {
        $this->reseau = $reseau;

        return $this;
    }
}
