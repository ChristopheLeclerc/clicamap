<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\DTO;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\AdhesionImport;
use PsrLib\ORM\Entity\Adhesion;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\AdhesionValue;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AdhesionAmapienRepository;
use PsrLib\ORM\Repository\AdhesionAmapRepository;
use PsrLib\ORM\Repository\AdhesionFermeRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class AdhesionImportTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testEmpty()
    {
        $adhesions = new AdhesionImport([]);
        $violations = $this->validator->validate($adhesions, null, ['import']);
        self::assertEmpty($violations);
    }

    public function testDiffrentCreator()
    {
        $creator1 = $this->prophesize(Amapien::class);
        $creator1->getId()->willReturn(1);
        $adhesion1 = $this->prophesize(Adhesion::class);
        $adhesion1->getCreator()->willReturn($creator1->reveal());

        $creator2 = $this->prophesize(Amapien::class);
        $creator2->getId()->willReturn(2);
        $adhesion2 = $this->prophesize(Adhesion::class);
        $adhesion2->getCreator()->willReturn($creator2->reveal());

        $adhesions = new AdhesionImport([$adhesion1->reveal(), $adhesion2->reveal()]);
        $violations = $this->validator->validate($adhesions, null, ['import']);
        self::assertCount(1, $violations);
        self::assertSame('Toutes les adhésions doivent avoir le meme createur.', $violations[0]->getMessage());
    }

    public function testVoucherNumberDuplicationInFile()
    {
        $creator = $this->prophesize(Amapien::class);
        $creator->getId()->willReturn(1);
        $adhesion1Value = $this->prophesize(AdhesionValue::class);
        $adhesion1Value->getVoucherNumber()->willReturn('1');
        $adhesion1 = $this->prophesize(Adhesion::class);
        $adhesion1->getValue()->willReturn($adhesion1Value->reveal());
        $adhesion1->getId()->willReturn(1);
        $adhesion1->getCreator()->willReturn($creator);

        $adhesion2Value = $this->prophesize(AdhesionValue::class);
        $adhesion2Value->getVoucherNumber()->willReturn('1');
        $adhesion2 = $this->prophesize(Adhesion::class);
        $adhesion2->getValue()->willReturn($adhesion2Value->reveal());
        $adhesion2->getId()->willReturn(2);
        $adhesion2->getCreator()->willReturn($creator);

        $adhesions = new AdhesionImport([$adhesion1->reveal(), $adhesion2->reveal()]);
        $violations = $this->validator->validate($adhesions, null, ['import']);
        self::assertCount(1, $violations);
        self::assertSame('Duplication du numéro de reçus "1" dans le fichier.', $violations[0]->getMessage());
    }

    public function testVoucherNumberDuplicationInDatabase()
    {
        $creator = $this->prophesize(Amapien::class);
        $creator->getId()->willReturn(1);
        $adhesion1Value = $this->prophesize(AdhesionValue::class);
        $adhesion1Value->getVoucherNumber()->willReturn('1');
        $adhesion1 = $this->prophesize(Adhesion::class);
        $adhesion1->getValue()->willReturn($adhesion1Value->reveal());
        $adhesion1->getId()->willReturn(1);
        $adhesion1->getCreator()->willReturn($creator);

        $adhesion2Value = $this->prophesize(AdhesionValue::class);
        $adhesion2Value->getVoucherNumber()->willReturn('2');
        $adhesion2 = $this->prophesize(Adhesion::class);
        $adhesion2->getValue()->willReturn($adhesion2Value->reveal());
        $adhesion2->getId()->willReturn(2);
        $adhesion2->getCreator()->willReturn($creator);

        // Mock entity manager
        $em = $this->prophesize(EntityManagerInterface::class);
        $amapRepo = $this->prophesize(AdhesionAmapRepository::class);
        $amapRepo->getVoucherNumbers($creator->reveal())->willReturn([]);
        $em->getRepository(AdhesionAmap::class)->willReturn($amapRepo->reveal());
        $fermeRepo = $this->prophesize(AdhesionFermeRepository::class);
        $fermeRepo->getVoucherNumbers($creator->reveal())->willReturn([]);
        $em->getRepository(AdhesionFerme::class)->willReturn($fermeRepo->reveal());
        $amapienRepo = $this->prophesize(AdhesionAmapienRepository::class);
        $amapienRepo->getVoucherNumbers($creator->reveal())->willReturn(['1']);
        $em->getRepository(AdhesionAmapien::class)->willReturn($amapienRepo->reveal());

        // Mock Entity Manager
        $oldEm = self::$container->get(EntityManagerInterface::class);
        self::$container->set(EntityManagerInterface::class, $em->reveal());

        $adhesions = new AdhesionImport([$adhesion1->reveal(), $adhesion2->reveal()]);
        $violations = $this->validator->validate($adhesions, null, ['import']);
        self::assertCount(1, $violations);
        self::assertSame('Le numéro de reçus "1" existe déjà dans la base de donnée.', $violations[0]->getMessage());

        self::$container->set(EntityManagerInterface::class, $oldEm);
    }
}
