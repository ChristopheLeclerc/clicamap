<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use HTMLPurifier;
use HTMLPurifier_Config;
use PsrLib\ProjectLocation;
use RuntimeException;

/**
 * Wrapper around HTMLPurfier.
 */
class Purifier
{
    public const CONFIG_DEFAULT = 'CONFIG_DEFAULT';
    public const CONFIG_SUMMERNOTE = 'CONFIG_SUMMERNOTE';
    public const CONFIG_SUMMERNOTE_SINGLELINE = 'CONFIG_SUMMERNOTE_SINGLELINE';

    /**
     * @var HTMLPurifier
     */
    private $purifier;

    /**
     * @var HTMLPurifier_Config[]
     */
    private $CONFIG_MAP = [];

    public function __construct()
    {
        $cacheDir = ProjectLocation::PROJECT_ROOT.'/application/writable/cache/htmlpurifier';
        if (!is_dir($cacheDir) && !mkdir($cacheDir, 0777, true) && !is_dir($cacheDir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $cacheDir));
        }

        $configSummernote = HTMLPurifier_Config::createDefault();
        $configSummernote->set('HTML.Allowed', 'p[align|style],b,u,ol,ul,li,a[href],pre,blockquote,h1,h2,h3,h4,h5,h6,span[style],font[color],table[class],tbody,tr,td');
        $configSummernote->set('CSS.AllowedProperties', ['background-color', 'margin-left']);
        $configSummernote->set('Core.RemoveProcessingInstructions', true);
        $configSummernote->set('Cache.SerializerPath', $cacheDir);
        $configSummernote->set('Attr.AllowedClasses', ['table', 'table-bordered']);
        $this->CONFIG_MAP[self::CONFIG_SUMMERNOTE] = $configSummernote;

        $configSummernoteSingleLine = HTMLPurifier_Config::createDefault();
        $configSummernoteSingleLine->set('HTML.Allowed', 'b,u');
        $configSummernoteSingleLine->set('CSS.AllowedProperties', '');
        $configSummernoteSingleLine->set('Core.RemoveProcessingInstructions', true);
        $configSummernoteSingleLine->set('Cache.SerializerPath', $cacheDir);
        $this->CONFIG_MAP[self::CONFIG_SUMMERNOTE_SINGLELINE] = $configSummernoteSingleLine;

        $configDefault = HTMLPurifier_Config::createDefault();
        $configDefault->set('HTML.Allowed', '');
        $configDefault->set('CSS.AllowedProperties', '');
        $configDefault->set('Core.RemoveProcessingInstructions', true);
        $configDefault->set('Cache.SerializerPath', $cacheDir);
        $this->CONFIG_MAP[self::CONFIG_DEFAULT] = $configDefault;

        $this->purifier = new HTMLPurifier();
    }

    public function purify(string $input, string $filter = self::CONFIG_DEFAULT): string
    {
        return $this->purifier->purify($input, $this->CONFIG_MAP[$filter]);
    }
}
