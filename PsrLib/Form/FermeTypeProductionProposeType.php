<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\TypeProductionType;
use PsrLib\ORM\Entity\FermeTypeProductionPropose;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotNull;

class FermeTypeProductionProposeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('typeProduction', TypeProductionType::class, [
                'multiple' => false,
                'required' => false,
                'placeholder' => true,
            ])
            ->add('nbAmapLivrees', TextType::class, [
                'attr' => ['placeholder' => 'ex: 5'],
                'required' => false,
            ])
            ->add('nbContratsTotalAmap', TextType::class, [
                'attr' => ['placeholder' => 'ex: 5'],
                'required' => false,
            ])
            ->add('caTotal', TextType::class, [
                'attr' => ['placeholder' => 'ex: 25.5'],
                'required' => false,
            ])
            ->add('caAnnuelAmap', TextType::class, [
                'attr' => ['placeholder' => 'ex: 30000'],
                'required' => false,
            ])
            ->add('certification', FileType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotNull(['message' => 'Vous n\'avez pas sélectionné de fichier à envoyer.']),
                    new File([
                        'mimeTypes' => ['application/pdf'],
                        'maxSize' => '20Mi',
                        'mimeTypesMessage' => 'Le type de fichier que vous tentez d\'envoyer n\'est pas autorisé.',
                        'maxSizeMessage' => 'Le fichier que vous tentez d\'envoyer est plus gros que la taille autorisée.',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FermeTypeProductionPropose::class,
        ]);
    }
}
