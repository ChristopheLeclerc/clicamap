#!/usr/bin/env bash

set -xe

php vendor/bin/doctrine orm:schema-tool:update --force # init db stucture
mysql -h mariadb -u default -psecret defaultdb < docker/data.sql # add common data
php bin/console app:fixtures:load # load fixtures
