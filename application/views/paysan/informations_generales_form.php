<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\Paysan $paysan */
$currentUser = get_current_connected_user();
?>

<!-- ************************* CADRE RÉSEAU ******************************** -->

<?php if ($form->offsetExists('cadreReseau')) {
    ?>

    <div class="alert alert-warning">
        <div class="row">
            <div class="col-md-6">

                <label for="groupe_w" class="col-md-6 control-label">Groupe de travail :</label>
                <?= render_form_radio_widget($form['cadreReseau']['payCrGrpeW'], 'col-md-3'); ?>
            </div>

            <div class="col-md-6">

                <label for="ca" class="col-md-6 control-label">CA :</label>

                <?= render_form_radio_widget($form['cadreReseau']['payCrCa'], 'col-md-3'); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <label for="visite_dem" class="col-md-6 control-label">Visite démarrage :</label>
                <?= render_form_radio_widget($form['cadreReseau']['payCrVisiteDem'], 'col-md-3'); ?>

            </div>

            <div class="col-md-6">

                <label for="fiche_loc_exp" class="col-md-6 control-label">Fiche locale d’expérience :</label>
                <?= render_form_radio_widget($form['cadreReseau']['payCrFicheLocExp'], 'col-md-3'); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <label for="cagn_sol" class="col-md-6 control-label">Cagnotte solidaire :</label>

                <?= render_form_radio_widget($form['cadreReseau']['payCrCagnSol'], 'col-md-3'); ?>

            </div>
        </div>
    </div>

    <?php
}

?>

<!-- *********************************************************************** -->


<?php if ($form->offsetExists('ferme')): ?>
    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <label for="statut" class="col-md-12 control-label">Ferme :</label>
                <?= render_form_select_widget($form['ferme']); ?>

            </div>
        </div>
    </div>
<?php endif; ?>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <label for="statut" class="col-md-12 control-label">Statut :</label>
            <?= render_form_radio_widget($form['statut'], 'col-md-6'); ?>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6">
                <label for="nom" class="control-label">Nom : </label>
                <?= render_form_text_widget($form['nom']); ?>
            </div>

            <div class="form-group col-md-6">
                <label for="prenom" class="control-label">Prénom : </label>
                <?= render_form_text_widget($form['prenom']); ?>
            </div>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6">
                <label for="conjoint_nom" class="control-label">Nom du conjoint : </label>
                <?= render_form_text_widget($form['conjointNom']); ?>
            </div>

            <div class="form-group col-md-6">
                <label for="conjoint_prenom" class="control-label">Prénom du conjoint : </label>
                <?= render_form_text_widget($form['conjointPrenom']); ?>
            </div>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6">
                <label for="email" class="control-label">Email : </label>
                <?= render_form_text_widget($form['email']); ?>
            </div>

        </div>

    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <label for="email_invalid" class="col-md-12 control-label">Email "fictif" invalide</label>

            <?= render_form_radio_widget($form['emailInvalid'], 'col-md-6'); ?>

        </div>

        <div class="col-md-12">
            <div class="col-md-12">
                <p class="text-muted mb-0">Si vous indiquez un email qui n’existe pas, merci de sélectionner “oui”. Le
                    paysan ne pourra pas se connecter ni recevoir des emails, mais vous pourrez réaliser des contrats à
                    sa place. Si la coche est à "oui" sans intervention de votre part, merci de vérifier que l'adresse
                    email est correcte.</p>
            </div>
        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6">
                <label for="num_tel_1" class="control-label">Téléphone n°1 : </label>
                <?= render_form_text_widget($form['numTel1']); ?>
            </div>

            <div class="form-group col-md-6">
                <label for="num_tel_2" class="control-label">Téléphone n°2 : </label>
                <?= render_form_text_widget($form['numTel2']); ?>
            </div>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6">
                <label for="lib_adr" class="control-label">Adresse : </label>
                <?= render_form_text_widget($form['libAdr']); ?>
            </div>

            <div class="form-group col-md-6">
                <label for="code_postal_ville" class="control-label">Code postal, Ville (à remplir à l'aide des
                    suggestions) : </label>
                <?= render_form_text_widget($form['ville']); ?>

                <div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>

                <?= form_error('code_postal_ville'); ?>
            </div>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6">
                <label for="date_installation" class="control-label">Date d'installation</label>
                <?= render_form_datepicker_widget($form['dateInstallation']); ?>
            </div>

            <div class="form-group col-md-6">
                <label for="annee_deb_commercialisation_amap" class="control-label">Année de début de commercialisation
                    en AMAP : </label>
                <?= render_form_text_widget($form['anneeDebCommercialisationAmap']); ?>

                <?= form_error('annee_deb_commercialisation_amap'); ?>
            </div>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-6">
                <label for="msa" class="control-label">Numéro MSA : </label>
                <?= render_form_text_widget($form['msa']); ?>
            </div>

            <div class="form-group col-md-6">
                <label for="spg" class="control-label">Visite de partenariat : </label>
                <?= render_form_text_widget($form['spg']); ?>
            </div>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <label for="newsletter" class="col-md-12 control-label">J'autorise Clic'AMAP à mettre à jour mes coordonnées
                sur les listes de diffusion email du réseau auquel j'appartiens</label>

            <?= render_form_radio_widget($form['newsletter'], 'col-md-6'); ?>

        </div>
    </div>
</div>
