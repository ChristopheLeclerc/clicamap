# language: fr
Fonctionnalité: Comme amapien, je doit avoir un apercu du contrat avant sa validation

  Scénario: Creation d'un contrat jusqu'à l'étape de validation
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Quand je suis sur "/contrat_signe/contrat_own_new/"
    Et que je suis "Souscrire" sur la ligne contenant "contrat 1"
    Et que remplis le texte suivant:
      | commande[1][1] | 1 |
      | commande[2][1] | 1 |
      | commande[3][1] | 1 |
      | commande[4][1] | 1 |
      | commande[5][1] | 1 |
    Et que je presse "Suivant"
    Et que je remplis "nb_reglements" avec "1"
    Et que je remplis "reglement[0]" avec "1"
    Et que je presse "Suivant"
    Alors je devrais voir "Validation juridique de mon contrat"
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    Pour un panier de production
    contrat 1
    dans le cadre de l'AMAP amap test
    """
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    Le présent contrat est signé entre :
    Madame/Monsieur :  prenom amapien nom amapien
    Résidant : adresse 69001 LYON 1er Arrondissement
    """
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    La ferme :  ferme
    Production de production
    Nom de l’entité juridique : ferme
    Résidant : 58 Rue Raulin 69007 LYON 7e Arrondissement
    """
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    Article 4 : Durée du contrat
    Le contrat court du  01/01/2030  au  30/04/2030  et comprend 18 livraisons.
    """
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    5.1 Le/La paysan.ne s’engage
    Type de panier Prix du panier TTC
    produit 1 / cond1 1,00 €
    produit 2 / cond2 2,00 €
    Le/La paysan.ne s’engage au(x) date(s) suivante(s) :
    produit 1 produit 2
    01/01/2030   1   1
    08/01/2030   1   1
    15/01/2030   1   1
    22/01/2030   1   1
    29/01/2030   1   1
    05/02/2030   1   1
    12/02/2030   1   1
    19/02/2030   1   1
    26/02/2030   1   1
    05/03/2030   1   1
    12/03/2030   1   1
    19/03/2030   1   1
    26/03/2030   1   1
    02/04/2030   1   1

    09/04/2030   1   1
    16/04/2030   1   1
    23/04/2030   1   1
    30/04/2030   1   1
    """
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    5.2 L’amapien.ne s’engage
    produit 1 / cond1 produit 2 / cond2
    01/01/2030 1
    0
    08/01/2030 1
    0
    15/01/2030 1
    0
    22/01/2030 1
    0
    29/01/2030 1
    0
    L’amapien.ne s’engage au(x) date(s) suivante(s) :
    01/01/2030-
    08/01/2030-
    15/01/2030-
    22/01/2030-
    29/01/2030-
    """
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    Le(s) règlement(s) est/sont encaissé(s) à la/aux date(s) suivante(s) :
    Nombre de règlement Date d’encaissement Montant
    1 01/01/2020 5,00 €
    """
    Et le ficher PDF "http://localhost/contrat_signe/preview_pdf" devrais contenir :
    """
    L’amapien.ne
    prenom amapien nom amapien Le/La Paysan.ne
    Paysan prénom Paysan nom
    """
