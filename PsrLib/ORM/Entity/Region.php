<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\ORM\Entity\Files\Logo;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\RegionRepository")
 * @ORM\Table(name="ak_region")
 */
class Region implements EntityWithLogoInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="reg_id", type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="reg_nom", length=55, nullable=false)
     */
    private $nom;

    /**
     * @var ArrayCollection<Amapien>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="adminRegions")
     * @JoinTable(name="ak_region_administrateur",
     *      joinColumns={@JoinColumn(name="reg_adm_fk_region_id", referencedColumnName="reg_id")},
     *      inverseJoinColumns={@JoinColumn(name="reg_adm_fk_amapien_id", referencedColumnName="a_id")}
     * )
     */
    private $admins;

    /**
     * @var null|Logo
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Files\Logo", cascade={"ALL"}, orphanRemoval=true)
     */
    private $logo;

    /**
     * @var null|string
     * @ORM\Column(name="reg_reseau_url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var ArrayCollection<Departement>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\Departement", mappedBy="region")
     */
    private $departements;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->admins = new ArrayCollection();
        $this->departements = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getNom();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Region
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Add admin.
     *
     * @return Region
     */
    public function addAdmin(Amapien $admin)
    {
        $this->admins[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdmin(Amapien $admin)
    {
        return $this->admins->removeElement($admin);
    }

    /**
     * Get admins.
     *
     * @return ArrayCollection<Amapien>
     */
    public function getAdmins()
    {
        return $this->admins;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Region
    {
        $this->url = $url;

        return $this;
    }

    public function getLogo(): ?Logo
    {
        return $this->logo;
    }

    public function setLogo(?Logo $logo): Region
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Add admin.
     *
     * @return Region
     */
    public function addDepartement(Departement $admin)
    {
        $this->departements[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDepartement(Departement $admin)
    {
        return $this->departements->removeElement($admin);
    }

    /**
     * Get departements.
     *
     * @return ArrayCollection<Departement>
     */
    public function getDepartements()
    {
        return $this->departements;
    }
}
