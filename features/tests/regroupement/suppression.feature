# language: fr
Fonctionnalité: Suppression d'un regroupement depuis différents profils

  @database
  Plan du Scénario: Suppression du regroupement
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Et que je suis sur "/evenement"
    Quand je suis "Gestion des regroupements"
    Et que je suis "Supprimer le regroupement"
    Et que je presse "Confirmer la suppression du regroupement"
    Alors je devrais voir "Regroupement supprimé"
    Alors je ne devrais pas voir "regroupement@test.amap-aura.org"

    Exemples:
      | mail                          |
      | admin@test.amap-aura.org      |
      | admin.aura@test.amap-aura.org |
