<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\ServiceFactories;

use PsrLib\ProjectLocation;
use PsrLib\Twig\ArrayExtension;
use PsrLib\Twig\CodeIgniterFormExtension;
use PsrLib\Twig\CodeigniterTemplateRenderExtension;
use PsrLib\Twig\DateExtension;
use PsrLib\Twig\DebugBarExtension;
use PsrLib\Twig\FlashExtension;
use PsrLib\Twig\SecurityExtension;
use PsrLib\Twig\StringExtension;
use PsrLib\Twig\UrlExtension;
use Symfony\Bridge\Twig\Extension\DumpExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Translation\Translator;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Twig\Environment;
use Twig\Extension\CoreExtension;
use Twig\Loader\FilesystemLoader;
use Twig\RuntimeLoader\FactoryRuntimeLoader;

class TwigEnvironmentFactory
{
    public function create(
        Translator $translator,
        UrlExtension $urlExtension,
        CodeigniterTemplateRenderExtension $codeigniterTemplateRenderExtension,
        DebugBarExtension $debugBarExtension,
        FlashExtension $flashExtension,
        SecurityExtension $securityExtension,
        CodeIgniterFormExtension $codeIgniterFormExtension,
        DateExtension $dateExtension,
        ArrayExtension $arrayExtension,
        StringExtension $stringExtension
    ): Environment {
        $loader = new FilesystemLoader(
            [
                ProjectLocation::PROJECT_ROOT.'/vendor/symfony/twig-bridge/Resources/views/Form',
                ProjectLocation::PROJECT_ROOT.'/templates',
            ]
        );
        $twig = new Environment($loader, [
            'debug' => 'production' !== getenv('CI_ENV'),
            'cache' => ProjectLocation::PROJECT_ROOT.'/application/writable/cache/twig',
            'strict_variables' => true,
        ]);
        $twig->addExtension(new DumpExtension(
            new VarCloner()
        ));
        $twig->addExtension(new FormExtension());
        $twig->addExtension(new TranslationExtension($translator));
        $twig->addExtension($debugBarExtension);
        $twig->addExtension($urlExtension);
        $twig->addExtension($codeigniterTemplateRenderExtension);
        $twig->addExtension($codeIgniterFormExtension);
        $twig->addExtension($flashExtension);
        $twig->addExtension($securityExtension);
        $twig->addExtension($dateExtension);
        $twig->addExtension($arrayExtension);
        $twig->addExtension($stringExtension);
        $twig->addRuntimeLoader(new FactoryRuntimeLoader([
            FormRenderer::class => function () use ($twig) {
                return new FormRenderer(
                    new TwigRendererEngine(
                        [
                            'form/form_theme.html.twig',
                        ],
                        $twig
                    )
                );
            },
        ]));

        /** @phpstan-ignore-next-line */
        $twig->getExtension(CoreExtension::class)->setDateFormat('d/m/Y', '%d jours');

        return $twig;
    }
}
