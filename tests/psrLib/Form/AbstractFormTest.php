<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Form;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\Services\AuthentificationMockable;
use Symfony\Component\Form\FormFactoryInterface;
use Test\ContainerAwareTestCase;

abstract class AbstractFormTest extends ContainerAwareTestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var AuthentificationMockable
     */
    protected $authentification;

    public function setUp()
    {
        parent::setUp();
        $this->em = self::$container->get(EntityManagerInterface::class);
        $this->formFactory = self::$container->get(FormFactoryInterface::class);
        $this->authentification = self::$container->get(AuthentificationMockable::class);
    }
}
