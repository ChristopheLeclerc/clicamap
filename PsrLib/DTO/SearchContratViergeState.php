<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\Reseau;

class SearchContratViergeState
{
    /**
     * @var null|Region
     */
    private $region;

    /**
     * @var null|Departement
     */
    private $departement;

    /**
     * @var null|Reseau
     */
    private $reseau;

    /**
     * @var Amap | null
     */
    private $amap;

    /**
     * @var Ferme | null
     */
    private $ferme;

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): SearchContratViergeState
    {
        $this->region = $region;

        return $this;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): SearchContratViergeState
    {
        $this->departement = $departement;

        return $this;
    }

    public function getReseau(): ?Reseau
    {
        return $this->reseau;
    }

    public function setReseau(?Reseau $reseau): SearchContratViergeState
    {
        $this->reseau = $reseau;

        return $this;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap): SearchContratViergeState
    {
        $this->amap = $amap;

        return $this;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): SearchContratViergeState
    {
        $this->ferme = $ferme;

        return $this;
    }
}
