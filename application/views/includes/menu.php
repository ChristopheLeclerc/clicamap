<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
$currentUser = get_current_connected_user();
?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#"><img alt="<?= SITE_NAME; ?>" src="<?= base_url('assets/images/logo-clicamap.png'); ?>" style="height:3.5em;"/></a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav"><!-- align left -->

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?= SITE_NAME; ?>&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('portail'); ?>">Les événements</a></li>
                            <li><a href="https://amap-aura.org/categorie/clicamap/" target="_blank"><?= SITE_NAME; ?> c'est quoi ?</a></li>
                            <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_DOCUMENT_GETOWN)): ?>
                                <li><a href="<?= site_url('documentation'); ?>">Documentation</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>

                    <?php if ($currentUser instanceof \PsrLib\ORM\Entity\FermeRegroupement): ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestionnaire regroupement&nbsp;<span class="caret"></span>&nbsp;</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= site_url('ferme'); ?>">Gestion des fermes</a></li>
                                <li><a href="<?= site_url('paysan'); ?>">Gestion des paysans</a></li>
                                <?php if ($currentUser->getFermes()->count() > 0): ?>
                                    <li><a href="<?= site_url('ferme/calendrier/'.$currentUser->getFermes()->first()->getId()); ?>">Les livraisons</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>


                    <?php
                    if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isSuperAdmin()) {
                        ?>

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Réseaux&nbsp;<span class="caret"></span>&nbsp;</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= site_url('region'); ?>">Régions &amp; Départements</a></li>
                                <li><a id="menu-link-network" href="<?= site_url('reseau'); ?>">Réseaux</a></li>
                            </ul>
                        </li>

                    <?php
                    }
                    ?>

                    <?php
                    if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin()) {
                        ?>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestionnaire AMAP&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('amap'); ?>">Gestion des AMAP</a></li>
                            <li><a href="<?= site_url('amapien'); ?>">Gestion des amapiens</a></li>
                            <li><a href="<?= site_url('import'); ?>">Import</a></li>
			    <li><hr/></li>
                            <li><a href="<?= site_url('ferme'); ?>">Gestion des fermes</a></li>
                            <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST)): ?>
                                <li><a href="<?= site_url('/ferme_regroupement'); ?>">Gestion des regroupements</a></li>
                            <?php endif; ?>
                            <li><a href="<?= site_url('paysan'); ?>">Gestion des paysans</a></li>
			    <li><hr/></li>
                            <li><a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a></li>
                            <li><a href="<?= site_url('contrat_signe'); ?>">Gestion des contrats signés</a></li>
                        </ul>
                    </li>

                    <?php
                    } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
                        ?>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestionnaire AMAP&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('amap'); ?>">Gestion de mon AMAP</a></li>
                            <li><a href="<?= site_url('amapien'); ?>">Gestion des amapiens</a></li>
                            <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_IMPORT_AMAP_AMAPIEN_ADHESION)): ?>
                                <li><a href="<?= site_url('adhesion/amap_amapien'); ?>">Gestion des adhésions</a></li>
                            <?php endif; ?>
                            <li><hr/></li>
                            <li><a href="<?= site_url('ferme'); ?>">Gestion des fermes</a></li>
                            <li><a href="<?= site_url('paysan'); ?>">Gestion des paysans</a></li>
			    <li><hr/></li>
                            <li><a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a></li>
                            <li><a href="<?= site_url('contrat_signe'); ?>">Gestion des contrats signés</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Agenda&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
			  <li><a href="<?= site_url('amap/calendrier/'.$currentUser->getId()); ?>">Calendrier des livraisons AMAP</a></li>
                          <li><a href="<?= site_url('distribution'); ?>">Gestion de la distrib'AMAP</a></li>
			</ul>
		    </li>

                    <?php
                    } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
                        ?>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestionnaire référent&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('ferme'); ?>">Gestion des fermes</a></li>
                            <li><a href="<?= site_url('paysan'); ?>">Gestion des paysans</a></li>
			    <li><hr/></li>
                            <li><a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a></li>
                            <li><a href="<?= site_url('contrat_signe'); ?>">Gestion des contrats signés</a></li>
                        </ul>
                    </li>

                    <?php
                    }
                    if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
                        ?>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestionnaire ferme&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('ferme/home_paysan'); ?>">Ma ferme</a></li>
                            <li><a href="<?= site_url('produit/accueil/'.$currentUser->getFerme()->getId()); ?>">Mes produits</a></li>
                            <li><a href="<?= site_url('ferme/calendrier/'.$currentUser->getFerme()->getId()); ?>">Mes livraisons</a></li>
                        </ul>
                    </li>
                        <?php
                    }
                    if ($currentUser instanceof \PsrLib\ORM\Entity\UserWithFermes) {
                        ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestion des contrats&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('contrat_vierge'); ?>">Contrats à valider</a></li>
                            <li><a href="<?= site_url('contrat_signe'); ?>">Contrats en cours</a></li>
                            <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_LIST_ARCHIVED)) :?>
				<li><a href="<?= site_url('contrat_signe/contrat_paysan_archived'); ?>">Contrats archivés</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>

                    <?php
                    }
                    if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY_OWN)) {
                        ?>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mes contrats&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?=site_url('contrat_signe/contrat_own_new/'); ?>">Les nouveaux contrats disponibles</a>
                            </li>
                            <li>
                                <a href="<?=site_url('contrat_signe/contrat_own_existing/'); ?>">Mes contrats existants</a>
                            </li>
                            <li>
                                <a href="<?=site_url('contrat_signe/contrat_own_archived/'); ?>">Mes contrats archivés</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mon agenda&nbsp;<span class="caret"></span>&nbsp;</a>
                      <ul class="dropdown-menu">
                        <?php if (!$currentUser->isAdmin() && null !== $currentUser->getAmap()) { ?>
                          <li><a href="<?= site_url('amapien/calendrier/'.$currentUser->getId()); ?>">Mes livraisons</a></li>
                        <?php } ?>
                        <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN)) {?>
                          <li><a href="<?= site_url('distribution/amapien'); ?>">Mes distrib'AMAP</a></li>
                        <?php } ?>
                      </ul>
                    </li>

                    <?php
                    }
                    if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin()) {
                        ?>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Communication&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('evenement'); ?>">Événements</a></li>
                            <li><a href="<?= site_url('publipostage'); ?>">Publipostage</a></li>
                            <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_DOCUMENT_MANAGE)) {?>
                                <li><a href="<?= site_url('documentation/list_documents'); ?>">Gestion des documents</a></li>
                            <?php } ?>
                        </ul>
                    </li>

		    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reçus&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_IMPORT)) :?>
                                <li><a href="<?= site_url('adhesion/import'); ?>">Import</a></li>
                            <?php endif; ?>
                            <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_LIST)): ?>
                                <li><a href="<?= site_url('adhesion/amap'); ?>">AMAP</a></li>
                                <li><a href="<?= site_url('adhesion/paysan'); ?>">Paysan</a></li>
                                <li><a href="<?= site_url('adhesion/amapien'); ?>">Amapien</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>

                    <?php
                    }
                    ?>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php
                        if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
                            echo render_menu_amap_link_paysan($currentUser);
                        }
                        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien) {
                            echo render_menu_amap_link_amapien($currentUser);
                        }
                        if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
                            echo render_menu_ferme_link_amap($currentUser);
                        }
                    ?>

                    <?php
                    if (null !== $currentUser
                        && !($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin())
                        && !($currentUser instanceof \PsrLib\ORM\Entity\FermeRegroupement)) {
                        ?>

                        <li class="dropdown dropdown-menu-right">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Contact&nbsp;<span class="caret"></span>&nbsp;</a>
                            <ul class="dropdown-menu">

                                <?php
                                if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
                                    echo '<li><a href="'.site_url('contact/admin').'">Un administrateur réseau</a></li>';
                                } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
                                    echo '<li><a href="'.site_url('contact/admin').'">Un administrateur réseau</a></li>';
                                    echo '<li><a href="'.site_url('contact/amap').'">Une AMAP</a></li>';
                                } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien) {
                                    echo '<li><a href="'.site_url('contact/mon_amap').'">Mon AMAP</a></li>';
                                    echo '<li><a href="'.site_url('contact/referent_produit').'">Un référent produit</a></li>';
                                } ?>


                            </ul>
                        </li>

                        <?php
                    }
                    ?>

                    <li class="dropdown pull-right">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mon compte&nbsp;<span class="caret"></span>&nbsp;</a>
                        <ul class="dropdown-menu">

                            <?php
                            if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
                                if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_LIST_SELF)) {
                                    echo '<li><a href="'.site_url('adhesion/mes_recus_amap').'">Mes reçus</a></li>';
                                }
                                echo '<li><a href="'.site_url('amap/mot_de_passe/'.$currentUser->getId()).'">Mot de passe</a></li>';
                            }

                            if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien) {
                                echo '<li><a href="'.site_url('amapien/display/'.$currentUser->getId()).'">Mon profil</a></li>';

                                // Si il est affilié à une AMAP !
                                if (!$currentUser->isAdmin() && null !== $currentUser->getAmap()) {
                                    if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_LIST_SELF)) {
                                        echo '<li><hr/></li>';
                                        echo '<li><a href="'.site_url('adhesion/mes_recus_amapien').'">Mes reçus</a></li>';
                                    }
                                }
                            } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
                                echo '<li><a href="'.site_url('paysan/display/'.$currentUser->getId()).'">Mon profil</a></li>';
                                echo '<li><a href="'.site_url('paysan/recherche_amap/').'">Mes recherches d’AMAP</a></li>';

                                if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_LIST_SELF)) {
                                    echo '<li><a href="'.site_url('adhesion/mes_recus_ferme').'">Mes reçus</a></li>';
                                }
                            }

                            if ($currentUser instanceof \PsrLib\ORM\Entity\FermeRegroupement) {
                                echo '<li><a href="'.site_url('ferme_regroupement/profil').'">Mon profil</a></li>';
                            }

                            if (null !== $currentUser) {
                                echo '<li><hr/></li>';
                                echo '<li><a href="'.site_url('portail/deconnexion').'">Déconnexion</a></li>';
                            } else {
                                echo '<li><a href="'.site_url('portail/connexion').'">Connexion</a></li>';
                            }
                            ?>

                        </ul>
                    </li>
                </ul>

    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
