<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\Paysan $paysan */
$currentUser = get_current_connected_user();

echo '<h3>Informations générales</h3>';

if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
    echo '<h5><a href="'.site_url('paysan/display/'.$currentUser->getId()).'">Profil</a> <i class="glyphicon glyphicon-menu-right"></i> ';
} else {
    echo '<h5><a href="'.site_url('paysan').'">Gestion des paysans</a> <i class="glyphicon glyphicon-menu-right"></i> ';
}

echo mb_strtoupper($paysan->getNom()).' '.ucfirst($paysan->getPrenom()).' <i class="glyphicon glyphicon-menu-right"></i> Édition';

echo '</h5>';

echo '<form method="POST" action="">';

include 'informations_generales_form.php';
?>

<div class="form-group">

    <input type="submit" value="Sauvegarder" class="pull-right btn btn-success" style="margin-left:5px;"/>

    <?php if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
    echo '<a href="'.site_url('paysan/display/'.$currentUser->getId()).'" class="pull-right btn btn-default">Annuler</a>';
} // Redirection sur toutes les fiches
    else {
        echo '<a href="'.site_url('paysan').'" class="pull-right btn btn-default">Annuler</a>';
    }

    ?>

</div>

</form>
