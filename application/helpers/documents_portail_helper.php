<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!function_exists('render_documents_portail')) {
    function render_documents_portail()
    {
        // Charge l'instance
        $CI = &get_instance();

        /** @var \PsrLib\ORM\Entity\Document[] $documents */
        $documents = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\Doctrine\ORM\EntityManagerInterface::class)
            ->getRepository(\PsrLib\ORM\Entity\Document::class)
            ->findBy([
                'permissionAnonyme' => true,
            ])
        ;

        // Tri par ordre aphabétique
        usort($documents, function (PsrLib\ORM\Entity\Document $a, PsrLib\ORM\Entity\Document $b) {
            return strnatcmp(mb_strtolower($a->getNom()), mb_strtolower($b->getNom()));
        });

        $res = '';
        foreach ($documents as $document) {
            $res .= '<p><a href="'.prep_url($document->getLien()).'" target="_blank">'.$document->getNom().'</a></p>';
        }

        return $res;
    }
}
