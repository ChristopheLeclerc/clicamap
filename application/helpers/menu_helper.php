<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('render_menu_amap_link_paysan')) {
    function render_menu_amap_link_paysan(PsrLib\ORM\Entity\Paysan $paysan)
    {
        // Charge l'instance
        $CI = &get_instance();
        /** @var \PsrLib\ORM\Entity\Amap[] $amaps */
        $amaps = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\Doctrine\ORM\EntityManagerInterface::class)
            ->getRepository(\PsrLib\ORM\Entity\Amap::class)
            ->findByFerme($paysan->getFerme())
        ;

        // Filtre les amaps avec url
        $amaps = array_filter($amaps, function (PsrLib\ORM\Entity\Amap $amap) {
            return '' !== $amap->getUrl() && null !== $amap->getUrl();
        });

        // Tri les amaps par ordre alphabétique
        usort($amaps, function (PsrLib\ORM\Entity\Amap $a, PsrLib\ORM\Entity\Amap $b) {
            return strnatcmp(strtolower($a->getNom()), strtolower($b->getNom()));
        });

        // Formate pour l'impression
        $liens = array_map(function (PsrLib\ORM\Entity\Amap $amap_model) {
            return [
                'lien' => $amap_model->getUrl(),
                'nom' => $amap_model->getNom(),
            ];
        }, $amaps);

        return $CI->load->view('menu/external_links', ['liens' => $liens], true);
    }
}

if (!function_exists('render_menu_amap_link_amapien')) {
    function render_menu_amap_link_amapien(PsrLib\ORM\Entity\Amapien $amapien)
    {
        // Récupère l'amap de l'amapien
        $amapAmapien = $amapien->getAmap();

        if (empty($amapAmapien)) { // SI aucune amap associés à l'amapien
            return '';
        }

        // Récupère les fermes de l'amapien
        $CI = &get_instance();
        /** @var \PsrLib\ORM\Entity\Ferme[] $fermes */
        $fermes = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\Doctrine\ORM\EntityManagerInterface::class)
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->getFromAmap($amapAmapien)
        ;

        // Filtre les fermes avec url
        $fermes = array_filter($fermes, function (PsrLib\ORM\Entity\Ferme $ferme) {
            return '' !== $ferme->getUrl() && null !== $ferme->getUrl();
        });

        // Extrait les informations des fermes
        $liens = array_map(function (PsrLib\ORM\Entity\Ferme $ferme) {
            return [
                'lien' => $ferme->getUrl(),
                'nom' => $ferme->getNom(),
            ];
        }, $fermes);

        // Tri les fermes
        uasort($liens, function ($lien1, $lien2) {
            return strnatcmp(strtolower($lien1['nom']), strtolower($lien2['nom']));
        });

        // Ajoute le lien vers l'amap au début
        if ('' !== $amapAmapien->getUrl()) {
            array_unshift($liens, [
                'nom' => 'Mon amap',
                'lien' => $amapAmapien->getUrl(),
            ]);
        }

        return $CI->load->view('menu/external_links', ['liens' => $liens], true);
    }
}

if (!function_exists('render_menu_ferme_link_amap')) {
    function render_menu_ferme_link_amap(PsrLib\ORM\Entity\Amap $amap)
    {
        // Charge l'instance
        $CI = &get_instance();

        /** @var \PsrLib\ORM\Entity\Ferme[] $fermes */
        $fermes = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\Doctrine\ORM\EntityManagerInterface::class)
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->getFromAmap($amap)
        ;

        // Filtre les fermes avec url
        $fermes = array_filter($fermes, function (PsrLib\ORM\Entity\Ferme $ferme) {
            return '' !== $ferme->getUrl() && null !== $ferme->getUrl();
        });

        // Tri les fermes par ordre alphabétique
        usort($fermes, function (PsrLib\ORM\Entity\Ferme $a, PsrLib\ORM\Entity\Ferme $b) {
            return strnatcmp(strtolower($a->getNom()), strtolower($b->getNom()));
        });

        // Formate pour l'impression
        $liens = array_map(function (PsrLib\ORM\Entity\Ferme $ferme) {
            return [
                'lien' => $ferme->getUrl(),
                'nom' => $ferme->getNom(),
            ];
        }, $fermes);

        return $CI->load->view('menu/external_links', ['liens' => $liens], true);
    }
}
