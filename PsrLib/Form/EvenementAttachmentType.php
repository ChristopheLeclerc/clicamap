<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class EvenementAttachmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('img', FileType::class, [
                'constraints' => [
                    new File([
                        'mimeTypes' => ['image/jpeg', 'image/png'],
                        'maxSize' => '1Mi',
                        'mimeTypesMessage' => 'Le type de fichier que vous tentez d\'envoyer n\'est pas autorisé.',
                        'maxSizeMessage' => 'Le fichier que vous tentez d\'envoyer est plus gros que la taille autorisée.',
                    ]),
                ],
            ])
            ->add('pj', FileType::class, [
                'constraints' => [
                    new File([
                        'mimeTypes' => ['application/pdf'],
                        'maxSize' => '1Mi',
                        'mimeTypesMessage' => 'Le type de fichier que vous tentez d\'envoyer n\'est pas autorisé.',
                        'maxSizeMessage' => 'Le fichier que vous tentez d\'envoyer est plus gros que la taille autorisée.',
                    ]),
                ],
            ])
        ;
    }
}
