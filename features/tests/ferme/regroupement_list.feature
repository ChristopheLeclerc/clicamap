# language: fr
Fonctionnalité: Liste des fermes associée au regroupement

  @database
  Scénario: Liste des fermes associée au regroupement
    Etant donné que je suis authentifié comme "regroupement" avec le mail "regroupement@test.amap-aura.org"
    Et que la ferme "ferme" est associées au regroupement "regroupement@test.amap-aura.org"
    Quand je suis sur "/evenement"
    Et que je suis "Gestion des fermes"
    Alors je devrais être sur "/ferme"
    Et je devrais voir "Paysan prénom PAYSAN NOM"
