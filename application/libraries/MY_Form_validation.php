<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class MY_Form_validation extends CI_Form_validation
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    public function __construct($rules = [])
    {
        parent::__construct($rules);
        $this->em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(\Doctrine\ORM\EntityManagerInterface::class);
    }

    public function bool_check($bool)
    {
        return '0' === $bool || '1' === $bool;
    }

    /**
     * @param $cp_ville
     *
     * @return bool
     */
    public function cp_ville_check($cp_ville)
    {
        if ($cp_ville) {
            // On recherche la virgule dans la chaîne
            $pos = strpos($cp_ville, ',');
            if (!$pos) {
                $this->set_message('cp_ville_check', 'Le champ "Code postal, Ville" est mal formaté.');

                return false;
            } // On vérifie le couple cp / ville

            $adr = explode(',', $cp_ville);
            $cp = trim($adr[0]);
            $ville = trim($adr[1]);
            $exist = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Ville::class)
                ->isCpVilleExistAndUnique($cp, $ville)
            ;
            if (!$exist) {
                $this->set_message('cp_ville_check', 'Le couple "'.$cp.'" / "'.$ville.'" ne correspond à aucun lieu.');

                return false;
            }
        }

        return true;
    }

    //  Format de la date
    public function date_format($date)
    {
        $format = 'Y-m-d';
        $d = DateTime::createFromFormat($format, $date);
        //Check for valid date in given format
        if (!empty($date)) {
            if ($d && $d->format($format) == $date) {
                return true;
            }

            $this->set_message('date_format', 'Une date doit être au format suivant : '.$format.' (exemple : 2018-06-28)');

            return false;
        }
    }

    //Quand on édite un paysan, l'Email doit être unique
    //Seulement s'il diffère de l'email du paysan à modifier
    public function email_check_paysan($email, $paysan_id)
    {
        $CI = &get_instance();
        $exist = $CI->paysan_model->paysan_email_check($email, $paysan_id);
        if (!$exist) {
            return true;
        }
        $this->set_message('email_check_paysan', 'Cet email existe déjà dans la base de donnée');

        return false;
    }

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    /////////////////// PARTIE EVENEMENTS  ////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    //partie evenenements
    public function date_deb_format_check($date)
    {
        $format = 'Y-m-d';
        $d = DateTime::createFromFormat($format, $date);
        //Check for valid date in given format
        if ($d && $d->format($format) == $date) {
            return true;
        }
        $this->set_message(
            'date_deb_format_check',
            'Une date doit être au format suivant : '.$format.' (exemple : 2018-06-28)'
        );

        return false;
    }

    public function date_fin_format_check($date)
    {
        $format = 'Y-m-d';
        $d = DateTime::createFromFormat($format, $date);
        //Check for valid date in given format
        if ($date) {
            if ($d && $d->format($format) == $date) {
                return true;
            }
            $this->set_message(
                'date_fin_format_check',
                'Une date doit être au format suivant : '.$format.' (exemple : 2018-06-28)'
            );

            return false;
        }

        return true;
    }

    public function hor_deb_check($h)
    {
        if (!preg_match('/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/', $h)) {
            $this->set_message('hor_deb_check', 'L\'heure de début est mal formatée');

            return false;
        }

        return true;
    }

    public function hor_fin_check($h)
    {
        if (!preg_match('/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/', $h)) {
            $this->set_message('hor_fin_check', 'L\'heure de fin est mal formatée');

            return false;
        }

        return true;
    }

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    /////////////////// PARTIE EVENEMENTS + RESEAUX ///////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    //  Vérifier le champ amapien
    public function amapien_check($a)
    {
        $amapien = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->findOneBy([
                'email' => $a,
            ])
        ;
        if (null === $amapien) {
            $this->set_message('amapien_check', 'L\'amapien n\'existe pas dans la base de donnée.');

            return false;
        }

        return true;
    }

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ////////////////////////////// FERME //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    //  Vérifier l'autocomplétion paysan
    public function paysan_check($pay)
    {
        $CI = &get_instance();
        $paysan = $CI->paysan_model->paysan_get_from_email($pay);
        if (!$paysan) {
            $this->set_message('paysan_check', 'Le paysan n\'existe pas dans la base de donnée ou le nom est mal formaté.');

            return false;
        }

        return true;
    }

    // edition ferme
    // Le numéro SIRET est-il unique ?
    // Le SIRET doit être unique s'il diffère du siret de la ferme à modifier
    public function edit_siret_check($siret, $ferme_id)
    {
        $CI = &get_instance();
        $exist = $CI->ferme_model->check_siret_ferme($siret, $ferme_id);
        if ($exist) {
            $this->set_message('edit_siret_check', 'Ce siret existe déjà dans la base de donnée.');

            return false;
        }

        return true;
    }

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////// CONTRAT VIERGE //////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    public function forclusion_check($f)
    {
        if (empty($f)) {
            return true;
        }
        $format = 'Y-m-d';
        $d = DateTime::createFromFormat($format, $f);
        //Check for valid date in given format
        if ($d && $d->format($format) == $f) {
            return true;
        }

        $this->set_message('forclusion_check', 'Une date doit être au format suivant : '.$format.' (exemple : 2016-06-28)');

        return false;
    }

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////// CONTRAT VIERGE //////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    //Quand on édite un amapien, l'Email doit être unique
    //Seulement s'il diffère de l'email de l'amapien à modifier
    public function email_check_amapien($email, $amapien_id)
    {
        $CI = &get_instance();
        $exist = $CI->amapien_model->amapien_email_check($email, $amapien_id);
        if (!$exist) {
            return true;
        }
        $this->set_message('email_check_amapien', 'Cet email existe déjà dans la base de donnée');

        return false;
    }

    /**
     * Vérifie que le captcha fourni est correct.
     *
     * @param string $captcha
     *
     * @return bool
     */
    public function captcha_check($captcha)
    {
        $CI = &get_instance();
        $CI->load->library('Captcha');
        if (ENVIRONMENT === 'testing' && 'captchaValid' === $captcha) {
            return true;
        }

        $captchaValid = $CI->captcha->verifCaptcha($captcha);

        if (!$captchaValid) {
            $this->set_message('captcha_check', 'Code de vérification incorrect');

            return false;
        }

        return true;
    }

    public function password_check($confirmation, $password)
    {
        return $password === $confirmation;
    }

    public function password_strength_check($password)
    {
        $score = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\PsrLib\Services\Zxcvbn::class)
            ->estimateScore($password)
        ;

        return $score >= 2;
    }

    /**
     * Validate given url is valid.
     *
     * @ref https://stackoverflow.com/a/6427654
     *
     * @param string $str
     *
     * @return bool
     */
    public function valid_url($str)
    {
        $regex = '((https?|ftp)://)?'; // SCHEME
        $regex .= '([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?'; // User and Pass
        $regex .= '([a-z0-9\-\.]*)\.(([a-z]{2,4})|([0-9]{1,3}\.([0-9]{1,3})\.([0-9]{1,3})))'; // Host or IP
        $regex .= '(:[0-9]{2,5})?'; // Port
        $regex .= '(/([a-z0-9+$_%-]\.?)+)*/?'; // Path
        $regex .= '(\?[a-z+&\$_.-][a-z0-9;:@&%=+/$_.-]*)?'; // GET Query
        $regex .= '(#[a-z_.-][a-z0-9+$%_.-]*)?'; // Anchor

        return 1 === preg_match("~{$regex}~", $str);
    }

    /**
     * Test la limite de 3 documents en permission anonyme
     * Exclut le document courant
     * Appelé uniquement si ca se cochée.
     *
     * @param $val
     * @param mixed $document_id
     *
     * @return bool
     */
    public function document_permission_anonyme_max_count_check($val, $document_id)
    {
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(\Doctrine\ORM\EntityManagerInterface::class);

        $nbAnonymes = $em
            ->getRepository(\PsrLib\ORM\Entity\Document::class)
            ->countDocumentAnonyme((int) $document_id)
        ;

        return $nbAnonymes <= 2;
    }

    public function valid_date($str)
    {
        if (1 !== preg_match('~\\d{4}-\\d{2}-\\d{2}~', $str)) {
            $this->set_message('valid_date', 'Date invalide');

            return false;
        }

        return true;
    }

    public function date_inf($date, $otherDateInputs)
    {
        $date = DateTime::createFromFormat('Y-m-d', $date);

        foreach (explode(',', $otherDateInputs) as $otherDateInput) {
            $otherDate = DateTime::createFromFormat(
                'Y-m-d',
                $otherDateInput
            );

            if ($date >= $otherDate) {
                $this->set_message('date_inf', 'Date invalide');

                return false;
            }
        }

        return true;
    }

    public function date_jour_semaine($date, $jours)
    {
        $weekDayMap = [
            'lundi' => '1',
            'mardi' => '2',
            'mercredi' => '3',
            'jeudi' => '4',
            'vendredi' => '5',
            'samedi' => '6',
            'dimanche' => '7',
        ];

        $date = DateTime::createFromFormat('Y-m-d', $date);
        $weekDay = $date->format('N');

        foreach (explode(',', $jours) as $jour) {
            if ($weekDayMap[$jour] === $weekDay) {
                return true;
            }
        }

        $this->set_message('date_jour_semaine', 'Jour invalide');

        return false;
    }

    public function no_duplication($product_id, $input)
    {
        $inputArray = explode(',', $input);
        $count = 0;
        array_walk($inputArray, function ($inputItem) use (&$count, $product_id) {
            if ($product_id === $inputItem) {
                ++$count;
            }

            return true;
        });

        return 1 === $count;
    }

    /**
     * Test le que nombre de lignes avec au moins une commande est au moins égal au nombre en parametres.
     *
     * @param $minLineCount
     *
     * @return bool
     */
    public function command_row_gte_than_count($minLineCount)
    {
        return true;
    }

    /**
     * Is Unique.
     *
     * Check if the input value doesn't already exist
     * in the specified entity field.
     *
     * WARNING : entity field is not safe from SQL injection !!
     *
     * @param string $str
     * @param string $entityField
     *
     * @return bool
     */
    public function is_unique($str, $entityField)
    {
        sscanf($entityField, '%[^.].%[^.]', $entity, $field);

        $count = $this
            ->em
            ->createQueryBuilder()
            ->select('COUNT(e)')
            ->from($entity, 'e')
            ->where('e.'.$field.' = :val')
            ->setParameter('val', $str)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return 0 === (int) $count;
    }

    public function liv_jour_check($str)
    {
        return in_array($str, \PsrLib\ORM\Entity\AmapLivraisonHoraire::JOURS, true);
    }

    public function liv_saison_check($str)
    {
        return in_array($str, \PsrLib\ORM\Entity\AmapLivraisonHoraire::SAISONS, true);
    }
}
