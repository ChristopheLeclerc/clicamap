<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\AmapAnneeAdhesionType;
use PsrLib\Form\Type\VilleType;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\Amap;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class AmapCreationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => false,
            ])
            ->add('adresseAdmin', TextType::class, [
                'required' => false,
            ])
            ->add('adresseAdminNom', TextType::class, [
                'required' => false,
            ])
            ->add('etat', ChoiceType::class, [
                'choices' => [
                    'En création' => Amap::ETAT_CREATION,
                    'En fonctionnement' => Amap::ETAT_FONCIONNEMENT,
                    'En attente' => Amap::ETAT_ATTENTE,
                ],
            ])
            ->add('possedeAssurance', YesNoChoiceType::class, [
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'constraints' => $options['emailRequired'] ? [new NotBlank()] : [],
            ])
            ->add('url', TextType::class, [
                'required' => false,
            ])
            ->add('anneeCreation', TextType::class, [
                'required' => false,
            ])
            ->add('nbAdherents', TextType::class, [
                'required' => false,
            ])
            ->add('adresseAdminVille', VilleType::class, [
                'constraints' => [new NotNull(['message' => 'Le champ "Code postal, Ville administrative" est requis.'])],
            ])
            ->add('anneeAdhesions', AmapAnneeAdhesionType::class, [
                'amap' => $options['data'],
            ])
            ->add('siret', TextType::class, [
                'required' => false,
            ])
            ->add('rna', TextType::class, [
                'required' => false,
            ])
        ;

        if ($options['additionalFields']) {
            $builder
                ->add('amapEtudiante', YesNoChoiceType::class, [
                    'required' => false,
                ])
                ->add('associationDeFait', YesNoChoiceType::class, [
                    'required' => false,
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Amap::class,
            'emailRequired' => true,
            'additionalFields' => true,
        ]);
    }
}
