<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\FermeAnneeAdhesion;

/**
 * Bind added file to ferme adhesion list.
 */
class AdhesionFermePostGenerateSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [Events::ADHESION_POST_GENERATE];
    }

    public function adhesionPostGenerate(AdhesionPostGenerateEventArgs $args): void
    {
        $adhesion = $args->getAdhesion();
        if (!($adhesion instanceof AdhesionFerme)) {
            return;
        }

        $ferme = $adhesion->getFerme();
        $year = $adhesion->getValue()->getYear();

        $existingAdhesion = $ferme
            ->getAnneeAdhesions()
            ->filter(function (FermeAnneeAdhesion $amapAnneeAdhesion) use ($year) {
                return $amapAnneeAdhesion->getAnnee() === $year;
            })
        ;

        if (!$existingAdhesion->isEmpty()) {
            return;
        }

        $newFermeAnneeAdhesion = new FermeAnneeAdhesion($year, $ferme);
        $ferme->addAnneeAdhesion($newFermeAnneeAdhesion);
    }
}
