<?php

$config = [
    'protocol' => getenv('PROTOCOL'),
    'smtp_host' => getenv('SMTP_HOST'),
    'smtp_port' => getenv('SMTP_PORT'),
    'smtp_user' => getenv('SMTP_USER'),
    'smtp_pass' => getenv('SMTP_PASS'),
    'mailtype' => getenv('MAILTYPE'),
    'charset' => getenv('CHARSET'),
    'crlf' => "\r\n",
    'newline' => "\r\n",
];
