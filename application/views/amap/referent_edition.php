<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \Symfony\Component\Form\FormView $form */
?>

<h3>Le correspondant réseau AMAP</h3>

<?php

if (get_current_connected_user() instanceof \PsrLib\ORM\Entity\Amap) {
    $retour = 'Gestion de mon AMAP';
} else {
    $retour = 'Gestion des AMAP';
}

?>

<h5><a href="<?= site_url('amap'); ?>"><?= $retour; ?></a> <i
            class="glyphicon glyphicon-menu-right"></i> <?= mb_strtoupper($amap->getNom()); ?> <i
            class="glyphicon glyphicon-menu-right"></i>
    Édition</h5>

<form method="POST" action="">

    <input type="hidden" name="step_ref" value="1"/>

    <div class="alert alert-info">Si vous voulez ajouter un correspondant réseau à l'AMAP, utilisez le menu déroulant
        ci-dessous. L'amapien choisi pour être correspondant réseau AMAP doit faire partie de l'AMAP. Pour ensuite
        retirer les droits d'administration à l'amapien, il suffit de cliquer sur l'amapien à supprimer. Vous ne pouvez
        pas retirez vos propres droits d'administration.
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            <?= render_form_select_widget($form['referent']); ?>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" value="Éditer" class="pull-right btn btn-success"/>
        </div>
    </div>
</form>

<?php
$refReseau = $amap->getAmapienRefReseau();
if (null !== $refReseau) {
    echo '<div class="alert alert-success">Le correspondant réseau AMAP actuel est <strong>'.$refReseau->getPrenom().'&nbsp;'.$refReseau->getNom().'</strong>.</div>';
} else {
    echo '<div class="alert alert-warning">L\'AMAP n\'a pas encore de correspondant réseau AMAP.</div>';
}
?>
