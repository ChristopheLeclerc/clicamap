<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\AdhesionAmapAmapien[] $adhesions */
/** @var int[] $adhesionYears */
/** @var int $selectedYear */
/** @var string $selectedSearchKeyword */
?>

<div class="col-md-12">
    <h3>Reçus amapiens</h3>
</div>

<div class="col-md-12">
    <div class="alert alert-info">
        Pour importer les données nécessaires à la création des reçus d’adhésions veuillez <a href="<?=site_url('/import'); ?>">cliquer ici</a>
    </div>
</div>

<div class="col-md-12">
    <div class="well">
        <form action="/adhesion/amap_amapien" class="js-form-search">
            <div class="row">


                <div class="col-md-4">
                    <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Année d'adhésion</button>
                    </span>

                        <?=form_dropdown(
    'search_year',
    [-1 => null] + array_combine($adhesionYears, $adhesionYears),
    $selectedYear,
    ['class' => 'form-control']
); ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Mot clé</button>
                    </span>
                        <?=form_input(
    'search_keyword',
    $selectedSearchKeyword,
    ['class' => 'form-control']
); ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <a class="btn btn-success" href="<?=site_url('/import'); ?>">Import des reçus</a>
                    <a class="btn btn-success pull-right" href="<?=site_url('/adhesion/amap_amapien'); ?>">Réinitialiser les critères</a>
                </div>
            </div>
        </form>
    </div>
</div>

<form method="POST" action="/adhesion/amap_amapien_action">
    <div class="col-md-12">
        <?php if (count($adhesions) > 0): ?>
            <button
                class="btn btn-success js-trigger-modal"
                name="action-generate"
                type="submit"
                data-title="Êtes-vous sûr ?"
                data-message="Vous allez générer tous les reçus en brouillon. Cette action va se lancer dès la prochaine minute. Veuillez actualiser la page afin d’accéder aux reçus."
                data-confirm-class="btn-danger"
            >Générer les reçus</button>
            <button
                    class="btn btn-success"
                    name="action-download"
                    type="submit"
            >Télécharger</button>
            <button
                class="btn btn-danger js-trigger-modal"
                name="action-delete"
                type="submit"
                data-title="Êtes-vous sûr ?"
                data-message="Vous allez supprimer les reçus sélectionnés. Cette action est irréversible"
                data-confirm-class="btn-danger"
            >Supprimer les reçus</button>
        <?php endif; ?>

        <?php foreach ($adhesions as $adhesion): ?>
            <input
                type="checkbox"
                class="js-input-binded hidden"
                name="<?= sprintf('selected[%d]', $adhesion->getId()); ?>"
            />
        <?php endforeach; ?>
    </div>
</form>
<?php if (count($adhesions) > 0): ?>
    <div class="col-md-12">
        <table class="table table-datatable" data-order='[[ 1, "desc" ]]'>
            <thead>
            <tr>
                <th data-orderable="false">
                    <input
                        type="checkbox"
                        class="js-select-all"
                    />
                </th>
                <th>Numéro de reçu</th>
                <th>Nom</th>
                <th>Année</th>
                <th>Statut</th>
                <th>Createur</th>
                <th>Outils</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($adhesions as $adhesion): ?>
                <tr>
                    <td>
                        <input
                            type="checkbox"
                            class="js-auto-selectable js-binder"
                            data-target-name="<?= sprintf('selected[%d]', $adhesion->getId()); ?>"
                        />
                    </td>
                    <td><?= $adhesion->getValue()->getVoucherNumber(); ?></td>
                    <td><?= $adhesion->getValue()->getPayer(); ?></td>
                    <td><?= $adhesion->getValue()->getYear(); ?></td>
                    <td><?= \PsrLib\ORM\Entity\Adhesion::STATE_LABEL[$adhesion->getState()]; ?></td>
                    <td><?= $adhesion->getCreator()->getEmail(); ?></td>
                    <td>
                        <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD, $adhesion)) : ?>
                            <a title="Télécharger le reçu" href="<?=site_url('/adhesion/amap_amapien_download/'.$adhesion->getId()); ?>">
                                <i class="glyphicon glyphicon-file" data-toggle="tooltip" title="Télécharger le reçu"></i>
                            </a>
                        <?php endif; ?>

                        <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_AMAPIEN_DELETE, $adhesion)) : ?>
                            <form method="POST" action="<?=site_url('/adhesion/amap_amapien_delete/'.$adhesion->getId()); ?>" class="form-inline">
                                <button
                                        class="btn btn-link js-trigger-modal"
                                        data-title="Êtes-vous sûr ?"
                                        data-message="Vous allez supprimer le reçu sélectionné. Cette action est irréversible"
                                        data-confirm-class="btn-danger"
                                        type="submit">
                                    <i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le reçu"></i>
                                </button>
                            </form>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
