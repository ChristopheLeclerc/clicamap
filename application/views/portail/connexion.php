<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?= form_open('portail/connexion');

?>

<div class="well">
    <div class="row">
        <div class="col-sm-12">

            <label for="statut" class="col-sm-12 control-label">Vous êtes :</label>

            <div class="form-group col-md-3">
                <label class="control-label radio-inline">
                    <input type="radio" name="statut"
                           value="amapien" <?= set_radio('statut', 'amapien', true); ?> /> Un(e)
                    administrateur(trice)</label>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label radio-inline">
                    <input type="radio" name="statut"
                           value="amapien" <?= set_radio('statut', 'amapien', true); ?> /> Un(e)
                    amapien(ne)</label>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label radio-inline">
                    <input type="radio" name="statut" value="paysan" <?= set_radio('statut', 'paysan'); ?> />
                    Un(e) paysan(ne)
                </label>
            </div>

            <div class="form-group col-md-3">
                <label class="control-label radio-inline">
                    <input type="radio" name="statut" value="regroupement" <?= set_radio('statut', 'regroupement'); ?> />
                    Un regroupement
                </label>
            </div>

            <div class="form-group col-sm-6">
                <input class="form-control" type="text" name="email" placeholder="Email"
                       value="<?= set_value('email'); ?>"/>
                <?php
                if ($alerte_email) {
                    echo '<div class="alert alert-danger">'.$alerte_email.'</div>';
                }

                echo form_error('email'); ?>
            </div>

            <div class="form-group col-sm-6">
                <input class="form-control" type="password" name="password" placeholder="Mot de passe"
                       value="<?= set_value('password'); ?>"/>
                <?php
                if ($alerte_password) {
                    echo '<div class="alert alert-danger">'.$alerte_password.'</div>';
                }
                echo form_error('password');
                ?>

                <div class="text-left">
                    <a href="/portail/mdp_oublie" >Mot de passe oublié ?</a>
                </div>

            </div>

        </div>
    </div>
</div>

<button type="submit" class="btn btn-success">Connexion</button>

</form>

<div class="row">
    <div class="col-sm-6 col-xs-12">
        <div class="well text-left">
            <p><b>Contact :</b></p>
            <p>Vous souhaitez administrer <?= SITE_NAME; ?> pour votre AMAP,
		<a href="<?= site_url('portail/contact'); ?>">contactez-nous</a>.
		<br />Le réseau vous transmettra alors un identitfiant qui vous
                permettra de gérer votre AMAP et vos contrats en toute simplicité grâce à <?= SITE_NAME; ?>.</p>
                <br />
        </div>
    </div>

    <div class="col-sm-6 col-xs-12">
        <div class="well text-left">
            <p><b>Pour aller plus loin :</b></p>
            <?= render_documents_portail(); ?>
        </div>
    </div>
</div>

<?= render_slick_logo_reseaux(); ?>

<?php
/** Formulaire de contact de la page d'accueil */
if (isset($print_contact_form) && true === $print_contact_form) {
    require_once __DIR__.'/../includes/contact_modal.php';
}
?>

<?php require_once __DIR__.'/connexionModal/modalLostPasswordForm.php'; ?>
<?php require_once __DIR__.'/connexionModal/modalLostPasswordResetForm.php'; ?>
