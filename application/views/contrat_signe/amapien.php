<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<h3>Sélection de l'amapien</h3>

<?= '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.$mc[0]->mc_nom.' <i class="glyphicon glyphicon-menu-right"></i> Ajout du contrat <i class="glyphicon glyphicon-menu-right"></i> Étape 1</h5>';

?>

<form method="POST" action="">

    <div class="row">
        <div class="form-group">
            <label for="amapien_id" class="col-md-12 control-label">Amapiens : </label>
            <div class="col-md-12">
                <select class="form-control" id="amapien_id" name="amapien_id">

                    <?php

                    foreach ($amapiens as $amapien) {
                        echo '<option value="'.$amapien->a_id.'">'.strtoupper(htmlspecialchars($amapien->a_nom)).' '.ucfirst(htmlspecialchars($amapien->a_prenom)).' ('.$amapien->a_email.')</option>';
                    }

                    ?>

                </select>
            </div>
        </div>
    </div>

    <div class="form-group">

        <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>

        <a href="<?= site_url('contrat_signe/'); ?>" class="pull-right btn btn-default">Annuler</a>

    </div>

</form>
