<?php

namespace PsrLib\Services\Security\Checkers;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Paysan;

class DocumentCheck extends AbstractCheck
{
    public function isGrantedManageDocument(): bool
    {
        if ($this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isSuperAdmin()) {
            return true;
        }

        return false;
    }

    public function isGrantedGetOwnDocument(): bool
    {
        $currentUser = $this->getCurrentUser();

        return $currentUser instanceof Amapien || $currentUser instanceof Paysan || $currentUser instanceof Amap;
    }
}
