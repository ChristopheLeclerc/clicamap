<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use LogicException;

/**
 * Simple Manager registry to use doctrine symfony bridge.
 */
class SimpleManagerRegistry implements ManagerRegistry
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * SimpleManagerRegistry constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getDefaultConnectionName()
    {
        return 'default';
    }

    public function getConnection($name = null)
    {
        return $this->em->getConnection();
    }

    public function getConnections()
    {
        return [$this->em->getConnection()];
    }

    public function getConnectionNames()
    {
        return ['default'];
    }

    public function getDefaultManagerName()
    {
        return 'default';
    }

    public function getManager($name = null)
    {
        return $this->em;
    }

    public function getManagers()
    {
        return [$this->em];
    }

    public function resetManager($name = null)
    {
        throw new LogicException('Not implemented yet');
    }

    public function getAliasNamespace($alias)
    {
        throw new LogicException('Not implemented yet');
    }

    public function getManagerNames()
    {
        throw new LogicException('Not implemented yet');
    }

    public function getRepository($persistentObject, $persistentManagerName = null)
    {
        throw new LogicException('Not implemented yet');
    }

    public function getManagerForClass($class)
    {
        return $this->em;
    }
}
