<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
$currentUser = get_current_connected_user();
?>

<h3>Types de production</h3>

<?php if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan): ?>
    <h5><a href="<?=site_url('paysan/display/'.$currentUser->getId()); ?>">Profil</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="<?=site_url('/ferme/home_paysan/'.$ferme->getId()); ?>"><?=$ferme->getNom(); ?></a><i class="glyphicon glyphicon-menu-right"></i> Types de production</h5>
<?php else: ?>
    <h5><a href="<?=site_url('ferme'); ?>">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="<?=site_url('ferme/display/'.$ferme->getId()); ?>"><?=$ferme->getNom(); ?></a><i class="glyphicon glyphicon-menu-right"></i> Types de production</h5>
<?php endif; ?>

<p>
    <a href="<?= site_url('ferme/type_production_creation/'.$ferme->getId()); ?>"
       class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Ajouter</a>
</p>

<p>

    <div class="table-responsive">
        <table class="table">
            <thead class="thead-inverse">
            <tr>
                <th>Type de production</th>
                <th>Nombre d'AMAP livrées/s</th>
                <th>Nombre total de contrats en AMAP</th>
                <th>Chiffre d'affaire total en AMAP</th>
                <th>Part du CA total (sur l'année écoulée en %)</th>
                <th>Attestation de certification</th>
                <th>Outils</th>
            </tr>
            </thead>

            <?php

            echo '<tbody>';

            /** @var \PsrLib\ORM\Entity\FermeTypeProductionPropose $tpp */
            foreach ($ferme->getTypeProductionProposes() as $tpp) {
                echo '<tr>';
                echo '<th scope="row">';

                echo $tpp->getTypeProduction()->getNomComplet();

                echo '</th>';

                echo '<td>';
                echo $tpp->getNbAmapLivrees();
                echo '</td>';

                echo '<td>';
                echo $tpp->getNbContratsTotalAmap();
                echo '</td>';

                echo '<td>';
                echo $tpp->getCaAnnuelAmap();
                echo '</td>';

                echo '<td>';
                echo $tpp->getCaTotal();
                echo '</td>';

                echo '<td>';
                echo '<a href="/'.$tpp->getAttestationCertification()->getFileRelativePath().'" target="_blank"/>'.$tpp->getAttestationCertification()->getOriginalFileName().'</a>';
                echo '</td>';

                echo '<td>';
                echo '<a title="Supprimer le produit" href="#" data-toggle="modal" data-target="#modal_remove_'.$tpp->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le produit"></i></a> ';
                echo '</td>';
                echo '</tr>'; ?>

            <!-- Modal remove -->
            <div class="modal fade" id="modal_remove_<?= $tpp->getId(); ?>" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer le produit ?</h4>
                        </div>
                        <div class="modal-body">
    <p class="text-right">
        <a href="<?= site_url('ferme/type_production_remove/'.$tpp->getId()); ?>"
           class="btn btn-danger">OUI</a>
        <a href="#" class="btn btn-success" data-dismiss="modal">NON</a>
    </p>
    </div>
    </div>
    </div>
    </div>

<?php
            } ?>

</tbody>
</table>
</div>
