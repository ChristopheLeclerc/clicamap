<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Services;

use Money\Money;

/**
 * @internal
 * @coversNothing
 */
class MoneyHelperTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider parseProvider
     */
    public function testParse(string $input, Money $expected)
    {
        $parsed = \PsrLib\Services\MoneyHelper::fromString($input);
        self::assertTrue($expected->equals($parsed));
    }

    public function parseProvider()
    {
        return [
            ['123', Money::EUR(12300)],
            ['123.45', Money::EUR(12345)],
            ['0123', Money::EUR(12300)],
            ['0', Money::EUR(0)],
            ['0.45', Money::EUR(45)],
        ];
    }
}
