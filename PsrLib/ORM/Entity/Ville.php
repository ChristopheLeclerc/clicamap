<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\VilleRepository")
 * @ORM\Table(name="ak_ville")
 */
class Ville implements LocalisableEntity
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="string", length=5, name="v_id", unique=true)
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", name="v_cp", columnDefinition="INT(5) UNSIGNED ZEROFILL NOT NULL")
     */
    private $cp;

    /**
     * @var string
     * @ORM\Column(type="string", length=30, name="v_nom")
     */
    private $nom;

    /**
     * @var Departement
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Departement", fetch="EAGER")
     * @ORM\JoinColumn(name="v_fk_dep_id", referencedColumnName="dep_id", nullable=false)
     */
    private $departement;

    /**
     * Ville constructor.
     */
    public function __construct(string $id, int $cp, string $nom, Departement $departement)
    {
        $this->id = $id;
        $this->cp = $cp;
        $this->nom = $nom;
        $this->departement = $departement;
    }

    public function __toString()
    {
        return $this->getCpString().', '.$this->getNom();
    }

    public function getCpString(): string
    {
        return sprintf('%05d', $this->getCp());
    }

    public function getRegion(): ?Region
    {
        return $this->getDepartement()->getRegion();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Ville
    {
        $this->id = $id;

        return $this;
    }

    public function getCp(): int
    {
        return $this->cp;
    }

    public function setCp(int $cp): Ville
    {
        $this->cp = $cp;

        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Ville
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDepartement(): Departement
    {
        return $this->departement;
    }

    public function setDepartement(Departement $departement): Ville
    {
        $this->departement = $departement;

        return $this;
    }
}
