<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $searchForm */
/** @var \PsrLib\ORM\Entity\Ferme[] $fermes */
$currentUser = get_current_connected_user();
/** @var int $gps */
/** @var mixed $map */
?>

<h3>Gestion des fermes</h3>

<form method="GET" action="/ferme" class="js-form-search">

    <div class="well">

        <div class="row">

            <div class="col-md-3">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Région</button>
		    </span>
                    <?= render_form_select_widget($searchForm['region']); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Département</button>
		    </span>
                    <?= render_form_select_widget($searchForm['departement']); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Réseau</button>
		    </span>
                    <?= render_form_select_widget($searchForm['reseau']); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Type de production</button>
		    </span>
                    <?= render_form_select_widget($searchForm['typeProduction']); ?>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Mot clé</button>
		    </span>
                    <?= render_form_text_widget($searchForm['keyword']); ?>
                </div>
            </div>

            <div class="form-group col-md-3 pull-right" style="margin-top:5px;">
                <?= render_form_checkbox_widget($searchForm['carto']); ?>
            </div>
	</div>
	
        <div class="row text-right">
	    <a href="/ferme?reset_search" class="btn btn-primary" style="margin-right:5px;"><i class="glyphicon glyphicon-refresh"></i> Réinitialiser les critères de recherche</a>
	    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Générer la liste</button>
	</div>

    </div><!-- well -->

</form>

<a href="/ferme/informations_generales_creation" class="btn btn-success"><i
            class="glyphicon glyphicon-plus"></i> Créer une nouvelle ferme</a>

<?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DOWNLOAD)): ?>
    <a href="<?= site_url('/ferme/telecharger_ferme_complet?').http_build_query($this->input->get()); ?>" class="btn btn-primary"><i
            class="glyphicon glyphicon-download"></i> Télécharger</a>
<?php endif; ?>

<?php if (null !== $map): ?>
    <?php if ($gps > 0): ?>
        <?= $map['js']; ?>
        <div class="alert alert-success"><?= $gps; ?> coordonnées GPS disponibles</div>
        <div id="map" style="width:100%; height:500px"></div>
        <br/>
    <?php else: ?>
        <div class="alert alert-success">Aucunes coordonnées GPS disponibles</div>
    <?php endif; ?>
<?php endif; ?>


<?php $fermesCount = count($fermes);
if ($fermesCount > 0) {
    if (1 === $fermesCount) {
        echo '<div class="alert alert-success">1 résultat pour cette recherche :</div>';
    } else {
        echo '<div class="alert alert-success">'.$fermesCount.' résultats pour cette recherche :</div>';
    }
} else {
    echo '<div class="alert alert-warning">Aucun résultat pour cette recherche</div>';
}

include 'display_all_list.php';
