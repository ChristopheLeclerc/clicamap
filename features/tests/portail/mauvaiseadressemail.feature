# language: fr
Fonctionnalité: Connexion mauvaise adresse mail et adresse mail non valide

  Plan du Scénario: Me connecter à l'application
    Etant donné que je suis sur "/"
    Quand je sélectionne "amapien" depuis "statut"
    Et remplis le texte suivant:
      | email    | <email>   |
      | password | P@ssw0rd! |
      | statut   | <statut>  |
    Et je presse "Connexion"
    Alors je devrais voir "<message>"

    Exemples:
      | statut       | email           | message                                                                |
      | amapien      | echec@echec.com | L'adresse email n'existe pas dans la base de donnée des amapiens.      |
      | amapien      | echec@echec.com | L'adresse email n'existe pas dans la base de donnée des amapiens.      |
      | amapien      | echec@echec.com | L'adresse email n'existe pas dans la base de donnée des amapiens.      |
      | amapien      | echec@echec.com | L'adresse email n'existe pas dans la base de donnée des amapiens.      |
      | paysan       | echec@echec.com | L'adresse email n'existe pas dans la base de donnée des paysans.       |
      | regroupement | echec@echec.com | L'adresse email n'existe pas dans la base de donnée des regroupements. |

  @database
  Plan du Scénario: Me connecter à l'application
    Etant donné que je suis sur "/"
    Quand je sélectionne "amapien" depuis "statut"
    Et remplis le texte suivant:
      | email    | <email>   |
      | password | P@ssw0rd! |
      | statut   | <statut>  |
    Et je presse "Connexion"
    Alors je devrais voir "<message>"

    Exemples:
      | statut       | email | message                   |
      | amapien      | echec | L'Email n'est pas valide. |
      | amapien      | echec | L'Email n'est pas valide. |
      | amapien      | echec | L'Email n'est pas valide. |
      | amapien      | echec | L'Email n'est pas valide. |
      | paysan       | echec | L'Email n'est pas valide. |
      | regroupement | echec | L'Email n'est pas valide. |
