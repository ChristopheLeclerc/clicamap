<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class FermeRegroupementCheckListTest extends AbstractSecurityTest
{
    public function testListAsPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST));
    }

    public function testListAsAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST));
    }

    public function testListAsAmapienNonAdmin()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST));
    }

    public function testListAsAmapienAdminRegion()
    {
        $amapien = $this->prophesize(Amapien::class);
        $amapien->isAdminRegion()->willReturn(true);
        $amapien->isSuperAdmin()->willReturn(false);

        $this->mockAuthenticatedUser($amapien->reveal());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST));
    }

    public function testListAsAmapienSuperAdmin()
    {
        $amapien = $this->prophesize(Amapien::class);
        $amapien->isAdminRegion()->willReturn(false);
        $amapien->isSuperAdmin()->willReturn(true);

        $this->mockAuthenticatedUser($amapien->reveal());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST));
    }
}
