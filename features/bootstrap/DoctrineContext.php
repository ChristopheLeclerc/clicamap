<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;

class DoctrineContext implements Context
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    private static $envLoaded = false;

    public function __construct()
    {
        $this->em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(\Doctrine\ORM\EntityManagerInterface::class);
    }

    /**
     * @AfterScenario
     */
    public static function cleanEm(AfterScenarioScope $scope)
    {
        if ($scope->getScenario()->hasTag('database')) {
            \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(\Doctrine\ORM\EntityManagerInterface::class)->clear();
        }
    }

    public static function loadDb()
    {
        if (!self::$envLoaded) {
            // Load environment for doctrine config
            $dovenv = new \Symfony\Component\Dotenv\Dotenv(true);
            $dovenv->load(__DIR__.'/../../.env.test');
            self::$envLoaded = true;
        }
        \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(\PsrLib\Services\FixtureLoader::class)->loadFixtures();
    }

    /**
     * @BeforeSuite
     */
    public static function initDbOnStartup()
    {
        // Wait for Db started
        exec('/bin/bash -c "while ! echo exit | curl -o /dev/null -s  http://localhost; do sleep 1; done"');
        self::loadDb();
    }

    /**
     * @AfterScenario
     */
    public static function resetDbAfterScenario(AfterScenarioScope $scope)
    {
        if ($scope->getScenario()->hasTag('database')) {
            self::loadDb();
        }
    }

    /**
     * @Given le paysan :paysan n'a pas de ferme associée
     */
    public function lePaysanNaPasDeFermeAssocie(string $paysanEmail)
    {
        /** @var null|\PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->findOneBy([
                'email' => $paysanEmail,
            ])
        ;
        if (null === $paysan) {
            throw new \RuntimeException($paysanEmail.' not found');
        }

        $paysan->setFerme(null);
        $this->em->flush();
    }

    /**
     * @Given le contrat :contratName possede la propriete :propriete a :valeur
     */
    public function leContratPossedeLaProprieteA(string $contratName, string $propriete, string $valeur)
    {
        $pa = \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor();

        $contrat = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->findOneBy([
                'nom' => $contratName,
            ])
        ;
        if (null === $contrat) {
            throw new \RuntimeException($contratName.' not found');
        }

        $pa->setValue($contrat, $propriete, $valeur);
        $this->em->flush();
    }

    /**
     * @Given j'ajoute le lieu de livraison :llNom a l'AMAP :amapEmail
     */
    public function ajouterLeLieuDeLivraisonALAMAP(string $llNom, string $amapEmail)
    {
        $amap = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amap::class)
            ->findOneBy([
                'email' => $amapEmail,
            ])
        ;
        if (null === $amap) {
            throw new \RuntimeException($amapEmail.' not found');
        }

        $ll = new \PsrLib\ORM\Entity\AmapLivraisonLieu();
        $ll->setNom($llNom);
        $ll->setAmap($amap);
        $this->em->persist($ll);
        $this->em->flush();
    }

    /**
     * @Given la ferme :ferme est associées au regroupement :regroupement
     */
    public function laFermeEstAssocieeAuRegroupement(string $fermeNom, string $regroupementEmail)
    {
        /** @var null|\PsrLib\ORM\Entity\FermeRegroupement $regroupement */
        $regroupement = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\FermeRegroupement::class)
            ->findOneBy([
                'email' => $regroupementEmail,
            ])
        ;
        if (null === $regroupementEmail) {
            throw new \RuntimeException($regroupementEmail.' not found');
        }

        /** @var null|\PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->findOneBy([
                'nom' => $fermeNom,
            ])
        ;
        if (null === $fermeNom) {
            throw new \RuntimeException($fermeNom.' not found');
        }

        $ferme->setRegroupement($regroupement);
        $this->em->flush();
    }
}
