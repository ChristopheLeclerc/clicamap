<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PsrLib\DTO\SearchFermeState;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\TypeProduction;

class ExcelGeneratorExportFerme
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TypeProduction[]
     */
    private $typeProductions;

    /**
     * @var ?int
     */
    private $nbPaysanMax;

    /**
     * @var ?int
     */
    private $fermeAmapNbMax;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function genererExportFermeComplet(SearchFermeState $searchFermeState): string
    {
        /** @var Ferme[] $fermes */
        $fermes = $this
            ->em
            ->getRepository(Ferme::class)
            ->search($searchFermeState)
        ;
        $this->initCommonVariables($fermes);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Filters
        $sheet->setCellValue('A1', 'Région '.$searchFermeState->getRegion());
        $sheet->setCellValue(
            'A2',
            sprintf('%s résultats (Extrait le %s)', count($fermes), date_format_french(new Carbon()))
        );
        $sheet->getStyle('A1:A2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        // Header
        $this->genererExportFermeCompletHeader($sheet);

        $line = 5;
        foreach ($fermes as $ferme) {
            $this->genererExportFermeCompletLigne($sheet, $line, $ferme);
            ++$line;
        }

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportAmapComplet_');
        $objWriter = new Xlsx($spreadsheet);
        $objWriter->save($outputFileName);

        return $outputFileName;
    }

    /**
     * @param Ferme[] $fermes
     */
    private function initCommonVariables($fermes): void
    {
        $this->nbPaysanMax = 0;
        foreach ($fermes as $ferme) {
            $fermePaysansCount = $ferme->getPaysans()->count();
            if ($fermePaysansCount > $this->nbPaysanMax) {
                $this->nbPaysanMax = $fermePaysansCount;
            }
        }

        $this->typeProductions = $this
            ->em
            ->getRepository(TypeProduction::class)
            ->findAllNoParent()
        ;

        $this->fermeAmapNbMax = 0;
        foreach ($fermes as $ferme) {
            $nbAmapiensRef = $ferme->getAmapienRefs()->count();
            if ($this->fermeAmapNbMax < $nbAmapiensRef) {
                $this->fermeAmapNbMax = $nbAmapiensRef;
            }
        }
    }

    private function genererExportFermeCompletHeader(Worksheet $sheet): void
    {
        $borderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'alignment' => [
                'wrapText' => true,
            ],
        ];
        $borderStyleCentered = array_merge_recursive($borderStyle, [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ]);
        $headerMap = [
            'Ferme : nom',
            'Ferme : adresse',
            'Ferme : code postal',
            'Ferme : ville',
            'Adhérent année',
            'SIRET',
            'Surface de la ferme (ha)',
            'Nombre d\'UTA',
            'Certification',
            'site internet',
        ];

        for ($i = 0; $i < $this->nbPaysanMax; ++$i) {
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $headerMap = array_merge($headerMap, [
                'Nom & Prénom du paysan',
                'Nom & prénom Conjoint',
                'Email',
                'Téléphone 1',
                'Téléphone 2',
                'Paysan : adresse',
                'Paysan : code postal',
                'Paysan : ville',
                'Date d\'installation',
                'Année commercialisation en AMAP',
                'Cagnotte solidaire',
            ]);
        }

        $column = 10 + $this->nbPaysanMax * 11 + 1;
        $sheet
            ->getCellByColumnAndRow($column, 3)
            ->setValue('Types de production')
        ;
        $sheet->mergeCellsByColumnAndRow(
            $column,
            3,
            $column + count($this->typeProductions) - 1,
            3
        );
        $sheet
            ->getStyleByColumnAndRow($column, 3)
            ->applyFromArray($borderStyleCentered)
        ;

        $headerMap = array_merge(
            $headerMap,
            array_map(function (TypeProduction $tp) {
                return $tp->getNomComplet();
            }, $this->typeProductions)
        );

        $headerMap = array_merge($headerMap, [
            'AMAP livrée',
            'nom & prénom du/des Référents',
            'adresse mail du/des Référents',
        ]);

        $column = 1;
        foreach ($headerMap as $header) {
            $sheet
                ->getCellByColumnAndRow($column, 4, true)
                ->setValue($header)
            ;
            $sheet
                ->getStyleByColumnAndRow($column, 4)
                ->applyFromArray($borderStyle)
            ;
            ++$column;
        }
        $sheet->getRowDimension(4, true)->setRowHeight(100);
    }

    private function genererExportFermeCompletLigne(Worksheet $sheet, int $line, Ferme $ferme): void
    {
        $valueMap = [
            $ferme->getNom(),
            $ferme->getLibAdr(),
            null === $ferme->getVille() ? '' : $ferme->getVille()->getCpString(),
            null === $ferme->getVille() ? '' : $ferme->getVille()->getNom(),
            implode(';', $ferme->getAnneeAdhesionsOrdered()),
            $ferme->getSiret(),
            $ferme->getSurface(),
            $ferme->getUta(),
            array_search($ferme->getCertification(), Ferme::AVAILABLE_CERTIFICATIONS, true),
            $ferme->getUrl(),
        ];

        /** @var Paysan[] $fermePaysans */
        $fermePaysans = $ferme->getPaysans()->toArray();
        for ($i = 0; $i < $this->nbPaysanMax; ++$i) {
            $paysan = $fermePaysans[$i] ?? null;
            if (null === $paysan) {
                /** @noinspection SlowArrayOperationsInLoopInspection */
                $valueMap = array_merge($valueMap, [
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                ]);
            } else {
                $valueMap[] = $paysan->getNomCompletInverse();
                $valueMap[] = $paysan->getConjointNom().' '.$paysan->getConjointPrenom();
                $valueMap[] = $paysan->getEmail();
                $valueMap[] = $paysan->getNumTel1();
                $valueMap[] = $paysan->getNumTel2();
                $valueMap[] = $paysan->getLibAdr();
                $valueMap[] = null === $paysan->getVille() ? null : $paysan->getVille()->getCpString();
                $valueMap[] = null === $paysan->getVille() ? null : $paysan->getVille()->getNom();
                $valueMap[] = date_format_french($paysan->getDateInstallation());
                $valueMap[] = $paysan->getAnneeDebCommercialisationAmap();
                $valueMap[] = (null !== $paysan->getCadreReseau() && $paysan->getCadreReseau()->isPayCrCagnSol()) ? 'O' : 'N';
            }
        }

        foreach ($this->typeProductions as $typeProduction) {
            $tpFoundInFerme = '';
            foreach ($ferme->getTypeProductionFromProduits() as $tpp) {
                if ($tpp === $typeProduction) {
                    $tpFoundInFerme = 'O';

                    continue;
                }
            }
            $valueMap[] = $tpFoundInFerme;
        }

        $fermeRefs = $ferme->getAmapienRefs()->toArray();
        $valueMap[] = implode(';', array_map(function (Amapien $amapien) {
            return (string) $amapien->getAmap();
        }, $fermeRefs));
        $valueMap[] = implode(';', array_map(function (Amapien $amapien) {
            return (string) $amapien->getNomCompletInverse();
        }, $fermeRefs));
        $valueMap[] = implode(';', array_map(function (Amapien $amapien) {
            return (string) $amapien->getEmail();
        }, $fermeRefs));

        $column = 1;
        foreach ($valueMap as $value) {
            $sheet
                ->getCellByColumnAndRow($column, $line, true)
                ->setValue($value)
            ;
            ++$column;
        }

        // Add specific cell style
        $sheet
            ->getCellByColumnAndRow(5, $line)
            ->getStyle()
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_TEXT)
        ;
    }
}
