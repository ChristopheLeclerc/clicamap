<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * As codeigniter templates does not filter XSS on print, we do it on input.
 * Better than nothing but not perfect security....
 */
class CleanInput
{
    public const INPUT_CONFIG_MAP = [
        'evt_txt' => \PsrLib\Services\Purifier::CONFIG_SUMMERNOTE,
        'evt_nom' => \PsrLib\Services\Purifier::CONFIG_SUMMERNOTE_SINGLELINE,
    ];

    /**
     * @var \PsrLib\Services\Purifier
     */
    private $purifier;

    public function __construct()
    {
        $this->purifier = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\PsrLib\Services\Purifier::class)
        ;
    }

    public function cleanAllInput()
    {
        if (is_array($_POST)) {
            $this->cleanArray($_POST);
        }

        if (is_array($_GET)) {
            $this->cleanArray($_GET);
        }
    }

    private function cleanArray(array &$array)
    {
        foreach ($array as $key => $item) {
            if (is_array($item)) {
                $this->cleanArray($array[$key]);
            } else {
                $array[$key] = $this->purifier->purify(
                    $item,
                    self::INPUT_CONFIG_MAP[$key] ?? \PsrLib\Services\Purifier::CONFIG_DEFAULT
                );
            }
        }
    }
}
