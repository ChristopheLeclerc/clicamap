<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use PsrLib\ORM\Entity\Amap;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class AmapTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testSiretLuhnInvalid()
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'siret', '11111111111111');
        self::assertCount(1, $violations);
        self::assertSame('Le SIRET est invalide.', $violations[0]->getMessage());
    }

    public function testSiretLuhnValid()
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'siret', '44835745900024');
        self::assertEmpty($violations);
    }

    /**
     * @dataProvider rnaInvalidProvider
     */
    public function testRnaInvalid(string $rna)
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'rna', $rna);
        self::assertCount(1, $violations);
        self::assertSame('Le numéro RNA doit être composé de W suivi de 9 chiffres.', $violations[0]->getMessage());
    }

    public function rnaInvalidProvider()
    {
        return [
            ['123'],
            ['W12345678'],
            ['W1234567891'],
            ['w123456789'],
            ['W12345678a'],
        ];
    }

    public function testRnaValid()
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'rna', 'W123456789');
        self::assertEmpty($violations);
    }
}
