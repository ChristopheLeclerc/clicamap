<?php

namespace Test\Twig;

use PHPUnit\Framework\TestCase;
use PsrLib\Twig\StringExtension;

/**
 * @internal
 * @coversNothing
 */
class StringExtensionTest extends TestCase
{
    /**
     * @var StringExtension
     */
    private $extension;

    public function setUp()
    {
        $this->extension = new StringExtension();
    }

    public function testUcfirst()
    {
        $res = $this->extension->ucfirst('test string');

        self::assertSame('Test string', $res);
    }

    public function testUcfirstNull()
    {
        $res = $this->extension->ucfirst(null);

        self::assertSame('', $res);
    }
}
