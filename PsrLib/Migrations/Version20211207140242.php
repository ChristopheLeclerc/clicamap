<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Carbon\Carbon;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Repository\AmapienRepository;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211207140242 extends AbstractMigration
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var AmapienRepository
     */
    private $amapienRepo;

    /**
     * @var array
     */
    private $existingData;

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        parent::preUp($schema);
        $this->em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        $this->amapienRepo = $this->em->getRepository(Amapien::class);
        $this->existingData = $this
            ->connection
            ->executeQuery('
            SELECT pl.*, lh.* FROM ak_amap_livraison_horaire_plage_horaire pl
                JOIN ak_amap_livraison_horaire lh ON pl.amap_liv_hor_ph_fk_amap_liv_hor_id = lh.amap_liv_hor_id
            ;')
            ->fetchAll()
        ;
    }

    public function up(Schema $schema): void
    {
        foreach ($this->existingData as $row) {
            $this->buildFromRow($row);
        }

        $this->em->flush();

        $this->addSql('DROP TABLE ak_amap_livraison_horaire_plage_horaire');
        $this->addSql('DROP TABLE ak_amap_livraison_horaire_plage_horaire_inscription');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    private function buildFromRow($row)
    {
        $startOffset = [
            'lundi' => 3,
            'mardi' => 4,
            'mercredi' => 5,
            'jeudi' => 6,
            'vendredi' => 0,
            'samedi' => 1,
            'dimanche' => 2,
        ];
        $distributions = [];

        $start = Carbon::create(2021, 01, 01);
        $start->addDays($startOffset[$row['amap_liv_hor_jour']]);
        $end = Carbon::create(2022, 12, 31);

        $livLieu = $this
            ->em
            ->getReference(AmapLivraisonLieu::class, $row['amap_liv_hor_fk_amap_liv_lieu_id'])
        ;
        if (null === $livLieu) {
            throw new \Exception('Unable to find ll '.$row['amap_liv_hor_fk_amap_liv_lieu_id']);
        }

        [$debutHeure, $debutMinute] = explode(':', $row['amap_liv_hor_ph_heure_debut']);
        [$finHeure, $finMinute] = explode(':', $row['amap_liv_hor_ph_heure_fin']);

        // Generate distributions
        while ($start->lte($end)) {
            if ($this->isDateInSeason($start, $row['amap_liv_hor_saison'])) {
                $distribution = new AmapDistribution(
                    AmapDistributionDetail::create(
                        new Time((int) $debutHeure, (int) $debutMinute),
                        new Time((int) $finHeure, (int) $finMinute),
                        (int) $row['amap_liv_hor_ph_nbr_amapiens'],
                        'Reprise des inscriptions'
                    ),
                    $livLieu,
                    $start->clone()
                );
                $this->em->persist($distribution);
                $distributions[] = $distribution;
            }

            $start->addWeek();
        }

        // Add inscriptions
        $existingInscriptions = $this
            ->connection
            ->executeQuery('
            SELECT * FROM ak_amap_livraison_horaire_plage_horaire_inscription
                WHERE amap_liv_hor_ph_inscr_fk_amap_liv_hor_ph_id = :id AND amap_liv_hor_ph_inscr_date >= "2021-01-01"
            ;', [
                'id' => $row['amap_liv_hor_ph_id'],
            ])
            ->fetchAll()
        ;
        foreach ($existingInscriptions as $existingInscription) {
            $date = Carbon::createFromFormat('Y-m-d', $existingInscription['amap_liv_hor_ph_inscr_date'])->startOfDay();

            $amapien = $this
                ->amapienRepo
                ->find($existingInscription['amap_liv_hor_ph_inscr_fk_amapien_id'])
            ;
            if (null === $amapien) {
                continue;
            }

            $distributionTarget = array_values(array_filter($distributions, function (AmapDistribution $distribution) use ($date) {
                return $distribution->getDate()->eq($date);
            }));

            if (1 !== count($distributionTarget)) {
                throw new \Exception('Unable to find target');
            }
            $distributionTarget[0]->addAmapien($amapien);
        }
    }

    private function isDateInSeason(Carbon $date, string $season)
    {
        $summerStart = Carbon::create($date->year, 3, 21);
        $summerEnd = Carbon::create($date->year, 9, 21);

        $isSummer = $date->gte($summerStart) && $date->lt($summerEnd);

        if ('ete' === $season) {
            return $isSummer;
        }

        return !$isSummer;
    }
}
