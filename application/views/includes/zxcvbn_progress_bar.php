<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<div class="progress">
    <div id="progress-password-strength" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="5" style="width: 0;">
    </div>
</div>

<script>
  // Manage password strength
  $('input[name=<?= $passwordFieldName; ?>]').on('keyup', $.debounce(250, function (e) {
    $.ajax({
      method: 'POST',
      data: {
        zxcvbn_password: e.target.value
      },
      url: "<?= $url; ?>",
      success: function (data) {
        var strength = data.strength;
        var color = 'danger';
        if(strength === 2) {
          color = 'warning';
        } else if(strength === 3) {
          color = 'info';
        } else if(strength === 4) {
          color = 'success';
        }

        $('#progress-password-strength')
          .attr('aria-valuenow', strength+1)
          .css('width', (strength+1)/5*100 + '%')
          .removeClass()
          .addClass('progress-bar progress-bar-' + color)
          .html(strength + '/4')
      }
    })

  }))
    .trigger('keyup') // Trigger to catch bad form submission
  ;
</script>
