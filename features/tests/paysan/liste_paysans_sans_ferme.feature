#language: fr
Fonctionnalité: Lister les paysans sans ferme

  @database
  Scénario: Ne pas lister les paysans sans ferme par défaut
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que le paysan "paysan@test.amap-aura.org" n'a pas de ferme associée
    Quand  je suis sur "/paysan"
    Et que je remplis "search_paysan[keyword]" avec "PAYSAN"
    Et que je presse "Générer la liste"
    Alors  je ne devrais pas voir "paysan@test.amap-aura.org"
    Et     je devrais voir "paysan2@test.amap-aura.org"

  @database
  Scénario: Lister les paysan sans ferme avec le selecteur "Paysans sans ferme"
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que le paysan "paysan@test.amap-aura.org" n'a pas de ferme associée
    Quand  je suis sur "/paysan"
    Et que je remplis "search_paysan[paysansSansFerme]" avec "1"
    Et que je presse "Générer la liste"
    Alors  je devrais voir "paysan@test.amap-aura.org"
    Et     je ne devrais pas voir "paysan2@test.amap-aura.org"
