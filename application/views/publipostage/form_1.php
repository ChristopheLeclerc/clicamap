<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
$currentUser = get_current_connected_user();
if (!($currentUser instanceof \PsrLib\ORM\Entity\Amapien)) {
    throw new \LogicException('Current user must be Amapien');
}
?>

<script type="text/javascript">

  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 4000);

  });

</script>

<?php
if (isset($envoi_erreur) || isset($envoi_ok)) {
    ?>

    <div class="modal fade" id="alerte">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">

                    <?php

                    if ($envoi_ok) {
                        echo '<p><i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> Le message a été envoyé avec succès !</p>';
                    } elseif ($envoi_erreur) {
                        echo '<p><i class="glyphicon glyphicon-remove" style="color:#9D4444;"></i> Le message n\'a pas pu être envoyé. Une adresse email n\'est sans doute pas valide.</p>';
                    } ?>

                </div>
            </div>
        </div>
    </div>

    <?php
}
?>

<h3>Choix des destinataires</h3>

<h5><a href="<?= site_url('publipostage'); ?>">Publipostage</a> <i class="glyphicon glyphicon-menu-right"></i>
    Choix des destinataires</h5>

<?php echo form_open_multipart('publipostage/form_1', 'id="form1"');
// VARIABLES CACHÉES
// ------------------------------------------------------------------------
if (set_value('titre')) {
    echo '<input type="hidden"  name="nom" value="'.set_value('titre').'" />';
}
if (set_value('txt')) {
    echo '<input type="hidden"  name="date" value="'.set_value('txt').'" />';
}
// ------------------------------------------------------------------------

// SESSION ADMIN
elseif ($currentUser->isAdmin()) {
    ?>

    <!-- COMMENT CONTACTER ? -->
    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-3">
                    <label for="etiquette" class="control-label">
                        <input type="radio" onclick="etape();" name="media" id="etiquette"
                               value="etiquette" <?= set_radio('media', 'etiquette'); ?> />
                        Étiquettes</label>
                </div>
                <div class="form-group col-md-3">
                    <label for="email" class="control-label">
                        <input type="radio" onclick="etape();" name="media" id="email"
                               value="email" <?= set_radio('media', 'email'); ?> />
                        Email</label>
                </div>
                <?= form_error('media'); ?>
                <div class="form-group col-md-3">
                    <label for="amap" class="control-label">
                        <input type="radio" onclick="etape();" name="qui" id="amap"
                               value="amap" <?= set_radio('qui', 'amap'); ?> />
                        AMAP</label>
                </div>
                <div class="form-group col-md-3">
                    <label for="paysans" class="control-label">
                        <input type="radio" onclick="etape();" name="qui" id="paysans"
                               value="paysans" <?= set_radio('qui', 'paysans'); ?> />
                        Paysans</label>
                    <?= form_error('qui'); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- EN RECHERCHE DE PARTENARIAT ET ADHÉSION -->
    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6">
                    <label for="en_recherche_partenariat" class="control-label">
                        <input type="checkbox" name="en_recherche_partenariat" id="en_recherche_partenariat"
                               value="1" <?= set_checkbox('en_recherche_partenariat', '1'); ?> />
                        En recherche de partenariats</label>
                    <?= form_error('en_recherche_partenariat'); ?>
                </div>
                <div class="form-group col-md-3">
                    <label for="adh_oui" class="control-label">
                        <input type="checkbox" onclick="etape();" name="adh_oui" id="adh_oui"
                               value="1" <?= set_checkbox('adh_oui', '1'); ?> />
                        Adhérent (les 2 dernières années)</label>
                    <?= form_error('adh_oui'); ?>
                </div>
                <div class="form-group col-md-3">
                    <label for="adh_non" class="control-label">
                        <input type="checkbox" onclick="etape();" name="adh_non" id="adh_non"
                               value="1" <?= set_checkbox('adh_non', '1'); ?> />
                        Non adhérent</label>
                    <?= form_error('adh_non'); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- RÉGIONS, DÉPARTEMENTS ET RÉSEAUX -->
    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <?php
                // RÉGIONS
                if ($currentUser->isSuperAdmin()) {
                    echo '<div class="form-group col-md-4">';
                    echo '<p class="pull-right"><button type="button" onclick="reg_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="reg_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>'; ?>
                <h4>Régions</h4>
                <input type="hidden" name="reg_clear" id="reg_clear" value="0"/>
                <div id="regions"
                     style="height:150px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                    <?php
                    foreach ($region_all as $region) {
                        echo '<div class="checkbox"><input type="checkbox" name="reg[]" id="reg_'.$region->getId().'" value="'.$region->getId().'" '.set_checkbox('reg[]', $region->getId()).' onclick="etape();" /><label for="reg_'.$region->getId().'" class="control-label">'.$region.'</label></div>';
                    } ?>
                </div>
            </div>
            <?php
                }

    // DÉPARTEMENTS
    if ($currentUser->isSuperAdmin() || $currentUser->isAdminRegion() || $currentUser->isAdminDepartment()) {
        if (!$currentUser->isSuperAdmin() && ($currentUser->isAdminRegion() || $currentUser->isAdminDepartment())) {
            echo '<div class="form-group col-md-6">';
        } else {
            echo '<div class="form-group col-md-4">';
        }
        $n = 0;
        foreach ($departement_all as $departement) {
            ++$n;
        }
        // Btn grisé
        if (0 == $n) {
            echo '<p class="pull-right"><button type="button" class="btn btn-default btn-xs disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-xs disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
        } else {
            echo '<p class="pull-right"><button type="button" onclick="dep_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="dep_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
        } ?>
            <h4>Départements</h4>
            <input type="hidden" name="dep_clear" id="dep_clear" value="0"/>
            <div id="departements"
                 style="height:150px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                <?php
                foreach ($departement_all as $departement) {
                    echo '<div class="checkbox"><input type="checkbox" name="dep[]" id="dep_'.$departement->getId().'" value="'.$departement->getId().'" '.set_checkbox('dep[]', $departement->getId()).' onclick="etape();" /><label for="dep_'.$departement->getId().'" class="control-label">'.$departement.'</label></div>';
                } ?>
            </div>
        </div>
        <?php
    }

    // RÉSEAUX
    if ($currentUser->isSuperAdmin()) {
        echo '<div class="form-group col-md-4">';
    } elseif ($currentUser->isAdminDepartment()) {
        echo '<div class="form-group col-md-12">';
    } else {
        echo '<div class="form-group col-md-6">';
    }
    $n = 0;
    foreach ($dep_res as $reseau) {
        ++$n;
    }
    // Btn grisé
    if (0 == $n) {
        echo '<p class="pull-right"><button type="button" class="btn btn-default btn-xs disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-xs disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } else {
        echo '<p class="pull-right"><button type="button" onclick="res_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="res_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } ?>
        <h4>Réseaux</h4>
        <input type="hidden" name="res_clear" id="res_clear" value="0"/>
        <div id="reseaux" style="height:150px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
            <?php
            $reseaux_id = []; // Pour dédoublonner
    foreach ($dep_res as $reseau) {
        if (!in_array($reseau->getId(), $reseaux_id)) {
            echo '<div class="checkbox"><input type="checkbox" name="res[]" id="res_'.$reseau->getId().'" value="'.$reseau->getId().'" '.set_checkbox('res[]', $reseau->getId()).' onclick="etape();" /><label for="res_'.$reseau->getId().'" class="control-label">'.$reseau.'</label></div>';
        }
        $reseaux_id[] = $reseau->getId();
    } ?>
        </div>
    </div>
    </div>
    </div>
    </div>

    <!-- PAYSAN : CERTIFICATION ET SES FILIÈRES -->
    <?php
    if ('paysans' == set_value('qui')) {
        ?>
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-12">
                    <!-- ------------------------------------------------------------- -->
                    <!-- TYPES DE PRODUCTION -->
                    <div class="form-group col-md-6">
                        <?php
                        // Btn grisé
                        if (!$type_prod) {
                            echo '<p class="pull-right"><button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
                        } else {
                            echo '<p class="pull-right"><button type="button" onclick="tp_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="tp_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
                        } ?>
                        <h4>Filières du paysan</h4>
                        <input type="hidden" name="tp_clear" id="tp_clear" value="0"/>
                        <div id="tp_all"
                             style="height:150px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                            <?php
        foreach ($type_prod as $tp) {
            $tp_nom = $tp->getNomComplet();
            echo '<div class="checkbox"><input type="checkbox" onclick="etape();" name="tp[]" id="tp_'.$tp->getSlug().'" value="'.$tp->getSlug().'" '.set_checkbox('tp[]', $tp->getSlug()).' /><label for="tp_'.$tp->getSlug().'" class="control-label">'.$tp_nom.'</label></div>';
        } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php
    }
}
?>

<div class="form-group">
    <?php
    if (set_value('media') && (set_value('reg') || set_value('dep') || set_value('res'))) {
        if ('etiquette' == set_value('media')) {
            $mot = 'Valider';
        } else {
            $mot = 'Valider';
        }

        if ('paysans' == set_value('qui')) {
            if (set_value('tp')) {
                if ('etiquette' == set_value('media')) {
                    echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;" onclick="valide()"/>';
                } elseif ('email' == set_value('media')) {
                    echo '<div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog"><div class="modal-content"><div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à toutes les AMAP de cette liste ?</h4>
            </div>
            <div class="modal-body"><h5>Pour envoyer un email à toutes ces AMAP, vous devez :</h5>
            <ul><li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit + Copier sur la zone bleue ci dessous</li><li>Ouvrir votre outil favori pour l\'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
            <li>Faire nouveau message</li><li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit + Coller dans la liste des destinataires du message.</li></ul>
            <form><div class="form-group"><textarea class="form-control" rows="5">';
                    echo $emails;
                    echo '</textarea></div></form></div></div></div></div>';

                    echo '<a href="mailto:?to='.$emails.'"><button type="button" class="btn btn-success pull-right"style=" margin-left:5px;">'.$mot.'</button></a>';

                    echo '<a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal_email" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>';
                }
            } else {
                echo '<input type="button" value="'.$mot.'" class="pull-right btn btn-default disabled" style="margin-left:5px;"/>';
            }
        } elseif ('amap' == set_value('qui')) {
            if ('etiquette' == set_value('media')) {
                echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;" onclick="valide()"/>';
            } elseif ('email' == set_value('media')) {
                echo '<div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog"><div class="modal-content"><div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à toutes les AMAP de cette liste ?</h4>
          </div>
          <div class="modal-body"><h5>Pour envoyer un email à toutes ces AMAP, vous devez :</h5>
          <ul><li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit + Copier sur la zone bleue ci dessous</li><li>Ouvrir votre outil favori pour l\'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
          <li>Faire nouveau message</li><li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit + Coller dans la liste des destinataires du message.</li></ul>
          <form><div class="form-group"><textarea class="form-control" rows="5">';
                echo $emails;
                echo '</textarea></div></form></div></div></div></div>';

                echo '<a href="mailto:?to='.$emails.'"><button type="button" class="btn btn-success pull-right"style=" margin-left:5px;">'.$mot.'</button></a>';

                echo '<a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal_email" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>';
            }
        } else {
            echo '<input type="button" value="'.$mot.'" class="pull-right btn btn-default disabled" style="margin-left:5px;"/>';
        }
    } else {
        echo '<input type="button" value="Valider" class="pull-right btn btn-default disabled" style="margin-left:5px;"/>';
    }
    ?>

    <script type="text/javascript">
      function etape() {
        $('#etape_validee').val(0);
        document.getElementById("form1").submit();
      }

      function valide() {
        $('#etape_validee').val(1);
        document.getElementById("form1").submit();
      }
    </script>
</div>
<input type="hidden" name="etape_validee" id="etape_validee" value="0"/>
</form>
