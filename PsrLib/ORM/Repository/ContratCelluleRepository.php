<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @extends EntityRepository<ContratCellule>
 */
class ContratCelluleRepository extends EntityRepository
{
    /**
     * @param ContratCellule[] $commandes
     *
     * @return float
     */
    public static function findCelluleCount(array $commandes, ModeleContratDate $date, ModeleContratProduit $produit)
    {
        foreach ($commandes as $commande) {
            if ($commande->getModeleContratProduit()->getId() === $produit->getId()
                && $commande->getModeleContratDate()->getId() === $date->getId()) {
                return $commande->getQuantite();
            }
        }

        return 0;
    }

    /**
     * @param ContratCellule[] $commandes
     */
    public static function countNbLivraisonsByDate(array $commandes, ModeleContratDate $date): float
    {
        $nbLiv = 0.0;
        foreach ($commandes as $commande) {
            if ((int) $commande->getModeleContratDate()->getId() === (int) $date->getId()) {
                $nbLiv += (float) $commande->getQuantite();
            }
        }

        return $nbLiv;
    }

    /**
     * @return ContratCellule[]
     */
    public function getFromAmapDate(Amap $amap, string $date)
    {
        return $this
            ->createQueryBuilder('cc')
            ->leftJoin('cc.contrat', 'contrat')
            ->leftJoin('contrat.modeleContrat', 'mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->leftJoin('cc.modeleContratDate', 'date')
            ->where('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('date.dateLivraison = :dateLivraison')
            ->setParameter('dateLivraison', $date)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ContratCellule[]
     */
    public function getFromFermeDate(Ferme $ferme, string $date): array
    {
        return $this
            ->createQueryBuilder('cc')
            ->leftJoin('cc.contrat', 'contrat')
            ->addSelect('contrat')
            ->leftJoin('contrat.amapien', 'amapien')
            ->addSelect('amapien')
            ->leftJoin('contrat.modeleContrat', 'mc')
            ->addSelect('mc')
            ->leftJoin('cc.modeleContratDate', 'date')
            ->addSelect('date')
            ->leftJoin('cc.modeleContratProduit', 'produit')
            ->addSelect('produit')
            ->leftJoin('amapien.amapAsRefReseau', 'amap_as_ref_reseau')
            ->addSelect('amap_as_ref_reseau')
            ->leftJoin('amapien.amapAsRefReseauSec', 'amap_as_ref_reseau_sec')
            ->addSelect('amap_as_ref_reseau_sec')
            ->where('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->andWhere('date.dateLivraison = :dateLivraison')
            ->setParameter('dateLivraison', $date)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ContratCellule[]
     */
    public function getFromAmapienDate(Amapien $amapien, string $date)
    {
        return $this
            ->createQueryBuilder('cc')
            ->leftJoin('cc.contrat', 'contrat')
            ->leftJoin('cc.modeleContratDate', 'date')
            ->where('contrat.amapien = :amapien')
            ->setParameter('amapien', $amapien)
            ->andWhere('date.dateLivraison = :dateLivraison')
            ->setParameter('dateLivraison', $date)
            ->getQuery()
            ->getResult()
            ;
    }
}
