<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap[] $amaps */
$currentUser = get_current_connected_user();
?>

<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-inverse">
        <tr>
            <th>Nom</th>
            <th>CP, Ville de livraison</th>
            <th>Contact Public</th>
            <th class="text-right">Outils</th>
        </tr>
        </thead>

        <tbody>

        <?php foreach ($amaps as $amap): ?>
            <tr>
                <th scope="row">

                    <?php
                    if (strlen($amap->getNom()) > 45) {
                        echo substr(mb_strtoupper($amap->getNom()), 0, 45).'...';
                    } else {
                        echo mb_strtoupper($amap->getNom());
                    }
                    ?>
                </th>

                <td>
                    <?php
                    /** @var false|\PsrLib\ORM\Entity\AmapLivraisonLieu $ll */
                    $ll = $amap->getLivraisonLieux()->first();
                    if (false !== $ll) {
                        $ville = $ll->getVille();
                        if (strlen($ville->getNom()) > 25) {
                            echo $ville->getCpString().',&nbsp;'.substr(mb_strtoupper($ville->getNom()), 0, 25).'...';
                        } else {
                            echo $ville->getCpString().',&nbsp;'.strtoupper($ville->getNom());
                        }
                    }
                    ?>
                </td>

                <td>
                    <?php if (null !== $amap->getEmail()): ?>
                        <a
                            href="mailto:<?php $amap->getEmail(); ?>;"
                        >
                            <?= strlen($amap->getEmail()) > 30
                                ? substr(mb_strtolower($amap->getEmail()), 0, 30).'...'
                                : mb_strtolower($amap->getEmail());
                            ?>
                        </a>
                    <?php endif; ?>
                </td>

                <td class="text-right">

                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_STATE, $amap)): ?>
                        <?php
                        if (\PsrLib\ORM\Entity\Amap::ETAT_CREATION === $amap->getEtat()) {
                            $icon = 'wrench';
                        } elseif (\PsrLib\ORM\Entity\Amap::ETAT_ATTENTE == $amap->getEtat()) {
                            $icon = 'play';
                        } elseif (\PsrLib\ORM\Entity\Amap::ETAT_FONCIONNEMENT == $amap->getEtat()) {
                            $icon = 'pause';
                        }
                        ?>
                        <a
                            title="Changer l\'état de l\'AMAP"
                            href="#"
                            data-toggle="modal"
                            data-target="#modal_etat_<?= $amap->getId(); ?>">
                            <i
                                class="glyphicon glyphicon-<?= $icon; ?>"
                                data-toggle="tooltip"
                                title="Changer l\'état de l\'AMAP"></i>
                        </a>&nbsp;
                    <?php endif; ?>

                    <a
                        title="Détails de l'AMAP"
                        href="<?= site_url('amap/display/'.$amap->getId()); ?>">
                        <i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="Détails de l'AMAP"></i>
                    </a>&nbsp;

                    <a
                        title="Modifier l'AMAP"
                        href="#"
                        data-toggle="modal"
                        data-target="#modal_edit_<?= $amap->getId(); ?>">
                        <i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier l'AMAP"></i>
                    </a>&nbsp;

                    <a
                        title="Ajouter / Modifier les rôles de l'AMAP"
                        href="#"
                        data-toggle="modal"
                        data-target="#modal_edit_roles_<?= $amap->getId(); ?>">
                        <i class="glyphicon glyphicon-education" data-toggle="tooltip"
                           title="Ajouter / Modifier les rôles de l'AMAP"></i>
                    </a>&nbsp;

                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_SEND_PASSWORD, $amap)): ?>
                        <a
                            title="Envoyer un mot de passe à l'AMAP"
                            href="#"
                            data-toggle="modal"
                            data-target="#modal_key_<?= $amap->getId(); ?>">
                            <i class="glyphicon glyphicon-lock" data-toggle="tooltip"
                               title="Envoyer un mot de passe à l'AMAP"></i>
                        </a>&nbsp;
                    <?php endif; ?>

                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_CHANGE_STATE, $amap)): ?>
                        <a
                            title="Supprimer l'AMAP"
                            href="#"
                            data-toggle="modal"
                            data-target="#modal_remove_<?= $amap->getId(); ?>">
                            <i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer l\'AMAP"></i>
                        </a>&nbsp;
                    <?php endif; ?>


                    <!-- Modal mot de passe -->
                    <div class="modal fade text-left" id="modal_key_<?= $amap->getId(); ?>" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment envoyer un mot de
                                        passe à
                                        l'email public de l'AMAP <?= $amap->getEmail(); ?> ?</h5>
                                </div>
                                <div class="modal-body">
                                    <p>Cette action créera un mot de passe pour l'AMAP (même s'elle en a déjà un) qui
                                        lui
                                        sera
                                        envoyé par email ; de plus, l'AMAP sera activé.</p>
                                    <p class="text-right">
                                        <a href="<?= site_url('amap/password/'.$amap->getId()); ?>"
                                           class="btn btn-danger btn-sm">OUI</a>
                                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal état -->
                    <div class="modal fade text-left" id="modal_etat_<?= $amap->getId(); ?>" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Modifier l'état de
                                        "<?= $amap->getNom(); ?>
                                        "</h4>
                                </div>
                                <div class="modal-body">
                                    <h5>Quel état voulez-vous appliquez à l'AMAP ?</h5>
                                    <p>
                                        <?php

                                        if (\PsrLib\ORM\Entity\Amap::ETAT_FONCIONNEMENT === $amap->getEtat()) {
                                            echo '<a href="#" class="btn btn-primary" disabled >En création</a> ';
                                        } else {
                                            echo '<a href="'.site_url('amap/edit_etat/creation/'.$amap->getId()).'" class="btn btn-primary">En création</a> ';
                                        }

                                        if (\PsrLib\ORM\Entity\Amap::ETAT_FONCIONNEMENT === $amap->getEtat()) {
                                            echo '<a href="#" class="btn btn-success" disabled >En fonctionnement</a> ';
                                        } else {
                                            echo '<a href="'.site_url('amap/edit_etat/fonctionnement/'.$amap->getId()).'" class="btn btn-success">En fonctionnement</a> ';
                                        }

                                        if (\PsrLib\ORM\Entity\Amap::ETAT_ATTENTE === $amap->getEtat()) {
                                            echo '<a href="#" class="btn btn-warning" disabled >En attente</a> ';
                                        } else {
                                            echo '<a href="'.site_url('amap/edit_etat/attente/'.$amap->getId()).'" class="btn btn-warning">En attente</a> ';
                                        } ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal edit AMAP -->
                    <div class="modal fade text-left" id="modal_edit_<?= $amap->getId(); ?>" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Modifier "<?= $amap->getNom(); ?>"</h4>
                                </div>
                                <div class="modal-body">
                                    <h5>Veuillez indiquer ce que vous souhaitez modifier :</h5>
                                    <?php
                                    echo '<p><a href="'.site_url('amap/informations_generales_edition/'.$amap->getId()).'"><i class="glyphicon glyphicon-info-sign"></i> Les informations générales</a></p>';
                                    echo '<p><a href="'.site_url('amap/livraison_lieu/'.$amap->getId()).'"><i class="glyphicon glyphicon-flag"></i> Les informations de livraisons</a></p>';
                                    if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_SEARCH_PAYSAN)) {
                                        echo '<p><a href="'.site_url('amap/recherche_paysan').'"><i class="glyphicon glyphicon-apple"></i> Les produits proposés / recherchés</a></p>';
                                    } else {
                                        echo '<p><a href="'.site_url('amap/produits/'.$amap->getId()).'"><i class="glyphicon glyphicon-apple"></i> Les produits proposés / recherchés</a></p>';
                                    } ?>
                                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_MANAGE_ABS, $amap)): ?>
                                        <p>
                                            <a href="<?= site_url('amap_absence/liste/'.$amap->getId()); ?>">
                                                <i class="far fa-calendar-check" aria-hidden="true"></i> Le planning des
                                                absences
                                            </a>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal edit Rôles -->
                    <div class="modal fade text-left" id="modal_edit_roles_<?= $amap->getId(); ?>" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Modifier "<?= $amap->getNom(); ?>"</h4>
                                </div>
                                <div class="modal-body">
                                    <h5>Veuillez indiquer ce que vous souhaitez ajouter / modifier :</h5>
                                    <p><a href="<?= site_url('amap/collectif/'.$amap->getId()); ?>"> Le collectif de
                                            l'AMAP</a></p>
                                    <p><a href="<?= site_url('amap/gestionnaire_display/'.$amap->getId()); ?>">Le(s)
                                            correspondant(s) Clic'AMAP</a></p>
                                    <p><a href="<?= site_url('amap/referent_edition/'.$amap->getId()); ?>">Le
                                            correspondant réseau AMAP</a></p>
                                    <p><a href="<?= site_url('amap/referent_second_edition/'.$amap->getId()); ?>">Le
                                            second correspondant réseau AMAP</a></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal remove -->
                    <div class="modal fade text-left" id="modal_remove_<?= $amap->getId(); ?>" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer l'AMAP
                                        "<?= strtoupper($amap->getNom()); ?>" ?</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-danger"><strong>Attention !</strong> Cette action supprimera
                                        tous
                                        les comptes et contrats liés à l'AMAP.<br/>En supprimant un(e) amapien(ne), vous
                                        supprimerez aussi ses contrats, et tous ses comptes : son compte référent
                                        produit si
                                        il
                                        ou elle en possède un et de fait tous les contrats vierges et signés rattachés à
                                        ce
                                        compte ; ses comptes d'administrateur si il ou elle en possède etc.
                                    </div>
                                    <p class="text-right">
                                        <a href="<?= site_url('amap/remove/'.$amap->getId()); ?>"
                                           class="btn btn-danger btn-sm" title="Confirmer la suppression">OUI</a>
                                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
