<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
/** @var \PsrLib\ORM\Entity\ModeleContratDate $date_a_deplacer */
/** @var string $nouvelle_date */
/** @var \PsrLib\ORM\Entity\Amapien[] $mc_amapiens_deplacement */
echo '<h3>Confirmation avant déplacement</h3>';

echo '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.$mc->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Déplacer une date de livraison <i class="glyphicon glyphicon-menu-right"></i> Étape 3</h5>';

?>

<form method="POST" action="<?= site_url('contrat_signe/move_step_3/'.$mc->getId()); ?>">

    <input type="hidden" name="step_3" value="1"/>

    <?php
    echo '<input type="hidden" name="date_a_deplacer" value="'.$date_a_deplacer->getId().'" />';
    echo '<input type="hidden" name="nouvelle_date" value="'.$nouvelle_date.'" />';
    ?>

    <div class="row">
        <div class="form-group">
            <div class="col-md-12">

                <?php

                echo '<p class="alert alert-danger">La livraison du <strong>'.$date_a_deplacer->getDateLivraison()->format('Y-m-d').'</strong> va être déplacée au <strong>'.$nouvelle_date.'.</strong></p>';

                $nbr_amapiens = count($mc_amapiens_deplacement);

                echo '<p>Les '.$nbr_amapiens.' utilisateurs suivants sont impactés par cette suppression :</p>';

                if (0 != $nbr_amapiens) {
                    $i = 1;
                    $virgule = 1;

                    echo '<div class="alert alert-warning">';

                    $amapiens = array_map(function (PsrLib\ORM\Entity\Amapien $amapien) {
                        return strtoupper($amapien->getNom()).' '.$amapien->getPrenom();
                    }, $mc_amapiens_deplacement);
                    echo implode(' ; ', $amapiens);

                    echo '</div>';
                }

                echo '<p>Pour les prévenir de ce changement, voici la liste de leurs adresses email :</p>';

                if (0 != $nbr_amapiens) {
                    echo '<div class="alert alert-warning">';

                    $virgule = 1;

                    $emails = array_map(function (PsrLib\ORM\Entity\Amapien $amapien) {
                        return $amapien->getEmail();
                    }, $mc_amapiens_deplacement);
                    echo implode(' ; ', $emails);

                    echo '</div>';
                }

                ?>

                <p>Appuyez sur Sauvegarder pour réaliser cette modification, ou Annuler pour ne rien modifier.</p>
            </div>
        </div>
    </div>

    <div class="form-group">

        <input type="submit" value="Étape suivante" class="pull-right btn btn-success"/>

        <a href="<?= site_url('contrat_signe/accueil'); ?>" class="pull-right btn btn-default">Annuler</a>

    </div>

</form>
