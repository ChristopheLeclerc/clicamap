<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<script type="text/javascript">
</script>

<h3>Mentions légales</h3>
<div class="well">

    <p>Les données recueillies sur ce site sont enregistrées dans un fichier informatisé du Réseau AMAP Auvergne-Rhône-Alpes pour la gestion des adhérents (AMAP, Amapien-ne, Paysan-ne).<br/>
        Ces données sont conservées pendant 3 ans et sont destinées aux réseaux associatifs suivants :<ul>
	<li>Alliance PEC Isère</li>
        <li>Alliance PEC Savoie</li>
	<li>Réseau AMAP Aura</li>
	</ul>
        Conformément à la loi « informatique et libertés », vous pouvez exercer votre droit d'accès aux données vous concernant et les faire rectifier en contactant :<br/>
	<b>Réseau AMAP AuRA - 58 rue Raulin 69007 Lyon</b>
    </p>

</div>
