<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ak_amap_distribution")
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AmapDistributionRepository")
 */
class AmapDistribution
{
    /**
     * @var ?int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var AmapDistributionDetail
     * @ORM\Embedded(class="AmapDistributionDetail")
     */
    private $detail;

    /**
     * @var AmapLivraisonLieu
     * @ORM\ManyToOne(targetEntity="AmapLivraisonLieu", inversedBy="distributions")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="amap_liv_lieu_id")
     */
    private $amapLivraisonLieu;

    /**
     * @var ?Carbon
     * @ORM\Column(type="carbon")
     * @Assert\NotNull
     */
    private $date;

    /**
     * @var ArrayCollection<Amapien>
     * @ORM\ManyToMany(targetEntity="Amapien")
     * @JoinTable(
     *      inverseJoinColumns={@JoinColumn(referencedColumnName="a_id")},
     * )
     */
    private $amapiens;

    public function __construct(
        AmapDistributionDetail $detail,
        AmapLivraisonLieu $amapLivraisonLieu,
        Carbon $date
    ) {
        $this->amapiens = new ArrayCollection();
        $this->detail = $detail;
        $this->amapLivraisonLieu = $amapLivraisonLieu;
        $this->date = $date;
    }

    public function getDateDebut(): ?Carbon
    {
        $heureDebut = $this->getDetail()->getHeureDebut();

        return $this
            ->getDate()
            ->clone()
            ->setHour($heureDebut->getHour())
            ->setMinute($heureDebut->getMinute())
        ;
    }

    public function getDateFin(): ?Carbon
    {
        $heureFin = $this->getDetail()->getHeureFin();

        return $this
            ->getDate()
            ->clone()
            ->setHour($heureFin->getHour())
            ->setMinute($heureFin->getMinute())
        ;
    }

    public function getPlacesRestantes(): int
    {
        return $this->detail->getNbPersonnes() - $this->amapiens->count();
    }

    public function isCompleted(): bool
    {
        return $this->getPlacesRestantes() <= 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDetail(): AmapDistributionDetail
    {
        return $this->detail;
    }

    public function setDetail(AmapDistributionDetail $detail): AmapDistribution
    {
        $this->detail = $detail;

        return $this;
    }

    public function getAmapLivraisonLieu(): AmapLivraisonLieu
    {
        return $this->amapLivraisonLieu;
    }

    public function setAmapLivraisonLieu(AmapLivraisonLieu $amapLivraisonLieu): AmapDistribution
    {
        $this->amapLivraisonLieu = $amapLivraisonLieu;

        return $this;
    }

    public function getDate(): ?Carbon
    {
        return $this->date;
    }

    public function setDate(?Carbon $date): AmapDistribution
    {
        $this->date = $date;

        return $this;
    }

    public function addAmapien(Amapien $amapiens): AmapDistribution
    {
        $this->amapiens[] = $amapiens;

        return $this;
    }

    public function removeAmapien(Amapien $amapiens): bool
    {
        return $this->amapiens->removeElement($amapiens);
    }

    public function getAmapiens(): Collection
    {
        return $this->amapiens;
    }
}
