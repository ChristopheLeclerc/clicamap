<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184618 extends AbstractMigration
{
    private $existingFermeRef = [];

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        $this->existingFermeRef = $this
            ->connection
            ->executeQuery('SELECT afr.f_ref_fk_ferme_id, afr.f_ref_fk_amap_amapien_id, aaa.amap_a_fk_amapien_id FROM ak_ferme_referent afr
                JOIN ak_amap_amapien aaa ON aaa.amap_a_id = afr.f_ref_fk_amap_amapien_id')
            ->fetchAll()
        ;
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ferme_amapien (ferme_f_id BIGINT NOT NULL, amapien_a_id BIGINT NOT NULL, INDEX IDX_AA769FCC48A4116C (ferme_f_id), INDEX IDX_AA769FCC19E9F30A (amapien_a_id), PRIMARY KEY(ferme_f_id, amapien_a_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ferme_amapien ADD CONSTRAINT FK_AA769FCC48A4116C FOREIGN KEY (ferme_f_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ferme_amapien ADD CONSTRAINT FK_AA769FCC19E9F30A FOREIGN KEY (amapien_a_id) REFERENCES ak_amapien (a_id)');

        foreach ($this->existingFermeRef as $fermeRef) {
            $this->addSql('INSERT INTO ferme_amapien (ferme_f_id, amapien_a_id) VALUES (:fId, :amapienId)', [
                'fId' => $fermeRef['f_ref_fk_ferme_id'],
                'amapienId' => $fermeRef['amap_a_fk_amapien_id'],
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
