<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
/** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire[] $livraisonHoraires */
/** @var \Symfony\Component\Form\FormView $form */
use PsrLib\ORM\Entity\AmapLivraisonHoraire;

?>

<h3>Les horaires de livraison</h3>

<?php

if (get_current_connected_user() instanceof \PsrLib\ORM\Entity\Amap) {
    $retour = 'Gestion de mon AMAP';
} else {
    $retour = 'Gestion des AMAP';
}

echo '<h5><a href="'.site_url('amap').'">'.$retour.'</a> <i class="glyphicon glyphicon-menu-right"></i> '.mb_strtoupper($amap->getNom()).' <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('amap/livraison_lieu/'.$amap->getId()).'"> Les lieux de livraison</a> <i class="glyphicon glyphicon-menu-right"></i> '.$livraisonLieu->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Les horaires de livraison</h5>';

// echo '<div class="alert alert-info">Ajoutez des lieux de livraison ...</div>';

?>

<div class="row">
    <div class="col-md-12" id="livraison_lieu">

        <!-- ----------------------------------------------------------------------- -->
        <!-- AJOUTER UN LIEU -->
        <form method="POST" action=""
              class="clearfix">

            <input type="hidden" name="step_l_h" value="1"/>

            <div class="entry">

                <div class="well well-sm">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">

                                <div class="col-md-3">
                                    <label for="liv_saison" class="control-label">Saison : </label>
                                    <?= render_form_select_widget($form['saison']); ?>
                                </div>

                                <div class="col-md-3">
                                    <label for="liv_jour" class="control-label">Jour de livraison : </label>
                                    <?= render_form_select_widget($form['jour']); ?>
                                </div>

                                <div class="col-md-3">
                                    <label for="liv_heure_deb" class="control-label">Heure de début : </label>
                                    <?= render_form_text_widget($form['heureDebut']); ?>
                                </div>

                                <div class="col-md-3">
                                    <label for="liv_heure_fin" class="control-label">Heure de fin : </label>
                                    <?= render_form_text_widget($form['heureFin']); ?>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-group pull-right">

                <input type="submit" value="Ajouter" class="pull-right btn btn-success clearfix"
                       style="margin-left:5px;"/>

            </div>

        </form>

        <!-- ----------------------------------------------------------------------- -->
        <!-- TABLEAU DES LIEUX EXISTANTS -->
        <?php
        if ($livraisonHoraires) {
            echo '<div class="table-responsive">';
            echo '<table class="table table-hover">';
            echo '<thead class="thead-inverse">';
            echo '<tr>';
            echo '<th>Saison</th>';
            echo '<th>Jour</th>';
            echo '<th>Heure de début</th>';
            echo '<th>Heure de fin</th>';
            echo '<th class="text-right">Outils</th>';
            echo '</tr>';
            echo '</thead>';

            echo '<tbody>';

            /** @var AmapLivraisonHoraire $liv_hor */
            foreach ($livraisonHoraires as $liv_hor) {
                echo '<tr>';
                echo '<th scope="row">';
                if (AmapLivraisonHoraire::SAISON_ETE === $liv_hor->getSaison()) {
                    echo 'Été';
                } else {
                    echo 'Hiver';
                }
                echo '</th>';

                echo '<td>';
                echo ucfirst($liv_hor->getJour());
                echo '</td>';

                echo '<td>';
                echo ucfirst($liv_hor->getHeureDebut());
                echo '</td>';

                echo '<td>';
                echo ucfirst($liv_hor->getHeureFin());
                echo '</td>';

                echo '<td class="text-right">';
                echo '<a title="Modifier l\'horaire de livraison" href="'.site_url('amap/livraison_horaire_edit/'.$liv_hor->getId()).'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier l\'horaire de livraison"></i></a>&nbsp;';
                echo '<a title="Supprimer l\'horaire de livraison" href="#" data-toggle="modal" data-target="#modal_remove_'.$liv_hor->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer l\'horaire de livraison"></i></a>';
                echo '</td>';
                echo '</tr>'; ?>
                <!-- Modal remove -->
                <div class="modal fade text-left" id="modal_remove_<?= $liv_hor->getId(); ?>"
                     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer l'horaire de
                                    livraison ?</h5>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger"><strong>Attention !</strong> En supprimant l'horaire
                                    vous supprimerez aussi ses horaires d'aide à la distrib'AMAP. Toutes les
                                    inscriptions liées à ces horaires seront de fait supprimées elles aussi.
                                </div>
                                <p class="text-right">
                                    <a href="<?= site_url('amap/livraison_horaire_remove/'.$liv_hor->getId()); ?>"
                                       class="btn btn-danger btn-sm">OUI</a>
                                    <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }

            echo '</tbody>';
            echo '</table>';
            echo '</div>';
        }
        ?>

    </div>
</div>
