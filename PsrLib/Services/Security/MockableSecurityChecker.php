<?php

namespace PsrLib\Services\Security;

/**
 * Mockable security checker. Used to unit test security checks with dependencies.
 */
class MockableSecurityChecker implements SecurityCheckerInterface
{
    /**
     * @var SecurityChecker
     */
    private $securityChecker;

    /**
     * @var bool[]
     */
    private $mocks = [];

    public function __construct(SecurityChecker $securityChecker)
    {
        $this->securityChecker = $securityChecker;
    }

    public function mockAction(string $action, bool $result): void
    {
        $this->mocks[$action] = $result;
    }

    public function cleanMocks(): void
    {
        $this->mocks = [];
    }

    public function isGranted(string $action, $subject = null): bool
    {
        if (isset($this->mocks[$action])) {
            return $this->mocks[$action];
        }

        return $this->securityChecker->isGranted($action, $subject);
    }
}
