<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Prophecy\Argument;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class AdhesionAmapTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testAmapAccessNoPermission()
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $value = $this->prophesize(AdhesionValueAmap::class)->reveal();
        $creator = $this->prophesize(Amapien::class);
        $creator->__toString()->willReturn('amapien');
        $creator->isAdminOf(Argument::is($amap))->willReturn(false);
        $adhesion = new AdhesionAmap($amap, $value, $creator->reveal());
        $violations = $this->validator->validate($adhesion, null, 'import');

        self::assertCount(1, $violations);
        self::assertSame("Erreur d'accès à l'AMAP", $violations[0]->getMessage());
    }

    public function testAmapAccessPermission()
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $value = $this->prophesize(AdhesionValueAmap::class)->reveal();
        $creator = $this->prophesize(Amapien::class);
        $creator->isAdminOf(Argument::is($amap))->willReturn(true);
        $adhesion = new AdhesionAmap($amap, $value, $creator->reveal());
        $violations = $this->validator->validate($adhesion, null, 'import');

        self::assertCount(0, $violations);
    }
}
