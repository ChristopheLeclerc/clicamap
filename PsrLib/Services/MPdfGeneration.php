<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use CI_Config;
use CI_Loader;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;
use Psr\Log\AbstractLogger;
use PsrLib\ORM\Entity\Adhesion;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Files\ContratPdf;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratProduitExclure;
use PsrLib\ORM\Repository\ModeleContratDateRepository;
use PsrLib\ProjectLocation;
use Ramsey\Uuid\Uuid;

class MPdfGeneration
{
    /**
     * @var Mpdf
     */
    private $mPdf;

    /**
     * @var CI_Loader
     */
    private $ciLoader;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CI_Config
     */
    private $ciConfig;

    public function __construct(EntityManagerInterface $em, CI_Loader $ciLoader, CI_Config $config)
    {
        $this->em = $em;
        $this->ciLoader = $ciLoader;
        $this->ciConfig = $config;

        // Override backtrack setting to avoid rendering errors
        ini_set('pcre.backtrack_limit', '10000000000');

        $this->mPdf = $this->buildMpdf();

        // DEBUG log
        $this->mPdf->setLogger(new class() extends AbstractLogger {
            public function log($level, $message, array $context = [])
            {
                $date = new DateTime();
                file_put_contents(
                    ProjectLocation::PROJECT_ROOT.'/application/logs/mpdf.log',
                    $date->format(DATE_ATOM).'-'.$level.': '.$message.PHP_EOL,
                    FILE_APPEND
                );
            }
        });
    }

    public function genererContratVierge(ModeleContrat $mc): void
    {
        $livraison_lieu = $mc->getLivraisonLieu();
        $amap = $livraison_lieu->getAmap();
        $ferme = $mc->getFerme();
        $dates = $mc->getDates()->matching(ModeleContratDateRepository::criteriaOrderDateAsc())->toArray();
        $produits = $mc->getProduits()->toArray();
        $dates_reglement = $mc->getDateReglements()->toArray();
        $paysans = $ferme->getPaysans();
        $exclusions = $this->orderExclusionByProduitDate(
            $this
                ->em
                ->getRepository(ModeleContratProduitExclure::class)
                ->getByContrat($mc)
        );

        $html = $this->ciLoader->view('PDF/contrat.php', [
            'contrat' => null,
            'amapien' => null,
            'commandes' => [],
            'contrat_signe_dates_reglements' => [],
            'mc' => $mc,
            'amap' => $amap,
            'ferme' => $ferme,
            'livraison_lieu' => $livraison_lieu,
            'dates' => $dates,
            'produits' => $produits,
            'dates_reglement' => $dates_reglement,
            'paysans' => $paysans,
            'exclusions' => $exclusions,
        ], true);

        $this->mPdf->WriteHTML($html);
        $this->mPdf->Output('contrat.pdf', Destination::INLINE);
    }

    /**
     * @throws MpdfException
     */
    public function genererContratSigneInline(Contrat $contrat): void
    {
        $this->genererContratSigne($contrat);

        $this->mPdf->Output('contrat.pdf', Destination::INLINE);
    }

    /**
     * @throws MpdfException
     */
    public function genererContratSigneFile(Contrat $contrat): string
    {
        $this->genererContratSigne($contrat);

        // Creation destination si existe pas
        $contratPath = ContratPdf::getUploadPath();
        if (!is_dir($contratPath)) {
            mkdir($contratPath, 0777, true);
        }

        $name = Uuid::uuid4()->toString().'.pdf';
        $path = $contratPath.'/'.$name;
        $this->mPdf->Output($path, Destination::FILE);

        return $name;
    }

    public function genererAdhesion(Adhesion $adhesion): string
    {
        $mPdf = $this->buildMpdf(); // Use new Mpdf instance on each call as generation made in bash
        $html = $this->ciLoader->view('PDF/adhesion.php', [
            'adhesion' => $adhesion,
        ], true);

        $mPdf->WriteHTML($html);

        // Creation destination si existe pas
        $adhesionPath = $this->ciConfig->item('adhesion_path');
        if (!is_dir($adhesionPath)) {
            mkdir($adhesionPath, 0777, true);
        }

        $name = Uuid::uuid4()->toString().'.pdf';
        $path = $adhesionPath.'/'.$name;
        $mPdf->Output($path, Destination::FILE);

        return $name;
    }

    // Build a new pdf instance for each file generation
    private function buildMpdf(): Mpdf
    {
        return new Mpdf([
            'debug' => true,
            'tempDir' => ProjectLocation::PROJECT_ROOT.'/application/writable/mpdf/',
        ]);
    }

    /**
     * @param ModeleContratProduitExclure[] $exclusions
     *
     * @return ModeleContratProduitExclure[][]
     */
    private function orderExclusionByProduitDate($exclusions)
    {
        $res = [];
        foreach ($exclusions as $exclusion) {
            $produitId = $exclusion->getModeleContratProduit()->getId();
            $dateId = $exclusion->getModeleContratDate()->getId();
            if (!array_key_exists($produitId, $res)) {
                $res[$produitId] = [];
            }

            if (!array_key_exists($dateId, $res)) {
                $res[$produitId][$dateId] = $exclusion;
            }
        }

        return $res;
    }

    private function genererContratSigne(Contrat $contrat): void
    {
        $mc = $contrat->getModeleContrat();
        $amap = $mc->getLivraisonLieu()->getAmap();
        $ferme = $mc->getFerme();
        $livraison_lieu = $mc->getLivraisonLieu();
        $dates = $mc->getDates()->toArray();
        $produits = $mc->getProduits()->toArray();
        $dates_reglement = $mc->getDateReglements();
        $paysans = $ferme->getPaysans();
        $exclusions = $this->orderExclusionByProduitDate(
            $this
                ->em
                ->getRepository(ModeleContratProduitExclure::class)
                ->getByContrat($mc)
        );

        $amapien = $contrat->getAmapien();

        $commandes = $contrat->getCellules()->toArray();
        $contrat_signe_dates_reglements = $contrat->getDatesReglements()->toArray();

        $html = $this->ciLoader->view('PDF/contrat.php', [
            'contrat' => $contrat,
            'amapien' => $amapien,
            'commandes' => $commandes,
            'contrat_signe_dates_reglements' => $contrat_signe_dates_reglements,
            'mc' => $mc,
            'amap' => $amap,
            'ferme' => $ferme,
            'livraison_lieu' => $livraison_lieu,
            'dates' => $dates,
            'produits' => $produits,
            'dates_reglement' => $dates_reglement,
            'paysans' => $paysans,
            'exclusions' => $exclusions,
        ], true);

        $this->mPdf->WriteHTML($html);
    }
}
