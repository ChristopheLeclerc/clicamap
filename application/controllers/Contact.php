<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Contact.
 */
class Contact extends AppController
{
    protected $data;

    /**
     * Contact constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['envoi' => false,
            'envoi_erreur' => false,
            'envoi_ok' => false,
            'alerte_modal' => false,
            'vide' => false,
            'reseau_all' => [],
            'amap_all' => [],
            'amapien_all' => [],
            'ID' => null,
            'admin_ID' => null,
            'amap_ID' => null,
            'erreur' => false,
            'identifiant' => null, ];

        $this->load->library('email');
        $this->load->library('form_validation');

        $this
            ->form_validation
            ->set_error_delimiters('<div class="alert alert-danger">', '</div>')
        ;
    }

    /**
     * Le tableau est-il vide de valeurs ?
     *
     * @param $a
     *
     * @return bool
     */
    public function _array_vide($a)
    {
        foreach ($a as $k => $v) {
            if (!empty($v)) {
                return false;
            }
        }

        return true;
    }

    /**
     * ADMIN
     * JE SUIS une FERME / PAY ou une AMAP.
     */
    public function admin()
    {
        // Sécurité
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTACT_ADMIN);

        $adminRegions = $this->getUser()->getRegion()->getAdmins()->toArray();
        $adminDepartements = $this->getUser()->getDepartement()->getAdmins()->toArray();

        $targetIds = $this->extractTargetIds(array_merge($adminRegions, $adminDepartements));

        $form = $this->formFactory->create(\PsrLib\Form\ContactTargetType::class, null, [
            'available_target_id' => $targetIds,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $target = $this
                    ->em
                    ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                    ->findOneBy([
                        'id' => $form->get('targetId')->getData(),
                    ])
                ;
                $currentUser = $this->getUser();
                if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
                    $statut = 'une AMAP';
                    $expediteur_nom = $currentUser->getNom();
                    $expediteur_email = $currentUser->getEmail();
                } else {
                    $statut = 'un paysan';
                    $expediteur_nom = strtoupper($currentUser->getNom())
                        .' '
                        .ucfirst(strtolower($currentUser->getPrenom()));
                    $expediteur_email = $currentUser->getEmail();
                }

                $this->email->clear();
                $this->email->from(EMAIL, FROM);
                $this->email->to($target->getEmail());
                $this->email->subject('Message '.SITE_NAME);
                $this->email->message('<p>Bonjour, '.$statut.' vous a envoyé un message depuis '.SITE_NAME.' :</p>
                      <div style="padding:15px; margin:10px 0px; background-color:#fcf8e3;"><p>Objet : '.$form->get('title')->getData().'</p><p>'.nl2br($form->get('content')->getData()).'</p><p>--<br/>'.$expediteur_nom.'<br/>'.$expediteur_email.'</p></div>
            
                      <p><a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a></p>');
                if ($this->email->send()) {
                    $this->addFlash(
                        'notice_success',
                        'Le message a été envoyé avec succès !'
                    );
                    redirect('/contact/admin');

                    return;
                }
            }

            $this->addFlash(
                'notice_error',
                'Le message n\'a pas pu être envoyé. Une adresse email n\'est sans doute pas valide.'
            );
            redirect('/contact/admin');

            return;
        }

        $this->loadViewWithTemplate('contact/admin', [
            'form' => $form->createView(),
            'adminRegions' => $adminRegions,
            'adminDepartements' => $adminDepartements,
        ]);
    }

    /**
     * MON AMAP
     * JE SUIS un AMAPIEN ou un RÉFÉRENT PRODUIT.
     */
    public function mon_amap()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTACT_OWN_AMAP);

        /** @var \PsrLib\ORM\Entity\Amapien $currentUser Granted by permission */
        $currentUser = $this->getUser();

        $form = $this->formFactory->create(\PsrLib\Form\ContactType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $statut = 'amapien';
                if ($currentUser->isRefProduit()) {
                    $statut = 'référent produit';
                }

                $expediteur_prenom = ucfirst(strtolower($currentUser->getPrenom()));
                $expediteur_nom = mb_strtoupper($currentUser->getNom());
                $expediteur_email = $currentUser->getEmail();
                $amap = $currentUser->getAmap();

                $this->email->clear();
                $this->email->from(EMAIL, FROM);
                $this->email->to($currentUser->getAmap()->getEmail());
                $this->email->subject('Message '.SITE_NAME);
                $this->email->message('<p>Bonjour, un '.$statut.' vous a envoyé un message depuis '.SITE_NAME.' :</p>
            <div style="padding:15px; margin:10px 0px; background-color:#fcf8e3;"><p>Objet : '.$form->get('title')->getData().'</p><p>'.nl2br($form->get('content')->getData()).'</p><p>--<br/>'.$expediteur_nom.' '.$expediteur_prenom.'<br/>'.$expediteur_email.'<br/>'.$amap.'</p></div>

            <p><a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a></p>');

                if ($this->email->send()) {
                    $this->addFlash(
                        'notice_success',
                        'Le message a été envoyé avec succès !'
                    );
                    redirect('/contact/mon_amap');

                    return;
                }
            }

            $this->addFlash(
                'notice_error',
                'Le message n\'a pas pu être envoyé. Une adresse email n\'est sans doute pas valide.'
            );
        }

        $this->loadViewWithTemplate('contact/mon_amap', [
            'amap' => $currentUser->getAmap(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * AMAP
     * JE SUIS une FERME / PAYSAN.
     */
    public function amap()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTACT_AMAP);

        /** @var \PsrLib\ORM\Entity\Paysan $currentUser Granted by permission */
        $currentUser = $this->getUser();

        $amaps = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amap::class)
            ->findByFerme($currentUser->getFerme())
        ;

        $form = $this->formFactory->create(\PsrLib\Form\ContactTargetType::class, null, [
            'available_target_id' => $this->extractTargetIds($amaps),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $target = $this
                    ->em
                    ->getRepository(\PsrLib\ORM\Entity\Amap::class)
                    ->findOneBy([
                        'id' => $form->get('targetId')->getData(),
                    ])
                ;
                $expediteur_prenom = ucfirst(strtolower($currentUser->getPrenom()));
                $expediteur_nom = mb_strtoupper($currentUser->getNom());
                $expediteur_email = $currentUser->getEmail();

                // Référents en copie ----------------------------------------------
                $ref_prod_en_copie = [];
                /** @var \PsrLib\ORM\Entity\Amapien $amapien */
                foreach ($target->getAmapiens() as $amapien) {
                    if ($amapien->isRefProduit()) {
                        $ref_prod_en_copie[] = $amapien->getEmail();
                    }
                }
                $ref_prod_en_copie = array_unique($ref_prod_en_copie);

                // Ajout du paysan en copie
                $ref_prod_en_copie[] = $expediteur_email;

                $this->email->clear();
                $this->email->from(EMAIL, FROM);
                $this->email->to($target->getEmail());
                $this->email->cc($ref_prod_en_copie);
                $this->email->subject('Message '.SITE_NAME);
                $this->email->message('<p>Bonjour, un paysan vous a envoyé un message depuis '.SITE_NAME.' :</p>
            <div style="padding:15px; margin:10px 0px; background-color:#fcf8e3;"><p>Objet : '.$form->get('title')->getData().'</p><p>'.nl2br($form->get('content')->getData()).'</p><p>--<br/>'.$expediteur_nom.' '.$expediteur_prenom.'<br/>'.$expediteur_email.'</p></div>
            <p><a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a></p>');

                if ($this->email->send()) {
                    $this->addFlash(
                        'notice_success',
                        'Le message a été envoyé avec succès !'
                    );
                    redirect('/contact/amap');

                    return;
                }
            }

            $this->addFlash(
                'notice_error',
                'Le message n\'a pas pu être envoyé. Une adresse email n\'est sans doute pas valide.'
            );
            redirect('/contact/amap');

            return;
        }

        $this->loadViewWithTemplate('contact/amap', [
            'amaps' => $amaps,
            'form' => $form->createView(),
        ]);
    }

    /**
     * RÉFÉRENT PRODUIT
     * JE SUIS un AMAPIEN ou un RÉFÉRENT PRODUIT.
     */
    public function referent_produit()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTACT_OWN_AMAP);

        /** @var \PsrLib\ORM\Entity\Amapien $currentUser Granted by permission */
        $currentUser = $this->getUser();

        $amap = $currentUser->getAmap();

        $fermes = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->getFromAmap($amap)
        ;

        $amapienRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class);
        $refProduits = [];
        foreach ($fermes as $ferme) {
            $refProduits[$ferme->getId()] = $amapienRepo
                ->getRefProduitFromAmapFerme($amap, $ferme)
            ;
        }

        $form = $this->formFactory->create(\PsrLib\Form\ContactTargetType::class, null, [
            'available_target_id' => array_map(function (PsrLib\ORM\Entity\Ferme $ferme) {
                return (string) $ferme->getId();
            }, $fermes),
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $statut = 'amapien';
                if ($currentUser->isRefProduit()) {
                    $statut = 'référent produit';
                }

                $prenom = $currentUser->getPrenom();
                $nom = $currentUser->getNom();
                $email_from = $currentUser->getEmail();

                $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $form->get('targetId')->getData());
                $refProduits = $amapienRepo
                    ->getRefProduitFromAmapFerme($amap, $ferme)
                ;
                $emails = [];
                foreach ($refProduits as $refProduit) {
                    $emails[] = $refProduit->getEmail();
                }

                $this->email->clear();
                $this->email->from(EMAIL, FROM);
                $this->email->reply_to($email_from);
                $this->email->to($emails);
                $this->email->cc($amap->getEmail());
                $this->email->subject('Message '.SITE_NAME);
                $this->email->message('<p>Bonjour, un '.$statut.' vous a envoyé un message depuis '.SITE_NAME.' :</p>
          <div style="padding:15px; margin:10px 0px; background-color:#fcf8e3;"><p>Objet : '.$form->get('title')->getData().'</p><p>'.nl2br($form->get('content')->getData()).'</p><p>--<br/>'.mb_strtoupper($nom).' '.ucfirst(strtolower($prenom)).'<br/>'.$amap->getNom().'<br/>'.$email_from.'<br/>'.$currentUser->getDepartement()->getNom().'</p></div>
          <p><a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a></p>');

                if ($this->email->send()) {
                    $this->addFlash(
                        'notice_success',
                        'Le message a été envoyé avec succès !'
                    );
                    redirect('/contact/referent_produit');

                    return;
                }
            }

            $this->addFlash(
                'notice_error',
                'Le message n\'a pas pu être envoyé. Une adresse email n\'est sans doute pas valide.'
            );
            redirect('/contact/referent_produit');

            return;
        }

        $this->loadViewWithTemplate('contact/referent', [
            'fermes' => $fermes,
            'refProduits' => $refProduits,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Mentions légales.
     */
    public function mentions_legales()
    {
        $this->loadViewWithTemplate('contact/mentions-legales');
    }

    /**
     * @param \PsrLib\ORM\Entity\BaseUser[] $user
     */
    private function extractTargetIds(array $users)
    {
        return array_map(function (PsrLib\ORM\Entity\BaseUser $user) {
            return (string) $user->getId();
        }, $users);
    }
}
