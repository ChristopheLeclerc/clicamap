<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Publipostage extends AppController
{
    /**
     * Publipostage constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['type_prod' => [], 'emails' => null];

        $this->load->helper('array');
        $this->load->helper('email');
        $this->load->helper('string');

        $this->load->library('email');
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    /**
     * Index.
     */
    public function index()
    {
        $this->form_1();
    }

    public function form_1()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_MAILSHOT);

        /** @var \PsrLib\ORM\Entity\Amapien $currentUser Granted by permission */
        $currentUser = $this->getUser();

        /** @var \PsrLib\ORM\Repository\RegionRepository $regionRepo */
        $regionRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Region::class);
        /** @var \PsrLib\ORM\Repository\DepartementRepository $departementRepo */
        $departementRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Departement::class);
        /** @var \PsrLib\ORM\Repository\TypeProductionRepository $typeProductionRepo */
        $typeProductionRepo = $this->em->getRepository(\PsrLib\ORM\Entity\TypeProduction::class);
        /** @var \PsrLib\ORM\Repository\ReseauRepository $reseauRepo */
        $reseauRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Reseau::class);
        /** @var \PsrLib\ORM\Repository\PaysanRepository $paysanRepo */
        $paysanRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Paysan::class);
        /** @var \PsrLib\ORM\Repository\AmapRepository $amapRepo */
        $amapRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class);

        // SESSIONS + POST -----------------------------------------------------------
        // SI SUPER ADMIN
        if ($currentUser->isSuperAdmin()) {
            $this->data['departement_all'] = [];
            $this->data['dep_res'] = [];
            $this->data['region_all'] = $regionRepo->findAllOrdered();

            // BOUTON DÉSÉLECTIONNER
            if ($this->input->post('reg_clear')) {
                $_POST['reg'] = [];
            }
            if ($this->input->post('dep_clear')) {
                $_POST['dep'] = [];
            }
            if ($this->input->post('res_clear')) {
                $_POST['res'] = [];
            }
            if ($this->input->post('tp_clear')) {
                $_POST['tp'] = [];
            }

            // $_POST
            // SI RÉGION CHOISI(S)
            if (!empty($this->input->post('reg'))) {
                $departements = [];
                foreach ($this->input->post('reg') as $reg) {
                    $departements = array_merge(
                        $departements,
                        $departementRepo->findBy([
                            'region' => $this->em->getReference(\PsrLib\ORM\Entity\Region::class, $reg),
                        ])
                    );
                }
                sort($departements);

                $this->data['departement_all'] = $departements;
                $this->data['type_prod'] = $typeProductionRepo->findAllNoParent();
            }

            // SI DÉPARTEMENT CHOISI(S)
            if (!empty($this->input->post('dep'))) {
                $this->data['type_prod'] = $typeProductionRepo->findAllNoParent();

                // AFFICHAGE DES RÉSEAUX
                // On pourrait aussi utiliser WHERE_IN
                // Tableaux dans 1 tableau !
                $reseaux = [];
                foreach ($this->input->post('dep') as $dep) {
                    $reseaux = array_merge(
                        $reseaux,
                        $reseauRepo->findByDepartement($this->em->getReference(\PsrLib\ORM\Entity\Departement::class, $dep))
                    );
                }

                foreach ($reseaux as $reseau) {
                    foreach ($reseau as $res) {
                        $this->data['dep_res'][] = $res;
                    }
                }
            }
        } elseif ($currentUser->isAdminRegion()) { // SI RÉGION
            $this->data['dep_res'] = [];
            $this->data['departement_all'] = $departementRepo->findByMultipleRegion($currentUser->getAdminRegions()->toArray());

            // BOUTON DÉSÉLECTIONNER
            if ($this->input->post('dep_clear')) {
                $_POST['dep'] = [];
            }
            if ($this->input->post('res_clear')) {
                $_POST['res'] = [];
            }
            if ($this->input->post('tp_clear')) {
                $_POST['tp'] = [];
            }

            // $_POST
            // SI DÉPARTEMENT CHOISI(S)
            if (!empty($this->input->post('dep'))) {
                $this->data['type_prod'] = $typeProductionRepo->findAllNoParent();

                // AFFICHAGE DES RÉSEAUX
                // Tableaux dans 1 tableau !
                $reseaux = [];
                foreach ($this->input->post('dep') as $dep) {
                    $reseaux = array_merge(
                        $reseaux,
                        $reseauRepo->findByDepartement($this->em->getReference(\PsrLib\ORM\Entity\Departement::class, $dep))
                    );
                }

                foreach ($reseaux as $reseau) {
                    foreach ($reseau as $res) {
                        $this->data['dep_res'][] = $res;
                    }
                }

                // PRINT_R($this->data['dep_res']);
            }
        } // SI DEPARTEMENT
        elseif ($currentUser->isAdminDepartment()) {
            $this->data['dep_res'] = [];
            $this->data['departement_all'] = $currentUser->getAdminDepartments()->toArray();

            // BOUTON DÉSÉLECTIONNER
            if ($this->input->post('dep_clear')) {
                $_POST['dep'] = [];
            }
            if ($this->input->post('res_clear')) {
                $_POST['res'] = [];
            }
            if ($this->input->post('tp_clear')) {
                $_POST['tp'] = [];
            }

            // $_POST
            // SI DÉPARTEMENT CHOISI(S)
            if (!empty($this->input->post('dep'))) {
                $this->data['type_prod'] = $typeProductionRepo->findAllNoParent();

                // AFFICHAGE DES RÉSEAUX
                // Tableaux dans 1 tableau !
                $reseaux = [];
                foreach ($this->input->post('dep') as $dep) {
                    $reseaux = array_merge(
                        $reseaux,
                        $reseauRepo->findByDepartement($this->em->getReference(\PsrLib\ORM\Entity\Departement::class, $dep))
                    );
                }

                foreach ($reseaux as $reseau) {
                    foreach ($reseau as $res) {
                        $this->data['dep_res'][] = $res;
                    }
                }

                // PRINT_R($this->data['dep_res']);
            }
        } // SI RÉSEAU
        elseif ($currentUser->isAdminReseaux()) {
            $this->data['dep_res'] = $currentUser->getAdminReseaux()->toArray();

            // BOUTON DÉSÉLECTIONNER
            // if($this->input->post('dep_clear'))
            //   $_POST['dep'] = array();
            if ($this->input->post('res_clear')) {
                $_POST['res'] = [];
            }
            if ($this->input->post('tp_clear')) {
                $_POST['tp'] = [];
            }

            // $_POST
            // SI DÉPARTEMENT CHOISI(S)
            if (!empty($this->input->post('res'))) {
                $this->data['type_prod'] = $typeProductionRepo->findAllNoParent();
            }
        }

        // ---------------------------------------------------------------------------
        if ($this->input->post('etape_validee') && 'etiquette' == $this->input->post('media')) {
            if ('paysans' == $this->input->post('qui')) {
                if ($this->input->post('adh_non') && !$this->input->post('adh_oui')) {
                    $paysans_mdr_all = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), null);
                    $paysans_mdr_all_ID = [];
                    foreach ($paysans_mdr_all as $paysan) {
                        array_push($paysans_mdr_all_ID, $paysan->getId());
                    }

                    $paysans_mdr_adh_oui = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), 1);
                    $paysans_mdr_adh_oui_ID = [];
                    foreach ($paysans_mdr_adh_oui as $paysan) {
                        array_push($paysans_mdr_adh_oui_ID, $paysan->getId());
                    }

                    $this->data['paysans'] = [];
                    foreach ($paysans_mdr_all as $paysan) {
                        if (!in_array($paysan->getId(), $paysans_mdr_adh_oui_ID)) {
                            array_push($this->data['paysans'], $paysan);
                        }
                    }
                } elseif (!$this->input->post('adh_non') && $this->input->post('adh_oui')) {
                    $this->data['paysans'] = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), 1);
                } else {
                    $this->data['paysans'] = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), null);
                }
            } elseif ('amap' == $this->input->post('qui')) {
                if ($this->input->post('adh_non') && !$this->input->post('adh_oui')) {
                    $amap_mdr_all = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), null);
                    $amap_mdr_all_ID = [];
                    foreach ($amap_mdr_all as $amap) {
                        array_push($amap_mdr_all_ID, $amap->getId());
                    }

                    $amap_mdr_adh_oui = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), 1);
                    $amap_mdr_adh_oui_ID = [];
                    foreach ($amap_mdr_adh_oui as $amap) {
                        array_push($amap_mdr_adh_oui_ID, $amap->getId());
                    }

                    $this->data['amap'] = [];
                    foreach ($amap_mdr_all as $amap) {
                        if (!in_array($amap->getId(), $amap_mdr_adh_oui_ID)) {
                            array_push($this->data['amap'], $amap);
                        }
                    }
                } elseif (!$this->input->post('adh_non') && $this->input->post('adh_oui')) {
                    $this->data['amap'] = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), 1);
                } else {
                    $this->data['amap'] = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), null);
                }
            }
            $this->etiquette();
        } elseif ('email' == $this->input->post('media')) {
            if ('paysans' == $this->input->post('qui')) {
                if ($this->input->post('adh_non') && !$this->input->post('adh_oui')) {
                    $paysans_mdr_all = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), null);
                    $paysans_mdr_all_ID = [];
                    foreach ($paysans_mdr_all as $paysan) {
                        array_push($paysans_mdr_all_ID, $paysan->getId());
                    }

                    $paysans_mdr_adh_oui = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), 1);
                    $paysans_mdr_adh_oui_ID = [];
                    foreach ($paysans_mdr_adh_oui as $paysan) {
                        array_push($paysans_mdr_adh_oui_ID, $paysan->getId());
                    }

                    $this->data['paysans'] = [];
                    foreach ($paysans_mdr_all as $paysan) {
                        if (!in_array($paysan->getId(), $paysans_mdr_adh_oui_ID)) {
                            array_push($this->data['paysans'], $paysan);
                        }
                    }
                } elseif (!$this->input->post('adh_non') && $this->input->post('adh_oui')) {
                    $this->data['paysans'] = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), 1);
                } else {
                    $this->data['paysans'] = $paysanRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('tp'), $this->input->post('en_recherche_partenariat'), null);
                }

                foreach ($this->data['paysans'] as $paysan) {
                    $this->data['emails'] .= $paysan->getEmail().',';
                }
            } elseif ('amap' == $this->input->post('qui')) {
                if ($this->input->post('adh_non') && !$this->input->post('adh_oui')) {
                    $amap_mdr_all = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), null);
                    $amap_mdr_all_ID = [];
                    foreach ($amap_mdr_all as $amap) {
                        array_push($amap_mdr_all_ID, $amap->getId());
                    }

                    $amap_mdr_adh_oui = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), 1);
                    $amap_mdr_adh_oui_ID = [];
                    foreach ($amap_mdr_adh_oui as $amap) {
                        array_push($amap_mdr_adh_oui_ID, $amap->getId());
                    }

                    $this->data['amap'] = [];
                    foreach ($amap_mdr_all as $amap) {
                        if (!in_array($amap->getId(), $amap_mdr_adh_oui_ID)) {
                            array_push($this->data['amap'], $amap);
                        }
                    }
                } elseif (!$this->input->post('adh_non') && $this->input->post('adh_oui')) {
                    $this->data['amap'] = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), 1);
                } else {
                    $this->data['amap'] = $amapRepo->publipostage_mdr($this->input->post('reg'), $this->input->post('dep'), $this->input->post('res'), $this->input->post('en_recherche_partenariat'), null);
                }

                foreach ($this->data['amap'] as $amap) {
                    $c = null;
                    $president = null;
                    // CORRESPONDANT RÉSEAU ----------------------------------------------
                    $c = $amap->getAmapienRefReseau();
                    if ($c) {
                        $this->data['emails'] .= $c->getEmail().',';
                    } else {
                        $president = $amap->getCollectifs()->matching(\PsrLib\ORM\Repository\AmapCollectifRepository::criteriaPresident())->first();
                        if ($president) {
                            $this->data['emails'] .= $president->getAmapien()->getEmail().',';
                        }
                    }
                }
            }
            $this->loadViewWithTemplate('publipostage/form_1', $this->data, 'template', [
                'header_publipostages' => 1,
            ]);
        } else {
            $this->loadViewWithTemplate('publipostage/form_1', $this->data, 'template', [
                'header_publipostages' => 1,
            ]);
        }
    }

    public function etiquette()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_MAILSHOT);

        $this->load->library('Pdf');
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(7.2, 13.1, 0);
        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)

        // set font
        $pdf->SetFont('courier', '', 9);
        $pdf->SetPrintHeader(false);
        // add a page
        $pdf->AddPage();
        // set cell padding
        $pdf->setCellPaddings(3, 3, 3, 3);
        // set cell margins
        $pdf->setCellMargins(0, 0, 2.5, 0);
        // set color for background
        $pdf->SetFillColor(255, 255, 127);
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

        $i = 1;
        $x = 0;
        $n = 0;
        $z = 0;

        if (isset($this->data['paysans'])) {
            /** @var \PsrLib\ORM\Entity\Paysan $paysan */
            foreach ($this->data['paysans'] as $paysan) {
                $payVille = $paysan->getVille();
                $ferme = $paysan->getFerme();
                $fermeVille = $ferme->getVille();

                if (isset($paysan) && 'nc' != strtolower(trim($paysan->getNom())) && 'nc' != strtolower(trim($paysan->getPrenom())) && ($paysan->getLibAdr() && 'nc' != strtolower(trim($paysan->getLibAdr()))) && null !== $payVille) {
                    $txt = null === ucfirst(strtolower($paysan->getNom())).' '.ucfirst(strtolower($paysan->getPrenom())).'
'.$paysan->getLibAdr().'
'.$payVille ? '' : $payVille->getCpString().' '.$payVille->getNom();
                } elseif (isset($paysan) && 'nc' != strtolower(trim($paysan->getNom())) && 'nc' != strtolower(trim($paysan->getPrenom())) && ($ferme->getLibAdr() && 'nc' != strtolower(trim($ferme->getLibAdr()))) && null !== $fermeVille) {
                    $txt = null === ucfirst(strtolower($paysan->getNom())).' '.ucfirst(strtolower($paysan->getPrenom())).'
'.$ferme->getLibAdr().'
'.$fermeVille ? '' : $fermeVille->getCpString().' '.$fermeVille->getNom();
                } else {
                    $txt = ucfirst(strtolower($paysan->getNom())).' '.ucfirst(strtolower($paysan->getPrenom())).'
Le paysan et sa ferme ne possèdent pas d\'adresse complète correctement formatée.';
                    $z = 1; // Couleur de la cellule : jaune
                }
                // retour à la ligne au bout de 3 cellules sur une ligne
                if (3 == $i) {
                    $x = 1;
                }

                $pdf->MultiCell(63.5, 33.9, $txt, 0, 'L', $z, $x, '', '', true);
                // Nouvelle ligne : réinitialisation de $i et de $x
                if (3 == $i) {
                    $i = 1;
                    $x = 0;
                } else {
                    ++$i;
                }

                // Nouvelle page au bout de 24 étiquettes ------------------------------------
                ++$n;
                if (24 == $n) {
                    $pdf->AddPage();
                    $n = 0;
                }

                $z = 0; // Réinitialisation de la couleur de la cellule : blanc
            }
        }

        // TXT pour une AMAP -----------------------------------------------------------
        // NOM prénom du correspondant réseau + adresse complète
        // OU 1er de la liste du collectif OU message d'erreur
        if (isset($this->data['amap'])) {
            /** @var \PsrLib\ORM\Entity\Amap $amap */
            foreach ($this->data['amap'] as $amap) {
                $txt = null;
                $c = null;
                $president = null;
                // CORRESPONDANT RÉSEAU ------------------------------------------------------
                $c = $amap->getAmapienRefReseau();
                if ($c) {
                    $amapienVille = $c->getVille();
                    $amapienCp = null === $amapienVille ? null : $amapienVille->getCpString();
                    $amapienNomVille = null === $amapienVille ? null : $amapienVille->getNom();
                    // $txt = $amap->getNom().'
                    // L\'AMAP ne possède pas de correspondant réseau AURA.';
                    // $z = 1; // Couleur de la cellule : jaune
                    // ADRESSE 1
                    if ('nc' != strtolower(trim($c->getNom())) && 'nc' != strtolower(trim($c->getPrenom())) && ($c->getLibAdr1() && 'nc' != strtolower(trim($c->getLibAdr1()))) && $amapienCp && $amapienNomVille) {
                        $txt = strtoupper($c->getNom()).' '.ucfirst(strtolower($c->getPrenom())).'
'.$amap->getNom().'
'.$c->getLibAdr1().'
'.$amapienCp.' '.$amapienNomVille;
                    } // ADRESSE 2
                    elseif ('nc' != strtolower(trim($c->getNom())) && 'nc' != strtolower(trim($c->getPrenom())) && ($c->getLibAdr2() && 'nc' != strtolower(trim($c->getLibAdr2()))) && $amapienCp && $amapienNomVille) {
                        $txt = strtoupper($c->getNom()).' '.ucfirst(strtolower($c->getPrenom())).'
'.$amap->getNom().'
'.$c->getLibAdr2().'
'.$amapienCp.' '.$amapienNomVille;
                    }
                }
                // Si toujours pas de texte (pas de correspondant ou il est mal formaté)------
                if (!$txt) {
                    // $txt = $amap->getNom().'
                    // Le correspondant réseau AURA de l\'AMAP ne possède pas une adresse complète correctement formatée.';
                    // $z = 1; // Couleur de la cellule : jaune
                    $president = $amap->getCollectifs()->matching(\PsrLib\ORM\Repository\AmapCollectifRepository::criteriaPresident())->first();

                    if ($president) {
                        $president = $president->getAmapien();
                        $amapienVille = $president->getVille();
                        $amapienCp = $amapienCp;
                        $amapienNomVille = $amapienNomVille;
                        // ADRESSE 1
                        if ('nc' != strtolower(trim($president->getNom())) && 'nc' != strtolower(trim($president->getPrenom())) && ($president->getLibAdr1() && 'nc' != strtolower(trim($president->getLibAdr1()))) && $amapienCp && $amapienNomVille) {
                            $txt = strtoupper($president->getNom()).' '.ucfirst(strtolower($president->getPrenom())).'
'.$amap->getNom().'
'.$president->getLibAdr1().'
'.$amapienCp.' '.$amapienNomVille;
                        } // ADRESSE 2
                        elseif ('nc' != strtolower(trim($president->getNom())) && 'nc' != strtolower(trim($president->getPrenom())) && ($president->getLibAdr2() && 'nc' != strtolower(trim($president->getLibAdr2()))) && $amapienCp && $amapienNomVille) {
                            $txt = strtoupper($president->getNom()).' '.ucfirst(strtolower($president->getPrenom())).'
'.$amap->getNom().'
'.$president->getLibAdr2().'
'.$amapienCp.' '.$amapienNomVille;
                        } else {
                            $txt = $amap->getNom().'
Le président de l\'AMAP ne possède pas une adresse complète correctement formatée.';
                            $z = 1; // Couleur de la cellule : jaune
                        }
                    } else {
                        $txt = $amap->getNom().'
Le correspondant réseau AURA ne possède pas une adresse complète correctement formatée et L\'AMAP ne possède aucun membre dans son collectif.';
                        $z = 1; // Couleur de la cellule : jaune
                    }
                }
                // retour à la ligne au bout de 3 cellules sur une ligne
                if (3 == $i) {
                    $x = 1;
                }

                $pdf->MultiCell(63.5, 33.9, $txt, 0, 'L', $z, $x, '', '', true);
                // Nouvelle ligne : réinitialisation de $i et de $x
                if (3 == $i) {
                    $i = 1;
                    $x = 0;
                } else {
                    ++$i;
                }

                // Nouvelle page au bout de 24 étiquettes ------------------------------------
                ++$n;
                if (24 == $n) {
                    $pdf->AddPage();
                    $n = 0;
                }
                $z = 0; // Réinitialisation de la couleur de la cellule : blanc
            }
        }
        // $pdf->Ln(4);
        // move pointer to last page
        $pdf->lastPage();
        //Close and output PDF document
        $pdf->Output('etiquettes.pdf', 'D');
    }
}
