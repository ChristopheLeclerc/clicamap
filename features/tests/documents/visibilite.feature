# language: fr
Fonctionnalité: Comme utilisateur, lors de l'ajout d'un document je devrais pouvoir le visualiser selon sa visibilité

  Contexte:
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Et que je remplis "nom" avec "Nom du nouveau document"
    Et que je remplis "lien" avec "http://www.amap-aura.org"

  @database
  Plan du Scénario: Visibilité portail
    Quand je coche "Page d'accueil (utilisateur non connecté)"
    Et que je presse "Sauvegarder"
    Alors je me déconnecte
    Et je vais sur "/"
    Et je devrais voir le lien "Nom du nouveau document" vers "http://www.amap-aura.org"
    Et je suis authentifié comme "<statut>" avec le mail "<email>"
    Et je suis sur "/documentation"
    Alors je ne devrais pas voir "Nom du nouveau document"
    Exemples:
      | statut  | email                         |
      | paysan  | paysan@test.amap-aura.org     |
      | amapien | amap@test.amap-aura.org       |
      | amapien | amapienref@test.amap-aura.org |
      | amapien | amapien@test.amap-aura.org    |

  @database
  Plan du Scénario: Visibilié paysan
    Quand je coche "Paysan"
    Et que je presse "Sauvegarder"
    # Cas valide
    Alors je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Et je vais sur "/documentation"
    Et je devrais voir le lien "Nom du nouveau document" vers "http://www.amap-aura.org"
    # Visibilité portail
    Et je me déconnecte
    Et je vais sur "/"
    Et je ne devrais pas voir "Nom du nouveau document"
    # Visibilité autres utilisateurs
    Et je suis authentifié comme "<statut>" avec le mail "<email>"
    Et je suis sur "/documentation"
    Alors je ne devrais pas voir "Nom du nouveau document"
    Exemples:
      | statut  | email                         |
      | amapien | admin@test.amap-aura.org      |
      | amapien | amapienref@test.amap-aura.org |
      | amapien | amap@test.amap-aura.org       |
      | amapien | amapien@test.amap-aura.org    |

  @database
  Plan du Scénario: Visibilié amap
    Quand je coche "AMAP"
    Et que je presse "Sauvegarder"
    # Cas valide
    Alors je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et je vais sur "/documentation"
    Et je devrais voir le lien "Nom du nouveau document" vers "http://www.amap-aura.org"
    # Visibilité portail
    Et je me déconnecte
    Et je vais sur "/"
    Et je ne devrais pas voir "Nom du nouveau document"
    # Visibilité autres utilisateurs
    Et je suis authentifié comme "<statut>" avec le mail "<email>"
    Et je suis sur "/documentation"
    Alors je ne devrais pas voir "Nom du nouveau document"
    Exemples:
      | statut  | email                         |
      | amapien | admin@test.amap-aura.org      |
      | amapien | amapienref@test.amap-aura.org |
      | amapien | amapien@test.amap-aura.org    |
      | paysan  | paysan@test.amap-aura.org     |

  @database
  Plan du Scénario: Visibilié amapien référent
    Quand je coche "AMAPien référent"
    Et que je presse "Sauvegarder"
    # Cas valide
    Alors je suis authentifié comme "amapien" avec le mail "amapienref@test.amap-aura.org"
    Et je vais sur "/documentation"
    Et je devrais voir le lien "Nom du nouveau document" vers "http://www.amap-aura.org"
    # Visibilité portail
    Et je me déconnecte
    Et je vais sur "/"
    Et je ne devrais pas voir "Nom du nouveau document"
    # Visibilité autres utilisateurs
    Et je suis authentifié comme "<statut>" avec le mail "<email>"
    Et je suis sur "/documentation"
    Alors je ne devrais pas voir "Nom du nouveau document"
    Exemples:
      | statut  | email                      |
      | amapien | admin@test.amap-aura.org   |
      | amapien | amapien@test.amap-aura.org |
      | paysan  | paysan@test.amap-aura.org  |

  @database
  Plan du Scénario: Visibilié amapien
    Quand je coche "AMAPien"
    Et que je presse "Sauvegarder"
    # Cas valide
    Alors je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Et je vais sur "/documentation"
    Et je devrais voir le lien "Nom du nouveau document" vers "http://www.amap-aura.org"
    # Visibilité portail
    Et je me déconnecte
    Et je vais sur "/"
    Et je ne devrais pas voir "Nom du nouveau document"
    # Visibilité autres utilisateurs
    Et je suis authentifié comme "<statut>" avec le mail "<email>"
    Et je suis sur "/documentation"
    Alors je ne devrais pas voir "Nom du nouveau document"
    Exemples:
      | statut  | email                         |
      | amapien | admin@test.amap-aura.org      |
      | amapien | amapienref@test.amap-aura.org |
      | paysan  | paysan@test.amap-aura.org     |
