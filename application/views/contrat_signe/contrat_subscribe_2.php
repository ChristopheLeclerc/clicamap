<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
/** @var \PsrLib\ORM\Entity\Ferme $ferme */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraison_lieu */
/** @var \PsrLib\ORM\Entity\ModeleContratDatesReglement[] $dates_reglement */
/** @var \Money\Money $total */
/** @var string[] $reglements_input */
/** @var \PsrLib\ORM\Entity\ContratDatesReglement[] $reglements_existants */
/** @var \PsrLib\ORM\Entity\Contrat $contrat */
/** @var string $url_retour */
?>


<?php
/**
 * @param $lineCount
 * @param \PsrLib\ORM\Entity\ModeleContratDatesReglement[] $dates_reglement
 * @param \PsrLib\ORM\Entity\ModeleContratDatesReglement   $selectedDateId
 * @param null|mixed                                       $selectedDate
 *
 * @return string
 */
function printDateSelect($lineCount, $dates_reglement, $selectedDate = null)
{
    $res = "<tr><td><select class='form-control' name='reglement[".$lineCount."]' ><option value=''></option>";
    foreach ($dates_reglement as $date) {
        $selectedDateId = null === $selectedDate ? null : $selectedDate->getId();
        $selected = $date->getId() === $selectedDateId ? 'selected' : '';
        if (1 === count($dates_reglement)) {
            $selected = 'selected';
        }
        $res .= '<option value=\''
                .$date->getId()
                .'\''
                .$selected
                .'>'
                .date_format_french($date->getDate())
                .'</option>';
    }
    $res .= "</select></td><td><div class='input-group'><input class='form-control text-right' disabled/><div class='input-group-addon'>€</div></div></td>";

    return $res;
}?>

<h3>Étape 2 - Mode et fréquence de règlement</h3>

<h5>
    <?php if ('/contrat_signe/contrat_own_new/' === $url_retour): ?>
        <a href="<?= site_url('/contrat_signe/contrat_own_new/'); ?>">Les nouveaux contrats disponibles</a>
    <?php elseif (str_contains($url_retour, 'contrat_signe')): ?>
        <a href="<?= $url_retour; ?>">Contrats signés</a>
    <?php else: ?>
        <a href="<?= $url_retour; ?>">Mes contrats existants</a>
    <?php endif; ?>
    <i class="glyphicon glyphicon-menu-right"></i>
    Souscription
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('/contrat_signe/contrat_subscribe_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 2
</h5>

<div class="well well-sm">
    <form method="POST">
        <div class="row">

            <div class="col-xs-6">
                <div class="form-group">
                    <label>Nombre de règlements</label>
                    <input
                        id="nb_reglements"
                        name="nb_reglements"
                        class="form-control"
                        type="number"
                        min="1"
                        max="<?=$mc->getReglementNbMax(); ?>"
                        required
                        value="<?=set_value('nb_reglements', 0 === count($reglements_existants) ? '' : count($reglements_existants)); ?>"
                        />
                </div>
            </div>

            <div class="col-xs-8">
                <div class="form-group">
                    <label>Modes de règlement</label>
                    <?=form_error('mode'); ?>
                    <?php $modes = $mc->getReglementType(); ?>
                    <?php foreach ($modes as $mode) : ?>
                        <div class="checkbox">
                            <label>
                                <input
                                    name="mode"
                                    type="radio"
                                    value="<?=$mode; ?>"
                                    <?php $radioChecked = str_contains($contrat->getReglementType() ?? '', $mode) || (null === $contrat->getReglementType() && 1 === count($modes)); ?>
                                    <?=set_radio('mode', $mode, $radioChecked); ?>
                                > <?=\PsrLib\ORM\Entity\ModeleContrat::getModeLabel($mode); ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="col-xs-4">
                <label>Modalités de règlement :</label> <br />
                <?=nl2br($mc->getReglementModalite()); ?>
            </div>

            <div class="col-xs-6">
                <?=form_error('reglement[]'); ?>
                <table class="table" id="reglements" data-select-template="<?=printDateSelect('__lineCount__', $dates_reglement); ?>">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Montant à régler</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (null !== $reglements_input) {
                                $lineCount = 0;
                                foreach ($reglements_input as $inputDateId) {
                                    echo printDateSelect($lineCount, $dates_reglement, $inputDateId);
                                    ++$lineCount;
                                }
                            } elseif (null !== $reglements_existants) {
                                $lineCount = 0;
                                foreach ($reglements_existants as $reglements_existant) {
                                    echo printDateSelect($lineCount, $dates_reglement, $reglements_existant->getModeleContratDatesReglement());
                                    ++$lineCount;
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label>Montant total :</label>
                    <div class="input-group">
			<input
                            class="form-control text-right"
                            disabled
                            value="<?=\PsrLib\Services\MoneyHelper::toString($total); ?>"
			/>
                        <div class="input-group-addon">€</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <button
                    style="margin-left: 10px"
                    type="submit"
                    class="btn btn-success pull-right"
                >
                    Suivant
                </button>

                <a
                    href="<?=site_url('/contrat_signe/contrat_subscribe_1'); ?>"
                    class="btn btn-default pull-right"
                >
                    Étape précédente
                </a>
            </div>
        </div>

    </form>

</div>

<script>
    $(document).ready(function () {
      var lineCounter = <?=count($reglements_input); ?>;

      $('#nb_reglements').on('change', function () {
        var newValue = parseInt($(this).val());

        if(isNaN(newValue)) {
          return;
        }

        var nbChild = $('#reglements tbody').children().length;
        var i = 0;

        while(newValue + i < nbChild) {
          deleteLine();
          i++;
        }

        while(newValue - i > nbChild) {
          newLine();
          i++;
        }

        updateAmounts(newValue);

      })
        .trigger('change');

      function newLine() {
        var selectTemplate = $('#reglements').data('select-template');
        selectTemplate = selectTemplate.replace('__lineCount__', lineCounter);

        $('#reglements tbody')
          .append(selectTemplate);

        lineCounter++;
      }

      function deleteLine() {
        $('#reglements tbody tr:last-child').remove();
      }

      function updateAmounts(nbReglements) {
        $.ajax('/contrat_signe/ajax_calc_prix?nb_reglements=' + nbReglements, {
          success: function (res) {
            if(res === "") {
              return;
            }
            var data = JSON.parse(res);

            var i = 0;
            $('#reglements input')
              .each(function () {
                $(this).val(data[i]);
                i++;
              });

          }
        })
      }

    });
</script>
