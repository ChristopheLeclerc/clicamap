<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
/** @var \Symfony\Component\Form\FormView $form */
?>
<h3>Ajouter des types de production</h3>

<div class="alert alert-info">Plusieurs produits peuvent être proposés dans cette rubrique. Vous avez la possibilité de
    les ajouter un par un en passant par les champs d'ajout ci-dessous. Une fois terminé vos ajouts, cliquez sur
    Terminer.
</div>

<?= form_open_multipart('');

?>

<div class="well">

    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-3">
                <label for="type_production" class="control-label">Produit proposé :</label>
                <?= render_form_select_widget($form['typeProduction']); ?>
            </div>

            <div class="form-group col-md-3">
                <label for="nb_amap_part" class="control-label">Nombre d'AMAP livrées/s : </label>
                <?= render_form_number_widget($form['nbAmapLivrees'], '0', '0.01'); ?>
            </div>

            <div class="form-group col-md-3">
                <label for="nb_contrats_totalstep" class="control-label">Nombre total de contrats en AMAP : </label>
                <?= render_form_number_widget($form['nbContratsTotalAmap'], '0', '1'); ?>
            </div>

            <div class="form-group col-md-3">
                <label for="ca_annuel" class="control-label">Chiffre d'affaire total en AMAP : </label>
                <?= render_form_number_widget($form['caAnnuelAmap'], '0', '0.01'); ?>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-4">
                <label for="p_ca_total" class="control-label">Part du CA total (sur l'année écoulée en %) : </label>
                <?= render_form_number_widget($form['caTotal'], '0', '0.01'); ?>
            </div>

            <div class="form-group col-md-4">
                <label for="attest_cert" class="control-label">Attestation de certification (.pdf < 20Mo) : </label>
                <?= render_form_file_widget($form['certification']); ?>
                <small id="fileHelp" class="form-text text-muted">Possibilité de télécharger un scan de l'attestation de
                    certification.
                </small>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <input type="submit" class="btn btn-success pull-right" value="Ajouter le type de production"
                   style="margin-left:5px;">
            <a href="<?= site_url('ferme/type_production_display/'.$ferme->getId()); ?>"
               class="btn btn-default pull-right" style="margin-right:5px;">Terminer</a>
        </div>
    </div>

</div>

</form>
