<?php

namespace PsrLib\AliceProvider;

use Carbon\Carbon;

class CarbonProvider
{
    public function carbon(string $date): Carbon
    {
        return new Carbon($date);
    }

    public function carbon_timestamp_calculate(int $timestamp, int $delta = 0, string $factor = ''): Carbon
    {
        return Carbon::createFromTimestamp($timestamp + $delta * (int) $factor);
    }
}
