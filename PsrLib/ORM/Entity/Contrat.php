<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ORM\Entity\Files\ContratPdf;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * AkContrat.
 *
 * @ORM\Table(name="ak_contrat", indexes={@ORM\Index(name="IND_CONTRAT_FK_MODELE_CONTRAT", columns={"c_fk_modele_contrat_id"})})
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\ContratRepository")
 */
class Contrat
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="c_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizardContract"})
     */
    private $id;

    /**
     * @var ?DateTime
     *
     * @ORM\Column(name="c_date_creation", type="datetime", nullable=true)
     * @Groups({"wizardContract"})
     */
    private $dateCreation;

    /**
     * @var ?DateTime
     *
     * @ORM\Column(name="c_date_modification", type="datetime", nullable=true)
     * @Groups({"wizardContract"})
     */
    private $dateModification;

    /**
     * @var ?ModeleContrat
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\ModeleContrat", inversedBy="contrats")
     * @ORM\JoinColumn(name="c_fk_modele_contrat_id", referencedColumnName="mc_id")
     * @Groups({"wizardContract"})
     */
    private $modeleContrat;

    /**
     * @var ?Amapien
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="contrats")
     * @ORM\JoinColumn(name="c_fk_amapien_id", referencedColumnName="a_id")
     * @Groups({"wizardContract"})
     */
    private $amapien;

    /**
     * @var null|string
     *
     * @ORM\Column(name="c_reglement_type", type="string", length=255, nullable=true)
     * @Groups({"wizardContract"})
     */
    private $reglementType;

    /**
     * @var null|int
     *
     * @ORM\Column(name="c_contrat_deplacements_effectues", type="integer", nullable=true)
     * @Groups({"wizardContract"})
     */
    private $deplacementsEffectues;

    /**
     * @var null|int
     *
     * @ORM\Column(name="c_contrat_report_effectues", type="integer", nullable=true)
     * @Groups({"wizardContract"})
     */
    private $reportEffectues;

    /**
     * @var ArrayCollection<ContratCellule>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ContratCellule", mappedBy="contrat", cascade="persist", orphanRemoval=true)
     * @Groups({"wizardContract"})
     */
    private $cellules;

    /**
     * @var ArrayCollection<ContratDatesReglement>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ContratDatesReglement", mappedBy="contrat", cascade="persist", orphanRemoval=true)
     * @Groups({"wizardContract"})
     */
    private $datesReglements;

    /**
     * @var ?ContratPdf
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Files\ContratPdf", cascade="persist", orphanRemoval=true)
     */
    private $pdf;

    /**
     * Contrat constructor.
     */
    public function __construct()
    {
        $this->cellules = new ArrayCollection();
        $this->datesReglements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): Contrat
    {
        $this->id = $id;

        return $this;
    }

    public function getModeleContrat(): ?ModeleContrat
    {
        return $this->modeleContrat;
    }

    public function setModeleContrat(?ModeleContrat $modeleContrat): Contrat
    {
        $this->modeleContrat = $modeleContrat;

        return $this;
    }

    public function getDateCreation(): ?DateTime
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?DateTime $dateCreation): Contrat
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateModification(): ?DateTime
    {
        return $this->dateModification;
    }

    public function setDateModification(?DateTime $dateModification): Contrat
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): Contrat
    {
        $this->amapien = $amapien;

        return $this;
    }

    public function getReglementType(): ?string
    {
        return $this->reglementType;
    }

    public function setReglementType(?string $reglementType): Contrat
    {
        $this->reglementType = $reglementType;

        return $this;
    }

    public function getDeplacementsEffectues(): ?int
    {
        return $this->deplacementsEffectues;
    }

    public function setDeplacementsEffectues(?int $deplacementsEffectues): Contrat
    {
        $this->deplacementsEffectues = $deplacementsEffectues;

        return $this;
    }

    public function getReportEffectues(): ?int
    {
        return $this->reportEffectues;
    }

    public function setReportEffectues(?int $reportEffectues): Contrat
    {
        $this->reportEffectues = $reportEffectues;

        return $this;
    }

    /**
     * Add cellule.
     *
     * @return Contrat
     */
    public function addCellule(ContratCellule $cellule)
    {
        $this->cellules[] = $cellule;
        $cellule->setContrat($this);

        return $this;
    }

    /**
     * Remove cellule.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCellule(ContratCellule $cellule)
    {
        $cellule->setContrat(null);

        return $this->cellules->removeElement($cellule);
    }

    /**
     * Get cellules.
     *
     * @return Collection
     */
    public function getCellules()
    {
        return $this->cellules;
    }

    /**
     * Add cellule.
     *
     * @return Contrat
     */
    public function addDatesReglement(ContratDatesReglement $date)
    {
        $this->datesReglements[] = $date;
        $date->setContrat($this);

        return $this;
    }

    /**
     * Remove cellule.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDatesReglement(ContratDatesReglement $date)
    {
        $date->setContrat(null);

        return $this->datesReglements->removeElement($date);
    }

    /**
     * @return Collection
     */
    public function getDatesReglements()
    {
        return $this->datesReglements;
    }

    public function getPdf(): ?ContratPdf
    {
        return $this->pdf;
    }

    public function setPdf(?ContratPdf $pdf): Contrat
    {
        $this->pdf = $pdf;

        return $this;
    }
}
