<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
function addErrors(string $renderedField, Symfony\Component\Form\FormView $formView)
{
    $translator = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
        ->get(\Symfony\Component\Translation\Translator::class)
    ;
    $errors = $formView->vars['errors'];
    if ($errors->count() > 0) {
        foreach ($errors as $error) {
            $renderedField .= sprintf(
                '<div class="alert alert-danger">%s</div>',
                $translator->trans($error->getMessageTemplate(), $error->getMessageParameters())
            );
        }
    }

    return $renderedField;
}

if (!function_exists('render_form_text_widget')) {
    function render_form_text_widget(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<input class="form-control %s" type="text" name="%s" value="%s" %s %s %s/>',
            $formView->vars['attr']['class'] ?? null,
            $formView->vars['full_name'],
            $formView->vars['value'],
            $formView->vars['required'] ? 'required="required"' : null,
            isset($formView->vars['attr']['readonly']) ? 'readonly' : null,
            sprintf('placeholder="%s"', $formView->vars['attr']['placeholder'] ?? null)
        );

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_number_widget')) {
    function render_form_number_widget(Symfony\Component\Form\FormView $formView, string $min = null, string $step = null, string $max = null)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<input class="form-control %s" type="number" name="%s" value="%s" %s %s %s %s %s %s/>',
            $formView->vars['attr']['class'] ?? null,
            $formView->vars['full_name'],
            $formView->vars['value'],
            $formView->vars['required'] ? 'required="required"' : null,
            isset($formView->vars['attr']['readonly']) ? 'readonly' : null,
            sprintf('placeholder="%s"', $formView->vars['attr']['placeholder'] ?? null),
            null === $min ? null : sprintf('min="%s"', $min),
            null === $step ? null : sprintf('step="%s"', $step),
            null === $max ? null : sprintf('max="%s"', $max)
        );

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_file_widget')) {
    function render_form_file_widget(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<input type="file" class="form-control-file %s" name="%s" value="%s"/>',
            $formView->vars['attr']['class'] ?? null,
            $formView->vars['full_name'],
            $formView->vars['value']
        );

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_hidden_widget')) {
    function render_form_hidden_widget(Symfony\Component\Form\FormView $formView, $value = null)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<input class="%s" type="hidden" name="%s" value="%s"/>',
            $formView->vars['attr']['class'] ?? null,
            $formView->vars['full_name'],
            $value ?? $formView->vars['value']
        );

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_textarea_widget')) {
    function render_form_textarea_widget(Symfony\Component\Form\FormView $formView, $value = null)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<textarea class="form-control %s" name="%s" %s %s>%s</textarea>',
            $formView->vars['attr']['class'] ?? null,
            $formView->vars['full_name'],
            isset($formView->vars['attr']['rows']) ? 'rows="'.$formView->vars['attr']['rows'].'"' : null,
            sprintf('placeholder="%s"', $formView->vars['attr']['placeholder'] ?? null),
            $value ?? $formView->vars['value']
        );

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_datepicker_widget')) {
    function render_form_datepicker_widget(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<input class="form-control datepicker %s" type="text" name="%s" value="%s" data-date-format="yyyy-mm-dd" %s %s/>',
            $formView->vars['attr']['class'] ?? null,
            $formView->vars['full_name'],
            $formView->vars['value'],
            $formView->vars['required'] ? 'required="required"' : null,
            isset($formView->vars['attr']['readonly']) ? 'readonly' : null
        );

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_radio_widget')) {
    function render_form_radio_widget(Symfony\Component\Form\FormView $formView, string $rowClass = null)
    {
        $formView->setRendered();
        $ret = '';
        /** @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice */
        foreach ($formView->vars['choices'] as $choice) {
            $ret .= sprintf(
                '<div class="form-group %s"><label class="control-label radio-inline"><input type="radio" name="%s" value="%s" %s %s>%s</label></div>',
                $rowClass,
                $formView->vars['full_name'],
                $choice->value,
                $formView->vars['value'] === $choice->value ? 'checked="checked"' : '',
                $formView->vars['required'] ? 'required="required"' : null,
                $choice->label
            );
        }

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_radio_widget_inline')) {
    function render_form_radio_widget_inline(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = '';
        /** @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice */
        foreach ($formView->vars['choices'] as $choice) {
            $ret .= sprintf(
                '<label class="control-label radio-inline"><input type="radio" name="%s" value="%s" %s %s>%s</label>',
                $formView->vars['full_name'],
                $choice->value,
                $formView->vars['value'] === $choice->value ? 'checked="checked"' : '',
                $formView->vars['required'] ? 'required="required"' : null,
                $choice->label
            );
        }

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_multiselect_widget')) {
    function render_form_multiselect_widget(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<select name="%s" class="form-control" multiple="multiple" %s %s %s>',
            $formView->vars['full_name'],
            isset($formView->vars['attr']['size']) ? 'size="'.$formView->vars['attr']['size'].'"' : null,
            isset($formView->vars['attr']['readonly']) && true === $formView->vars['attr']['readonly'] ? 'readonly=""' : null,
            isset($formView->vars['disabled']) && true === $formView->vars['disabled'] ? 'disabled=""' : null
        );

        /** @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice */
        foreach ($formView->vars['choices'] as $choice) {
            $ret .= sprintf(
                '<option value="%s" %s>%s</option>',
                $choice->value,
                in_array($choice->value, $formView->vars['value'], true) ? 'selected="selected"' : '',
                $choice->label
            );
        }
        $ret .= '</select>';

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_multiselect_expanded_widget')) {
    function render_form_multiselect_expanded_widget(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = '';

        /** @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice */
        foreach ($formView->vars['choices'] as $choice) {
            $ret .= sprintf(
                '<div class="checkbox"><label><input type="checkbox" name="%s[]" value="%s" %s>%s</label></div>',
                $formView->vars['full_name'],
                $choice->value,
                in_array($choice->value, $formView->vars['value'], true) ? 'checked="checked"' : '',
                $choice->label
            );
        }

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_select_widget')) {
    function render_form_select_widget(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<select name="%s" class="form-control" %s>',
            $formView->vars['full_name'],
            true === $formView->vars['disabled'] ? 'disabled' : null
        );

        if (null !== $formView->vars['placeholder']) {
            $ret .= sprintf(
                '<option>%s</option>',
                true === $formView->vars['placeholder'] ? '' : $formView->vars['placeholder']
            );
        }

        /** @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice */
        foreach ($formView->vars['choices'] as $choice) {
            $ret .= sprintf(
                '<option value="%s" %s>%s</option>',
                $choice->value,
                $formView->vars['value'] === $choice->value ? 'selected="selected"' : '',
                $choice->label
            );
        }
        $ret .= '</select>';

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_checkbox_widget')) {
    function render_form_checkbox_widget(Symfony\Component\Form\FormView $formView)
    {
        $formView->setRendered();
        $ret = sprintf(
            '<label for="%s" class="control-label">
                    <input type="checkbox" name="%s" value="%s" %s/>
                    %s
                </label>
            ',
            $formView->vars['full_name'],
            $formView->vars['full_name'],
            $formView->vars['value'],
            $formView->vars['data'] ? 'checked="checked"' : '',
            $formView->vars['label']
        );

        return addErrors($ret, $formView);
    }
}

if (!function_exists('render_form_global_errors')) {
    function render_form_global_errors(Symfony\Component\Form\FormView $formView)
    {
        return addErrors('', $formView);
    }
}
