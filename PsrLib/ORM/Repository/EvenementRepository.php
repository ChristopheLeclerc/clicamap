<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\Reseau;
use PsrLib\ORM\Entity\TypeProduction;

/**
 * @extends EntityRepository<Evenement>
 */
class EvenementRepository extends EntityRepository
{
    public function getEvementForUser(BaseUser $user): array
    {
        $ret = [];
        $ret['createur'] = [];
        $ret['evenement_all'] = [];

        /** @var Evenement[] $evenement_all */
        $evenement_all = $this->findAll();

        foreach ($evenement_all as $ev) {
            $tab = [];
            $tab[] = $ev->getId();
            $tab[] = $ev->getCreateur();
            if ('amap' === $ev->getCreateur()) {
                $tab[] = $ev->getAmap()->getNom();
            }
            $ret['createur'][] = $tab;
        }

        $evenement_1 = [];
        $evenement_2 = [];
        $evenement_3 = [];
        $evenement_4 = [];

        // SESSIONS --------------------------------------------------------------
        if ($user instanceof Amapien && $user->isAdmin()) {
            // SUPER ADMIN ---------------------------------------------------------
            if ($user->isSuperAdmin()) {
                $ret['evenement_all'] = $evenement_all;
            }

            // ADMINISTRATEUR RÉGION -----------------------------------------------
            elseif ($user->isAdminRegion()) {
                // 1. Les événements créés par l'admin reg ---------------------------
                $evenement_1 = $this->findBy([
                    'createur' => 'adm_reg',
                    'amapien' => $user,
                ]);

                // 2. Les événements qui doivent remonter à l'adm reg (créés par un adm dep)
                foreach ($evenement_all as $evenement) {
                    if ('adm_reg' === $evenement->getInfoCreateurReseau()) {
                        // a. Créé par un adm dep ----------------------------------------
                        $createur = $evenement->getAmapien();
                        if (null !== $createur) {
                            // On récupère tous les dep du créateur (un adm dep peut ê adm de plusieurs dep)
                            foreach ($createur->getAdminDepartments() as $dep) {
                                // Les départements du créateur ont-ils une jonction avec ceux de l'adm reg ?
                                if ($user->isAdminOf($dep)) {
                                    $evenement_2[] = $evenement;

                                    break;
                                }
                            }
                        } // a. Créé par une AMAP ------------------------------------------
                        elseif (null !== $evenement->getAmap()) {
                            $livraison_lieux = $evenement->getAmap()->getLivraisonLieux();
                            // La région du créateur (AMAP) est-elle la même que celle de l'admin reg ?
                            if ($user->isAdminOf($livraison_lieux->first())) {
                                $evenement_2[] = $evenement;
                            }
                        }
                    }
                }

                // 3. On décompile et dédoublonne le tout au sein de 'evenement_all'
                $evenement_all_all = [];
                $evenement_all_all[] = $evenement_1;
                $evenement_all_all[] = $evenement_2;
                foreach ($evenement_all_all as $evenement_all) {
                    foreach ($evenement_all as $evenement) {
                        $ret['evenement_all'][] = $evenement;
                    }
                }
            } // ADMINISTRATEUR DÉPARTEMENT ------------------------------------------
            elseif ($user->isAdminDepartment()) {
                // 1. Les événements créés par l'admin dép ---------------------------
                $evenement_1 = $this->findBy([
                    'createur' => 'adm_dep',
                    'amapien' => $user,
                ]);

                // 2. Les événements qui doivent remonter à l'adm dep (créés par un adm res ou une AMAP)
                foreach ($evenement_all as $evenement) {
                    if ('adm_dep' === $evenement->getInfoCreateurReseau()) {
                        // a. Créé par un adm rés local ----------------------------------
                        $amapien = $evenement->getAmapien();
                        if ($amapien) {
                            // On récupère tous les reseaux du créateur --------------------
                            /** @var Reseau[] $createur_res */
                            $createur_res = $amapien->getAdminReseaux();
                            foreach ($createur_res as $res) {
                                // Les réseaux du créateur ont-ils une jonction avec ceux de l'adm dep ?
                                if ($user->isAdminOf($res->getVilles()->first())) {
                                    $evenement_2[] = $evenement;

                                    break;
                                }
                            }
                        } // a. Créé par une AMAP ------------------------------------------
                        elseif ($evenement->getAmap()) {
                            $amap_departements = [];
                            /** @var AmapLivraisonLieu $ll */
                            foreach ($evenement->getAmap()->getLivraisonLieux() as $ll) {
                                $amap_departements[] = $ll->getVille()->getDepartement();
                            }
                            foreach ($amap_departements as $dep) {
                                // Les réseaux du créateur (AMAP) ont-ils une jonction avec ceux de l'adm dep ?
                                if ($user->isAdminOf($dep)) {
                                    $evenement_2[] = $evenement;

                                    break;
                                }
                            }
                        }
                    }
                }

                // 3. On décompile et dédoublonne le tout au sein de 'evenement_all'
                $evenement_all_all = [];
                $evenement_all_all[] = $evenement_1;
                $evenement_all_all[] = $evenement_2;
                foreach ($evenement_all_all as $evenement_all) {
                    foreach ($evenement_all as $evenement) {
                        $ret['evenement_all'][] = $evenement;
                    }
                }
            } // ADMINISTRATEUR RÉSEAU -----------------------------------------------
            elseif ($user->isAdminReseaux()) {
                // 1. Les événements créés par l'admin res ---------------------------
                $evenement_1 = $this->findBy([
                    'createur' => 'adm_res',
                    'amapien' => $user,
                ]);

                // 2. Les événements qui doivent remonter à l'adm res (créés par une AMAP)
                foreach ($evenement_all as $evenement) {
                    if ('adm_res' === $evenement->getInfoCreateurReseau()) {
                        // Créé par une AMAP ---------------------------------------------
                        if ($evenement->getAmap()) {
                            // Les réseaux du créateur (AMAP) ont-ils une jonction avec ceux de l'adm réseau ?
                            if ($user->isAdminOf($evenement->getAmap())) {
                                $evenement_2[] = $evenement;
                            }
                        }
                    }
                }

                // 3. On décompile et dédoublonne le tout au sein de 'evenement_all'
                $evenement_all_all = [];
                $evenement_all_all[] = $evenement_1;
                $evenement_all_all[] = $evenement_2;
                foreach ($evenement_all_all as $evenement_all) {
                    foreach ($evenement_all as $evenement) {
                        $ret['evenement_all'][] = $evenement;
                    }
                }
            }
        }

        // AMAP ------------------------------------------------------------------
        if ($user instanceof Amap) {
            // Les événements de l'AMAP
            $evenement_1 = $this->findBy([
                'createur' => 'amap',
                'amap' => $user,
            ]);

            // Les événements à destination des AMAP
            foreach ($evenement_all as $evenement) {
                if ('amap' !== $evenement->getCreateur()) {
                    // L'événement prévient directement des AMAP
                    // elseif
                    if ($evenement->getInfoAmap() && $evenement->getRelAmaps()->contains($user)) {
                        $evenement_4[] = $evenement;
                    }
                }
            }

            $evenement_all_all = [];
            $evenement_all_all[] = $evenement_1;
            $evenement_all_all[] = $evenement_2;
            $evenement_all_all[] = $evenement_3;
            $evenement_all_all[] = $evenement_4;
            foreach ($evenement_all_all as $evenement_all) {
                foreach ($evenement_all as $evenement) {
                    $ret['evenement_all'][] = $evenement;
                }
            }
        } // AMAPIENS ET RÉFÉRENTS RÉSEAUX -----------------------------------------
        elseif ($user instanceof Amapien && !$user->isAdmin()) {
            // Les événements de mon AMAP
            $evenement_1 = $this
                ->getForAmapienAmap($user)
            ;

            // Les événements à destination de mon AMAP
            foreach ($evenement_all as $evenement) {
                if ('amap' !== $evenement->getCreateur()) {
                    // L'événement prévient directement des AMAP
                    if ($evenement->getInfoAmap()) {
                        foreach ($evenement->getRelAmaps() as $amap) {
                            // Les départements de l'évènement et le département de mon AMAP
                            if ($amap->getId() == $user->getAmap()->getId()) {
                                $evenement_4[] = $evenement;
                            }
                        }
                    }
                }
            }

            $evenement_all_all = [];
            $evenement_all_all[] = $evenement_1;
            $evenement_all_all[] = $evenement_2;
            $evenement_all_all[] = $evenement_3;
            $evenement_all_all[] = $evenement_4;
            foreach ($evenement_all_all as $evenement_all) {
                foreach ($evenement_all as $evenement) {
                    $ret['evenement_all'][] = $evenement;
                }
            }
        } // PAYSANS ---------------------------------------------------------------
        elseif ($user instanceof Paysan) {
            $evenement_1 = $this->findBy([
                'createur' => 'amap',
                'infoFerme' => $user->getFerme(),
            ]);

            foreach ($evenement_all as $evenement) {
                if ('amap' !== $evenement->getCreateur()) {
                    // DÉPARTEMENTS ----------------------------------------------------
                    // L'événement prévient une / plusieurs type de production dans un / plusieurs départements
                    if ($evenement->getInfoDep() && $evenement->getInfoTp()) {
                        $tppIntersect = array_uintersect(
                            $evenement->getRelTypeProduction()->toArray(),
                            $user->getFerme()->getTypeProductionFromProduits(),
                            function (TypeProduction $a, TypeProduction $b) {
                                return $a->getId() === $b->getId();
                            }
                        );
                        if ($evenement->getRelDepartements()->contains($user->getDepartement())
                            && count($tppIntersect) > 0) {
                            $evenement_2[] = $evenement;
                        }
                    }
                    // RÉSEAUX ---------------------------------------------------------
                    // L'événement prévient une / plusieurs type de production dans un / plusieurs réseaux
                    elseif ($evenement->getInfoReseaux() && $evenement->getInfoTp()) {
                        $tppIntersect = array_uintersect(
                            $evenement->getRelTypeProduction()->toArray(),
                            $user->getFerme()->getTypeProductionFromProduits(),
                            function (TypeProduction $a, TypeProduction $b) {
                                return $a->getId() === $b->getId();
                            }
                        );

                        $villeMatch = false;
                        /** @var Reseau $reseau */
                        foreach ($evenement->getRelReseau() as $reseau) {
                            if ($reseau->getVilles()->contains($user->getFerme()->getVille())) {
                                $villeMatch = true;
                            }
                        }

                        if (count($tppIntersect) > 0
                            && $villeMatch) {
                            $evenement_2[] = $evenement;
                        }
                    }
                    // L'événement prévient une ou plusieurs fermes --------------------
                    elseif ($evenement->getInfoFermes()) {
                        if ($evenement->getRelFerme()->contains($user->getFerme())) {
                            $evenement_4[] = $evenement;
                        }
                    }
                }
            }

            $evenement_all_all = [];
            $evenement_all_all[] = $evenement_1;
            $evenement_all_all[] = $evenement_2;
            $evenement_all_all[] = $evenement_3;
            $evenement_all_all[] = $evenement_4;
            foreach ($evenement_all_all as $evenement_all) {
                foreach ($evenement_all as $evenement) {
                    $ret['evenement_all'][] = $evenement;
                }
            }
        }

        // force sort by reverse id
        usort($ret['evenement_all'], function (Evenement $ev1, Evenement $ev2) {
            return $ev2->getId() - $ev1->getId();
        });

        return $ret;
    }

    /**
     * @return Evenement[]
     */
    public function getForAmapienAmap(Amapien $amapien)
    {
        return $this
            ->createQueryBuilder('e')
            ->where('e.createur = \'amap\'')
            ->andWhere('e.infoAmapiens = true')
            ->andWhere('e.amap = :amap')
            ->setParameter('amap', $amapien->getAmap())
            ->getQuery()
            ->getResult()
        ;
    }
}
