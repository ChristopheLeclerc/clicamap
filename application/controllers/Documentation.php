<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'ContactTrait.php';

/**
 * Class Documentation.
 */
class Documentation extends AppController
{
    use ContactTrait;
    protected $data;

    /**
     * Documentation constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = [];

        $this->load->library('email');
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    /**
     * Index.
     *
     * @param null|mixed $print_contact
     */
    public function index($print_contact = null)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DOCUMENT_GETOWN);

        $currentUser = $this->getUser();

        $documents = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Document::class)
            ->findForUser($currentUser)
        ;

        // Manage contact form
        if ($print_contact) {
            $this->handleContactRequest('/documentation');
        }

        $this->loadViewWithTemplate(
            'documentation/index',
            ['documents' => $documents, 'print_contact_form' => $print_contact]
        );
    }

    public function contact()
    {
        return $this->index(true);
    }

    public function list_documents()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DOCUMENT_MANAGE);
        $documents = $this->em->getRepository(\PsrLib\ORM\Entity\Document::class)->findAll();

        $this->loadViewWithTemplate('documentation/list', ['documents' => $documents]);
    }

    /**
     * @param $document_id
     */
    public function form($document_id = null)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DOCUMENT_MANAGE);

        if (null === $document_id) {
            $document = new \PsrLib\ORM\Entity\Document();
        } else {
            $document = $this->findOrExit(\PsrLib\ORM\Entity\Document::class, $document_id);
        }

        $this->form_validation->set_rules('nom', '"Nom"', 'required|max_length[200]');
        $this->form_validation->set_rules('lien', '"Lien"', 'required|valid_url|max_length[200]');
        $this->form_validation->set_rules('permission_anonyme', '"Accueil"', 'document_permission_anonyme_max_count_check['.$document_id.']', [
            'document_permission_anonyme_max_count_check' => 'Trois documents maximum sont autorisés en page d\'accueil',
        ]);

        $errorPermission = false;
        if ($this->form_validation->run()) {
            $document->setNom($this->input->post('nom'));
            $document->setLien($this->input->post('lien'));
            $document->setPermissionAnonyme(null !== $this->input->post('permission_anonyme'));
            $document->setPermissionAdmin(null !== $this->input->post('permission_admin'));
            $document->setPermissionPaysan(null !== $this->input->post('permission_paysan'));
            $document->setPermissionAmap(null !== $this->input->post('permission_amap'));
            $document->setPermissionAmapienref(null !== $this->input->post('permission_amapienref'));
            $document->setPermissionAmapien(null !== $this->input->post('permission_amapien'));

            if ($document->nbPermissionsSelected() > 0) {
                if (null === $document->getId()) {
                    $this->em->persist($document);
                }
                $this->em->flush();

                if (null === $document_id) {
                    $this->addFlash(
                        'flash_document_form_confirm',
                        'Document correctement ajouté'
                    );
                } else {
                    $this->addFlash(
                        'flash_document_form_confirm',
                        'Document correctement modifié'
                    );
                }

                return redirect('documentation/list_documents');
            }

            $errorPermission = true;
        }

        $this->loadViewWithTemplate(
            'documentation/form',
            ['document' => $document, 'errorPermission' => $errorPermission]
        );
    }

    /**
     * @param $document_id
     */
    public function supprimer($document_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DOCUMENT_MANAGE);

        $document = $this->findOrExit(\PsrLib\ORM\Entity\Document::class, $document_id);
        $this->em->remove($document);
        $this->em->flush();

        return redirect('documentation/list_documents');
    }
}
