# language: fr
Fonctionnalité: Comme administrateur, je devrais pouvoir supprimer un document

  Contexte:
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Et que je remplis "nom" avec "Nom du nouveau document"
    Et que je remplis "lien" avec "http://www.amap-aura.org"
    Et que je coche "Page d'accueil (utilisateur non connecté)"
    Et que je presse "Sauvegarder"

  @database
  Scénario: Suppression du document
    Etant donné je suis sur "/documentation/list_documents"
    Quand je suis "Supprimer le document"
    Et que je suis "OUI"
    Alors je ne devrais pas voir "Nom du nouveau document"

  @database
  Scénario: Annulation suppression du document
    Etant donné je suis sur "/documentation/list_documents"
    Quand je suis "Supprimer le document"
    Et que je suis "NON"
    Alors je devrais voir "Nom du nouveau document"
