<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
?>

<script type="text/javascript">

  /*AUTOCOMPLÉTION*/
  $(document).ready(function () {

    $("#liv_autocomplete").autocomplete({
      source: <?= '"'.site_url('ajax/geocode_get_suggestion').'"'; ?>,
      dataType: "json",
      minLength: 4,
      appendTo: "#container_liv_autocomplete",
      select: function (event, ui) {

        // Maj des informations sur la page
        $("#liv_autocomplete").val(ui.item.label);
        $('input[name="amap_livraison_lieu[adresse]"]').val(ui.item.value.address);
        $('input[name="amap_livraison_lieu[gpsLatitude]"]').val(ui.item.value.lat.toString());
        $('input[name="amap_livraison_lieu[gpsLongitude]"]').val(ui.item.value.long.toString());
        $('input[name="amap_livraison_lieu[ville]"]').val(
          ui.item.value.cp.toString() + ', ' + ui.item.value.ville.toString()
        );

        // Stop la propagation de l'évènement
        return false;
      }
    });
  });

</script>

<h3>Les lieux de livraison</h3>

<?php

echo '<h5><a href="'.site_url('amap').'">Gestion des AMAP</a> <i class="glyphicon glyphicon-menu-right"></i> Création d\'une AMAP <i class="glyphicon glyphicon-menu-right"></i> Étape 2</h5>';

echo '<form method="POST" action="'.site_url('amap/livraison_lieu_creation').'">';

echo '<div class="alert alert-info">L\'ajout de lieux de livraison complémentaires se fait par la mise à jour de l\'AMAP rubrique "Gestion des AMAP" -> "Modifier l\'AMAP" -> "Les informations de livraison".</div>';

?>

<div class="row">
    <div class="col-md-12" id="livraison_lieu">

        <div class="entry">

            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12">
                        <label for="liv_lieu" class="control-label">Nom du lieu</label>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <?= render_form_text_widget($form['nom']); ?>
                    </div>
                </div>
            </div>

            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12">
                        <label for="liv_autocomplete" class="control-label">Adresse de livraison</label>
                    </div>
                    <div class="col-xs-12 form-group">

                        <div class="alert alert-info">
                            Utilisez le champ ci-dessous pour compléter l'adresse de manière automatique grâce aux
                            suggestions proposées.
                            Il est possible que la rue ou le numéro ne soit pas connu de l'outil. Dans ce cas, vous
                            pouvez soit modifier
                            manuellement le champ N° et rue ci-dessus, soit contribuer au projet <a target="_blank"
                                                                                                    href="https://wiki.openstreetmap.org/wiki/FR:Guide_du_d%C3%A9butant">Open
                                Street Map</a>
                            pour que votre adresse soit précisément localisée.
                        </div>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                            <input type="text" class="form-control" id="liv_autocomplete" name="liv_autocomplete"
                                   placeholder="Veuillez taper ici l'adresse et selectionner une suggestion"/>
                        </div>

                        <div id="container_liv_autocomplete" class="ui-autocomplete"
                             style="width:100%;font-size:11px;"></div>
                    </div>

                    <div class="col-xs-12">
                        <div class="alert alert-info">
                            Si vous ne connaissez pas l'adresse exacte, indiquez simplement la ville ou le lieu-dit et
                            la rue
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label for="liv_adr" class="control-label">N°, voie</label>
                        <?= render_form_text_widget($form['adresse']); ?>
                    </div>

                    <div class="col-md-4">
                        <label for="liv_cp_ville" class="control-label">Code postal, Ville</label>
                        <?= render_form_text_widget($form['ville']); ?>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="liv_gps_lat" class="control-label">GPS Latitude<br/></label>
                        <?= render_form_text_widget($form['gpsLatitude']); ?>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="liv_gps_long" class="control-label">GPS Longitude<br/></label>
                        <?= render_form_text_widget($form['gpsLongitude']); ?>
                    </div>

                </div>

            </div>


        </div>

    </div>
</div>

<div class="form-group">

    <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>

    </form>

    <?php

    echo '<a href="/amap/informations_generales_creation" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>';

    echo '<a href="'.site_url('amap').'" class="pull-right btn btn-default">Annuler</a>';

    echo '</div>';

    echo '</form>';

    ?>
</div>
