# language: fr
Fonctionnalité: Connexion mauvais statut

  Plan du Scénario: Me connecter à l'application
    Etant donné que je suis sur "/"
    Quand je sélectionne "amapien" depuis "statut"
    Et remplis le texte suivant:
      | email    | <email>   |
      | password | P@ssw0rd! |
      | statut   | <statut>  |
    Et je presse "Connexion"
    Alors je devrais voir "<message>"

    Exemples:
      | statut  | email                           | message                                                           |
      | paysan  | admin@test.amap-aura.org        | L'adresse email n'existe pas dans la base de donnée des paysans.  |
      | paysan  | amap@test.amap-aura.org         | L'adresse email n'existe pas dans la base de donnée des paysans.  |
      | paysan  | amapienref@test.amap-aura.org   | L'adresse email n'existe pas dans la base de donnée des paysans.  |
      | paysan  | amapien@test.amap-aura.org      | L'adresse email n'existe pas dans la base de donnée des paysans.  |
      | amapien | paysan@test.amap-aura.org       | L'adresse email n'existe pas dans la base de donnée des amapiens. |
      | amapien | regroupement@test.amap-aura.org | L'adresse email n'existe pas dans la base de donnée des amapiens. |
