<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use PsrLib\ORM\Entity\Files\ContratPdf;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_contrat ADD pdf_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_contrat ADD CONSTRAINT FK_72C7BFC6511FC912 FOREIGN KEY (pdf_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_72C7BFC6511FC912 ON ak_contrat (pdf_id)');

        $existingFiles = $this
            ->connection
            ->executeQuery('SELECT c_id, c_contrat_fichier FROM ak_contrat WHERE c_contrat_fichier IS NOT NULL')
            ->fetchAll()
        ;

        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        foreach ($existingFiles as $existingFile) {
            $file = new ContratPdf($existingFile['c_contrat_fichier']);
            $em->persist($file);
            $em->flush();
            $em->clear(); // optimisation

            $this->addSql('UPDATE ak_contrat SET pdf_id=:certId WHERE c_id=:cId', [
                'certId' => $file->getId(),
                'cId' => $existingFile['c_id'],
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
