<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Evenement $ev */
?>

<h3>Informations générales</h3>

<?php

if (isset($ev_id)) {
    $mot = 'Modification';
} else {
    $mot = 'Création';
}

echo '<h5><a href="'.site_url('evenement').'">Événements</a> <i class="glyphicon glyphicon-menu-right"></i> '.$mot.' d\'un événement <i class="glyphicon glyphicon-menu-right"></i> Informations générales</h5>';

if (isset($ev_id)) {
    echo form_open_multipart('evenement/add_2/'.$ev_id, 'id="add2"');
} else {
    echo form_open_multipart('evenement/add_2', 'id="add2"');
}
// VARIABLES CACHÉES À LA CRÉATION
echo '<input type="hidden"  name="etape_infos" value="1" />';
// ------------------------------------------------------------------------
// SESSION AMAP OU RÉFÉRENT
if (set_value('amapiens')) {
    echo '<input type="hidden" name="amapiens" value="'.set_value('amapiens').'" />';
}
if (set_value('reseau')) {
    echo '<input type="hidden" name="reseau" value="'.set_value('reseau').'" />';
}
if (set_value('ferme_id')) {
    echo '<input type="hidden" name="ferme_id" value="'.set_value('ferme_id').'" />';
}
// ------------------------------------------------------------------------
// SESSION ADMIN RÉGION
if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('amap')) {
    foreach (set_value('amap') as $amap) {
        echo '<input type="hidden"  name="amap[]" value="'.$amap.'" />';
    }
}

if (set_value('ferme')) {
    foreach (set_value('ferme') as $ferme) {
        echo '<input type="hidden"  name="ferme[]" value="'.$ferme.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}
// ------------------------------------------------------------------------
?>

<div class="well well-sm">
    <div class="row">
        <div class="form-group">
            <label for="evt_nom" class="col-md-12 control-label">Nom : </label>
            <div class="col-md-12">

                <textarea rows="1" cols="50" class="form-control summernote-singleline" name="evt_nom"/><?php if (set_value('evt_nom')) {
    echo set_value('evt_nom');
} elseif (isset($ev)) {
    echo $ev->getNom();
} ?></textarea>

                <?= form_error('evt_nom'); ?>
            </div>
        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group col-md-3">
                <label for="date_deb" class="control-label">Date de début : </label>
                <input type="text" data-date-format="yyyy-mm-dd" class="form-control datepicker" name="date_deb"
                       value="<?php if (set_value('date_deb')) {
    echo set_value('date_deb');
} elseif (isset($ev)) {
    echo null === $ev->getDateDeb() ? null : $ev->getDateDeb()->format('Y-m-d');
} ?>"/>
                <?= form_error('date_deb'); ?>
            </div>

            <div class="form-group col-md-3">
                <label for="hor_deb" class="control-label">Heure de début (hh:mm) : </label>
                <input type="text" data-format="hh:mm" class="form-control" name="hor_deb"
                       value="<?php if (set_value('hor_deb')) {
    echo set_value('hor_deb');
} elseif (isset($ev)) {
    echo $ev->getHorDeb();
} ?>"/>
                <?= form_error('hor_deb'); ?>
            </div>

            <div class="form-group col-md-3">
                <label for="date_fin" class="control-label">Date de fin : </label>
                <input type="text" data-date-format="yyyy-mm-dd" class="form-control datepicker" name="date_fin"
                       value="<?php if (set_value('date_fin')) {
    echo set_value('date_fin');
} elseif (isset($ev)) {
    echo null === $ev->getDateFin() ? null : $ev->getDateFin()->format('Y-m-d');
} ?>"/>
                <?= form_error('date_fin'); ?>
            </div>

            <div class="form-group col-md-3">
                <label for="hor_fin" class="control-label">Heure de fin (hh:mm) : </label>
                <input type="text" data-format="hh:mm" class="form-control" name="hor_fin"
                       value="<?php if (set_value('hor_fin')) {
    echo set_value('hor_fin');
} elseif (isset($ev)) {
    echo $ev->getHorFin();
} ?>"/>
                <?= form_error('hor_fin'); ?>
            </div>

        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="form-group">
            <label for="evt_txt" class="col-md-12 control-label">Description : </label>
            <div class="col-md-12">
                <textarea rows="4" cols="50" class="form-control summernote" name="evt_txt"/><?php if (set_value('evt_txt')) {
    echo set_value('evt_txt');
} elseif (isset($ev)) {
    echo $ev->getEvTxt();
} ?></textarea>
                <?= form_error('evt_txt'); ?>
            </div>
        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="form-group col-md-12">
            <label for="url" class="col-md-12 control-label">Page web de l’événement : </label>
            <div class="input-group">
                <div class="input-group-addon">www.</div>
                <input class="form-control" type="text" name="url" placeholder="exemple.fr"
                       value="<?php if (set_value('url')) {
    echo set_value('url');
} elseif (isset($ev)) {
    echo $ev->getUrl();
} ?>"/>
                <?= form_error('url'); ?>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <?php
    if ($ev_id) {
        $mot = 'Valider';
    } else {
        $mot = 'Étape suivante';
    }

    echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;"/>';
    ?>
</div>

</form>

<!-- ----------------------------------------------------------------------- -->
<!-- ÉTAPE PRÉCÉDENTE -->
<?php echo form_open_multipart('evenement/add_1', 'id="add1"');
// VARIABLES CACHÉES
// ------------------------------------------------------------------------
// SESSION AMAP OU RÉFÉRENT
if (set_value('amapiens')) {
    echo '<input type="hidden" name="amapiens" value="'.set_value('amapiens').'" />';
}
if (set_value('reseau')) {
    echo '<input type="hidden" name="reseau" value="'.set_value('reseau').'" />';
}
if (set_value('ferme_id')) {
    echo '<input type="hidden" name="ferme_id" value="'.set_value('ferme_id').'" />';
}
// ------------------------------------------------------------------------
// SESSION ADMIN RÉGION
if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('amap')) {
    foreach (set_value('amap') as $amap) {
        echo '<input type="hidden"  name="amap[]" value="'.$amap.'" />';
    }
}

if (set_value('ferme')) {
    foreach (set_value('ferme') as $ferme) {
        echo '<input type="hidden"  name="ferme[]" value="'.$ferme.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}
// ------------------------------------------------------------------------
if (set_value('evt_nom')) {
    echo '<input type="hidden"  name="evt_nom" value="'.set_value('evt_nom').'" />';
}
if (set_value('date_deb')) {
    echo '<input type="hidden"  name="date_deb" value="'.set_value('date_deb').'" />';
}
if (set_value('date_fin')) {
    echo '<input type="hidden"  name="date_fin" value="'.set_value('date_fin').'" />';
}
if (set_value('hor_deb')) {
    echo '<input type="hidden"  name="hor_deb" value="'.set_value('hor_deb').'" />';
}
if (set_value('hor_fin')) {
    echo '<input type="hidden"  name="hor_fin" value="'.set_value('hor_fin').'" />';
}
if (set_value('url')) {
    echo '<input type="hidden"  name="url" value="'.set_value('url').'" />';
}
if (set_value('evt_txt')) {
    echo '<input type="hidden"  name="evt_txt" value="'.set_value('evt_txt').'" />';
}
?>
<div class="form-group">
    <?php
    if (!$ev_id) {
        echo '<input type="submit" value="Étape précédente" class="pull-right btn btn-default" style="margin-left:5px;"/>';
    }
    ?>
    <a href="<?= site_url('evenement'); ?>" class="pull-right btn btn-default">Annuler</a>
</div>

</form>
