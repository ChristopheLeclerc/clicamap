<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Assert\Assertion;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AdhesionAmapienRepository")
 */
class AdhesionAmapien extends Adhesion
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var Amapien
     * @ORM\ManyToOne(targetEntity="Amapien")
     * @ORM\JoinColumn(referencedColumnName="a_id", nullable=false)
     */
    protected $amapien;

    /**
     * @var AdhesionValueAmapien
     * @ORM\OneToOne(targetEntity="AdhesionValueAmapien", cascade="ALL")
     * @Assert\Valid()
     * @ORM\JoinColumn(nullable=false)
     */
    protected $value;

    /**
     * @var Amapien
     * @ORM\ManyToOne(targetEntity="Amapien")
     * @ORM\JoinColumn(referencedColumnName="a_id", nullable=false)
     * @Assert\Expression("value.isAdminOf(this.getAmapien())", groups={"import"}, message="Erreur d'accès à la ferme")
     */
    protected $creator;

    /**
     * AdhesionFerme constructor.
     */
    public function __construct(Amapien $amapien, AdhesionValueAmapien $value, Amapien $creator)
    {
        $this->amapien = $amapien;
        $this->value = $value;
        $this->creator = $creator;
    }

    public function getAmapien(): Amapien
    {
        return $this->amapien;
    }

    /**
     * @param Amapien $amapien
     */
    public function setAmapien($amapien): AdhesionAmapien
    {
        $this->amapien = $amapien;

        return $this;
    }

    public function getValue(): AdhesionValueAmapien
    {
        return $this->value;
    }

    /**
     * @param AdhesionValueAmapien $value
     *
     * @return AdhesionAmapien
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Amapien
     */
    public function getCreator(): BaseUser
    {
        return $this->creator;
    }

    /**
     * @param Amapien $creator
     *
     * @return AdhesionAmapien
     */
    public function setCreator(BaseUser $creator)
    {
        Assertion::isInstanceOf($creator, Amapien::class);
        $this->creator = $creator;

        return $this;
    }
}
