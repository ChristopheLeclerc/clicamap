<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var string $url_retour */
?>

<h3>Validation juridique de mon contrat</h3>

<h5>
    <?php if ('/contrat_signe/contrat_own_new/' === $url_retour): ?>
        <a href="<?= site_url('/contrat_signe/contrat_own_new/'); ?>">Les nouveaux contrats disponibles</a>
    <?php elseif (false !== strpos($url_retour, 'contrat_signe')): ?>
        <a href="<?= $url_retour; ?>">Contrats signés</a>
    <?php else: ?>
        <a href="<?= $url_retour; ?>">Mes contrats existants</a>
    <?php endif; ?>
    <i class="glyphicon glyphicon-menu-right"></i>
    Souscription
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('/contrat_signe/contrat_subscribe_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('/contrat_signe/contrat_subscribe_2'); ?>">Étape 2</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Validation juridique de mon contrat
</h5>


<div class="row">
    <div class="col-xs-12">
        <h4 class="mb-2 mt-1">
            <div class="alert alert-danger">
              Vous vous apprêtez à vous engager dans le cadre d'un partenariat avec la ferme.<br/>Lisez le contrat ci-dessous, acceptez les conditions puis validez, sinon il ne sera pas pris en compte.
            </div>
        </h4>
    </div>
    <div class="col-xs-12">
        <?=pdf_embedded(site_url('/contrat_signe/preview_pdf')); ?>
    </div>

    <div class="col-xs-12">
        <form method="POST">
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input
                                type="checkbox"
                                name="confirm-sign"
                                value="1"
                            <?=set_checkbox('confirm-sign', '1'); ?>
                        > <b>J'accepte les termes de <a href="<?=site_url('/contrat_signe/preview_pdf'); ?>" target="_blank"> mon contrat juridique</a></b>
                    </label>
                </div>
                <?=form_error('confirm-sign'); ?>
                <div class="checkbox">
                    <label>
                        <input
                                type="checkbox"
                                name="confirm-charter"
                                value="1"
                            <?=set_checkbox('confirm-charter', '1'); ?>
                        ><b> Je confirme avoir pris connaissance de la <a href="https://amap-aura.org/la-charte-des-amap/" target="_blank">Charte des AMAP</a></b>
                    </label>
                </div>
                <?=form_error('confirm-charter'); ?>

                <p>Une fois <b>validé</b>, ce contrat juridique reste disponible au format PDF dans "Mes contrats existants" : l'impression du contrat n'est pas nécessaire.</p>

                <button type="submit" class="btn btn-success pull-right" style="margin-left: 10px">Valider</button>
                <a
                        href="<?=site_url('/contrat_signe/contrat_subscribe_2'); ?>"
                        class="btn btn-default pull-right"
                >Étape précédente</a>
            </div>
        </form>
    </div>
</div>

<script>
    var alertOnExitEnabled = true;
    function alertOnExit(event) {
        if(!alertOnExitEnabled) {
            return;
        }

        event.preventDefault();
        event.returnValue = '';
    }
    window.addEventListener('beforeunload', alertOnExit);

    $('button[type="submit"]').click(function (e) {
      alertOnExitEnabled = false;
    });
</script>
