<?php

namespace Test;

use DI\Container;
use PHPUnit\Framework\TestCase;
use PsrLib\Services\PhpDiContrainerSingleton;

/**
 * @internal
 * @coversNothing
 */
class ContainerAwareTestCase extends TestCase
{
    /**
     * @var Container
     */
    public static $container;

    public function setUp()
    {
        self::$container = PhpDiContrainerSingleton::getContainer();
    }

    public function tearDown()
    {
        PhpDiContrainerSingleton::cleanContainer();
        self::$container = null;
    }
}
