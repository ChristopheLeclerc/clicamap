<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Map extends AppController
{
    /**
     * Map constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('leaflet');
    }

    /**
     * @param $amap_id
     */
    public function amap_display($amap_id)
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAP_SHOW_MAP, $amap);

        // CARTOGRAPHIE
        $gps = 0;
        $map = null;

        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $liv_lieu */
        foreach ($amap->getLivraisonLieux() as $liv_lieu) {
            if ($liv_lieu->getGpsLatitude()) {
                ++$gps;
                $config = [
                    'center' => $liv_lieu->getGpsLatitude().','.$liv_lieu->getGpsLongitude(),
                    'zoom' => 12,
                ];

                $this->leaflet->initialize($config);

                // INFOS DU POPUP
                $infos = '<strong>'.$amap->getNom().'</strong>';
                $infos = $infos.'<br/>'.$liv_lieu->getNom();
                if ($liv_lieu->getAdresse()) {
                    $infos = $infos.'<br/>'.$liv_lieu->getAdresse();
                }
                $ville = $liv_lieu->getVille();
                if (null !== $ville) {
                    $infos = $infos.'<br/>'.$ville->getCpString();
                }
                if (null !== $ville) {
                    $infos = $infos.' '.$ville->getNom();
                }
                if ($amap->getEmail()) {
                    $infos = $infos.'<br/>'.$amap->getEmail();
                }
                if ($amap->getUrl()) {
                    $infos = $infos.'<br/>'.$amap->getUrl();
                }

                // Emplacement du marker
                $marker = [
                    'latlng' => $liv_lieu->getGpsLatitude().','.$liv_lieu->getGpsLongitude(), // Marker Location
                    // POPUP
                    'popupContent' => addslashes($infos), // Popup Content
                ];

                $this->leaflet->add_marker($marker);
            }
        }

        if ($gps > 0) {
            $map = $this->leaflet->create_map();
        } else {
            $gps = 'aucun';
        }

        $this->loadViewWithTemplate('map/amap', [
            'gps' => $gps,
            'map' => $map,
            'amap' => $amap,
        ]);
    }

    /**
     * @param mixed $ferme_id
     */
    public function ferme_display($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $data['ferme'] = $ferme;

        $config = [
            'center' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(),
            'zoom' => 16,
        ];

        $this->leaflet->initialize($config);

        // INFOS DU POPUP
        $infos = '<strong>'.$ferme->getNom().'</strong>';
        if ($ferme->getLibAdr()) {
            $infos = $infos.'<br/>'.$ferme->getLibAdr();
        }
        $ville = $ferme->getVille();
        if ($ville->getCpString()) {
            $infos = $infos.'<br/>'.$ville->getCpString();
        }
        if ($ville->getNom()) {
            $infos = $infos.', '.$ville->getNom();
        }

        // Emplacement du marker
        $marker = [
            'latlng' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(), // Marker Location

            // POPUP
            'popupContent' => $infos, // Popup Content
        ];

        $this->leaflet->add_marker($marker);
        $data['map'] = $this->leaflet->create_map();
        $this->loadViewWithTemplate('map/ferme', $data);
    }
}
