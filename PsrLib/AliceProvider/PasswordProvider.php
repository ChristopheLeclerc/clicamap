<?php

namespace PsrLib\AliceProvider;

use PsrLib\Services\Password\PasswordEncoder;

class PasswordProvider
{
    /**
     * @var PasswordEncoder
     */
    private $passwordEncoder;

    /**
     * Cache to avoid same password encoding multiple time.
     *
     * @var string[]
     */
    private $encodedPasswordCache = [];

    public function __construct(PasswordEncoder $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function password_encode(string $password): string
    {
        if (isset($this->encodedPasswordCache[$password])) {
            return $this->encodedPasswordCache[$password];
        }

        $this->encodedPasswordCache[$password] = $this->passwordEncoder->encodePassword($password);

        return $this->encodedPasswordCache[$password];
    }
}
