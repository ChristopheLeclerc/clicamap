# language: fr
Fonctionnalité: Comme utilisateur, je devrais pouvoir visualiser un document

  Contexte:
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/documentation/form"
    Et que je remplis "nom" avec "Nom du nouveau document"
    Et que je remplis "lien" avec "http://www.amap-aura.org"
    Et que je coche "Page d'accueil (utilisateur non connecté)"
    Et que je presse "Sauvegarder"

  @database
  Scénario: Visualisation du document
    Etant donné je suis sur "/documentation/list_documents"
    Alors je devrais voir le lien "Voir le document" vers "http://www.amap-aura.org"
