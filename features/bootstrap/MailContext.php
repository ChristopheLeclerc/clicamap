<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Behat\Behat\Context\Context;

class MailContext implements Context
{
    /**
     * @Then un mail avec le sujet :subject devrait avoir été envoyé à :target
     *
     * @param mixed $subject
     * @param mixed $target
     */
    public function unMailAvecLeTitreDevraitAvoirEteEnvoyeA($subject, $target)
    {
        $response = $this
            ->_clientBuilder()
            ->get('/api/v1/messages')
            ->getBody()
            ->getContents()
        ;
        $messages = \GuzzleHttp\json_decode($response, true);
        $errormessages = [];
        foreach ($messages as $message) {
            $tos = array_map(function ($to) {
                return $to['Mailbox'].'@'.$to['Domain'];
            }, $message['To']);
            $messageSubject = iconv_mime_decode($message['Content']['Headers']['Subject'][0]);

            if (in_array($target, $tos) && $subject === $messageSubject) {
                return;
            }

            $errormessages[] = sprintf(
                '%s[%s]',
                $messageSubject,
                implode(',', $tos)
            );
        }

        throw new \RuntimeException('Mail non trouvé parmi : '.implode('; ', $errormessages));
    }

    /**
     * @AfterScenario
     */
    public function afterScenario()
    {
        $res = $this
            ->_clientBuilder()
            ->delete('/api/v1/messages')
            ->getStatusCode()
        ;
        if (200 !== $res) {
            throw new \RuntimeException('Impossible de nettoyer les mails');
        }
    }

    private function _clientBuilder()
    {
        return new \GuzzleHttp\Client([
            'base_uri' => 'http://mailhog:8025',
        ]);
    }
}
