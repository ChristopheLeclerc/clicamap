# language: fr
Fonctionnalité: Je dois pouvoir accéder au lien vers les mentions légales en étant connecté et ce, quel que soit mon profil.

  Plan du Scénario: J'ai accès aux mentions légales quel que soit mon profil
    Etant donné que je suis authentifié comme "<type>" avec le mail "<mail>"
    Et que je suis sur "/evenement"
    Lorsque je suis "Mentions légales"
    Alors je devrais être sur "/contact/mentions_legales"

    Exemples:
      | type    | mail                          |
      | amapien | amapien@test.amap-aura.org    |
      | amapien | amapienref@test.amap-aura.org |
      | amapien | amap@test.amap-aura.org       |
      | amapien | admin@test.amap-aura.org      |
      | paysan  | paysan@test.amap-aura.org     |
