<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ContratCelluleNormalizer implements DenormalizerAwareInterface, ContextAwareDenormalizerInterface
{
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        /** @var ContratCellule $cellule */
        $cellule = $this->denormalizer->denormalize($data, ContratCellule::class);

        $proId = $data['modeleContratProduit']['id'] ?? null;
        if (null !== $proId) {
            $cellule->setModeleContratProduit($this->em->getReference(ModeleContratProduit::class, $proId));
        }

        $dateId = $data['modeleContratDate']['id'] ?? null;
        if (null !== $dateId) {
            $cellule->setModeleContratDate($this->em->getReference(ModeleContratDate::class, $dateId));
        }

        return $cellule;
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return ContratCellule::class === $type
            && isset($context['groups'])
            && 'wizardContract' === $context['groups']
        ;
    }
}
