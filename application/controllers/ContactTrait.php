<?php

use DI\Annotation\Inject;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
trait ContactTrait
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\ContactCPSelecteur
     */
    private $contactcpselecteur;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Email_Sender
     */
    private $email_sender;

    /**
     * @param string $redirectionUrl
     */
    private function handleContactRequest($redirectionUrl)
    {
        $this->form_validation->set_rules('contact_nom', '"Nom"', 'required|max_length[20]');
        $this->form_validation->set_rules('contact_prenom', '"Prénom"', 'required|max_length[20]');
        $this->form_validation->set_rules('contact_email', '"Email"', 'required|valid_email|max_length[50]');
        $this->form_validation->set_rules('contact_cp', '"Code postal"', 'required|numeric|exact_length[5]');
        $this->form_validation->set_rules('contact_contenu', '"Contenu"', 'required|max_length[1000]');
        $this->form_validation->set_rules('contact_captcha', '"Code de vérification"', 'required|captcha_check');

        if ($this->form_validation->run()) {
            $destMails = $this->contactcpselecteur->choisirContactDepuisCP($this->input->post('contact_cp'));

            // Envoi de l'email
            $this->email_sender->envoyerMailContact(
                $this->input->post('contact_nom'),
                $this->input->post('contact_prenom'),
                $this->input->post('contact_email'),
                $destMails,
                $this->input->post('contact_cp'),
                $this->input->post('contact_contenu')
            );

            // Envoi de l'accusé de reception
            if (null !== $this->input->post('contact_envoiconfirmation')) {
                $this->email_sender->envoyerEnvoiConfirmationMailContact(
                    $this->input->post('contact_nom'),
                    $this->input->post('contact_prenom'),
                    $this->input->post('contact_email'),
                    $this->input->post('contact_cp'),
                    $this->input->post('contact_contenu')
                );
            }

            // Message de confirmation pour l'utilisateur
            $this->addFlash(
                'flash_contact_confirmation',
                'Votre message est bien envoyé. Merci et bonne journée'
            );

            return redirect($redirectionUrl);
        }
    }
}
