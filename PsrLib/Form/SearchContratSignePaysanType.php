<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\UserWithFermes;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchContratSignePaysanType extends AbstractType
{
    /**
     * @var ModeleContratRepository
     */
    private $modeleContratRepo;

    /**
     * @var AmapRepository
     */
    private $amapRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->modeleContratRepo = $em
            ->getRepository(ModeleContrat::class)
        ;
        $this->amapRepo = $em
            ->getRepository(Amap::class)
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $filterArchived = $options['filter_archived'];

        /** @var UserWithFermes $currentUser */
        $currentUser = $options['currentUser'];
        Assertion::isInstanceOf($currentUser, UserWithFermes::class);

        $fermes = $currentUser->getFermes()->toArray();
        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => $this->amapRepo->findByFermes($fermes),
                'required' => false,
                'placeholder' => true,
                'choice_label' => function (?Amap $amap) {
                    return null === $amap ? '' : $amap->getNom();
                },
                'choice_value' => function (?Amap $amap) {
                    return null === $amap ? '' : $amap->getId();
                },
            ])
            ->add('mc', ChoiceType::class, [
                'choices' => [],
                'disabled' => true,
            ])
        ;

        $builder
            ->get('amap')
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($fermes, $filterArchived) {
                $form = $event->getForm();
                $amap = $form->getParent()->get('amap')->getData();

                if (null === $amap) {
                    return;
                }

                $choices = $this->modeleContratRepo->findValidatedByFermesAmap($fermes, $amap);
                $choices = array_filter($choices, function (ModeleContrat $mc) use ($filterArchived) {
                    return $mc->isArchived() === $filterArchived;
                });
                $form
                    ->getParent()
                    ->add('mc', ChoiceType::class, [
                        'choices' => $choices,
                        'choice_label' => function (?ModeleContrat $modeleContrat) {
                            return null === $modeleContrat ? null : $modeleContrat->getNom();
                        },
                        'choice_value' => function (?ModeleContrat $modeleContrat) {
                            return null === $modeleContrat ? null : $modeleContrat->getId();
                        },
                        'placeholder' => true,
                        'required' => false,
                    ])
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'currentUser' => null,
            'method' => 'GET',
            'data_class' => SearchContratSigneState::class,
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch,
            'filter_archived' => true,
        ]);
    }
}
