<?php

namespace PsrLib\Services;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AmapDistributionRepository;

class AmapDistributionNotificationSender
{
    public const AMAPIENS_RAPPELS = [6, 18];
    public const AMAP_RELANCE = [1, 6, 18];
    public const AMAP_ALERTE_FIN = [30, 60];

    /**
     * @var Email_Sender
     */
    private $emailSender;

    /**
     * @var AmapDistributionRepository
     */
    private $amapDistributionRepo;

    public function __construct(Email_Sender $emailSender, EntityManagerInterface $em)
    {
        $this->emailSender = $emailSender;
        $this->amapDistributionRepo = $em->getRepository(AmapDistribution::class);
    }

    public function amapienRappels(): void
    {
        // Group messages to avoid long delay between database requests
        $messages = [];

        foreach (self::AMAPIENS_RAPPELS as $delay) {
            $distributions = $this->amapDistributionRepo->getDistributionInDaysFromNow($delay);
            foreach ($distributions as $distribution) {
                if ($distribution->getPlacesRestantes() > 0) {
                    foreach ($distribution->getAmapiens() as $amapien) {
                        $messages[] = [
                            'distribution' => $distribution,
                            'amapien' => $amapien,
                        ];
                    }
                }
            }
        }

        foreach ($messages as $message) {
            if ($message['amapien']->estActif()) {
                $this->emailSender->envoyerDistributionAmapienRappel($message['distribution'], $message['amapien']);
            }
            usleep(100000);
        }
    }

    public function amapRelance(): void
    {
        // Group messages to avoid long delay between database requests
        $messages = [];

        foreach (self::AMAP_RELANCE as $delay) {
            $distributions = $this->amapDistributionRepo->getDistributionInDaysFromNow($delay);
            /** @var AmapDistribution[][] $amapDistributionsByAmap */
            $amapDistributionsByAmap = [];
            $amaps = [];

            foreach ($distributions as $distribution) {
                if ($distribution->getPlacesRestantes() > 0) {
                    $distributionAmap = $distribution->getAmapLivraisonLieu()->getAmap();
                    $distributionAmapId = $distributionAmap->getId();
                    if (!isset($amapDistributionsByAmap[$distributionAmapId])) {
                        $amapDistributionsByAmap[$distributionAmapId] = [];
                        $amaps[$distributionAmapId] = $distributionAmap;
                    }
                    $amapDistributionsByAmap[$distributionAmapId][] = $distribution;
                }
            }

            foreach ($amapDistributionsByAmap as $amapId => $amapDistributions) {
                usort($amapDistributionsByAmap[$amapId], function (AmapDistribution $a, AmapDistribution $b) {
                    return $a->getDateDebut()->timestamp - $b->getDateDebut()->timestamp;
                });
            }

            foreach ($amapDistributionsByAmap as $amapId => $amapDistributions) {
                $dst = $amaps[$amapId]
                    ->getAmapiens()
                    ->toArray()
                ;
                $dst = array_filter($dst, function (Amapien $amapien) use ($amapDistributions) {
                    if (!$amapien->estActif()) {
                        return false;
                    }
                    foreach ($amapDistributions as $amapDistribution) {
                        if ($amapDistribution->getAmapiens()->contains($amapien)) {
                            return false;
                        }
                    }

                    return true;
                });
                $messages[] = [
                    'distributions' => $amapDistributions,
                    'amapiens' => array_values($dst),
                    'amapCopie' => 1 === $delay,
                ];
            }
        }

        foreach ($messages as $message) {
            $this->emailSender->envoyerDistributionAmapiensRelance(
                $message['distributions'],
                $message['amapiens'],
                $message['amapCopie']
            );
            usleep(100000);
        }
    }

    public function amapAlerteFin(): void
    {
        // Group messages to avoid long delay between database requests
        $messages = [];

        $distributions = $this->amapDistributionRepo->getLastDistributionByAmap();
        $now = Carbon::now()->startOfDay();
        foreach ($distributions as $distribution) {
            foreach (self::AMAP_ALERTE_FIN as $alerte) {
                $limit = $now->clone()->addDays($alerte);
                if ($distribution->getDate()->eq($limit)) {
                    $messages[] = $distribution;
                }
            }
        }

        foreach ($messages as $distribution) {
            $this->emailSender->envoyerDistributionAmapAlerteFin($distribution);
            usleep(100000);
        }
    }
}
