<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratDatesReglement;
use PsrLib\ORM\Entity\ModeleContrat;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ContratNormalizer implements DenormalizerAwareInterface, ContextAwareDenormalizerInterface
{
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        /** @var Contrat $contrat */
        $contrat = $this->denormalizer->denormalize($data, Contrat::class);

        $mcId = $data['modeleContrat']['id'] ?? null;
        if (null !== $mcId) {
            $contrat->setModeleContrat($this->em->getReference(ModeleContrat::class, $mcId));
        }

        $amapienId = $data['amapien']['id'] ?? null;
        if (null !== $amapienId) {
            $contrat->setAmapien($this->em->getReference(Amapien::class, $amapienId));
        }

        $contrat->getCellules()->clear();
        foreach ($data['cellules'] as $cellule) {
            /** @var ContratCellule $celluleDenormalized */
            $celluleDenormalized = $this->denormalizer->denormalize($cellule, ContratCellule::class, null, [
                'groups' => 'wizardContract',
            ]);
            $contrat->addCellule($celluleDenormalized);
        }

        $contrat->getDatesReglements()->clear();
        foreach ($data['datesReglements'] as $date) {
            /** @var ContratDatesReglement $dateDenormalized */
            $dateDenormalized = $this->denormalizer->denormalize($date, ContratDatesReglement::class, null, [
                'groups' => 'wizardContract',
            ]);
            $contrat->addDatesReglement($dateDenormalized);
        }

        return $contrat;
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return Contrat::class === $type
            && isset($context['groups'])
            && 'wizardContract' === $context['groups']
        ;
    }
}
