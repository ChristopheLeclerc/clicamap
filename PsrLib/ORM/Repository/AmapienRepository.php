<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use PsrLib\DTO\SearchAmapienAmapState;
use PsrLib\DTO\SearchAmapienState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Entity\Ville;

class AmapienRepository extends BaseUserRepository
{
    public static function criteriaGestionaire(): Criteria
    {
        return Criteria::create()
            ->where(Criteria::expr()->eq('gestionnaire', true))
        ;
    }

    /**
     * @return Amapien[]
     */
    public function search(SearchAmapienAmapState $amapienSearchState): array
    {
        if ($amapienSearchState->isEmpty()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')

            // optimisation
            ->leftJoin('a.amap', 'amap')
            ->addSelect('amap')
            ->leftJoin('amap.livraisonLieux', 'll')
            ->addSelect('ll')
            ->leftJoin('ll.ville', 'ville')
            ->addSelect('ville')
            ->leftJoin('ville.departement', 'departement')
            ->addSelect('departement')
            ->leftJoin('a.fermeAttentes', 'ferme_attentes')
            ->addSelect('ferme_attentes')
            ->leftJoin('a.amapAsRefReseau', 'amap_as_ref_reseau')
            ->addSelect('amap_as_ref_reseau')
            ->leftJoin('a.amapAsRefReseauSec', 'amap_as_ref_reseau_sec')
            ->addSelect('amap_as_ref_reseau_sec')
        ;

        if (null !== $amapienSearchState->getAmap()) {
            $qb
                ->andWhere('a.amap = :amap')
                ->setParameter('amap', $amapienSearchState->getAmap())
            ;
        }

        if (null !== $amapienSearchState->getAdhesion()) {
            $qb
                ->leftJoin('a.anneeAdhesions', 'aa')
                ->andWhere('aa.annee = :annee')
                ->setParameter('annee', $amapienSearchState->getAdhesion())
            ;
        }

        if (null !== $amapienSearchState->getKeyWord()) {
            $qb
                ->andWhere('a.nom LIKE :keyword')
                ->setParameter('keyword', '%'.$amapienSearchState->getKeyWord().'%')
            ;
        }

        if ($amapienSearchState->getNewsletter()) {
            $qb
                ->andWhere('a.newsletter = TRUE')
            ;
        }

        if ($amapienSearchState->getActive()) {
            $qb
                ->andWhere('a.etat = :etat')
                ->setParameter('etat', Amapien::ETAT_ACTIF)
            ;
        }

        if ($amapienSearchState  instanceof SearchAmapienState) {
            if (null !== $amapienSearchState->getRegion()) {
                $qb
                    ->andWhere('departement.region = :region')
                    ->setParameter('region', $amapienSearchState->getRegion())
                ;
            }

            if (null !== $amapienSearchState->getDepartement()) {
                $qb
                    ->andWhere('departement = :departement')
                    ->setParameter('departement', $amapienSearchState->getDepartement())
                ;
            }

            if ($amapienSearchState->getNoAmap()) {
                $qb
                    ->andWhere('a.amap IS NULL')
                ;
            }
        }

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }

    public function qbByAmap(Amap $amap): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.amap = :amap')
            ->setParameter('amap', $amap)
            ->orderBy('a.nom', 'ASC')
        ;
    }

    public function qbRefProduitFromAmap(Amap $amap): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')
            ->where('a.refProdFermes IS NOT EMPTY')
            ->andWhere('a.amap = :amap')
            ->setParameter('amap', $amap)
        ;
    }

    public function qbNotRefProduitFromAmapFerme(Amap $amap, Ferme $ferme): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')
            ->where(':ferme NOT MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $ferme)
            ->andWhere('a.amap = :amap')
            ->setParameter('amap', $amap)
            ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromAmap(Amap $amap): array
    {
        return $this
            ->qbRefProduitFromAmap($amap)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromFerme(Ferme $ferme): array
    {
        return $this
            ->createQueryBuilder('a')
            ->where(':ferme MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $ferme)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromAmapFerme(Amap $amap, Ferme $ferme): array
    {
        return $this
            ->qbRefProduitFromAmap($amap)
            ->andWhere(':ferme MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $ferme)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Amap[] $amaps
     *
     * @return Amapien[]
     */
    public function getRefProduitFromMultipleAmapsFerme($amaps, Ferme $ferme): array
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')
            ->andWhere(':ferme MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $ferme)
            ->andWhere('a.amap IN (:amaps)')
            ->setParameter('amaps', $amaps)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromAmapTp(Amap $amap, TypeProduction $typeProduction): array
    {
        return $this
            ->qbRefProduitFromAmap($amap)
            ->leftJoin('a.refProdFermes', 'ferme')
            ->leftJoin('ferme.produits', 'p')
            ->where('a.amap = :amap')
            ->andWhere('p.typeProduction = :typeproduction')
            ->setParameter('typeproduction', $typeProduction)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return string[]
     */
    public function getAllEmails()
    {
        $emails = $this
            ->createQueryBuilder('a')
            ->select('a.email')
            ->where('a.email IS NOT NULL')
            ->andWhere('a.email != \'\'')
            ->getQuery()
            ->getArrayResult()
        ;

        return array_column($emails, 'email');
    }

    /**
     * @return Amapien[]
     */
    public function getAllForceableFromModeleContract(ModeleContrat $modeleContrat): array
    {
        $amapienWithContractFromMc = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.contrats', 'contrats')
            ->where('contrats.modeleContrat = :modeleContrat')
            ->setParameter('modeleContrat', $modeleContrat)
            ->getQuery()
            ->getResult()
        ;

        $amap = $modeleContrat->getLivraisonLieu()->getAmap();

        $qb = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.contrats', 'contrats')
            ->where('a.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('a.etat = :etat')
            ->setParameter('etat', Amapien::ETAT_ACTIF)
            ->andWhere(':mcFerme NOT MEMBER OF a.fermeAttentes')
            ->setParameter('mcFerme', $modeleContrat->getFerme())
            ->orderBy('a.nom', 'ASC')
        ;

        if (count($amapienWithContractFromMc) > 0) {
            $qb
                ->andWhere('a NOT IN (:amapienWithContract)')
                ->setParameter(':amapienWithContract', $amapienWithContractFromMc)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Amapien[]
     */
    public function searchByName(string $name, Amap $amap = null)
    {
        $qb = $this
            ->createQueryBuilder('a')
            ->where('a.nom LIKE :name')
            ->setParameter('name', '%'.$name.'%')
        ;

        if (null !== $amap) {
            $qb
                ->andWhere('a.amap = :amap')
                ->setParameter('amap', $amap)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Amapien[]
     */
    public function getAdminDepFromCP(string $cp)
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.adminDepartments', 'd')
            ->leftJoin(Ville::class, 'v', Join::WITH, 'v.departement = d')
            ->where('v.cp = :cp')
            ->setParameter('cp', $cp)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getAdminRegFromCP(string $cp)
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.adminRegions', 'r')
            ->leftJoin(Departement::class, 'd', Join::WITH, 'd.region = r')
            ->leftJoin(Ville::class, 'v', Join::WITH, 'v.departement = d')
            ->where('v.cp = :cp')
            ->setParameter('cp', $cp)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getAdminResFromCP(string $cp)
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.adminReseaux', 'r')
            ->leftJoin('r.villes', 'v')
            ->where('v.cp = :cp')
            ->setParameter('cp', $cp)
            ->getQuery()
            ->getResult()
        ;
    }

    public function qbDistributionAmapienAvailiable(AmapDistribution $amapDistribution): QueryBuilder
    {
        $qb = $this
            ->qbByAmap($amapDistribution->getAmapLivraisonLieu()->getAmap())
            ->andWhere('a.etat = :actif')
            ->setParameter('actif', Amapien::ETAT_ACTIF)
        ;

        $amapiens = $amapDistribution->getAmapiens()->toArray();
        if (count($amapiens) > 0) {
            $qb
                ->andWhere('a.id NOT IN (:amapiens)')
                ->setParameter('amapiens', $amapDistribution->getAmapiens()->toArray())
            ;
        }

        return $qb;
    }
}
