# language: fr
Fonctionnalité: Comme utilisateur, je dois pouvoir récupérer mon mot de passe oublié

  Scénario: Accéder à la page depuis l'accueil
    Etant donné que je suis sur "/"
    Quand je suis "Mot de passe oublié ?"
    Alors je devrais voir "Mot de passe oublié"
    Et je devrais être sur "/portail/mdp_oublie"

  Scénario: Aucun statut sélectionné
    Etant donné que je suis sur "/portail/mdp_oublie"
    Quand je presse "Envoyer"
    Alors je devrais voir "Aucun statut fourni"

  Plan du Scénario: Mail inconnu
    Etant donné que je suis sur "/portail/mdp_oublie"
    Quand je sélectionne "<statut>" depuis "lostpassword_statut"
    Et je remplis "lostpassword_email" avec "inconnu@domain.tld"
    Et je presse "Envoyer"
    Alors je devrais voir "<erreur>"
    Exemples:
      | statut       | erreur                                                                |
      | amapien      | L'adresse email n'existe pas dans la base de donnée des amapiens      |
      | paysan       | L'adresse email n'existe pas dans la base de donnée des paysans       |
      | regroupement | L'adresse email n'existe pas dans la base de donnée des regroupements |

  Plan du Scénario: Mail invalide
    Etant donné que je suis sur "/portail/mdp_oublie"
    Quand je sélectionne "<statut>" depuis "lostpassword_statut"
    Et je remplis "lostpassword_email" avec "invalid@"
    Et je presse "Envoyer"
    Alors je devrais voir "<erreur>"
    Exemples:
      | statut       | erreur                   |
      | amapien      | L'Email n'est pas valide |
      | paysan       | L'Email n'est pas valide |
      | regroupement | L'Email n'est pas valide |

  @database
  Plan du Scénario: Mail valide
    Etant donné que je suis sur "/portail/mdp_oublie"
    Quand je sélectionne "<statut>" depuis "lostpassword_statut"
    Et je remplis "lostpassword_email" avec "<mail>"
    Et je presse "Envoyer"
    Alors je devrais voir "Un email contenant un lien vous permettant de générer un nouveau mot de passe vient de vous être envoyé, veuillez consulter votre boite mail"
    Et un mail avec le sujet "Réinitialisation de votre mot de passe Clic'AMAP" devrait avoir été envoyé à "<mail>"
    Exemples:
      | statut       | mail                            |
      | amapien      | admin@test.amap-aura.org        |
      | paysan       | paysan@test.amap-aura.org       |
      | regroupement | regroupement@test.amap-aura.org |
