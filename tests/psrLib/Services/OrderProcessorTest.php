<?php

namespace Test\Services;

use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\Services\OrderProcessor;

/**
 * @internal
 * @coversNothing
 */
class OrderProcessorTest extends TestCase
{
    /**
     * @var OrderProcessor
     */
    private $orderProcessor;

    public function setUp()
    {
        $this->orderProcessor = new OrderProcessor();
    }

    public function testCountNbLivraisonsEmpty()
    {
        self::assertSame(0, $this->orderProcessor->countNbLivraisons([]));
    }

    public function testCountNbLivraisonsSameDate()
    {
        $date1 = $this->prophetizeDate(1);
        $cell1 = new ContratCellule();
        $cell1->setQuantite(1);
        $cell1->setModeleContratDate($date1);

        $cell2 = new ContratCellule();
        $cell2->setQuantite(1);
        $cell2->setModeleContratDate($date1);

        self::assertSame(1, $this->orderProcessor->countNbLivraisons([$cell1, $cell2]));
    }

    public function testCountNbLivraisonsDiffrentDate()
    {
        $cell1 = new ContratCellule();
        $cell1->setQuantite(1);
        $cell1->setModeleContratDate($this->prophetizeDate(1));

        $cell2 = new ContratCellule();
        $cell2->setQuantite(1);
        $cell2->setModeleContratDate($this->prophetizeDate(2));

        self::assertSame(2, $this->orderProcessor->countNbLivraisons([$cell1, $cell2]));
    }

    public function testLivSameEmpty()
    {
        self::assertTrue($this->orderProcessor->areLivSame([]));
    }

    public function testLivSameTrueSameDate()
    {
        $cell1 = new ContratCellule();
        $cell1->setQuantite(1);
        $cell1->setModeleContratDate($this->prophetizeDate(1));

        $cell2 = new ContratCellule();
        $cell2->setQuantite(1);
        $cell2->setModeleContratDate($this->prophetizeDate(1));

        self::assertTrue($this->orderProcessor->areLivSame([$cell1, $cell2]));
    }

    public function testLivSameTrueDiffrentDate()
    {
        $cell1 = new ContratCellule();
        $cell1->setQuantite(1);
        $cell1->setModeleContratDate($this->prophetizeDate(1));

        $cell2 = new ContratCellule();
        $cell2->setQuantite(1);
        $cell2->setModeleContratDate($this->prophetizeDate(2));

        self::assertTrue($this->orderProcessor->areLivSame([$cell1, $cell2]));
    }

    public function testLivSameTrueWithEmptyDate()
    {
        $cell1 = new ContratCellule();
        $cell1->setQuantite(1);
        $cell1->setModeleContratDate($this->prophetizeDate(1));

        $cell2 = new ContratCellule();
        $cell2->setQuantite(2);
        $cell2->setModeleContratDate($this->prophetizeDate(1));

        $cell3 = new ContratCellule();
        $cell3->setQuantite(0);
        $cell3->setModeleContratDate($this->prophetizeDate(2));

        $cell4 = new ContratCellule();
        $cell4->setQuantite(0);
        $cell4->setModeleContratDate($this->prophetizeDate(2));

        $cell5 = new ContratCellule();
        $cell5->setQuantite(1);
        $cell5->setModeleContratDate($this->prophetizeDate(3));

        $cell6 = new ContratCellule();
        $cell6->setQuantite(2);
        $cell6->setModeleContratDate($this->prophetizeDate(3));

        self::assertTrue($this->orderProcessor->areLivSame([$cell1, $cell2, $cell3, $cell4, $cell5, $cell6]));
    }

    public function testLivSameFalse()
    {
        $cell1 = new ContratCellule();
        $cell1->setQuantite(1);
        $cell1->setModeleContratDate($this->prophetizeDate(1));

        $cell2 = new ContratCellule();
        $cell2->setQuantite(1);
        $cell2->setModeleContratDate($this->prophetizeDate(1));

        $cell3 = new ContratCellule();
        $cell3->setQuantite(1);
        $cell3->setModeleContratDate($this->prophetizeDate(2));

        $cell4 = new ContratCellule();
        $cell4->setQuantite(0);
        $cell4->setModeleContratDate($this->prophetizeDate(2));

        self::assertFalse($this->orderProcessor->areLivSame([$cell1, $cell2, $cell3, $cell4]));
    }

    private function prophetizeDate(int $id)
    {
        $date = $this->prophesize(ModeleContratDate::class);
        $date->getId()->willReturn($id);

        return $date->reveal();
    }
}
