<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \Symfony\Component\Form\FormView $form */
?>

<h3>Informations générales</h3>

<h5><a href="<?= site_url('amap'); ?>">Gestion des AMAP</a> <i class="glyphicon glyphicon-menu-right"></i>
    Création d'une AMAP <i class="glyphicon glyphicon-menu-right"></i>
    Étape 1</h5>

<form method="POST" class="js-form-amap" action="informations_generales_creation">

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-12">
                    <label for="nom" class="control-label">Nom : </label>
                    <?= render_form_text_widget($form['nom']); ?>
                </div>
                <div class="form-group col-md-12">
                    <label for="associationDeFait" class="control-label">Association de fait : </label>
                    <div class="row">
                        <?= render_form_radio_widget($form['associationDeFait'], 'col-md-4'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6 js-hide-association">
                    <label for="siret" class="control-label">Numéro SIRET :</label>
                    <?= render_form_text_widget($form['siret']); ?>
                </div>
                <div class="form-group col-md-6 js-hide-association">
                    <label for="rna" class="control-label">Numéro RNA :</label>
                    <?= render_form_text_widget($form['rna']); ?>
                </div>
                <div class="form-group col-md-12">
                    <label for="possedeAssurance" class="control-label">Assurance réseau</label>
                    <div class="row">
                        <?= render_form_radio_widget($form['possedeAssurance'], 'col-md-4'); ?>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label for="amapEtudiante" class="control-label">AMAP étudiante</label>
                    <div class="row">
                        <?= render_form_radio_widget($form['amapEtudiante'], 'col-md-4'); ?>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label for="anneeCreation" class="control-label">Année de création de l'AMAP : </label>
                    <?= render_form_text_widget($form['anneeCreation']); ?>
                </div>

                <div class="form-group col-md-6">
                    <label for="nbAdherents" class="control-label">Nombre d'adhérents : </label>
                    <?= render_form_text_widget($form['nbAdherents']); ?>
                </div>

                <div class="form-group col-md-12">
                    <label for="anneeAdhesions" class="control-label">Année(s) d'adhésion (Liste à choix multiple ;
                        maintenez Shift ou Ctrl enfoncés pour sélectionner / désélectionner plusieurs années)
                        :</label>
                    <?= render_form_multiselect_widget($form['anneeAdhesions']); ?>
                </div>

                <div class="form-group col-md-6">
                    <label for="email" class="control-label">Email de contact public : </label>
                    <?= render_form_text_widget($form['email']); ?>
                </div>
                <div class="form-group col-md-6">
                    <label for="url" class="control-label">Adresse site Web ou blog : </label>
                    <div class="input-group">
                        <div class="input-group-addon">www.</div>
                        <?= render_form_text_widget($form['url']); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-md-12">
                    <label for="admin_lib_adr" class="control-label">Nom du lieu de l'adresse administrative : </label>
                    <?= render_form_text_widget($form['adresseAdminNom']); ?>
                </div>

                <div class="form-group col-md-6">
                    <label for="admin_lib_adr" class="control-label">Adresse administrative : </label>
                    <?= render_form_text_widget($form['adresseAdmin']); ?>
                </div>

                <div class="form-group col-md-6">
                    <label for="admin_cp_ville" class="control-label">Code Postal, Ville administrative (à remplir à
                        l'aide des suggestions) : </label>
                    <?= render_form_text_widget($form['adresseAdminVille']); ?>

                    <div id="container_admin" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>

                </div>

            </div>
        </div>
    </div>


    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <label for="etat" class="col-md-12 control-label">Etat</label>

                <?= render_form_radio_widget($form['etat'], 'col-md-4'); ?>

            </div>
        </div>
    </div>

    <div class="form-group">

        <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/><a
                href="<?= site_url('amap'); ?>" class="pull-right btn btn-default">Annuler</a>

    </div>

</form>
