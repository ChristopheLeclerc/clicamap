#language: fr
Fonctionnalité: Modifier la liste des produits d'une AMAP

#  @database
  Scénario: Modification de produits, profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/amap/recherche_paysan"
    Lorsque je sélectionne "1" depuis "amap_produit_recherche[tpPropose][]"
    Et que je sélectionne une autre option "2" depuis "amap_produit_recherche[tpPropose][]"
    Et que je sélectionne "9" depuis "amap_produit_recherche[tpRecherche][]"
    Et que je sélectionne une autre option "13" depuis "amap_produit_recherche[tpRecherche][]"
    Et que je coche "amap_produit_recherche[activation]"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAP a été modifiée avec succès !"
    Quand je suis sur "/amap/display/1"
    Alors je devrais voir "Légumes | Fruits"
    Et je devrais voir "Pain | Porc"

  @database
  Scénario: Modification de produits, profil réseau
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin.aura@test.amap-aura.org"
    Quand je suis sur "/amap/produits/1"
    Lorsque je sélectionne "1" depuis "amap_produit[tpPropose][]"
    Et que je sélectionne une autre option "2" depuis "amap_produit[tpPropose][]"
    Et que je sélectionne "9" depuis "amap_produit[tpRecherche][]"
    Et que je sélectionne une autre option "13" depuis "amap_produit[tpRecherche][]"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAP a été modifiée avec succès !"
    Quand je suis sur "/amap/display/1"
    Alors je devrais voir "Légumes | Fruits"
    Et je devrais voir "Pain | Porc"

  @database
  Scénario: Modification de produits, profil super-admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand je suis sur "/amap/produits/1"
    Lorsque je sélectionne "1" depuis "amap_produit[tpPropose][]"
    Et que je sélectionne une autre option "2" depuis "amap_produit[tpPropose][]"
    Et que je sélectionne "9" depuis "amap_produit[tpRecherche][]"
    Et que je sélectionne une autre option "13" depuis "amap_produit[tpRecherche][]"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAP a été modifiée avec succès !"
    Quand je suis sur "/amap/display/1"
    Alors je devrais voir "Légumes | Fruits"
    Et je devrais voir "Pain | Porc"
