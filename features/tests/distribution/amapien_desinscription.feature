#language: fr
Fonctionnalité: Désinscription à une distribution en tant qu'amapien

  @database
  Scénario: Désinscription à une distribution en tant qu'amapien
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Et que je suis "Mes distrib'AMAP"
    Et que je remplis le datepicker "search_distribution_amapien[period]" avec "01/01/2030 au 01/12/2030"
    Et que je devrais voir "test"
    Quand je presse "S'inscrire"
    Quand je presse "Se désinscrire"
    Alors je devrais voir "1/1"
