<?php

namespace PsrLib\Form;

use PsrLib\Form\Type\CarbonDateType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Repository\AmapLivraisonLieuRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapDistributionEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?Amap $amap */
        $amap = $options['amap'];
        if (null === $amap) {
            throw new \LogicException('Amap cannot be null');
        }

        $builder
            ->add('amapLivraisonLieu', EntityType::class, [
                'label' => 'Lieu de livraison*',
                'class' => AmapLivraisonLieu::class,
                'query_builder' => function (AmapLivraisonLieuRepository $amapLivraisonLieuRepository) use ($amap) {
                    return $amapLivraisonLieuRepository->qbByAmap($amap);
                },
                'required' => false,
            ])
            ->add('detail', AmapDistributionDetailType::class, [
                'label' => false,
            ])
            ->add('date', CarbonDateType::class, [
                'label' => 'Date',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'data_class' => AmapDistribution::class,
        ]);
    }
}
