# language: fr
Fonctionnalité: En tant que paysan je souhaite modifier ma date d'installation

  @database
  Plan du Scénario: Vérification si la date d'installation est valide
    Etant donné que je suis authentifié comme "paysan" avec le mail "paysan@test.amap-aura.org"
    Et que je suis sur "/paysan/display/1"
    Et que je suis "Éditer le profil"
    Lorsque je remplis "paysan[dateInstallation]" avec "<date_installation>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | date_installation            | message                                                             |
      | Date d'installation modifiée | Une date doit être au format suivant : Y-m-d (exemple : 2018-06-28) |
      | 2018-06-28                   | Le profil a été édité avec succès !                                 |
