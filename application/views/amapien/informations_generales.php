<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\Amapien $amapien */
$currentUser = get_current_connected_user();
?>

<script type="text/javascript">

  /* AUTOCOMPLÉTION CP / VILLE */
  $(document).ready(function () {

    $(function () {
      $("#code_postal_ville").autocomplete({
        source: <?= '"'.site_url('ajax/get_cp_ville_suggestion"'); ?>,
        dataType: "json",
        minLength: 4,
        appendTo: "#container"
      });
    });

  });

</script>

<h3>Informations générales</h3>
<?php

if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && null !== $amapien) {
    $url_retour = site_url('amapien/display/'.$amapien->getId());
    echo '<h5><a href="'.$url_retour.'">Mon profil</a>';
} else {
    $url_retour = site_url('amapien');
    echo '<h5><a href="'.$url_retour.'">Gestion des amapiens</a>';
}
echo '<i class="glyphicon glyphicon-menu-right"></i>';

if (null !== $amapien) {
    // ÉDITION
    echo mb_strtoupper($amapien->getNom()).' '.ucfirst($amapien->getPrenom()).' <i class="glyphicon glyphicon-menu-right"></i> Édition';
} else {
    // CRÉATION
    echo 'Création';
}

echo '</h5>';

?>

<form method="POST" action="">

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-md-6">
                    <label for="nom" class="control-label">Nom : </label>
                    <?= render_form_text_widget($form['nom']); ?>
                </div>

                <div class="form-group col-md-6">
                    <label for="prenom" class="control-label">Prénom : </label>
                    <?= render_form_text_widget($form['prenom']); ?>
                </div>

            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-md-4">
                    <label for="email" class="control-label">Email : </label>
                    <?= render_form_text_widget($form['email']); ?>
                </div>

                <div class="form-group col-md-4">
                    <label for="num_tel_1" class="control-label">Téléphone n°1 : </label>
                    <?= render_form_text_widget($form['numTel1']); ?>
                </div>

                <?php if ($form->offsetExists('numTel2')): ?>
                    <div class="form-group col-md-4">
                        <label for="num_tel_2" class="control-label">Téléphone n°2 : </label>
                        <?= render_form_text_widget($form['numTel2']); ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <?php if ($form->offsetExists('emailInvalid')): ?>
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-12">

                    <label for="email_invalid" class="col-lg-12 control-label">Email "fictif" invalide</label>

                    <?= render_form_radio_widget($form['emailInvalid'], 'col-md-6'); ?>

                    <div class="col-md-12">
                        <p class="text-muted mb-0">Si vous indiquez un email qui n’existe pas, merci de sélectionner “oui”. L'amapien ne pourra pas se connecter ni recevoir des emails, mais vous pourrez réaliser des contrats à sa place. Si la coche est à "oui" sans intervention de votre part, merci de vérifier que l'adresse email est correcte.</p>
                    </div>

                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <label for="newsletter" class="col-lg-12 control-label">J'autorise Clic'AMAP à mettre à jour mes coordonnées sur les listes de diffusion email du réseau auquel j'appartiens ?</label>

                <?= render_form_radio_widget($form['newsletter'], 'col-md-6'); ?>

            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group <?= $form->offsetExists('libAdr1Complement') ? 'col-md-12' : 'col-md-6'; ?>">
                    <label for="lib_adr_1" class="control-label">Adresse : </label>
                    <?= render_form_text_widget($form['libAdr1']); ?>
                </div>

                <?php if ($form->offsetExists('libAdr1Complement')): ?>
                    <div class="form-group col-md-6">
                        <label for="lib_adr_1" class="control-label">Complément d'adresse : </label>
                        <?= render_form_text_widget($form['libAdr1Complement']); ?>
                    </div>
                <?php endif; ?>

                <div class="form-group col-md-6">
                    <label for="code_postal_ville" class="control-label">Code postal, Ville (à remplir à l'aide des
                        suggestions) : </label>
                    <?= render_form_text_widget($form['ville']); ?>

                    <div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>

                </div>

            </div>
        </div>
    </div>

    <?php if ($form->offsetExists('adhesionInfo')): ?>
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group col-md-6">
                        <label class="control-label">Nom du réseau</label>
                        <?= render_form_text_widget($form['adhesionInfo']['nomReseau']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Ville mentionnée dans la signature (Fait à )</label>
                        <?= render_form_text_widget($form['adhesionInfo']['villeSignature']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">SIRET</label>
                        <?= render_form_text_widget($form['adhesionInfo']['siret']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">RNA</label>
                        <?= render_form_text_widget($form['adhesionInfo']['rna']); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Site internet</label>
                        <?= render_form_text_widget($form['adhesionInfo']['site']); ?>
                    </div>
                </div>

            </div>
        </div>
    <?php endif; ?>

    <?php if ($form->offsetExists('anneeAdhesions')): ?>
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group">
                        <label for="annee_adhesion" class="control-label">Années d'adhésion (Liste à choix multiples
                            (maintenez Shift ou Ctrl enfoncés pour sélectionner / désélectionner plusieurs années))
                            :</label>
                        <?= render_form_multiselect_widget($form['anneeAdhesions']); ?>
                    </div>

                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($form->offsetExists('needWaiting')): ?>

        <div class="well well-sm">
            <div class="row">
                <div class="col-md-12">

                    <label for="liste_attente" class="col-lg-12 control-label">L'amapien doit-il être mis sur la liste
                        d'attente de certaines fermes ?</label>

                    <?= render_form_radio_widget($form['needWaiting'], 'col-md-6'); ?>

                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group text-right">

        <a href="<?= $url_retour; ?>" class="btn btn-default">Annuler</a>
	
        <input type="submit" value="Sauvegarder" class="btn btn-success" style="margin-left:5px;"/>

    </div>

</form>
