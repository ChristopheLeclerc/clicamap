<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if ($c_id) {
    echo '<h3>Modifier les quantités</h3>';

    if (!$this->session->amapien) {
        echo '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.ucfirst($modele_contrat[0]->mc_nom).' <i class="glyphicon glyphicon-menu-right"></i> '.ucfirst($amapien_prenom).' '.strtoupper($amapien_nom).' <i class="glyphicon glyphicon-menu-right"></i> Modifier</h5>';
    }
} elseif ($test) {
    echo '<h3>Tester le contrat</h3>';

    echo '<h5><a href="'.site_url('contrat_vierge').'">Gestion des contrats vierges</a> <i class="glyphicon glyphicon-menu-right"></i> '.$modele_contrat[0]->mc_nom.' <i class="glyphicon glyphicon-menu-right"></i> Test du contrat</h5>';
} else {
    echo '<h3>Sélection des quantités</h3>';

    if (!$this->session->amapien) {
        echo '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.$modele_contrat[0]->mc_nom.' <i class="glyphicon glyphicon-menu-right"></i> Ajout du contrat <i class="glyphicon glyphicon-menu-right"></i> Étape 2</h5>';
    }
}

// CADRE INFO MC ---------------------------------------------------------------
echo '<div class="alert alert-info col-md-12">';
echo '<div class="col-md-12"><strong>Contrat</strong> : '.$modele_contrat[0]->mc_nom.'</div>';
echo '<div class="col-md-12"><strong>Description</strong> : ';
if ($modele_contrat[0]->mc_description) {
    echo $modele_contrat[0]->mc_description;
}
echo '</div>';
echo '<div class="col-md-4"><strong>Paniers obligatoires : </strong>';
if ($modele_contrat[0]->mc_nbliv_plancher) {
    echo ucfirst($modele_contrat[0]->mc_nbliv_plancher);
} else {
    echo 'NON';
}
echo '</div>';
echo '<div class="col-md-4"><strong>Choix identiques : </strong>';
if ($modele_contrat[0]->mc_choix_identiques) {
    echo 'OUI';
} else {
    echo 'NON';
}
echo '</div>';
echo '<div class="col-md-4"><strong>Date de fin de souscription : </strong>';
if ($modele_contrat[0]->mc_forclusion) {
    echo ucfirst($modele_contrat[0]->mc_forclusion);
} else {
    echo 'NON';
}
echo '</div>';
echo '<div class="col-md-4"><strong>Date de début du contrat : </strong>';
if (isset($mc_date_debut)) {
    echo ucfirst($mc_date_debut[0]->mc_d_date_livraison);
} else {
    echo 'NON';
}
echo '</div>';
echo '<div class="col-md-8"><strong>Date de fin du contrat : </strong>';
if (isset($mc_date_fin)) {
    echo ucfirst($mc_date_fin[0]->mc_d_date_livraison);
} else {
    echo 'NON';
}
echo '</div>';
echo '<div class="col-md-4"><strong>Ferme</strong> : ';
if (isset($mc_ferme)) {
    echo $mc_ferme[0]->f_nom;
}
echo '</div>';
echo '<div class="col-md-8"><strong>Paysans</strong> : ';
if ($mc_ferme_paysan) {
    $pay_nbr = count($mc_ferme_paysan);
    $point_virgule = 1;
    foreach ($mc_ferme_paysan as $pay) {
        echo strtoupper($pay->pay_nom).' '.ucfirst(strtolower($pay->pay_prenom));
        if ($point_virgule < $pay_nbr) {
            echo ' ; ';
        }
        ++$point_virgule;
    }
}
echo '</div>';
echo '</div>';
// -----------------------------------------------------------------------------
?>

<form method="POST" action="" id="create">

    <div class="form-group">

        <button class="btn btn-primary btn-paste" onclick="cpl()" type="button">Copier la première ligne partout
        </button>

        <button class="btn btn-primary btn-paste" onclick="ct()" type="button">Calcul des montants</button>

        <script type="text/javascript">
          function cpl() {

            $('#copier_premiere_ligne').val(1);
            document.getElementById("create").submit();

          }

          function ct() {

            $('#calculer_totaux').val(1);
            document.getElementById("create").submit();

          }
        </script>

    </div>

    <input type="hidden" name="copier_premiere_ligne" id="copier_premiere_ligne" value="0"/>

    <input type="hidden" name="calculer_totaux" id="calculer_totaux" value="0"/>

    <input type="hidden" name="envoi" value="x"/>

    <?php

    // ÉDITION
    echo '<input type="hidden" name="mc_id" value="'.$mc_id.'" />';

    if ($c_id) {
        echo '<input type="hidden" name="c_id" value="'.$c_id.'" />';
    }

    if ($amapien_id) {
        echo '<input type="hidden" name="amapien_id" value="'.$amapien_id.'"/>';

        if ($this->session->ID == $amapien_id && !$this->session->gest_amap_active) {
            echo '<input type="hidden" name="ref_prod" value="1"/>';
        }
    }

    if ($alerte_livraisons_obligatoires) {
        echo '<div class="alert alert-danger ">'.$alerte_livraisons_obligatoires.'</div>';
    }

    if ($alerte_choix_identiques) {
        echo '<div class="alert alert-danger ">'.$alerte_choix_identiques.'</div>';
    }

    if ($mes_contrats) {
        echo '<input type="hidden" name="mes_contrats" value="1" />';
    }

    ?>

    <div id="parent">
        <table id="fixTable" class="table">
            <thead class="thead-inverse">
            <tr>
                <th></th>

                <?php

                if ($total) {
                    echo '<th><input style="min-width:8em;" value="'.number_format($total, 2, ',', '').'" readonly/></th>';
                }

                foreach ($mc_produits as $mc_pro) {
                    echo '<th class="text-center">'.ucfirst($mc_pro->f_pro_nom).' <br/>'.ucfirst($mc_pro->f_pro_conditionnement).'<br/>'.$mc_pro->mc_pro_prix.' €</th>';
                }

                ?>

            </tr>
            </thead>
            <tbody>

            <?php

            foreach ($mc_dates_livraison as $mc_d) {
                echo '<tr><th scope="row">'.$mc_d->mc_d_date_livraison.'</th>';

                // SOUS  TOTAUX
                if ($sous_totaux) {
                    echo '<td><input style="min-width:8em;" value="'.number_format($sous_totaux[$mc_d->mc_d_date_livraison], 2, ',', '').'" readonly/></td>';
                }

                $i = 0; // incrémentation des tableaux dates : dates_$mc_d->mc_d_id[$i]

                foreach ($mc_produits as $mc_pro) {
                    $x = 0; // Si exclusion sur le produit, incrémentation de $x et on passe a un autre produit de $mc_exclusions

                    foreach ($mc_produits_exclusions as $mc_pro_ex) {
                        if ($mc_pro->mc_pro_id == $mc_pro_ex->mc_pro_ex_fk_modele_contrat_produit_id && $mc_d->mc_d_id == $mc_pro_ex->mc_pro_ex_fk_modele_contrat_date_id) {
                            echo '<td><input type="text" style="min-width:8em;" class="form-control text-center input-medium" name="date_'.$mc_d->mc_d_id.'[]" value="xxx" readonly /></td>';

                            // ATTENTION : le script ne marche plus avec "disabled" au lieu de "readonly". La valeur "xxx" doit être envoyée !

                            ++$x;

                            break;
                        }
                    }

                    if (0 == $x) {
                        if ($copier_premiere_ligne) {
                            $v = $copier_premiere_ligne[$i];
                        } else {
                            $v = set_value('date_'.$mc_d->mc_d_id.'['.$i.']');
                        }

                        echo '<td><input style="min-width:8em;" type="number" class="form-control text-center" name="date_'.$mc_d->mc_d_id.'[]" value="'.$v.'" step="0.5" min="0" /></td>';
                    }

                    ++$i;
                }

                echo '</tr>';
            }

            ?>

            </tbody>
        </table>
    </div>


    <div class="form-group">

        <input type="submit" value="Sauvegarder" class="pull-right btn btn-success"/>

        <?php

        if ($mes_contrats) {
            echo '<a href="'.site_url('amapien/mes_contrats').'" class="pull-right btn btn-default" style="margin-right:5px;">Annuler</a>';
        } elseif ($test) {
            echo '<a href="'.site_url('contrat_vierge').'" class="pull-right btn btn-default" style="margin-right:5px;">Annuler</a>';
        } else {
            echo '<a href="'.site_url('contrat_signe/').'" class="pull-right btn btn-default" style="margin-right:5px;">Annuler</a>';
        }

        ?>

    </div>

</form>
