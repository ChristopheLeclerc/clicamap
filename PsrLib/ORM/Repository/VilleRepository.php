<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PsrLib\ORM\Entity\Ville;

/**
 * @extends EntityRepository<Ville>
 */
class VilleRepository extends EntityRepository
{
    public function qbCpVille(string $cp, string $ville): QueryBuilder
    {
        return $this
            ->createQueryBuilder('v')
            ->where('v.cp = :cp')
            ->setParameter('cp', $cp)
            ->andWhere('v.nom = :ville')
            ->setParameter('ville', $ville)
        ;
    }

    public function isCpVilleExistAndUnique(string $cp, string $ville): bool
    {
        $count = $this
            ->qbCpVille($cp, $ville)
            ->select('COUNT(v)')
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return 1 === (int) $count;
    }

    public function getOneFromCpVille(string $cp, string $ville): Ville
    {
        return $this
            ->qbCpVille($cp, $ville)
            ->getQuery()
            ->getSingleResult()
        ;
    }

    /**
     * @return Ville[]
     */
    public function getByCpOrdered(string $cp): array
    {
        return $this
            ->createQueryBuilder('v')
            ->where('v.cp LIKE :cp')
            ->setParameter('cp', '%'.$cp.'%')
            ->orderBy('v.cp', 'ASC')
            ->addOrderBy('v.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
