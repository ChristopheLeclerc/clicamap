<?php

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class AmapDistributionAjout
{
    public const REP_AUCUNE = 'REP_AUCUNE';
    public const REP_SEMAINE = 'REP_SEMAINE';
    public const REP_2SEMAINES = 'REP_2SEMAINES';
    public const REPETITIONS = [
        'Aucune' => self::REP_AUCUNE,
        'Tous les semaines' => self::REP_SEMAINE,
        'Toutes les deux semaines' => self::REP_2SEMAINES,
    ];

    /**
     * @var ?AmapLivraisonLieu
     * @Assert\NotNull(message="Merci de sélectionner un lieu de livraison")
     * @Assert\Valid
     */
    private $amapLivraisonLieu;

    /**
     * @var ?AmapDistributionDetail
     * @Assert\Valid
     */
    private $detail;

    /**
     * @var ?Period
     * @Assert\Valid
     */
    private $period;

    /**
     * @var ?string
     * @Assert\Choice(choices=AmapDistributionAjout::REPETITIONS)
     */
    private $repetition;

    /**
     * @Assert\Callback
     */
    public function validatePeriodRepetition(ExecutionContextInterface $context): void
    {
        $period = $this->getPeriod();
        if (null === $period) {
            return;
        }

        if (self::REP_AUCUNE !== $this->getRepetition()) {
            return;
        }

        $periodStart = $period->getStartAt();
        $periodEnd = $period->getEndAt();

        if (null === $periodStart || null === $periodEnd) {
            return;
        }

        if ($period->getStartAt()->notEqualTo($period->getEndAt())) {
            $context
                ->buildViolation('La date de début et de fin doivent être les mêmes si il n\'y a pas de répétition')
                ->atPath('repetition')
                ->addViolation()
            ;
        }
    }

    public function getAmapLivraisonLieu(): ?AmapLivraisonLieu
    {
        return $this->amapLivraisonLieu;
    }

    public function setAmapLivraisonLieu(?AmapLivraisonLieu $amapLivraisonLieu): AmapDistributionAjout
    {
        $this->amapLivraisonLieu = $amapLivraisonLieu;

        return $this;
    }

    public function getDetail(): ?AmapDistributionDetail
    {
        return $this->detail;
    }

    public function setDetail(?AmapDistributionDetail $detail): AmapDistributionAjout
    {
        $this->detail = $detail;

        return $this;
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): AmapDistributionAjout
    {
        $this->period = $period;

        return $this;
    }

    public function getRepetition(): ?string
    {
        return $this->repetition;
    }

    public function setRepetition(?string $repetition): AmapDistributionAjout
    {
        $this->repetition = $repetition;

        return $this;
    }
}
