<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use PsrLib\DTO\SearchAmapState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\Reseau;

class AmapRepository extends BaseUserRepository
{
    public function qbAllOrdered(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')
        ;
    }

    public function qbAmapAccessibleForAmapien(Amapien $amapien): QueryBuilder
    {
        if ($amapien->isSuperAdmin()) {
            return $this->qbBaseForLocationSearch();
        }

        if ($amapien->isAdminRegion()) {
            return $this
                ->qbBaseForLocationSearch()
                ->andWhere('departement.region IN (:regions)')
                ->setParameter('regions', $amapien->getAdminRegions()->toArray())
            ;
        }

        if ($amapien->isAdminDepartment()) {
            return $this
                ->qbBaseForLocationSearch()
                ->andWhere('departement IN (:departements)')
                ->setParameter('departements', $amapien->getAdminDepartments()->toArray())
                ;
        }

        // Defaut amapien's amap
        return $this
            ->qbBaseForLocationSearch()
            ->andWhere(':amapien MEMBER OF a.amapiens')
            ->setParameter('amapien', $amapien)
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByRegion(Region $region): array
    {
        return $this
            ->qbBaseForLocationSearch()
            ->where('departement.region = :region')
            ->setParameter('region', $region)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByReseau(Reseau $reseau)
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'ville')
            ->leftJoin(Reseau::class, 'reseau', Join::WITH, 'reseau.ville = :ville')
            ->where('reseau = :reseau')
            ->setParameter('reseau', $reseau)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByDepartement(Departement $departement): array
    {
        return $this
            ->qbBaseForLocationSearch()
            ->where('departement = :departement')
            ->setParameter('departement', $departement)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Ferme[] $fermes
     */
    public function qbByFermes($fermes): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.amapiens', 'amapiens')
            ->leftJoin('amapiens.refProdFermes', 'ferme')
            ->andWhere('ferme in (:fermes)')
            ->setParameter('fermes', $fermes)
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByFerme(Ferme $ferme)
    {
        return $this
            ->qbByFermes([$ferme])
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param mixed $fermes
     *
     * @return Ferme[]
     */
    public function findByFermes($fermes)
    {
        return $this
            ->qbByFermes($fermes)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Amap[]
     */
    public function search(SearchAmapState $amapSearchState): array
    {
        $qb = $this->qbBaseForLocationSearch()
            // optimisation
            ->leftJoin('a.amapienRefReseau', 'amapienRefReseau')
            ->addSelect('amapienRefReseau')
            ->leftJoin('a.amapienRefReseauSecondaire', 'amapienRefReseauSecondaire')
            ->addSelect('amapienRefReseauSecondaire')
        ;

        if ($amapSearchState->isEmpty()) {
            return [];
        }
        if (null !== $amapSearchState->getRegion()) {
            $qb
                ->andWhere('departement.region = :region')
                ->setParameter('region', $amapSearchState->getRegion())
            ;
        }

        if (null !== $amapSearchState->getDepartement()) {
            $qb
                ->andWhere('departement = :departement')
                ->setParameter('departement', $amapSearchState->getDepartement())
            ;
        }

        if (null !== $amapSearchState->getReseaux()) {
            $qb
                ->andWhere('ville IN (:villes)')
                ->setParameter('villes', $amapSearchState->getReseaux()->getVilles())
            ;
        }

        if (null !== $amapSearchState->getAdhesion()) {
            $qb
                ->leftJoin('a.anneeAdhesions', 'aa')
                ->andWhere('aa.annee = :annee')
                ->setParameter('annee', $amapSearchState->getAdhesion())
            ;
        }

        if (null !== $amapSearchState->getEtudiante()) {
            $qb
                ->andWhere('a.amapEtudiante = :etudiante')
                ->setParameter('etudiante', $amapSearchState->getEtudiante())
            ;
        }

        if (null !== $amapSearchState->getKeyWord()) {
            $qb
                ->andWhere('a.nom LIKE :keyword')
                ->setParameter('keyword', '%'.$amapSearchState->getKeyWord().'%')
            ;
        }

        if ($amapSearchState->getInsurance()) {
            $qb
                ->andWhere('a.possedeAssurance = TRUE')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return string[]
     */
    public function getAllEmails()
    {
        $emails = $this
            ->createQueryBuilder('a')
            ->select('a.email')
            ->where('a.email IS NOT NULL')
            ->andWhere('a.email != \'\'')
            ->getQuery()
            ->getArrayResult()
        ;

        return array_column($emails, 'email');
    }

    /**
     * @param int[]  $reg_ids
     * @param int[]  $dep_ids
     * @param int[]  $res_ids
     * @param string $en_recherche_partenariat
     * @param string $adh_oui
     *
     * @return Amap[]
     */
    public function publipostage_mdr($reg_ids, $dep_ids, $res_ids, $en_recherche_partenariat, $adh_oui): array
    {
        $qb = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.anneeAdhesions', 'aa')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'v')
            ->leftJoin('v.departement', 'd')
            ->leftJoin('d.region', 'r')
            ->leftJoin(Reseau::class, 'reseau', Join::WITH, 'v MEMBER OF reseau.villes')
        ;

        if ($adh_oui) {
            $annee_en_cours = date('Y');
            $annee_passee = $annee_en_cours - 1;
            $annees = [$annee_en_cours, $annee_passee];

            $qb
                ->andWhere('aa.annee IN (:annees)')
                ->setParameter('annees', $annees)
            ;
        }

        // -----------------------------------------------------------------------
        if ($res_ids) {
            $qb
                ->andWhere('reseau.id IN (:res_ids)')
                ->setParameter('res_ids', $res_ids)
            ;
        } elseif ($dep_ids) {
            $qb
                ->andWhere('d.id IN (:dep_ids)')
                ->setParameter('dep_ids', $dep_ids)
            ;
        } elseif ($reg_ids) {
            $qb
                ->andWhere('d.id IN (:reg_ids)')
                ->setParameter('reg_ids', $reg_ids)
            ;
        }
        $qb->orderBy('a.nom', 'ASC');

        return $qb->getQuery()->getResult();
    }

    private function qbBaseForLocationSearch(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')
            ->leftJoin('a.livraisonLieux', 'll')
            ->addSelect('ll')
            ->leftJoin('ll.ville', 'ville')
            ->addSelect('ville')
            ->leftJoin('ville.departement', 'departement')
            ->addSelect('departement')
        ;
    }
}
