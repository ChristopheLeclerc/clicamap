<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('BASEPATH') or exit('No direct script access allowed');

class ConnexionRules
{
    public const EMAIL_EXISTE_ET_ACTIF_CALLABLE = 'email_existe_et_actif_callable';
    public const CONCORDANCE_EMAIL_PASSWORD_CALLABLE = 'concordance_email_password_callable';

    /**
     * @var \PsrLib\Services\UserRequestHelper
     */
    private $userrequesthelper;

    /**
     * ConnexionRules constructor.
     */
    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->library('form_validation');

        $this->userrequesthelper = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\PsrLib\Services\UserRequestHelper::class)
        ;
    }

    /**
     * Vérifier si le profil existe et s'il est actif.
     *
     * @param $email
     * @param $statut
     *
     * @return bool
     */
    public function email_existe_et_actif($email)
    {
        $statut = $this->CI->input->post('statut');
        if (null === $statut) {
            $statut = $this->CI->input->post('lostpassword_statut');
        }
        if (null === $statut) {
            $this
                ->CI
                ->form_validation
                ->set_message(
                    self::EMAIL_EXISTE_ET_ACTIF_CALLABLE,
                    'Aucun statut fourni'
                )
            ;

            return false;
        }

        /** @var \PsrLib\ORM\Entity\BaseUser $user */
        $user = $this->userrequesthelper->getUserFromEmailStatut($email, $statut);

        if (null === $user) {
            $this
                ->CI
                ->form_validation
                ->set_message(
                    self::EMAIL_EXISTE_ET_ACTIF_CALLABLE,
                    'L\'adresse email n\'existe pas dans la base de donnée des '.$statut.'s.'
                )
            ;

            return false;
        }

        if (!$user->estActif()) {
            $this
                ->CI
                ->form_validation
                ->set_message(
                    self::EMAIL_EXISTE_ET_ACTIF_CALLABLE,
                    'Le profil est désactivé.'
                )
            ;

            return false;
        }

        if ($user instanceof \PsrLib\ORM\Entity\Paysan && null === $user->getFerme()) {
            $this
                ->CI
                ->form_validation
                ->set_message(
                    self::EMAIL_EXISTE_ET_ACTIF_CALLABLE,
                    'Le paysan n\'a aucune ferme associée.'
                )
            ;

            return false;
        }

        return true;
    }

    /**
     * L'email et le mot de passe concordent-ils ?
     *
     * @param $email
     * @param $password
     * @param $statut
     *
     * @return bool
     */
    public function concordance_email_password($password)
    {
        $statut = $this->CI->input->post('statut');
        $email = $this->CI->input->post('email');

        /** @var \PsrLib\ORM\Entity\BaseUser $user */
        $user = $this->userrequesthelper->getUserFromEmailStatut($email, $statut);

        if (null === $user || false === password_verify($password, $user->getPassword())) {
            $this
                ->CI
                ->form_validation
                ->set_message(
                    self::CONCORDANCE_EMAIL_PASSWORD_CALLABLE,
                    'L\'adresse email et le mot de passe saisis ne correspondent pas.'
                )
            ;

            return false;
        }

        return true;
    }
}
