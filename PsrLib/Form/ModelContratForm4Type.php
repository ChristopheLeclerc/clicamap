<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\ModeleContrat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModelContratForm4Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('produitsIdentiquePaysan', YesNoChoiceType::class)
            ->add('produitsIdentiqueAmapien', YesNoChoiceType::class)
            ->add('nblivPlancher', NumberType::class)
            ->add('nblivPlancherDepassement', YesNoChoiceType::class, [
                'required' => false,
            ])
            ->add('amapienPermissionReportLivraison', YesNoChoiceType::class, [
                'required' => false,
            ])
            ->add('amapienReportNb', NumberType::class, [
                'required' => false,
            ])
            ->add('amapienPermissionDeplacementLivraison', YesNoChoiceType::class, [
                'required' => false,
            ])
            ->add('amapienDeplacementMode', ChoiceType::class, [
                'choices' => [
                    '1/2' => 'demi',
                    '1' => 'entier',
                ],
            ])

            // Initialize nb deplacement
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var ModeleContrat $data */
                $data = $event->getData();
                $data->setAmapienDeplacementNb(
                    $data->getDates()->count() - $data->getNblivPlancher()
                );
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'data_class' => ModeleContrat::class,
            'validation_groups' => ['form4'],
        ]);
    }
}
