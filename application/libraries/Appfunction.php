<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Appfunction
{
    //Nettoyer la chaîne de caractère
    public function caracteres_speciaux($chaine, $charset = 'utf-8')
    {
        $chaine = htmlentities($chaine, ENT_NOQUOTES, $charset);
        $chaine = trim($chaine);
        $chaine = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $chaine); // Enlève les accents
        $chaine = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $chaine); // pour les ligatures (le e dans le o)
        $chaine = preg_replace('#&[^;]+;#', '', $chaine); // supprime les autres caractères
        $chaine = preg_replace('/[^A-Za-z0-9]+/', '-', $chaine); // On remplace les caracteres non-alphanumériques par le tiret
        $chaine = strtolower($chaine); // On convertit le tout en minuscules

        return trim($chaine, '-'); // Supprime les tirets en début ou en fin de chaine
    }

    // Le tableau est-il vide de valeurs ?
    public function array_vide($a)
    {
        foreach ($a as $k => $v) {
            if (!empty($v)) {
                return false;
            }
        }

        return true;
    }

    public function array_find(array $xs, callable $f)
    {
        foreach ($xs as $x) {
            if (true === call_user_func($f, $x)) {
                return $x;
            }
        }

        return null;
    }

    public function cloneArray(array $old)
    {
        $new = [];

        foreach ($old as $k => $v) {
            $new[$k] = clone $v;
        }

        return $new;
    }
}
