<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
/** @var \Symfony\Component\Form\FormView $form */
$nbDatesContract = $contrat->getDates()->count();
?>

<h3><?= $contrat->getNom(); ?> : modalités du contrat</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Création
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_2'); ?>">Étape 2</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_3'); ?>">Étape 3</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 4
</h5>

<form method="POST" action="">

    <div class="well well-sm">
        <div class="row">

            <div class="col-xs-12">
  		<fieldset><legend>Modalités générales</legend>
                <label>Le paysan propose-t-il des produits identiques à toutes les livraisons ? <span class="text-danger">*</span></label>
                <div class="form-group">

                    <?= render_form_radio_widget_inline($form['produitsIdentiquePaysan']); ?>
                </div>

                <label>L'amapien choisit-il des produits identiques à chaque livraison ? <span class="text-danger">*</span></label>
                <div class="form-group">

                    <?= render_form_radio_widget_inline($form['produitsIdentiqueAmapien']); ?>
                </div>

                <div class="form-group">
                    <label>Nombre de livraison plafond sur lequel le paysan s’engage :</label>
                    <input type="text" id="nb_liv_paysan" class="form-control" disabled value="<?= $nbDatesContract; ?>">
                </div>

                <label>Nombre de livraison plancher sur lequel le paysan accepte que l’amapien s’engage : <span class="text-danger">*</span></label>
                <div class="btn btn-info"
                     data-container="body"
                     data-toggle="popover"
                     data-placement="top"
                     data-content="Un paysan peut proposer que l'amapien ait la possibilité d'avoir moins de livraisons que le nombre total de livraisons."
                >
                    <i class="glyphicon glyphicon-info-sign"></i> Bulle info
                </div>
                <div class="form-group">

                    <?= render_form_number_widget($form['nblivPlancher'], '1', '1', $nbDatesContract); ?>
                </div>
		<label>L'amapien peut-il dépasser le nombre de livraison plancher ? <span class="text-danger">*</span></label>
                <div class="form-group">
                    <?= render_form_radio_widget_inline($form['nblivPlancherDepassement']); ?>
                </div>
		</fieldset>

		<fieldset><legend>Modalités de déplacement/report</legend>
		    <div class="col-xs-12">
			<dl class="alert alert-info">
			    <dt>Définition des termes :</dt>
			    <dd>Une livraison amapien est déplacée quand elle est affectée sur une date avec une livraison paysan et une livraison amapien vide.</dd>
			    <dd>Une livraison amapien est reportée quand elle est affectée sur une date ayant déjà un livraison.</dd>
			    <dt>Conditions d'utilisation :</dt>
			    <dd>L'automatisation des déplacements est possible uniquement si le paysan propose des produits identiques à toutes les livraisons et si l'amapien choisit des produits identiques.</dd>
			    <dd>Dans les autres cas seuls l'AMAP et le référent peuvent intervenir sur les déplacements ou les reports des contrats amapiens.</dd>
			</dl>
		    </div>
                <div class="form-deplacement">

                    <div class="form-deplacement-vide">
                        <label id="label-deplacements">L'amapien est-il autorisé à déplacer une livraison sur une date sans livraison amapien ? <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <?= render_form_radio_widget_inline($form['amapienPermissionDeplacementLivraison']); ?>
                        </div>
                    </div>

                    <label id="label-report" >L'amapien est-il autorisé à reporter une livraison sur une date ayant déjà une livraison ? <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <?= render_form_radio_widget_inline($form['amapienPermissionReportLivraison']); ?>
                    </div>

                    <div class="form-report-details">
                        <div class="form-group">
                            <label for="mc_amapien_report_nb">Si oui, combien de livraisons peut-il reporter :</label>
                            <?= render_form_number_widget($form['amapienReportNb']); ?>
                        </div>
                    </div>
                    <div class="form-group form-deplacement-mode">

                        <label>Mode de report/déplacement des livraisons par demi ou entier : <span class="text-danger">*</span></label>
                        <div class="btn btn-info"
                             data-container="body"
                             data-toggle="popover"
                             data-placement="top"
                             data-content="Dans le cas de report de livraison par 1/2  et si 2 reports sont possibles, alors 2 dates deviennent sans livraison et 4 dates deviennent 1 + 1/2 livraisons (soit 1.5 livraisons). Le nombre de livraisons totales reste identique."
                        >
                            <i class="glyphicon glyphicon-info-sign"></i> Bulle info
                        </div>
                        <?= render_form_select_widget($form['amapienDeplacementMode']); ?>
                    </div>

                </div>
		</fieldset>

                <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>
                <a href="<?= site_url('/contrat_vierge/v2_contrat_form_3'); ?>" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
                <a href="<?= site_url('contrat_vierge'); ?>" class="pull-right btn btn-default">Annuler</a>

            </div>
        </div>
    </div>

</form>


<script>

  function showForm(formClass) {
    $(formClass).show();
    $(formClass + ' input').attr('disabled', false);
    $(formClass + ' select').attr('disabled', false);
  }

  function hideForm(formClass) {
    $(formClass).hide();
    $(formClass + ' input').attr('disabled', true);
    $(formClass + ' select').attr('disabled', true);
  }

    $(document).ready(function () {
      function updateShowFormPermission()
      {
        if($('input[name="model_contrat_form4[produitsIdentiquePaysan]"]').filter(":checked").val() === '1'
            && $('input[name="model_contrat_form4[produitsIdentiqueAmapien]"]').filter(":checked").val() === '1') {
          showForm('.form-deplacement');
        } else {
          hideForm('.form-deplacement');
        }
      }
      updateShowFormPermission();

      $('input[name="model_contrat_form4[produitsIdentiquePaysan]"]').on('change', function () {
        updateShowFormPermission();
      });
      $('input[name="model_contrat_form4[produitsIdentiqueAmapien]"]').on('change', function () {
        updateShowFormPermission();
      });

      function updateShowFormReport()
      {
        if(parseInt($('#nb_liv_paysan').val()) <= parseInt($('input[name="model_contrat_form4[nblivPlancher]"]').val())) {
          hideForm('.form-deplacement-vide');
        } else {
          showForm('.form-deplacement-vide');
        }
      }
      updateShowFormReport();
      $('input[name="model_contrat_form4[nblivPlancher]"]').on('change', updateShowFormReport);

      function updateShowFormReportDetails()
      {
        if($('input[name="model_contrat_form4[amapienPermissionReportLivraison]"]').filter(":checked").val() === '1') {
          showForm('.form-report-details');
        } else {
          hideForm('.form-report-details');
        }
      }
      updateShowFormReportDetails();
      $('input[name="model_contrat_form4[amapienPermissionReportLivraison]"]').on('change', updateShowFormReportDetails);

      function updateShowFormReportMode()
      {
        if($('input[name="model_contrat_form4[amapienPermissionReportLivraison]"]').filter(":checked").val() === '1'
            || $('input[name="mc_amapien_permission_deplacement_livraison"]').filter(":checked").val() === '1') {
          showForm('.form-deplacement-mode');
        } else {
          hideForm('.form-deplacement-mode');
        }
      }
      updateShowFormReportMode();
      $('input[name="model_contrat_form4[amapienPermissionReportLivraison]"]').on('change', function () {
        updateShowFormReportMode();
      });
      $('input[name="mc_amapien_permission_deplacement_livraison"]').on('change', function () {
        updateShowFormReportMode();
      });

    });

</script>
