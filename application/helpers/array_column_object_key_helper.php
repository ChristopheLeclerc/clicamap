<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!function_exists('array_column_object_key')) {
    function array_column_object_key(array $items, string $key, string $value)
    {
        $pa = \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor();
        $res = [];
        foreach ($items as $item) {
            $itemKey = $pa->getValue($item, $key);
            $itemValue = $pa->getValue($item, $value);
            $res[$itemKey] = $itemValue;
        }

        return $res;
    }
}
