<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Annotation\DraftPropertie;
use PsrLib\Validator\Numeric;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AkAmapLivraisonLieu.
 *
 * @ORM\Table(name="ak_amap_livraison_lieu")
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AmapLivraisonLieuRepository")
 */
class AmapLivraisonLieu implements LocalisableEntity
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="amap_liv_lieu_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizard"})
     */
    private $id;

    /**
     * @var Amap
     *
     * @ORM\ManyToOne(targetEntity="Amap", inversedBy="livraisonLieux")
     * @ORM\JoinColumn(name="amap_liv_lieu_fk_amap_id", nullable=false, referencedColumnName="amap_id")
     */
    private $amap;

    /**
     * @var string
     *
     * @ORM\Column(name="amap_liv_lieu", type="string", length=255)
     * @Assert\NotBlank
     */
    private $nom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="amap_liv_lieu_lib_adr", type="string", length=255)
     * @Assert\NotBlank
     */
    private $adresse = '';

    /**
     * @var string
     *
     * @ORM\Column(name="amap_liv_lieu_gps_latitude", type="string", length=255)
     * @Assert\NotBlank
     * @Numeric
     */
    private $gpsLatitude = '';

    /**
     * @var string
     *
     * @ORM\Column(name="amap_liv_lieu_gps_longitude", type="string", length=255)
     * @Assert\NotBlank
     * @Numeric
     */
    private $gpsLongitude = '';

    /**
     * @DraftPropertie()
     *
     * @var null|string
     *
     * @ORM\Column(name="amap_liv_lieu_uuid", type="string", length=255, nullable=true)
     */
    private $amapLivLieuUuid;

    /**
     * @var ArrayCollection<AmapLivraisonHoraire>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\AmapLivraisonHoraire", mappedBy="livraisonLieu", cascade={"persist", "remove"})
     */
    private $livraisonHoraires;

    /**
     * @var ArrayCollection<ModeleContrat>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ModeleContrat", mappedBy="livraisonLieu", cascade={"persist", "remove"})
     */
    private $modeleContrats;

    /**
     * @var null|Ville
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ville", fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="v_id")
     * @Assert\NotNull
     */
    private $ville;

    /**
     * @var ArrayCollection<AmapDistribution>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\AmapDistribution", mappedBy="amapLivraisonLieu", cascade="remove")
     */
    private $distributions;

    /**
     * AmapLivraisonLieu constructor.
     */
    public function __construct()
    {
        $this->livraisonHoraires = new ArrayCollection();
        $this->modeleContrats = new ArrayCollection();
        $this->distributions = new ArrayCollection();
        $this->amapLivLieuUuid = Uuid::uuid4()->toString();
    }

    public function __toString()
    {
        return (string) $this->nom;
    }

    public function getRegion(): ?Region
    {
        return $this->getVille()->getDepartement()->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return $this->getVille()->getDepartement();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): AmapLivraisonLieu
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): AmapLivraisonLieu
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getGpsLatitude(): string
    {
        return $this->gpsLatitude;
    }

    public function setGpsLatitude(string $gpsLatitude): AmapLivraisonLieu
    {
        $this->gpsLatitude = $gpsLatitude;

        return $this;
    }

    public function getGpsLongitude(): string
    {
        return $this->gpsLongitude;
    }

    public function setGpsLongitude(string $gpsLongitude): AmapLivraisonLieu
    {
        $this->gpsLongitude = $gpsLongitude;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): AmapLivraisonLieu
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }

    public function setAmap(Amap $amap): AmapLivraisonLieu
    {
        $this->amap = $amap;

        return $this;
    }

    /**
     * Add livraisonHoraire.
     *
     * @return AmapLivraisonLieu
     */
    public function addLivraisonHoraire(AmapLivraisonHoraire $livraisonHoraire)
    {
        $this->livraisonHoraires[] = $livraisonHoraire;

        return $this;
    }

    /**
     * Remove livraisonHoraire.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeLivraisonHoraire(AmapLivraisonHoraire $livraisonHoraire)
    {
        return $this->livraisonHoraires->removeElement($livraisonHoraire);
    }

    /**
     * Get livraisonHoraires.
     *
     * @return Collection
     */
    public function getLivraisonHoraires()
    {
        return $this->livraisonHoraires;
    }

    /**
     * Add modeleContrat.
     *
     * @return AmapLivraisonLieu
     */
    public function addModeleContrat(ModeleContrat $modeleContrat)
    {
        $this->modeleContrats[] = $modeleContrat;

        return $this;
    }

    /**
     * Remove modeleContrat.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModeleContrat(ModeleContrat $modeleContrat)
    {
        return $this->modeleContrats->removeElement($modeleContrat);
    }

    /**
     * Get modeleContrats.
     *
     * @return Collection
     */
    public function getModeleContrats()
    {
        return $this->modeleContrats;
    }

    /**
     * @return AmapLivraisonLieu
     */
    public function addDistribution(AmapDistribution $livraisonHoraire)
    {
        $this->distributions[] = $livraisonHoraire;

        return $this;
    }

    /**
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDistribution(AmapDistribution $livraisonHoraire)
    {
        return $this->distributions->removeElement($livraisonHoraire);
    }

    /**
     * @return Collection
     */
    public function getDistributions()
    {
        return $this->distributions;
    }
}
