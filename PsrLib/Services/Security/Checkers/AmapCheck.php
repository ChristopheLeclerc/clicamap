<?php

namespace PsrLib\Services\Security\Checkers;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Reseau;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class AmapCheck extends AbstractCheck
{
    public function isGrantedAmapList(): bool
    {
        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        if ($this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isAdmin()) {
            return true;
        }

        return false;
    }

    public function isGrantedAmapChangePassword(Amap $amap): bool
    {
        if ($this->getCurrentUser() instanceof Amap) {
            return $this->getCurrentUser()->getId() === $amap->getId();
        }

        return false;
    }

    public function isGrantedAmapSendPassword(Amap $amap): bool
    {
        return !empty($amap->getEmail())
            && $this->isGrantedMapChangeState($amap);
    }

    public function isGrantedMapChangeState(Amap $amap): bool
    {
        if ($this->getCurrentUser() instanceof Amapien) {
            // Le super admin peut changer l'état de  toutes les AMAPS
            if ($this->getCurrentUser()->isSuperAdmin()) {
                return true;
            }

            // L'admin de région peut changer l'état de  toutes les AMAP de sa région
            if ($this->getCurrentUser()->isAdminOf($amap)) {
                return true;
            }

            //L'admin réseau peut changer l'état de  les amaps de ses réseaux
            if ($this->getCurrentUser()->isAdminReseaux()) {
                /** @var Reseau $reseau */
                foreach ($this->getCurrentUser()->getAdminReseaux() as $reseau) {
                    if ($reseau->getVilles()->contains($amap->getAdresseAdminVille())) {
                        return true;
                    }
                }

                return false;
            }
        }

        return false;
    }

    public function isGrantedAmapDisplay(Amap $amap): bool
    {
        if ($this->getCurrentUser() instanceof Amap) {
            return $this->getCurrentUser()->getId() === $amap->getId();
        }

        if ($this->isGrantedMapChangeState($amap)) {
            return true;
        }

        if ($this->getCurrentUser() instanceof Amapien && $this->getCurrentUser()->isRefProduit()) {
            return $this->getCurrentUser()->getAmap()->getId() === $amap->getId();
        }

        return false;
    }

    public function isGrantedAmapCreate(): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isAdmin()
        ;
    }

    public function isGrantedAmapSearchPaysan(): bool
    {
        return $this->getCurrentUser() instanceof Amap;
    }

    public function isGrantedAmapSubscribe(?BaseUser $amap): bool
    {
        if (!($amap instanceof Amap)) {
            return false;
        }
        // Les amaps peuvent gérer les inscriptions à eux meme
        if ($this->getCurrentUser() instanceof Amap) {
            return $this->getCurrentUser()->getId() === $amap->getId();
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            return $this->getCurrentUser()->getAmap()->getId() === $amap->getId();
        }

        return false;
    }

    public function isGrantedAmapShowMap(Amap $amap): bool
    {
        // Les comptent qui peuvent afficher l'amap peuvent afficher la carte
        if ($this->isGrantedAmapDisplay($amap)) {
            return true;
        }

        // Les amapiens associés à l'amap peuvent afficher la carte
        if ($this->getCurrentUser() instanceof Amapien) {
            return $amap->getAmapiens()->contains($this->getCurrentUser());
        }

        return false;
    }

    public function isGrantedAmapManageAbs(?Amap $amap): bool
    {
        if (null === $amap) {
            return false;
        }

        if (true === $amap->getAmapEtudiante()) {
            return $this->isGrantedAmapDisplay($amap);
        }

        return false;
    }
}
