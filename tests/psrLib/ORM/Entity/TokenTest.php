<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\Token;

/**
 * @internal
 * @coversNothing
 */
class TokenTest extends TestCase
{
    public function testValidSameDate()
    {
        CarbonImmutable::setTestNow('1900-01-01');

        $token = new Token();
        self::assertTrue($token->isValid());
    }

    public function testValidNextDay()
    {
        CarbonImmutable::setTestNow('1900-01-01');
        $token = new Token();

        CarbonImmutable::setTestNow('1900-01-02');
        self::assertTrue($token->isValid());
    }

    public function testInvalid()
    {
        CarbonImmutable::setTestNow('1900-01-01');
        $token = new Token();

        CarbonImmutable::setTestNow('1900-01-04');
        self::assertFalse($token->isValid());
    }

    public function testInvalidNextMonth()
    {
        CarbonImmutable::setTestNow('1900-01-01');
        $token = new Token();

        CarbonImmutable::setTestNow('1900-02-01');
        self::assertFalse($token->isValid());
    }
}
