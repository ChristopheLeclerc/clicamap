<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\VilleRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VilleType extends AbstractType
{
    /**
     * @var VilleRepository
     */
    private $villeRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->villeRepo = $em
            ->getRepository(Ville::class)
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var bool $notNullOption */
        $notNullOption = $options['not_null'];

        $builder
            ->addModelTransformer(new CallbackTransformer(
                function (?Ville $ville) {
                    return $ville;
                },
                function (?string $cpVille) use ($notNullOption) {
                    if (null === $cpVille) {
                        if (true === $notNullOption) {
                            throw new TransformationFailedException(
                                '',
                                0,
                                null,
                                'Le champ "Code postal, Ville administrative" est requis.'
                            );
                        }

                        return null;
                    }
                    $adr = explode(',', $cpVille);
                    if (2 !== count($adr)) {
                        throw new TransformationFailedException(
                            '',
                            0,
                            null,
                            'Le champ "Code postal, Ville" est mal formaté.'
                        );
                    }

                    $cp = trim($adr[0]);
                    $ville = trim($adr[1]);

                    $ville = $this
                        ->villeRepo
                        ->findOneBy([
                            'cp' => $cp,
                            'nom' => $ville,
                        ])
                    ;
                    if (null === $ville) {
                        throw new TransformationFailedException(
                            '',
                            0,
                            null,
                            'Le couple "'.$cp.'" / "'.$ville.'" ne correspond à aucun lieu.'
                        );
                    }

                    return $ville;
                }
            ))
        ;
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ville::class,
            'attr' => ['class' => 'admin_cp_ville_autocomplete'],
            'empty_data' => null,
            'required' => false,
            'not_null' => false,
        ]);
    }
}
