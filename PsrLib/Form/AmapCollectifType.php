<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\AmapCollectif;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AmapienRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapCollectifType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('profil', ChoiceType::class, [
                'choices' => [
                    'Président' => AmapCollectif::PROFIL_PRESIDENT,
                    'Secrétaire' => AmapCollectif::PROFIL_SECRETAIRE,
                    'Trésorier' => AmapCollectif::PROFIL_TRESORIER,
                    'Autre' => AmapCollectif::PROFIL_AUTRE,
                ],
            ])
            ->add('precision', TextType::class)
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                /** @var AmapCollectif $data */
                $data = $event->getData();
                $form = $event->getForm();

                $form
                    ->add('amapien', EntityType::class, [
                        'class' => Amapien::class,
                        'query_builder' => function (AmapienRepository $repository) use ($data) {
                            return $repository
                                ->qbByAmap($data->getAmap())
                            ;
                        },
                        'choice_label' => function (?Amapien $amapien) {
                            if (null === $amapien) {
                                return null;
                            }

                            return strtoupper($amapien->getNom()).' '.$amapien->getPrenom();
                        },
                    ])
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AmapCollectif::class,
        ]);
    }
}
