<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
echo '<h3>Informations générales</h3>';

echo '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.$mc->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Mise à zéro des quantités commandées sur une ou plusieurs dates de livraison <i class="glyphicon glyphicon-menu-right"></i> Étape 1</h5>';

?>


<div class="row">
    <div class="form-group">
        <div class="col-md-12">

            <div class="alert alert-info">
                <p>Cette fonctionnalité sera à nouveau bientôt disponible</p>
            </div>

        </div>
    </div>
</div>
