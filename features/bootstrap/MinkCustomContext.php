<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Behat\Mink\Exception\ElementNotFoundException;
use Behat\MinkExtension\Context\RawMinkContext;

class MinkCustomContext extends RawMinkContext
{
    use MinkContextTrait;

    public const USER_PASSWORD = 'P@ssw0rd!';

    /**
     * @Given je suis authentifié comme :type avec le mail :email
     *
     * @param mixed $type
     * @param mixed $email
     */
    public function jeSuisAuthentifieCommeAvecLeMail($type, $email)
    {
        $this->minkContext->visit('/portail/connexion');
        $this->minkContext->fillField('statut', $type);
        $this->minkContext->fillField('email', $email);
        $this->minkContext->fillField('password', self::USER_PASSWORD);
        $this->minkContext->pressButton('Connexion');
        $this->minkContext->assertPageAddress('http://localhost/evenement');
    }

    /**
     * @Then je me déconnecte
     */
    public function jeMeDeconnecte()
    {
        $this->minkContext->visit('/portail/deconnexion');
    }

    /**
     * @Then je devrais voir le lien :selecteurLien vers :destination
     *
     * @param mixed $selecteurLien
     * @param mixed $destination
     */
    public function jeDevraisVoirLeLienVers($selecteurLien, $destination)
    {
        $lien = $this->minkContext->getSession()->getPage()->findLink($selecteurLien);
        if (null === $lien) {
            throw new ElementNotFoundException($this->minkContext->getSession()->getDriver(), 'link', 'id|title|alt|text', $selecteurLien);
        }

        $destinationLien = $lien->getAttribute('href');
        if ($destinationLien !== $destination) {
            throw new \LogicException(
                sprintf('Erreur de lien. Attentu : %s. Trouvé : %s', $destination, $destinationLien)
            );
        }
    }

    /**
     * @Then le selecteur :selecteur devrait contenir l'option :option
     *
     * @param mixed $selecteurLien
     * @param mixed $destination
     * @param mixed $selecteur
     * @param mixed $option
     */
    public function leSelecteurDevraitContenirLoption($selecteur, $option)
    {
        $selecteur = $this->minkContext->getSession()->getPage()->find('css', "select[name='{$selecteur}']");
        if (null === $selecteur) {
            throw new ElementNotFoundException($this->minkContext->getSession()->getDriver(), 'select', 'name', $selecteur);
        }

        $option = $selecteur->find('named', ['option', $option]);
        if (null === $option) {
            throw new \LogicException(
                sprintf('Option %s introuvable', $option)
            );
        }
    }

    /**
     * @Then le selecteur :selecteur ne devrait pas contenir l'option :option
     *
     * @param mixed $selecteurLien
     * @param mixed $destination
     * @param mixed $selecteur
     * @param mixed $option
     */
    public function leSelecteurNeDevraitPasContenirLoption($selecteur, $option)
    {
        $selecteur = $this->minkContext->getSession()->getPage()->find('css', "select[name='{$selecteur}']");
        if (null === $selecteur) {
            throw new ElementNotFoundException($this->minkContext->getSession()->getDriver(), 'select', 'name', $selecteur);
        }

        $option = $selecteur->find('named', ['option', $option]);
        if (null !== $option) {
            throw new \LogicException(
                sprintf('Option %s présente alors qu\'elle ne devrais pas', $option)
            );
        }
    }

    /**
     * @Then je remplis le datepicker :datePickerName avec :val
     *
     * @param mixed $datePickerName
     * @param mixed $val
     */
    public function jeRemplisLeDatepickerAvecLaValeur($datePickerName, $val)
    {
        $this->minkContext->getSession()->executeScript(
            sprintf(
                "$('input.datepicker[name=\"%s\"]').val('%s').trigger('changeDate')",
                $datePickerName,
                $val
            )
        );

        $this->minkContext->getSession()->executeScript(
            sprintf(
                "var field = $('input[name=\"%s\"]'); field.val('%s'); var searchForm = field.parents('.js-form-search'); if(searchForm.length > 0){searchForm.submit()}",
                $datePickerName,
                $val
            )
        );
    }
}
