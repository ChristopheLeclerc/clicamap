<?php

namespace Test\DTO;

use Carbon\Carbon;
use PsrLib\DTO\AmapDistributionAjout;
use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;
use Test\ValidationViolationTrait;

/**
 * @internal
 * @coversNothing
 */
class AmapDistributionAjoutTest extends ContainerAwareTestCase
{
    use ValidationViolationTrait;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testNoRepetitionPeriodSameDate()
    {
        $ajout = new AmapDistributionAjout();
        $ajout->setPeriod(Period::buildFromDates(new Carbon('2000-01-01'), new Carbon('2000-01-01')));
        $ajout->setRepetition(AmapDistributionAjout::REP_AUCUNE);

        $violations = $this->validator->validate($ajout);
        self::assertViolationNotContainsMessage($violations, 'La date de début et de fin doivent être les mêmes si il n\'y a pas de répétition');
    }

    public function testNoRepetitionPeriodDifferentDate()
    {
        $ajout = new AmapDistributionAjout();
        $ajout->setPeriod(Period::buildFromDates(new Carbon('2000-01-01'), new Carbon('2000-01-02')));
        $ajout->setRepetition(AmapDistributionAjout::REP_AUCUNE);

        $violations = $this->validator->validate($ajout);
        self::assertViolationContainsMessage($violations, 'La date de début et de fin doivent être les mêmes si il n\'y a pas de répétition');
    }

    /**
     * @dataProvider repetitionProvider
     */
    public function testRepetitionPeriodSameDate(string $repetition)
    {
        $ajout = new AmapDistributionAjout();
        $ajout->setPeriod(Period::buildFromDates(new Carbon('2000-01-01'), new Carbon('2000-01-01')));
        $ajout->setRepetition($repetition);

        $violations = $this->validator->validate($ajout);
        self::assertViolationNotContainsMessage($violations, 'La date de début et de fin doivent être les mêmes si il n\'y a pas de répétition');
    }

    /**
     * @dataProvider repetitionProvider
     */
    public function testRepetitionPeriodDifferentDate(string $repetition)
    {
        $ajout = new AmapDistributionAjout();
        $ajout->setPeriod(Period::buildFromDates(new Carbon('2000-01-01'), new Carbon('2000-01-02')));
        $ajout->setRepetition($repetition);

        $violations = $this->validator->validate($ajout);
        self::assertViolationNotContainsMessage($violations, 'La date de début et de fin doivent être les mêmes si il n\'y a pas de répétition');
    }

    public function repetitionProvider()
    {
        return [
            [AmapDistributionAjout::REP_SEMAINE],
            [AmapDistributionAjout::REP_2SEMAINES],
        ];
    }
}
