<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var string $validation_url */
/** @var string $validation_label */
/** @var string $cancel_url */
/** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $produits */
/** @var \PsrLib\ORM\Entity\ModeleContratDate[] $dates */
/** @var \PsrLib\ORM\Entity\ModeleContratProduitExclure[] $livExclusions */
/** @var Carbon $dateInscriptionMin */
/** @var \PsrLib\ORM\Entity\ContratCellule[] $existingCommand */
/** @var ?string $prevalidationMessage */
use Carbon\Carbon;

/**
 * @param \PsrLib\ORM\Entity\ModeleContratProduitExclure[] $exclusions
 * @param \PsrLib\ORM\Entity\ModeleContratDate             $date
 * @param \PsrLib\ORM\Entity\ModeleContratProduit          $produit
 */
function exclusionExiste($exclusions, PsrLib\ORM\Entity\ModeleContratDate $date, PsrLib\ORM\Entity\ModeleContratProduit $produit)
{
    foreach ($exclusions as $exclusion) {
        if ($exclusion->getModeleContratDate()->getId() === $date->getId()
            && $exclusion->getModeleContratProduit()->getId() === $produit->getId()) {
            return true;
        }
    }

    return false;
}

/**
 * @param Carbon                               $dateInscriptionMin
 * @param \PsrLib\ORM\Entity\ModeleContratDate $dateLiv
 */
function dateLimiteLivraisonPassee($dateInscriptionMin, $dateLiv)
{
    return $dateInscriptionMin->greaterThan($dateLiv->getDateLivraison());
}

?>

<button class="btn btn-success btn-copy">
    Copier la première ligne partout
</button>

<form method="POST" action="<?=$validation_url; ?>" id="form-simulation">
        <table class="table table-bordered simulation-container">
            <thead>
                <tr class="active">
                    <th colspan="2"></th>
                    <?php foreach ($produits as $produit) : ?>
                        <th class="text-center">
                            <u><?=$produit->getNom(); ?></u>
                            <br />
                            <?=$produit->getConditionnement(); ?>
                            <br />
                            <span class="badge"><?=number_format_french($produit->getPrix()).' €'; ?></span>
                        </th>
                    <?php endforeach; ?>
                </tr>

                <tr class="active">
                    <th class="text-right" style="border-right: 0px;">Total :</th>
                    <th>
                        <div class="input-group">
                            <input
                                    name="total"
                                    class="form-control text-right total"
				    size="4"
                                    disabled
                            >
                            <span class="input-group-addon">€</span>
                        </div>
                    </th>
                    <?php foreach ($produits as $produit) : ?>
                        <th>
                            <div class="input-group">
                                <input
                                        disabled
                                        class="form-control text-right total-produit"
                                        data-produit-id="<?=$produit->getId(); ?>"
                                />
                                <span class="input-group-addon">€</span>
                            </div>
                        </th>
                    <?php endforeach; ?>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($dates as $date) :?>
                <tr class="input-row">
                    <td class="text-left" style="vertical-align: middle;"><?=date_format_french($date->getDateLivraison()); ?></td>
                    <td>
                        <input
			    disabled
			    class="form-control text-right total-date"
			    size="3"
			    data-date="<?=$date->getDateLivraison()->format('Y-m-d'); ?>"
                        />
                    </td>
                    <?php foreach ($produits as $produit) : ?>
                        <td>
                            <?php if (!exclusionExiste($livExclusions, $date, $produit)): ?>
                                <?php $fieldName = "commande[{$date->getId()}][{$produit->getId()}]"; ?>
                                <input
                                    <?php if (dateLimiteLivraisonPassee($dateInscriptionMin, $date)): ?>readonly<?php endif; ?>
                                    name="<?=$fieldName; ?>"
                                    type="number"
                                    class="form-control text-center simulation-input"
                                    min="0"
                                    step="1"
                                    data-produit-id="<?=$produit->getId(); ?>"
                                    data-date="<?=$date->getDateLivraison()->format('Y-m-d'); ?>"
                                    value="<?=set_value($fieldName, \PsrLib\ORM\Repository\ContratCelluleRepository::findCelluleCount($existingCommand, $date, $produit)); ?>"
                                />
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>

        </table>

        <div style="margin-top: 10px">
            <?php if (null !== $prevalidationMessage): ?>
            <p><b><?=$prevalidationMessage; ?></b></p>
            <?php endif; ?>
            <?php if (null !== $validation_url): ?>
                <button
                    class="btn btn-success pull-right"
                    type="submit"
                    style="margin-left: 10px;"
                >
                    <?= $validation_label; ?>
                </button>
            <?php endif; ?>
            <a class="btn btn-default pull-right" href="<?=$cancel_url; ?>">Retour</a>
        </div>
    <div class="clearfix"></div>
</form>

<script>
  function formatNumber(num)
  {
    return num.toFixed(2).replace('.', ',');
  }
  $(document).ready(function () {
    var priceMap = {
        <?php
            $prices = [];
            foreach ($produits as $produit) {
                $prices[] = $produit->getId().':'.$produit->getPrix();
            }
            echo implode(',', $prices);
        ?>
    };

    // Activate Datatable
    $('.simulation-container').DataTable({
      paging:           false,
      ordering:         false,
      info:             false,
      searching:        false,
      scrollY:          600,
      scrollX:          true,
      scrollCollapse:   true,
      fixedColumns: {
        leftColumns: 2,
      }
    });

    function updateCount()
    {
      // Calculate sum
      var sumDate = {};
      var sumProduct = {};
      $('.simulation-input')
        .each(function (index, elt) {
          var $elt = $(elt);
          var pId = $elt.data('produit-id');
          var pDate = $elt.data('date');
          var val = $elt.val();
          var price = Math.round(val  * priceMap[pId] * 100)/100;

          if(!(pId in sumProduct)) {
            sumProduct[pId] = 0.0;
          }
          sumProduct[pId] += price;

          if(!(pDate in sumDate)) {
            sumDate[pDate] = 0.0;
          }
          sumDate[pDate] += price;
        });

      // Update columns
      Object.keys(sumProduct).forEach(function (pId) {
        var sumRounded = Math.round(sumProduct[pId]*100)/100;
        $('.total-produit[data-produit-id=' + pId + ']').val(formatNumber(sumRounded));
      });

      // Update rows
      var total = 0.0;
      Object.keys(sumDate).forEach(function (pDate) {
        var sumRounded = Math.round(sumDate[pDate]*100)/100;

        $('.total-date[data-date=' + pDate + ']').val(formatNumber(sumRounded));
        total += sumDate[pDate];
      });

      // Update total
      $('.total').val(formatNumber(Math.round(total*100)/100));
    }

    $('.simulation-container').on('change', '.simulation-input', function (e) {
      updateCount();
    });

    // Trigger calc on startup
    updateCount();

    $('.btn-copy').on('click', function (e) {
      e.preventDefault();

      $('.input-row')
        .each(function () {
          var $rowSimulationInputs = $(this).find('.simulation-input');
          if($rowSimulationInputs.length > 0) {
            $rowSimulationInputs
              .each(function () {
                  var pId = $(this).data('produit-id');
                  var val = $(this).val();
                  $('.simulation-input[data-produit-id=' + pId + ']')
                    .val(val)
                });
            return false; // only trigger first non empty line
          }
        });
      updateCount();
    });

  });
</script>

<style>
    .simulation-input {
        min-width: 150px;
    }

    .total-date {
        min-width: 100px;
    }
</style>

