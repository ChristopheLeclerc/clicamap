<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\CarbonDateType;
use PsrLib\Form\Type\VilleType;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Paysan;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaysanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var BaseUser $currentUser */
        $currentUser = $options['current_user'];

        $nameOption = [];
        if ($currentUser instanceof Paysan) {
            $nameOption = [
                'required' => false,
                'disabled' => true,
                'attr' => ['readonly' => true],
            ];
        }

        $builder
            ->add('statut', ChoiceType::class, [
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    'Groupement' => Paysan::STATUT_GROUPEMENT,
                    'Entreprise individuelle' => Paysan::STATUT_EI,
                ],
            ])
            ->add('nom', TextType::class, $nameOption)
            ->add('prenom', TextType::class, $nameOption)
            ->add('conjointNom', TextType::class, [
                'required' => false,
            ])
            ->add('conjointPrenom', TextType::class, [
                'required' => false,
            ])
            ->add('email', EmailType::class)
            ->add('emailInvalid', YesNoChoiceType::class)
            ->add('numTel1', TextType::class, [
                'required' => false,
            ])
            ->add('numTel2', TextType::class, [
                'required' => false,
            ])
            ->add('libAdr', TextType::class, [
                'required' => false,
            ])
            ->add('ville', VilleType::class, [
                'required' => false,
            ])
            ->add('dateInstallation', CarbonDateType::class, [
                'required' => false,
            ])
            ->add('anneeDebCommercialisationAmap', TextType::class, [
                'required' => false,
            ])
            ->add('msa', TextType::class, [
                'required' => false,
            ])
            ->add('spg', TextType::class, [
                'required' => false,
            ])
            ->add('newsletter', YesNoChoiceType::class)
        ;

        if ($currentUser instanceof Amapien && $currentUser->isAdmin()) {
            $builder
                ->add('cadreReseau', PaysanCadreReseauType::class)
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Paysan::class,
            'current_user' => null,
        ]);
    }
}
