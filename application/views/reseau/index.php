<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Reseau[] $reseaux */
?>

<script type="text/javascript">

  /*ALERTES*/
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 3000);

  });

</script>

<?php
// ALERTES
if ($create || $edit || $remove || $erreur_403) {
    if ($create) {
        $verbe = 'créé';
    } elseif ($edit) {
        $verbe = 'édité';
    } elseif ($remove) {
        $verbe = 'supprimé';
    } ?>
    <div class="modal fade" id="alerte">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">

                    <?php

                    /*        if($vide)
                                echo '<p><i class="glyphicon glyphicon-remove" style="color:#C9302C;"></i> Le champ ne peut pas être vide !</p>';*/

                    if ($erreur_403) {
                        echo '<p><i class="glyphicon glyphicon-remove" style="color:#C9302C;"></i> Erreur 403 Accès refusé/interdit. Vous n\'avez pas les droits nécessaires.</p>';
                    } else {
                        echo '<p><i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> Le réseau a été '.$verbe.' avec succès !</p>';
                    } ?>

                </div>
            </div>
        </div>
    </div>

    <?php
}

echo '<h3>Les réseaux</h3>';

echo '<div class="alert alert-info">Pour créer un réseau qui ne soit pas un département ou une région, comme une communauté de commune, une Métropole ou autre, assurez-vous d\'abord qu\'il n\'existe pas déjà dans la liste ci-dessous. Sinon, créez-le avec le champ d\'ajout ci-dessous.</div>';

// AJOUTER UN RÉSEAU
echo '<form method="POST" action="'.site_url('reseau/creation').'">';

echo '<div class="row">';
echo '<div class="form-group col-sm-12">';
echo '<input type="text" name="reseau" class="form-control" placeholder="Exemple : Métropole de Lyon"/>';
echo form_error('reseau');
echo '</div>';
echo '<div class="form-group col-sm-12">';
echo '<input type="submit" value="Créer le réseau" class="btn btn-success pull-right"/>';
echo '</div>';
echo '</div>';
echo '</form>';

// LISTE DES RÉSEAUX
if (count($reseaux) > 0) {
    echo '<div class="alert alert-info">À l\'aide des outils de cette liste, vous pourrez changer le nom du réseau, le supprimer, lui ajouter des villes ainsi que des administrateurs.</div>';

    echo '<div class="table-responsive">';
    echo '<table class="table table-hover">';
    echo '<thead class="thead-inverse">';
    echo '<tr>';
    echo '<th>Réseaux</th>';
    echo '<th class="text-right">Outils</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    foreach ($reseaux as $reseau) {
        echo '<tr>';
        echo '<th scope="row">';
        echo ucfirst($reseau->getNom());
        echo '</th>';

        echo '<td class="text-right">';
        // Logo de la région
        echo '<a title="Icone du réseau" href="'.site_url('reseau/reseau_logo/'.$reseau->getId()).'"><i class="glyphicon glyphicon-picture" data-toggle="tooltip" title="Icone du réseau"></i></a>&nbsp;';
        // Éditer le nom du réseau
        echo '<a title="Éditer le nom du réseau" href="#" data-toggle="modal" data-target="#modal_edition_'.$reseau->getId().'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Éditer le nom du réseau"></i></a>&nbsp;';
        // Villes du réseau
        echo '<a title="Villes du réseau" href="'.site_url('reseau/ville/'.$reseau->getId()).'"><i class="glyphicon glyphicon-cloud" data-toggle="tooltip" title="Villes du réseau"></i></a>&nbsp;';
        // Administrateurs du réseau
        echo '<a title="Administrateurs du réseau" href="'.site_url('reseau/admin/'.$reseau->getId()).'"><i class="glyphicon glyphicon-education" data-toggle="tooltip" title="Administrateurs du réseau"></i></a>&nbsp;';
        // Supprimer le réseau
        echo '<a title="Supprimer le réseau" href="#" data-toggle="modal" data-target="#modal_remove_'.$reseau->getId().'"><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le réseau"></i></a>'; ?>

        <!--MODAL ÉDITION-->
        <div class="modal fade text-left" id="modal_edition_<?= $reseau->getId(); ?>" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Édition du nom du réseau</h4>
                    </div>
                    <div class="modal-body">

                        <form method="POST" action="<?= site_url('reseau/creation/'.$reseau->getId()); ?>">

                            <input type="hidden" name="ID" value="<?= $reseau->getId(); ?>"/>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <input class="form-control" name="reseau"
                                               value="<?= htmlspecialchars($reseau->getNom()); ?>"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="submit" value="Éditer" class="pull-right btn btn-success"/>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!--MODAL SUPPRESSION-->
        <div class="modal fade text-left" id="modal_remove_<?= $reseau->getId(); ?>" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Réseau "<?= $reseau->getNom(); ?>"</h4>
                    </div>
                    <div class="modal-body">
                        <p class="pull-right">
                            <a href="<?= site_url('reseau/remove/'.$reseau->getId()); ?>"
                               class="btn btn-danger btn-sm">OUI</a>
                            <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                        </p>
                        <h5>Voulez-vous vraiment supprimer le réseau ?</h5>
                    </div>
                </div>
            </div>
        </div>

        <?php

        echo '</td>';
        echo '</tr>';
    }

    echo '</tbody>';
    echo '</table>';
    echo '</div>';
}

?>
