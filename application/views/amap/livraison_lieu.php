<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
$livraison_lieu = $amap->getLivraisonLieux();
/** @var \Symfony\Component\Form\FormView $form */
?>

<script type="text/javascript">
  $(document).ready(function () {

    $(function () {
      $("#liv_autocomplete").autocomplete({
        source: <?= '"'.site_url('ajax/geocode_get_suggestion').'"'; ?>,
        dataType: "json",
        minLength: 4,
        appendTo: "#container_liv_autocomplete",
        select: function (event, ui) {

          // Maj des informations sur la page
          $("#liv_autocomplete").val(ui.item.label);
          $('input[name="amap_livraison_lieu[adresse]"]').val(ui.item.value.address);
          $('input[name="amap_livraison_lieu[gpsLatitude]"]').val(ui.item.value.lat.toString());
          $('input[name="amap_livraison_lieu[gpsLongitude]"]').val(ui.item.value.long.toString());
          $('input[name="amap_livraison_lieu[ville]"]').val(
            ui.item.value.cp.toString() + ', ' + ui.item.value.ville.toString()
          );

          // Stop la propagation de l'évènement
          return false;
        }
      });
    });

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 3000);

  });
</script>

<?php

// ALERTES
if (isset($impossible)) {
    ?>
    <div class="modal fade" id="alerte">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">

                    <p><i class="glyphicon glyphicon-remove" style="color:#9D4444;"></i>&nbsp;Une AMAP doit avoir au
                        moins un lieu de livraison.</p>

                </div>
            </div>
        </div>
    </div>
    <?php
}

echo '<h3>Les lieux de livraison</h3>';

if (get_current_connected_user() instanceof \PsrLib\ORM\Entity\Amap) {
    $retour = 'Gestion de mon AMAP';
} else {
    $retour = 'Gestion des AMAP';
}

?>

<h5><a href="<?= site_url('amap'); ?>"><?= $retour; ?></a> <i
            class="glyphicon glyphicon-menu-right"></i> <?= mb_strtoupper($amap->getNom()); ?> <i
            class="glyphicon glyphicon-menu-right"></i> Les lieux de livraison</h5>


<div class="row">
    <div class="col-md-12" id="livraison_lieu">

        <!-- ----------------------------------------------------------------------- -->
        <!-- AJOUTER UN LIEU -->
        <form method="POST" action="<?= site_url('amap/livraison_lieu/'.$amap->getId()); ?>" class="clearfix">

            <input type="hidden" name="step_l" value="1"/>

            <div class="entry">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="liv_lieu" class="control-label">Nom du lieu</label>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <?= render_form_text_widget($form['nom']); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="entry">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="liv_autocomplete" class="control-label">Adresse de livraison</label>
                        </div>
                        <div class="col-xs-12 form-group">

                            <div class="alert alert-info">
                                Utilisez le champ ci-dessous pour compléter l'adresse de manière automatique grâce aux
                                suggestions proposées.
                                Il est possible que la rue ou le numéro ne soit pas connu de l'outil. Dans ce cas, vous
                                pouvez soit modifier
                                manuellement le champ N° et rue ci-dessus, soit contribuer au projet <a target="_blank"
                                                                                                        href="https://wiki.openstreetmap.org/wiki/FR:Guide_du_d%C3%A9butant">Open
                                    Street Map</a>
                                pour que votre adresse soit précisément localisée.
                            </div>

                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                                <input type="text" class="form-control" id="liv_autocomplete" name="liv_autocomplete"
                                       placeholder="Veuillez taper ici l'adresse et sélectionner une suggestion"/>
                            </div>

                            <div id="container_liv_autocomplete" class="ui-autocomplete"
                                 style="width:100%;font-size:11px;"></div>

                        </div>

                        <div class="col-xs-12">
                            <div class="alert alert-info">
                                Si vous ne connaissez pas l'adresse exacte, indiquez simplement la ville ou le lieu-dit
                                et la rue
                            </div>
                        </div>


                        <div class="col-md-2">
                            <label for="liv_adr" class="control-label">N°, voie</label>
                            <?= render_form_text_widget($form['adresse']); ?>
                        </div>

                        <div class="col-md-4">
                            <label for="liv_cp_ville" class="control-label">Code postal, Ville</label>
                            <?= render_form_text_widget($form['ville']); ?>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="liv_gps_lat" class="control-label">GPS Latitude<br/></label>
                            <?= render_form_text_widget($form['gpsLatitude']); ?>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="liv_gps_long" class="control-label">GPS Longitude<br/></label>
                            <?= render_form_text_widget($form['gpsLongitude']); ?>
                        </div>
                    </div>
                </div>

            </div>

	    <div class="form-group text-right">
		<input type="submit" value="Ajouter" class="pull-right btn btn-success clearfix"
		       style="margin-left:5px;"/>
	    </div>

	</form>

    </div>
</div><!-- row -->

<!-- ----------------------------------------------------------------------- -->
<!-- TABLEAU DES LIEUX EXISTANTS -->
<?php
if (!$livraison_lieu->isEmpty()) {
    echo '<div class="table-responsive">';
    echo '<table class="table table-hover">';
    echo '<thead class="thead-inverse">';
    echo '<tr>';
    echo '<th>Nom</th>';
    echo '<th>Adresse, CP, Ville de livraison</th>';
    echo '<th class="text-right">Outils</th>';
    echo '</tr>';
    echo '</thead>';

    echo '<tbody>';

    /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $liv_lieu */
    foreach ($livraison_lieu as $liv_lieu) {
        echo '<tr>';
        echo '<th scope="row">';
        echo ucfirst($liv_lieu->getNom());
        echo '</th>';

        echo '<td>';
        echo ucfirst($liv_lieu->getAdresse()).', '.$liv_lieu->getVille()->getCpString().' '.mb_strtoupper($liv_lieu->getVille()->getNom());
        echo '</td>';

        echo '<td class="text-right">';
        echo '<a title="Modifier les horaires de livraison" href="'.site_url('amap/livraison_horaire/'.$liv_lieu->getId()).'"><i class="glyphicon glyphicon-time" data-toggle="tooltip" title="Modifier les horaires de livraison"></i></a>&nbsp;';
        echo '<a title="Modifier le lieu de livraison" href="'.site_url('amap/livraison_lieu_edit/'.$liv_lieu->getId()).'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier le lieu de livraison"></i></a>&nbsp;';
        if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_DELIVERY_PLACE_DELETE, $liv_lieu)) {
            echo '<a title="Supprimer le lieu de livraison" href="#" data-toggle="modal" data-target="#modal_remove_'.$liv_lieu->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le lieu de livraison"></i></a>';
        }
        echo '</td>';
        echo '</tr>'; ?>
        <!-- Modal remove -->
        <div class="modal fade text-left" id="modal_remove_<?= $liv_lieu->getId(); ?>"
             tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                        </button>
                        <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer le lieu de
                            livraison <?= $liv_lieu->getNom(); ?>" ?</h5>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger"><strong>Attention !</strong> En supprimant le lieu de
                            livraison, vous supprimerez aussi ses horaires ainsi que ses plages horaires de
                            distrib'AMAP. Toutes les inscriptions liées à ces plages seront de fait supprimées
                            elles aussi.
                        </div>
                        <p class="text-right">
                            <a href="<?= site_url('amap/livraison_lieu_remove/'.$liv_lieu->getId()); ?>"
                               class="btn btn-danger btn-sm">OUI</a>
                            <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    echo '</tbody>';
    echo '</table>';
    echo '</div>';
}
?>
