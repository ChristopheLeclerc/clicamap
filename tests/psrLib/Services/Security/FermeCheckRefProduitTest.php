<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class FermeCheckRefProduitTest extends AbstractSecurityTest
{
    public function testRefProduitRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REF_PRODUITS, new Ferme()));
    }
}
