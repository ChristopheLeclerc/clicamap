<?php

namespace Test\Form;

use PsrLib\DTO\SearchDistributionAmapState;
use PsrLib\Form\SearchDistributionAmapType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;

/**
 * @internal
 * @coversNothing
 */
class SearchDistributionAmapTypeTest extends AbstractFormTest
{
    public function testAmapPreselectLLIfOne()
    {
        $amap = new Amap();
        $amap->addLivraisonLieux($this->buildLL(1, 'll1'));

        $form = $this->formFactory->create(SearchDistributionAmapType::class, null, [
            'amap' => $amap,
        ]);

        $form->submit([]);
        /** @var SearchDistributionAmapState $data */
        $data = $form->getData();
        self::assertSame('ll1', $data->getLivLieu()->getNom());
        self::assertCount(1, $form->createView()->children['livLieu']->vars['choices']);
        self::assertSame('ll1', $form->createView()->children['livLieu']->vars['choices'][0]->label);
        self::assertNull($form->createView()->children['livLieu']->vars['placeholder']);
    }

    public function testAmapLLMultiple()
    {
        $amap = new Amap();
        $amap->addLivraisonLieux($this->buildLL(1, 'll1'));
        $amap->addLivraisonLieux($this->buildLL(2, 'll2'));

        $form = $this->formFactory->create(SearchDistributionAmapType::class, null, [
            'amap' => $amap,
        ]);
        $form->submit([]);

        /** @var SearchDistributionAmapState $data */
        $data = $form->getData();
        self::assertNull($data->getLivLieu());
        self::assertCount(2, $form->createView()->children['livLieu']->vars['choices']);
        self::assertSame('ll1', $form->createView()->children['livLieu']->vars['choices'][0]->label);
        self::assertSame('ll2', $form->createView()->children['livLieu']->vars['choices'][1]->label);
        self::assertSame('', $form->createView()->children['livLieu']->vars['placeholder']);
    }

    private function buildLL(int $id, string $nom): AmapLivraisonLieu
    {
        $ll = $this->prophesize(AmapLivraisonLieu::class);
        $ll->getId()->willReturn($id);
        $ll->getNom()->willReturn($nom);

        return $ll->reveal();
    }
}
