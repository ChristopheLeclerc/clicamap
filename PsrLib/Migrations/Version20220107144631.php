<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220107144631 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX FK_87ABDC63B3E7E790 ON ak_amap');
        $this->addSql('DROP INDEX fk_amap_id ON ak_amap_annee_adhesion');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) NOT NULL, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) NOT NULL');
        $this->addSql('DROP INDEX FK_244CA7849C2BA34A ON ak_ferme_produit');
        $this->addSql('DROP INDEX FK_244CA784708B3AB7 ON ak_ferme_produit');
        $this->addSql('DROP INDEX FK_DF572660DA1F6AB5 ON ak_ferme_type_production_propose');
        $this->addSql('DROP INDEX FK_A15C0C389C2BA34A ON ak_modele_contrat_produit');
        $this->addSql('DROP INDEX IND_MODELE_CONTRAT_PRODUIT_FK_MODELE_CONTRAT ON ak_modele_contrat_produit');
        $this->addSql('DROP INDEX reseau_amap_id ON ak_reseau_administrateur');
        $this->addSql('DROP INDEX ville_reseau_id ON ak_reseau_villes');
        $this->addSql('DROP INDEX vill_id ON ak_ville');
        $this->addSql('ALTER TABLE ak_modele_contrat CHANGE mc_reglement_type mc_reglement_type_old VARCHAR(255);');
        $this->addSql('ALTER TABLE ak_modele_contrat ADD COLUMN mc_reglement_type  LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\';');
        $this->addSql('UPDATE ak_modele_contrat SET mc_reglement_type = mc_reglement_type_old WHERE mc_reglement_type_old IS NOT NULL;');
        $this->addSql('ALTER TABLE ak_modele_contrat DROP COLUMN mc_reglement_type_old;');

        //TODO: compare database before and after migration with production data
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
