<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use CI_Config;
use CI_Loader;
use CI_Parser;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\Paysan;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class Email_Sender
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRequestHelper
     */
    private $userrequesthelper;

    /**
     * @var CI_Parser
     */
    private $parser;

    /**
     * @var CI_Loader
     */
    private $load;

    /**
     * @var CI_Config
     */
    private $config;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var MailerInterface
     */
    private $sfMailer;

    public function __construct(EntityManagerInterface $em, UserRequestHelper $userrequesthelper, CI_Parser $parser, CI_Loader $load, CI_Config $config, Environment $twig, MailerInterface $sfMailer)
    {
        $this->em = $em;
        $this->userrequesthelper = $userrequesthelper;
        $this->parser = $parser;
        $this->load = $load;
        $this->config = $config;
        $this->twig = $twig;
        $this->sfMailer = $sfMailer;
    }

    public function envoyerMailContact(string $nom, string $prenom, string $email, array $destination, string $cp, string $message): void
    {
        $msgTitre = 'Nouveau message du formulaire de contact Clic\'AMAP';
        $msgContenu = $this->parser->parse('email/contact.tpl', [
            'nom' => $nom,
            'prenom' => $prenom,
            'email' => $email,
            'cp' => $cp,
            'message' => $message,
        ], true);

        $this->_doSend($destination, $msgTitre, $msgContenu);
    }

    public function envoyerEnvoiConfirmationMailContact(string $nom, string $prenom, string $email, string $cp, string $message): void
    {
        $msgTitre = 'Accusé de  formulaire de contact Clic\'AMAP';
        $msgContenu = $this->parser->parse('email/contact_confirmation.tpl', [
            'nom' => $nom,
            'prenom' => $prenom,
            'email' => $email,
            'cp' => $cp,
            'message' => $message,
        ], true);

        $this->_doSend($email, $msgTitre, $msgContenu);
    }

    public function envoyerMdpReinit(string $email, BaseUser $user): void
    {
        $lien = site_url('/portail/mdp_oublie_confirmation/'.$user->getPasswordResetToken()->getToken());

        $msgTitre = 'Réinitialisation de votre mot de passe Clic\'AMAP';
        $msgContenu = $this->parser->parse('email/mdp_reinit.tpl', [
            'lien' => $lien,
        ], true);

        $this->_doSend($email, $msgTitre, $msgContenu);
    }

    public function envoyerModelContratValidationPaysan(ModeleContrat $mc, BaseUser $user): void
    {
        $lien = site_url('/');

        $ferme = $mc->getFerme();
        $amap = $mc->getLivraisonLieu()->getAmap();
        $paysans = $ferme->getPaysans();

        $msgTitre = "Contrat {$mc->getNom()} de {$amap->getNom()}";
        $msgContenu = $this->parser->parse('email/contrat_vierge_validation_paysan.tpl', [
            'lien' => $lien,
            'amap' => $amap->getNom(),
            'contrat' => $mc->getNom(),
            'nom' => $user->getNom(),
            'email' => $user->getEmail(),
        ], true);

        foreach ($paysans as $paysan) {
            $this->_doSend($paysan->getEmail(), $msgTitre, $msgContenu);
        }
    }

    public function envoyerModelContratValide(ModeleContrat $contrat, Paysan $currentUser): void
    {
        $amap = $contrat->getLivraisonLieu()->getAmap();
        $ferme = $contrat->getFerme();
        $paysans = $ferme->getPaysans();

        $refProduits = $this
            ->em
            ->getRepository(Amapien::class)
            ->getRefProduitFromAmapFerme($amap, $ferme)
        ;

        $msgTitre = "Contrat {$contrat->getNom()} validé par le paysan ".$currentUser->getPrenom().' '.$currentUser->getNom();
        $msgContenu = $this->load->view('email/contrat_vierge_valide.php', [
            'contrat' => $contrat,
            'currentUser' => $currentUser,
            'paysans' => $paysans,
        ], true);

        $this->_doSend($amap->getEmail(), $msgTitre, $msgContenu);
        foreach ($refProduits as $refProduit) {
            sleep(5);
            $this->_doSend($refProduit->getEmail(), $msgTitre, $msgContenu);
        }
    }

    public function envoyerModelContratRefus(ModeleContrat $contrat, Paysan $currentUser, string $motif): void
    {
        $amap = $contrat->getLivraisonLieu()->getAmap();
        $ferme = $contrat->getFerme();
        $paysans = $ferme->getPaysans();

        $refProduits = $this
            ->em
            ->getRepository(Amapien::class)
            ->getRefProduitFromAmapFerme($amap, $ferme)
        ;

        $msgTitre = "Contrat {$contrat->getNom()} refusé par le paysan ".$currentUser->getNom();
        $msgContenu = $this->load->view('email/contrat_vierge_refus.php', [
            'contrat' => $contrat,
            'currentUser' => $currentUser,
            'paysans' => $paysans,
            'motif' => $motif,
        ], true);

        $this->_doSend($amap->getEmail(), $msgTitre, $msgContenu);
        foreach ($refProduits as $refProduit) {
            sleep(5);
            $this->_doSend($refProduit->getEmail(), $msgTitre, $msgContenu);
        }
    }

    /**
     * @param BaseUser[] $users
     */
    public function envoyerNotificationEmailInvalide($users): void
    {
        $msgTitre = 'Nouvel email invalide detecté';
        $msgContenu = $this->load->view('email/notification_email_invalide.php', [
            'users' => $users,
        ], true);
        $msgDest = $this->config->item('invalid_email_notification_dest');

        $this->_doSend($msgDest, $msgTitre, $msgContenu);
    }

    public function envoyerDistributionInscriptionAmapAmapien(AmapDistribution $amapDistribution, Amapien $amapien): void
    {
        $msgTitre = 'Votre inscription à la Distrib\'AMAP du '.$amapDistribution->getDate()->format('d/m/Y');
        $msgContenu = $this->twig->render('email/distribution_amap_amapien_inscription.twig', [
            'distribution' => $amapDistribution,
        ]);
        $msgDest = $amapien->getEmail();

        $this->_doSend($msgDest, $msgTitre, $msgContenu);
    }

    public function envoyerDistributionDesinscriptionAmapAmapien(AmapDistribution $amapDistribution, Amapien $amapien): void
    {
        $msgTitre = 'Votre désinscription à la Distrib\'AMAP du '.$amapDistribution->getDate()->format('d/m/Y');
        $msgContenu = $this->twig->render('email/distribution_amap_amapien_deinscription.twig', [
            'distribution' => $amapDistribution,
        ]);
        $msgDest = $amapien->getEmail();

        $this->_doSend($msgDest, $msgTitre, $msgContenu);
    }

    public function envoyerDistributionAmapienRappel(AmapDistribution $amapDistribution, Amapien $amapien): void
    {
        $msgTitre = '[Rappel] Vous êtes inscrit pour aider une distribution';
        $msgContenu = $this->twig->render('email/distribution_amapien_rappel.twig', [
            'distribution' => $amapDistribution,
        ]);
        $msgDest = $amapien->getEmail();

        $this->_doSend($msgDest, $msgTitre, $msgContenu);
    }

    /**
     * @param AmapDistribution[] $amapDistributions
     * @param Amapien[]          $amapiens
     */
    public function envoyerDistributionAmapiensRelance($amapDistributions, $amapiens, bool $amapCopie = false): void
    {
        $msgTitre = 'Inscrivez-vous, reste des places pour la prochaine distrib\'';
        $msgContenu = $this->twig->render('email/distribution_amapien_relance.twig', [
            'distributions' => $amapDistributions,
        ]);

        foreach ($amapiens as $amapien) {
            $this->_doSend(
                $amapien->getEmail(),
                $msgTitre,
                $msgContenu,
                $amapCopie ? $amapDistributions[0]->getAmapLivraisonLieu()->getAmap()->getEmail() : null
            );
        }
    }

    public function envoyerDistributionAmapAlerteFin(AmapDistribution $amapDistribution): void
    {
        $msgTitre = 'Mettez à jour vos créneaux de distrib\'AMAP !';
        $msgContenu = $this->twig->render('email/distribution_amap_alerte_fin.twig');

        $this->_doSend(
            $amapDistribution->getAmapLivraisonLieu()->getAmap()->getEmail(),
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerPaysanAlerte(string $dest, string $sujet, string $contenu): void
    {
        $this->_doSend(
            $dest,
            $sujet,
            $contenu,
            'clicamap@amap-aura.org'
        );
    }

    public function envoyerMailMdp(BaseUser $user, string $password): void
    {
        $msgTitre = 'Bienvenue sur '.getenv('SITE_NAME');
        $msgContenu = $this->twig->render('email/user_mdp.twig', [
            'user' => $user,
            'password' => $password,
        ]);

        $this->_doSend(
            $user->getEmail(),
            $msgTitre,
            $msgContenu
        );
    }

    /**
     * @param string|string[] $destination
     * @param string|string[] $copie
     */
    private function _doSend($destination, string $titre, string $contenu, $copie = null): void
    {
        // Disable send for invalid emails
        if ($this->userrequesthelper->isEmailInvalid($destination)) {
            return;
        }

        $email = (new Email())
            ->from(new Address(getenv('EMAIL'), getenv('FROM')))
            ->subject($titre)
            ->html($contenu)
        ;

        if (is_array($destination)) {
            $email->to(...$destination);
        } else {
            $email->to($destination);
        }

        if (null !== $copie) {
            if (is_array($copie)) {
                $email->cc(...$copie);
            } else {
                $email->cc($copie);
            }
        }

        $errorCount = 0;
        while (true) {
            try {
                $this->sfMailer->send($email);

                return;
            } catch (TransportExceptionInterface $e) {
                ++$errorCount;
                sleep(1);

                if ($errorCount >= 5) {
                    sentryCapture($e);

                    return;
                }
            }
        }
    }
}
