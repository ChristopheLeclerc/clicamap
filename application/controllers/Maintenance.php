<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Maintenance extends AppController
{
    public function index()
    {
        // Disable maintenance page if maintenance not enable to avoid used blocked on page
        if (($_ENV['MAINTENANCE_ENABLED'] ?? null) === 'false') {
            redirect('/');
        }

        $this->loadViewWithTemplate(
            'maintenance/index',
            [],
            'template',
            [
                'header_portail' => 1,
                'menu_less' => 1,
            ]
        );
    }
}
