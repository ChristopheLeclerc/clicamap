# language: fr
Fonctionnalité: Liste des paysans associée au regroupement

  @database
  Scénario: Liste des paysans associée au regroupement
    Etant donné que je suis authentifié comme "regroupement" avec le mail "regroupement@test.amap-aura.org"
    Et que la ferme "ferme" est associées au regroupement "regroupement@test.amap-aura.org"
    Quand je suis sur "/evenement"
    Et que je suis "Gestion des paysans"
    Alors je devrais être sur "/paysan"
    Et je devrais voir "PAYSAN NOM"
