<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
?>
<h3>Les horaires de livraison</h3>

<?php

echo '<h5><a href="'.site_url('amap').'">Gestion des AMAP</a> <i class="glyphicon glyphicon-menu-right"></i> Création d\'une AMAP <i class="glyphicon glyphicon-menu-right"></i> Étape 3</h5>';

echo '<form method="POST" action="'.site_url('amap/livraison_horaire_creation').'">';

echo '<div class="alert alert-info">L\'ajout de plusieurs horaires de livraison se fait par la mise à jour de l\'AMAP rubrique "Gestion des AMAP" -> "Modifier l\'AMAP" -> "Les informations de livraison".</div>';

?>

<div class="row">
    <div class="col-md-12" id="livraison_horaires">

        <div class="entry">
            <div class="well">

                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">

                            <div class="col-md-3">
                                <label for="liv_saison" class="control-label">Saison : </label>
                                <?= render_form_select_widget($form['saison']); ?>
                            </div>

                            <div class="col-md-3">
                                <label for="liv_jour" class="control-label">Jour de livraison : </label>
                                <?= render_form_select_widget($form['jour']); ?>
                            </div>

                            <div class="col-md-3">
                                <label for="liv_heure_deb" class="control-label">Heure de début : </label>
                                <?= render_form_text_widget($form['heureDebut']); ?>
                            </div>

                            <div class="col-md-3">
                                <label for="liv_heure_fin" class="control-label">Heure de fin : </label>
                                <?= render_form_text_widget($form['heureFin']); ?>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="form-group">

    <?php
    echo '<input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>';
    echo '</form>';

    echo '<a href="/amap/livraison_lieu_creation" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>';

    echo '<a href="'.site_url('amap').'" class="pull-right btn btn-default">Annuler</a>';

    ?>

    </form>
</div>
