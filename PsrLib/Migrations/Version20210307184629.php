<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184629 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $existingVilles = $this
            ->connection
            ->executeQuery('SELECT f_id, f_code_postal, f_ville FROM ak_ferme')
            ->fetchAll()
        ;

        $this->addSql('ALTER TABLE ak_ferme ADD ville_id VARCHAR(5) DEFAULT NULL');

        foreach ($existingVilles as $existingVille) {
            if ('01240' === $existingVille['f_code_postal'] && 'LA-CHAPPELLE-DU-CHATELARD' === $existingVille['f_ville']) {
                $existingVille['f_ville'] = 'LA CHAPELLE-DU-CHÂTELARD';
            }
            if ('01460' === $existingVille['f_code_postal'] && 'PEYRIAT' === $existingVille['f_ville']) {
                $existingVille['f_code_postal'] = '01430';
            }
            if ('01370' === $existingVille['f_code_postal'] && 'TREFFORT' === $existingVille['f_ville']) {
                $existingVille['f_ville'] = 'TREFFORT-CUISIAT';
            }
            if ('38660' === $existingVille['f_code_postal'] && 'SAINT-HILAIRE' === $existingVille['f_ville']) {
                $existingVille['f_ville'] = 'PLATEAU-DES-PETITES-ROCHES';
            }
            if ('00000' === $existingVille['f_code_postal']) {
                $existingVille['f_code_postal'] = '38260';
                $existingVille['f_ville'] = 'PAJAY';
            }

            $ville = $this
                ->connection
                ->executeQuery('SELECT v_id FROM ak_ville WHERE v_nom = :nom AND v_cp = :cp', [
                    'nom' => $existingVille['f_ville'],
                    'cp' => $existingVille['f_code_postal'],
                ])
                ->fetchAll()
            ;

            if (1 !== count($ville)) {
                throw new Exception(
                    sprintf('Unable to find ville with %s %s', $existingVille['f_code_postal'], $existingVille['f_ville'])
                );
            }
            $this->addSql('UPDATE ak_ferme SET ville_id = :villeId WHERE f_id = :id', [
                'villeId' => $ville[0]['v_id'],
                'id' => $existingVille['f_id'],
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
