<?php

namespace PsrLib\Services\Security\Checkers;

use DI\Annotation\Inject;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\Services\AuthentificationMockable;
use PsrLib\Services\Security\SecurityCheckerInterface;

abstract class AbstractCheck
{
    /**
     * @var null|BaseUser
     */
    protected $currentUser;

    /**
     * @Inject
     *
     * @var SecurityCheckerInterface
     */
    protected $securitychecker;

    /**
     * @Inject
     *
     * @var AuthentificationMockable
     */
    protected $authentification;

    public function getCurrentUser(): ?BaseUser
    {
        return $this->authentification->getCurrentUser();
    }
}
