<?php

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ArrayExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('array_unique', [$this, 'arrayUnique']),
        ];
    }

    public function arrayUnique(array $array): array
    {
        return array_unique($array);
    }
}
