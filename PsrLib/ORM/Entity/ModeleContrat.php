<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="ak_modele_contrat")
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\ModeleContratRepository")
 */
class ModeleContrat
{
    public const frequences_available = [
        'Toutes les semaines' => 7,
        'Tous les quinze jours' => 14,
    ];

    public const mode_available = [
        'Chèque' => 'cheque',
        'Virement' => 'virement',
        'SEPA' => 'sepa',
        'Carte bancaire' => 'carte',
    ];

    public const ETAT_CREATION = 'creation';
    public const ETAT_VALIDATION_PAYSAN = 'validation';
    public const ETAT_VALIDE = 'valide';
    public const ETAT_REFUS = 'refus';
    public const ETAT_ACTIF = 'actif'; // Old contract state not used for new one

    /**
     * @var ?int
     *
     * @ORM\Column(name="mc_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizard", "wizardContract"})
     */
    private $id;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="mc_date_activation", type="datetime", nullable=true)
     * @Groups({"wizard"})
     */
    private $dateActivation;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_nom", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"form1"})
     * @Assert\Length(max="200", groups={"form1"})
     * @Groups({"wizard"})
     */
    private $nom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_description", type="text", length=65535, nullable=true)
     * @Groups({"wizard"})
     */
    private $description;

    /**
     * @var null|AmapLivraisonLieu
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\AmapLivraisonLieu", inversedBy="modeleContrats")
     * @ORM\JoinColumn(name="mc_livraison_lieu", referencedColumnName="amap_liv_lieu_id")
     * @Assert\NotNull(groups={"form1"}, message="Merci de sélectionner un lieu de livraison")
     * @Groups({"wizard"})
     */
    private $livraisonLieu;

    /**
     * @var ?int
     *
     * @ORM\Column(name="mc_frequence", type="integer", nullable=true)
     * @Assert\Choice(choices=ModeleContrat::frequences_available, groups={"form2"})
     * @Groups({"wizard"})
     */
    private $frequence;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="mc_forclusion", type="date", nullable=true)
     * @Assert\NotNull(groups={"form2"})
     * @Groups({"wizard"})
     */
    private $forclusion;

    /**
     * @var null|int
     *
     * @ORM\Column(name="mc_modalites", type="integer", nullable=true)
     * @Groups({"wizard"})
     */
    private $modalites;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_choix_identiques", type="boolean", nullable=true)
     * @Groups({"wizard"})
     */
    private $choixIdentiques;

    /**
     * @var null|int
     *
     * @ORM\Column(name="mc_frequence_articles", type="integer", nullable=true)
     * @Groups({"wizard"})
     */
    private $frequenceArticles;

    /**
     * @var null|int
     *
     * @ORM\Column(name="mc_nbliv_plancher", type="integer", nullable=true)
     * @Groups({"wizard"})
     */
    private $nblivPlancher;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_etat", type="string", length=255, nullable=true)
     * @Groups({"wizard"})
     */
    private $etat = self::ETAT_CREATION;

    /**
     * @var int
     *
     * @ORM\Column(name="mc_version", type="integer")
     * @Groups({"wizard"})
     */
    private $version = 2;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_filiere", type="text", length=65535, nullable=true)
     * @Assert\NotBlank(groups={"form1"})
     * @Assert\Length(max="200", groups={"form1"})
     * @Groups({"wizard"})
     */
    private $filiere;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_specificite", type="text", length=65535, nullable=true)
     * @Assert\Length(max="400", groups={"form1"})
     * @Groups({"wizard"})
     */
    private $specificite;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_regul_poid", type="boolean", nullable=true)
     * @Groups({"wizard"})
     */
    private $regulPoid;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_produits_identique_paysan", type="boolean", nullable=true)
     * @Groups({"wizard"})
     * @Assert\NotNull(groups={"form4"})
     */
    private $produitsIdentiquePaysan;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_produits_identique_amapien", type="boolean", nullable=true)
     * @Groups({"wizard"})
     * @Assert\NotNull(groups={"form4"})
     */
    private $produitsIdentiqueAmapien;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_amapien_permission_intervenir_planning", type="boolean", nullable=true)
     * @Groups({"wizard"})
     */
    private $amapienPermissionIntervenirPlanning;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_amapien_permission_deplacement_livraison", type="boolean", nullable=true)
     * @Groups({"wizard"})
     */
    private $amapienPermissionDeplacementLivraison;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_amapien_deplacement_mode", type="string", length=255, nullable=true)
     * @Groups({"wizard"})
     */
    private $amapienDeplacementMode;

    /**
     * @var null|int
     *
     * @ORM\Column(name="mc_amapien_deplacement_nb", type="integer", nullable=true)
     * @Groups({"wizard"})
     */
    private $amapienDeplacementNb;

    /**
     * @var array
     *
     * @ORM\Column(name="mc_reglement_type", type="simple_array")
     * @Assert\Count(min="1", minMessage="Merci de sélectionner au moins un mode de règlement", groups={"form6"})
     * @Groups({"wizard"})
     */
    private $reglementType = [];

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_reglement_modalite", type="text", length=65535, nullable=true)
     * @Groups({"wizard"})
     * @Assert\NotBlank(groups={"form6"})
     * @Assert\Length(max="65535")
     */
    private $reglementModalite;

    /**
     * @var null|int
     *
     * @ORM\Column(name="mc_reglement_nb_max", type="integer", nullable=true)
     * @Groups({"wizard"})
     * @Assert\NotNull(groups={"form6"})
     * @Assert\Range(min="1", minMessage="Le nombre de règlements maximum doit être supérieur ou égal à 1", groups={"form6"})
     */
    private $reglementNbMax;

    /**
     * @var null|string
     *
     * @ORM\Column(name="mc_contrat_annexes", type="text", length=65535, nullable=true)
     * @Groups({"wizard"})
     */
    private $contratAnnexes;

    /**
     * @var Ferme | null
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ferme", inversedBy="modeleContrats")
     * @ORM\JoinColumn(name="mc_fk_ferme_id", referencedColumnName="f_id")
     * @Groups({"wizard"})
     */
    private $ferme;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_amapien_permission_report_livraison", type="boolean", nullable=true)
     * @Groups({"wizard"})
     * @Assert\Expression(
     *     groups={"form4"},
     *     expression="(this.getProduitsIdentiquePaysan() !== true or this.getProduitsIdentiqueAmapien() !== true) or (value !== null)",
     *     message="Le champ ci-dessous est requis à la saisie pour passer à l'étape suivante"
     * )
     */
    private $amapienPermissionReportLivraison;

    /**
     * @var null|int
     *
     * @ORM\Column(name="mc_amapien_report_nb", type="integer", nullable=true)
     * @Groups({"wizard"})
     */
    private $amapienReportNb;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="mc_nbliv_plancher_depassement", type="boolean", nullable=true)
     * @Groups({"wizard"})
     * @Assert\NotNull(groups={"form4"})
     */
    private $nblivPlancherDepassement = false;

    /**
     * @var ArrayCollection<Contrat>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\Contrat", mappedBy="modeleContrat")
     * @Groups({"wizard"})
     */
    private $contrats;

    /**
     * @var ArrayCollection<ModeleContratDate>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ModeleContratDate", mappedBy="modeleContrat", orphanRemoval=true, cascade="persist")
     * @Assert\Count(min="1", minMessage="Les dates de livraisons sont obligatoires, veuillez cliquer sur le bouton 'générer des dates'")
     * @Assert\All({@Assert\Expression("value.getDateLivraison() !== null")}, groups={"form2"})
     * @Groups({"wizard"})
     */
    private $dates;

    /**
     * @var ArrayCollection<ModeleContratDatesReglement>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ModeleContratDatesReglement", mappedBy="modeleContrat", orphanRemoval=true, cascade="persist")
     * @Groups({"wizard"})
     */
    private $dateReglements;

    /**
     * @var ArrayCollection<ModeleContratProduit>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ModeleContratProduit", mappedBy="modeleContrat", orphanRemoval=true, cascade="persist")
     * @Groups({"wizard"})
     */
    private $produits;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->contrats = new ArrayCollection();
        $this->dates = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->dateReglements = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getNom();
    }

    public function getNbLivPlafond(): int
    {
        if ($this->getNblivPlancherDepassement()) {
            return $this->getDates()->count();
        }

        return $this->getNblivPlancher();
    }

    public static function getEtatLabel(string $etat): string
    {
        switch ($etat) {
            case self::ETAT_CREATION:
                return 'En création';

            case self::ETAT_VALIDATION_PAYSAN:
                return 'En attente validation paysan';

            case self::ETAT_VALIDE:
                return 'Validé';

            case self::ETAT_REFUS:
                return 'A revoir';
        }

        throw new InvalidArgumentException('Etat inconnu : '.$etat);
    }

    public static function getModeLabel(string $mode): string
    {
        return array_search($mode, self::mode_available, true);
    }

    public function getDatePosition(ModeleContratDate $date): ?int
    {
        $dates = $this->getDates()->toArray();
        if (0 === count($dates)) {
            return null;
        }
        usort($dates, function (ModeleContratDate $mcd1, ModeleContratDate $mcd2) {
            return $mcd1->getDateLivraison() > $mcd2->getDateLivraison();
        });
        $key = array_search($date, $dates, true);
        if (false === $key) {
            return null;
        }

        return $key + 1;
    }

    public function isArchived(): bool
    {
        $now = new Carbon();
        $now->startOfDay();
        /** @var ModeleContratDate $date */
        foreach ($this->getDates() as $date) {
            if ($now->lessThanOrEqualTo($date->getDateLivraison())) {
                return false;
            }
        }

        return true;
    }

    public function getLastDate(): ?ModeleContratDate
    {
        $dates = $this->getDates()->toArray();
        if (0 === count($dates)) {
            return null;
        }

        usort($dates, function (ModeleContratDate $a, ModeleContratDate $b) {
            $ac = Carbon::instance($a->getDateLivraison());
            $bc = Carbon::instance($b->getDateLivraison());

            return $bc->getTimestamp() - $ac->getTimestamp();
        });

        return $dates[0];
    }

    /**
     * @Assert\Callback(groups={"form6"})
     */
    public function validateReglementNbMax(ExecutionContextInterface $context): void
    {
        if (null !== $this->getReglementNbMax()
            && $this->getReglementNbMax() > $this->getDateReglements()->count()) {
            $context
                ->buildViolation('Le nombre de règlements maximum doit être inférieur ou égal au nombre de dates des règlements')
                ->atPath('reglementNbMax')
                ->addViolation()
            ;

            return;
        }
    }

    /**
     * @Assert\Callback(groups={"form4"})
     */
    public function validateAmapienReportNb(ExecutionContextInterface $context): void
    {
        if (true === $this->getAmapienPermissionReportLivraison()
            && true === $this->getProduitsIdentiqueAmapien()
            && true === $this->getProduitsIdentiquePaysan()) {
            $reportNb = $this->getAmapienReportNb();
            if ($reportNb <= 0) {
                $context
                    ->buildViolation('Cet nombre doit etre supérieur ou égal à 1')
                    ->atPath('amapienReportNb')
                    ->addViolation()
                ;

                return;
            }

            if ($reportNb > $this->getDates()->count()) {
                $context
                    ->buildViolation('Ce nombre doit être inférieur au nombre de dates du contrat')
                    ->atPath('amapienReportNb')
                    ->addViolation()
                ;

                return;
            }
        }
    }

    /**
     * @Assert\Callback(groups={"form4"})
     */
    public function validatePermissionDeplacementLivraison(ExecutionContextInterface $context): void
    {
        if (true === $this->getProduitsIdentiqueAmapien()
            && true === $this->getProduitsIdentiquePaysan()
        ) {
            if (null === $this->getAmapienPermissionDeplacementLivraison()
                && $this->getNblivPlancher() < $this->getDates()->count()) {
                $context
                    ->buildViolation('Le champ ci-dessous est requis à la saisie pour passer à l\'étape suivante')
                    ->atPath('amapienPermissionDeplacementLivraison')
                    ->addViolation()
                ;

                return;
            }
        }
    }

    /**
     * @Assert\Callback(groups={"form2"})
     */
    public function validateDateForclusion(ExecutionContextInterface $context): void
    {
        $dateForclusion = $this->getForclusion();
        if (null === $dateForclusion) {
            return;
        }

        $ferme = $this->getFerme();
        $dateForclusionAvecDelaisFerme = $ferme->getPremiereDateLivrableAvecDelais($dateForclusion);

        foreach ($this->getDates()->toArray() as $dateLivraison) {
            if ($dateForclusionAvecDelaisFerme->gt($dateLivraison->getDateLivraison())) {
                $context
                    ->buildViolation('La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)')
                    ->atPath('forclusion')
                    ->addViolation()
                    ;

                return;
            }
        }
    }

    /**
     * @Assert\Callback(groups={"form2"})
     */
    public function validateDateUnicity(ExecutionContextInterface $context): void
    {
        $dates = $this->getDates()->map(function (ModeleContratDate $date) {
            if (null === $date->getDateLivraison()) {
                return null;
            }

            return $date->getDateLivraison()->format('d/m/Y');
        })->toArray();
        if (count($dates) !== count(array_unique($dates))) {
            $context
                ->buildViolation('Merci de sélectionner une seule fois chaque date')
                ->atPath('dates')
                ->addViolation()
            ;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ModeleContrat
    {
        $this->id = $id;

        return $this;
    }

    public function getLivraisonLieu(): ?AmapLivraisonLieu
    {
        return $this->livraisonLieu;
    }

    public function setLivraisonLieu(?AmapLivraisonLieu $livraisonLieu): ModeleContrat
    {
        $this->livraisonLieu = $livraisonLieu;

        return $this;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): ModeleContrat
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getDateActivation(): ?DateTime
    {
        return $this->dateActivation;
    }

    public function setDateActivation(?DateTime $dateActivation): ModeleContrat
    {
        $this->dateActivation = $dateActivation;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): ModeleContrat
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): ModeleContrat
    {
        $this->description = $description;

        return $this;
    }

    public function getFrequence(): ?int
    {
        return $this->frequence;
    }

    public function setFrequence(?int $frequence): ModeleContrat
    {
        $this->frequence = $frequence;

        return $this;
    }

    public function getForclusion(): ?DateTime
    {
        return $this->forclusion;
    }

    public function setForclusion(?DateTime $forclusion): ModeleContrat
    {
        $this->forclusion = $forclusion;

        return $this;
    }

    public function getModalites(): ?int
    {
        return $this->modalites;
    }

    public function setModalites(?int $modalites): ModeleContrat
    {
        $this->modalites = $modalites;

        return $this;
    }

    public function getChoixIdentiques(): ?bool
    {
        return $this->choixIdentiques;
    }

    public function setChoixIdentiques(?bool $choixIdentiques): ModeleContrat
    {
        $this->choixIdentiques = $choixIdentiques;

        return $this;
    }

    public function getFrequenceArticles(): ?int
    {
        return $this->frequenceArticles;
    }

    public function setFrequenceArticles(?int $frequenceArticles): ModeleContrat
    {
        $this->frequenceArticles = $frequenceArticles;

        return $this;
    }

    public function getNblivPlancher(): ?int
    {
        return $this->nblivPlancher;
    }

    public function setNblivPlancher(?int $nblivPlancher): ModeleContrat
    {
        $this->nblivPlancher = $nblivPlancher;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): ModeleContrat
    {
        $this->etat = $etat;

        return $this;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): ModeleContrat
    {
        $this->version = $version;

        return $this;
    }

    public function getFiliere(): ?string
    {
        return $this->filiere;
    }

    public function setFiliere(?string $filiere): ModeleContrat
    {
        $this->filiere = $filiere;

        return $this;
    }

    public function getSpecificite(): ?string
    {
        return $this->specificite;
    }

    public function setSpecificite(?string $specificite): ModeleContrat
    {
        $this->specificite = $specificite;

        return $this;
    }

    public function getRegulPoid(): ?bool
    {
        return $this->regulPoid;
    }

    public function setRegulPoid(?bool $regulPoid): ModeleContrat
    {
        $this->regulPoid = $regulPoid;

        return $this;
    }

    public function getProduitsIdentiquePaysan(): ?bool
    {
        return $this->produitsIdentiquePaysan;
    }

    public function setProduitsIdentiquePaysan(?bool $produitsIdentiquePaysan): ModeleContrat
    {
        $this->produitsIdentiquePaysan = $produitsIdentiquePaysan;

        return $this;
    }

    public function getProduitsIdentiqueAmapien(): ?bool
    {
        return $this->produitsIdentiqueAmapien;
    }

    public function setProduitsIdentiqueAmapien(?bool $produitsIdentiqueAmapien): ModeleContrat
    {
        $this->produitsIdentiqueAmapien = $produitsIdentiqueAmapien;

        return $this;
    }

    public function getAmapienPermissionIntervenirPlanning(): ?bool
    {
        return $this->amapienPermissionIntervenirPlanning;
    }

    public function setAmapienPermissionIntervenirPlanning(?bool $amapienPermissionIntervenirPlanning): ModeleContrat
    {
        $this->amapienPermissionIntervenirPlanning = $amapienPermissionIntervenirPlanning;

        return $this;
    }

    public function getAmapienPermissionDeplacementLivraison(): ?bool
    {
        return $this->amapienPermissionDeplacementLivraison;
    }

    public function setAmapienPermissionDeplacementLivraison(?bool $amapienPermissionDeplacementLivraison): ModeleContrat
    {
        $this->amapienPermissionDeplacementLivraison = $amapienPermissionDeplacementLivraison;

        return $this;
    }

    public function getAmapienDeplacementMode(): ?string
    {
        return $this->amapienDeplacementMode;
    }

    public function setAmapienDeplacementMode(?string $amapienDeplacementMode): ModeleContrat
    {
        $this->amapienDeplacementMode = $amapienDeplacementMode;

        return $this;
    }

    public function getAmapienDeplacementNb(): ?int
    {
        return $this->amapienDeplacementNb;
    }

    public function setAmapienDeplacementNb(?int $amapienDeplacementNb): ModeleContrat
    {
        $this->amapienDeplacementNb = $amapienDeplacementNb;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getReglementType(): array
    {
        return $this->reglementType;
    }

    /**
     * @param string[] $reglementType
     */
    public function setReglementType(array $reglementType): ModeleContrat
    {
        $this->reglementType = $reglementType;

        return $this;
    }

    public function getReglementModalite(): ?string
    {
        return $this->reglementModalite;
    }

    public function setReglementModalite(?string $reglementModalite): ModeleContrat
    {
        $this->reglementModalite = $reglementModalite;

        return $this;
    }

    public function getReglementNbMax(): ?int
    {
        return $this->reglementNbMax;
    }

    public function setReglementNbMax(?int $reglementNbMax): ModeleContrat
    {
        $this->reglementNbMax = $reglementNbMax;

        return $this;
    }

    public function getContratAnnexes(): ?string
    {
        return $this->contratAnnexes;
    }

    public function setContratAnnexes(?string $contratAnnexes): ModeleContrat
    {
        $this->contratAnnexes = $contratAnnexes;

        return $this;
    }

    public function getAmapienPermissionReportLivraison(): ?bool
    {
        return $this->amapienPermissionReportLivraison;
    }

    public function setAmapienPermissionReportLivraison(?bool $amapienPermissionReportLivraison): ModeleContrat
    {
        $this->amapienPermissionReportLivraison = $amapienPermissionReportLivraison;

        return $this;
    }

    public function getAmapienReportNb(): ?int
    {
        return $this->amapienReportNb;
    }

    public function setAmapienReportNb(?int $amapienReportNb): ModeleContrat
    {
        $this->amapienReportNb = $amapienReportNb;

        return $this;
    }

    public function getNblivPlancherDepassement(): ?bool
    {
        return $this->nblivPlancherDepassement;
    }

    public function setNblivPlancherDepassement(?bool $nblivPlancherDepassement): ModeleContrat
    {
        $this->nblivPlancherDepassement = $nblivPlancherDepassement;

        return $this;
    }

    /**
     * Add contrat.
     *
     * @return ModeleContrat
     */
    public function addContrat(Contrat $contrat)
    {
        $this->contrats[] = $contrat;

        return $this;
    }

    /**
     * Remove contrat.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeContrat(Contrat $contrat)
    {
        return $this->contrats->removeElement($contrat);
    }

    /**
     * Get contrats.
     *
     * @return Collection
     */
    public function getContrats()
    {
        return $this->contrats;
    }

    /**
     * Add date.
     *
     * @return ModeleContrat
     */
    public function addDate(ModeleContratDate $date)
    {
        $this->dates[] = $date;
        $date->setModeleContrat($this);

        return $this;
    }

    /**
     * Remove date.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDate(ModeleContratDate $date)
    {
        $date->setModeleContrat(null);

        return $this->dates->removeElement($date);
    }

    /**
     * Get dates.
     *
     * @return Collection
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * Add dateReglement.
     *
     * @return ModeleContrat
     */
    public function addDateReglement(ModeleContratDatesReglement $dateReglement)
    {
        $this->dateReglements[] = $dateReglement;
        $dateReglement->setModeleContrat($this);

        return $this;
    }

    /**
     * Remove dateReglement.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDateReglement(ModeleContratDatesReglement $dateReglement)
    {
        $dateReglement->setModeleContrat(null);

        return $this->dateReglements->removeElement($dateReglement);
    }

    /**
     * Get dateReglements.
     *
     * @return Collection
     */
    public function getDateReglements()
    {
        return $this->dateReglements;
    }

    /**
     * Add produit.
     *
     * @return ModeleContrat
     */
    public function addProduit(ModeleContratProduit $produit)
    {
        $this->produits[] = $produit;
        $produit->setModeleContrat($this);

        return $this;
    }

    /**
     * Remove produit.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeProduit(ModeleContratProduit $produit)
    {
        $produit->setModeleContrat(null);

        return $this->produits->removeElement($produit);
    }

    /**
     * Get produits.
     *
     * @return Collection
     */
    public function getProduits()
    {
        return $this->produits;
    }
}
