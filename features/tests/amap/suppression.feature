#language: fr
Fonctionnalité: suppression d'une amap

  @database
  Plan du Scénario: suppression d'une amap en tant qu'administrateur
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/amap"
    Et que je remplis "search_amap[region]" avec "84"
    Et que je suis "Supprimer l'AMAP"
    Et je devrais voir "Voulez-vous vraiment supprimer l'AMAP \"AMAP TEST\" ?"
    Et je devrais voir "Attention ! Cette action supprimera tous les comptes et contrats liés à l'AMAP."
    Et que je suis "Confirmer la suppression"
    Et je devrais voir "L'AMAP a été supprimée avec succès"
    Et je ne devrais pas voir "amapien@test.amap-aura.org"

    Exemples:
      | mail                           |
      | admin@test.amap-aura.org       |
      | admin.aura@test.amap-aura.org  |
      | admin.rhone@test.amap-aura.org |
      
      
      


