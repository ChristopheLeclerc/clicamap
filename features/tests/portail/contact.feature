# language: fr
Fonctionnalité: Comme utilisateur, je devrais pouvoir envoyer un message de contact

  Scénario: Accès depuis l'accueil
    Etant donné que je suis sur "/"
    Quand je suis "contactez-nous"
    Alors je devrais être sur "/portail/contact"

  @database
  Scénario: Envoi d'une demande de contact
    Etant donné que je suis sur "/portail/contact"
    Quand remplis le texte suivant:
      | contact_nom     | Nom                    |
      | contact_prenom  | Prénom                 |
      | contact_email   | test@test.amap-aura.org |
      | contact_cp      | 00000                   |
      | contact_contenu | contenu                 |
      | contact_captcha | captchaValid            |
    Et je presse "Envoyer"
    Alors je devrais voir "Votre message est bien envoyé. Merci et bonne journée"
    Et un mail avec le sujet "Nouveau message du formulaire de contact Clic'AMAP" devrait avoir été envoyé à "clicamap@amap-aura.org"


