<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use LogicException;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Files\EvenementImg;
use PsrLib\ORM\Entity\Files\EvenementPj;
use PsrLib\ORM\Entity\Reseau;
use PsrLib\ORM\Entity\TypeProduction;

class EvenementBuilder
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param null|mixed $amapiens
     * @param null|mixed $reseau
     * @param null|mixed $ferme_id
     * @param null|mixed $dep
     * @param null|mixed $res
     * @param null|mixed $amap
     * @param null|mixed $ferme
     * @param null|mixed $tp
     * @param null|mixed $currentUser
     *
     * @throws ORMException
     */
    public function bindEvementRelations(
        Evenement $evenement,
        $amapiens,
        $reseau,
        $ferme_id,
        $dep,
        $res,
        $amap,
        $ferme,
        $tp,
        $currentUser
    ): void {
        // SESSIONS --------------------------------------------------------------
        // SI RÉGION
        $relAmap = null;
        $relAmapien = null;

        if ($currentUser instanceof Amapien && $currentUser->isSuperAdmin()) {
            $createur = 'sup_adm';
            $relAmapien = $currentUser;
        } elseif ($currentUser instanceof Amapien && $currentUser->isAdminRegion()) {
            $createur = 'adm_reg';
            $relAmapien = $currentUser;
        } elseif ($currentUser instanceof Amapien && $currentUser->isAdminDepartment()) {
            $createur = 'adm_dep';
            $relAmapien = $currentUser;
            $reseau = 'adm_reg'; // pour prévenir l'admin régional
        } elseif ($currentUser instanceof Amapien && $currentUser->isAdminReseaux()) {
            $createur = 'adm_res';
            $relAmapien = $currentUser;
        } // SI AMAP
        elseif ($currentUser instanceof Amap) {
            $createur = 'amap';
            $relAmap = $currentUser;

            if ($reseau) {
                if (!$currentUser->getDepartement()->getAdmins()->isEmpty()) {
                    $reseau = 'adm_dep';
                } else {
                    $reseau = 'adm_reg';
                }
            }
        } else {
            throw new LogicException('Unable to manage user');
        }
        // -----------------------------------------------------------------------
        $evenement
            ->setCreateur($createur)
            ->setInfoFerme(null === $ferme_id ? null : $this->em->getReference(Ferme::class, $ferme_id))
            ->setInfoAmapiens(null === $amapiens ? null : '1' === $amapiens)
            ->setInfoFermes(null === $ferme ? null : count($ferme) > 0)
            ->setInfoCreateurReseau($reseau)
            ->setAmap($relAmap)
            ->setAmapien($relAmapien)
        ;

        // On enregistre les AMAP sélectionnées
        if ($amap) {
            $evenement->getRelAmaps()->clear();
            foreach ($amap as $a) {
                $evenement->addRelAmap($this->em->getReference(Amap::class, $a));
            }
            $evenement->setInfoAmap(true);
        }

        // On enregistre les TP sélectionnés
        if ($tp) {
            $evenement->getRelTypeProduction()->clear();
            foreach ($tp as $p) {
                $evenement->addRelTypeProduction($this->em->getReference(TypeProduction::class, $p));
            }
            $evenement->setInfoTp(true);
        } // Les TP prennent le pas sur les fermes
        elseif ($ferme) {
            $evenement->getRelFerme()->clear();
            foreach ($ferme as $f) {
                $evenement->addRelFerme($this->em->getReference(Ferme::class, $f));
            }
            $evenement->setInfoFermes(true);
        }

        // Réseaux et départements ne se cumulent qu'avec un type de production
        if ($res) {
            $evenement->getRelReseau()->clear();
            foreach ($res as $r) {
                $evenement->addRelReseau($this->em->getReference(Reseau::class, $r));
            }
            $evenement->setInfoReseaux(true);
        }
        // Les Réseaux prennent le pas sur les départements
        if ($dep) {
            $evenement->getRelDepartements()->clear();
            foreach ($dep as $d) {
                $evenement->addRelDepartement($this->em->getReference(Departement::class, $d));
            }
            $evenement->setInfoDep(true);
        }
    }

    /**
     * @param null|mixed $nom
     * @param null|mixed $date_deb
     * @param null|mixed $date_fin
     * @param null|mixed $hor_deb
     * @param null|mixed $hor_fin
     * @param null|mixed $txt
     * @param null|mixed $url
     * @param null|mixed $amapiens
     * @param null|mixed $reseau
     * @param null|mixed $ferme_id
     * @param null|mixed $dep
     * @param null|mixed $res
     * @param null|mixed $amap
     * @param null|mixed $ferme
     * @param null|mixed $tp
     * @param null|mixed $currentUser
     *
     * @throws ORMException
     *
     * @return Evenement
     */
    public function buildEvenement(
        $nom,
        $date_deb,
        $date_fin,
        $hor_deb,
        $hor_fin,
        $txt,
        $url,
        ?EvenementImg $img,
        ?EvenementPj $pj,
        $amapiens,
        $reseau,
        $ferme_id,
        $dep,
        $res,
        $amap,
        $ferme,
        $tp,
        $currentUser
    ) {
        $evenement = new Evenement();
        $evenement
            ->setNom($nom)
            ->setHorDeb($hor_deb)
            ->setHorFin($hor_fin)
            ->setEvTxt($txt)
            ->setUrl($url)
            ->setImg($img)
            ->setPj($pj)
        ;

        if (null !== $date_deb) {
            $evenement
                ->setDateDeb(Carbon::createFromFormat('Y-m-d', $date_deb))
            ;
        }

        if (null !== $date_fin) {
            $evenement
                ->setDateFin(Carbon::createFromFormat('Y-m-d', $date_fin))
            ;
        }

        $this->bindEvementRelations($evenement, $amapiens, $reseau, $ferme_id, $dep, $res, $amap, $ferme, $tp, $currentUser);

        return $evenement;
    }
}
