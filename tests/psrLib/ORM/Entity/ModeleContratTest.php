<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Carbon\Carbon;
use Prophecy\Argument;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;
use Test\ValidationViolationTrait;

/**
 * @internal
 * @coversNothing
 */
class ModeleContratTest extends ContainerAwareTestCase
{
    use ValidationViolationTrait;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    /**
     * @dataProvider amapienPermissionReportLivraisonValidProvider
     */
    public function testAmapienPermissionReportLivraisonValid(bool $produitsIdentiquesAmapien, bool $produitIdentiquesPaysan, ?bool $permissionReport)
    {
        $mc = new ModeleContrat();
        $mc->setProduitsIdentiqueAmapien($produitsIdentiquesAmapien);
        $mc->setProduitsIdentiquePaysan($produitIdentiquesPaysan);
        $mc->setAmapienPermissionReportLivraison($permissionReport);

        $violations = $this->validator->validateProperty($mc, 'amapienPermissionReportLivraison', ['form4']);
        self::assertEmpty($violations);
    }

    public function amapienPermissionReportLivraisonValidProvider()
    {
        return [
            [false, false, null],
            [true, false, null],
            [false, true, null],
            [true, true, true],
            [true, true, false],
        ];
    }

    public function testAmapienPermissionReportLivraisonInvalid()
    {
        $mc = new ModeleContrat();
        $mc->setProduitsIdentiqueAmapien(true);
        $mc->setProduitsIdentiquePaysan(true);
        $mc->setAmapienPermissionReportLivraison(null);

        $violations = $this->validator->validateProperty($mc, 'amapienPermissionReportLivraison', ['form4']);
        self::assertNotEmpty($violations);
        self::assertSame('Le champ ci-dessous est requis à la saisie pour passer à l\'étape suivante', $violations[0]->getMessage());
    }

    public function testDatePositionNoDates()
    {
        $mc = new ModeleContrat();
        $date = $this->buildModeleContratDate(new \DateTime('2000-01-01'));

        self::assertNull($mc->getDatePosition($date));
    }

    public function testDatePositionDateOrdered()
    {
        $mc = new ModeleContrat();
        $date1 = $this->buildModeleContratDate(new \DateTime('2000-01-01'));
        $date2 = $this->buildModeleContratDate(new \DateTime('2000-01-02'));
        $mc->addDate($date1);
        $mc->addDate($date2);

        self::assertSame(1, $mc->getDatePosition($date1));
        self::assertSame(2, $mc->getDatePosition($date2));
    }

    public function testDatePositionDateReverseOrdered()
    {
        $mc = new ModeleContrat();
        $date1 = $this->buildModeleContratDate(new \DateTime('2000-01-02'));
        $date1->setId(1);
        $date2 = $this->buildModeleContratDate(new \DateTime('2000-01-01'));
        $date2->setId(2);
        $mc->addDate($date1);
        $mc->addDate($date2);

        self::assertSame(2, $mc->getDatePosition($date1));
        self::assertSame(1, $mc->getDatePosition($date2));
    }

    public function testDatePositionDateNotAssociated()
    {
        $mc = new ModeleContrat();
        $date1 = $this->buildModeleContratDate(new \DateTime('2000-01-02'));
        $date2 = $this->buildModeleContratDate(new \DateTime('2000-01-01'));
        $mc->addDate($date1);

        self::assertNull($mc->getDatePosition($date2));
    }

    public function testNbLivPlafondDepassement()
    {
        $mc = new ModeleContrat();
        $mc->setNblivPlancherDepassement(true);
        $mc->addDate(new ModeleContratDate());
        $mc->addDate(new ModeleContratDate());
        $mc->setNblivPlancher(3);

        self::assertSame(2, $mc->getNbLivPlafond());
    }

    public function testNbLivPlafondSansDepassement()
    {
        $mc = new ModeleContrat();
        $mc->setNblivPlancherDepassement(false);
        $mc->addDate(new ModeleContratDate());
        $mc->addDate(new ModeleContratDate());
        $mc->setNblivPlancher(3);

        self::assertSame(3, $mc->getNbLivPlafond());
    }

    /**
     * @dataProvider validationDateForclusionProvider
     *
     * @param Carbon[] $datesLivraisons
     */
    public function testValidationDateForclusion(?Carbon $dateForclusion, Carbon $premiereDateLivrable, $datesLivraisons, bool $hasViolation)
    {
        $mc = new ModeleContrat();
        $mc->setForclusion($dateForclusion);

        $fermeProphet = $this->prophesize(Ferme::class);
        $fermeProphet
            ->getPremiereDateLivrableAvecDelais(Argument::any())
            ->willReturn($premiereDateLivrable)
        ;
        $mc->setFerme($fermeProphet->reveal());

        foreach ($datesLivraisons as $datesLivraison) {
            $mc->addDate($this->buildModeleContratDate($datesLivraison));
        }

        $violations = $this->validator->validate($mc, null, ['form2']);
        if ($hasViolation) {
            self::assertViolationContainsMessage($violations, 'La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)');
        } else {
            self::assertViolationNotContainsMessage($violations, 'La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)');
        }
    }

    public function validationDateForclusionProvider()
    {
        return [
            [null, new Carbon('2000-01-01'), [], false],
            [new Carbon('2000-01-01'), new Carbon('2000-01-01'), [new Carbon('2000-01-01'), new Carbon('2000-01-02')], false],
            [new Carbon('2000-01-01'), new Carbon('2000-01-03'), [new Carbon('2000-01-01'), new Carbon('2000-01-02')], true],
        ];
    }

    private function buildModeleContratDate(\DateTime $dateTime)
    {
        $date = new ModeleContratDate();
        $date->setDateLivraison($dateTime);

        return $date;
    }
}
