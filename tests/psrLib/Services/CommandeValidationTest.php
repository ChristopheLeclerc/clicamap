<?php

namespace Test\Services;

use PHPUnit\Framework\TestCase;
use Prophecy\Prophet;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\Services\CommandeValidation;
use PsrLib\Services\OrderProcessor;

/**
 * @internal
 * @coversNothing
 */
class CommandeValidationTest extends TestCase
{
    /**
     * @var Prophet
     */
    private $orderProcessorProphet;

    /**
     * @var CommandeValidation
     */
    private $commandeValidation;

    public function setUp()
    {
        $this->orderProcessorProphet = $this->prophesize(OrderProcessor::class);
        $this->commandeValidation = new CommandeValidation($this->orderProcessorProphet->reveal());
    }

    /**
     * @dataProvider validationProvider
     */
    public function testValidation(
        bool $bypass,
        int $nbLivPlancher,
        bool $nbLivPlancherDepassement,
        int $nbLiv,
        bool $produitsIdentiquesAmapien,
        bool $produitsIdentiques,
        ?string $expectedRes
    ) {
        $mc = new ModeleContrat();
        $mc->setNblivPlancher($nbLivPlancher);
        $mc->setNblivPlancherDepassement($nbLivPlancherDepassement);
        $mc->setProduitsIdentiqueAmapien($produitsIdentiquesAmapien);

        $this->orderProcessorProphet->countNbLivraisons([])->willReturn($nbLiv);
        $this->orderProcessorProphet->areLivSame([])->willReturn($produitsIdentiques);

        $res = $this->commandeValidation->validate([], $mc, $bypass);
        self::assertSame($expectedRes, $res);
    }

    public function validationProvider()
    {
        return [
            [false, 3, false, 3, true, true, null],
            [false, 3, false, 4, true, true, 'Vous devez sélectionner <b>3</b> livraisons'],
            [false, 3, true, 3, true, true, null],
            [false, 3, true, 4, true, true, null],
            [false, 3, true, 2, true, true, 'Vous devez sélectionner <b>3</b> livraisons'],
            [false, 3, false, 3, true, false, 'Toutes les livraisons doivent être identiques'],
            [false, 3, false, 3, false, true, null],
            [false, 3, false, 3, false, false, null],
            [true, 3, false, 2, true, true, null],
            [true, 3, false, 4, true, true, null],
            [true, 3, true, 2, true, true, null],
            [true, 3, true, 3, true, false, null],
        ];
    }
}
