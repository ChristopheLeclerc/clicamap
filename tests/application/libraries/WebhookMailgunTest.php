<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once 'application/libraries/WebhookMailgun.php';

use Carbon\CarbonImmutable;

/**
 * @internal
 * @coversNothing
 */
class WebhookMailgunTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WebhookMailgun
     */
    private $webhook;

    /**
     * @var string
     */
    private $token = '6cf66bd9eb42df2e15377750224df934';

    public function setUp()
    {
        $this->webhook = new WebhookMailgun($this->token);
        CarbonImmutable::setTestNow(
            CarbonImmutable::create(2020, 07, 29, 9, 20, 00, 'UTC')
        );
    }

    public function testCheckSignatureValid()
    {
        $res = $this->webhook->checkSignature([
            'timestamp' => '1596014399',
            'token' => '9a7cfee6e9fe42248bbdc6a4e7f6db9816a2961560017f0d46',
            'signature' => '32a3097a0cd2466c1ab78e06833ea4397e19f05c57795f5e75bb05766d6b775d',
        ]);

        self::assertTrue($res);
    }

    public function testCheckSignatureInvalidTimestamp()
    {
        CarbonImmutable::setTestNow(
            CarbonImmutable::create(2020, 07, 29, 9, 30, 00, 'UTC')
        );
        $res = $this->webhook->checkSignature([
            'timestamp' => '1596014399',
            'token' => '9a7cfee6e9fe42248bbdc6a4e7f6db9816a2961560017f0d46',
            'signature' => '32a3097a0cd2466c1ab78e06833ea4397e19f05c57795f5e75bb05766d6b775d',
        ]);

        self::assertFalse($res);
    }

    public function testCheckSignatureInvalidToken()
    {
        $res = $this->webhook->checkSignature([
            'timestamp' => '1596014399',
            'token' => 'invalid',
            'signature' => '32a3097a0cd2466c1ab78e06833ea4397e19f05c57795f5e75bb05766d6b775d',
        ]);

        self::assertFalse($res);
    }

    public function testCheckSignatureInvalidSignature()
    {
        $res = $this->webhook->checkSignature([
            'timestamp' => '1596014399',
            'token' => '9a7cfee6e9fe42248bbdc6a4e7f6db9816a2961560017f0d46',
            'signature' => 'invalid',
        ]);

        self::assertFalse($res);
    }

    public function testInvalidJson()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid Json');
        $this->webhook->parseRequest('invalidJson');
    }

    public function testInvalidSignature()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid signature');
        $this->webhook->parseRequest(
            '{
          "signature": {
            "timestamp": "1596014399",
            "token": "invalid",
            "signature": "invalid"
          }
          }'
        );
    }

    public function testInvalidRequest()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid request');
        $this->webhook->parseRequest(
            '{
          "signature": {
            "timestamp": "1596014399",
            "token": "9a7cfee6e9fe42248bbdc6a4e7f6db9816a2961560017f0d46",
            "signature": "32a3097a0cd2466c1ab78e06833ea4397e19f05c57795f5e75bb05766d6b775d"
          },
          "event-data" : {}
          }'
        );
    }

    public function testValid()
    {
        $res = $this->webhook->parseRequest(file_get_contents(__DIR__.'/../../data/mailgun_request.json'));
        self::assertSame('alice@example.com', $res);
    }
}
