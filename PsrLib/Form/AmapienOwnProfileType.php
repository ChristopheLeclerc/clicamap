<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\VilleType;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\Amapien;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class AmapienOwnProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => false,
                'disabled' => true,
                'attr' => ['readonly' => true],
            ])
            ->add('prenom', TextType::class, [
                'required' => false,
                'disabled' => true,
                'attr' => ['readonly' => true],
            ])
            ->add('email', EmailType::class, [
                'required' => false,
            ])
            ->add('numTel1', TextType::class, [
                'required' => false,
            ])
            ->add('numTel2', TextType::class, [
                'required' => false,
            ])
            ->add('emailInvalid', YesNoChoiceType::class)
            ->add('newsletter', YesNoChoiceType::class, [
                'required' => false,
                'constraints' => [new NotNull()],
            ])
            ->add('libAdr1', TextType::class, [
                'required' => false,
            ])
            ->add('ville', VilleType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Amapien::class,
        ]);
    }
}
