<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php if (!empty($messages)) { ?>

    <div class="modal fade" id="flash-alert">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">
                <?php foreach ($messages as $type => $messages) {
    foreach ($messages as $message) {
        if ('notice_error' === $type) {
            echo '<p><i class="glyphicon glyphicon-remove" style="color:#C9302C;"></i> '.$message.'</p>';
        } else {
            echo '<p><i class="glyphicon glyphicon-ok" style="color:#449d44;"></i>'.$message.'</p>';
        }
    }
} ?>
                </div>
            </div>
        </div>
    </div>

<script>
  $('#flash-alert').modal('show');

  setTimeout(function () {
    $('#flash-alert').modal('hide')
  }, 2500);
</script>

<?php } ?>
