<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AkPaysanCommentaire.
 *
 * @ORM\Table(name="ak_paysan_commentaire")
 * @ORM\Entity
 */
class PaysanCommentaire
{
    /**
     * @var int | null
     *
     * @ORM\Column(name="pay_com_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var null|string
     *
     * @ORM\Column(name="pay_com_com", type="text", length=65535, nullable=true)
     * @Assert\Length(max=65535)
     */
    private $commentaire;

    /**
     * @var Paysan
     *
     * @ORM\ManyToOne(targetEntity="\PsrLib\ORM\Entity\Paysan", inversedBy="commentaires")
     * @ORM\JoinColumn(name="pay_com_fk_pay_id", referencedColumnName="pay_id", nullable=false)
     */
    private $paysan;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="pay_com_date", type="date", nullable=true)
     */
    private $payComDate;

    /**
     * PaysanCommentaire constructor.
     */
    public function __construct(Paysan $paysan)
    {
        $this->paysan = $paysan;
        $this->payComDate = Carbon::now();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): PaysanCommentaire
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getPaysan(): Paysan
    {
        return $this->paysan;
    }

    public function setPaysan(Paysan $paysan): PaysanCommentaire
    {
        $this->paysan = $paysan;

        return $this;
    }

    public function getPayComDate(): ?DateTime
    {
        return $this->payComDate;
    }

    public function setPayComDate(?DateTime $payComDate): PaysanCommentaire
    {
        $this->payComDate = $payComDate;

        return $this;
    }
}
