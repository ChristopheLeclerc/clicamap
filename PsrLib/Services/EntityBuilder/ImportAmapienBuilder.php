<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapienAnneeAdhesion;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\VilleRepository;

class ImportAmapienBuilder
{
    /**
     * @var VilleRepository
     */
    private $villeRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->villeRepo = $em->getRepository(Ville::class);
    }

    /**
     * @return Amapien[]
     */
    public function buildAmapiensFromImport(array $entete, array $valeurs, Amap $amap)
    {
        $amapiens = [];

        // A->prénom ; B->nom ; C->mail ; D->tel ; E->adresse ; F->code postal
        // G->ville ; H->adhérents années ; I->abonné newsletter
        foreach ($valeurs as $valeur) {
            $amapien = new Amapien();
            $amapien->setEtat(Amapien::ETAT_INACTIF);
            $amapien->setNewsletter(true);
            $amapien->setAmap($amap);

            $ville = $this->villeRepo->findOneBy([
                'cp' => trim($valeur['F']),
                'nom' => trim($valeur['G']),
            ]);
            $amapien->setVille($ville);

            foreach ($valeur as $k => $v) {
                if ($v) {
                    $v = trim($v); // trim() supprime les espaces
                    // Prénom
                    if ('A' == $k) {
                        $amapien->setPrenom(ucfirst($v));
                    }
                    // Nom
                    if ('B' == $k) {
                        $amapien->setNom(mb_strtoupper($v));
                    }
                    // Email
                    if ('C' == $k) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("mailto:', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0]);
                        }

                        $amapien->setEmail($v);
                    }
                    // Téléphone
                    if ('D' == $k) {
                        $v_array = explode('|', $v);
                        // Si il y a plusieurs numéro
                        if (count($v_array) > 1) {
                            $amapien->setNumTel1($v_array[0]);
                            $amapien->setNumTel2($v_array[1]);
                        } else {
                            $amapien->setNumTel1($v);
                        }
                    }
                    // Adresse
                    if ('E' == $k) {
                        $amapien->setLibAdr1($v);
                    }
                    // Années d'adhésion
                    if ('H' == $k) {
                        $annee = explode('|', $v);
                        foreach ($annee as $a) {
                            $annne = new AmapienAnneeAdhesion((int) trim($a), $amapien);
                            $amapien->addAnneeAdhesion($annne);
                        }
                    }
                    // Abonné newsletter
                    if ('I' == $k) {
                        $amapien->setNewsletter('non' !== $v);
                    }
                }
            }

            $amapiens[] = $amapien;
        }

        return $amapiens;
    }
}
