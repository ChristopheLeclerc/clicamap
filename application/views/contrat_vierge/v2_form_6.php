<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
/** @var \Symfony\Component\Form\FormView $form */
?>

<h3><?= $contrat->getNom(); ?> : l'échéancier des paiements</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Création
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_2'); ?>">Étape 2</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_3'); ?>">Étape 3</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_4'); ?>">Étape 4</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_5'); ?>">Étape 5</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 6
</h5>

<script>
  $(document).ready(function () {
    var lineId = 1;

    $('#btn-plus').click(function (e) {
      e.preventDefault();

      newLine();
    });

    $('#form-reglements').on('click', '.btn-remove', function (e) {
      e.preventDefault();

      $(this)
        .closest('.date-row')
        .remove();

    });

      <?php
      $datesInput = $contrat
          ->getDateReglements()
          ->map(function (PsrLib\ORM\Entity\ModeleContratDatesReglement $modeleContratDatesReglement) {
              return $modeleContratDatesReglement->getDate()->format('Y-m-d');
          })
          ->toArray()
;
      ?>
    var datesExistantes = JSON.parse('<?= json_encode($datesInput); ?>');
    datesExistantes.forEach(function (date) {
      newLine(date);
    });


    function newLine(date = null) {
      var input = $("<div class='date-row row'><div class='col-xs-3'><input type='text' data-date-format='yyyy-mm-dd' class='form-control datepicker' required /></div><div class='col-xs-2'><button type='button' title='Supprimer cette date de règlement' class='btn btn-danger btn-remove'><i class='glyphicon glyphicon-minus'></i></button></div></div>");
      input
        .find('input')
        .prop('name', 'model_contrat_form6[dateReglements][' + lineId + '][date]');

      if (date !== null) {
        input
          .find('input')
          .val(date);
      }

      $('#dates-list')
        .append(input)
      ;

      lineId = lineId + 1;
      input.find('.datepicker').datepicker();
    }
  });
</script>

<form method="POST" id="form-reglements" action="">

    <div class="well well-sm">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Nombre de règlements maximum : <span class="text-danger">*</span></label>
                    <?= render_form_number_widget($form['reglementNbMax'], '0', '1'); ?>
                </div>
            </div>
            <div class="col-xs-12">
                <label>Dates des règlements à effectuer : <span class="text-danger">*</span></label>
            </div>
            <div id="dates-list" class="col-xs-12">

            </div>
            <div class="col-xs-12">
                <button
                        type="button"
                        id="btn-plus"
                        title="Ajouter une date de règlement"
                        class="btn btn-success">
                    <i class="glyphicon glyphicon-plus"></i>
                </button>
            </div>

            <div class="col-xs-12">
                <div class="form-group" style="margin-top: 30px;">
                    <label>Mode de règlement : <span class="text-danger">*</span></label>
                    <?= render_form_multiselect_expanded_widget($form['reglementType']); ?>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label>Modalités de règlement : <span class="text-danger">*</span></label>
                    <?= render_form_textarea_widget($form['reglementModalite']); ?>
                </div>
            </div>

            <div class="col-xs-12">
                <input type="submit" value="Étape suivante" class="pull-right btn btn-success"
                       style="margin-left:5px;"/>
                <?php if ($contrat->getProduitsIdentiquePaysan()) { ?>
                    <a href="<?= site_url('/contrat_vierge/v2_contrat_form_4'); ?>"
                       class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
                <?php } else { ?>
                    <a href="<?= site_url('/contrat_vierge/v2_contrat_form_5'); ?>"
                       class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
                <?php } ?>
                <a href="<?= site_url('contrat_vierge'); ?>" class="pull-right btn btn-default">Annuler</a>
            </div>

        </div>


    </div>
</form>


