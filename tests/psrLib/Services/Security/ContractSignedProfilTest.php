<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class ContractSignedProfilTest extends AbstractSecurityTest
{
    public function testAmapien()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_PROFIL));
    }

    public function testAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_PROFIL));
    }

    public function testPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_PROFIL));
    }

    public function testFermeRegroupement()
    {
        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_REGROUPEMENT_PROFIL));
    }
}
