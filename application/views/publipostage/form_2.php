<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<h3>Email</h3>

<?php

$mot = 'Création';

echo '<h5><a href="'.site_url('publipostage').'">Publipostage</a> <i class="glyphicon glyphicon-menu-right"></i> Email</h5>';

echo form_open_multipart('publipostage/email_form_2', 'id="form2"');
// VARIABLES CACHÉES À LA CRÉATION
echo '<input type="hidden"  name="etape_email" value="1" />';
// -----------------------------------------------------------------------------
if (set_value('reg')) {
    foreach (set_value('reg') as $reg) {
        echo '<input type="hidden"  name="reg[]" value="'.$reg.'" />';
    }
}

if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}

if (set_value('media')) {
    echo '<input type="hidden"  name="media" value="'.set_value('media').'" />';
}
if (set_value('qui')) {
    echo '<input type="hidden"  name="qui" value="'.set_value('qui').'" />';
}
if (set_value('en_recherche_partenariat')) {
    echo '<input type="hidden"  name="en_recherche_partenariat" value="'.set_value('en_recherche_partenariat').'" />';
}
if (set_value('adh_non')) {
    echo '<input type="hidden"  name="adh_non" value="'.set_value('adh_non').'" />';
}
if (set_value('adh_oui')) {
    echo '<input type="hidden"  name="adh_oui" value="'.set_value('adh_oui').'" />';
}
if (set_value('certification')) {
    echo '<input type="hidden"  name="certification" value="'.set_value('certification').'" />';
}
// -----------------------------------------------------------------------------
?>

<div class="well well-sm">
    <div class="row">
        <div class="form-group col-md-12">
            <label for="titre" class="col-md-12 control-label">Titre : </label>
            <div class="col-md-12">
                <input class="form-control" type="text" name="titre" value="<?php if (set_value('titre')) {
    echo set_value('titre');
} ?>"/>
                <?= form_error('titre'); ?>
            </div>
        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="form-group col-md-12">
            <label for="txt" class="col-md-12 control-label">Description : </label>
            <div class="col-md-12">
                <textarea rows="4" cols="50" class="form-control" name="txt"/><?php if (set_value('txt')) {
    echo set_value('txt');
} ?></textarea>
                <?= form_error('txt'); ?>
            </div>
        </div>
    </div>
</div>

<div class="well well-sm">
    <div class="row">
        <div class="form-group col-md-12">
            <label for="url" class="col-md-12 control-label">Page web : </label>
            <div class="input-group">
                <div class="input-group-addon">www.</div>
                <input class="form-control" type="text" name="url" placeholder="exemple.fr"
                       value="<?php if (set_value('url')) {
    echo set_value('url');
} ?>"/>
                <?= form_error('url'); ?>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <?php
    $mot = 'Étape suivante';

    echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;"/>';
    ?>
</div>

</form>

<!-- ----------------------------------------------------------------------- -->
<!-- ÉTAPE PRÉCÉDENTE -->
<?php echo form_open_multipart('publipostage/form_1', 'id="form1"');
// VARIABLES CACHÉES
if (set_value('reg')) {
    foreach (set_value('reg') as $reg) {
        echo '<input type="hidden"  name="reg[]" value="'.$reg.'" />';
    }
}

if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}
// -----------------------------------------------------------------------------
if (set_value('media')) {
    echo '<input type="hidden"  name="media" value="'.set_value('media').'" />';
}
if (set_value('qui')) {
    echo '<input type="hidden"  name="qui" value="'.set_value('qui').'" />';
}
if (set_value('en_recherche_partenariat')) {
    echo '<input type="hidden"  name="en_recherche_partenariat" value="'.set_value('en_recherche_partenariat').'" />';
}
if (set_value('adh_non')) {
    echo '<input type="hidden"  name="adh_non" value="'.set_value('adh_non').'" />';
}
if (set_value('adh_oui')) {
    echo '<input type="hidden"  name="adh_oui" value="'.set_value('adh_oui').'" />';
}
if (set_value('certification')) {
    echo '<input type="hidden"  name="certification" value="'.set_value('certification').'" />';
}
if (set_value('titre')) {
    echo '<input type="hidden"  name="titre" value="'.set_value('titre').'" />';
}
if (set_value('txt')) {
    echo '<input type="hidden"  name="txt" value="'.set_value('txt').'" />';
}
if (set_value('url')) {
    echo '<input type="hidden"  name="url" value="'.set_value('url').'" />';
}

echo '<div class="form-group">';
echo '<input type="submit" value="Étape précédente" class="pull-right btn btn-default" style="margin-left:5px;"/>';
// echo '<a href="'.site_url('publipostage').'" class="pull-right btn btn-default">Annuler</a>';
echo '</div>';

echo '</form>';
