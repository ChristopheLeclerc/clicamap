<?php

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * Compatibility table for codeigniter captcha.
 *
 * @ORM\Entity()
 * @ORM\Table(name="captcha")
 */
class Captcha
{
    /**
     * @ORM\Id
     * @ORM\Column(name="captcha_id", type="bigint", length=13, options={"unsigned"=true})
     * @GeneratedValue
     *
     * @var int|string
     */
    private $id;

    /**
     * @var ?int
     * @ORM\Column(name="captcha_time", type="integer", options={"unsigned"=true}, nullable=true)
     */
    private $time;

    /**
     * @var string
     * @ORM\Column(name="ip_address", type="string", length=45, nullable=false)
     */
    private $ipAddress;

    /**
     * @var string
     * @ORM\Column(name="word", type="string", length=20, nullable=false)
     */
    private $word;

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function getWord(): string
    {
        return $this->word;
    }
}
