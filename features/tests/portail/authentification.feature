# language: fr
Fonctionnalité: Comme utilisateur, je devrais pouvoir m'authentifier

  Plan du Scénario: Authentification
    Etant donné que je suis sur "/"
    Quand je sélectionne "amapien" depuis "statut"
    Et remplis le texte suivant:
      | email    | <email>   |
      | password | P@ssw0rd! |
      | statut   | <statut>  |
    Et je presse "Connexion"
    Alors je devrais voir "<message>"

    Exemples:
      | statut       | email                           | message                                                                                                                                                         |
      | amapien      | admin@test.amap-aura.org        | Bonjour Admin_prenom ADMIN_NOM, bienvenue sur Clic'AMAP ! Vous êtes actuellement connecté en tant que Super Administrateur. Vous avez tous les droits.          |
      | amapien      | amap@test.amap-aura.org         | Bonjour, bienvenue sur Clic'AMAP ! Vous êtes actuellement connecté en tant qu'AMAP : amap test.                                                                 |
      | amapien      | amapienref@test.amap-aura.org   | Bonjour Prénom amapien référent NOM AMAPIEN RÉFÉRENT, bienvenue sur Clic'AMAP ! Vous êtes actuellement connecté en tant que Référent produit auprès de : ferme. |
      | amapien      | amapien@test.amap-aura.org      | Bonjour Prenom amapien NOM AMAPIEN, bienvenue sur Clic'AMAP ! Vous êtes actuellement connecté en tant qu'amapien de : amap test.                                |
      | paysan       | paysan@test.amap-aura.org       | Bonjour Paysan prénom PAYSAN NOM, bienvenue sur Clic'AMAP ! Vous êtes actuellement connecté en tant que paysan de la ferme ferme.                               |
      | regroupement | regroupement@test.amap-aura.org | Bonjour, bienvenue sur Clic'AMAP ! Vous êtes actuellement connecté en tant que regroupement |
