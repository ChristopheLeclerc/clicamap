<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;
use DI\Annotation\Inject;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Ferme.
 */
class Ferme extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\Exporters\ExcelGeneratorExportFerme
     */
    public $excelgeneratorexportferme;

    protected $data;

    /**
     * Ferme constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['annee_adhesion' => [],
            'map' => null,
            'gps' => null,
            'create' => null,
            'edit' => null,
            'remove' => null,
            'key' => null,
            'key_all' => null,
            'paysan_all' => [],
            'ajout' => null,
            'vide' => null,
            'alerte' => null,
            'alerte_remove_impossible_actif' => null,
            'alerte_remove_impossible_creation' => null,
            'ferme' => null,
            'ferme_all' => [],
            'f_id' => null,
            'amap_id' => null,
            'alertes_2' => null,
            'alertes' => null,
            'reseau_all' => [],
            'livraisons' => [],
            'ferme_referents_produits' => false,
            'referents' => null, ];

        $this->load->helper('array');
        $this->load->helper('email');
        $this->load->helper('string');

        $this->load->library('email');
        $this->load->library('leaflet');
        $this->load->library('form_validation');
        //  Cette méthode permet de changer les délimiteurs par défaut des messages d'erreur (<p></p>).
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    /**
     * Index.
     */
    public function index()
    {
        $currentUser = $this->getUser();
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
            $this->home_refprod($currentUser);

            return;
        }

        if ($currentUser instanceof \PsrLib\ORM\Entity\FermeRegroupement) {
            $this->home_regroupement($currentUser);

            return;
        }
        $this->accueil();
    }

    /**
     * Accueil / Moteur de recherche.
     */
    public function accueil()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_LIST);

        $this->redirectFromStoredGetParamIfExist('ferme');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchFermeType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $gps = 0;
        $map = null;
        $fermes = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->search($searchForm->getData())
        ;

        if ($searchForm->get('carto')->getData()) {
            [$gps, $map] = $this->cartographie($fermes);
        }

        $this->loadViewWithTemplate('ferme/display_all', [
            'fermes' => $fermes,
            'searchForm' => $searchForm->createView(),
            'gps' => $gps,
            'map' => $map,
        ]);
    }

    public function home_paysan()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY_OWN);

        /** @var \PsrLib\ORM\Entity\Paysan $paysan Type granted by permission */
        $paysan = $this->getUser();

        $this->loadViewWithTemplate('ferme/home_paysan', [
            'paysan' => $paysan,
        ]);
    }

    /**
     *  Ajout / Édition des informations générales.
     *
     * @param null $ferme_id
     */
    public function informations_generales($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $currentUser = $this->getUser();
        $form = $this->formFactory->create(\PsrLib\Form\FermeType::class, $ferme, [
            'permission_edit_adhesions' => $currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin(),
            'permission_edit_regroupement' => $currentUser instanceof \PsrLib\ORM\Entity\Amapien
                && ($currentUser->isSuperAdmin() || $currentUser->isAdminRegion()),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'La ferme a été modifiée avec succès !'
            );

            if ($this->getUser() instanceof \PsrLib\ORM\Entity\Paysan) {
                redirect('ferme/home_paysan');
            }
            redirect('/ferme');
        }

        $this->loadViewWithTemplate('ferme/informations_generales', [
            'form' => $form->createView(),
            'ferme' => $ferme,
        ]);
    }

    public function informations_generales_creation()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_CREATE);

        $ferme = new \PsrLib\ORM\Entity\Ferme();

        if ($this->getUser() instanceof \PsrLib\ORM\Entity\FermeRegroupement) {
            $ferme->setRegroupement($this->getUser());
        }

        $currentUser = $this->getUser();
        $form = $this->formFactory->create(\PsrLib\Form\FermeType::class, $ferme, [
            'permission_edit_adhesions' => $currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin(),
            'permission_edit_regroupement' => $currentUser instanceof \PsrLib\ORM\Entity\Amapien
                && ($currentUser->isSuperAdmin() || $currentUser->isAdminRegion()),
        ]);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($ferme);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Vous venez de créer une ferme. Vous pouvez d\'ores et déjà lui ajouter un ou plusieurs paysans.'
            );

            redirect('/paysan/informations_generales_creation');
        }

        $this->loadViewWithTemplate('ferme/informations_generales', [
            'form' => $form->createView(),
            'ferme' => $ferme,
        ]);
    }

    /**
     * Afficher les détails de la ferme.
     *
     * @param $ferme_id
     */
    public function display($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $this->loadViewWithTemplate('ferme/display', [
            'ferme' => $ferme,
        ]);
    }

    /**
     * Calendrier de livraisons.
     *
     * @param $ferme_id
     * @param null $an
     * @param null $mois
     */
    public function calendrier($ferme_id, $an = null, $mois = null)
    {
        /** @var \PsrLib\ORM\Repository\ModeleContratRepository $modeleContratRepo */
        $modeleContratRepo = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
        ;

        /** @var \PsrLib\ORM\Repository\ContratCelluleRepository $contratCelluleRepo */
        $contratCelluleRepo = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ContratCellule::class)
        ;

        // PRÉFÉRENCES DU CALENDRIER
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->data['ferme'] = $ferme;

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $this->data['ferme']);

        $prefs = ['start_day' => 'monday',
            'day_type' => 'long',
            'show_next_prev' => true,
            'next_prev_url' => site_url('ferme/calendrier/'.$ferme_id), ]; // Le zéro est pour $date_id

        $prefs['template'] = '
      {table_open}<table class="table"  border="0" cellpadding="0" cellspacing="0">{/table_open}

      {heading_row_start}<tr>{/heading_row_start}

      {heading_previous_cell}<th><a href="{previous_url}"><i class="glyphicon glyphicon-menu-left"></i></a></th>{/heading_previous_cell}
      {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
      {heading_next_cell}<th><a href="{next_url}"><i class="glyphicon glyphicon-menu-right"></i></a></th>{/heading_next_cell}

      {heading_row_end}</tr>{/heading_row_end}

      {week_row_start}<tr>{/week_row_start}
      {week_day_cell}<td>{week_day}</td>{/week_day_cell}
      {week_row_end}</tr>{/week_row_end}

      {cal_row_start}<tr>{/cal_row_start}
      {cal_cell_start}<td>{/cal_cell_start}
      {cal_cell_start_today}<td>{/cal_cell_start_today}
      {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

      {cal_cell_content}<a href="{content}" data-toggle="modal" data-target="#modal_{day}"><button type=button class="btn btn-primary btn-xs">{day}</button></a>{/cal_cell_content}

      {cal_cell_no_content}{day}{/cal_cell_no_content}

      {cal_cell_blank}&nbsp;{/cal_cell_blank}

      {cal_cell_other}{day}{/cal_cel_other}

      {cal_cell_end}</td>{/cal_cell_end}
      {cal_cell_end_today}</td>{/cal_cell_end_today}
      {cal_cell_end_other}</td>{/cal_cell_end_other}
      {cal_row_end}</tr>{/cal_row_end}

      {table_close}</table>{/table_close}
      ';

        $this->load->library('calendar', $prefs);

        if (!$an) {
            $this->data['an'] = date('Y');
        } else {
            $this->data['an'] = $an;
        }
        if (!$mois) {
            $this->data['mois'] = date('m');
        } else {
            $this->data['mois'] = $mois;
        }

        $this->data['liens'] = null;

        // TOUTES LES DATES DE CONTRATS VIERGES (MC) LIÉS À UNE FERME
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $date_all */
        $date_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratDate::class)
            ->getByFerme($ferme)
        ;
        if (count($date_all) > 0) {
            // On dédoublonne les dates ------------------------------------------
            $ferme_date_all = [];
            foreach ($date_all as $date) {
                $ferme_date_all[] = $date->getDateLivraison()->format('Y-m-d');
            }
            $ferme_date_all = array_unique($ferme_date_all);

            // Liens du calendrier et infos des Modals ---------------------------
            $this->data['liens'] = [];

            foreach ($ferme_date_all as $f_d) {
                // Année
                $f_d_an = substr($f_d, 0, 4);
                // Mois
                $f_d_mois = substr($f_d, 5, 2);

                // L'année et le mois du contrat correspondent avec l'année et le mois courant du calendrier
                if ($this->data['an'] == $f_d_an && $this->data['mois'] == $f_d_mois) {
                    $jour = substr($f_d, -2);
                    // On enlève le premier Zéro du jour s'il y en a un --------------
                    if (0 == substr($jour, 0, 1)) {
                        $jour = substr($jour, 1, 2);
                    }

                    // On place un lien sur le jour du calendrier --------------------
                    $this->data['liens'][$jour] = '#';
                    $this->data['livraisons'][$jour] = [];

                    $mcs = $modeleContratRepo->getFromFermeDate($ferme, $f_d);
                    $this->data['livraisons'][$jour]['mc'] = $mcs;
                    $amaps = [];
                    foreach ($mcs as $mc) {
                        $amaps[] = $mc->getLivraisonLieu()->getAmap();
                    }
                    $this->data['livraisons'][$jour]['amap'] = array_unique($amaps);

                    $cellules = $contratCelluleRepo->getFromFermeDate($ferme, $f_d);
                    $this->data['livraisons'][$jour]['cellules'] = $cellules;

                    $amapiens = [];
                    foreach ($cellules as $cellule) {
                        $amapiens[] = $cellule->getContrat()->getAmapien();
                    }
                    $this->data['livraisons'][$jour]['amapiens'] = array_unique($amapiens);
                }
            }
        }

        $this->data['calendrier'] = $this->calendar->generate($this->data['an'], $this->data['mois'], $this->data['liens']);
        /** @var \PsrLib\ORM\Entity\BaseUser $currentUser */
        $currentUser = $this->getUser();
        if ($currentUser instanceof \PsrLib\ORM\Entity\FermeRegroupement) {
            $this->data['fermes_accessibles'] = $currentUser->getFermes()->toArray();
        }

        $this->loadViewWithTemplate('ferme/calendrier', $this->data);
    }

    /**
     * Supprimer la ferme.
     *
     * @param $ferme_id
     */
    public function remove_ferme($ferme_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REMOVE);
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->em->remove($ferme);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'La ferme a été supprimée avec succès'
        );

        redirect('/ferme');
    }

    /**
     * Afficher les paysans de la ferme.
     *
     * @param $ferme_id
     */
    public function paysan_display($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $form = $this->formFactory->create(\PsrLib\Form\FermePaysanType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\Paysan $paysan */
            $paysan = $form->get('paysan')->getData();
            $paysan->setFerme($ferme);

            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le paysan a été ajouté avec succès'
            );

            redirect('/ferme/paysan_display/'.$ferme->getId());
        }

        $this->loadViewWithTemplate('ferme/paysan_display', [
            'ferme' => $ferme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Supprimer un paysan de la ferme.
     *
     * @param $pay_id
     */
    public function remove_paysan($pay_id)
    {
        /** @var \PsrLib\ORM\Entity\Paysan $ferme */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $pay_id);
        $ferme = $paysan->getFerme();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $paysan->setFerme(null);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le paysan a été supprimé avec succès'
        );

        redirect('/ferme/paysan_display/'.$ferme->getId());
    }

    /**
     * @param $ferme_id
     */
    public function referent_display($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        /** @var \PsrLib\ORM\Entity\BaseUser $currentUser */
        $currentUser = $this->getUser();
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
            $amapAccessibles = [$currentUser];
        } else {
            $amapAccessibles = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Amap::class)
                ->qbAmapAccessibleForAmapien($currentUser)
                ->getQuery()
                ->getResult()
            ;
        }

        $referents = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->getRefProduitFromMultipleAmapsFerme($amapAccessibles, $ferme)
        ;

        $form = $this->formFactory->create(\PsrLib\Form\FermeRefProduitType::class, null, [
            'amaps' => $amapAccessibles,
            'ferme' => $ferme,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $amapien = $form->get('amapien')->getData();
            $referents = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                ->getRefProduitFromMultipleAmapsFerme([$form->get('amap')->getData()], $ferme)
            ;
            if (null !== $amapien) {
                $ferme->addAmapienRef($amapien);
                $this->em->flush();

                $this->addFlash(
                    'notice_success',
                    'Le référent a été ajouté avec succès'
                );

                redirect('/ferme/referent_display/'.$ferme->getId());
            }
        }

        $this->loadViewWithTemplate('ferme/referent_display', [
            'ferme' => $ferme,
            'referents' => $referents,
            'form' => $form->createView(),
            'amapAccessibles' => $amapAccessibles,
        ]);
    }

    /**
     * @param $f_ref_id
     * @param mixed $f_id
     */
    public function remove_referent_produit($f_id, $f_ref_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $refProd */
        $refProd = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $f_ref_id);
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $f_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DISPLAY, $ferme);

        $ferme->removeAmapienRef($refProd);

        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le référent a été supprimé avec succès'
        );

        redirect('ferme/referent_display/'.$ferme->getId());
    }

    /**
     * Ajouter / Éditer les produits proposés.
     *
     * @param $paysan_id
     * @param mixed $ferme_id
     */
    public function type_production_creation($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_TYPE_PRODUCTION_MANAGE, $ferme);

        $tpp = new \PsrLib\ORM\Entity\FermeTypeProductionPropose($ferme);
        $form = $this->formFactory->create(\PsrLib\Form\FermeTypeProductionProposeType::class, $tpp);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $certificate granted by form */
            $certificate = $form->get('certification')->getData();
            $file = $this
                ->uploader
                ->uploadFile($certificate, \PsrLib\ORM\Entity\Files\FermeTypeProductionAttestationCertification::class)
            ;
            $tpp->setAttestationCertification($file);

            $this->em->persist($tpp);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le type de production a été ajouté avec succès'
            );

            redirect('ferme/type_production_display/'.$ferme->getId());
        }

        $this->loadViewWithTemplate('ferme/type_production_creation', [
            'ferme' => $ferme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Afficher les produits proposés par le paysan.
     *
     * @param mixed $ferme_id
     */
    public function type_production_display($ferme_id)
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_TYPE_PRODUCTION_MANAGE, $ferme);

        $this->loadViewWithTemplate('ferme/type_production_display', [
            'ferme' => $ferme,
        ]);
    }

    /**
     * Supprimer un produit proposé par le paysan.
     *
     * @param $type_production_id
     */
    public function type_production_remove($type_production_id)
    {
        /** @var \PsrLib\ORM\Entity\FermeTypeProductionPropose $tpp */
        $tpp = $this->findOrExit(\PsrLib\ORM\Entity\FermeTypeProductionPropose::class, $type_production_id);
        $ferme = $tpp->getFerme();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_TYPE_PRODUCTION_MANAGE, $ferme);

        $this->em->remove($tpp);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le produit n\'est plus proposé par le paysan'
        );

        redirect('ferme/type_production_display/'.$ferme->getId());
    }

    public function telecharger_ferme_complet()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_DOWNLOAD);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchFermeType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $this->load->helper('download');
        $outFileName = $this->excelgeneratorexportferme->genererExportFermeComplet($searchForm->getData());
        force_download(
            sprintf('liste_des_fermes_%s.xlsx', Carbon::now()->format('Y_m_d')),
            file_get_contents($outFileName),
            true
        );
        unlink($outFileName);
    }

    private function home_refprod(PsrLib\ORM\Entity\Amapien $refProd)
    {
        $this->loadViewWithTemplate('ferme/display_all_refprod', [
            'fermes' => $refProd->getRefProdFermes()->toArray(),
        ]);
    }

    private function home_regroupement(PsrLib\ORM\Entity\FermeRegroupement $regroupement)
    {
        $this->loadViewWithTemplate('ferme/display_all_regroupement', [
            'fermes' => $regroupement->getFermes()->toArray(),
        ]);
    }

    /**
     * Générer la CARTOGRAPHIE.
     *
     * @param mixed $fermes
     *
     * @var \PsrLib\ORM\Entity\Ferme[]
     */
    private function cartographie($fermes)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_LIST);

        $gps = 0;
        $map = null;
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        foreach ($fermes as $ferme) {
            if ($ferme->getGpsLatitude()) {
                ++$gps;
                $config = [
                    'center' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(),
                    'zoom' => 7,
                ];

                $this->leaflet->initialize($config);

                // INFOS DU POPUP
                $infos = '<strong>'.addslashes($ferme->getNom()).'</strong>';
                if ($ferme->getLibAdr()) {
                    $infos = $infos.'<br/>'.addslashes($ferme->getLibAdr());
                }
                $ville = $ferme->getVille();
                $infos = $infos.'<br/>'.$ville->getCpString();
                $infos = $infos.', '.$ville->getNom();

                // Emplacement du marker
                $marker = [
                    'latlng' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(), // Marker Location
                    // POPUP
                    'popupContent' => $infos, // Popup Content
                ];

                $this->leaflet->add_marker($marker);
            }
        }

        if ($gps > 0) {
            $map = $this->leaflet->create_map();
        }

        return [$gps, $map];
    }
}
