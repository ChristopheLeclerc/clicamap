<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\ORM\Entity\Files\EvenementImg;
use PsrLib\ORM\Entity\Files\EvenementPj;

/**
 * @ORM\Table(name="ak_evenement")
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\EvenementRepository")
 */
class Evenement
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="ev_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ?string
     *
     * @ORM\Column(name="ev_nom", type="string", length=150, nullable=true)
     */
    private $nom;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="ev_date_deb", type="date", nullable=true)
     */
    private $dateDeb;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="ev_date_fin", type="date", nullable=true)
     */
    private $dateFin;

    /**
     * @var null|string
     *
     * @ORM\Column(name="ev_hor_deb", type="string", length=5, nullable=true)
     */
    private $horDeb;

    /**
     * @var null|string
     *
     * @ORM\Column(name="ev_hor_fin", type="string", length=5, nullable=true)
     */
    private $horFin;

    /**
     * @var null|string
     *
     * @ORM\Column(name="ev_txt", type="text", length=65535, nullable=true)
     */
    private $evTxt;

    /**
     * @var null|string
     *
     * @ORM\Column(name="ev_url", type="string", length=500, nullable=true)
     */
    private $url;

    /**
     * @var null|EvenementImg
     *
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Files\EvenementImg", cascade="persist", orphanRemoval=true)
     */
    private $img;

    /**
     * @var null|EvenementPj
     *
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Files\EvenementPj", cascade="persist", orphanRemoval=true)
     */
    private $pj;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="ev_info_dep", type="boolean", nullable=true)
     */
    private $infoDep;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="ev_info_reseaux", type="boolean", nullable=true)
     */
    private $infoReseaux;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="ev_info_amap", type="boolean", nullable=true)
     */
    private $infoAmap;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="ev_info_fermes", type="boolean", nullable=true)
     */
    private $infoFermes;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="ev_info_tp", type="boolean", nullable=true)
     */
    private $infoTp;

    /**
     * @var null|bool
     *
     * @ORM\Column(name="ev_info_amap_amapiens", type="boolean", nullable=true)
     */
    private $infoAmapiens;

    /**
     * @var null|Ferme
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ferme")
     * @ORM\JoinColumn(name="ev_info_amap_ferme", referencedColumnName="f_id")
     */
    private $infoFerme;

    /**
     * @var null|string
     *
     * @ORM\Column(name="ev_info_createur_reseau", type="string", length=30, nullable=true)
     */
    private $infoCreateurReseau;

    /**
     * @var string
     *
     * @ORM\Column(name="ev_createur", type="string", length=50, nullable=false)
     */
    private $createur;

    /**
     * @var null|Amap
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amap")
     * @ORM\JoinColumn(name="ev_fk_amap_id", referencedColumnName="amap_id")
     */
    private $amap;

    /**
     * @var null|Amapien
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amapien")
     * @ORM\JoinColumn(name="ev_fk_amapien_id", referencedColumnName="a_id")
     */
    private $amapien;

    /**
     * @var ArrayCollection<Amap>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Amap")
     * @JoinTable(name="ak_evenement_info_amap",
     *      joinColumns={@JoinColumn(name="ev_info_amap_fk_ev_id", referencedColumnName="ev_id")},
     *      inverseJoinColumns={@JoinColumn(name="ev_info_amap_fk_amap_id", referencedColumnName="amap_id")}
     * )
     */
    private $relAmaps;

    /**
     * @var ArrayCollection<Departement>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Departement")
     * @JoinTable(name="ak_evenement_info_departement",
     *      joinColumns={@JoinColumn(name="ev_info_dep_fk_ev_id", referencedColumnName="ev_id")},
     *      inverseJoinColumns={@JoinColumn(name="ev_info_dep_fk_dep_id", referencedColumnName="dep_id")}
     * )
     */
    private $relDepartements;

    /**
     * @var ArrayCollection<Ferme>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Ferme")
     * @JoinTable(name="ak_evenement_info_ferme",
     *      joinColumns={@JoinColumn(name="ev_info_f_fk_ev_id", referencedColumnName="ev_id")},
     *      inverseJoinColumns={@JoinColumn(name="ev_info_f_fk_f_id", referencedColumnName="f_id")}
     * )
     */
    private $relFerme;

    /**
     * @var ArrayCollection<Reseau>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Reseau")
     * @JoinTable(name="ak_evenement_info_reseau",
     *      joinColumns={@JoinColumn(name="ev_info_res_fk_ev_id", referencedColumnName="ev_id")},
     *      inverseJoinColumns={@JoinColumn(name="ev_info_res_fk_res_id", referencedColumnName="res_id")}
     * )
     */
    private $relReseau;

    /**
     * @var ArrayCollection<TypeProduction>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\TypeProduction")
     * @JoinTable(name="ak_evenement_info_type_prod",
     *      joinColumns={@JoinColumn(name="ev_info_tp_fk_ev_id", referencedColumnName="ev_id")},
     *      inverseJoinColumns={@JoinColumn(name="ev_info_tp_fk_tp_id", referencedColumnName="tp_id")}
     * )
     */
    private $relTypeProduction;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->relAmaps = new ArrayCollection();
        $this->relDepartements = new ArrayCollection();
        $this->relFerme = new ArrayCollection();
        $this->relReseau = new ArrayCollection();
        $this->relTypeProduction = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Evenement
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateDeb(): ?DateTime
    {
        return $this->dateDeb;
    }

    public function setDateDeb(?DateTime $dateDeb): Evenement
    {
        $this->dateDeb = $dateDeb;

        return $this;
    }

    public function getDateFin(): ?DateTime
    {
        return $this->dateFin;
    }

    public function setDateFin(?DateTime $dateFin): Evenement
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getHorDeb(): ?string
    {
        return $this->horDeb;
    }

    public function setHorDeb(?string $horDeb): Evenement
    {
        $this->horDeb = $horDeb;

        return $this;
    }

    public function getHorFin(): ?string
    {
        return $this->horFin;
    }

    public function setHorFin(?string $horFin): Evenement
    {
        $this->horFin = $horFin;

        return $this;
    }

    public function getTimestampDeb(): ?string
    {
        $dateDeb = $this->getDateDeb();
        $horDeb = $this->getHorDeb();
        $timestamp = $dateDeb->format('Ymd');
        if ($horDeb) {
            $timestamp .= str_replace(':', '', $horDeb);
        } else {
            $timestamp .= '0000';
        }

        return $timestamp;
    }

    public function getTimestampFin(): ?string
    {
        $dateFin = $this->getDateFin();
        $horFin = $this->getHorFin();
        $timestamp = $dateFin->format('Ymd');
        if ($horFin) {
            $timestamp .= str_replace(':', '', $horFin);
        } else {
            $timestamp .= '0000';
        }

        return $timestamp;
    }

    public function getEvTxt(): ?string
    {
        return $this->evTxt;
    }

    public function setEvTxt(?string $evTxt): Evenement
    {
        $this->evTxt = $evTxt;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Evenement
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return null|EvenementImg
     */
    public function getImg(): ?Files\EvenementImg
    {
        return $this->img;
    }

    /**
     * @param null|EvenementImg $img
     */
    public function setImg(?Files\EvenementImg $img): Evenement
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return null|EvenementPj
     */
    public function getPj(): ?Files\EvenementPj
    {
        return $this->pj;
    }

    /**
     * @param null|EvenementPj $pj
     */
    public function setPj(?Files\EvenementPj $pj): Evenement
    {
        $this->pj = $pj;

        return $this;
    }

    public function getInfoDep(): ?bool
    {
        return $this->infoDep;
    }

    public function setInfoDep(?bool $infoDep): Evenement
    {
        $this->infoDep = $infoDep;

        return $this;
    }

    public function getInfoReseaux(): ?bool
    {
        return $this->infoReseaux;
    }

    public function setInfoReseaux(?bool $infoReseaux): Evenement
    {
        $this->infoReseaux = $infoReseaux;

        return $this;
    }

    public function getInfoAmap(): ?bool
    {
        return $this->infoAmap;
    }

    public function setInfoAmap(?bool $infoAmap): Evenement
    {
        $this->infoAmap = $infoAmap;

        return $this;
    }

    public function getInfoFermes(): ?bool
    {
        return $this->infoFermes;
    }

    public function setInfoFermes(?bool $infoFermes): Evenement
    {
        $this->infoFermes = $infoFermes;

        return $this;
    }

    public function getInfoTp(): ?bool
    {
        return $this->infoTp;
    }

    public function setInfoTp(?bool $infoTp): Evenement
    {
        $this->infoTp = $infoTp;

        return $this;
    }

    public function getInfoAmapiens(): ?bool
    {
        return $this->infoAmapiens;
    }

    public function setInfoAmapiens(?bool $infoAmapiens): Evenement
    {
        $this->infoAmapiens = $infoAmapiens;

        return $this;
    }

    public function getInfoFerme(): ?Ferme
    {
        return $this->infoFerme;
    }

    public function setInfoFerme(?Ferme $infoFerme): Evenement
    {
        $this->infoFerme = $infoFerme;

        return $this;
    }

    public function getInfoCreateurReseau(): ?string
    {
        return $this->infoCreateurReseau;
    }

    public function setInfoCreateurReseau(?string $infoCreateurReseau): Evenement
    {
        $this->infoCreateurReseau = $infoCreateurReseau;

        return $this;
    }

    public function getCreateur(): string
    {
        return $this->createur;
    }

    public function setCreateur(string $createur): Evenement
    {
        $this->createur = $createur;

        return $this;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap): Evenement
    {
        $this->amap = $amap;

        return $this;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): Evenement
    {
        $this->amapien = $amapien;

        return $this;
    }

    /**
     * Add relAmap.
     */
    public function addRelAmap(Amap $relAmap): Evenement
    {
        $this->relAmaps[] = $relAmap;

        return $this;
    }

    /**
     * Remove relAmap.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRelAmap(Amap $relAmap): bool
    {
        return $this->relAmaps->removeElement($relAmap);
    }

    /**
     * Get relAmaps.
     *
     * @return Collection
     */
    public function getRelAmaps()
    {
        return $this->relAmaps;
    }

    /**
     * Add relDepartement.
     */
    public function addRelDepartement(Departement $relDepartement): Evenement
    {
        $this->relDepartements[] = $relDepartement;

        return $this;
    }

    /**
     * Remove relDepartement.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRelDepartement(Departement $relDepartement): bool
    {
        return $this->relDepartements->removeElement($relDepartement);
    }

    /**
     * Get relDepartements.
     *
     * @return Collection
     */
    public function getRelDepartements()
    {
        return $this->relDepartements;
    }

    /**
     * Add relFerme.
     */
    public function addRelFerme(Ferme $relFerme): Evenement
    {
        $this->relFerme[] = $relFerme;

        return $this;
    }

    /**
     * Remove relFerme.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRelFerme(Ferme $relFerme): bool
    {
        return $this->relFerme->removeElement($relFerme);
    }

    /**
     * Get relFerme.
     *
     * @return Collection
     */
    public function getRelFerme()
    {
        return $this->relFerme;
    }

    /**
     * Add relReseau.
     */
    public function addRelReseau(Reseau $relReseau): Evenement
    {
        $this->relReseau[] = $relReseau;

        return $this;
    }

    /**
     * Remove relReseau.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRelReseau(Reseau $relReseau): bool
    {
        return $this->relReseau->removeElement($relReseau);
    }

    /**
     * Get relReseau.
     *
     * @return Collection
     */
    public function getRelReseau()
    {
        return $this->relReseau;
    }

    /**
     * Add relTypeProduction.
     */
    public function addRelTypeProduction(TypeProduction $relTypeProduction): Evenement
    {
        $this->relTypeProduction[] = $relTypeProduction;

        return $this;
    }

    /**
     * Remove relTypeProduction.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRelTypeProduction(TypeProduction $relTypeProduction): bool
    {
        return $this->relTypeProduction->removeElement($relTypeProduction);
    }

    /**
     * Get relTypeProduction.
     *
     * @return Collection
     */
    public function getRelTypeProduction()
    {
        return $this->relTypeProduction;
    }
}
