<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Symfony\Component\PropertyAccess\PropertyAccess;

/** @var \PsrLib\ORM\Entity\Contrat $contrat */
/** @var \PsrLib\ORM\Entity\Amapien $amapien */
/** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \PsrLib\ORM\Entity\Ferme $ferme */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraison_lieu */
/** @var \PsrLib\ORM\Entity\ModeleContratDate[] $dates */
/** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $produits */
/** @var \PsrLib\ORM\Entity\ModeleContratDatesReglement[] $dates_reglement */
/** @var \PsrLib\ORM\Entity\Paysan[] $paysans */
/** @var \PsrLib\ORM\Entity\ModeleContratProduitExclure[] $exclusions */
$imgCheck = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAF+CAYAAAD0uKLWAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH5AIWDjUhqnVX9QAADzlJREFUeNrt3d+L5Xd9x/HnrPFH68QgRW2txm7MWkhFjK0BMbtuBC8sFLzWG+m1Xgjeil5Y6FXBf6FiGxSkkDTagq7kh78aRagttWt+jFp/IJiYX0p+7PZizsASszF7TmbmnPN+POBDILlJvnNmn6/vd86Z7AQA2+/l1Turd1Vvqa6vXl1dXZ2oflv9utqrzlffq+6qfuHSAcBm2a0+VN1e/aa6uMT5z+oT1SmXEwDW22urT1W/WjL6lzt3V3/j8gLAevmDRfiXvdt/oeer1VtdbgA4fmerBw85/JeeJ6tPV1e59ABw9E4s7vqfPsL4X3ruqa71ZQCAo/Oy6tZjCv+l52fVjb4cAHD4dquvrEH8D87D1RlfFgA4PK+szq1R/A/O49V7fXkAYE78jQAAGBp/IwAAhsbfCACAofE3AgBgaPyNAAAYGn8jAACGxt8IAICh8TcCAGBo/I0AABgafyMAAIbG3wgAgKHxNwIAEP+h8TcCABD/4ccIAED8jQAAEH8jAADE3wgAAPE3AgBA/I0AABB/IwAAxN8IAADxNwIAQPyNAAAQfyMAAMTfCAAA8TcCAED8jQAAxN8xAgAQf8cIAED8HSMAAPF3jAAAxN8xAgAQf8cIAED8HSMAgCO2W90prhtzHqvOeNkC4M5/3nmkeoeXLwDu/Oedn1UnvYwBcOc/79xbvezZX9yXeH0DcJk7/y9V73EpNt7rF2Pu310KANz5zzrPVDd5aQMg/vPO16udgy+0HwEAcGn8b6/OuhRb6Y3Vd6sfuBQAuPOfdb7ppQ7AAR/1m3X+qvwIAMCd//5jf+/2n+Px6t92XAeA0Xf+d1SnXYpRflq9wQAAmH3nf9alGOkGPwIAEH/m+b4BACD+zPNTAwBA/JnnEQMAYAa/259LnTAAAObc+Ys/By76FADA9t/5+6gfz/bUCdcAYKvv/G8Tf57DMwYAwPbG3xv+uJzHrnINALaOx/78Pg95AgCwfXf+Hvvz+/zQEwAAd/7Mc94TAAB3/szzLR8DBNiO+HvDH1fiWj8CANhsHvtzpf6r+rEfAQBs9p2/x/5cqX+u8iMAAHf+zHGhOlXd7wkAgDt/5vhidb8nAADu/Jnlpuo/qjwBAHDnzwy3HsTfEwCAzYq/j/qxrCeqG6q9g7/hCQCA+LP9PnZp/AHYjPifqy46zpLnC8/1wvIjAID15Q1/rOq77T85etQAANicO3+P/VnF/dW7q58/1z/0HgAA8Wf7/Kh63+Xi7wkAgPiznfG/ZfEEIAMAQPwRfwMAQPyZGn8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwBA/BkYfwMAQPwZGH8DAED8GRh/AwAQf/FnYPwNAED8xZ+B8TcAAPGHgfE3AADxh4HxNwAA8YeB8TcAAPGHgfE3AADxh4HxNwAA8YeB8TcAAPGHgfE3AADxh4HxnzAA/rg6Xb29ur56U3VN9YrqQvVo9VB1fnG+UX27etLrHjbabnXH4vsflrG3iP8DLsXmOFV9svp+dXGJ80R1W/XBxR8iwObF/84lv/8d52L1YHXSt9LmuHkR7gsv4ovgkeoz1Z+6vLARXlmdEzBnhbNXXedbaTP8efXlQ35BPFH9vScCIP6O+HP8rqr+rv2f1x/Vi+OB6oxLD2vHY3/HY/8hrq2+fkwvkqfbf4/BCV8GEH9H/Dk6b61+sgYvmC+2/2kC4Ph47O947D/EmerhNXrhfCXvCwB3/o47fw7Vze1/bn/dXkB3V1f78oA7f8edP3PibwSA+Dviz9D4GwEg/o74MzT+RgCIvyP+DI2/EQDi74g/Q+NvBID4O+LP0PgbASD+jvgzNP5GAIi/I/4Mjb8RAOLviD9D428EgPg74s/Q+BsBIP6O+DM0/kYAiL8j/gyNvxEA4u+Iv/gPjb8RAOLviL/4Dz9GAIi/I/7ibwSA+DuO+Iu/EQDi7zjiL/5GAIi/44i/+BsBIP6O+CP+RgCIvyP+iL8RAOLviD/ibwSA+Dvij/gbASD+jvgj/kYAiL8j/oi/EQDi74g/4m8EgPg74o/4GwEg/o74I/5GAIi/I/7i7xgBiL/vT0f8xd8xAhB/xxF/8XeMAMTfccRf/B0jAPF3HPEXf8cIQPwd8Uf8HSMA8XfEH/F3jADE3xF/jtiZ6jEv2I05d1a7XrYckt3Fa8z3mrPsebA66Vtp/b2tesgL1pMAcOfvuPOf42T1cy9YTwLAnb/jzn+Ol1bf9IL1JADc+Tvu/Gf5By9YTwLAnb/jzn+Wd1RPe9F6EoA7f3f+jjv/OXaqb3jRGgGIv/g74j/LB7xojQDEX/wd8Z/nHi9cIwDx933iiP+cx/5V76y+7XKMcFf11+3/gic4sFvdUZ12KVjSXnVL9YBLsRlOLP76IZdijNPVlz0J4Fl3/reJPyv4UfVe8d+8JwA71f9Vf+JyeBKAO39w5z/nCcAN4u9JAO78wZ3/vAHwHpdhrHdXXzICxsb/9uqsS8EK8b+lut+l2NwBcKPLYAQYAeIP4j9vAJxyGYwAI0D8QfznDQC/p5mDEfCv+X8HbLPdxdATf5a1t3j9iP+WDIBrXAYWvDFwu+/8veGPVe/8veFvi+xUT1VXuRRcwkcEt+/O30f9WPXO30f9tvAJwAWXAU8C3PmDO/95A8BdHs/FGwO3I/7e8Meq8feGvy0eAL9yGXieEeCNgZvJG/5YlTf8DRgA97kMPA8/DtjMO3+P/Vn1zt9j/wED4H9dBjwJcOcP7vznDQD/G2A8CXDnD+78h9mp3lD92KXgBbqnen/1qEuxdvH3hj9Wjb83/A17AvCT6r9dCl4gPw5YPx77syqP/YcOgKpbXQqugB8HrNedv8f+rHrn77H/QDuLv765/TcDnnBJuAJ+Y+Dx3/n7DX+seufvN/wNfwJwX/UvLgeeBLjzx50/s54AVN1UfcslwZMAd/6482fOE4Da/zjg510SPAlw5487f2Y9Aaj9jwT+z+IPGbhSPiJ4+PH3UT9Wjb+P+vE7TwBq/yOBH3dZWJL/gZD4I/5suH+qLjrOkuduI+BFj/85rytnhbNXXedbiUvtXObvv6r6WnWjS8SSvDHwxeENf6zKG/64ogFQ9ZrFndxbXCaW5D0Bq9/5e+zPKjz257Ke7xf//LJ6n9XICrwnQPwRfzbwCcCBa9v/ccBJlwtPAsQf8WfOADACMALEH/Fn6AAwAjACxB/xZ+gAMAIwAsQf8WfoADACMALEH/Fn6AAwAjACxB/xZ+gAMAIwAsQf8WfoADACMALEH/Fn6AAwAjACxB/xZ+gAMAIwAsQf8WfoADACMALEH/Fn6AAwAjACxB/xZ+gAMAIwAsQf8WfoADACMALEH/Fn6AAwAjACxB/xZ+gAMAKYPALEH/Fn9AAwApg4AsQf8ccAMAIYNgLEH/HHADACGDYCxB/xxwAwAhg2AsQf8ccAMAIYNgLEH/HHADACGDYCxB/xxwAwAhg2AsQf8ccAMAIYNgLEH/HHADACGDYCxB/xxwAwAhg2AsQf8ccAMAIYNgLEH/HHADACGDYCxB/xxwAwAhg2AsQf8ccAMAIYNgLEH/HHADACGDYCxB/xxwAwAhg2AsQf8ccAMAIYNgLEH/HHADACGDYCxB/xxwAwAhg2AsQf8ccAMAIYNgLEH/HHADACGDYCxB/xxwAwAhg2Ai6IP+KPAbBZTlbnqjf5MrKkuxZ/Pe1SsKS9RfwfcCkwADwJANz5gwFgBADiDwaAEQCIPxgARgAg/mAAGAGA+IMBYAQA4g8GgBEAiD8YAEYAIP5gABgBgPiDAWAEAOIPsweAEQCIPwwdAEYAIP4wdAAYAYD4w9ABYAQA4g9DB4ARAIg/BsDg/3YjABB/DAAjABB/MACMAED8wQAwAgDxBwPACADEHwwAIwAQfzAAjABA/MEAMAIA8QcDwAgAxB8MACMAEH8wAIwAQPzBADACAPEHA8AIAMQfDAAjABB/MACMABB/8QcDwAgA8QcMACMAxB8wAIwAEH8wADACQPzBAMAIAPEHAwAjAMQfDACMABB/MAAwAkD8wQDACADxBwPACDACQPzBADACAPEHA8AIAMQfDAAjABB/OBonXIIj+wPtrD/QQPzBE4C5TwLOVde5FCD+YAAYASD+4g8GgBEA4g8YAEYAiD9gABgBIP6AAWAEgPgDBoARAOIPGABGAIg/YAAYASD+gAFgBID4gwGAEQDiDwYARgCIPxgAGAEg/mAAYASA+IMBgBEA4g8GAEYA4g8YABgBiD9gAGAEIP6AAYARgPgDBgBGAOIPGAAYAYg/YABgBCD+gAGAEYD4AwaAEWAEIP6AAWAEgPgDBoARAOIPGABGAIg/GAAYASD+YABgBID4gwGAEYD4AwYARgDiDxgAGAGIP2AAYAQg/oABgBGA+AMGAEYA4g/A8Y6A+6qLzuizZwgCGAGO+ANgBDjiD4AR4Ig/AEaAI/4AGAGO+ANgBDjiD4AR4Ig/AEaAI/4AGAGO+ANgBDjiD4AR4Ig/AEaAI/4AGAGO+ANgBIg/ABgB4g8ARoD4A4ARIP4AYASIPwAYAeIPAEaA+AOAESD+AGAEiD8AGAHiD4AR4Ig/AEaAI/4AGAGO+ANgBDjiD4ARIP4AYASIPwAYAeIPAEaA+AOAESD+AGAEiD8AGAHiDwBGgPgDgBEg/gBgBIg/ABgB4g8ARoD4A4ARIP4AMHkEiD8ADBsB4g8Aw0aA+APAsBEg/gAwbASIPwAMGwHiDwCH7M+q82sU//OLfycA4JC9rvrOGsT/3sW/CwBwRK6uPneM8f9stevLAADH42+rh48w/A9VH3bZAeD4/VH1meqZQwz/heof88gfANbO9Ysh8JsXMfxPLsL/Fy4vAKy311Ufre6pnl4i+k9Vd1UfqV7rcgLbZsclYIBXVTdXf1mdqt5cXVP94eKfP149Uv2w/Y/03bsYDo+6dMC2+n/rGJjue/oQEgAAAABJRU5ErkJggg==';
$imgTimes = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWAAAAFgCAYAAACFYaNMAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH5AIWDjYnaDuhAwAAEVJJREFUeNrt3VuspXdZx/HvXmXa2oPYUqXl0AZ7AEQSLVRqwAanCEUCXmAMV95p9AJLCcZojOHCREy88U4UA1QSjeECGCmnthcSHIi9RChtCbSAhYZSZQ4tQ0m92Gs7m82c9+Fd7/t+vskv03bapPn/n+c7z3r2u9Zaa7xcV72metnyr6+vLqt+avnr0WX+p3qkeqh6oPp89Z/VsQCsCudXN1U3Vzcs+/mF1c9UFy3zRPXk8tcHl/lS9dnqq45wd3lW9YbqA9W3qme2kaPV3dUfVM9xtMAgXFH9YXXPsie309PfrN5fvb46z9HuHM+r/rr6zjYv6GQ5Vn20usVRA3vCa6uPLXtvN3r629V7qqsc9blzdfW+6qlduqQT5WD1RkcP7Apvqr6wh/38VPX3y1UGzpB91e3VoT28qK35TPVSVwHsCNdVBwbs5yPVu6sLXcWpeUX1lQEvauufnu+s1lwLcE6sVe/a41exp8r91Y2u5cQXdfsKXdTWafhKVwScFVdU/7aC/XxsOQ0vXNHxlcOHVvCiNufr1UtcFXBGvLR6eMV7+s6le2bNxdVdK35RG3m8erXeAk7JTdVjI+npu6tL53pRz279qYNnRpTD1X49BpyQ/cseGVNPH1y6iHxHkiPVrXoN+DF+rWGfXNpO7mv93bPkS8IA+ZIw+ZIwMA/5zkLCU5IvCQPTku+kJTxF+ZIwyHeaPT0pCU9ZviQM8iVh8iVhgHxJeJ7yJWGQLwmTLwkD5DtnCc9ZviQM8iVh8iVhgHznJGHyJWGQLwmTLwkD5DsPCZMvCYN8SZh8SRgg33lImHxJGORLwgNImHxJGOQrA0iYfEkY5CsDSJh8SRjkKwNImHxJGOQrA0iYfEkY5CsDSPgS8t0TCfu2ZewV+5c1p/d299uWL9nuRe2rPuEwTcIw+cpZ5+7qgnO9qLXqQw7RJAyTr5xz7ly69Ky5w+GZhGHylW3nj872sl5RPeXgSBjkK9vOU9WNZ3pZF1ZfcWjWEbB2kB3L/We6D/5zh2UShslXdjx/eroLe2F12EGRMMhXdqWXrznVpb3PIVlHwNpBdi3vPdmlXVk96YBMwjD5yq7+QO55G5e12HRxdyx/AIfV5KLqYyZhnGby/WQ78A4s7BoXVG/f+JuNB4SfVX2zeq7zWXmOVm+p7nEU2DL53kW+o+Cx6vnV0xsT8OvId3STsHUEyHec/Fz12jq+gnibMxmlhK0jYO0wTt5Wx1cQ32rTYhijwTrC5GvyHSffqK5eq66vHnAeJAzyxZ7yokX1aucwaqwj5oe1wzS4ZVH9onOYhIQP5Adzc5l8P7q8c4ybX1i0voLAdCZhEp62fK0dpsMNi+pa5zA5CVtHTA9rh+lx3aK63DlMTsLWEdObfK0dpsdli+pi5zDZSZiEpyFfa4dpcula9XR1nrOYJEerN1f3OopRsn/5asbkO02eXlTHnMOkJ2HriPFOvtYO0+bYovUPYMe0JWwdMT75WjtMn0OL6vvOYTYS9nTE6uNph/nw/UX1iHOYjYStI1Z/8rV2mA8PL6oHncPsJmESXk35WjvMiwcXrX8FPeYnYeuI1cHaYZ48sKi+4BxmKWHriNWZfK0d5snBter86gkFMEt8lOXw8rV2mCeHq8s3ngM+6DxmOwlbRwyDtcO8+Vz1w42vJPqw85i1hK0j9n7ytXaYNx+u419JdHn1aOvrCMwT64i9k6+1w7z5YXVV9fjGBPy96hPOZfaTsHXE7mLtgKqPV49v/Ye3VM/I7HPEOmLXJt9D6kuq15ysSD7ncISEyVd2LZ/fXBiLLYXyl3oF1hHWDtg1/uJ0/8IBf0qJSdjkKzuej2wtkLUTFM211RerC/UP8nTEduTraQds8IPq5W357J0TfRPGE9WT1RucGap91W+3/pb1rzkO8sU58cfLmjgj1lp/UNzLBrGOsHaQ7eWuk2wbTvwPl1xR3Vddo6ewaR3hO+ZOju9ww1a+Xt1UffdEv7k4xX/43ep11WPOEEu8bfnUk6+3F2Mzj1dvPJl8z5SbW//kHi8lZPM6wiNqPz75HlEXsimHq1fZa4mdsJ2vTKA3FJqQsJ6QAXtCwQkJ6wUZsBcUnsxdwnpABu0BBShzlbDal5WofYUoc5OwmpeVqnkFKXORsFqXlax1hSlTl7Aal5WucQUqU5Ww2pZR1LZClalJWE3LqGpawcpUJKyWZZS1rHBl7BJWwzLqGlbAMtYCVrsyiVdxClnGVshqVib1cwwFLWMpaLUqk3ySR2HLqhe2GpVJP8uuwGVVC1xtyizezanQZdUKXU3KrD7PRMHLqhS8WpRZfqKfwpehC18Nylw/TlUDyKANoPZk1vLVCDJUI6g5IV8NIQM0hFoT8tUYMkBjqDEhXw0iAzSI2hLy1SgyQKOoKSFfDSMDNIxaEvLVODJA46ghIV8SlgEaSO0I+ZKwDNBIakbIl4RlgIZSK0K+JCwDNJYakVHKd21EEr6rusSfR1hytHpLdUxt4CS1cQ8B7xz7qwPVReoLmxotNYEtNfHm6t4x/M+ujexwTcIARj/5jlXAJAxgEvIdq4BJGMDo5TtmAZMwgFHLd+wCJmGAfEcr3ykImIQB8h0taxO5DBIGyJeASRgA+c5PwCQMkC8BkzAA8p2ngEkYIF8CJmEA5DtPAZMwQL4ETMIAyHeeAiZhgHwJmIQBkO88BUzCAPkSMAkDmLt85ypgEgbIl4BJGCDfucp37gImYYB8CZiEAfIlYBImYYB8CZiEAfIlYBIGQL4ETMIA+RIwCQMgXwImYYB8CZiEAZAvAZMwQL4ETMIA+YKASRggXwImYYB8QcAkDJAvAZMwQL4EDBIGyJeASRggXwIGCQPkS8AkDJAvAYOEQb4gYBIGyJeAQcIgXxAwCQPkS8AkTMIgXxAwCQPkS8AkDJAvCJiEAfIlYBIGyJeAQcIgX/IlYBIGyJeAQcIgXxAwCQPkS8AgYZAvCJiEAfIlYJAwyBcETMIA+QJnL+FD1TMiu5Aj1a3aDCBhIV+AhIV8AZCwkC9AwkK+AEhYyBcgYSFfgIRFyBcgYSFfgISFfMl3dCwcAQAApl8xBQPkKyQMgHyFhAHyFRIGyFeEhAHyFRIGyFeEhAHyFRIGyFeEhFcaX8q5WvL1pZzYbXwpJwGDfEHCIGDyBQmDgMkXIGECBvmChEHA5AuQMAGDfEHCIGDyBUiYgEG+IGEQMPkCJEzAIF+QMAiYfAESJmDyBUgYBEy+AAkTMPkCJEzAjoB8ARImYPIFSJiAQb4ACRMw+QIkTMAgX4CECZh8ARImYPIlX4CECZh8ARImYPIFSNhREDD5AiRMwOQLkDAImHwBEiZg8gVImIBBvgAJEzD5AiRMwORLvgAJEzD5AiRMwOQLgIQJmHwBEiZg8gVAwgRMvgAJEzD5AiBhAiZfgIQJmHwBkPDsBUy+AAkTMPkCmLuE5yJg8gVImIDJFwAJz0PA5AuQMAGTLwASnoeAyRcgYQImXwAkPA8Bky9AwgRMvgBIeB4CJl+AhAmYfAGQ8DwETL4ACY9WwmMWMPkCGLWExypg8gUwegmPUcDkC2ASEh6bgMkXwGQkvBjR4e6vPkm+2NJwRx0DllxUfWzpCgLe4cn3o8sDBjZPO7dVhx0HNkn4QHWro9g5+R6qnhFZ5siWBlMjcroaAfnKLjaWWhESJl8ZsKHUjJAw+cqAjaR2hITJVwZsIDUkJEy+MmDjqCUhYQ0jAzaMmhIS1igyYKOoLSFhDSIDNogaExLWGDJgY6g1IWENIQM2hJoTEtYIMmAjqD2ZtYQ1gAzdAGpQZilhhS+rUvhqUWYlYQUvq1bwalJmIWGFLqta6GpTJi1hBS6rXuBqVCYpYYUtYylstSqTkrCClrEVtJqVSUhYIctYC1ntyqglrIBl7FOEGpZR1rDClans0dSyjKqWFaxM7SfJalpGUdMKVab6LKXalpWubQUqU383kRqXlaxxhSlzeT+9WpeVqnUFKXP7RCk1LytR8wpR5vqZqmpfBq19BShzla8ekEF7QOHJ3OWrF2SQXlBwQr56QgboCYUm5Ks3ZIDeuLk67FBlS4Ht597/Z//yTNSGbOTw0p3b4vrqOw5TTL4mYTnrfLd66amKZu0Uv/ez1X3V1XoLS45Wb67udRQnnYQPVBc5Cix5uHrlUsY/weIUYv5H8sUW+b6FfE/JvdVty5efQNU11Z2nGXZ/gnd5+SDWDtYRsmO540xXENdWX6wu1EfYNPne4yjOWsJ3VZc4ClTHqpdXD5xuBfG35Avy3TafrX7TOgJLzq/+5nT/0pu8VJA8araTeERNNue2UxXLQQckdr52wrJr+cLJiuTXHY6QLwnLrueWE+2A36FH7Hxbf87Xznd3dsK/tTxjzJt3bvzFxlMQV1T/Xe1zNrOWrx+47c0k7OmIefPD6qrq8Y0J+HfIl3zJd88mYU9HzJt91Vvr+Arirc5k1vK1dth7CVtHzJu31voK4vzqibx/3eSLvcY6Yt69d9mi+hXyJV8MNglbR8yTi6obF9WrnMUs5WvtsDoSto6YJ7+6qF7sHGY5+fpUs9XBp6jNk+sXrX/oOuYlX5Pvak7C1hHz4oZFPvN3TvK1dlh9CVtHzIdrFtWlzmE2k6+1w+pjHTEfLl3kEZi5yNfkO65J2DpiJgI+3zlMWr7WDuOVsHXEtDl/0fqnX2G6k6+1w3ixjpg2RxYud9LyNflOYxK2jpgmhxbV95zD5ORr7TA9CVtHTI/vLaqvOofJTb7WDtPDOmJ6PLSoHnQOk5KvyXfak7B1xHR4cNH6V9Bj/PK1dpiPhK0jpsGX1qprq4echckXo8JHWY6fF218JdEj1QudB/mChLEnfKO6euMbMe52HqOUr7XDvLGOGC+fquNfSfQvzmOUk6+nHeDpiHHyz3X8W5HPq75ZXelcRiNfky82Yx0xHr5dvaD60cYE/KPqA85lFPK1dsCJsI4YD+9fOvfHeG71ZPWMrGSOVLeqXZzBJHxIv6xsnqqu2risxaaL+071T+rX5AuTMHZ1+n30ZL/5An96mnxhEpZdyaHq+ae7uD9zUOQLEpYdz5+cyaVdUN3vsFZCvvs5BNtk/7KW9NSw+XJn8eUXL88P5Ey+MAnLTv3g7ZfP9tJud3DkCxKWbeft53Jha9WdDs/aAdYRcs75YMff8HbW7Gv93TUO0uQLk7CcXe5uB770+JLqoMM0+cIkLGecg9XFO3VhP03CJl+YhOWMcl912U5fGAmTL0hYBpAvCZMvSFgGlC8Jky9IWAaULwmTL0hYBpQvCZMvSFgGlC8Jky9ImHwHlC8Jky9ImHxXABImX5Aw+ZIw+QIkPC/5kjD5goTJl4TJFyDhecqXhMkXJEy+JEy+AAnPU75zlDD5goTJl4TJFyBh8p2HhMkXJEy+JEy+AAmT7zwkTL7AtCQ8aflOScLkC0xLwrOQ7xQkTL7AtCQ8K/lu8OwRSvhwvr0YOBn7lz0ytm8vfvZcL+zi6uMjuajHq1frMeCU3FQ9NpKe/kx16dwvbF9154pf1NeqF+st4Ix4SfX1Fe/pDy7dgyW/Wx1dwYv6dPVc1wOcFVdUB1awn5+qbq/WXNFPcmN1/4pc1JPVO1wUcM6sVXcse2kVevrL1S+5ltOvJG6vvj/gRR2oft5VADvCtdW/NuyTS++uLnAVZ84LqvcuXzLs1UV9rnq9owd2hduq/9jjV7F/Vz3f0Z87V1bvqR7dpUv6QfWR1p9jBLD73LLsuWO71NOPVn+1dAd2iPOq36jeVz3S9p/n/VT1e9XljhYYhOdUv9/6D7q3+/zww9U/tP4mqfPGcgBj/gHTi5ZT68uq66vrWn83y8XLX48s87/Ly3mo+kr1+dbf+fK0+gdWhn3VK6ubqxuWPX1162+SuHiZJ5Y9/UT14LKn/6v699Yfexsd/wdi3wMWBXNePQAAAABJRU5ErkJggg==';

function getValue($item, $value, $default = '')
{
    $pa = PropertyAccess::createPropertyAccessor();
    if ($pa->isReadable($item, $value)) {
        return $pa->getValue($item, $value);
    }

    return $default;
}

/**
 * @param Contrat_vierge_dates_reglement[] $datesReglement
 * @param $id
 *
 * @return Contrat_vierge_dates_reglement|mixed
 */
function getDateReglementById(array $datesReglement, $id)
{
    foreach ($datesReglement as $date) {
        if ((int) $date->id === (int) $id) {
            return $date;
        }
    }

    throw new \InvalidArgumentException('Unknow date id');
}

?>

<html>
    <head>
        <style>
            @page {
                sheet-size: A4;
                margin-top: 25mm;
            }

            ul {
                list-style-type: 'U+002D';
                font-family: sans-serif;
                font-size: 12pt;
            }

            table {
                width: 100%;
            }

            th {
                text-align: left;
                font-family: sans-serif;
                font-size: 12pt;
            }

            td {
                font-family: sans-serif;
                font-size: 12pt;
            }

            td.disabled {
                background-color: #bdbdbd;
            }

            table.border , .border th,.border td {
                border: 1px solid black;
                border-collapse: collapse;
            }

            .text-danger {
                color: red;
            }

            p.title-amap {
                font-size: 14pt;
                color: green;
            }

            p.text-left {
                text-align: left;
            }

            p {
                font-family: sans-serif;
                font-size: 12pt;
                text-align: justify;
            }

            h1 {
                font-family: sans-serif;
                font-size: 18pt;
                color: red;
            }

            h2 {
                font-size: 12pt;
                font-weight: normal;
                text-decoration:underline;
            }

            .hide { /* Used in functional test */
                color: white;
            }
        </style>

    </head>

    <body>

    <htmlpageheader name="PageHeader">
        <span style="font-size: 28pt; color: green; font-family: sans-serif; text-decoration:underline;">
            CONTRA<span style="color: red">T</span> D'ENGAGEMENT SOLIDAIRE
        </span>
    </htmlpageheader>
    <sethtmlpageheader name="PageHeader" value="ON" show-this-page="1"/>


    <p class="title-amap">Pour un panier de <?=nl2br($mc->getFiliere()); ?> </p>
    <p class="title-amap"><?=$mc->getNom(); ?></p>
    <p class="title-amap">dans le cadre de l'AMAP <?=$amap->getNom(); ?></p>
    <p class="text-left">
        Le présent contrat est signé entre : <br />
        <b>Madame/Monsieur : </b><?=getValue($amapien, 'nomComplet', '[AMAPIEN]'); ?><br />
        Résidant : <?=getValue($amapien, 'adresse', '[ADRESSE]'); ?><br />
        <i>Ci-après dénommé l’amapien.ne</i><br />
        D'une part,
    </p>

    <p class="text-left">
        Et, <br />
        <b>La ferme : </b> <?=$ferme->getNom(); ?> <br />
        Production de <?=nl2br($mc->getFiliere()); ?> <br />
        Nom de l’entité juridique : <?=$ferme->getNom(); ?> <br />
        Résidant : <?=$ferme->getLibAdr(); ?> <?=$ferme->getVille()->getCpString(); ?> <?=$ferme->getVille()->getNom(); ?> <br />
        <i>Ci-après dénommé le/la paysan.ne.</i> <br />
        D'autre part
    </p>

    <h1>Article 1 : L’objet du contrat</h1>
    <p>
        Le présent contrat a pour objet de déterminer les modalités et les conditions de l'engagement des parties signataires du présent contrat en vue de : </p>
        <ul>
        <li>Soutenir l'exploitation agricole <?=$ferme->getNom(); ?></li>
        <li>Fournir à l’amapien.ne des paniers de saison et de qualité</li>
        </ul>
    <p>
        Le tout dans le respect du texte et de l'esprit de la Charte des AMAP.
    </p>

    <h1>Article 2 : Engagement du/de la paysan.ne</h1>
    <p>
        Conformément à la Charte des AMAP, le/la paysan.ne s’engage à fournir à l’amapien.ne des produits de qualité en termes gustatif et sanitaire et issus d’une exploitation respectueuse de la nature et de l’environnement.<br />
        Il/Elle s’engage à livrer des produits de sa ferme aux livraisons prévues pendant la durée du contrat (cf article 4).
    </p>

    <p>
        La livraison s’effectue à/au : <b><?=$livraison_lieu->getNom(); ?></b> <?=$livraison_lieu->getAdresse(); ?> <?=$livraison_lieu->getVille()->getCpString(); ?> <?=$livraison_lieu->getVille()->getNom(); ?> <br />
        Le lieu est couvert par une assurance souscrite par l’AMAP ou par le réseau local si ce dernier en contractualise une pour les AMAP de son territoire. L’assurance du réseau couvre le lieu de livraison, à condition que l’AMAP soit à jour de sa cotisation annuelle.<br />
        Le/La paysan.ne s’engage à être présent au moment des livraisons et à être transparent.e et disponible pour discuter avec l’amapien.ne de la vie de la ferme. <br />
    </p>

    <h1>Article 3 : Engagement de l’amapien.ne</h1>
    <p>
        L’amapien.ne s’engage :
        <ul>
            <li>à respecter la Charte des AMAP.</li>
            <li>à récupérer ses paniers aux moments de leurs livraisons.</li>
            <li>à payer, par avance, l’ensemble des paniers de la saison.</li>
        </ul>
    </p>

    <p>
        Conformément à la Charte des AMAP, l’amapien.ne accepte les risques liés aux aléas de la production.
    </p>

    <h1>Article 4 : Durée du contrat</h1>
    <p>
        Le contrat court du <b><?=date_format_french($dates[0]->getDateLivraison()); ?></b> au <b><?=date_format_french($dates[count($dates) - 1]->getDateLivraison()); ?></b> et comprend <?=count($dates); ?> livraisons.
    </p>

    <h1>Article 5 : Contenu et prix du panier</h1>
    <p>Le panier contient des produits de saison. La valeur du panier est une moyenne annuelle. Il peut y avoir une variation en fonction des saisons</p>

    <h2>5.1 Le/La paysan.ne s’engage</h2>
    <table class="border">
        <tr>
            <th>Type de panier</th>
            <th>Prix du panier TTC</th>
        </tr>
        <?php foreach ($produits as $produit) : ?>
            <tr>
                <td><?=$produit->getNom(); ?> / <?=$produit->getConditionnement(); ?></td>
                <td><?=number_format_french($produit->getPrix()); ?> €</td>
            </tr>
        <?php endforeach; ?>
    </table>

    <p>Le/La paysan.ne s’engage au(x) date(s) suivante(s) : </p>

    <table class="border">
        <tr>
            <th></th>
            <?php foreach ($produits as $produit) : ?>
                <th><?=$produit->getNom(); ?></th>
            <?php endforeach; ?>
        </tr>

        <?php foreach ($dates as $date) : ?>
            <tr>
                <td><b><?= date_format_french($date->getDateLivraison()); ?></b></td>
                <?php foreach ($produits as $produit) : ?>
                    <td>
                        <?php if (isset($exclusions[$produit->getId()][$date->getId()])): ?>
                            <img style="width: 12px" src="<?=$imgTimes; ?>" />
                            <span class="hide">0</span>
                        <?php else: ?>
                            <img style="width: 12px" src="<?=$imgCheck; ?>" />
                            <span class="hide">1</span>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </table>

    <h2>5.2 L’amapien.ne s’engage</h2>

    <table class="border">
        <tr>
            <th></th>
            <?php foreach ($produits as $produit): ?>
                <th><?=$produit->getNom(); ?> / <?=$produit->getConditionnement(); ?></th>
            <?php endforeach; ?>
        </tr>

        <?php foreach ($dates as $date): ?>
            <tr>
                <?php if (empty($commandes) || \PsrLib\ORM\Repository\ContratCelluleRepository::countNbLivraisonsByDate($commandes, $date) > 0.0) : ?>
                    <td><?=date_format_french($date->getDateLivraison()); ?></td>
                    <?php foreach ($produits as $produit): ?>
                        <td
                                class="<?=isset($exclusions[$produit->getId()][$date->getId()]) ? 'disabled' : ''; ?>"
                        >
                            <?php if (!empty($commandes)): ?>
                                <?=\PsrLib\ORM\Repository\ContratCelluleRepository::findCelluleCount($commandes, $date, $produit); ?>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>

    </table>

    <p>
        L’amapien.ne s’engage au(x) date(s) suivante(s) :
        <ul>
            <?php foreach ($dates as $date): ?>
                <?php if (empty($commandes) || \PsrLib\ORM\Repository\ContratCelluleRepository::countNbLivraisonsByDate($commandes, $date) > 0.0) : ?>
                    <li><?= date_format_french($date->getDateLivraison()); ?></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </p>

    <h2>5.3 Modalités de paiement</h2>
    <p>
        Le règlement se fait en
            <?php
                foreach ($mc->getReglementType() as $type) {
                    $reglements[] = \PsrLib\ORM\Entity\ModeleContrat::getModeLabel($type);
                }
                echo implode('/', $reglements);

            ?>
        selon les modalités suivantes : <br />
        <?=nl2br($mc->getReglementModalite()); ?>
    </p>
    <p>A l’ordre de <?=$ferme->getNom(); ?></p>

    <p>
        Le(s) règlement(s) est/sont encaissé(s) à la/aux date(s) suivante(s) :
        <table class="border">
            <tr>
                <th>Nombre de règlement</th>
                <th>Date d’encaissement</th>
                <th>Montant</th>
            </tr>
            <?php if (empty($contrat_signe_dates_reglements)): ?>
                <?php foreach ($dates_reglement as $i => $date): ?>
                    <tr>
                        <td><?= $i + 1; ?></td>
                        <td><?= date_format_french($date->getDate()); ?></td>
                        <td></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <?php foreach ($contrat_signe_dates_reglements as $i => $date): ?>
                    <tr>
                        <td><?= $i + 1; ?></td>
                        <td><?= date_format_french($date->getModeleContratDatesReglement()->getDate()); ?></td>
                        <td><?= money_format_french($date->getMontant()); ?> €</td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
    </p>

    <?php if (\PsrLib\ORM\Repository\ModeleContratProduitRepository::hasRegul($produits)): ?>
        <h2>5.4 Contrat avec régularisation de poids</h2>
        <p>
            Les produits tels que la viande et le poisson font l’objet d’une régularisation de poids car sur ces types de produits le montant total du contrat peut varier en fonction du poids réel livré.<br />
            Les prix qui sont inscrits au contrat sont des prix unitaires estimés. Ces prix unitaires estimés sont calculés : prix au kg et estimation du poids moyen du produit livré.<br />
            A chaque livraison les produits sont soient : <br />
            <ul>
            <li>En amont de la livraison et étiquetés,</li>
            <li>Pendant la livraison devant l’AMAPien</li>
            </ul>
            Le(s) poids livré(s) est/sont noté(s) dans un tableau de suivi par consom’acteur. Le tableau de suivi est disponible à la demande<br />
            A la fin du contrat, un rééquilibrage est à effectuer si le montant des produits livrés est supérieur ou inférieur au montant du contrat.<br />

        </p>
    <?php endif; ?>

    <h1>Article 6. Absences</h1>
    <h2>6.1 Absences imprévues</h2>
    <p>Le panier est considéré comme "perdu" si l'amapien.ne n'a pas manifesté son absence et n'a pas pu prendre ses dispositions pour le faire récupérer.</p>

    <h2>6.2 Options : Absences prévues</h2>
    <p>
        <i>Côté paysan.ne :</i> <br/>
        Le/La paysan.ne peut prévoir des dates de non livraison, indiquées dans les contrats.
    </p>
    <p class="text-danger">
        <i>
            [Attention, dans le cadre du respect de la charte, les non-livraisons doivent se faire au bénéfice du confort de travail du paysan sans remettre en cause le soutien des amapien.nes en cas d’aléas de production]
        </i>
    </p>

    <p>
        <i>Côté amapien.ne :</i> <br />
        En accord avec l’organisation implicite de l’AMAP et en prévenant à l’avance et en accord avec le/la paysan.ne, l’amapien.ne peut annuler une livraison à une date et récupérer une double livraison à une date convenue. Il n’y a pas de modification de volume de livraison sur le contrat initial.
    </p>

    <h1>Article 7 : Rupture anticipée du contrat</h1>
    <h2>Cas de non-respect des termes du contrat :</h2>
    <p>En cas de non-respect des termes du contrat d’engagement par l’une ou l’autre des parties, le présent contrat pourra être rompu après un préavis de 1 mois.<br />
        Le/La paysan.ne s’engage à livrer les livraisons dues durant la période de préavis. Par ailleurs, les sommes correspondantes à la période ultérieure au préavis sont restituées à l’amapien.ne.</p>

    <h2>Option : Cas de force majeure</h2>
    <p>
        Le contrat ne peut être résilié par l’amapien.ne qu’en cas de force majeure avérée (déménagement, changement non prévisible et conséquent de la composition de la famille ou de la situation sociale). <br />
        Il ne peut être résilié par le/la paysan.ne qu’en cas de force majeure avérée (perte de l’exploitation,  changement important de la situation familiale entraînant une impossibilité de production.) <br />
        Si la rupture intervient du fait de l’amapien.ne, il/elle doit  informer l’AMAP afin que la procédure retenue dans ce cas soit mise en place pour lui succéder au présent contrat dans ses droits et obligations, avec l’accord du/de la paysan.ne. <br />
        En cas de force majeure par l’une ou l’autre des parties,  le présent contrat pourra être rompu après un préavis de un mois. <br />
        Le/la paysan.ne s’engage à livrer les produits dus durant la période de préavis. Par ailleurs, les sommes correspondant à la période ultérieure au préavis sont restituées l’amapien.ne.
    </p>

    <h1>Article 8 : Litiges</h1>
    <p>
        En cas de litige relatif à l’application ou à l’interprétation du présent contrat d’engagement, il sera fait appel, en premier lieu, à la médiation du Réseau des AMAP de rattachement de l’AMAP s’il en existe un sur le territoire de l’AMAP. <br />
        En cas d’échec de la médiation, l’article 7 du présent contrat d’engagement s’appliquera de plein droit.<br />
        Les tribunaux compétents pourront alors connaître de tout litige persistant.<br />
    </p>

    <?php if (null !== $mc->getContratAnnexes() && '' !== trim($mc->getContratAnnexes())): ?>
        <h1>Article 9 : Autre.s élement.s au contrat</h1>
        <p><?=nl2br($mc->getContratAnnexes()); ?></p>
    <?php endif; ?>

    <p>Fait à <?=$amap->getAdresseAdminVille()->getNom(); ?> le <?php
        $date = getValue($contrat, 'dateModification', null);
        if (null === $date) {
            $date = getValue($contrat, 'dateCreation', null);
        }
        echo null === $date ? '[DATE DE SIGNATURE]' : date_format_french($date); ?>
    </p>

    <table>
        <tr>
            <td style="vertical-align: top">
                L’amapien.ne <br />
                <?=getValue($amapien, 'prenom', '[PRENOM AMAPIEN]'); ?> <?=getValue($amapien, 'nom', '[NOM AMAPIEN]'); ?>
            </td>
            <td style="vertical-align: top">
                <?php foreach ($paysans as $i => $paysan): ?>
                    <?php if (0 === $i) {
            echo 'Le/La Paysan.ne';
        } ?> <br />
                    <?= $paysan->getPrenom().' '.$paysan->getNom(); ?>
                <?php endforeach; ?>

                <?php if ($ferme->inRegroupement()): ?>
                    <br />
                    <br />
                    Membre(s) de <br />
                    <?= $ferme->getRegroupement()->getNom(); ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>

    </body>
</html>
