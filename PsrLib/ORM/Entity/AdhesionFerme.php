<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Assert\Assertion;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AdhesionFermeRepository")
 */
class AdhesionFerme extends Adhesion
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var Ferme
     * @ORM\ManyToOne(targetEntity="Ferme")
     * @ORM\JoinColumn(referencedColumnName="f_id", nullable=false)
     */
    protected $ferme;

    /**
     * @var AdhesionValueFerme
     * @ORM\OneToOne(targetEntity="AdhesionValueFerme", cascade="ALL")
     * @Assert\Valid()
     * @ORM\JoinColumn(nullable=false)
     */
    protected $value;

    /**
     * @var Amapien
     * @ORM\ManyToOne(targetEntity="Amapien")
     * @ORM\JoinColumn(referencedColumnName="a_id", nullable=false)
     * @Assert\Expression("value.isAdminOf(this.getFerme())", groups={"import"}, message="Erreur d'accès à la ferme")
     */
    protected $creator;

    /**
     * AdhesionFerme constructor.
     */
    public function __construct(Ferme $ferme, AdhesionValueFerme $value, Amapien $creator)
    {
        $this->ferme = $ferme;
        $this->value = $value;
        $this->creator = $creator;
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }

    public function setFerme(Ferme $ferme): AdhesionFerme
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getValue(): AdhesionValueFerme
    {
        return $this->value;
    }

    /**
     * @param AdhesionValueFerme $value
     *
     * @return AdhesionFerme
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Amapien
     */
    public function getCreator(): BaseUser
    {
        return $this->creator;
    }

    /**
     * @param Amapien $creator
     *
     * @return AdhesionFerme
     */
    public function setCreator(BaseUser $creator)
    {
        Assertion::isInstanceOf($creator, Amapien::class);
        $this->creator = $creator;

        return $this;
    }
}
