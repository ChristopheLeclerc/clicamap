# language: fr
Fonctionnalité: Comme amap, je doit pouvoir revenir à l'étape précédente lors de la création du contrat

  @database
  Scénario: Retour étape 1
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que j'ajoute le lieu de livraison "ll amap test 2" a l'AMAP "amap@test.amap-aura.org"
    Et que je suis "Gestion des contrats vierges"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Et que je suis "Créer un nouveau contrat"
    Et que je remplis "model_contrat_form1[nom]" avec "nom"
    Et que je remplis "model_contrat_form1[filiere]" avec "filiere"
    Et que je remplis "model_contrat_form1[livraisonLieu]" avec "3"
    Et je presse "Étape suivante"
    Quand je suis "Étape précédente"
    Alors le champ "model_contrat_form1[nom]" devrait contenir "nom"
    Alors le champ "model_contrat_form1[filiere]" devrait contenir "filiere"
    Alors le champ "model_contrat_form1[livraisonLieu]" devrait contenir "3"
