<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;
use DI\Annotation\Inject;
use PsrLib\Services\AmapDistributionNotificationSender;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cron extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\MPdfGeneration
     */
    public $mpdfgeneration;

    protected $data;

    /**
     * @Inject
     *
     * @var AmapDistributionNotificationSender
     */
    private $amapDistributionNotificationSender;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Email_Sender
     */
    private $emailSender;

    /**
     * Cron constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->data = [];
        $this->load->helper('array');
        $this->load->helper('email');
        $this->load->helper('string');

        $this->load->library('email');

        $this->setTestDateFromRequest();
    }

    public function index()
    {
        $this->cron_distribution_amapien_rappel();
        $this->cron_distribution_amapien_relance();
        $this->cron_distribution_amap_fin();

        $this->cron_paysan_alerte();

        // Healthcheck check
        if (isset($_ENV['HEALTHCHECK_ENDPOINT'])) {
            $client = new \GuzzleHttp\Client();
            $client->get($_ENV['HEALTHCHECK_ENDPOINT']);
        }
    }

    private function setTestDateFromRequest()
    {
        if ('true' !== getenv('CRON_ALLOW_FORCE_DATE')) {
            return;
        }

        $timestamp = $this->request->get('timestamp');
        if (null === $timestamp) {
            return;
        }

        \Carbon\Carbon::setTestNow(Carbon::createFromTimestamp($timestamp));
    }

    //  TÂCHES CRON : ACTIVÉES À 1H DU MATIN ---------------------------------------

    /**
     * ALERTE PAYSAN EN FONCTION DU DÉLAI DE LA FERME.
     */
    private function cron_paysan_alerte()
    {
        echo 'Envoi des alertes paysan : ';

        // FERMES ----------------------------------------------------------------
        $fermeRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Ferme::class);
        /** @var \PsrLib\ORM\Entity\Ferme[] $ferme_all */
        $ferme_all = $fermeRepo->findAll();
        foreach ($ferme_all as $f) {
            $fermeRepo->refreshConnection();

            // Pour prévenir le référent que la ferme n'a pas de délais ------------
            if (!$f->getDelaiModifContrat()) {
                $ref_alerte = true;
            } else {
                $ref_alerte = false;
            }

            // RÉFÉRENTS -----------------------------------------------------------
            /** @var \PsrLib\ORM\Entity\Amapien[] $ferme_referent */
            $ferme_referent = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->getRefProduitFromFerme($f);
            if (!empty($ferme_referent)) {
                foreach ($ferme_referent as $f_ref) {
                    if ($ref_alerte) {
                        $this->emailSender->envoyerPaysanAlerte(
                            $f_ref->getEmail(),
                            'Message '.SITE_NAME,
                            '<p>Bonjour, la ferme <b>'.ucfirst($f).'</b> ne possède pas de délai en jours entre l\'envoi du fichier par email et la livraison. Merci de renseigner cette information sur '.SITE_NAME.' afin que les paysans de cette ferme soient tenus au courant par email des futures livraisons.</p><p><a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a></p><p>'.'Merci de ne pas répondre à cet email qui a été généré automatiquement.<br/>
                                Cordialement.<br/>Ensemble nous sommes le mouvement des AMAP</p>'
                        );
                        usleep(100000);
                    }
                }
            }
        }

        echo 'ok <br />';
    }

    private function cron_distribution_amapien_rappel()
    {
        echo 'Envoi du rappel distribution aux amapiens : ';
        $this->amapDistributionNotificationSender->amapienRappels();
        echo 'ok <br />';
    }

    private function cron_distribution_amapien_relance()
    {
        echo 'Envoi de la relance distribution aux amapiens : ';
        $this->amapDistributionNotificationSender->amapRelance();
        echo 'ok <br />';
    }

    private function cron_distribution_amap_fin()
    {
        echo 'Envoi de l\'alerte de fin des distributions pour les amaps : ';
        $this->amapDistributionNotificationSender->amapAlerteFin();
        echo 'ok <br />';
    }
}
