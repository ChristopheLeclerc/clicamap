<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use LogicException;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Settings;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

class ExcelDecoder implements DecoderInterface
{
    public function decode($data, $format, array $context = [])
    {
        if ('xls' !== $format) {
            throw new LogicException('Unsupported format');
        }

        Settings::setLocale('fr');

        $reader = new Xls();
        if (!$reader->canRead($data)) {
            return null;
        }

        $sheet = $reader->load($data);
        $sheet0 = $sheet->getSheet(0);

        // Read headers
        $headers = [];
        $i = 1;
        while (($cell = $sheet0->getCellByColumnAndRow($i, 1, false)) !== null) {
            $headers[$i] = $cell->getFormattedValue();
            ++$i;
        }
        $headersCount = count($headers);

        // Read data
        $res = [];
        $line = 2;
        do {
            $lineEmpty = true;
            $lineData = [];
            for ($i = 1; $i <= $headersCount; ++$i) {
                $cell = $sheet0->getCellByColumnAndRow($i, $line, false);

                if (null === $cell) {
                    $lineData[$headers[$i]] = null;

                    continue;
                }

                $value = $cell->getFormattedValue();
                // convert datetime to french format
                if (Date::isDateTime($cell)) {
                    $value = Date::excelToDateTimeObject($cell->getValue());
                    $value = $value->format('d/m/Y');
                }
                if ('' === $value) {
                    $value = null;
                }

                if (null !== $value) {
                    $lineEmpty = false;
                }
                $lineData[$headers[$i]] = $value;
            }
            ++$line;
            if (!$lineEmpty) {
                $res[] = $lineData;
            }
        } while (!$lineEmpty);

        return $res;
    }

    public function supportsDecoding($format)
    {
        return 'xls' === $format;
    }
}
