<?php

namespace Test\Twig;

use PHPUnit\Framework\TestCase;
use PsrLib\Twig\ArrayExtension;

/**
 * @internal
 * @coversNothing
 */
class ArrayExtensionTest extends TestCase
{
    /**
     * @var ArrayExtension
     */
    private $extension;

    public function setUp()
    {
        $this->extension = new ArrayExtension();
    }

    public function testArrayUnique()
    {
        $res = $this->extension->arrayUnique(['a', 'b', 'a']);

        self::assertSame(['a', 'b'], $res);
    }
}
