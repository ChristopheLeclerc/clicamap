<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184617 extends AbstractMigration
{
    private $existingAmapAmapiens = [];

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        $this->existingAmapAmapiens = $this
            ->connection
            ->executeQuery('SELECT amap_a_id, amap_a_fk_amap_id, amap_a_fk_amapien_id, amap_a_gestionnaire FROM ak_amap_amapien')
            ->fetchAll()
        ;

        $amapienIds = array_column($this->existingAmapAmapiens, 'amap_a_fk_amapien_id');
        if (count($amapienIds) !== count(array_unique($amapienIds))) {
            throw new Exception('Duplicate in ak_amap_amapien');
        }
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_amapien ADD amap_id BIGINT DEFAULT NULL, ADD gestionnaire TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8D52AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('CREATE INDEX IDX_2CDF7F8D52AA66E8 ON ak_amapien (amap_id)');

        foreach ($this->existingAmapAmapiens as $existingAmapAmapien) {
            $this->addSql('UPDATE ak_amapien SET amap_id = :amap_id WHERE a_id = :amapien_id', [
                'amap_id' => $existingAmapAmapien['amap_a_fk_amap_id'],
                'amapien_id' => $existingAmapAmapien['amap_a_fk_amapien_id'],
            ]);
            if ('oui' === $existingAmapAmapien['amap_a_gestionnaire']) {
                $this->addSql('UPDATE ak_amapien SET gestionnaire = 1 WHERE a_id = :amapien_id', [
                    'amapien_id' => $existingAmapAmapien['amap_a_fk_amapien_id'],
                ]);
            }
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
