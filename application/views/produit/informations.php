<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
/** @var \Symfony\Component\Form\FormView $form */
?>

<h3>Produits proposés</h3>

<?= '<h5><a href="'.site_url('ferme').'">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.$ferme->getNom().'</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('produit').'">Produits proposés</a> <i class="glyphicon glyphicon-menu-right"></i> Ajouter</h5>';

?>

<div class="alert alert-info">
    Le catalogue produit est lié à une ferme : toute création ou modification d'un produit sera visible par toutes les AMAP travaillant avec cette ferme.
</div>

<form method="POST" action="">

    <input type="hidden" name="step" value="1"/>

    <div class="well">
        <div class="row">

            <div class="col-md-4">
                <div class="input-group">

<span class="input-group-btn">
<button class="btn btn-primary" type="button">Type de production</button>
</span>

                    <?= render_form_select_widget($form['typeProduction']); ?>

                </div>

            </div>

            <div class="col-md-4">
                <div class="input-group">

<span class="input-group-btn">
<button class="btn btn-primary" type="button">Nom</button>
</span>

                    <?= render_form_text_widget($form['nom']); ?>
                </div>

            </div>

            <div class="col-md-4">
                <div class="input-group">

                <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Conditionnement</button>
                </span>

                    <?= render_form_text_widget($form['conditionnement']); ?>

                </div>

            </div>

            <div class="col-md-4">
                <div class="input-group">

                <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Prix</button>
                </span>

                    <?= render_form_number_widget($form['prix'], 0, 0.01); ?>

                </div>

            </div>

            <div class="col-md-4">
                <div class="input-group">

                <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Label</button>
                </span>

                    <?= render_form_select_widget($form['certification']); ?>

                </div>

            </div>

            <div class="col-md-2">
                <button class="btn btn-primary" type="button">Régularisation de poids ?</button>
            </div>
            <div class="col-md-1 col-md-offset-1">
                <div>
                    <div class="checkbox">
                        <?= render_form_checkbox_widget($form['regulPds']); ?>
                    </div>
                </div>

            </div>


        </div>
    </div>

    <div class="form-group text-right">

        <a href="<?= site_url('produit/accueil/'.$ferme->getId()); ?>" class="btn btn-default">Annuler</a>
        <input type="submit" value="Sauvegarder" class="btn btn-success"/>

    </div>

</form>
