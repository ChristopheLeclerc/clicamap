<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
$currentUser = get_current_connected_user();
?>

<h3>Choix de la ferme</h3>
<h5><a href="<?= site_url('paysan'); ?>">Gestion des paysans</a> <i class="glyphicon glyphicon-menu-right"></i>
    Choix de la ferme</h5>

<form method="POST" action="">

<?php
    if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
        echo '<div class="alert alert-info">Vous devez associer le paysan a une ferme.</div>';
    } else {
        echo '<div class="alert alert-info">Vous devez associer le paysan a une ferme. Si cette ferme existe déjà, choisissez-la dans la liste ci-dessous puis cliquez sur le bouton "Étape suivante", sinon cliquez sur ce dernier sans faire de choix, vous serez alors invité à créer cette ferme.</div>';
    }

    ?>

    <div class="row">

        <div class="col-md-12">
            <div class="input-group">

<span class="input-group-btn">
<button class="btn btn-primary" type="button">Ferme</button>
</span>

                <?= render_form_select_widget($form['ferme']); ?>
            </div>
        </div>
    </div>

    <div class="form-group">

        <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>

        <a href="/paysan/informations_generales_creation" type="submit" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>

        <a href="<?= site_url('paysan'); ?>" class="pull-right btn btn-default">Annuler</a>
</form>
