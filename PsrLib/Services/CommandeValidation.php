<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContrat;

class CommandeValidation
{
    /**
     * @var OrderProcessor
     */
    private $orderProcessor;

    public function __construct(OrderProcessor $orderProcessor)
    {
        $this->orderProcessor = $orderProcessor;
    }

    /**
     * @param ContratCellule[] $commande
     * @param ModeleContrat    $mc
     * @param bool             $bypassNbLivCheck
     *
     * @return null|string null indicate validation success
     */
    public function validate($commande, $mc, $bypassNbLivCheck = false)
    {
        $nbLivAmapienMin = (int) $mc->getNblivPlancher();
        if ($bypassNbLivCheck) {
            $nbLivAmapienMin = 1;
        }
        $mcNblivBypass = $mc->getNblivPlancherDepassement();

        if (
            (($bypassNbLivCheck || $mcNblivBypass) && $this->orderProcessor->countNbLivraisons($commande) < $nbLivAmapienMin)
            || ((!$bypassNbLivCheck && !$mcNblivBypass) && $this->orderProcessor->countNbLivraisons($commande) !== $nbLivAmapienMin)
        ) {
            return sprintf('Vous devez sélectionner <b>%s</b> livraisons', $nbLivAmapienMin);
        }
        if (($mc->getProduitsIdentiqueAmapien() && !$this->orderProcessor->areLivSame($commande))
            && (!$bypassNbLivCheck)) {
            return 'Toutes les livraisons doivent être identiques';
        }

        return null;
    }
}
