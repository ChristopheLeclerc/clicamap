<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
/** @var \Symfony\Component\Form\FormView $searchForm */
/** @var null|\PsrLib\DTO\SearchAmapienState $amapienSearchState */
$currentUser = get_current_connected_user();
?>

<script type="text/javascript">
  /*ALERTES*/
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 3000);

  });

</script>

<?php

if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_CREATE_NO_AMAP)) {
    echo '<a title="Destiné à la création de futurs comptes administrateurs" href="'.site_url('amapien/informations_generales_creation').'" class="btn btn-success pull-right" data-toggle="tooltip" title="Destiné à la création de futurs comptes administrateurs" style="margin-top:5px;"><i class="glyphicon glyphicon-plus"></i> Créer un amapien hors AMAP</a>&nbsp;';
}

if (get_current_connected_user() instanceof \PsrLib\ORM\Entity\Amapien) {
    echo '<h3>Liste des amapiens</h3>';
} else {
    echo '<h3>Gestion des amapiens</h3>';
}
?>

<form method="GET" id="mdr" action="/amapien" class="js-form-search">
    <div class="well">
        <div class="row">
            <?php if ($searchForm->offsetExists('region')): ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Région</button>
                        </span>
                        <?= render_form_select_widget($searchForm['region']); ?>

                    </div>
                </div>
            <?php endif; ?>

            <?php if ($searchForm->offsetExists('departement')): ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary"
                                    type="button">Département</button>
                        </span>
                        <?= render_form_select_widget($searchForm['departement']); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($searchForm->offsetExists('reseau')): ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button">Réseau</button>
                        </span>
                        <?= render_form_select_widget($searchForm['reseau']); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">AMAP</button>
                    </span>
                    <?= render_form_select_widget($searchForm['amap']); ?>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="input-group">

            <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Mot clé</button>
            </span>
                    <?= render_form_text_widget($searchForm['keyword']); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="input-group">

            <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Années d'adhésion</button>
            </span>
                    <?= render_form_select_widget($searchForm['adhesion']); ?>
                </div>
            </div>

        </div>


        <div class="row">

            <div class="form-group col-md-4">
                <?= render_form_checkbox_widget($searchForm['newsletter']); ?>
            </div>
            <div class="form-group col-md-4">
                <?= render_form_checkbox_widget($searchForm['active']); ?>
            </div>
            <?php if ($searchForm->offsetExists('noAmap')): ?>
                <div class="form-group col-md-4">
                    <?= render_form_checkbox_widget($searchForm['noAmap']); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="row text-right">
	    <a href="<?= site_url('amapien?reset_search'); ?>" class="btn btn-primary" style="margin-right:5px;"><i class="glyphicon glyphicon-refresh"></i> Réinitialiser les critères de recherche</a>
	</div>

    </div><!-- well -->

</form>

<?php
function amapienCreationBouton($amapienSearchState)
{
    // Un amapien ne peut-être créé qu'en lien avec une AMAP
    if (null !== $amapienSearchState && null !== $amapienSearchState->getAmap()) {
        return '<a href="'.site_url('amapien/informations_generales_creation/'.$amapienSearchState->getAmap()->getId()).'" class="btn btn-success"><i
                class="glyphicon glyphicon-plus"></i> Créer un nouvel amapien</a>';
    }

    return '<a href="#" class="btn btn-success" disabled><i class="glyphicon glyphicon-plus"></i> Créer un nouvel amapien</a>';
}

if (count($amapiens) > 0) {
    echo '<p>';
    echo amapienCreationBouton($amapienSearchState); ?>

<?php if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) : ?>
<a class="btn btn-success" href="<?= site_url('import'); ?>">
    <i class="glyphicon glyphicon-upload"></i>
    Importer une liste
</a>
<a href="<?= site_url('amapien/telecharger_amapiens_amap?'.http_build_query($this->input->get())); ?>" class="btn btn-primary"><i
            class="glyphicon glyphicon-download"></i> Télécharger la liste</a>
<?php else: ?>
    <a href="<?= site_url('amapien/telecharger_amapiens?'.http_build_query($this->input->get())); ?>" class="btn btn-primary"><i
                class="glyphicon glyphicon-download"></i> Télécharger la liste</a>
<?php endif; ?>
<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_email" data-dismiss="modal"
   aria-hidden="true"><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>


<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_autre"><i
            class="glyphicon glyphicon-lock"></i></a>

</p>

<!-- Modal email -->
<div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les amapiens de cette liste
                    ?</h4>
            </div>
            <div class="modal-body">
                <h5>Pour envoyer un email à tous ces amapiens , vous devez :</h5>
                <ul>
                    <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                        Copier sur la zone bleue ci dessous
                    </li>
                    <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook, ...)</li>
                    <li>Faire nouveau message</li>
                    <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                        Coller dans la liste des destinataires du message.
                    </li>
                </ul>

                <form>
                    <div class="form-group">
                        <?php
                        $amapienEmails = array_map(function (PsrLib\ORM\Entity\Amapien $amapien) {
                            return $amapien->getEmail();
                        }, $amapiens); ?>
                        <textarea class="form-control" rows="5"><?=implode(PHP_EOL, $amapienEmails); ?></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Autre -->
<div class="modal fade" id="modal_autre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Veuillez indiquer ce que vous souhaitez faire :</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">Attention ! Cette fonction n'a pas d'étape de validation, l'action est
                    immédiate : <br/> (fonction désactivée à cause des quotas pour l'envoi de mails)
                </div>
                <p><a href="#">Envoyer un mot de passe à tous les amapiens de la liste qui n'en ont pas</a></p>
            </div>
        </div>
    </div>
</div>

<?php

if (1 === count($amapiens)) {
    echo '<div class="alert alert-success">1 résultat pour cette recherche :</div>';
} else {
    echo '<div class="alert alert-success">'.count($amapiens).' résultats pour cette recherche :</div>';
} ?>

<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-inverse">
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Email</th>
            <th>Mot de passe</th>
            <th>Liste d'attente</th>
            <th class="text-right">Outils</th>
        </tr>
        </thead>

        <?php

        echo '<tbody>';

    foreach ($amapiens as $amapien) {
        echo '<tr>';
        echo '<th scope="row">';
        echo strtoupper($amapien->getNom());
        echo '</th>';

        echo '<td>';
        echo ucfirst($amapien->getPrenom());
        echo '</td>';

        echo '<td>';
        echo '<a href="mailto:'.$amapien->getEmail().'">'.$amapien->getEmail().'</a>';
        echo '</td>';

        echo '<td>';
        if (null !== $amapien->getPassword()) {
            echo 'Oui';
        } else {
            echo 'Non';
        }
        echo '</td>';

        echo '<td>';
        if (!$amapien->getFermeAttentes()->isEmpty()) {
            echo 'Oui';
        } else {
            echo 'Non';
        }
        echo '</td>';

        echo '<td class="text-right">';
        echo '<a title="Activation/desactivation de l\'amapien" href="'.site_url('amapien/de_activate/'.$amapien->getId().'/'.$amapien->getEtat()).'" >';

        if (\PsrLib\ORM\Entity\Amapien::ETAT_ACTIF === $amapien->getEtat()) {
            echo '<i title="Désactiver le compte de l\'amapien" class="glyphicon glyphicon-pause" data-toggle="tooltip" title="Désactiver le compte de l\'amapien"></i></a>&nbsp;';
        } else {
            echo '<i title="Activer le compte de l\'amapien" class="glyphicon glyphicon-play" data-toggle="tooltip" title="Activer le compte de l\'amapien"></i></a>&nbsp;';
        }

        echo '<a title="Afficher la fiche de l\'amapien" href="'.site_url('amapien/display/'.$amapien->getId()).'"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="Afficher la fiche de l\'amapien"></i></a>&nbsp;';

        echo '<a title="Éditer la fiche de l\'amapien" href="'.site_url('amapien/informations_generales_edition/'.$amapien->getId()).'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Éditer la fiche de l\'amapien"></i></a>&nbsp;';

        echo '<a title="Liste d\'attente" href="'.site_url('amapien/liste_attente/'.$amapien->getId()).'"><i class="glyphicon glyphicon-list" data-toggle="tooltip" title="Liste d\'attente"></i></a>&nbsp;';

        echo '<a title="Envoyer un mot de passe à l\'amapien" href="#" data-toggle="modal" data-target="#modal_key_'.$amapien->getId().'" ><i class="glyphicon glyphicon-lock" data-toggle="tooltip" title="Envoyer un mot de passe à l\'amapien"></i></a>&nbsp;';

        echo '<a title="Supprimer le compte de l\'amapien" href="#" data-toggle="modal" data-target="#modal_remove_'.$amapien->getId().'" ><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le compte de l\'amapien"></i></a>'; ?>

            <!-- Modal mot de passe -->
            <div class="modal fade text-left" id="modal_key_<?= $amapien->getId(); ?>" tabindex="-1"
                 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment envoyer un mot de passe à
                                l'amapien(ne) <?= strtoupper($amapien->getNom()).' '.ucfirst($amapien->getPrenom()); ?>
                                ?</h5>
                        </div>
                        <div class="modal-body">
                            <p>Cette action créera un mot de passe à l'amapien (même s'il en a déjà un) qui lui sera
                                envoyé par email ; de plus, son profil sera activé.</p>
                            <p class="text-right">
                                <a href="<?= site_url('amapien/password/'.$amapien->getId()); ?>"
                                   class="btn btn-danger btn-sm">OUI</a>
                                <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal remove -->
            <div class="modal fade text-left" id="modal_remove_<?= $amapien->getId(); ?>" tabindex="-1"
                 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                            </button>
                            <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer
                                l'amapien(ne) <?= strtoupper($amapien->getNom()).' '.ucfirst($amapien->getPrenom()); ?>
                                " ?</h5>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger"><strong>Attention !</strong> En supprimant
                                l'amapien(ne), vous supprimerez aussi ses contrats ainsi que tous ses comptes : son
                                compte référent produit si il ou elle en possède un et de fait tous les contrats
                                vierges et signés rattachés à ce compte ; ses comptes d'administrateur si il ou elle
                                en possède etc.
                            </div>
                            <p class="text-right">
                                <a href="<?= site_url('amapien/remove/'.$amapien->getId()); ?>"
                                   class="btn btn-danger btn-sm" title="supprimer amapien">OUI</a>
                                <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <?php

            echo '</td>';

        echo '</tr>';
    }

    echo '</tbody>';
    echo '</table>';
    echo '</div>';
} else {
    echo amapienCreationBouton($amapienSearchState);

    echo '&nbsp;<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-download"></i> Télécharger la liste</a>&nbsp;';
    echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>&nbsp;';
    echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-lock"></i></a></p>';
}

        ?>
