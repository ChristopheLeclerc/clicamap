<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\Numeric;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AkFermeProduit.
 *
 * @ORM\Table(name="ak_ferme_produit")
 * @ORM\Entity
 */
class FermeProduit
{
    public const AVAILABLE_CERTIFICATIONS = [
        'AB' => 'ab',
        'N&P' => 'np',
        'En conversion AB' => 'conversion',
        'Non labellisé' => 'aucun',
    ];

    /**
     * @var ?int
     *
     * @ORM\Column(name="f_pro_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *  @Groups({"wizard"})
     */
    private $id;

    /**
     * @var string | null
     *
     * @ORM\Column(name="f_pro_nom", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $nom;

    /**
     * @var string | null
     *
     * @ORM\Column(name="f_pro_conditionnement", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $conditionnement;

    /**
     * @var Ferme | null
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ferme", inversedBy="produits")
     * @ORM\JoinColumn(name="f_pro_fk_ferme_id", referencedColumnName="f_id")
     * @Assert\NotNull
     */
    private $ferme;

    /**
     * @var TypeProduction | null
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\TypeProduction")
     * @ORM\JoinColumn(referencedColumnName="tp_id")
     * @Assert\NotNull
     */
    private $typeProduction;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_pro_prix", type="string", length=255, nullable=true)
     * @Numeric
     * @Assert\GreaterThanOrEqual(0)
     */
    private $prix;

    /**
     * @var bool
     *
     * @ORM\Column(name="f_pro_regul_pds", type="boolean", nullable=false)
     * @Assert\NotNull
     */
    private $regulPds = false;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_pro_certification", type="string", length=255, nullable=true)
     * @Assert\Choice(choices=FermeProduit::AVAILABLE_CERTIFICATIONS)
     */
    private $certification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): FermeProduit
    {
        $this->nom = $nom;

        return $this;
    }

    public function getConditionnement(): ?string
    {
        return $this->conditionnement;
    }

    public function setConditionnement(?string $conditionnement): FermeProduit
    {
        $this->conditionnement = $conditionnement;

        return $this;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): FermeProduit
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getTypeProduction(): ?TypeProduction
    {
        return $this->typeProduction;
    }

    public function setTypeProduction(?TypeProduction $typeProduction): FermeProduit
    {
        $this->typeProduction = $typeProduction;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): FermeProduit
    {
        $this->prix = $prix;

        return $this;
    }

    public function isRegulPds(): bool
    {
        return $this->regulPds;
    }

    public function setRegulPds(bool $regulPds): FermeProduit
    {
        $this->regulPds = $regulPds;

        return $this;
    }

    public function getCertification(): ?string
    {
        return $this->certification;
    }

    public function setCertification(?string $certification): FermeProduit
    {
        $this->certification = $certification;

        return $this;
    }
}
