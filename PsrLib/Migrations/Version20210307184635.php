<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184635 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_grpe_W=1 WHERE pay_cr_grpe_W=\'oui\'');
        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_grpe_W=0 WHERE pay_cr_grpe_W=\'non\'');

        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_CA=1 WHERE pay_cr_CA=\'oui\'');
        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_CA=0 WHERE pay_cr_CA=\'non\'');

        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_visite_dem=1 WHERE pay_cr_visite_dem=\'oui\'');
        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_visite_dem=0 WHERE pay_cr_visite_dem=\'non\'');

        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_fiche_loc_exp=1 WHERE pay_cr_fiche_loc_exp=\'oui\'');
        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_fiche_loc_exp=0 WHERE pay_cr_fiche_loc_exp=\'non\'');

        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_cagn_sol=1 WHERE pay_cr_cagn_sol=\'oui\'');
        $this->addSql('UPDATE ak_paysan_cadre_reseau SET pay_cr_cagn_sol=0 WHERE pay_cr_cagn_sol=\'non\'');

        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau CHANGE pay_cr_grpe_W pay_cr_grpe_W TINYINT(1) NOT NULL, CHANGE pay_cr_CA pay_cr_CA TINYINT(1) NOT NULL, CHANGE pay_cr_visite_dem pay_cr_visite_dem TINYINT(1) NOT NULL, CHANGE pay_cr_fiche_loc_exp pay_cr_fiche_loc_exp TINYINT(1) NOT NULL, CHANGE pay_cr_cagn_sol pay_cr_cagn_sol TINYINT(1) NOT NULL, CHANGE pay_cr_fk_pay_id pay_cr_fk_pay_id INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
