<?php

namespace PsrLib\Services\Password;

class PasswordEncoder
{
    public function encodePassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}
