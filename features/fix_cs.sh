#!/bin/bash

# Fix code style
docker-compose -f ../docker/docker-compose.yml exec server vendor/bin/php-cs-fixer fix
