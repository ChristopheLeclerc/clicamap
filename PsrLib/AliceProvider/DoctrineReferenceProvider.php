<?php

namespace PsrLib\AliceProvider;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Entity\Ville;

class DoctrineReferenceProvider
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function em_ville(int $id): Ville
    {
        return $this->em->getReference(Ville::class, $id);
    }

    public function em_region(int $id): Region
    {
        return $this->em->getReference(Region::class, $id);
    }

    public function em_departement(int $id): Departement
    {
        return $this->em->getReference(Departement::class, $id);
    }

    public function em_tp(int $id): TypeProduction
    {
        return $this->em->getReference(TypeProduction::class, $id);
    }
}
