<?php

namespace PsrLib\Twig;

use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FlashExtension extends AbstractExtension
{
    /**
     * @var Session
     */
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_flash_messages', [$this, 'getFlashMessages']),
        ];
    }

    public function getFlashMessages(): array
    {
        return $this->session->getFlashBag()->all();
    }
}
