<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var string[] $errors */
/** @var string $action */
$print_errors = function ($expectedAction) use ($action, $errors) {
    if ($expectedAction === $action) {
        foreach ($errors as $error) {
            echo sprintf('<div class="alert alert-danger">%s</div>', $error);
        }
    }
}
?>

<h3>Import des reçus</h3>

<div class="alert alert-info">
    Cet outil vous permet d'importer les informations nécessaires à la création de reçus. Pour importer des éléments en masse, vous devez d'abord remplir un fichier Excel au format XLS. Il est très important de respecter la structure du modèle mis à disposition en téléchargement dans chaque rubrique (nombre et nom des colonnes) et un format de donnée standard. Une fois que votre fichier Excel est prêt, vous pouvez le charger dans l'application.
</div>

<div class="well">
    <h4>AMAP</h4>
    <a href="<?= site_url('assets/import/Import_adhesion_amap.xls'); ?>" target="_blank">Exemple de format attendu</a>

    <?= $print_errors('amap'); ?>
    <form action="/adhesion/import" method="POST" enctype='multipart/form-data'>
        <div class="row">
            <div class="col-xs-12">
                <input type="file" name="import_amap" class="pull-left" />
                <button type="submit" class="btn btn-success pull-left ml-1" name="import_amap">Importer</button>
            </div>
        </div>
    </form>
</div>

<div class="well">
    <h4>Paysan</h4>
    <a href="<?= site_url('assets/import/Import_adhesion_paysan.xls'); ?>" target="_blank">Exemple de format attendu</a>

    <?= $print_errors('paysan'); ?>
    <form action="/adhesion/import" method="POST" enctype='multipart/form-data'>
        <div class="row">
            <div class="col-xs-12">
                <input type="file" name="import_paysan" class="pull-left" />
                <button type="submit" class="btn btn-success pull-left ml-1" name="import_paysan">Importer</button>
            </div>
        </div>
    </form>
</div>

<div class="well">
    <h4>Amapien</h4>
    <a href="<?= site_url('assets/import/Import_adhesion_amapien.xls'); ?>" target="_blank">Exemple de format attendu</a>

    <?= $print_errors('amapien'); ?>
    <form action="/adhesion/import" method="POST" enctype='multipart/form-data'>
        <div class="row">
            <div class="col-xs-12">
                <input type="file" name="import_amapien" class="pull-left"/>
                <button type="submit" class="btn btn-success pull-left ml-1" name="import_amapien">Importer</button>
            </div>
        </div>
    </form>
</div>
