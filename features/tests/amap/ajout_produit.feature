#language: fr
Fonctionnalité: Ajouter un produit à la liste d'une AMAP

  @database
  Scénario: Message erreur coche obligatoire, profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/amap"
    Et que je suis "Modifier l'AMAP"
    Et que je suis "Les produits proposés / recherchés"
    Alors je devrais être sur "/amap/recherche_paysan"
    Lorsque je sélectionne "1" depuis "amap_produit_recherche[tpPropose][]"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "Le champ d'activation est requis."

  @database
  Scénario: Ajout de produits effectué, profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/amap"
    Et que je suis "Modifier l'AMAP"
    Et que je suis "Les produits proposés / recherchés"
    Alors je devrais être sur "/amap/recherche_paysan"
    Lorsque je sélectionne une autre option "1" depuis "amap_produit_recherche[tpPropose][]"
    Et que je sélectionne une autre option "2" depuis "amap_produit_recherche[tpRecherche][]"
    Et que je coche "amap_produit_recherche[activation]"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAP a été modifiée avec succès !"
    Quand je suis sur "/amap/display/1"
    Alors je devrais voir "Légumes | Pain | Porc"
    Et je devrais voir "Fruits | Œufs | Pisciculture"

  @database
  Scénario: Ajout de produits effectué, profil réseau
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin.aura@test.amap-aura.org"
    Quand je suis sur "/amap/produits/1"
    Lorsque je sélectionne une autre option "1" depuis "amap_produit[tpPropose][]"
    Et que je sélectionne une autre option "2" depuis "amap_produit[tpRecherche][]"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAP a été modifiée avec succès !"
    Quand je suis sur "/amap/display/1"
    Alors je devrais voir "Légumes | Pain | Porc"
    Et je devrais voir "Fruits | Œufs | Pisciculture"

  @database
  Scénario: Ajout de produits effectué, profil super-admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand je suis sur "/amap/produits/1"
    Lorsque je sélectionne une autre option "1" depuis "amap_produit[tpPropose][]"
    Et que je sélectionne une autre option "2" depuis "amap_produit[tpRecherche][]"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAP a été modifiée avec succès !"
    Quand je suis sur "/amap/display/1"
    Alors je devrais voir "Légumes | Pain | Porc"
    Et je devrais voir "Fruits | Œufs | Pisciculture"
