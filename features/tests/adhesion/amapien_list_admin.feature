# language: fr
Fonctionnalité: Liste des recus amapiens

  @database
  Plan du Scénario: Lancement de la génération
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Et que je suis sur "/adhesion/import"
    Et que j'attache le fichier "Import_adhesion_amapien_success.xls" à "import_amapien"
    Et que je presse "import_amapien"
    Quand je suis sur "/adhesion/amapien"
    Alors je devrais voir "Brouillon"
    Et Je clique sur l'élément ".js-select-all"
    Et je presse "Générer les reçus"
    Et je presse "Confirmer"
    Alors je devrais voir "Généré"

    Exemples:
      | mail                            |
      | admin.aura@test.amap-aura.org   |
      | admin.rhone@test.amap-aura.org  |

  @database
  Plan du Scénario: Suppression des reçus
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Et que je suis sur "/adhesion/import"
    Et que j'attache le fichier "Import_adhesion_amapien_success.xls" à "import_amapien"
    Et que je presse "import_amapien"
    Quand je suis sur "/adhesion/amapien"
    Alors je devrais voir "<mail>"
    Et Je clique sur l'élément ".js-select-all"
    Et je presse "Supprimer les reçus"
    Et je presse "Confirmer"
    Alors je devrais voir "1 adhésions supprimées avec succès"
    Et je ne devrais pas voir "<mail>"

    Exemples:
      | mail                            |
      | admin.aura@test.amap-aura.org   |
      | admin.rhone@test.amap-aura.org  |
