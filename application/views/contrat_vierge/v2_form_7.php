<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
?>

<h3>Article complémentaire au contrat juridique</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Création
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_2'); ?>">Étape 2</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_3'); ?>">Étape 3</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_4'); ?>">Étape 4</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_5'); ?>">Étape 5</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_6'); ?>">Étape 6</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 7
</h5>


<form method="POST" id="form-reglements" action="">

    <div class="well well-sm">

        <div class="row">
            <div class="col-xs-12">
                <?php $modeleContrat = $contrat;

                include __DIR__.'/../contrat_signe/_info_modele_contrat.php'; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-info">
                    Cette étape vous permet de rajouter un article spécifique au contrat tel que "les dates de livraison sont indicatives" (exemple pour les fruits d’été). Toutefois, un contrat en AMAP ne peut pas être sous à condition d'être à jour de sa cotisation pour le paysan et ou l'amapien.
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label>Article  : Autres éléments au contrat</label>
                    <?= render_form_textarea_widget($form['contratAnnexes']); ?>
                </div>

            </div>

            <div class="col-xs-12">
                <p><b>Vous pouvez consulter le contrat juridique (format PDF) après sauvegarde. Il restera modifiable jusqu'à votre validation.</b></p>
            </div>

            <div class="col-xs-12">
                <input type="submit" value="Sauvegarder" class="pull-right btn btn-success" style="margin-left:5px;"/>
                <a href="<?= site_url('/contrat_vierge/v2_contrat_form_6'); ?>" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
                <a href="<?= site_url('contrat_vierge'); ?>" class="pull-right btn btn-default">Annuler</a>
            </div>
        </div>


    </div>
</form>

