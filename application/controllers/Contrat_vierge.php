<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;
use DI\Annotation\Inject;
use PsrLib\Services\Security\SecurityChecker;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Contrat_vierge.
 */
class Contrat_vierge extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\MPdfGeneration
     */
    public $mpdfgeneration;
    protected $data;

    /**
     * @Inject
     *
     * @var \Symfony\Component\Serializer\SerializerInterface
     */
    private $serializer;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\EntityBuilder\Contrat_signe_cellule_model_builder
     */
    private $contrat_signe_cellule_model_builder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\CommandeValidation
     */
    private $commandevalidation;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Email_Sender
     */
    private $email_sender;

    /**
     * Contrat_vierge constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['create' => null,
            'ferme_vide' => null,
            'liv_lieu_vide' => null,
            'alerte_f_ref_mc_min_1' => false,
            'alerte_f_ref_mc_yet' => false,
            'edit' => false,
            'add' => false,
            'remove' => false,
            'vide' => null,
            'test' => null,
            'reseau_all' => null,
            'mc_id' => null,
            'edit_all' => null,
            'mc' => null,
            'modeles_contrat' => null,
            'date_premiere_livraison' => null,
            'date_derniere_livraison' => null,
            'dates_livraison' => [],
            'dates_livraison_generees' => [],
            'autre' => false,
            'produits' => [],
            'produits_ids' => [],
            'produits_prix' => [],
            'frequence_articles' => null,
            'choix_identiques' => null,
            'livraisons' => [], ];

        $this->load->helper('array');
        $this->load->helper('date');

        $this->load->library('form_validation');

        $this
            ->form_validation
            ->set_error_delimiters(
                '<div class="alert alert-danger">',
                '</div>'
            )
        ;
    }

    public function index()
    {
        if ($this->securitychecker->isGranted(SecurityChecker::ACTION_CONTRACT_MODEL_LIST_VALIDATE)) {
            $this->list_to_validate();

            return;
        }

        $this->accueil();
    }

    /**
     * Accueil.
     */
    public function accueil()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_LIST);

        $this->redirectFromStoredGetParamIfExist('contrat_vierge');

        $currentUser = $this->getUser();

        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
            $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratViergeRefProduitType::class, null, [
                'refProduit' => $currentUser,
            ]);
        } elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
            $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratViergeAmapType::class, null, [
                'amap_choices' => [$currentUser],
            ]);
        } else {
            $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratViergeType::class, null, [
                'currentUser' => $currentUser,
            ]);
        }
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        if ($searchForm->has('amapFerme')) {
            $amapFermeField = $searchForm->get('amapFerme');
            $fermeSelected = $amapFermeField->get('ferme')->getData();
            $amapSelected = $amapFermeField->get('amap')->getData();
        } else {
            $fermeSelected = $searchForm->get('ferme')->getData();
            $amapSelected = $searchForm->get('amap')->getData();
        }

        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->search($searchForm->getData())
        ;

        // filter ref prod contracts
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit()) {
            $contrats = array_filter($contrats, function (PsrLib\ORM\Entity\ModeleContrat $mc) use ($currentUser) {
                return $currentUser->getRefProdFermes()->contains($mc->getFerme());
            });
        }

        $this->loadViewWithTemplate('contrat_vierge/display_all', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'fermeSelected' => $fermeSelected,
            'amapSelected' => $amapSelected,
        ]);
    }

    /**
     * Supprimer le contrat vierge.
     *
     * @param $mc_id
     */
    public function remove($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_REMOVE, $mc);

        $this->em->remove($mc);
        $this->em->flush();

        return redirect('/contrat_vierge');
    }

    /**
     * @param $mc_id
     */
    public function v2_state_to_validate($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_EDIT, $mc);

        $mc->setEtat(\PsrLib\ORM\Entity\ModeleContrat::ETAT_VALIDATION_PAYSAN);
        $this->em->flush();

        $this->email_sender->envoyerModelContratValidationPaysan($mc, $this->getUser());

        return redirect('/contrat_vierge');
    }

    /**
     * @param $contrat_id
     */
    public function v2_state_paysan_approval($contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_VALIDATE, $mc);

        $this->form_validation->set_rules('confirm-sign', 'Confirmation de signature', 'required');
        $this->form_validation->set_rules('confirm-charter', 'Confirmation de la charte', 'required');

        if ($this->form_validation->run()) {
            $mc->setEtat(\PsrLib\ORM\Entity\ModeleContrat::ETAT_VALIDE);

            $this->em->flush();

            $this->email_sender->envoyerModelContratValide($mc, $this->getUser());

            return redirect('contrat_vierge/');
        }

        $this->loadViewWithTemplate('contrat_vierge/paysan_approval', [
            'contrat' => $mc,
        ]);
    }

    /**
     * @param $contrat_id
     */
    public function v2_state_paysan_refuse($contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_VALIDATE, $mc);

        $mc->setEtat(\PsrLib\ORM\Entity\ModeleContrat::ETAT_REFUS);
        $this->em->flush();

        $motif = $this->input->post('refus');
        $this->email_sender->envoyerModelContratRefus($mc, $this->getUser(), $motif);

        return redirect('contrat_vierge/');
    }

    /**
     * @param $contrat_id
     */
    public function v2_preview_pdf($contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_PREVIEW_PDF, $mc);

        $this->mpdfgeneration->genererContratVierge($mc);
    }

    /**
     * @param $contrat_id
     */
    public function v2_simulation($contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_EDIT, $contrat);

        // Add validation rules for inputs
        foreach ($contrat->getDates() as $date) {
            foreach ($contrat->getProduits() as $produit) {
                $inputName = 'commande['.$date->getId().']['.$produit->getId().']';
                $this->form_validation->set_rules($inputName, 'Commande', 'numeric|greater_than_equal_to[0]');
            }
        }

        $validation = null;
        if ($this->form_validation->run()) {
            $commande = $this->contrat_signe_cellule_model_builder->buildFromInput(
                $this->input->post('commande') ?? []
            );

            $validation = $this->commandevalidation->validate($commande, $contrat, false);
        }

        $this->loadViewWithTemplate('contrat_vierge/simulation', [
            'contrat' => $contrat,
            'validation_error' => $validation,
        ]);
    }

    public function v2_contrat_edit($contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_EDIT, $contrat);
        $this->v2_store_data_to_session($contrat);

        return redirect('/contrat_vierge/v2_contrat_form_1');
    }

    /**
     * Première étape de la création d'un nouveau contrat.
     *
     * @param $ferme_id
     * @param $amap_id
     * @param $contrat_id
     */
    public function v2_contrat_form_1($ferme_id = null, $amap_id = null)
    {
        // Get data from argument or from session
        if (null === $ferme_id || null === $amap_id) {
            /** @var \PsrLib\ORM\Entity\ModeleContrat $contrat | false */
            $contrat = $this->v2_get_data_from_session();
            if (false === $contrat) {
                redirect('/contrat_vierge');
            }
            $amap = $contrat->getLivraisonLieu()->getAmap();
            $defaultll = $contrat->getLivraisonLieu();
        } else {
            $contrat = new \PsrLib\ORM\Entity\ModeleContrat();
            /** @var \PsrLib\ORM\Entity\Amap $amap */
            $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
            /** @var \PsrLib\ORM\Entity\Ferme $ferme */
            $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

            $contrat->setFerme($ferme);
            $contrat->setLivraisonLieu($amap->getLivraisonLieux()->first());
            $defaultll = 1 === $amap->getLivraisonLieux()->count() ? $amap->getLivraisonLieux()->first() : null;
        }

        $this->denyAccessUnlessGranted(SecurityChecker::ACTION_CONTRACT_MODEL_EDIT, $contrat);

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm1Type::class, $contrat, [
            'amap' => $amap,
            'defaultLl' => $defaultll,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->v2_store_data_to_session($contrat);

            return redirect('/contrat_vierge/v2_contrat_form_2');
        }

        $this->loadViewWithTemplate('contrat_vierge/v2_form_1', [
            'contrat' => $contrat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Seconde étape de la création d'un nouveau contrat.
     *
     * @param $ferme_id
     * @param $amap_id
     * @param $contrat_id
     */
    public function v2_contrat_form_2()
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return redirect('/contrat_vierge');
        }

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm2Type::class, $contrat);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->v2_store_data_to_session($contrat);

            return redirect('/contrat_vierge/v2_contrat_form_3');
        }

        $this->loadViewWithTemplate('contrat_vierge/v2_form_2', [
            'contrat' => $contrat,
            'form' => $form->createView(),
            'amap_livraisons_horaires' => $contrat->getLivraisonLieu()->getLivraisonHoraires()->toArray(),
        ]);
    }

    /**
     * Troisième étape de la création d'un nouveau contrat.
     *
     * @param $ferme_id
     * @param $amap_id
     * @param $contrat_id
     */
    public function v2_contrat_form_3()
    {
        /**
         * @param $inputData
         * @param \PsrLib\ORM\Entity\FermeProduit[] $fermeProduits
         *
         * @return array
         */
        function generateProductsFromInput($inputData, array $fermeProduits)
        {
            $inputLineIds = array_keys($inputData['prod']);
            $produits = [];
            foreach ($inputLineIds as $i) {
                $newProduit = new \PsrLib\ORM\Entity\ModeleContratProduit();
                $newProduit->setPrix($inputData['prix'][$i]);
                if (array_key_exists('regul', $inputData) && array_key_exists($i, $inputData['regul'])) {
                    $newProduit->setRegulPds(true);
                } else {
                    $newProduit->setRegulPds(false);
                }

                $newProduitId = $inputData['prod'][$i];
                /** @var \PsrLib\ORM\Entity\FermeProduit $fermeProduit */
                $fermeProduit = array_values(array_filter($fermeProduits, function (PsrLib\ORM\Entity\FermeProduit $produit) use ($newProduitId) {
                    return $produit->getId() === (int) $newProduitId;
                }))[0];
                $newProduit->setTypeProduction($fermeProduit->getTypeProduction());
                $newProduit->setNom($fermeProduit->getNom());
                $newProduit->setConditionnement($fermeProduit->getConditionnement());
                $newProduit->setFermeProduit($fermeProduit);

                $produits[] = $newProduit;
            }

            return $produits;
        }

        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return redirect('/contrat_vierge');
        }

        /** @var \PsrLib\ORM\Entity\FermeProduit[] $fermeProduits */
        $fermeProduits = $contrat
            ->getFerme()
            ->getProduits()
            ->toArray()
        ;
        usort($fermeProduits, function (PsrLib\ORM\Entity\FermeProduit $p1, PsrLib\ORM\Entity\FermeProduit $p2) {
            $catComparaison = strnatcasecmp($p1->getTypeProduction()->getNom(), $p2->getTypeProduction()->getNom());
            if (0 !== $catComparaison) {
                return $catComparaison;
            }

            return strnatcasecmp($p1->getNom(), $p2->getNom());
        });
        $fermeProduitIds = array_map(function (PsrLib\ORM\Entity\FermeProduit $fermeProduit) {
            return $fermeProduit->getId();
        }, $fermeProduits);

        $this
            ->form_validation
            ->set_rules('prix[]', 'Prix', 'required|greater_than_equal_to[0]')
        ;
        $this
            ->form_validation
            ->set_rules('prod[]', 'Produit', 'required|in_list['.implode(',', $fermeProduitIds).']|no_duplication['.implode(',', $this->input->post('prod') ?? []).']', [
                'no_duplication' => 'Impossible d\'indiquer plusieurs fois le même produit',
            ])
        ;

        if ($this->form_validation->run()) {
            $inputData = $this->input->post(null);
            $produits = generateProductsFromInput($inputData, $fermeProduits);

            $contrat->getProduits()->clear();
            foreach ($produits as $produit) {
                $contrat->addProduit($produit);
            }

            $this->v2_store_data_to_session($contrat);

            return redirect('/contrat_vierge/v2_contrat_form_4');
        }

        $produits = $contrat->getProduits()->toArray();
        $inputData = $this->input->post(null);
        if (!empty($inputData)) { // Envoi échoué, on replace les produits envoyés
            $produits = generateProductsFromInput($inputData, $fermeProduits);
        } elseif (empty($produits)) {
            // By default init session data with all products
            foreach ($fermeProduits as $fermeProduit) {
                $newProduit = new \PsrLib\ORM\Entity\ModeleContratProduit();
                $newProduit->setPrix($fermeProduit->getPrix() ?? '');
                $newProduit->setTypeProduction($fermeProduit->getTypeProduction());
                $newProduit->setFermeProduit($fermeProduit);
                $newProduit->setRegulPds($fermeProduit->isRegulPds());
                $produits[] = $newProduit;
            }
        }

        $this->loadViewWithTemplate('contrat_vierge/v2_form_3', [
            'fermeProduits' => $fermeProduits,
            'produitsExistants' => $produits,
            'contrat' => $contrat,
        ]);
    }

    /**
     * Quatrième étape de la création d'un nouveau contrat.
     *
     * @param $ferme_id
     * @param $amap_id
     * @param $contrat_id
     */
    public function v2_contrat_form_4()
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return redirect('/contrat_vierge');
        }

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm4Type::class, $contrat);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->v2_store_data_to_session($contrat);

            return redirect('/contrat_vierge/v2_contrat_form_5');
        }

        $this->loadViewWithTemplate('contrat_vierge/v2_form_4', [
            'nbDatesContract' => $contrat->getDates()->count(),
            'ferme' => $contrat->getFerme(),
            'contrat' => $contrat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Cinquième étape de la création d'un nouveau contrat.
     *
     * @param $ferme_id
     * @param $amap_id
     * @param $contrat_id
     */
    public function v2_contrat_form_5()
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return redirect('/contrat_vierge');
        }

        // Ne pas afficher la fenetre si les livraisons sont identiques
        if (true === $contrat->getProduitsIdentiquePaysan()) {
            return redirect('/contrat_vierge/v2_contrat_form_6');
        }

        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $dates */
        $dates = $contrat->getDates()->toArray();
        /** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $produits */
        $produits = $contrat->getProduits()->toArray();

        $livraisonsExclues = [];
        foreach ($dates as $date) {
            $dateString = $date->getDateLivraison()->format('Y-m-d');
            $livraisonsExclues[$dateString] = [];
            foreach ($produits as $produit) {
                $livraisonsExclues[$dateString][$produit->getFermeProduit()->getId()] = $produit
                    ->getExclusions()
                    ->filter(function (PsrLib\ORM\Entity\ModeleContratProduitExclure $exclure) use ($date) {
                        return $exclure->getModeleContratDate() === $date;
                    })
                    ->first()
                ;
            }
        }

        $this
            ->form_validation
            ->set_rules('liv', 'Livraison', 'bool_check')
        ;

        if ($this->form_validation->run()) {
            $inputs = $this->input->post(null);

            // Clear old exclusions
            foreach ($produits as $produit) {
                $produit->getExclusions()->clear();
            }
            foreach ($dates as $date) {
                $date->getExclusions()->clear();
            }

            // Parsing du formulaire
            foreach ($dates as $date) {
                $dateString = $date->getDateLivraison()->format('Y-m-d');

                foreach ($produits as $produit) {
                    if (!isset($inputs['liv'][$dateString][$produit->getFermeProduit()->getId()])) {
                        $exclusion = new \PsrLib\ORM\Entity\ModeleContratProduitExclure();
                        $date->addExclusion($exclusion);
                        $produit->addExclusion($exclusion);
                    }
                }
            }

            $this->v2_store_data_to_session($contrat);

            return redirect('/contrat_vierge/v2_contrat_form_6');
        }

        $this->loadViewWithTemplate('contrat_vierge/v2_form_5', [
            'dates' => $dates,
            'produits' => $produits,
            'livraisons_exclues' => $livraisonsExclues,
            'contrat' => $contrat,
        ]);
    }

    public function v2_contrat_form_6()
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return redirect('/contrat_vierge');
        }

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm6Type::class, $contrat);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->v2_store_data_to_session($contrat);

            return redirect('/contrat_vierge/v2_contrat_form_7');
        }

        $this->loadViewWithTemplate('contrat_vierge/v2_form_6', [
            'contrat' => $contrat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function v2_contrat_form_7()
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return redirect('/contrat_vierge');
        }

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm7Type::class, $contrat);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            // remplace le contrat en BDD. Permis parce que seuls les contrats en cours de création peuvent etre modifiés
            if (null !== $contrat->getId()) {
                $this->em->remove($this->em->getReference(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat->getId()));
            }
            $this->em->persist($contrat);
            $this->em->flush();

            return redirect('contrat_vierge');
        }

        $this->loadViewWithTemplate('contrat_vierge/v2_form_7', [
            'contrat' => $contrat,
            'form' => $form->createView(),
        ]);
    }

    public function ajax_test_date_absence()
    {
        $amapId = (int) $this->input->get('amap_id');
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amapId);

        $this->denyAccessUnlessGranted(SecurityChecker::ACTION_AMAP_DISPLAY, $amap);

        try {
            $date = Carbon::createFromFormat('Y-m-d', $this->input->get('date'));
        } catch (\Exception $e) {
            echo json_encode(['result' => false]);

            return;
        }

        if (!$amap->getAmapEtudiante()) {
            echo json_encode(['result' => false]);

            return;
        }

        /** @var \PsrLib\ORM\Entity\AmapAbsence[] $absences */
        $absences = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapAbsence::class)
            ->findBy([
                'amap' => $amap,
            ])
        ;

        foreach ($absences as $absence) {
            if ($date->gte($absence->getAbsenceDebut()) && $date->lte($absence->getAbsenceFin())) {
                echo json_encode([
                    'result' => true,
                    'title' => $absence->getTitre(),
                ]);

                return;
            }
        }

        echo json_encode(['result' => false]);
    }

    private function list_to_validate()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_LIST_VALIDATE);

        /** @var \PsrLib\ORM\Entity\Paysan|\PsrLib\ORM\Entity\UserWithFermes $currentUser Granted by security */
        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(\PsrLib\Form\ContratViergeChooseAmapPaysan::class, null, [
            'current_user' => $currentUser,
        ]);
        $searchForm->handleRequest($this->request);

        $amap = null;
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $amap = $searchForm->get('amap')->getData();
        }

        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->findToValidateForUserWithFermes($currentUser, $amap)
        ;

        $this->loadViewWithTemplate('contrat_vierge/list_to_validate', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @return \PsrLib\ORM\Entity\ModeleContrat | false
     */
    private function v2_get_data_from_session()
    {
        try {
            $deserialized = $this
                ->serializer
                ->deserialize($this->sfSession->get('mc_wizard'), \PsrLib\ORM\Entity\ModeleContrat::class, 'json', [
                    'groups' => 'wizard',
                ])
            ;
        } catch (\Exception $e) {
            $deserialized = false;
        }

        return $deserialized;
    }

    private function v2_store_data_to_session(PsrLib\ORM\Entity\ModeleContrat $modeleContrat)
    {
        $this->sfSession->set(
            'mc_wizard',
            $this->serializer->serialize($modeleContrat, 'json', [
                'groups' => 'wizard',
            ])
        );
    }
}
