<?php

namespace PsrLib\Form;

use PsrLib\DTO\SearchDistributionAmapState;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDistributionAmapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('complete', CheckboxType::class, [
                'label' => 'Afficher également les distrib\' complètes',
            ])
        ;
    }

    public function getParent()
    {
        return SearchDistributionBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchDistributionAmapState::class,
        ]);
    }
}
