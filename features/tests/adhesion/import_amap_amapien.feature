# language: fr
Fonctionnalité: Import des reçus amapien

  @database
  Scénario: Import avec succès
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/import"
    Et que j'attache le fichier "Import_adhesion_amap_amapien_success.xls" à "amap_amapien_adhesions"
    Et que je presse "action_import_amap_amapien"
    Alors je devrais voir "Import validé"

  @database
  Scénario: Import avec erreur
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/import"
    Et que j'attache le fichier "Import_adhesion_amap_amapien_error.xls" à "amap_amapien_adhesions"
    Et que je presse "action_import_amap_amapien"
    Alors je devrais voir "Le mail email@exemple.com ne correspond pas à un amapien"
