<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\BaseUser;
use PsrLib\Services\AuthentificationMockable;
use PsrLib\Services\Security\MockableSecurityChecker;
use Test\ContainerAwareTestCase;

abstract class AbstractSecurityTest extends ContainerAwareTestCase
{
    /**
     * @var MockableSecurityChecker
     */
    protected $securityChecker;

    /**
     * @var AuthentificationMockable
     */
    protected $authentification;

    public function setUp()
    {
        parent::setUp();
        $this->securityChecker = self::$container->get(MockableSecurityChecker::class);
        $this->authentification = self::$container->get(AuthentificationMockable::class);
    }

    public function tearDown()
    {
        $this->authentification->mockUser();
        $this->securityChecker->cleanMocks();
    }

    protected function mockAuthenticatedUser(?BaseUser $user = null)
    {
        $this->authentification->mockUser($user);
    }
}
