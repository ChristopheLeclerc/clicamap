<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\Validator\Numeric;
use PsrLib\Validator\UniqueEntity;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\GroupSequenceProviderInterface;

/**
 * @ORM\Table(name="ak_ferme", uniqueConstraints={@ORM\UniqueConstraint(name="f_siret", columns={"f_siret"})})
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\FermeRepository")
 * @UniqueEntity(fields={"siret"}, message="Ce SIRET existe déjà dans la base de donnée.")
 * @ORM\HasLifecycleCallbacks
 * @Assert\GroupSequenceProvider
 */
class Ferme implements LocalisableEntity, GroupSequenceProviderInterface
{
    public const AVAILABLE_CERTIFICATIONS = [
        '' => null,
        'N&P' => 'np',
        'AB' => 'ab',
        'Partiellement AB' => 'ab_part',
        'En conversion' => 'conversion',
    ];

    /**
     * @var ?int
     *
     * @ORM\Column(name="f_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizard"})
     */
    private $id;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_nom", type="string", length=100, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(max="100")
     */
    private $nom;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_siret", type="string", length=14, nullable=true)
     * @Assert\Length(max="14", min="14", exactMessage="Le champ ""SIRET"" doit contenir exactement 14 caractères.")
     * @Assert\NotBlank(groups={"SansRegroupement"})
     */
    private $siret;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_description", type="text", length=65535, nullable=true)
     * @Assert\Length(max="65535")
     */
    private $description;

    /**
     * @var int | null
     *
     * @ORM\Column(name="f_delai_modif_contrat", type="integer", nullable=true)
     * @Assert\NotBlank
     * @Assert\GreaterThanOrEqual(0)
     */
    private $delaiModifContrat;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_lib_adr", type="string", length=255, nullable=true)
     */
    private $libAdr;

    /**
     * @var Ville | null
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Ville")
     * @ORM\JoinColumn(referencedColumnName="v_id")
     * @Assert\NotNull
     */
    private $ville;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_gps_latitude", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     * @Numeric()
     */
    private $gpsLatitude;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_gps_longitude", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     * @Numeric()
     */
    private $gpsLongitude;

    /**
     * @var bool
     *
     * @ORM\Column(name="f_tva", type="boolean")
     */
    private $tva = true;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_tva_taux", type="string", length=50, nullable=true)
     * @Assert\Length(max="50")
     */
    private $tvaTaux;

    /**
     * @var null|DateTime
     *
     * @ORM\Column(name="f_tva_date", type="date", nullable=true)
     */
    private $tvaDate;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_url", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $url;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_uuid", type="string", length=255, nullable=true)
     */
    private $uuid;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_surface", type="decimal", precision=30, scale=2, nullable=true)
     * @Assert\GreaterThanOrEqual(0)
     */
    private $surface;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_uta", type="decimal", precision=30, scale=2, nullable=true)
     * @Assert\GreaterThanOrEqual(0)
     */
    private $uta;

    /**
     * @var null|string
     *
     * @ORM\Column(name="f_certification", type="string", length=255, nullable=true)
     * @Assert\Choice(choices=Ferme::AVAILABLE_CERTIFICATIONS)
     */
    private $certification;

    /**
     * @var ArrayCollection<Paysan>
     * @ORM\OneToMany (targetEntity="PsrLib\ORM\Entity\Paysan", mappedBy="ferme")
     */
    private $paysans;

    /**
     * @var ArrayCollection<Amapien>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="refProdFermes")
     * @JoinTable(
     *     joinColumns={@JoinColumn(referencedColumnName="f_id")},
     *     inverseJoinColumns={@JoinColumn(referencedColumnName="a_id")}
     * )
     */
    private $amapienRefs;

    /**
     * @var ArrayCollection<FermeProduit>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\FermeProduit", mappedBy="ferme", cascade="remove")
     */
    private $produits;

    /**
     * @var ArrayCollection<FermeAnneeAdhesion>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\FermeAnneeAdhesion", mappedBy="ferme",  cascade="PERSIST", orphanRemoval=true)
     */
    private $anneeAdhesions;

    /**
     * @var ArrayCollection<FermeTypeProductionPropose>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\FermeTypeProductionPropose", mappedBy="ferme")
     */
    private $typeProductionProposes;

    /**
     * @var ArrayCollection<ModeleContrat>
     * @ORM\OneToMany(targetEntity="PsrLib\ORM\Entity\ModeleContrat", mappedBy="ferme")
     */
    private $modeleContrats;

    /**
     * @var ArrayCollection<Amapien>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Amapien", mappedBy="fermeAttentes")
     * @JoinTable(
     *     name="ak_ferme_amapien_liste_attente",
     *     joinColumns={@JoinColumn(name="f_a_la_fk_ferme_idf_a_la_fk_amapien_id", referencedColumnName="f_id")},
     *     inverseJoinColumns={@JoinColumn(name="f_a_la_fk_amapien_id", referencedColumnName="a_id")}
     * )
     */
    private $amapienAttentes;

    /**
     * @var null|FermeRegroupement
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\FermeRegroupement", inversedBy="fermes")
     */
    private $regroupement;

    public function __construct()
    {
        $this->paysans = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->anneeAdhesions = new ArrayCollection();
        $this->typeProductionProposes = new ArrayCollection();
        $this->modeleContrats = new ArrayCollection();
        $this->amapienAttentes = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
    }

    public function __toString()
    {
        return (string) $this->getNom();
    }

    public function getGroupSequence()
    {
        $groups = ['Ferme'];
        if ($this->inRegroupement()) {
            $groups[] = 'Regroupement';
        } else {
            $groups[] = 'SansRegroupement';
        }

        return [$groups];
    }

    public function inRegroupement(): bool
    {
        return null !== $this->regroupement;
    }

    /**
     * @return TypeProduction[]
     */
    public function getTypeProductionFromProduits()
    {
        $tpps = [];
        /** @var FermeProduit $produit */
        foreach ($this->getProduits() as $produit) {
            $tpps[] = $produit->getTypeProduction();
        }

        return array_unique($tpps);
    }

    public function getPremiereDateLivrableAvecDelais(DateTime $date): Carbon
    {
        $dateCarbon = Carbon::instance($date)->startOfDay();
        $delais = $this->getDelaiModifContrat();
        if (null === $delais || $delais <= 0) {
            $delais = 0;
        }

        return $dateCarbon->addDays($delais);
    }

    /**
     * @ORM\PreRemove
     */
    public function dissociatePaysanOnRemove(): void
    {
        /** @var Paysan $paysan */
        foreach ($this->getPaysans() as $paysan) {
            $paysan->setFerme(null);
        }
    }

    public function getDepartement(): ?Departement
    {
        return $this->getVille()->getDepartement();
    }

    public function getRegion(): ?Region
    {
        return $this->getVille()->getDepartement()->getRegion();
    }

    /**
     * @return int | null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Ferme
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): Ferme
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Add paysan.
     *
     * @return Ferme
     */
    public function addPaysan(Paysan $paysan)
    {
        $this->paysans[] = $paysan;

        return $this;
    }

    /**
     * Remove paysan.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removePaysan(Paysan $paysan)
    {
        return $this->paysans->removeElement($paysan);
    }

    /**
     * Get paysans.
     *
     * @return Collection
     */
    public function getPaysans()
    {
        return $this->paysans;
    }

    /**
     * Add amapienRef.
     *
     * @return Ferme
     */
    public function addAmapienRef(Amapien $amapienRef)
    {
        $this->amapienRefs[] = $amapienRef;

        return $this;
    }

    /**
     * Remove amapienRef.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAmapienRef(Amapien $amapienRef)
    {
        return $this->amapienRefs->removeElement($amapienRef);
    }

    /**
     * Get amapienRefs.
     *
     * @return Collection
     */
    public function getAmapienRefs()
    {
        return $this->amapienRefs;
    }

    /**
     * Add produit.
     *
     * @return Ferme
     */
    public function addProduit(FermeProduit $produit)
    {
        $this->produits[] = $produit;

        return $this;
    }

    /**
     * Remove produit.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeProduit(FermeProduit $produit)
    {
        return $this->produits->removeElement($produit);
    }

    /**
     * Get produits.
     *
     * @return Collection
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Add anneeAdhesion.
     *
     * @return Ferme
     */
    public function addAnneeAdhesion(FermeAnneeAdhesion $anneeAdhesion)
    {
        $this->anneeAdhesions[] = $anneeAdhesion;

        return $this;
    }

    /**
     * Remove anneeAdhesion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAnneeAdhesion(FermeAnneeAdhesion $anneeAdhesion)
    {
        return $this->anneeAdhesions->removeElement($anneeAdhesion);
    }

    /**
     * Get anneeAdhesions.
     *
     * @return Collection
     */
    public function getAnneeAdhesions()
    {
        return $this->anneeAdhesions;
    }

    /**
     * @return FermeAnneeAdhesion[]
     */
    public function getAnneeAdhesionsOrdered()
    {
        $adhesions = $this->anneeAdhesions->toArray();
        usort($adhesions, function (FermeAnneeAdhesion $a, FermeAnneeAdhesion $b) {
            return $b->getAnnee() - $a->getAnnee();
        });

        return $adhesions;
    }

    /**
     * @return Ville | null
     */
    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    /**
     * @param Ville | null $ville
     */
    public function setVille(?Ville $ville): Ferme
    {
        $this->ville = $ville;

        return $this;
    }

    public function getGpsLatitude(): ?string
    {
        return $this->gpsLatitude;
    }

    public function setGpsLatitude(?string $gpsLatitude): Ferme
    {
        $this->gpsLatitude = $gpsLatitude;

        return $this;
    }

    public function getGpsLongitude(): ?string
    {
        return $this->gpsLongitude;
    }

    public function setGpsLongitude(?string $gpsLongitude): Ferme
    {
        $this->gpsLongitude = $gpsLongitude;

        return $this;
    }

    public function getLibAdr(): ?string
    {
        return $this->libAdr;
    }

    public function setLibAdr(?string $libAdr): Ferme
    {
        $this->libAdr = $libAdr;

        return $this;
    }

    public function getDelaiModifContrat(): ?int
    {
        return $this->delaiModifContrat;
    }

    public function setDelaiModifContrat(?int $delaiModifContrat): Ferme
    {
        $this->delaiModifContrat = $delaiModifContrat;

        return $this;
    }

    public function getSurface(): ?string
    {
        return $this->surface;
    }

    public function setSurface(?string $surface): Ferme
    {
        $this->surface = $surface;

        return $this;
    }

    public function getUta(): ?string
    {
        return $this->uta;
    }

    public function setUta(?string $uta): Ferme
    {
        $this->uta = $uta;

        return $this;
    }

    public function getCertification(): ?string
    {
        return $this->certification;
    }

    public function setCertification(?string $certification): Ferme
    {
        $this->certification = $certification;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Ferme
    {
        $this->description = $description;

        return $this;
    }

    public function isTva(): bool
    {
        return $this->tva;
    }

    public function setTva(bool $tva): Ferme
    {
        $this->tva = $tva;

        return $this;
    }

    public function getTvaTaux(): ?string
    {
        return $this->tvaTaux;
    }

    public function setTvaTaux(?string $tvaTaux): Ferme
    {
        $this->tvaTaux = $tvaTaux;

        return $this;
    }

    public function getTvaDate(): ?DateTime
    {
        return $this->tvaDate;
    }

    public function setTvaDate(?DateTime $tvaDate): Ferme
    {
        $this->tvaDate = $tvaDate;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Ferme
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Add typeProductionPropose.
     *
     * @return Ferme
     */
    public function addTypeProductionPropose(FermeTypeProductionPropose $typeProductionPropose)
    {
        $this->typeProductionProposes[] = $typeProductionPropose;

        return $this;
    }

    /**
     * Remove typeProductionPropose.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTypeProductionPropose(FermeTypeProductionPropose $typeProductionPropose)
    {
        return $this->typeProductionProposes->removeElement($typeProductionPropose);
    }

    /**
     * Get typeProductionProposes.
     *
     * @return Collection
     */
    public function getTypeProductionProposes()
    {
        return $this->typeProductionProposes;
    }

    /**
     * Add modeleContrat.
     *
     * @return Ferme
     */
    public function addModeleContrat(ModeleContrat $modeleContrat)
    {
        $this->modeleContrats[] = $modeleContrat;

        return $this;
    }

    /**
     * Remove modeleContrat.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModeleContrat(ModeleContrat $modeleContrat)
    {
        return $this->modeleContrats->removeElement($modeleContrat);
    }

    /**
     * Get modeleContrats.
     *
     * @return Collection
     */
    public function getModeleContrats()
    {
        return $this->modeleContrats;
    }

    /**
     * Add fermeAttente.
     *
     * @return Ferme
     */
    public function addAmapienAttente(Amapien $amapienAttente)
    {
        $this->amapienAttentes[] = $amapienAttente;

        return $this;
    }

    /**
     * Remove amapienAttente.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAmapienAttente(Amapien $amapienAttente)
    {
        return $this->amapienAttentes->removeElement($amapienAttente);
    }

    /**
     * Get amapienAttentes.
     *
     * @return Collection
     */
    public function getAmapienAttentes()
    {
        return $this->amapienAttentes;
    }

    /**
     * @return null|string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function getRegroupement(): ?FermeRegroupement
    {
        return $this->regroupement;
    }

    public function setRegroupement(?FermeRegroupement $regroupement): Ferme
    {
        $this->regroupement = $regroupement;

        return $this;
    }
}
