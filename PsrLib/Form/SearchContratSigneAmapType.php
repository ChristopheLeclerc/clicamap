<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Repository\ModeleContratRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchContratSigneAmapType extends AbstractType
{
    /**
     * @var ModeleContratRepository
     */
    private $modeleContratRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->modeleContratRepo = $em
            ->getRepository(ModeleContrat::class)
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Amap $amap */
        $amap = $options['amap_choices'][0];
        $builder
            ->add('mc', ChoiceType::class, [
                'disabled' => true,
            ])
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($amap) {
                $form = $event->getForm();
                $data = $event->getData();

                $fermeId = $data['ferme'] ?? '';

                if ('' === $fermeId) {
                    return;
                }

                /** @var AmapFermeType $parentForm */
                $parentForm = $form
                    ->getConfig()
                    ->getType()
                    ->getParent()
                    ->getInnerType()
                ;

                $fermesChoices = $parentForm
                    ->getFermeFromAmapCached($amap)
                ;
                $ferme = null;
                foreach ($fermesChoices as $fermeChoice) {
                    if ($fermeChoice->getId() === (int) $fermeId) {
                        $ferme = $fermeChoice;
                    }
                }
                if (null === $ferme) {
                    return;
                }

                $choices = $this->modeleContratRepo->findValidatedByFermeAmap($ferme, $amap);
                $form
                    ->add('mc', ChoiceType::class, [
                        'choices' => $choices,
                        'choice_label' => function (?ModeleContrat $modeleContrat) {
                            return null === $modeleContrat ? null : $modeleContrat->getNom();
                        },
                        'choice_value' => function (?ModeleContrat $modeleContrat) {
                            return null === $modeleContrat ? null : $modeleContrat->getId();
                        },
                        'placeholder' => true,
                        'required' => false,
                    ])
                ;
            })
        ;
    }

    public function getParent()
    {
        return AmapFermeType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap_choices' => [],
            'method' => 'GET',
            'data_class' => SearchContratSigneState::class,
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
            'empty_data' => function (FormInterface $form) {
                $state = new SearchContratSigneState();
                $state->setAmap($form->get('amap')->getData());

                return $state;
            },
        ]);
    }
}
