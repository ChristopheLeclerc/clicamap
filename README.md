# POUR LES DÉVELOPPEURS

## 1. Technologies utilisées
- PHP : >= 7.2
- CodeIgniter : 3
- Base de données MariaDB : 10.1
- Framework Bootstrap : 3.3
- Behat : 3.5

## 2. Lancement de ClicAMAP en local
ClicAMAP propose un environnement de développement intégré, basé sur Docker. Il nécessite l'installation préalable de Docker (https://docs.docker.com/) & de Docker compose (https://docs.docker.com/compose/install/). Celui-ci peut être lancé simplement avec la commande `docker-compose up -d` depuis le sous répertoire `docker`.

Des instructions de lancement plus complètes notamment pour les tests sont disponibles dans le wiki à l'adresse https://gitlab.com/reseau-amap/clicamap/-/wikis/home.

## 3. Architecture de ClicAMAP

Voici la structure des différents dossiers de ClicAMAP
- `/application` Code applicatif lié à CodeIgniter. Attention : n'utilise pas PSR-4 pour l'instanciation mais des procédures propres à CodeIgniter. Il n'y a donc pas de namespaces ni d'autoloading.
  - `/config` Configuration générale
  - `/controllers` Controlleurs
  - `/core` Surcharge des composants du core de CodeIgniter. Ici le controller de base qui est utilisé dans tous les controleurs applicatifs
  - `/helpers` Déclaration de fonctions utilisées principalement dans les vues 
  - `/hooks` Hook sur certains évènements du cycle de vie de la requete dans CodeIgniter
  - `/libraries` Bibliothèques applicatives, fortement liées à CodeIgniter
  - `/logs` Logs systèmes
  - `/views` Vues utilisant le système de template interne de CodeIgniter
  - `/writable` Répertoire temporaire utilisé pour divers caches
- `docker` Composants dockers utilisés pour le lancement de l'environnement de développement local
- `features` Tests fonctionnels utilisant `Behat` ainsi que la configuration de ce dernier et des utilitaires pour faciliter leur usage
- `PsrLib` Ensemble de composants utilisant PSR-4.
  - `Annotation` Annotations utilisée dans les entités
  - `DTO` Data Transfert Objects utilisés pour structurer le code dans certains cas d'usages
  - `Exceptions` Exceptions applicatives
  - `Form` Formulaires utilisant le composant `Symfony Form`
  - `Migrations` Migrations utilisant le composant `Doctrine Migrations`
  - `ORM` Code relatif à `Doctrine`
  - `Serializer` Serializers compatibles avec `Symfony Serializer`
  - `Services` Services applicatifs
  - `Twig` Extensions twig
  - `Validator` Validateurs compatibles avec `Symfony Validator`
- `public` Répertoire d'accès publique pour les serveurs web
- `system` Code principal de CodeIgniter
- `templates` Vues utilisant le système de template Twig
- `tests` tests unitaires via PHPUnit

## 4. Documentation complémentaire
Documentation Bootstrap :
- https://getbootstrap.com/docs/3.3/

Tutoriels Codeigniter :
- https://www.codeigniter.com/userguide3/
- https://openclassrooms.com/courses/codeigniter-le-framework-au-service-des-zeros/installation-et-configuration

