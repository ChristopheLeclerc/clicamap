<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
?>

<h5><a href="<?= site_url('/portail'); ?>">Accueil</a> <i class="glyphicon glyphicon-menu-right"></i> Nouveau mot de passe </h5>

<h3>Nouveau mot de passe</h3>

<h4>Utilisez les champs ci-dessous pour créer un nouveau mot de passe. Veuillez confirmer pour qu'il soit bien pris en compte</h4>

<?= form_open('', ['class' => 'form-horizontal']); ?>
<div class="form-group">
        <label for="password_1" class="col-sm-3 control-label">Mot de passe *</label>
        <div class="col-sm-9">
            <?= form_password('password_1', set_value('password_1'), ['class' => 'form-control']); ?>
        </div>
    </div>
    <?= form_error('password_1'); ?>

    <div class="form-group">
        <label for="lostpassword_email" class="col-sm-3 control-label">Confirmez le mot de passe *</label>
        <div class="col-sm-9">
            <?= form_password('password_2', set_value('password_2'), ['class' => 'form-control']); ?>
        </div>
    </div>
    <?= form_error('password_2'); ?>

    <div class="form-group">
        <label class="col-sm-3 control-label">Force du mot de passe</label>
        <div class="col-sm-9">
            <?= render_zxcvbn_progress_bar('password_1', site_url('/amap/mot_de_passe_test/'.$amap->getId())); ?>
            <p class="help-block">Le mot de passe doit avoir au moins une force de 2 sur 4 et au moins 8 caractères</p>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right">Modifier le mot de passe</button>
        </div>
    </div>

<?= form_close(); ?>
