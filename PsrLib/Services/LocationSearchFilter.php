<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Assert\Assertion;
use CI_Input;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Repository\RegionRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class LocationSearchFilter
{
    /**
     * @var Authentification
     */
    private $authentification;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var CI_Input
     */
    private $input;

    public function __construct(Authentification $authentification, EntityManagerInterface $em, Session $session, CI_Input $input)
    {
        $this->authentification = $authentification;
        $this->em = $em;
        $this->session = $session;
        $this->input = $input;
    }

    public function getCurrentLocationSearch(string $searchRegionField, string $searchDepartementField): array
    {
        /** @var Amapien $currentUser */
        $currentUser = $this->authentification->getCurrentUser();
        Assertion::isInstanceOf($currentUser, Amapien::class);

        /** @var RegionRepository $regionRepository */
        $regionRepository = $this->em->getRepository(Region::class);
        $regions = $currentUser->getAdminRegions()->toArray();
        if ($this->session->get('sup_adm')) {
            $regions = $regionRepository->findAllOrdered();
        }
        if (1 === count($regions)) {
            $selectedRegion = $regions[0];
        } else {
            $regionFiltered = array_filter($regions, function (Region $region) use ($searchRegionField) {
                return $region->getId() === (int) $this->input->get($searchRegionField);
            });
            $selectedRegion = array_shift($regionFiltered);
        }

        if ($currentUser->isAdminDepartment()) {
            $departments = $currentUser->getAdminDepartments()->toArray();
            $selectedRegion = $departments[0]->getRegion();
            $regions = [$selectedRegion];
        } else {
            $departments = $this->em->getRepository(Departement::class)->findByRegion($selectedRegion);
        }

        if (1 === count($departments)) {
            $selectedDepartment = $departments[0];
        } else {
            $departmentFiltered = array_filter($departments, function (Departement $departement) use ($searchDepartementField) {
                return (int) $departement->getId() === (int) $this->input->get($searchDepartementField);
            });
            $selectedDepartment = array_shift($departmentFiltered);
        }

        return [$regions, $departments, $selectedRegion, $selectedDepartment];
    }
}
