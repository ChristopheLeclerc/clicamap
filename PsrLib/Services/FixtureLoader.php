<?php

namespace PsrLib\Services;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Embeddable;
use Nelmio\Alice\Loader\NativeLoader;
use PsrLib\ProjectLocation;

class FixtureLoader
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ORMPurger
     */
    private $purger;

    /**
     * @var NativeLoader
     */
    private $fixtureLoader;

    /**
     * @var AnnotationReader
     */
    private $annotationReader;

    public function __construct(EntityManagerInterface $em, CustomAliceNativeLoader $nativeLoader, AnnotationReader $annotationReader)
    {
        $this->em = $em;
        $this->fixtureLoader = $nativeLoader;
        $this->annotationReader = $annotationReader;

        $this->purger = new ORMPurger($em, [
            // Imported on startup via raw SQL requests. See docker/data.sql
            'ak_region',
            'ak_departement',
            'ak_ville',
            'ak_type_production',
        ]);
        $this->purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
    }

    public function loadFixtures(): void
    {
        $this->purgeDb();

        $fixtures = $this->fixtureLoader->loadFile(ProjectLocation::PROJECT_ROOT.'/fixtures/all.yml');
        foreach ($fixtures->getObjects() as $object) {
            $objectClass = get_class($object);
            if ( // Test entity persistable
                str_contains($objectClass, 'PsrLib\ORM\Entity')
                && !$this->is_class_embedable($objectClass)
            ) {
                $this->em->persist($object);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    private function is_class_embedable(string $class): bool
    {
        return null !== $this->annotationReader->getClassAnnotation(
            new \ReflectionClass($class),
            Embeddable::class
        );
    }

    private function purgeDb(): void
    {
        // Disable foreign key check on purge. @ref Fidry\AliceDataFixtures\Bridge\Doctrine\Purger::purge
        $connection = $this->em->getConnection();
        $connection->exec('SET FOREIGN_KEY_CHECKS = 0;');
        $this->purger->purge();
        $connection->exec('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
