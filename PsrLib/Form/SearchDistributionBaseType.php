<?php

namespace PsrLib\Form;

use PsrLib\DTO\SearchDistributionBaseState;
use PsrLib\Form\Type\PeriodFlatpickrType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDistributionBaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var null|Amap $amap */
        $amap = $options['amap'];
        if (null === $amap) {
            throw new \LogicException('Amap cannot be null');
        }

        $lls = $amap->getLivraisonLieux()->toArray();

        $builder
            ->add('livLieu', ChoiceType::class, [
                'label' => 'Lieu de livraison',
                'empty_data' => 1 === count($lls) ? (string) $lls[0]->getId() : null,
                'choices' => $lls,
                'required' => false,
                'placeholder' => count($lls) > 1 ? '' : false,
                'choice_label' => function (?AmapLivraisonLieu $ll) {
                    return null === $ll ? '' : $ll->getNom();
                },
                'choice_value' => function (?AmapLivraisonLieu $ll) {
                    return null === $ll ? '' : $ll->getId();
                },
            ])
            ->add('period', PeriodFlatpickrType::class, [
                'label' => 'Période',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchDistributionBaseState::class,
            'amap' => null,
            'method' => 'GET',
            'attr' => ['class' => 'js-form-search'],
        ]);
    }
}
