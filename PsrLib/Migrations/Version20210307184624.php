<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        // Fix database
        $this
            ->connection
            ->executeQuery('UPDATE ak_amap_livraison_lieu
            SET amap_liv_lieu_ville=\'MAGNY-LE-HONGRE\'
            , amap_liv_lieu_cp = \'77700\'
            , amap_liv_lieu_gps_latitude=\'48.8631156\'
            , amap_liv_lieu_gps_longitude=\'2.8135257\'
            WHERE amap_liv_lieu_id=547
        ')
        ;
        $this->connection->executeQuery('UPDATE ak_amap_livraison_lieu SET amap_liv_lieu = \'\' WHERE amap_liv_lieu IS NULL');
        $this->connection->executeQuery('UPDATE ak_amap_livraison_lieu SET amap_liv_lieu_lib_adr = \'\' WHERE amap_liv_lieu_lib_adr IS NULL');
        $this->connection->executeQuery('UPDATE ak_amap_livraison_lieu SET amap_liv_lieu_gps_latitude = \'0.0\' WHERE amap_liv_lieu_gps_latitude IS NULL');
        $this->connection->executeQuery('UPDATE ak_amap_livraison_lieu SET amap_liv_lieu_gps_latitude = \'0.0\' WHERE amap_liv_lieu_gps_latitude = \'\' ');
        $this->connection->executeQuery('UPDATE ak_amap_livraison_lieu SET amap_liv_lieu_gps_longitude = \'0.0\' WHERE amap_liv_lieu_gps_longitude IS NULL');
        $this->connection->executeQuery('UPDATE ak_amap_livraison_lieu SET amap_liv_lieu_gps_longitude = \'0.0\' WHERE amap_liv_lieu_gps_longitude = \'\' ');

        $existingLls = $this
            ->connection
            ->executeQuery('SELECT amap_liv_lieu_id, amap_liv_lieu_cp, amap_liv_lieu_ville FROM ak_amap_livraison_lieu')
            ->fetchAll()
        ;
        $this->addSql('ALTER TABLE ak_amap_livraison_lieu ADD ville_id VARCHAR(5) DEFAULT NULL');

        foreach ($existingLls as $ll) {
            // Fix name
            if ('38660' === $ll['amap_liv_lieu_cp'] && 'SAINT-HILAIRE' === $ll['amap_liv_lieu_ville']) {
                $ll['amap_liv_lieu_ville'] = 'PLATEAU-DES-PETITES-ROCHES';
            }
            if ('38190' === $ll['amap_liv_lieu_cp'] && 'Crolles' === $ll['amap_liv_lieu_ville']) {
                $ll['amap_liv_lieu_cp'] = '38920';
            }

            $ville = $this
                ->connection
                ->executeQuery('SELECT v_id FROM ak_ville WHERE v_nom = :nom AND v_cp = :cp', [
                    'nom' => $ll['amap_liv_lieu_ville'],
                    'cp' => $ll['amap_liv_lieu_cp'],
                ])
                ->fetchAll()
            ;
            if (1 !== count($ville)) {
                throw new Exception(
                    sprintf('Unable to find ville with %s %s', $ll['amap_liv_lieu_cp'], $ll['amap_liv_lieu_ville'])
                );
            }
            $this->addSql('UPDATE ak_amap_livraison_lieu SET ville_id = :villeId WHERE amap_liv_lieu_id = :llId', [
                'villeId' => $ville[0]['v_id'],
                'llId' => $ll['amap_liv_lieu_id'],
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
