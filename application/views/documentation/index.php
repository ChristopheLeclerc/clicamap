<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Document[] $documents */
/** @var bool $print_contact_form */
?>

<h3>Documentation</h3>
<div class="well well-lg">
    <div class="panel-group" id="accordion">
    <?php
    foreach ($documents as $document) {
        echo '<p><a href="'.prep_url($document->getLien()).'" target="_blank">'.$document->getNom().'</a></p>';
    }
    ?>
    </div>
</div>

<?php
/** Formulaire de contact */
if (isset($print_contact_form) && true === $print_contact_form) {
    require_once __DIR__.'/../includes/contact_modal.php';
}
?>
