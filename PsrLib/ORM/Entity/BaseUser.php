<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class BaseUser implements LocalisableEntity
{
    public const STATUT_PAYSAN = 'paysan';
    public const STATUT_AMAP = 'amap';
    public const STATUT_AMAPIEN = 'amapien';

    public const STATUTS_DISPO = [
        self::STATUT_PAYSAN,
        self::STATUT_AMAP,
        self::STATUT_AMAPIEN,
    ];

    /**
     * @var null|Token
     * @ORM\OneToOne(targetEntity="Token", cascade={"all"}, orphanRemoval=true)
     */
    private $passwordResetToken;

    /**
     * Verifie si l'utilisateur est actif.
     */
    abstract public function estActif(): bool;

    abstract public function getEmail(): ?string;

    abstract public function getPassword(): ?string;

    abstract public function getNom(): ?string;

    /**
     * @return BaseUser
     */
    abstract public function setPassword(?string $password);

    abstract public function getId(): ?int;

    /**
     * @return BaseUser
     */
    abstract public function setEmailInvalid(bool $emailInvalid);

    /**
     * @return BaseUser
     */
    public function setPlainPassword(string $password)
    {
        $pass_hash = password_hash($password, PASSWORD_DEFAULT);
        $this->setPassword($pass_hash);

        return $this;
    }

    public function getPasswordResetToken(): ?Token
    {
        return $this->passwordResetToken;
    }

    public function setPasswordResetToken(?Token $passwordResetToken): BaseUser
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }
}
