<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\CarbonImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Token
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var CarbonImmutable
     * @ORM\Column(type="carbon_immutable")
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $validityInHours;

    public function __construct(int $validityInHours = 48)
    {
        $this->createdAt = CarbonImmutable::now();
        $this->token = hash('sha256', random_bytes(32));
        $this->validityInHours = $validityInHours;
    }

    public function isValid(): bool
    {
        $now = CarbonImmutable::now();
        $diff = $now->diffInHours($this->getCreatedAt(), true);

        return $diff <= $this->validityInHours;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): CarbonImmutable
    {
        return $this->createdAt;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getValidityInHours(): int
    {
        return $this->validityInHours;
    }
}
