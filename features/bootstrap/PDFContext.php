<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Behat\Gherkin\Node\PyStringNode;
use Behat\MinkExtension\Context\RawMinkContext;

class PDFContext extends RawMinkContext
{
    use MinkContextTrait;

    /**
     * @var \Smalot\PdfParser\Parser
     */
    private $parser;

    /**
     * @var \GuzzleHttp\Client
     */
    private $httpClient;

    /**
     * @var string[]
     */
    private $pdfCache;

    public function __construct()
    {
        $this->parser = new \Smalot\PdfParser\Parser();
        $this->httpClient = new \GuzzleHttp\Client();
    }

    /**
     * @When le ficher PDF :url devrais contenir :
     */
    public function lefichierpdfdevraiscontenir(string $url, PyStringNode $expectedText)
    {
        $parsed = $this->parseFilePdfCached($url);

        if (!str_contains($parsed, $expectedText->getRaw())) {
            throw new \Behat\Mink\Exception\ExpectationException('Text not found', $this->getMink()->getSession()->getDriver());
        }
    }

    private function parseFilePdfCached(string $url): string
    {
        $sessionToken = $this->getMink()->getSession()->getDriver()->getCookie('PHPSESSID');
        $cacheKey = hash('sha256', $sessionToken.$url);
        if (isset($this->pdfCache[$cacheKey])) {
            return $this->pdfCache[$cacheKey];
        }

        $jar = \GuzzleHttp\Cookie\CookieJar::fromArray(['PHPSESSID' => $sessionToken], 'localhost');
        $res = $this->httpClient->get($url, ['cookies' => $jar]);
        if ('application/pdf' !== $res->getHeaderLine('Content-Type')) {
            throw new \RuntimeException(sprintf('File %s is not a pdf', $url));
        }

        $parsed = $this->parser->parseContent((string) $res->getBody())->getText();
        $this->pdfCache[$cacheKey] = $parsed;

        return $parsed;
    }
}
