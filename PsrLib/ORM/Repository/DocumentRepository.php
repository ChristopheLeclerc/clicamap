<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use LogicException;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Document;
use PsrLib\ORM\Entity\Paysan;

/**
 * @extends EntityRepository<Document>
 */
class DocumentRepository extends EntityRepository
{
    public function findForUser(BaseUser $user): array
    {
        if ($user instanceof Amapien) {
            if ($user->isAdmin()) { // Admin réseau
                return $this->findBy([
                    'permissionAdmin' => true,
                ]);
            }

            if ($user->isRefProduit()) {
                return $this->findBy([
                    'permissionAmapienref' => true,
                ]);
            }

            return $this->findBy([
                'permissionAmapien' => true,
            ]);
        }

        if ($user instanceof Paysan) {
            return $this->findBy([
                'permissionPaysan' => true,
            ]);
        }

        if ($user instanceof Amap) {
            return $this->findBy([
                'permissionAmap' => true,
            ]);
        }

        throw new LogicException('Unable to get documents for current user');
    }

    /**
     * Compte le nombre de documents avec la permission anonyme. Exclut le document joint si pas null.
     *
     * @return mixed
     */
    public function countDocumentAnonyme(int $exludeDocumentId = null)
    {
        $qb = $this
            ->createQueryBuilder('d')
            ->select('COUNT(d)')
            ->where('d.permissionAnonyme = true')
        ;

        if (null !== $exludeDocumentId) {
            $qb
                ->andWhere('d.id != :docId')
                ->setParameter('docId', $exludeDocumentId)
            ;
        }

        return $qb->getQuery()->getSingleScalarResult();
    }
}
