# language: fr
Fonctionnalité: Import des reçus paysan

  @database
  Plan du Scénario: Import avec succès
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/adhesion/import"
    Et que j'attache le fichier "Import_adhesion_paysan_success.xls" à "import_paysan"
    Et que je presse "import_paysan"
    Alors je devrais voir "Import validé"

    Exemples:
      | mail                            |
      | admin.aura@test.amap-aura.org   |
      | admin.rhone@test.amap-aura.org  |

  @database
  Plan du Scénario: Import avec erreur
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/adhesion/import"
    Et que j'attache le fichier "Import_adhesion_amap_error.xls" à "import_paysan"
    Et que je presse "import_paysan"
    Alors je devrais voir "Format de fichier invalide"

    Exemples:
      | mail                            |
      | admin.aura@test.amap-aura.org   |
      | admin.rhone@test.amap-aura.org  |
