# language: fr
Fonctionnalité: Comme amap, je doit pouvoir ajouter un contrat signé à un amapien

  @database
  Scénario: Ajouter un contrat signé à un amapien
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je vais sur "/contrat_signe/contrat_subscribe_1"
    Et que je remplis "search_contrat_signe_amap[ferme]" avec "1"
    Et que je remplis "search_contrat_signe_amap[mc]" avec "1"
    Et que je suis "Ajouter le contrat"
    Et que je remplis "amapien" avec "2"
    Et que je presse "Étape suivante"
    Et que je remplis "commande[1][1]" avec "1"
    Et que je remplis "commande[2][2]" avec "1"
    Et que je presse "Suivant"
    Et que je remplis "nb_reglements" avec "1"
    Et que je presse "Suivant"
    Et que je remplis "confirm-sign" avec "1"
    Et que je remplis "confirm-charter" avec "1"
    Et que je presse "Valider"
    Alors je devrais voir "NOM AMAPIEN"



