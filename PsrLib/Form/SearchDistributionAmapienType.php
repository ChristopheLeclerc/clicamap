<?php

namespace PsrLib\Form;

use Carbon\Carbon;
use PsrLib\DTO\SearchDistributionAmapienState;
use PsrLib\Form\Type\PeriodFlatpickrType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDistributionAmapienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('registered', CheckboxType::class, [
                'label' => 'Afficher uniquement les créneaux auxquels je suis inscrit',
            ])
            ->add('period', PeriodFlatpickrType::class, [
                'label' => 'Période',
                'min_date' => Carbon::now(),
            ])
        ;
    }

    public function getParent()
    {
        return SearchDistributionBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchDistributionAmapienState::class,
        ]);
    }
}
