<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Document $document */
/** @var bool $errorPermission */
?>

<h3>Nouveau document</h3>

<h5>
    <a href="<?= site_url('/portail'); ?>">Accueil</a>
    <i class="glyphicon glyphicon-menu-right"></i> Communication
    <i class="glyphicon glyphicon-menu-right"></i> <a href="<?= site_url('/documentation/list_documents'); ?>">Gestion des documents</a>
    <i class="glyphicon glyphicon-menu-right"></i> Nouveau document
</h5>

<?= form_open('', ['class' => 'form-horizontal']); ?>

    <div class="form-group">
        <label for="nom" class="col-sm-3 control-label">Nom du document</label>
        <div class="col-sm-5">
            <?= form_input('nom', set_value('nom', $document->getNom()), ['class' => 'form-control']); ?>
        </div>
    </div>
    <?= form_error('nom'); ?>

    <div class="form-group">
        <label for="permission" class="col-sm-3 control-label">Type d'utilisateur</label>
        <div class="col-sm-5">
                <div class="checkbox"><label>
                    <?= form_checkbox('permission_anonyme', true, set_value('permission_anonyme', $document->isPermissionAnonyme())); ?>
                Page d'accueil (utilisateur non connecté)</label></div>
                <?= form_error('permission_anonyme'); ?>
                <div class="checkbox"><label>
                    <?= form_checkbox('permission_admin', true, set_value('permission_admin', $document->isPermissionAdmin())); ?>
                Administrateur réseau</label></div>
                <div class="checkbox"><label>
                    <?= form_checkbox('permission_paysan', true, set_value('permission_paysan', $document->isPermissionPaysan())); ?>
                Paysan</label></div>
                <div class="checkbox"><label>
                    <?= form_checkbox('permission_amap', true, set_value('permission_amap', $document->isPermissionAmap())); ?>
                AMAP</label></div>
                <div class="checkbox"><label>
                    <?= form_checkbox('permission_amapienref', true, set_value('permission_amapienref', $document->isPermissionAmapienref())); ?>
                AMAPien référent</label></div>
                <div class="checkbox"><label>
                    <?= form_checkbox('permission_amapien', true, set_value('permission_amapien', $document->isPermissionAmapien())); ?>
                AMAPien</label></div>

                <?php if (true === $errorPermission) {
    echo '<div class="alert alert-danger">Merci de selectionner au moins un type d\'utilisateur</div>';
} ?>
        </div>

    </div>

    <div class="form-group">
        <label for="lien" class="col-sm-3 control-label">Lien vers le document</label>
        <div class="col-sm-5">
            <?= form_input('lien', set_value('lien', $document->getLien()), ['class' => 'form-control']); ?>
        </div>
    </div>
    <?= form_error('lien'); ?>

    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-success pull-right">Sauvegarder</button>
            <a href="<?= site_url('documentation/list_documents'); ?>" class="btn btn-default pull-right" style="margin-right:5px;">Annuler</a>
        </div>
    </div>

<?= form_close(); ?>
