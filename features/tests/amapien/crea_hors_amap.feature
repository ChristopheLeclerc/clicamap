# language: fr
Fonctionnalité: Créer un AMAPien hors AMAP

  @database
  Plan du Scénario: Tester les messages d'erreur et de réussite
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand je suis sur "/amapien"
    Et que je suis "Créer un amapien hors AMAP"
    Et que je remplis "amapien_creation[prenom]" avec "<contenu_prenom>"
    Et que je remplis "amapien_creation[nom]" avec "<contenu_nom>"
    Et que je remplis "amapien_creation[email]" avec "<contenu_mail>"
    Et que je remplis "amapien_creation[newsletter]" avec "1"
    Et que je remplis "amapien_creation[needWaiting]" avec "0"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | contenu_prenom | contenu_nom | contenu_mail               | message                                         |
      | Eugène         | Martin      | eugene@bonmail.fr          | L'amapien(ne) a été créé(e) avec succès !       |
      |                | Martin      | eugene@mauvaismail.com     | Cette valeur ne doit pas être vide.             |
      | Eugène         |             | eugene@mauvaismail.com     | Cette valeur ne doit pas être vide.             |
      | Eugène         | Martin      |                            | Cette valeur ne doit pas être vide.             |
      | Eugène         | Martin      | amapien@test.amap-aura.org | Cet email existe déjà dans la base de donnée.   |
