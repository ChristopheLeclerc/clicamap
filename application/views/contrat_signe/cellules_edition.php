<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<h3>Modifier les quantités</h3>

<?php

if (!$this->session->amapien) {
    echo '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.ucfirst($modele_contrat[0]->mc_nom).' <i class="glyphicon glyphicon-menu-right"></i> '.ucfirst($amapien_prenom).' '.strtoupper($amapien_nom).' <i class="glyphicon glyphicon-menu-right"></i> Modifier</h5>';
}

?>

<!--REDIRIGÉ VERS LA MÉTHODE CELLULE !-->
<form method="POST" id="edit" action="<?= site_url('contrat_signe/cellules'); ?>">

    <div class="form-group">

        <button class="btn btn-primary btn-paste" onclick="cpl()" type="button">Copier la première ligne partout
        </button>

        <button class="btn btn-primary btn-paste" onclick="ct()" type="button">Calcul des montants</button>

        <script type="text/javascript">
          function cpl() {

            $('#copier_premiere_ligne').val(1);
            document.getElementById("edit").submit();

          }

          function ct() {

            $('#calculer_totaux').val(1);
            document.getElementById("edit").submit();

          }
        </script>

    </div>

    <input type="hidden" name="copier_premiere_ligne" id="copier_premiere_ligne" value="0"/>

    <input type="hidden" name="calculer_totaux" id="calculer_totaux" value="0"/>

    <input type="hidden" name="envoi" value="x"/>

    <?php

    echo '<input type="hidden" name="mc_id" value="'.$mc_id.'" />';
    echo '<input type="hidden" name="c_id" value="'.$c_id.'" />';
    echo '<input type="hidden" name="amapien_id" value="'.$amapien_id.'"/>';

    if ($mes_contrats) {
        echo '<input type="hidden" name="mes_contrats" value="1" />';
    }

    /*if($this->session->ID == $amapien_id && ! $this->session->gest_amap_active)
        echo '<input type="hidden" name="ref_prod" value="1"/>';*/

    if ($alerte_livraisons_obligatoires) {
        echo '<div class="alert alert-danger ">'.$alerte_livraisons_obligatoires.'</div>';
    }

    if ($alerte_choix_identiques) {
        echo '<div class="alert alert-danger ">'.$alerte_choix_identiques.'</div>';
    }

    ?>

    <div id="parent">
        <table id="fixTable" class="table">
            <thead class="thead-inverse">
            <tr>
                <th></th>

                <?php

                foreach ($mc_produits as $mc_pro) {
                    echo '<th class="text-center">'.ucfirst($mc_pro->f_pro_nom).' <br/>'.ucfirst($mc_pro->f_pro_conditionnement).'<br/>'.$mc_pro->mc_pro_prix.' €</th>';
                }

                ?>

            </tr>
            </thead>
            <tbody>

            <?php

            foreach ($mc_dates_livraison as $mc_d) {
                echo '<tr><th scope="row">'.$mc_d->mc_d_date_livraison.'</th>';

                foreach ($mc_produits as $mc_pro) {
                    $x = 0;

                    foreach ($mc_produits_exclusions as $mc_pro_ex) {
                        if ($mc_pro->mc_pro_id == $mc_pro_ex->mc_pro_ex_fk_modele_contrat_produit_id && $mc_d->mc_d_id == $mc_pro_ex->mc_pro_ex_fk_modele_contrat_date_id) {
                            echo '<td><input type="text" style="min-width:8em;" class="form-control text-center input-medium" name="date_'.$mc_d->mc_d_id.'[]" value="xxx" readonly /></td>';

                            // ATTENTION : le script ne marche plus avec "disabled" au lieu de "readonly". La valeur "xxx" doit être envoyée !

                            ++$x;

                            break;
                        }
                    }
                    // Pas d'exclusion ; $x est toujours égal à 0
                    if (0 == $x) {
                        foreach ($contrat_cellule as $c_c) {
                            if ($mc_pro->mc_pro_id == $c_c->c_c_fk_modele_contrat_produit_id && $mc_d->mc_d_id == $c_c->c_c_fk_modele_contrat_date_id) {
                                echo '<td><input style="min-width:8em;" type="number" step="0.5" min="0" class="form-control text-center" name="date_'.$mc_d->mc_d_id.'[]" value="'.$c_c->c_c_quantite.'"/></td>';

                                ++$x;

                                break;
                            }
                        }
                    }

                    if (0 == $x) {
                        echo '<td><input style="min-width:8em;" type="number" step="0.5" min="0" class="form-control text-center" name="date_'.$mc_d->mc_d_id.'[]" value="" /></td>';
                    }
                }

                echo '</tr>';
            }

            ?>
            </tbody>
        </table>
    </div>

    <div class="form-group">

        <input type="submit" value="Sauvegarder" class="pull-right btn btn-success"/>

        <?php

        if ($mes_contrats) {
            echo '<a href="'.site_url('amapien/mes_contrats').'" class="pull-right btn btn-default" style="margin-right:5px;">Annuler</a>';
        } else {
            echo '<a href="'.site_url('contrat_signe/').'" class="pull-right btn btn-default" style="margin-right:5px;">Annuler</a>';
        }

        ?>

    </div>

</form>
