# language: fr
Fonctionnalité: Modification des années d'adhésion d'une ferme

  @database
  Scénario: Modification des annes d'adhesion d'une ferme
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et je suis sur "/ferme/informations_generales/1"
    Lorsque je remplis "ferme[anneeAdhesions][]" avec "2011"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "La ferme a été modifiée avec succès !"
    Et je vais sur "/ferme/display/1"
    Et je devrais voir "2011"
    Et je vais sur "/paysan/display/1"
    Et je devrais voir "2011"
