<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Assert\Assertion;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\Amap;

class AdhesionAmapAmapienRepository extends AdhesionRepository
{
    /**
     * @return AdhesionAmapAmapien[]
     */
    public function search(?int $year, ?string $keyword, Amap $creator)
    {
        $qb = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.amapien', 'amapien')
            ->leftJoin('aa.value', 'value')
            ->where('aa.creator = :creator')
            ->setParameter('creator', $creator)
        ;

        if (null !== $year && $year > 0) {
            $qb
                ->andWhere('value.year = :year')
                ->setParameter('year', $year)
            ;
        }

        if ('' !== $keyword) {
            $qb
                ->andWhere('amapien.nom LIKE :keyWord')
                ->setParameter('keyWord', '%'.$keyword.'%')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Amap $creator
     *
     * @return array|false
     */
    public function getVoucherNumbers($creator)
    {
        // Override condition as creator is AMAP
        Assertion::isInstanceOf($creator, Amap::class);
        $res = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.value', 'value')
            ->select('value.voucherNumber as voucherNumber')
            ->where('aa.creator = :creator')
            ->setParameter('creator', $creator)
            ->getQuery()
            ->getScalarResult()
        ;

        return array_column($res, 'voucherNumber');
    }
}
