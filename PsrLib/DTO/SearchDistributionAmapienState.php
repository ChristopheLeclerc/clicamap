<?php

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Amapien;

class SearchDistributionAmapienState extends SearchDistributionBaseState
{
    /**
     * @var bool
     */
    private $registered = false;

    /**
     * @var Amapien
     */
    private $amapien;

    public function __construct(Amapien $amapien)
    {
        $this->amapien = $amapien;
    }

    public function getAmapien(): Amapien
    {
        return $this->amapien;
    }

    public function isRegistered(): bool
    {
        return $this->registered;
    }

    public function setRegistered(bool $registered): SearchDistributionAmapienState
    {
        $this->registered = $registered;

        return $this;
    }
}
