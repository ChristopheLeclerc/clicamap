<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\CarbonDateType;
use PsrLib\Form\Type\FermeAnneeAdhesionType;
use PsrLib\Form\Type\VilleType;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\AuthentificationMockable;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FermeType extends AbstractType
{
    /**
     * @var AuthentificationMockable
     */
    private $authentification;

    public function __construct(AuthentificationMockable $authentification)
    {
        $this->authentification = $authentification;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Ferme $ferme */
        $ferme = $options['data'];
        Assertion::isInstanceOf($ferme, Ferme::class);

        $currentUser = $this->authentification->getCurrentUser();
        $siretDisabled = $currentUser instanceof Paysan && $ferme->inRegroupement();

        $builder
            ->add('nom', TextType::class, [
                'required' => false,
            ])
            ->add('siret', TextType::class, [
                'required' => false,
                'disabled' => $siretDisabled,
                'attr' => $siretDisabled ? ['readonly' => true] : [],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
            ])
            ->add('delaiModifContrat', TextType::class, [
                'required' => false,
            ])
            ->add('libAdr', TextType::class, [
                'required' => false,
            ])
            ->add('ville', VilleType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('gpsLatitude', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('gpsLongitude', TextType::class, [
                'attr' => ['readonly' => true],
            ])
            ->add('tva', YesNoChoiceType::class)
            ->add('tvaTaux', TextType::class, [
                'required' => false,
            ])
            ->add('tvaDate', CarbonDateType::class, [
                'required' => false,
            ])
            ->add('surface', TextType::class, [
                'required' => false,
            ])
            ->add('uta', TextType::class, [
                'required' => false,
            ])
            ->add('certification', ChoiceType::class, [
                'choices' => Ferme::AVAILABLE_CERTIFICATIONS,
                'choice_label' => function (?string $choice) {
                    return array_search($choice, Ferme::AVAILABLE_CERTIFICATIONS, true);
                },
            ])
            ->add('url', TextType::class, [
                'required' => false,
            ])
        ;

        $permissionEditAdhesions = $options['permission_edit_adhesions'];
        $builder
            ->add('anneeAdhesions', FermeAnneeAdhesionType::class, [
                'ferme' => $options['data'],
                'required' => $permissionEditAdhesions,
                'disabled' => !$permissionEditAdhesions,
                'attr' => ['readonly' => !$permissionEditAdhesions],
            ])
        ;

        $permissionEditRegroupement = $options['permission_edit_regroupement'];
        if (true === $permissionEditRegroupement) {
            $builder
                ->add('regroupement', EntityType::class, [
                    'class' => FermeRegroupement::class,
                    'required' => false,
                    'placeholder' => true,
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ferme::class,
            'permission_edit_adhesions' => false,
            'permission_edit_regroupement' => false,
        ]);
    }
}
