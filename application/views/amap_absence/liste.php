<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\AmapAbsence[] $absences */
/** @var \PsrLib\ORM\Entity\Amap $amap $ */
?>

<h3>La planning des absences</h3>

<h5>
    Gestionnaire <i class="glyphicon glyphicon-menu-right"></i>
    <a href="/amap">Gestion de mon AMAP </a><i class="glyphicon glyphicon-menu-right"></i>
    Le planning des absences  <i class="glyphicon glyphicon-menu-right"></i>
</h5>

<p>
    <a href="<?=site_url('/amap_absence/form/'.$amap->getId()); ?>" class="btn btn-success" style="margin-right:5px;"><i
                class="glyphicon glyphicon-plus"></i> Ajouter une nouvelle période d'absence
    </a>
</p>

<table class="table table-datatable">
    <thead>
        <tr>
            <th>Titre</th>
            <th>Date de début</th>
            <th>Date de fin</th>
            <th>Outils</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($absences as $absence): ?>
            <tr>
                <td>
                    <?= $absence->getTitre(); ?>
                </td>
                <td>
                    <?= date_format_french($absence->getAbsenceDebut()); ?>
                </td>
                <td>
                    <?= date_format_french($absence->getAbsenceFin()); ?>
                </td>
                <td>
                    <a href="<?=site_url('/amap_absence/form/'.$absence->getId().'/'.$absence->getId()); ?>">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    <a href="#"
                       title="Supprimer la période d'absence"
                       data-toggle="modal"
                       data-target="#modal_remove_<?=$absence->getId(); ?>"
                    >
                        <i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer la période d'absence"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php foreach ($absences as $absence): ?>
    <div class="modal fade text-left" id="modal_remove_<?=$absence->getId(); ?>" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">Voulez-vous vraiment supprimer la période d'absence "<?=$absence->getTitre(); ?>" ? </h5>
                </div>
                <div class="modal-body">
                    <p class="text-right">
                        <a
                                id="modal-remove-link"
                                href="<?=site_url('/amap_absence/supprimer/'.$absence->getId()); ?>>"
                                class="btn btn-danger btn-sm">OUI</a>
                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
