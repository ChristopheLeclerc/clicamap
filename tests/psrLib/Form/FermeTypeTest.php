<?php

namespace Test\Form;

use PsrLib\Form\FermeType;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;

/**
 * @internal
 * @coversNothing
 */
class FermeTypeTest extends AbstractFormTest
{
    public function testSiretEditablePaysanFermeNonRegroupement()
    {
        $paysan = $this->buildPaysan();
        $this->authentification->mockUser($paysan);
        $form = $this->formFactory->create(FermeType::class, $paysan->getFerme());

        $siretField = $form->get('siret');
        self::assertFalse($siretField->isDisabled());
        self::assertSame([], $siretField->getConfig()->getOption('attr'));
    }

    public function testSiretEditablePaysanFermeRegroupement()
    {
        $paysan = $this->buildPaysan(true);
        $this->authentification->mockUser($paysan);
        $form = $this->formFactory->create(FermeType::class, $paysan->getFerme());

        $siretField = $form->get('siret');
        self::assertTrue($siretField->isDisabled());
        self::assertSame(['readonly' => true], $siretField->getConfig()->getOption('attr'));
    }

    public function testSiretEditableAmapienFermeNonRegroupement()
    {
        $paysan = $this->buildPaysan();
        $this->authentification->mockUser(new Amapien());
        $form = $this->formFactory->create(FermeType::class, $paysan->getFerme());

        $siretField = $form->get('siret');
        self::assertFalse($siretField->isDisabled());
        self::assertSame([], $siretField->getConfig()->getOption('attr'));
    }

    public function testSiretEditableAmapienFermeRegroupement()
    {
        $paysan = $this->buildPaysan(true);
        $this->authentification->mockUser(new Amapien());
        $form = $this->formFactory->create(FermeType::class, $paysan->getFerme());

        $siretField = $form->get('siret');
        self::assertFalse($siretField->isDisabled());
        self::assertSame([], $siretField->getConfig()->getOption('attr'));
    }

    private function buildPaysan(bool $regroupement = false)
    {
        $ferme = new Ferme();
        if ($regroupement) {
            $ferme->setRegroupement(new FermeRegroupement());
        }
        $paysan = new Paysan();
        $paysan->setFerme($ferme);

        return $paysan;
    }
}
