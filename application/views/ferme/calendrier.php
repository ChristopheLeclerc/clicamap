<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme $ferme */
$currentUser = get_current_connected_user();
?>

    <style>

        h3 {
            font-size: 18px;
        }

        h4.cal {
            border-top: 1px solid #ddd;
            padding-top: 10px;
        }

    </style>

    <h3>Calendrier des livraisons</h3>

<?php

if (!($currentUser instanceof \PsrLib\ORM\Entity\Paysan)) {
    echo '<h5><a href="'.site_url('ferme').'">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> <a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.ucfirst($ferme->getNom()).'</a> <i class="glyphicon glyphicon-menu-right"></i> Livraisons</h5>';
}

?>

<?php if (isset($fermes_accessibles)): ?>
    <div class="row form-inline">
        <div class="form-group">
                <label for="profil" class="control-label">Ferme : </label>
                <select id="js-select-ferme" class="form-control">
                    <?php foreach ($fermes_accessibles as $fermes_accessible) : ?>
                        <option
                            <?= $fermes_accessible === $ferme ? 'selected' : ''; ?>
                            value="<?= $fermes_accessible->getId(); ?>"
                        ><?=$fermes_accessible; ?></option>
                    <?php endforeach; ?>
                </select>
        </div>
    </div>
<?php endif; ?>

<div class="alert alert-info">
    Une date en vert indique au moins une livraison, cliquer dessus pour voir le détail des quantités à livrer.
</div>

<?php

echo '<div class="table-responsive calendrier">';
echo $calendrier;
echo '</div>';

if ($livraisons) {
    $i = 0;

    foreach ($livraisons as $key => $jour) {
        echo '<div class="modal fade" id="modal_'.$key.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Livraisons du '.$key.'/'.$mois.'/'.$an.'</h4>
    </div>
    <div class="modal-body">';

        // AMAP liées à la date
        if ($jour['amap']) {
            foreach ($jour['amap'] as $amap) {
                echo '<div class="well">';
                echo '<h3>'.$amap->getNom().'</h3>';

                $mc_nbr = 0;

                // MC liés à la date
                if ($jour['mc']) {
                    /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
                    foreach ($jour['mc'] as $mc) {
                        if ($mc->getLivraisonLieu()->getAmap() !== $amap) {
                            continue;
                        }
                        $mc_quantites = 0;

                        echo '<h4 class="cal">Contrat : '.ucfirst($mc->getNom()).'</h4>';
                        $ll = $mc->getLivraisonLieu();
                        $ville = $ll->getVille();
                        echo '<h4>Lieu : '.ucfirst($ll->getNom()).' - '.$ll->getAdresse().', '.(null === $ville ? '' : $ville->getCpString()).' '.(null === $ville ? '' : $ville->getNom()).'</h4>';

                        // Produits liés au MC
                        /** @var \PsrLib\ORM\Entity\ModeleContratProduit $produit */
                        foreach ($mc->getProduits() as $produit) {
                            $quantites = null;
                            // Quantités liées au produit => addition
                            /** @var \PsrLib\ORM\Entity\ContratCellule $quantite */
                            foreach ($jour['cellules'] as $quantite) {
                                if ($produit->getId() === $quantite->getModeleContratProduit()->getId()) {
                                    $quantites += $quantite->getQuantite();
                                }
                            }
                            // Somme
                            if ($quantites) {
                                if (0 == $mc_quantites) {
                                    echo '<h4>Quantités totales à livrer :</h4>';
                                    // Liste quantités totales des produits à livrer
                                    echo '<ul>';
                                }
                                echo '<li>'.ucfirst($produit->getNom()).' ('.$produit->getConditionnement().' / '.$produit->getPrix().' €) : <strong>'.$quantites.'</strong></li>';

                                ++$mc_quantites;
                            }
                        }

                        if ($mc_quantites) {
                            echo '</ul>';

                            // Liste des produits à livrer par AMAPIEN
                            echo '<button class="btn btn-primary btn-xs" data-toggle="collapse" data-target="#amapiens_'.$i.'">Détail par amapien</button>';

                            echo '<div id="amapiens_'.$i.'" class="alert alert-success collapse" style="padding-top:5px;">';

                            /** @var \PsrLib\ORM\Entity\Amapien $amapien */
                            foreach ($jour['amapiens'] as $amapien) {
                                // Entête
                                $n = 0;

                                /** @var \PsrLib\ORM\Entity\ContratCellule $quantite */
                                foreach ($jour['cellules'] as $quantite) {
                                    if ($quantite->getContrat()->getModeleContrat()->getId() === $mc->getId()
                                        && $quantite->getContrat()->getAmapien()->getId() === $amapien->getId()) {
                                        if (0 == $n) {
                                            echo '<p style="margin-top:10px;"><strong>'.$amapien->getNom().' '.$amapien->getPrenom().'</strong></p>';
                                        }

                                        if (0.0 !== $quantite->getQuantite()) {
                                            echo '<ul>';
                                            echo '<li>'.ucfirst($quantite->getModeleContratProduit()->getNom()).' ('.$quantite->getModeleContratProduit()->getConditionnement().' / '.$quantite->getModeleContratProduit()->getPrix().' €) : <strong>'.$quantite->getQuantite().'</strong></li>';
                                            echo '</ul>';
                                        }

                                        ++$n;
                                    }
                                }
                            }

                            echo '</div>';
                        }

                        if (0 == $mc_quantites) {
                            echo '<div class="alert alert-warning">Il n\'y a pas encore de souscriptions à ce contrat.</div>';
                        }

                        ++$i;
                    }
                }

                echo '</div>';
            }
        } else {
            echo '<h5>Il n\'y a pas encore de commande pour cette date.</h5>';
        }

        echo '</div>
    </div>
    </div>
    </div>';
    }
}
?>

<script>
  function initSelect() {
    const $select = $('#js-select-ferme');
    if($select.length === 0) {
      return;
    }
    $select.on('change', function (e) {
      window.location = "<?=site_url('/ferme/calendrier/'); ?>" + e.target.value
    })
  }
  initSelect();

</script>
