<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<div class="modal" tabindex="-1" role="dialog" id="contact-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= form_open('', ['class' => 'form-horizontal']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Formulaire de contact</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="form-group">
                        <label for="contact_nom" class="col-sm-2 control-label">Nom</label>
                        <div class="col-sm-10">
                            <?= form_input('contact_nom', set_value('contact_nom'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('contact_nom'); ?>

                    <div class="form-group">
                        <label for="contact_prenom" class="col-sm-2 control-label">Prénom</label>
                        <div class="col-sm-10">
                            <?= form_input('contact_prenom', set_value('contact_prenom'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('contact_prenom'); ?>

                    <div class="form-group">
                        <label for="contact_email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <?= form_input('contact_email', set_value('contact_email'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('contact_email'); ?>

                    <div class="form-group">
                        <label for="contact_cp" class="col-sm-2 control-label">Code postal</label>
                        <div class="col-sm-10">
                            <?= form_input('contact_cp', set_value('contact_cp'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('contact_cp'); ?>

                    <div class="alert alert-info">
                        Le code postal nous permet d'adresser votre message au bon réseau
                    </div>

                    <div class="form-group">
                        <label for="contact_contenu" class="col-sm-2 control-label">Contenu</label>
                        <div class="col-sm-10">
                            <?= form_textarea('contact_contenu', set_value('contact_contenu'), ['class' => 'form-control']); ?>

                        </div>
                    </div>
                    <?= form_error('contact_contenu'); ?>

                    <div class="form-group text-center">
                        <?= render_nouveau_captcha_img(); ?>

                    </div>
                    <div class="form-group ">
                        <label for="contact_captcha" class="col-sm-2 control-label">Code de vérification</label>
                        <div class="col-sm-10">
                            <?= form_input('contact_captcha', set_value('contact_captcha'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('contact_captcha'); ?>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox text-left">
                                <label >
                                    <input type="checkbox" name="contact_envoiconfirmation" value="1" <?= set_checkbox('contact_envoiconfirmation', '2'); ?> /> Recevoir un mail de confirmation
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </div>
            <?= form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  $('#contact-modal')
    .modal({
      show: true
    })
    // Redirect to previous page on close
    .on('hidden.bs.modal', function () {
      window.history.back()
    })
</script>
