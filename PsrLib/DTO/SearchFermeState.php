<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Reseau;
use PsrLib\ORM\Entity\TypeProduction;

class SearchFermeState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    /**
     * @var null|Reseau
     */
    private $reseau;

    /**
     * @var null|TypeProduction
     */
    private $typeProduction;

    /**
     * @var bool | null
     */
    private $carto;

    /**
     * @var string | null
     */
    private $keyword;

    public function isEmpty(): bool
    {
        return null === $this->getRegion()
            && null === $this->getDepartement()
            && null === $this->getTypeProduction()
            && null === $this->getReseau()
            && null === $this->getKeyword()
        ;
    }

    public function getCarto(): ?bool
    {
        return $this->carto;
    }

    public function setCarto(?bool $carto): SearchFermeState
    {
        $this->carto = $carto;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): SearchFermeState
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getReseau(): ?Reseau
    {
        return $this->reseau;
    }

    public function setReseau(?Reseau $reseau): SearchFermeState
    {
        $this->reseau = $reseau;

        return $this;
    }

    public function getTypeProduction(): ?TypeProduction
    {
        return $this->typeProduction;
    }

    public function setTypeProduction(?TypeProduction $typeProduction): SearchFermeState
    {
        $this->typeProduction = $typeProduction;

        return $this;
    }
}
