<?php

namespace PsrLib\Services\Security\Checkers;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class FermeCheck extends AbstractCheck
{
    public function isGrantedFermeList(): bool
    {
        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        return $this->getCurrentUser() instanceof Amapien
            && ($this->getCurrentUser()->isAdmin() || $this->getCurrentUser()->isRefProduit())
        ;
    }

    public function isGrantedFermeCreate(): bool
    {
        $user = $this->getCurrentUser();
        if ($user instanceof Amapien && $user->isAdmin()) {
            return true;
        }

        if ($user instanceof FermeRegroupement) {
            return true;
        }

        if ($user instanceof Amap) {
            return true;
        }

        return false;
    }

    public function isGrantedFermeDownload(): bool
    {
        return $this->getCurrentUser() instanceof Amapien && $this->getCurrentUser()->isAdmin();
    }

    public function isGrantedFermeDisplayOwn(): bool
    {
        return $this->getCurrentUser() instanceof Paysan;
    }

    public function isGrantedFermeDisplay(Ferme $ferme): bool
    {
        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            // Les rôles suivant peuvent éditer toutes les fermes
            if ($this->getCurrentUser()->isAdmin()) {
                return true;
            }

            // Les référents produits peuvent éditer leur ferme
            if ($this->getCurrentUser()->isRefProduit()) {
                foreach ($this->getCurrentUser()->getRefProdFermes() as $refProdFerme) {
                    if ($ferme->getId() === $refProdFerme->getId()) {
                        return true;
                    }
                }

                return false;
            }
        }

        // Les paysans ont accès à leur ferme
        if ($this->getCurrentUser() instanceof Paysan) {
            $currentUserFerme = $this->getCurrentUser()->getFerme();

            return null !== $currentUserFerme && $currentUserFerme->getId() === $ferme->getId();
        }

        if ($this->getCurrentUser() instanceof FermeRegroupement) {
            return $ferme->getRegroupement() === $this->getCurrentUser();
        }

        return false;
    }

    public function isGrantedRefProduit(Ferme $ferme): bool
    {
        $currentUser = $this->getCurrentUser();
        if ($currentUser instanceof FermeRegroupement) {
            return false;
        }

        return $this->isGrantedFermeDisplay($ferme);
    }

    public function isGrantedFermeRemove(): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isAdmin()
        ;
    }

    public function isGrantedFermeProductDelete(Ferme $ferme): bool
    {
        // Seul les paysans peuvent supprimer les produits des fermes
        if ($this->getCurrentUser() instanceof Paysan) {
            return $this->isGrantedFermeDisplay($ferme);
        }

        return false;
    }

    public function isGrantedTypeProductionManage(Ferme $ferme): bool
    {
        // Les paysans peuvent gérer les types de production
        if ($this->getCurrentUser() instanceof Paysan) {
            return $this->isGrantedFermeDisplay($ferme);
        }

        // Les rôles suivant peuvent gérer les types de production
        if ($this->getCurrentUser() instanceof Amapien) {
            if ($this->getCurrentUser()->isAdmin()) {
                return $this->isGrantedFermeDisplay($ferme);
            }
        }

        return false;
    }

    public function isGrantedPaysanChangeAdhesion(): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isAdmin()
        ;
    }
}
