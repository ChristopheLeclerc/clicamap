<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use DI\Annotation\Inject;
use Money\Money;
use PsrLib\ORM\Entity\UserWithFermes;

/**
 * Class Contrat_signe.
 */
class Contrat_signe extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\MPdfGeneration
     */
    public $mpdfgeneration;
    protected $data;

    /**
     * @Inject
     *
     * @var \Symfony\Component\Serializer\SerializerInterface
     */
    private $serializer;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\EntityBuilder\Contrat_signe_cellule_model_builder
     */
    private $contrat_signe_cellule_model_builder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\CommandeValidation
     */
    private $commandevalidation;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Exporters\ExcelGenerator
     */
    private $excelgenerator;

    /**
     * Contrat_signe constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data = ['create' => null,
            'edit' => null,
            'remove' => null,
            'test' => false,
            'amapien_id' => null,
            'alerte_livraisons_obligatoires' => null,
            'alerte_choix_identiques' => null,
            'copier_premiere_ligne' => null,
            'total' => null,
            'sous_totaux' => null,
            'mc_id' => null,
            'c_id' => null,
            'reseau_all' => [],
            'c_amap_all' => [],
            'ferme_all' => [],
            'mc_all' => [],
            'contrat_all' => [],
            'modele_contrat' => [],
            'contrats' => [],
            'redirection' => null,
            'mes_contrats' => null, ];

        $this->load->helper('array');
        $this->load->helper('date');

        $this->load->library('excel');
        $this->load->library('form_validation');

        $this
            ->form_validation
            ->set_error_delimiters('<div class="alert alert-danger">', '</div>')
        ;
    }

    public function index()
    {
        $currentUser = $this->getUser();
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amap) {
            $this->accueil_amap($currentUser);

            return;
        }
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien
            && $currentUser->isRefProduit()) {
            $this->accueil_ref_produit($currentUser);

            return;
        }
        if ($currentUser instanceof \PsrLib\ORM\Entity\UserWithFermes) {
            $this->accueil_paysan($currentUser);

            return;
        }

        $this->accueil();
    }

    /**
     * @param $mc_id
     */
    public function counter($mc_id)
    {
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_LIST);

        $this->loadViewWithTemplate('contrat_signe/counter_step_1', [
            'mc' => $mc,
        ]);
    }

    /**
     * DÉPLACER UNE DATE DE LIVRAISON.
     *
     * @param mixed $mc_id
     */
    public function move($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_MOVE_DATE, $mc);

        $this->form_validation->set_rules('step_1', '""', 'required');

        if ($this->form_validation->run()) {
            $this->move_step_2($mc_id);
        } else {
            $this->data['mc'] = $mc;
            $page = $this
                ->load
                ->view(
                    'contrat_signe/move_step_1',
                    $this->data,
                    true
                )
            ;
            $this->load->view('template', ['page' => $page]);
        }
    }

    public function move_step_2($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_MOVE_DATE, $mc);

        $this->form_validation->set_rules('step_2', '""', 'required');
        $this->form_validation->set_rules(
            'nouvelle_date',
            '"Nouvelle date"',
            'date_format'
        );

        $now = Carbon::now();
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $datesLivraisons */
        $datesLivraisons = $mc
            ->getDates()
            ->filter(function (PsrLib\ORM\Entity\ModeleContratDate $date) use ($now) {
                return $now->lte($date->getDateLivraison());
            })
            ->toArray()
        ;

        $this->data['alerte_date_existante'] = null;
        $this->data['mc_dates_livraison'] = $datesLivraisons;
        $this->data['mc'] = $mc;

        if ($this->form_validation->run()) {
            // On vérifie si la date existe déjà.
            $date_valide = true;

            $this->data['mc_nouvelle_date'] = $this->input->post('nouvelle_date');

            foreach ($datesLivraisons as $d_l) {
                if ($this->data['mc_nouvelle_date'] === $d_l->getDateLivraison()->format('Y-m-d')) {
                    $date_valide = false;
                    $this->data['alerte_date_existante']
                        = 'La nouvelle date de livraison '.
                        $this->data['mc_nouvelle_date'].
                        ' est déjà une date de livraison pour ce contrat.'
                    ;
                    $page = $this
                        ->load
                        ->view(
                            'contrat_signe/move_step_2',
                            $this->data,
                            true
                        )
                    ;
                    $this
                        ->load
                        ->view(
                            'template',
                            [
                                'page' => $page,
                                'header_dates' => 1,
                            ]
                        )
                    ;
                }
            }

            if ($date_valide) {
                $this->move_step_3($mc_id);
            }
        } else {
            $page = $this
                ->load
                ->view(
                    'contrat_signe/move_step_2',
                    $this->data,
                    true
                )
            ;
            $this
                ->load
                ->view(
                    'template',
                    [
                        'page' => $page,
                        'header_dates' => 1,
                    ]
                )
            ;
        }
    }

    public function move_step_3($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_MOVE_DATE, $mc);

        /** @var \PsrLib\ORM\Entity\ModeleContratDate $date */
        $date = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContratDate::class, $this->input->post('date_a_deplacer'));
        if ($date->getModeleContrat()->getId() !== $mc->getId()) {
            throw new \RuntimeException('Invalid date');
        }

        $this->form_validation->set_rules('step_3', '""', 'required');
        $this->data['mc'] = $mc;

        if ($this->form_validation->run()) {
            $nouvelleDate = Carbon::createFromFormat('Y-m-d', $this->input->post('nouvelle_date'));
            $date->setDateLivraison($nouvelleDate);
            $this->em->flush();
            $this->addFlash(
                'notice_success',
                'Le contrat a été modifiée avec succès !'
            );
            redirect('/contrat_signe');

            return;
        }
        $this->data['date_a_deplacer'] = $date;

        $this->data['nouvelle_date'] = $this->input->post('nouvelle_date');

        // AMAPIENS CONCERNÉS
        // Initialisation des variables
        $mc_amapiens_deplacement = [];

        /** @var \PsrLib\ORM\Entity\Contrat $contrat */
        $contratDates = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
            ->getWithDateMc($mc, $date)
            ;
        foreach ($contratDates as $contrat) {
            $mc_amapiens_deplacement[] = $contrat->getAmapien();
        }

        $this->data['mc_amapiens_deplacement'] = $mc_amapiens_deplacement;

        $page = $this->load->view(
            'contrat_signe/move_step_3',
            $this->data,
            true
        );

        $this->load->view('template', ['page' => $page]);
    }

    /**
     * TÉLÉCHARGEMENTS.
     *
     * @param null $mc_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_souscripteurs($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        $mc_nom = $mc->getNom();

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        /** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
        $contrats = $mc->getContrats()->toArray();
        usort($contrats, function (PsrLib\ORM\Entity\Contrat $contrat1, PsrLib\ORM\Entity\Contrat $contrat2) {
            return strcasecmp($contrat1->getAmapien()->getNom(), $contrat2->getAmapien()->getNom());
        });

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Liste des souscripteurs');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]], ];

        $this->excel->getActiveSheet()->setCellValue('A3', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Email');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Tél 1');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Adr');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('G3', 'Ville');

        $this->excel->getActiveSheet()->getStyle('A3:G3')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A3:G3')->getFont()->setBold(true);

        $i = 4;
        foreach ($contrats as $contrat) {
            $amapien = $contrat->getAmapien();
            $this->excel->getActiveSheet()->setCellValue('A'.$i, $this->appfunction->caracteres_speciaux($amapien->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, $this->appfunction->caracteres_speciaux($amapien->getPrenom()));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, $amapien->getEmail());
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('D'.$i, $this->appfunction->caracteres_speciaux($amapien->getNumTel1()));
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('E'.$i, $this->appfunction->caracteres_speciaux($amapien->getLibAdr1()));
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $ville = $amapien->getVille();
            $this->excel->getActiveSheet()->setCellValue('F'.$i, null === $ville ? null : $ville->getCpString());
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('G'.$i, $this->appfunction->caracteres_speciaux(null === $ville ? null : $ville->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A1', $time);

        $nom_fichier = 'liste-des-souscripteurs-de-'.$this->appfunction->caracteres_speciaux($mc_nom).'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * @param null $mc_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_liasse_contrats($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        $mc_nom = $mc->getNom();

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        /** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
        $contrats = $mc->getContrats()->toArray();
        $nbr_contrats = count($contrats);

        /** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $mc_produits */
        $mc_produits = $mc->getProduits()->toArray();
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $mc_dates */
        $mc_dates = $mc->getDates()->toArray();

        $style = [
            'font' => ['bold' => true, 'color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => ['horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER], ];

        $stylePrix = ['font' => ['color' => ['rgb' => '0000FF']]];

        // Un onglet par contrat
        $i = 0;
        foreach ($contrats as $contrat) {
            /** @var \PsrLib\ORM\Entity\ContratCellule[] $cellules */
            $cellules = $contrat->getCellules()->toArray();

            $amapien = $contrat->getAmapien();
            $this->excel->setActiveSheetIndex($i);
            $titre = $amapien->getNom().' '.substr($amapien->getPrenom(), 0, 1).'.';
            $this->excel->getActiveSheet()->setTitle($this->appfunction->caracteres_speciaux($titre));

            // ENTÊTE TABLEAU PRODUITS
            $this->excel->getActiveSheet()->mergeCells('A4:A7', $time);
            $this->excel->getActiveSheet()->setCellValue('A4', 'Dates');
            $this->excel->getActiveSheet()->getStyle('A4:A7')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $this->excel->getActiveSheet()->mergeCells('B4:B7', $time);
            $this->excel->getActiveSheet()->setCellValue('B4', 'Total');
            $this->excel->getActiveSheet()->getStyle('B4:B7')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $j = 'C';
            foreach ($mc_produits as $pro) {
                $prix = $pro->getPrix().' €';

                $k = 4;
                // Type de produit
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getTypeProduction()->getSlug()));
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Nom du produit
                ++$k;
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getNom()));
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

                ++$k;
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($prix));
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($stylePrix);

                ++$k;
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getConditionnement()));
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

                ++$j;
            }

            // DATES ET QUANTITÉS
            $this->excel->getActiveSheet()->setCellValue('A9', 'Cumul');
            $this->excel->getActiveSheet()->getStyle('A9')->applyFromArray($style);

            $k = 11;
            foreach ($mc_dates as $d_l) {
                $this->excel->getActiveSheet()->setCellValue('A'.$k, $d_l->getDateLivraison()->format('Y-m-d'));
                $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle('A'.$k)->applyFromArray($style);

                ++$k;
            }

            // CELLULES
            $k = 11; // Incrémentation des lignes
            $montant_total = 0;
            $produit_quantite_total = [];
            // Initialisation à 0 des valeurs du tableau $produit_quantite_total
            foreach ($mc_produits as $pro) {
                array_push($produit_quantite_total, 0);
            }

            foreach ($mc_dates as $mc_d) {
                $j = 'C'; // Incrémentation des colonnes
                $montant_total_date = 0;
                $index = 0; // Pour $produit_quantite_total

                foreach ($mc_produits as $mc_pro) {
                    $x = 0;

                    foreach ($cellules as $c_c) {
                        if ($mc_pro->getId() == $c_c->getModeleContratProduit()->getId() && $mc_d->getId() == $c_c->getModeleContratDate()->getId()) {
                            $this->excel->getActiveSheet()->setCellValue($j.$k, $c_c->getQuantite());
                            $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                            $this->excel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(false);

                            $produit_quantite_total[$index] += $c_c->getQuantite();
                            $montant_total_date += ($c_c->getQuantite() * $mc_pro->getPrix());
                            ++$x;

                            break;
                        }
                    }

                    if (0 == $x) {
                        $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                    }

                    ++$j;
                    ++$index;
                }

                $montant_total_date = number_format($montant_total_date, 2, '.', '');

                $this->excel->getActiveSheet()->setCellValue('B'.$k, $montant_total_date.' €');
                $this->excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($stylePrix);

                $montant_total += $montant_total_date;
                ++$k;
            }

            $montant_total = number_format($montant_total, 2, '.', '');

            $this->excel->getActiveSheet()->setCellValue('B9', $montant_total.' €');
            $this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($stylePrix);

            $j = 'C';
            foreach ($produit_quantite_total as $p_q_t) {
                $this->excel->getActiveSheet()->setCellValue($j.'9', $p_q_t);
                $this->excel->getActiveSheet()->getStyle($j.'9')->applyFromArray($style);
                ++$j;
            }

            // ENTÊTE
            $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->setCellValue('A2', $time);
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->setCellValue('A1', 'Contrat de '.$this->appfunction->caracteres_speciaux($titre));

            // On ne crée pas de nouvel onglet s'il ne reste plus de produit
            if ($i < ($nbr_contrats - 1)) {
                $this->excel->createSheet();
            }

            ++$i;
        }

        // On replace le curseur sur le premier onglet
        $this->excel->setActiveSheetIndex(0);

        $nom_fichier = 'liste-des-souscripteurs-de-'.$this->appfunction->caracteres_speciaux($mc_nom).'.xls'; // Nom du .xls

        header('Content-Type: application/vnd.ms-excel'); // Mime type
        // dit donne le nom du fichier au navigateur
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"');
        header('Cache-Control: max-age=0'); // Pas de cache

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * @param null $mc_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_feuilles_distribution($mc_id = null)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        $this->data['mc_nom'] = $mc->getNom();

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        $nbr_dates = $mc->getDates()->count();

        // Un onglet par contrat
        $i = 0;
        /** @var \PsrLib\ORM\Entity\ModeleContratDate $mc_d */
        foreach ($mc->getDates() as $mc_d) {
            $this->excel->setActiveSheetIndex($i);

            $style = [
                'font' => ['bold' => true, 'color' => ['rgb' => '000000']],
                'borders' => ['outline' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000'], ]],
                'alignment' => ['horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER], ];

            $stylePrix = ['font' => ['color' => ['rgb' => '0000FF']]];

            // ENTÊTE TABLEAU PRODUITS
            // Nom de l'amapien
            $this->excel->getActiveSheet()->mergeCells('A4:A7');
            $this->excel->getActiveSheet()->setCellValue('A4', 'Nom');
            $this->excel->getActiveSheet()->getStyle('A4:A7')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            // Prénom de l'amapien
            $this->excel->getActiveSheet()->mergeCells('B4:B7');
            $this->excel->getActiveSheet()->getStyle('B4:B7')->applyFromArray($style);
            $this->excel->getActiveSheet()->setCellValue('B4', 'Prénom');
            $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            /** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
            $contrats = $mc->getContrats()->toArray();
            usort($contrats, function (PsrLib\ORM\Entity\Contrat $c1, PsrLib\ORM\Entity\Contrat $c2) {
                return strcasecmp($c1->getAmapien()->getNom(), $c2->getAmapien()->getNom());
            });

            $j = 'C';
            /** @var \PsrLib\ORM\Entity\ModeleContratProduit $pro */
            foreach ($mc->getProduits() as $pro) {
                $prix = $pro->getPrix().' €';

                $k = 4;
                // Type de produit
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getTypeProduction()->getSlug()));
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Nom du produit
                ++$k;
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getNom()));
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Prix du produit
                ++$k;
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($prix));
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($stylePrix);
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Conditionnement du produit
                ++$k;
                $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getConditionnement()));
                $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                ++$j;
            }

            // NOM, PRÉNOM ET QUANTITÉS
            $this->excel->getActiveSheet()->setCellValue('A9', 'Cumul');
            $this->excel->getActiveSheet()->getStyle('A9')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($style);

            $k = 11;
            /** @var \PsrLib\ORM\Entity\Contrat $contrat */
            foreach ($contrats as $contrat) {
                $amapien = $contrat->getAmapien();
                $this->excel->getActiveSheet()->setCellValue('A'.$k, $this->appfunction->caracteres_speciaux($amapien->getNom()));
                $this->excel->getActiveSheet()->getStyle('A'.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

                $this->excel->getActiveSheet()->setCellValue('B'.$k, $this->appfunction->caracteres_speciaux($amapien->getPrenom()));
                $this->excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($style);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

                ++$k;
            }

            // CELLULES
            $k = 11; // Incrémentation des lignes
            $produit_quantite_total = [];
            // Initialisation à 0 des valeurs du tableau $produit_quantite_total
            foreach ($mc->getProduits() as $pro) {
                array_push($produit_quantite_total, 0);
            }

            /** @var \PsrLib\ORM\Entity\Contrat $contrat */
            foreach ($contrats as $contrat) {
                $j = 'C'; // Incrémentation des colonnes
                $index = 0; // Pour $produit_quantite_total

                /** @var \PsrLib\ORM\Entity\ModeleContratProduit $mc_pro */
                foreach ($mc->getProduits() as $mc_pro) {
                    $x = 0;

                    /** @var \PsrLib\ORM\Entity\ContratCellule $c_c */
                    foreach ($contrat->getCellules() as $c_c) {
                        if (
                            $mc_pro->getId() === $c_c->getModeleContratProduit()->getId()
                            && $mc_d->getId() === $c_c->getModeleContratDate()->getId()
                        ) {
                            if ($c_c->getQuantite() > 0.0) {
                                $this->excel->getActiveSheet()->setCellValue($j.$k, $c_c->getQuantite());
                            }
                            $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                            $this->excel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(false);
                            $produit_quantite_total[$index] += $c_c->getQuantite();

                            ++$x;

                            break;
                        }
                    }

                    if (0 == $x) {
                        $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                    }
                    ++$j;
                    ++$index;
                }
                ++$k;
            }

            $j = 'C';
            foreach ($produit_quantite_total as $p_q_t) {
                $this->excel->getActiveSheet()->setCellValue($j.'9', $p_q_t);
                $this->excel->getActiveSheet()->getStyle($j.'9')->applyFromArray($style);
                ++$j;
            }

            // On ne crée pas de nouvel onglet s'il ne reste plus de produit
            if ($i < ($nbr_dates - 1)) {
                $this->excel->createSheet();
            }

            // ENTÊTE (à la fin pour placer le curseur sur la cellule 'A1')
            $titre = $mc_d->getDateLivraison()->format('Y-m-d');
            $this->excel->getActiveSheet()->setTitle($this->appfunction->caracteres_speciaux($titre));

            $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($style);
            $this->excel->getActiveSheet()->setCellValue('A2', $time);
            $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($style);
            $this->excel->getActiveSheet()->setCellValue('A1', 'Feuille de distribution du '.$titre);

            ++$i;
        }

        // On replace le curseur sur le premier onglet
        $this->excel->setActiveSheetIndex(0);

        $nom_fichier = 'feuille-distribution-'.$this->appfunction->caracteres_speciaux($this->data['mc_nom']).'.xls'; // Nom du .xls

        header('Content-Type: application/vnd.ms-excel'); // Mime type
        // dit donne le nom du fichier au navigateur
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"');
        header('Cache-Control: max-age=0'); // Pas de cache

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * @param null $mc_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_synthese_contrat($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY, $mc);

        $this->load->helper('download');

        $slugify = new Slugify();
        $fileName = sprintf(
            'synthese_paiement_%s.xls',
            $slugify->slugify($mc->getNom(), '_')
        );

        $outFileName = $this->excelgenerator->generer_synthese_contrat_reglements($mc);
        force_download(
            $fileName,
            file_get_contents($outFileName),
            true
        );
        unlink($outFileName);
    }

    /**
     * @param $c_id
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_contrat($c_id)
    {
        /** @var \PsrLib\ORM\Entity\Contrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\Contrat::class, $c_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contrat);

        $time = 'Extrait le '.date('d/m/Y H:m:s');
        $amapien_nom = strtoupper($contrat->getAmapien()->getNom());
        $amapien_prenom = ucfirst($contrat->getAmapien()->getPrenom());
        $mc = $contrat->getModeleContrat();
        $mc_nom = $mc->getNom();
        /** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $mc_produits */
        $mc_produits = $mc->getProduits()->toArray();
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $mc_dates_livraison */
        $mc_dates_livraison = $mc->getDates()->toArray();
        /** @var \PsrLib\ORM\Entity\ContratCellule[] $contrat_cellules */
        $contrat_cellules = $contrat->getCellules()->toArray();

        $style = [
            'font' => ['bold' => true, 'color' => ['rgb' => '000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => ['horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER], ];
        $stylePrix = ['font' => ['color' => ['rgb' => '0000FF']]];

        // --

        $this->excel->setActiveSheetIndex(0);
        $titre = $this->appfunction->caracteres_speciaux($amapien_nom).' '.$this->appfunction->caracteres_speciaux($amapien_prenom);
        $titre = substr($titre, 0, 31);
        $this->excel->getActiveSheet()->setTitle($this->appfunction->caracteres_speciaux($titre));

        // ENTÊTE TABLEAU PRODUITS
        $this->excel->getActiveSheet()->mergeCells('A4:A7', $time);
        $this->excel->getActiveSheet()->setCellValue('A4', 'Dates');
        $this->excel->getActiveSheet()->getStyle('A4:A7')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->excel->getActiveSheet()->mergeCells('B4:B7', $time);
        $this->excel->getActiveSheet()->setCellValue('B4', 'Total');
        $this->excel->getActiveSheet()->getStyle('B4:B7')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $j = 'C';
        foreach ($mc_produits as $pro) {
            $prix = $pro->getPrix().' €';

            $k = 4;
            // Type de produit
            $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getTypeProduction()->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

            // Nom du produit
            ++$k;
            $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

            ++$k;
            $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($prix));
            $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($stylePrix);

            ++$k;
            $this->excel->getActiveSheet()->setCellValue($j.$k, $this->appfunction->caracteres_speciaux($pro->getConditionnement()));
            $this->excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

            ++$j;
        }

        // DATES ET QUANTITÉS
        $this->excel->getActiveSheet()->setCellValue('A9', 'Cumul');
        $this->excel->getActiveSheet()->getStyle('A9')->applyFromArray($style);

        $k = 11;
        foreach ($mc_dates_livraison as $d_l) {
            $this->excel->getActiveSheet()->setCellValue('A'.$k, $d_l->getDateLivraison()->format('Y-m-d'));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle('A'.$k)->applyFromArray($style);

            ++$k;
        }

        // CELLULES
        $k = 11; // Incrémentation des lignes
        $montant_total = 0;
        $produit_quantite_total = [];
        // Initialisation à 0 des valeurs du tableau $produit_quantite_total
        foreach ($mc_produits as $pro) {
            array_push($produit_quantite_total, 0);
        }

        foreach ($mc_dates_livraison as $mc_d) {
            $j = 'C'; // Incrémentation des colonnes
            $montant_total_date = 0;
            $index = 0; // Pour $produit_quantite_total

            foreach ($mc_produits as $mc_pro) {
                $x = 0;
                foreach ($contrat_cellules as $c_c) {
                    if ($mc_pro->getId() == $c_c->getModeleContratProduit()->getId() && $mc_d->getId() == $c_c->getModeleContratDate()->getId()) {
                        $this->excel->getActiveSheet()->setCellValue($j.$k, $c_c->getQuantite());
                        $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                        $this->excel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(false);
                        $produit_quantite_total[$index] += $c_c->getQuantite();
                        $montant_total_date += ($c_c->getQuantite() * $mc_pro->getPrix());
                        ++$x;

                        break;
                    }
                }
                if (0 == $x) {
                    $this->excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                }

                ++$j;
                ++$index;
            }

            $montant_total_date = number_format($montant_total_date, 2, '.', '');
            $this->excel->getActiveSheet()->setCellValue('B'.$k, $montant_total_date.' €');
            $this->excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($stylePrix);

            $montant_total += $montant_total_date;
            ++$k;
        }

        $montant_total = number_format($montant_total, 2, '.', '');

        $this->excel->getActiveSheet()->setCellValue('B9', $montant_total.' €');
        $this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($stylePrix);

        $j = 'C';
        foreach ($produit_quantite_total as $p_q_t) {
            $this->excel->getActiveSheet()->setCellValue($j.'9', $p_q_t);
            $this->excel->getActiveSheet()->getStyle($j.'9')->applyFromArray($style);
            ++$j;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A1', 'Contrat de '.$this->appfunction->caracteres_speciaux($titre));

        // On replace le curseur sur le premier onglet
        $this->excel->setActiveSheetIndex(0);

        $nom_fichier = 'contrat-'.$this->appfunction->caracteres_speciaux($mc_nom).'-'.$this->appfunction->caracteres_speciaux($amapien_nom).'-'.$this->appfunction->caracteres_speciaux($amapien_prenom).'.xls'; // Nom du .xls

        header('Content-Type: application/vnd.ms-excel'); // Mime type
        // dit donne le nom du fichier au navigateur
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"');
        header('Cache-Control: max-age=0'); // Pas de cache

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /**
     * @param $c_id
     */
    public function remove($c_id)
    {
        /** @var \PsrLib\ORM\Entity\Contrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\Contrat::class, $c_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EDIT, $contrat);
        $this->em->remove($contrat);

        $this->em->flush();

        $this->addFlash(
            'flash_contract_remove_confirmation',
            'Le contrat a été correctement supprimé.'
        );

        $this->redirect->redirectToReferer('/contrat_signe/contrat_own_existing/');
    }

    /**
     * Supprimer le contrat vierge.
     *
     * @param $mc_id
     */
    public function mc_remove($mc_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_REMOVE, $mc);

        $this->em->remove($mc);
        $this->em->flush();

        redirect('/contrat_signe');
    }

    public function contrat_own_new()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY_OWN);

        /** @var \PsrLib\ORM\Entity\Amapien $currentUser Type granted by permission */
        $currentUser = $this->getUser();
        /** @var \PsrLib\ORM\Entity\ModeleContrat[] $mc_all */
        $mc_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->getSubscribableContractForAmapien($currentUser)
        ;

        // Filtre les contrats sont la dernière date de livraison est passée
        $now = new \Carbon\Carbon();
        $mc_all = array_filter($mc_all, function (PsrLib\ORM\Entity\ModeleContrat $mc) use ($now) {
            $lastDate = $mc->getLastDate();
            if (null === $lastDate) {
                return false;
            }

            return $now->lessThanOrEqualTo($lastDate->getDateLivraison());
        });

        $livLieux = [];
        foreach ($mc_all as $mc) {
            $livLieux[$mc->getId()] = $mc->getLivraisonLieu();
        }

        $attente = [];
        foreach ($mc_all as $mc) {
            $attente[$mc->getId()] = $mc->getFerme()->getAmapienAttentes()->contains($currentUser);
        }

        // Nettoie les données de session du wizard
        $this->clear_session_data();

        $this->loadViewWithTemplate('contrat_signe/contrat_own_new', [
            'mc_all' => $mc_all,
            'livLieux' => $livLieux,
            'attente' => $attente,
        ]);
    }

    public function contrat_edit($contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\Contrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\Contrat::class, $contrat_id);

        $contractForced = $this->request->query->has('force')
            && $this->securitychecker->isGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EDIT_FORCE, $contrat)
        ;

        if (!(
            $contractForced
            || $this->securitychecker->isGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EDIT, $contrat)
        )) {
            $this->exit_error();
        }

        // Met à jour la date de modification
        $contrat->setDateModification(Carbon::now());

        $this->load->library('user_agent');
        $referrer = $this->agent->referrer();
        if ('' === $referrer) {
            $referrer = '/contrat_signe/contrat_own_existing/';
        }
        $wizard = new \PsrLib\DTO\ContratWizard();
        $wizard->setContrat($contrat);
        $wizard->setUrlRetour($referrer);
        $wizard->setBypassNbLibCheck(
            $contractForced
        );

        $this->store_data_to_session($wizard);

        return redirect('/contrat_signe/contrat_subscribe_1');
    }

    /**
     * @param $modele_contrat_id
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function contrat_new_force($modele_contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $modele_contrat_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE, $mc);

        /** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->getAllForceableFromModeleContract($mc)
        ;

        $amapiensIds = array_map(function (PsrLib\ORM\Entity\Amapien $amapien_model) {
            return $amapien_model->getId();
        }, $amapiens);

        $this->form_validation->set_rules('amapien', 'Amapien', 'required|in_list['.implode(',', $amapiensIds).']');

        if ($this->form_validation->run()) {
            $contrat = new \PsrLib\ORM\Entity\Contrat();
            $contrat->setModeleContrat($mc);
            $contrat->setAmapien($this->em->getReference(\PsrLib\ORM\Entity\Amapien::class, $this->input->post('amapien')));
            $contrat->setDateCreation(Carbon::now());

            $wizardDto = new \PsrLib\DTO\ContratWizard();
            $wizardDto->setContrat($contrat);
            $wizardDto->setUrlRetour('/contrat_signe');
            $wizardDto->setBypassNbLibCheck(true);
            $this->store_data_to_session($wizardDto);

            return redirect('contrat_signe/contrat_subscribe_1');
        }

        $this->loadViewWithTemplate('contrat_signe/new_force', [
            'amapiens' => $amapiens,
        ]);
    }

    /**
     * @param null $modele_contrat_id
     */
    public function contrat_subscribe_1($modele_contrat_id = null)
    {
        if (null === $modele_contrat_id) {
            $wizard = $this->get_data_from_session();
            if (false === $wizard) {
                redirect('/contrat_signe');
            }
            $contrat = $wizard->getContrat();
            $mc = $contrat->getModeleContrat();
        } else {
            $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $modele_contrat_id);
            $wizard = new \PsrLib\DTO\ContratWizard();
            $wizard->setUrlRetour('/contrat_signe/contrat_own_new/');
        }
        $this->checkWizardPermission($wizard, $mc);

        // Add validation rules for inputs
        /** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $produits */
        $produits = $mc->getProduits()->toArray();
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $dates */
        $dates = $mc->getDates()->toArray();
        foreach ($dates as $date) {
            foreach ($produits as $produit) {
                $inputName = 'commande['.$date->getId().']['.$produit->getId().']';
                $this->form_validation->set_rules($inputName, 'Commande', 'numeric|greater_than_equal_to[0]');
            }
        }

        $validation = null;
        if ($this->form_validation->run()) {
            /** @var \PsrLib\ORM\Entity\ContratCellule[] $commande */
            $commande = $this->contrat_signe_cellule_model_builder->buildFromInput(
                $this->input->post('commande') ?? []
            );

            $validation = $this->commandevalidation->validate($commande, $mc, $wizard->isBypassNbLibCheck());

            if (null === $validation) {
                if (null !== $wizard->getContrat()) {
                    $contrat = $wizard->getContrat();
                } else {
                    $currentUser = $this->getUser();
                    if (!($currentUser instanceof \PsrLib\ORM\Entity\Amapien)) {
                        throw new \LogicException('Statut invalide pour souscrire un contrat de cette manière');
                    }
                    $contrat = new \PsrLib\ORM\Entity\Contrat();
                    $contrat->setModeleContrat($mc);
                    $contrat->setAmapien($currentUser);
                    $contrat->setDateCreation(Carbon::now());
                }

                $contrat->getCellules()->clear();
                foreach ($commande as $cellule) {
                    $contrat->addCellule($cellule);
                }

                $wizard->setContrat($contrat);

                $this->store_data_to_session($wizard);

                return redirect('contrat_signe/contrat_subscribe_2');
            }
        }

        $this->form_validation->set_message('commande', 'test');

        $ferme = $mc->getFerme();
        $livraison_lieu = $mc->getLivraisonLieu();

        $this->loadViewWithTemplate('contrat_signe/contrat_subscribe_1', [
            'contrat' => $mc,
            'ferme' => $ferme,
            'livraison_lieu' => $livraison_lieu,
            'commande' => isset($contrat) ? $contrat->getCellules()->toArray() : [],
            'url_retour' => $wizard->getUrlRetour(),
            'validation_error' => $validation,
            'bypassNbLivCheck' => $wizard->isBypassNbLibCheck(),
        ]);
    }

    public function contrat_subscribe_2()
    {
        $wizard = $this->get_data_from_session();
        if (false === $wizard) {
            redirect('/contrat_signe/contrat_own_new/');
        }

        $contrat = $wizard->getContrat();
        $mc = $contrat->getModeleContrat();
        /** @var \PsrLib\ORM\Entity\ModeleContratDatesReglement[] $dates_reglement */
        $dates_reglement = $mc->getDateReglements()->toArray();

        $this->form_validation->set_rules('reglement[]', 'Reglements', 'required|no_duplication['.implode(',', $this->input->post('reglement') ?? []).']', [
            'no_duplication' => 'Merci de ne pas sélectionner plus d\'une fois chaque date.',
            'required' => 'Merci de sélectionner une date de règlement pour toutes les échéances.',
        ]);
        $this->form_validation->set_rules('mode', 'Mode de règlements', 'required|in_list['.implode(',', $mc->getReglementType()).']', [
            'required' => 'Merci de sélectionner un mode de règlement.',
        ]);

        if ($this->form_validation->run()) {
            $contrat->setReglementType($this->input->post('mode'));

            $postReglements = $this->input->post('reglement[]');
            $prix = $this->calc_prix(count($postReglements));
            $i = 0;
            $contrat->getDatesReglements()->clear();
            foreach ($postReglements as $mc_date_id) {
                $new = new \PsrLib\ORM\Entity\ContratDatesReglement();
                $new->setMontant($prix[$i++]);
                $new->setModeleContratDatesReglement($this->em->getReference(\PsrLib\ORM\Entity\ModeleContratDatesReglement::class, $mc_date_id));
                $contrat->addDatesReglement($new);
            }
            $wizard->setContrat($contrat);
            $this->store_data_to_session($wizard);

            return redirect('/contrat_signe/contrat_subscribe_3/');
        }

        $ferme = $mc->getFerme();
        $livraison_lieu = $mc->getLivraisonLieu();

        $reglementsInput = [];
        foreach ($this->input->post('reglement[]') ?? [] as $reglementId) {
            if ('' !== $reglementId) {
                $reglementsInput[] = $this->em->getReference(\PsrLib\ORM\Entity\ModeleContratDatesReglement::class, $reglementId);
            }
        }

        $this->loadViewWithTemplate('contrat_signe/contrat_subscribe_2', [
            'mc' => $mc,
            'ferme' => $ferme,
            'livraison_lieu' => $livraison_lieu,
            'dates_reglement' => $dates_reglement,
            'total' => $this->commande_montant_total($contrat->getCellules()->toArray()),
            'reglements_input' => $reglementsInput,
            'reglements_existants' => $contrat->getDatesReglements()->toArray(),
            'contrat' => $contrat,
            'url_retour' => $wizard->getUrlRetour(),
        ]);
    }

    public function contrat_subscribe_3()
    {
        $wizard = $this->get_data_from_session();
        if (false === $wizard) {
            redirect('/contrat_signe/contrat_own_new/');
        }

        $this->form_validation->set_rules('confirm-sign', 'Confirmation de signature', 'required');
        $this->form_validation->set_rules('confirm-charter', 'Confirmation de la charte', 'required');

        if ($this->form_validation->run()) {
            $contrat = $wizard->getContrat();

            // Vérifie que le contrat n'existe pas en BDD, par ex si on a ouvert le wizard sur deux onglets en meme temps
            // et qu'on en a complété un en premier
            if (null === $contrat->getId()) {
                $existingContract = $this
                    ->em
                    ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
                    ->findOneBy([
                        'amapien' => $contrat->getAmapien(),
                        'modeleContrat' => $contrat->getModeleContrat(),
                    ])
                ;
                if (null !== $existingContract) {
                    $this->addFlash(
                        'notice_error',
                        'Une erreur est survenue. Merci de recommencer.'
                    );

                    return redirect($wizard->getUrlRetour());
                }
            }

            // remplace le contrat en BDD. Permis parce que pas de lien en BDD sur les contrats
            if (null !== $contrat->getId()) {
                $this->em->remove($this->em->getReference(\PsrLib\ORM\Entity\Contrat::class, $contrat->getId()));
            }

            // Generation du fichier de contrat definitif
            $fileName = $this->mpdfgeneration->genererContratSigneFile($contrat);
            $pdf = new \PsrLib\ORM\Entity\Files\ContratPdf($fileName);
            $contrat->setPdf($pdf);

            $this->em->persist($contrat);
            $this->em->flush();

            $this->clear_session_data();

            return redirect($wizard->getUrlRetour());
        }

        $this->loadViewWithTemplate('contrat_signe/contrat_subscribe_3', [
            'url_retour' => $wizard->getUrlRetour(),
        ]);
    }

    public function preview_pdf()
    {
        $wizard = $this->get_data_from_session();
        if (false === $wizard) {
            redirect('/contrat_signe/contrat_own_new/');
        }

        $this->mpdfgeneration->genererContratSigneInline(
            $wizard->getContrat()
        );
    }

    /**
     * @return false|string
     */
    public function ajax_calc_prix()
    {
        $res = [];

        $nb_reglements = (int) $this->input->get('nb_reglements');
        if ($nb_reglements <= 0) {
            return json_encode($res);
        }

        $prix = $this->calc_prix($nb_reglements);
        foreach ($prix as $p) {
            $res[] = \PsrLib\Services\MoneyHelper::toString($p);
        }
        echo json_encode($res);
    }

    public function contrat_own_existing()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY_OWN);

        /** @var \PsrLib\ORM\Entity\Amapien $currentUser Granted by permission */
        $currentUser = $this->getUser();

        /** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
            ->findBy([
                'amapien' => $currentUser,
            ])
        ;

        // Filtre les contrats sont la dernière date de livraison est passée
        $contrats = array_filter($contrats, function (PsrLib\ORM\Entity\Contrat $contrat) {
            return !$contrat->getModeleContrat()->isArchived();
        });

        $this->loadViewWithTemplate('contrat_signe/contrat_own_existing', [
            'contrats' => $contrats,
        ]);
    }

    public function contrat_own_archived()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY_OWN);

        /** @var \PsrLib\ORM\Entity\Amapien $currentUser Granted by permission */
        $currentUser = $this->getUser();

        /** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
            ->findBy([
                'amapien' => $currentUser,
            ])
        ;

        // Filtre les contrats sont la dernière date de livraison est passée
        $contrats = array_filter($contrats, function (PsrLib\ORM\Entity\Contrat $contrat) {
            return $contrat->getModeleContrat()->isArchived();
        });

        $this->loadViewWithTemplate('contrat_signe/contrat_own_archived', [
            'contrats' => $contrats,
        ]);
    }

    /**
     * @param $contrat_id
     */
    public function contrat_pdf($contrat_id)
    {
        $this->load->helper('download');

        /** @var \PsrLib\ORM\Entity\Contrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EXPORT_PDF, $contrat);

        $mc = $contrat->getModeleContrat();
        $amap = $mc->getLivraisonLieu()->getAmap();
        $ferme = $mc->getFerme();
        $creationDate = $contrat->getDateCreation();
        $fileName = $mc->getNom()
            .'-'
            .$amap->getNom()
            .'-'
            .$ferme->getNom()
            .'-'
            .$creationDate->format('d.m.Y')
            .'.pdf'
        ;

        $fileContent = file_get_contents($contrat->getPdf()->getFileFullPath());

        force_download($fileName, $fileContent, true);
    }

    public function contrat_deplacement($contrat_id)
    {
        /** @var \PsrLib\ORM\Entity\Contrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_MOVE_DATE, $contrat);

        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $contrat->getModeleContrat();
        /** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $produits */
        $produits = $mc->getProduits()->toArray();
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $datesContrat */
        $datesContrat = $mc->getDates()->toArray();
        $ferme = $mc->getFerme();
        /** @var \PsrLib\ORM\Entity\ContratCellule[] $cellules */
        $cellules = $contrat->getCellules()->toArray();

        $datesSrc = [];
        $datesDst = [];
        // Filtre les datesSrc pour ne pas prendre en compte les déplacements passés
        foreach ($datesContrat as $dateContrat) {
            $dateLivMin = $ferme->getPremiereDateLivrableAvecDelais(Carbon::now());

            if ($dateLivMin->lte($dateContrat->getDateLivraison())) {
                $datesSrc[] = $dateContrat;
                $datesDst[] = $dateContrat;
            }
        }

        // Filtre les datesSrc pour lesquelles il n'existe pas de livraisons
        $datesSrc = array_filter($datesSrc, function (PsrLib\ORM\Entity\ModeleContratDate $date) use ($cellules) {
            return \PsrLib\ORM\Repository\ContratCelluleRepository::countNbLivraisonsByDate($cellules, $date) > 0;
        });

        // Filtre les datesSrc pour lesquelles le report ou le déplacement est possible
        $datesDst = array_filter($datesDst, function (PsrLib\ORM\Entity\ModeleContratDate $date) use ($cellules, $mc) {
            $nbLivs = \PsrLib\ORM\Repository\ContratCelluleRepository::countNbLivraisonsByDate($cellules, $date);
            if ($mc->getAmapienPermissionReportLivraison() && !$mc->getAmapienPermissionDeplacementLivraison()) {
                return $nbLivs > 0;
            }

            if (!$mc->getAmapienPermissionReportLivraison() && $mc->getAmapienPermissionDeplacementLivraison()) {
                return 0.0 === $nbLivs;
            }

            return $nbLivs >= 0.0;
        });

        $errorMessage = null;

        $returnUrl = $this->input->get('ret');
        if (null === $returnUrl) {
            $returnUrl = '/contrat_signe';
        }

        $this->form_validation->set_rules('date_deplacement_src', 'Date de déplacement', 'required');
        if ($this->form_validation->run()) {
            $date_deplacement_src_id = $this->input->post('date_deplacement_src');

            // Input validation
            $inputDatesDst = $this->input->post('date_deplacement_dst');
            foreach ($inputDatesDst as $inputDate) {
                $found = array_filter($datesDst, function (PsrLib\ORM\Entity\ModeleContratDate $date) use ($inputDate) {
                    return $date->getId() === (int) $inputDate;
                });
                if (empty($found)) {
                    $errorMessage = 'Erreur de validation du formulaire';
                }
            }
            $dstProduits = $this->input->post('produit_deplacement');
            $productKeys = [];
            foreach ($dstProduits as $produit) {
                $id = array_keys($produit)[0];
                $count = $produit[$id];
                if (!array_key_exists($id, $productKeys)) {
                    $productKeys[$id] = $count;

                    continue;
                }

                if ($count !== $productKeys[$id]) {
                    $errorMessage = 'Erreur de validation du formulaire';
                }
            }

            if (null === $errorMessage) {
                // Remove cellules from date
                foreach ($cellules as $cellule) {
                    if ($cellule->getModeleContratDate()->getId() === (int) $date_deplacement_src_id) {
                        $cellule->setQuantite(0);
                    }
                }

                //TODO test des exclusions ?

                $errorMessage = null;
                $nbDeplacements = 0;
                $nbReports = 0;
                foreach ($inputDatesDst as $row => $dateId) {
                    $dateDst = array_filter($datesDst, function (PsrLib\ORM\Entity\ModeleContratDate $date) use ($dateId) {
                        return $date->getId() === (int) $dateId;
                    });
                    $dateDst = array_pop($dateDst);
                    if (\PsrLib\ORM\Repository\ContratCelluleRepository::countNbLivraisonsByDate($cellules, $dateDst) > 0) {
                        ++$nbReports;
                    } else {
                        ++$nbDeplacements;
                    }
                    foreach ($dstProduits[$row] as $produitId => $newValue) {
                        foreach ($cellules as $cellule) {
                            if ((int) $cellule->getModeleContratDate()->getId() === (int) $dateId
                                && (int) $cellule->getModeleContratProduit()->getId() === (int) $produitId) {
                                $cellule->setQuantite((float) $cellule->getQuantite() + (float) $newValue);
                            }
                        }
                    }
                }

                // Test move count only if report restriction
                if ($mc->getAmapienPermissionReportLivraison()
                    || $mc->getAmapienPermissionDeplacementLivraison()) {
                    if ((int) $contrat->getDeplacementsEffectues() + $nbDeplacements > (int) $mc->getAmapienDeplacementNb()) {
                        $errorMessage = sprintf(
                            'Erreur : vous essayez d\'effectuer %s déplacements. Il vous en reste %s',
                            $nbDeplacements,
                            (int) $mc->getAmapienDeplacementNb() - (int) $contrat->getDeplacementsEffectues()
                        );
                    }

                    if ((int) $contrat->getReportEffectues() + $nbReports > (int) $mc->getAmapienReportNb()) {
                        $errorMessage = sprintf(
                            'Erreur : vous essayez d\'effectuer %s reports. Il vous en reste %s',
                            $nbReports,
                            (int) $mc->getAmapienReportNb() - (int) $contrat->getReportEffectues()
                        );
                    }
                }

                if (null === $errorMessage) {
                    // Incrémente le compteur de déplacements
                    $contrat->setDeplacementsEffectues((int) $contrat->getDeplacementsEffectues() + $nbDeplacements);
                    $contrat->setReportEffectues((int) $contrat->getReportEffectues() + $nbReports);

                    $this->em->flush();

                    return redirect(site_url($returnUrl));
                }
            }
        }

        $exclusions = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratProduitExclure::class)
            ->getByContrat($mc)
        ;

        $page = $this->load->view('contrat_signe/contrat_deplacement', [
            'mc' => $mc,
            'contrat' => $contrat,
            'cellules' => $cellules,
            'produits' => $produits,
            'datesSrc' => $datesSrc,
            'datesDst' => $datesDst,
            'exclusions' => $exclusions,
            'errorMessage' => $errorMessage,
            'returnUrl' => $returnUrl,
        ], true);

        return $this->load->view('template', ['page' => $page]);
    }

    public function contrat_paysan_archived()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_LIST_ARCHIVED);

        /** @var \PsrLib\ORM\Entity\Paysan $currentUser Granted by permission */
        $currentUser = $this->getUser();

        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchContratState = new \PsrLib\DTO\SearchContratSigneState();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratSignePaysanType::class, $searchContratState, [
            'currentUser' => $currentUser,
            'filter_archived' => true,
        ]);
        $searchForm->handleRequest($this->request);

        $contrats = [];
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $contrats = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
                ->search($searchContratState)
            ;
        }

        $this->loadViewWithTemplate('contrat_signe/contrat_paysan_archived', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
        ]);
    }

    /**
     * Accueil.
     */
    private function accueil()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_LIST);

        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchContratState = new \PsrLib\DTO\SearchContratSigneState();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratSigneType::class, $searchContratState, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
            ->search($searchContratState)
        ;

        $this->loadViewWithTemplate('contrat_signe/display_all', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
        ]);
    }

    private function accueil_amap(PsrLib\ORM\Entity\Amap $amap)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_LIST);

        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratSigneAmapType::class, null, [
            'amap_choices' => [$amap],
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $searchContratState = $searchForm->getData();
        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
            ->search($searchContratState)
        ;

        $this->loadViewWithTemplate('contrat_signe/display_all_amap', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
        ]);
    }

    private function accueil_ref_produit(PsrLib\ORM\Entity\Amapien $amapien)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_LIST);

        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchContratState = new \PsrLib\DTO\SearchContratSigneState();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratSigneRefProduitType::class, $searchContratState, [
            'refProduit' => $amapien,
        ]);
        $searchForm->handleRequest($this->request);

        $contrats = [];
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $contrats = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
                ->search($searchContratState)
            ;
        }

        $this->loadViewWithTemplate('contrat_signe/display_all_amap', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
        ]);
    }

    private function accueil_paysan(UserWithFermes $paysan)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_LIST);

        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchContratState = new \PsrLib\DTO\SearchContratSigneState();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratSignePaysanType::class, $searchContratState, [
            'currentUser' => $paysan,
            'filter_archived' => false,
        ]);
        $searchForm->handleRequest($this->request);

        $contrats = [];
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $contrats = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
                ->search($searchContratState)
            ;
        }

        $this->loadViewWithTemplate('contrat_signe/display_all_paysan', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
        ]);
    }

    /**
     * @param $nb_reglements
     *
     * @return Money[]
     */
    private function calc_prix($nb_reglements)
    {
        $wizard = $this->get_data_from_session();
        if (null === $wizard) {
            redirect('/contrat_signe/contrat_own_new/');
        }

        $total = $this->commande_montant_total($wizard->getContrat()->getCellules()->toArray());

        return $total->allocateTo($nb_reglements);
    }

    /**
     * @param \PsrLib\ORM\Entity\ContratCellule[] $commandes
     */
    private function commande_montant_total(array $commandes): Money
    {
        $total = Money::EUR(0);
        foreach ($commandes as $commande) {
            $produitPrixUnitaire = \PsrLib\Services\MoneyHelper::fromString($commande->getModeleContratProduit()->getPrix());
            $produitTotal = $produitPrixUnitaire->multiply($commande->getQuantite());
            $total = $total->add($produitTotal);
        }

        return $total;
    }

    /**
     * @return \PsrLib\DTO\ContratWizard | false
     */
    private function get_data_from_session()
    {
        try {
            $deserialized = $this
                ->serializer
                ->deserialize($this->sfSession->get('c_wizard'), \PsrLib\DTO\ContratWizard::class, 'json', [
                    'groups' => 'wizardContract',
                ])
            ;
        } catch (\Exception $e) {
            $deserialized = false;
        }

        return $deserialized;
    }

    private function store_data_to_session(PsrLib\DTO\ContratWizard $wizard)
    {
        $this->sfSession->set(
            'c_wizard',
            $this->serializer->serialize($wizard, 'json', [
                'groups' => 'wizardContract',
            ])
        );
    }

    private function clear_session_data()
    {
        $this->sfSession->set('c_wizard', null);
    }

    private function checkWizardPermission(?PsrLib\DTO\ContratWizard $wizard, PsrLib\ORM\Entity\ModeleContrat $modeleContrat)
    {
        if (null === $wizard) {
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE, $modeleContrat);

            return;
        }
        $contrat = $wizard->getContrat();
        if (null === $contrat) {
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE, $modeleContrat);

            return;
        }

        if ($wizard->isBypassNbLibCheck()) {
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE, $contrat->getModeleContrat());

            return;
        }

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE, $contrat->getModeleContrat());
    }
}
