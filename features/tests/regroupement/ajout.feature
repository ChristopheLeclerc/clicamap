# language: fr
Fonctionnalité: Création d'un regroupement depuis différents profils

  @database
  Plan du Scénario: Création du regroupement
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Et que je suis sur "/evenement"
    Quand je suis "Gestion des regroupements"
    Et que je suis "Ajouter un regroupement"
    Et que je remplis "ferme_regroupement[nom]" avec "Nouveau regroupement"
    Et que je remplis "ferme_regroupement[common][email]" avec "nouveauregroupement@test.amap-aura.org"
    Et que je remplis "ferme_regroupement[common][siret]" avec "77282657406395"
    Et que je remplis "ferme_regroupement[common][adresseAdmin]" avec "adresse"
    Et que je remplis "ferme_regroupement[common][ville]" avec "69001, LYON 1er Arrondissement"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "Regroupement ajouté"
    Et que je devrais voir "Nouveau regroupement"

    Exemples:
      | mail                          |
      | admin@test.amap-aura.org      |
      | admin.aura@test.amap-aura.org |

  @database
  Plan du Scénario: Test des messages d'erreur
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/evenement"
    Quand je suis "Gestion des regroupements"
    Et que je suis "Ajouter un regroupement"
    Et que je remplis "ferme_regroupement[nom]" avec "<nom>"
    Et que je remplis "ferme_regroupement[common][email]" avec "<email>"
    Et que je remplis "ferme_regroupement[common][siret]" avec "<siret>"
    Et que je remplis "ferme_regroupement[common][adresseAdmin]" avec "<adresse>"
    Et que je remplis "ferme_regroupement[common][ville]" avec "<ville>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "<message>"

    Exemples:
      | nom                  | email                                  | siret          | adresse | ville                          | message                                                    |
      |                      | nouveauregroupement@test.amap-aura.org | 77282657406395 | adresse | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.                        |
      | Nouveau regroupement |                                        | 77282657406395 | adresse | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.                        |
      | Nouveau regroupement | regroupementtest@t                     | 77282657406395 | adresse | 69001, LYON 1er Arrondissement | Cette valeur n'est pas une adresse email valide.           |
      | Nouveau regroupement | nouveauregroupement@test.amap-aura.org |                | adresse | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.                        |
      | Nouveau regroupement | nouveauregroupement@test.amap-aura.org | 1111111111     | adresse | 69001, LYON 1er Arrondissement | Le SIRET doit faire exactement 14 caractères.              |
      | Nouveau regroupement | nouveauregroupement@test.amap-aura.org | 11111111111111 | adresse | 69001, LYON 1er Arrondissement | Le SIRET est invalide.                                     |
      | Nouveau regroupement | nouveauregroupement@test.amap-aura.org | 77282657406395 |         | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.                        |
      | Nouveau regroupement | nouveauregroupement@test.amap-aura.org | 77282657406395 |         |                                | Le champ \"Code postal, Ville administrative\" est requis. |
      | Nouveau regroupement | nouveauregroupement@test.amap-aura.org | 77282657406395 |         | invalid                        | Le champ \"Code postal, Ville\" est mal formaté.           |
