<?php

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CodeIgniterFormExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('ci_form_error', [$this, 'renderCiFormError'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * Bind form_error from internal Ci form management.
     *
     * @param mixed $field
     * @param mixed $prefix
     * @param mixed $suffix
     */
    public function renderCiFormError($field = '', $prefix = '', $suffix = ''): string
    {
        return form_error($field, $prefix, $suffix);
    }
}
