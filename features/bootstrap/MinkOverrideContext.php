<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\ResponseTextException;

class MinkOverrideContext extends \Behat\MinkExtension\Context\MinkContext
{
    /**
     * Based on Behat's own example.
     *
     * @see http://docs.behat.org/en/v2.5/cookbook/using_spin_functions.html#adding-a-timeout
     *
     * @param $lambda
     * @param int $wait
     *
     * @throws \Exception
     */
    public function spin($lambda, $wait = 15)
    {
        $time = time();
        $stopTime = $time + $wait;
        while (time() < $stopTime) {
            try {
                if ($lambda($this)) {
                    return;
                }
            } catch (\Exception $e) {
                // do nothing
            }

            usleep(250000);
        }

        throw new \Exception("Spin function timed out after {$wait} seconds");
    }

    /**
     * {@inheritdoc}
     */
    public function assertPageContainsText($text)
    {
        $this->spin(function (MinkOverrideContext $context) use ($text) {
            try {
                $context->assertSession()->pageTextContains($this->fixStepArgument($text));

                return true;
            } catch (ResponseTextException $e) {
                // NOOP
            }

            return false;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function assertPageNotContainsText($text)
    {
        $this->spin(function (MinkOverrideContext $context) use ($text) {
            try {
                parent::assertPageNotContainsText($text);

                return true;
            } catch (ResponseTextException $e) {
                // NOOP
            }

            return false;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function clickLink($link)
    {
        $this->spin(function (MinkOverrideContext $context) use ($link) {
            try {
                parent::clickLink($link);

                return true;
            } catch (ElementNotFoundException $e) {
                // NOOP
            }

            return false;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function assertElementOnPage($element)
    {
        $this->spin(function (MinkOverrideContext $context) use ($element) {
            try {
                $context->assertSession()->elementExists('css', $element);

                return true;
            } catch (ResponseTextException $e) {
                // NOOP
            }

            return false;
        });
    }

    /**
     * @When Je clique sur l'élément :elementSelector
     */
    public function clickOnElement(string $elementSelector)
    {
        $this->fixStepArgument($elementSelector);
        $this->assertElementOnPage($elementSelector);
        $this->getSession()->evaluateScript(
            sprintf("$('%s').trigger('click')", $elementSelector)
        );
    }

    /**
     * @When je suis :lien sur la ligne contenant :ligne
     */
    public function jeSuisSurLaLigneContenant(string $ligne, string $lien)
    {
        $ligne = $this->fixStepArgument($ligne);
        $lien = $this->fixStepArgument($lien);

        $row = $this->getRowWithText($ligne);

        $row->clickLink($lien);
    }

    /**
     * @Then je devrais voir :texte sur la ligne contenant :ligne
     */
    public function jeDevraisVoirSurLaLigneContenant(string $ligne, string $texte)
    {
        $ligne = $this->fixStepArgument($ligne);
        $texte = $this->fixStepArgument($texte);

        $row = $this->getRowWithText($ligne);

        if (!str_contains($row->getText(), $texte)) {
            throw new ElementNotFoundException($this->getSession()->getDriver(), 'text', 'Unable to find row containing text');
        }
    }

    private function getRowWithText($line)
    {
        $driver = $this->getSession()->getDriver();
        $row = $driver->find(
            sprintf('//table//*[text()[contains(.,\'%s\')]]//ancestor-or-self::tr', $line)
        );

        if (1 !== count($row)) {
            throw new ElementNotFoundException($driver, 'text', 'Unable to find row containing text');
        }

        return $row[0];
    }
}
