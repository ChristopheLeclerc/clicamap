<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var string $url_retour */
/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
/** @var \PsrLib\ORM\Entity\Ferme $ferme */
/** @var \PsrLib\ORM\Entity\ContratCellule[] $commande */
/** @var string $validation_error */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraison_lieu */
/** @var bool $bypassNbLivCheck */
?>

<h3>Étape 1 - Sélection des quantités</h3>

<h5>
    <?php if ('/contrat_signe/contrat_own_new/' === $url_retour): ?>
        <a href="<?= site_url('/contrat_signe/contrat_own_new/'); ?>">Les nouveaux contrats disponibles</a>
    <?php elseif (str_contains($url_retour, 'contrat_signe')): ?>
        <a href="<?= $url_retour; ?>">Contrats signés</a>
    <?php else: ?>
        <a href="<?= $url_retour; ?>">Mes contrats existants</a>
    <?php endif; ?>
    <i class="glyphicon glyphicon-menu-right"></i>
    Souscription
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 1
</h5>

<div class="row">
    <div class="col-md-12">
        <?php $modeleContrat = $contrat;

include '_info_modele_contrat.php'; ?>
    </div>

    <?php if (null !== $validation_error): ?>
        <div class="col-md-12">
            <div class="alert alert-danger">
                <span><?=$validation_error; ?></span>
            </div>
        </div>
    <?php endif; ?>

    <div class="col-md-12">

        <div class="well well-sm">
            <?=render_contrat_commande_form(
    $contrat,
    $url_retour,
    site_url(uri_string()),
    'Suivant',
    $commande,
    $bypassNbLivCheck ? '<div class="alert alert-danger">Attention, avec ce module de gestion des contrats signés, les contrôles cités dans le bandeau bleu ne s\'appliquent pas lors de la souscription d\'un contrat.</div>' : ''
); ?>
        </div>

    </div>

</div>
