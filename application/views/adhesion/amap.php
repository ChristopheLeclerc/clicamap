<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var PsrLib\ORM\Entity\Region[] $regions */
/** @var PsrLib\ORM\Entity\Departement[] $departments */
/** @var \PsrLib\ORM\Entity\AdhesionAmap[] $adhesions */
/** @var \PsrLib\ORM\Entity\Region $selectedRegion */
/** @var \PsrLib\ORM\Entity\Departement $selectedDepartment */
/** @var int[] $adhesionYears */
/** @var int $selectedYear */
/** @var ?bool $selectedIsStudent */
/** @var string $selectedSearchKeyword */
?>

<div class="col-md-12">
    <h3>Reçus AMAP</h3>
</div>

<div class="col-md-12">
    <div class="well">
        <form action="/adhesion/amap" class="js-form-search">
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Région</button>
                    </span>
                        <?=form_dropdown(
    'search_region',
    [-1 => null] + array_column_object_key($regions, 'id', 'nom'),
    null === $selectedRegion ? -1 : $selectedRegion->getId(),
    ['class' => 'form-control']
); ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Département</button>
                    </span>
                        <?=form_dropdown(
    'search_department',
    [-1 => null] + array_column_object_key($departments, 'id', 'nom'),
    null === $selectedDepartment ? -1 : $selectedDepartment->getId(),
    ['class' => 'form-control']
); ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Année d'adhésion</button>
                    </span>

                        <?=form_dropdown(
    'search_year',
    [-1 => null] + array_combine($adhesionYears, $adhesionYears),
    $selectedYear,
    ['class' => 'form-control']
); ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Type</button>
                    </span>
                        <?php
                            if (true === $selectedIsStudent) {
                                $selectedIsStudentSelectValue = '1';
                            } elseif (false === $selectedIsStudent) {
                                $selectedIsStudentSelectValue = '0';
                            } else {
                                $selectedIsStudentSelectValue = '';
                            }
                        ?>
                        <?=form_dropdown(
                            'search_student',
                            [
                                '' => '',
                                '1' => 'AMAP Étudiante',
                                '0' => 'AMAP',
                            ],
                            $selectedIsStudentSelectValue,
                            ['class' => 'form-control']
                        ); ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Mot clé</button>
                    </span>
                        <?=form_input(
                            'search_keyword',
                            $selectedSearchKeyword,
                            ['class' => 'form-control']
                        ); ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <a class="btn btn-success pull-right" href="<?=site_url('/adhesion/amap'); ?>">Réinitialiser les critères</a>
                </div>
            </div>
        </form>
    </div>
</div>

<form method="POST" action="/adhesion/amap_action">
    <div class="col-md-12">
        <?php if (count($adhesions) > 0): ?>
            <button
                class="btn btn-success js-trigger-modal"
                name="action-generate"
                type="submit"
                data-title="Êtes-vous sûr ?"
                data-message="Vous allez générer tous les reçus en brouillon. Cette action va se lancer dès la prochaine minute. Veuillez actualiser la page afin d’accéder aux reçus."
                data-confirm-class="btn-danger"
            >Générer les reçus</button>
            <button
                    class="btn btn-success"
                    name="action-download"
                    type="submit"
            >Télécharger</button>
            <button
                class="btn btn-danger js-trigger-modal"
                name="action-delete"
                type="submit"
                data-title="Êtes-vous sûr ?"
                data-message="Vous allez supprimer les reçus sélectionnés. Cette action est irréversible"
                data-confirm-class="btn-danger"
            >Supprimer les reçus</button>
        <?php endif; ?>

        <?php foreach ($adhesions as $adhesion): ?>
            <input
                type="checkbox"
                class="js-input-binded hidden"
                name="<?= sprintf('selected[%d]', $adhesion->getId()); ?>"
            />
        <?php endforeach; ?>
    </div>
</form>

<?php if (count($adhesions) > 0): ?>
    <div class="col-md-12">
        <table class="table table-datatable" data-order='[[ 1, "desc" ]]'>
            <thead>
            <tr>
                <th data-orderable="false">
                    <input
                        type="checkbox"
                        class="js-select-all"
                    />
                </th>
                <th>Numéro de reçu</th>
                <th>Nom</th>
                <th>Année</th>
                <th>Statut</th>
                <th>Createur</th>
                <th>Outils</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($adhesions as $adhesion): ?>
                <tr>
                    <td>
                        <input
                            type="checkbox"
                            class="js-auto-selectable js-binder"
                            data-target-name="<?= sprintf('selected[%d]', $adhesion->getId()); ?>"
                        />
                    </td>
                    <td><?= $adhesion->getValue()->getVoucherNumber(); ?></td>
                    <td><?= $adhesion->getAmap(); ?></td>
                    <td><?= $adhesion->getValue()->getYear(); ?></td>
                    <td><?= \PsrLib\ORM\Entity\Adhesion::STATE_LABEL[$adhesion->getState()]; ?></td>
                    <td><?= $adhesion->getCreator()->getEmail(); ?></td>
                    <td>
                        <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DOWNLOAD, $adhesion)) : ?>
                            <a title="Télécharger le reçu" href="<?=site_url('/adhesion/amap_download/'.$adhesion->getId()); ?>">
                                <i class="glyphicon glyphicon-file" data-toggle="tooltip" title="Télécharger le reçu"></i>
                            </a>
                        <?php endif; ?>

                        <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DELETE, $adhesion)) : ?>
                            <form method="POST" action="<?=site_url('/adhesion/amap_delete/'.$adhesion->getId()); ?>" class="form-inline">
                                <button
                                    class="btn btn-link js-trigger-modal"
                                    data-title="Êtes-vous sûr ?"
                                    data-message="Vous allez supprimer le reçu sélectionné. Cette action est irréversible"
                                    data-confirm-class="btn-danger"
                                    type="submit">
                                    <i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le reçu"></i>
                                </button>
                            </form>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
