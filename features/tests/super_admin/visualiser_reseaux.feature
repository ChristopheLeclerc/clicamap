# language: fr
Fonctionnalité: Vérification des liens des réseaux avec le profil super-admin

  Scénario: J'ai accès aux liens depuis l'accueil
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Et que je suis sur "/evenement"
    Lorsque je suis "Réseaux"
    Et que je suis "Régions & Départements"
    Alors je devrais être sur "/region"
    Et je devrais voir "À l'aide des outils de cette liste, vous pourrez ajouter des administrateurs aux régions ainsi qu'aux départements."
    Et je devrais voir "BRETAGNE"
    Lorsque je suis "Réseaux"
    Et que je suis "menu-link-network"
    Alors je devrais être sur "/reseau"
    Et je devrais voir "Pour créer un réseau qui ne soit pas un département ou une région, comme une communauté de commune, une Métropole ou autre, assurez-vous d'abord qu'il n'existe pas déjà dans la liste ci-dessous. Sinon, créez-le avec le champ d'ajout ci-dessous."
    Et je devrais voir "À l'aide des outils de cette liste, vous pourrez changer le nom du réseau, le supprimer, lui ajouter des villes ainsi que des administrateurs."
    Et je devrais voir "Métropole de Lille"
