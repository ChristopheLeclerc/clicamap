<?php

use DI\Annotation\Inject;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class Webhook extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\WebhookMailjet
     */
    private $webhookmailjet;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Email_Sender
     */
    private $email_sender;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('WebhookMailgun');
    }

    /**
     * @deprecated
     */
    public function mailgun()
    {
        // workaround for cross domain request
        $request = file_get_contents('php://input');

        // parse request
        try {
            $email = $this->webhookmailgun->parseRequest($request);
        } catch (LogicException $e) {
            http_response_code(406);

            exit();
        }

        $this->disableEmail($email);
    }

    public function mailjet()
    {
        // workaround for cross domain request
        $request = file_get_contents('php://input');

        $email = $this->webhookmailjet->getEmailFromRequest($request);
        if (null === $email) {
            http_response_code(200);

            exit();
        }

        $this->disableEmail($email);
    }

    private function disableEmail(string $email)
    {
        // Disable users
        /** @var \PsrLib\ORM\Entity\BaseUser[] $users */
        $users = array_merge(
            $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class)->findBy(['email' => $email]),
            $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->findBy(['email' => $email]),
            $this->em->getRepository(\PsrLib\ORM\Entity\Paysan::class)->findBy(['email' => $email])
        );
        foreach ($users as $user) {
            $user->setEmailInvalid(true);
        }
        $this->em->flush();

        // Do not notify if no user disabled. Ex: mail sended by contact form
        if (count($users) > 0) {
            $this->email_sender->envoyerNotificationEmailInvalide($users);
        }
    }
}
