<?php

namespace PsrLib\Services;

use Nelmio\Alice\Loader\NativeLoader;
use PsrLib\AliceProvider\CarbonProvider;
use PsrLib\AliceProvider\DoctrineReferenceProvider;
use PsrLib\AliceProvider\PasswordProvider;

class CustomAliceNativeLoader extends NativeLoader
{
    /**
     * @var DoctrineReferenceProvider
     */
    private $doctrineReferenceProvider;

    /**
     * @var PasswordProvider
     */
    private $passwordProvider;

    /**
     * @var CarbonProvider
     */
    private $carbonProvider;

    public function __construct(DoctrineReferenceProvider $doctrineReferenceProvider, PasswordProvider $passwordProvider, CarbonProvider $carbonProvider)
    {
        $this->doctrineReferenceProvider = $doctrineReferenceProvider;
        $this->passwordProvider = $passwordProvider;
        $this->carbonProvider = $carbonProvider;

        parent::__construct();
    }

    public function createFakerGenerator(): \Faker\Generator
    {
        $generator = parent::createFakerGenerator();
        $generator->addProvider($this->doctrineReferenceProvider);
        $generator->addProvider($this->passwordProvider);
        $generator->addProvider($this->carbonProvider);

        return $generator;
    }
}
