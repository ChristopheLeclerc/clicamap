<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @extends EntityRepository<ModeleContratProduit>
 */
class ModeleContratProduitRepository extends EntityRepository
{
    /**
     * Test si au moins un produit de la liste a une régulation de poid.
     *
     * @param ModeleContratProduit[] $produits
     */
    public static function hasRegul(array $produits): bool
    {
        foreach ($produits as $produit) {
            if (true === $produit->getRegulPds()) {
                return true;
            }
        }

        return false;
    }
}
