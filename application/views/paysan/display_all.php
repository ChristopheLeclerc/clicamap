<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'modal_email.php';

/** @var \PsrLib\ORM\Entity\Paysan[] $paysans */
/** @var \Symfony\Component\Form\FormView $searchForm */
$currentUser = get_current_connected_user();
?>

<h3>Gestion des paysans</h3>


<form method="GET" action="" class="js-form-search">

    <div class="well">

        <div class="row">
            <div class="col-md-4">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Région</button>
		    </span>
                    <?= render_form_select_widget($searchForm['region']); ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Département</button>
		    </span>
                    <?= render_form_select_widget($searchForm['departement']); ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Réseau</button>
		    </span>
                    <?= render_form_select_widget($searchForm['reseau']); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Type de production</button>
		    </span>
                    <?= render_form_select_widget($searchForm['typeProduction']); ?>
		</div>
            </div>

            <div class="col-md-3">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Année d'adhésion</button>
		    </span>
                    <?= render_form_select_widget($searchForm['adhesion']); ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Début de commercialisation en AMAP</button>
		    </span>
                    <?= render_form_select_widget($searchForm['commercialisation']); ?>
                </div>
            </div>

            <div class="col-md-2">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Suivi SPG</button>
		    </span>
                    <?= render_form_select_widget($searchForm['suiviSPG']); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="input-group">
		    <span class="input-group-btn">
			<button class="btn btn-primary" type="button">Mot clé</button>
		    </span>
                    <?= render_form_text_widget($searchForm['keyword']); ?>
                </div>
            </div>

            <div class="form-group col-md-2 pull-right" style="margin-top:5px;">
                <?= render_form_checkbox_widget($searchForm['paysansSansFerme']); ?>
            </div>

            <div class="form-group col-md-2 pull-right" style="margin-top:5px;">
                <?= render_form_checkbox_widget($searchForm['active']); ?>
            </div>
        </div>

	<div class="row text-right">
            <a href="/paysan?reset_search" class="btn btn-primary" style="margin-right:5px;"><i class="glyphicon glyphicon-refresh"></i> Réinitialiser les critères de recherche</a>
	    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Générer la liste</button>
	</div>
	    
    </div><!-- well -->

</form>


<?php if (count($paysans) > 0): ?>
    <p>

        <a href="/paysan/informations_generales_creation" class="btn btn-success"><i
                    class="glyphicon glyphicon-plus"></i> Créer un profil paysan</a>

        <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DOWNLOAD_LIST)) : ?>
            <a href="/paysan/telecharger_paysans?<?=http_build_query($this->input->get()); ?>"
               class="btn btn-primary" style="margin-left:5px;"><i class="glyphicon glyphicon-download"></i> Télécharger
                la liste</a>
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_email" data-dismiss="modal"
               aria-hidden="true" style="margin-left:5px;"><i class="glyphicon glyphicon-envelope"></i> Emails de la
                liste</a>
        <?php endif; ?>
    </p>
    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DOWNLOAD_LIST)) : ?>

        <?=modal_email($paysans); ?>

        <!-- Modal Autre -->
        <div class="modal fade" id="modal_autre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Veuillez indiquer ce que vous souhaitez faire :</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger">Attention ! Cette fonction n'a pas d'étape de validation,
                            l'action
                            est immédiate : <br/> (fonction désactivée à cause des quotas pour l'envoi de mails)
                        </div>
                        <p><a href="#">Envoyer un mot de passe à tous les paysans de la liste qui n'en ont pas</a></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php
    $nb_paysans = count($paysans);

    if (1 === $nb_paysans) {
        echo '<div class="alert alert-success">1 résultat pour cette recherche :</div>';
    } else {
        echo '<div class="alert alert-success">'.$nb_paysans.' résultats pour cette recherche :</div>';
    }

    include 'display_all_list.php';
    ?>


<?php else: ?>

    <p>
        <a href="/paysan/informations_generales_creation" class="btn btn-success"><i
                    class="glyphicon glyphicon-plus"></i> Créer un profil paysan</a>

        <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_DOWNLOAD_LIST)) : ?>
            <a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-download"></i> Télécharger la
                liste</a>
            <a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>
        <?php endif; ?>

    </p>

    <div class="alert alert-warning">Aucun résultat pour cette recherche</div>
<?php endif; ?>

