<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $modeleContrat */
?>
<div class="alert alert-info">
    <table style="width: 100%">
        <tr>
            <td>
                Contrat : <strong><?= $modeleContrat->getNom(); ?></strong>
            </td>
            <td>
                Ferme : <strong><?= $modeleContrat->getFerme()->getNom(); ?></strong>
            </td>
        </tr>
        <tr>
            <td>
                Lieu de livraison : <strong><?= $modeleContrat->getLivraisonLieu()->getNom(); ?></strong>
            </td>
            <td>
                Délai de la ferme : <strong><?= $modeleContrat->getFerme()->getDelaiModifContrat(); ?> jour(s)</strong>
            </td>
        </tr>
        <tr>
            <td>
                Nombre de livraisons plancher/plafond : <strong><?= $modeleContrat->getNblivPlancher(); ?>/<?= $modeleContrat->getNbLivPlafond(); ?></strong>
            </td>
            <td>
                Date de fin de souscription : <strong><?= date_format_french($modeleContrat->getForclusion()); ?></strong>
            </td>
        </tr>
        <tr>
            <td>
                Produits identiques toutes les livraisons amapien : <strong><?= $modeleContrat->getProduitsIdentiqueAmapien() ? 'OUI' : 'NON'; ?></strong>
            </td>
            <td>
                Produits identiques toutes les livraisons paysan : <strong><?= $modeleContrat->getProduitsIdentiquePaysan() ? 'OUI' : 'NON'; ?></strong>
            </td>
        </tr>
        <tr>
            <td>
                Déplacement amapien sur date avec/sans livraison : <strong><?= \PsrLib\Services\ModeleContratDeplacementReportFormater::formatModeleContratDeplacementReport($modeleContrat); ?></strong>
            </td>
            <td>
                Mode de report/déplacement : <strong><?= true === $modeleContrat->getAmapienPermissionDeplacementLivraison() ? $modeleContrat->getAmapienDeplacementMode() : ''; ?></strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Informations complémentaires : <strong><?= nl2br($modeleContrat->getSpecificite()); ?></strong>
            </td>
        </tr>
    </table>

</div>
