# language: fr
Fonctionnalité: Comme amap, je doit pouvoir créer un contrat et le simuler

  @database
  Scénario: Création d'un contrat avec depassement possible pour l'amapien
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je vais sur "/contrat_vierge"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Et que je suis "Créer un nouveau contrat"
    Et que je remplis "model_contrat_form1[nom]" avec "Test"
    Et que je remplis "model_contrat_form1[filiere]" avec "Filiere"
    Et que je presse "Étape suivante"
    Et que je remplis le datepicker "model_contrat_form2[forclusion]" avec "2030-01-01"
    Et que je presse "btn-plus"
    Et que je presse "btn-plus"
    Et que je remplis le datepicker "model_contrat_form2[dates][1][dateLivraison]" avec "2030-02-01"
    Et que je remplis le datepicker "model_contrat_form2[dates][2][dateLivraison]" avec "2030-02-02"
    Et que je presse "Étape suivante"
    Et que je presse "Étape suivante"
    Et que je remplis "model_contrat_form4[produitsIdentiquePaysan]" avec "0"
    Et que je remplis "model_contrat_form4[produitsIdentiqueAmapien]" avec "0"
    Et que je remplis "model_contrat_form4[nblivPlancher]" avec "1"
    Et que je remplis "model_contrat_form4[nblivPlancherDepassement]" avec "1"
    Et que je presse "Étape suivante"
    Et que je presse "Étape suivante"
    Et que je remplis "model_contrat_form6[reglementNbMax]" avec "1"
    Et que je presse "btn-plus"
    Et que je remplis "model_contrat_form6[dateReglements][1][date]" avec "2030-01-10"
    Et que je remplis "model_contrat_form6[reglementType][]" avec "cheque"
    Et que je remplis "model_contrat_form6[reglementModalite]" avec "modalite"
    Et que je presse "Étape suivante"
    Et que je presse "Sauvegarder"
    Quand je suis "Tester le contrat"
    Et que je remplis "commande[19][3]" avec "1"
    Et que je remplis "commande[20][3]" avec "1"
    Et que je presse "Tester"
    Alors je ne devrais pas voir l'élément ".alert-danger"


