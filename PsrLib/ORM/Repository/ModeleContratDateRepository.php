<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContratDate;

/**
 * @extends EntityRepository<ModeleContratDate>
 */
class ModeleContratDateRepository extends EntityRepository
{
    public static function criteriaOrderDateAsc(): Criteria
    {
        return Criteria::create()
            ->orderBy(['dateLivraison' => 'ASC'])
        ;
    }

    /**
     * @return ModeleContratDate[]
     */
    public function getByAmap(Amap $amap): array
    {
        return $this
            ->createQueryBuilder('mcd')
            ->leftJoin('mcd.modeleContrat', 'mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->distinct(true)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ModeleContratDate[]
     */
    public function getByFerme(Ferme $ferme)
    {
        return $this
            ->createQueryBuilder('mcd')
            ->leftJoin('mcd.modeleContrat', 'mc')
            ->where('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->getQuery()
            ->getResult()
        ;
    }
}
