<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Paysan $paysan */
$ferme = $paysan->getFerme();
?>

<h3>Ma ferme</h3>

<h5>
    <a href="<?= site_url('paysan/display/'.$paysan->getId()); ?>">Profil</a>
        <i class="glyphicon glyphicon-menu-right"></i><?= $ferme->getNom(); ?>
</h5>

<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-inverse">
            <tr>
                <th>Nom</th>
                <th>Paysan(s)</th>
                <th>Outils</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    <?= $ferme->getNom(); ?>
                </td>
                <td>
                    <?= implode(',', $ferme->getPaysans()->toArray()); ?>
                </td>
                <td>

                    <a title="Détails de la ferme" href="<?= site_url('ferme/display/'.$ferme->getId()); ?>">
                        <i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="Détails de la ferme"></i>
                    </a>&nbsp;
                    <a title="Modifier la ferme" href="<?= site_url('ferme/informations_generales/'.$ferme->getId()); ?>">
                        <i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier la ferme"></i>
                    </a>&nbsp;
                    <a title="Paysans de la ferme" href="<?= site_url('ferme/paysan_display/'.$ferme->getId()); ?>">
                        <i class="glyphicon glyphicon-user" data-toggle="tooltip" title="Paysans de la ferme"></i>
                    </a>&nbsp;

                    <a title="Types de production" href="<?= site_url('ferme/type_production_display/'.$ferme->getId()); ?>">
                        <i class="glyphicon glyphicon-certificate" data-toggle="tooltip" title="Types de production"></i>
                    </a>&nbsp;

                    <a title="Produits proposés" href="<?= site_url('produit/accueil/'.$ferme->getId()); ?>">
                        <i class="glyphicon glyphicon-apple" data-toggle="tooltip" title="Produits proposés"></i>
                    </a>&nbsp;
                    <a title="Calendrier des livraisons" href="<?= site_url('ferme/calendrier/'.$ferme->getId()); ?>">
                        <i class="glyphicon glyphicon-calendar" data-toggle="tooltip" title="Calendrier des livraisons"></i>
                    </a>&nbsp;

                </td>
            </tr>
        </tbody>
    </table>
</div>
