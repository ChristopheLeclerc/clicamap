<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Validator;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\AmapAbsence;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class AmapAbsenceNoOverlapValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (null === $value) {
            return;
        }

        if (!($value instanceof AmapAbsence)) {
            throw new UnexpectedTypeException($value, AmapAbsence::class);
        }

        /** @var AmapAbsence[] $absences */
        $absences = $this
            ->em
            ->getRepository(AmapAbsence::class)
            ->findBy([
                'amap' => $value->getAmap(),
            ])
        ;

        foreach ($absences as $absence) {
            if ($absence->getId() === $value->getId()) {
                continue;
            }

            $inputDebut = $value->getAbsenceDebut();
            $inputFin = $value->getAbsenceFin();
            $bddDebut = $absence->getAbsenceDebut();
            $bddFin = $absence->getAbsenceFin();
            if (($inputDebut->gte($bddDebut) && $inputDebut->lte($bddFin))
            || ($inputFin->gte($bddDebut) && $inputFin->lte($bddFin))
            || ($inputDebut->lte($bddDebut) && $inputFin->gte($bddFin))) {
                $this->context->buildViolation('Cette plage de date correspond à une période d\'absence existante')
                    ->addViolation()
                ;

                return;
            }
        }
    }
}
