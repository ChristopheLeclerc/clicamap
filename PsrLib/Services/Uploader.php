<?php

namespace PsrLib\Services;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use LogicException;
use PsrLib\ORM\Entity\Files\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Uploader
{
    public function uploadFile(UploadedFile $file, string $targetClass): object
    {
        if (!is_subclass_of($targetClass, File::class)) {
            throw new LogicException('Unable to manage target class');
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $fileName = $safeFilename.'-'.uniqid('', true).'.'.$file->guessExtension();

        $target = new $targetClass($fileName, $file->getClientOriginalName());
        $file->move($target->getUploadPath(), $fileName);

        return $target;
    }
}
