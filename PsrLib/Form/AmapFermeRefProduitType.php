<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapFermeRefProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Amapien $refProduit */
        $refProduit = $options['refProduit'];
        $amap = $refProduit->getAmap();

        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => [$amap],
                'data' => $amap,
                'disabled' => true,
                'required' => false,
                'placeholder' => true,
                'choice_label' => function (?Amap $amap) {
                    return null === $amap ? '' : $amap->getNom();
                },
                'choice_value' => function (?Amap $amap) {
                    return null === $amap ? '' : $amap->getId();
                },
            ])
            ->add('ferme', ChoiceType::class, [
                'choices' => null === $amap ? [] : $refProduit->getRefProdFermes()->toArray(),
                'required' => false,
                'placeholder' => true,
                'choice_label' => function (?Ferme $ferme) {
                    return null === $ferme ? '' : $ferme->getNom();
                },
                'choice_value' => function (?Ferme $ferme) {
                    return null === $ferme ? '' : $ferme->getId();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'refProduit' => null,
            ])
        ;
    }
}
