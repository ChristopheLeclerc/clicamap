<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \Symfony\Component\Form\FormView $form */
?>


<h3>Contacter mon AMAP</h3>
<div class="table-responsive">
    <table class="table">

        <tbody>

        <?php

        if ($amap->getEmail()) {
            echo '<tr>';
            echo '<th>Email contact public :</th>';
            echo '<td><a href="mailto:'.$amap->getEmail().'">'.$amap->getEmail().'</a>';

            echo '<a href="#" data-toggle="modal" data-target="#modal_envoi" ><button type=button class="btn btn-primary btn-sm pull-right" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Email</button></a>'; ?>

            <div class="modal fade text-left" id="modal_envoi" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h5 class="modal-title" id="myModalLabel">Envoyer un email à votre AMAP</h5>
                        </div>
                        <div class="modal-body">

                            <form method="POST" action="<?= site_url('contact/mon_amap'); ?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">

<span class="input-group-btn">
<button class="btn btn-primary" type="button">Objet</button>
</span>

                                            <div class="form-group">
                                                <?= render_form_text_widget($form['title']); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <?= render_form_textarea_widget($form['content']); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" value="Envoyer" class="pull-right btn btn-success"/>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>

            </td>

            <?php

            echo '</tr>';
        }

    if ($amap->getAdresseAdmin() && $amap->getAdresseAdminVille()) {
        echo '<tr>';
        echo '<th>Adresse Administrative :</th>';
        echo '<td>'.ucfirst($amap->getAdresseAdmin()).',<br/>'.$amap->getAdresseAdminVille()->getCpString().' '.mb_strtoupper($amap->getAdresseAdminVille()->getNom());
        echo '</tr>';
    }

    $livraison_lieu = $amap->getLivraisonLieux();
    if (!$livraison_lieu->isEmpty()) {
        echo '<tr>';

        if ($livraison_lieu->count() > 1) {
            echo '<th>Adresses de livraison :</th>';
        } else {
            echo '<th>Adresse de livraison :</th>';
        }

        echo '<td>';

        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $liv_lieu */
        foreach ($livraison_lieu as $liv_lieu) {
            echo '<div class="alert alert-success">';
            echo '<strong>';
            echo ucfirst($liv_lieu->getNom()).' - ';
            echo ucfirst($liv_lieu->getAdresse()).' '.$liv_lieu->getVille()->getCpString().' '.mb_strtoupper($liv_lieu->getVille()->getNom());
            echo '</strong>';

            $livraison_horaires = $liv_lieu->getLivraisonHoraires();
            if (!$livraison_horaires->isEmpty()) {
                echo '<ul>';

                /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $liv_hor */
                foreach ($livraison_horaires as $liv_hor) {
                    if ($liv_hor->getSaison() && $liv_hor->getJour() && $liv_hor->getHeureDebut() && $liv_hor->getHeureFin()) {
                        echo '<li>';
                        if (\PsrLib\ORM\Entity\AmapLivraisonHoraire::SAISON_ETE == $liv_hor->getSaison()) {
                            echo 'Été';
                        }
                        if (\PsrLib\ORM\Entity\AmapLivraisonHoraire::SAISON_HIVER == $liv_hor->getSaison()) {
                            echo 'Hiver';
                        }
                        echo ' -> ';
                        echo ucfirst($liv_hor->getJour()).' : '.$liv_hor->getHeureDebut().' - '.$liv_hor->getHeureFin();
                        echo '</li>';
                    }
                }

                echo '</ul>';
            }

            echo '</div>';
        }

        echo '</td></tr>';

        if ($livraison_lieu[0]->getGpsLatitude() && $livraison_lieu[0]->getGpsLongitude()) {
            // ICON CARTO
            echo '<tr>';
            echo '<th>Coordonnées GPS :</th>';
            echo '<td><a href="'.site_url('map/amap_display/'.$amap->getId()).'"><i class="glyphicon glyphicon-map-marker"></i>&nbsp;Visualiser le ou les adresse(s) de livraison sur une carte</a>&nbsp;';
            echo '</td></tr>';
        }
    }
        ?>

        </tbody>
    </table>
</div>
