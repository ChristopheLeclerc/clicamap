<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire[] $amap_livraisons_horaires */
?>

<h3><?= $contrat->getNom(); ?> : Dates et fréquence du contrat</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Création
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 2
</h5>

<?php
   $saisonMap = [
       'ete' => 'été',
       'hiver' => 'hiver',
   ];

    $contraintesLivraisonsString = [];
    $contraintesLivraisons = [];
    foreach ($amap_livraisons_horaires as $horaire) {
        $contraintesLivraisonsString[] = $horaire->getJour().' en saison '.$horaire->getSaison().'';
        $contraintesLivraisons[] = array_search(
            $horaire->getJour(),
            \PsrLib\ORM\Entity\AmapLivraisonHoraire::JOURS,
            true
        ) + 1;
    }

    $contraintesLivraisonsString = 'Pour rappel l\'AMAP livre le '.implode($contraintesLivraisonsString, ' OU ');

?>
<form method="POST" action="" id="form-dates">

    <div class="well well-sm">
        <div class="row">

            <div class="col-xs-12">
                <div class="alert alert-info">
                    L'AMAP et le référent pouront créer les contrats après la date de fin de souscription. Si le contrat permet la modification des livraisons, celles-ci sont modifiables jusqu'à la date de livraison diminuée du délai nécessaire pour le paysan (ce délai est précisé en nombre de jours dans la ferme). Ex: une date de livraison est prévu le 12 septembre, il est possible de la modifier jusqu'au 10 septembre si le délai précisé dans la ferme est de 2 jours.
                </div>
            </div>

            <div class="form-group col-md-12">
                <label for="forclusion" class="control-label">Date de fin de souscription <span class="text-danger">*</span> :</label>
                <?= render_form_datepicker_widget($form['forclusion']); ?>
            </div>

            <div class="col-md-3">

                <div class="form-group">
                    <label for="frequence" class="control-label">Fréquence des livraisons : </label>
                    <?= render_form_select_widget($form['frequence']); ?>
                    <p class="help-block">
                        <span class="text-danger">
                            Si vous avez besoin de définir une autre fréquence, utilisez simplement le bouton "+" ci-dessous afin d'ajouter les dates dont vous avez besoin
                        </span>
                    </p>
                </div>

            </div>

            <div class="form-group col-md-3">
                <label for="date_livraison_debut" class="control-label">Date de première livraison :</label>
                <input
                        type="text"
                        data-date-format="yyyy-mm-dd"
                        class="form-control datepicker"
                        name="date_livraison_debut"
                        id="date_livraison_debut"
                        readonly
                        value="<?= set_value('date_livraison_debut'); ?>"
                />
                <p class="help-block"><?= $contraintesLivraisonsString; ?></p>
                <?= form_error('date_livraison_debut'); ?>
            </div>

            <div class="form-group col-md-3">
                <label for="date_livraison_fin" class="control-label">Date de dernière livraison :</label>
                <input
                        type="text"
                        data-date-format="yyyy-mm-dd"
                        class="form-control datepicker"
                        name="date_livraison_fin"
                        id="date_livraison_fin"
                        readonly
                        value="<?= set_value('date_livraison_fin'); ?>"
                />
                <?= form_error('date_livraison_fin'); ?>
                <p class="help-block"><?= $contraintesLivraisonsString; ?></p>
            </div>

            <div class="col-md-3">
                <button id="btn-dategeneration" class="btn btn-default" style="margin-top: 24px;">Générer des dates</button>
                <p class="help-block">Merci de remplir les dates de première et dernière livraison pour que ce bouton fonctionne</p>
            </div>

            <div class="col-md-12">
                <label>Dates de livraison : </label><br />
                <?= render_form_global_errors($form); ?>
                <div id="dates-list">

                </div>
            </div>
            <div class="col-md-12">
                <button
                        type="button"
                        id="btn-plus"
                        class="btn btn-success">
                    <i class="glyphicon glyphicon-plus"></i>
                </button>
                <button id="btn-sort" class="btn btn-default">Trier</button>
            </div>

        </div>
    </div>

    <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;"/>
    <a href="<?= site_url('/contrat_vierge/v2_contrat_form_1'); ?>" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
    <a href="<?= site_url('contrat_vierge'); ?>" class="pull-right btn btn-default">Annuler</a>

</form>


<script>
  $(document).ready(function () {

    var lineId = 1;

    $('#btn-plus').click(function (e) {
      e.preventDefault();

      newLine();
    });

    $('#form-dates').on('click', '.btn-remove', function (e) {
      e.preventDefault();

      $(this)
        .closest('.date-row')
        .remove();

      $('input[type=submit]').trigger('set:disabled');

    });

    $('#btn-dategeneration').on('click', function (e) {
      e.preventDefault();

      // Delete all exising lines
      $('.btn-remove').trigger('click');

      var date = moment($('#date_livraison_debut').val());
      var dateFin = moment($('#date_livraison_fin').val());
      var increment = parseInt($('select[name="model_contrat_form2[frequence]"]').val());

      if(!date.isValid() || !date.isValid()) {
        return;
      }

      while (date.valueOf() <= dateFin.valueOf()) {
        newLine(date.format('YYYY-MM-DD'));
        date.add(increment, 'days');
      }

    });

    $('#btn-sort').on('click', function (e) {
      e.preventDefault();

      var dates = [];
      $('.date-input')
        .each(function () {
          dates.push($(this).val());
        });

      dates.sort();
      $('.date-input')
        .each(function () {
          $(this).val(dates.shift());
        });

      // Update error messages
      $('.datepicker').trigger('changeDate');

    });

    $('input[type=submit]').on('set:disabled', function () {
      var nbDates = $('.date-input').length;

      if(nbDates === 0) {
        $(this).prop('disabled', true)
      } else {
        $(this).prop('disabled', false)
      }
    })
      .trigger('set:disabled');

        <?php
            $contratDates = $contrat
                ->getDates()
                ->map(function (PsrLib\ORM\Entity\ModeleContratDate $date) {
                    if (null === $date->getDateLivraison()) {
                        return null;
                    }

                    return $date->getDateLivraison()->format('Y-m-d');
                })
                ->toArray()
;
        ?>
        var datesExistantes = JSON.parse('<?= json_encode($contratDates); ?>');
        for(const dateKey in datesExistantes) {
          newLine(datesExistantes[dateKey]);
        }

    function newLine(date = null) {
      var input = $("<div class='date-row row'><div class='col-xs-3'><div class='form-group'><input type='text' data-date-format='yyyy-mm-dd' class='form-control datepicker date-input' readonly required /><span class='help-block erreur-livraison'><?= $contraintesLivraisonsString; ?></span><span class='help-block erreur-etudiant'></span></div></div><div class='col-xs-2'><button type='button' class='btn btn-danger btn-remove'><i class='glyphicon glyphicon-minus'></i></button></div></div>");
      input
        .find('input')
        .prop('name', 'model_contrat_form2[dates][' + lineId + '][dateLivraison]');

      if(date !== null) {
        input
          .find('input')
          .val(date);
      }

      $('#dates-list')
        .append(input)
      ;

      lineId = lineId + 1;
      input
        .find('.datepicker')
        .datepicker()
        .on('changeDate', function(ev){
          var divErrorLivraison = $(this).closest('.form-group').find('.erreur-livraison');
          var divErrorEtudiant = $(this).closest('.form-group').find('.erreur-etudiant');
          var divFromGroup = $(this).closest('.form-group');
          function updateFormError() {
            if(divErrorLivraison.is(":visible") || divErrorEtudiant.is(":visible")) {
              divFromGroup
                .addClass('has-error');
            } else {
              divFromGroup
                .removeClass('has-error');
            }
          }

          // Test des jours autorisés
          var contraintesJoursAutorises = JSON.parse('<?= json_encode($contraintesLivraisons); ?>');

          var date = moment($(this).val());

          if(!date.isValid()
            || contraintesJoursAutorises.indexOf(date.day()) === -1) { // Date valide
            divErrorLivraison
              .show();
          } else {
            divErrorLivraison
              .hide()
            ;
          }

          // Test des dates d'absences

          divErrorEtudiant.hide();
          $.ajax('/contrat_vierge/ajax_test_date_absence?amap_id=<?= $contrat->getLivraisonLieu()->getAmap()->getId(); ?>&date=' + $(this).val(), {
            success: function (res) {
              if(res === "") {
                return;
              }
              var data = JSON.parse(res);

              if(data.result === true) {
                divErrorEtudiant
                  .html('Attention cette date est incluse sur la période d\'absence "' + data.title + '"')
                  .show()
                ;
                updateFormError();
              }
            }
          });
          updateFormError();

          $('input[type=submit]').trigger('set:disabled');
        })
        .trigger('changeDate');

    }

    function manageGenerationButtonActivationState() {
      var valLivFirst = $('#date_livraison_debut').val();
      var valLivLast = $('#date_livraison_fin').val();
      if(valLivFirst === "" || valLivLast === "") {
        $('#btn-dategeneration').prop('disabled', true);
      } else {
        $('#btn-dategeneration').prop('disabled', false);
      }
    }
    manageGenerationButtonActivationState();

    $('#date_livraison_debut').on('changeDate', manageGenerationButtonActivationState);
    $('#date_livraison_fin').on('changeDate', manageGenerationButtonActivationState);
  });
</script>


<style>
    .datepicker[readonly] {
        background-color: white;
    }
</style>
