<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<h3>Pièces jointes</h3>

<h5><a href="<?= site_url('evenement'); ?>">Événements</a> <i class="glyphicon glyphicon-menu-right"></i>
    Création d'un événement <i class="glyphicon glyphicon-menu-right"></i> Détails <i
            class="glyphicon glyphicon-menu-right"></i> Pièces jointes</h5>

<?php echo form_open_multipart('evenement/add_3', 'id="add3"');
// VARIABLES CACHÉES
echo '<input type="hidden"  name="etape_pj" value="1" />';
// -----------------------------------------------------------------------------
// SESSION AMAP OU RÉFÉRENT
if (set_value('amapiens')) {
    echo '<input type="hidden" name="amapiens" value="'.set_value('amapiens').'" />';
}
if (set_value('reseau')) {
    echo '<input type="hidden" name="reseau" value="'.set_value('reseau').'" />';
}
if (set_value('ferme_id')) {
    echo '<input type="hidden" name="ferme_id" value="'.set_value('ferme_id').'" />';
}
// -----------------------------------------------------------------------------
// SESSION ADMIN RÉGION
if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('amap')) {
    foreach (set_value('amap') as $amap) {
        echo '<input type="hidden"  name="amap[]" value="'.$amap.'" />';
    }
}

if (set_value('ferme')) {
    foreach (set_value('ferme') as $ferme) {
        echo '<input type="hidden"  name="ferme[]" value="'.$ferme.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}
// -----------------------------------------------------------------------------
if (set_value('evt_nom')) {
    echo '<input type="hidden"  name="evt_nom" value="'.set_value('evt_nom').'" />';
}
if (set_value('date_deb')) {
    echo '<input type="hidden"  name="date_deb" value="'.set_value('date_deb').'" />';
}
if (set_value('date_fin')) {
    echo '<input type="hidden"  name="date_fin" value="'.set_value('date_fin').'" />';
}
if (set_value('hor_deb')) {
    echo '<input type="hidden"  name="hor_deb" value="'.set_value('hor_deb').'" />';
}
if (set_value('hor_fin')) {
    echo '<input type="hidden"  name="hor_fin" value="'.set_value('hor_fin').'" />';
}
if (set_value('url')) {
    echo '<input type="hidden"  name="url" value="'.set_value('url').'" />';
}
if (set_value('evt_txt')) {
    echo '<input type="hidden"  name="evt_txt" value="'.set_value('evt_txt').'" />';
}
// -----------------------------------------------------------------------------
?>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label for="img" class="control-label">Image (.jpg ou .png < 1 Mo) : </label>
                <?= render_form_file_widget($form['img']); ?>
                <small id="fileHelp" class="form-text text-muted">Possibilité de télécharger une image.</small>
            </div>
            <div class="form-group col-md-6">
                <label for="pj" class="control-label">Pièce jointe (.pdf < 1 Mo) : </label>
                <?= render_form_file_widget($form['pj']); ?>
                <small id="fileHelp" class="form-text text-muted">Possibilité de télécharger une pièce jointe.</small>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <input type="submit" value="Sauvegarder" class="pull-right btn btn-success" style="margin-left:5px;"/>
</div>

</form>

<!-- ----------------------------------------------------------------------- -->
<!-- ÉTAPE PRÉCÉDENTE -->
<?php echo form_open_multipart('evenement/add_2', 'id="add2"');
// VARIABLES CACHÉES
// -----------------------------------------------------------------------------
// SESSION AMAP OU RÉFÉRENT
if (set_value('amapiens')) {
    echo '<input type="hidden" name="amapiens" value="'.set_value('amapiens').'" />';
}
if (set_value('reseau')) {
    echo '<input type="hidden" name="reseau" value="'.set_value('reseau').'" />';
}
if (set_value('ferme_id')) {
    echo '<input type="hidden" name="ferme_id" value="'.set_value('ferme_id').'" />';
}
// -----------------------------------------------------------------------------
// SESSION ADMIN RÉGION
if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('amap')) {
    foreach (set_value('amap') as $amap) {
        echo '<input type="hidden"  name="amap[]" value="'.$amap.'" />';
    }
}

if (set_value('ferme')) {
    foreach (set_value('ferme') as $ferme) {
        echo '<input type="hidden"  name="ferme[]" value="'.$ferme.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}
// -----------------------------------------------------------------------------
if (set_value('evt_nom')) {
    echo '<input type="hidden"  name="evt_nom" value="'.set_value('evt_nom').'" />';
}
if (set_value('date_deb')) {
    echo '<input type="hidden"  name="date_deb" value="'.set_value('date_deb').'" />';
}
if (set_value('date_fin')) {
    echo '<input type="hidden"  name="date_fin" value="'.set_value('date_fin').'" />';
}
if (set_value('hor_deb')) {
    echo '<input type="hidden"  name="hor_deb" value="'.set_value('hor_deb').'" />';
}
if (set_value('hor_fin')) {
    echo '<input type="hidden"  name="hor_fin" value="'.set_value('hor_fin').'" />';
}
if (set_value('url')) {
    echo '<input type="hidden"  name="url" value="'.set_value('url').'" />';
}
if (set_value('evt_txt')) {
    echo '<input type="hidden"  name="evt_txt" value="'.set_value('evt_txt').'" />';
}
// -----------------------------------------------------------------------------
?>
<div class="form-group">
    <input type="submit" value="Étape précédente" class="pull-right btn btn-default" style="margin-left:5px;"/>
    <a href="<?= site_url('evenement'); ?>" class="pull-right btn btn-default">Annuler</a>
</div>

</form>
