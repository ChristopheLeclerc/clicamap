<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once __DIR__.'/../../system/helpers/url_helper.php';

class MaintenanceHook
{
    public function redirectOnMaintenanceEnabled()
    {
        if (($_ENV['MAINTENANCE_ENABLED'] ?? null) === 'true'
            && '/maintenance' !== $_SERVER['REQUEST_URI']) {
            $whitelistedIps = explode(',', $_ENV['MAINTENANCE_WHITELIST_IP'] ?? '');
            if (!in_array(@$_SERVER['REMOTE_ADDR'], $whitelistedIps, true)) {
                header('Location: /maintenance');

                exit;
            }
        }
    }
}
