<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Cocur\Slugify\Slugify;

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('tpp_sort')) {
    function tpp_sort(&$tpps)
    {
        $cat = [];
        foreach ($tpps as $tp) {
            if ($tp->tp_categorie_fille) {
                $cat[$tp->tp_id] = $tp->tp_categorie_lib;
            }
        }

        $slugify = new Slugify(); // Gestion des ligatures
        uasort($tpps, function ($tp1, $tp2) use ($slugify, $cat) {
            if (null === $tp1->tp_categorie_mere) {  // Pas de sous catégorie, on compare directement les types de production
                $slug1 = $slugify->slugify($tp1->tp_categorie_lib);
            } else {
                $slug1 = $slugify->slugify($cat[$tp1->tp_categorie_mere]);
            }
            if (null === $tp2->tp_categorie_mere) {
                $slug2 = $slugify->slugify($tp2->tp_categorie_lib);
            } else {
                $slug2 = $slugify->slugify($cat[$tp2->tp_categorie_mere]);
            }

            return strnatcmp($slug1, $slug2);
        });
    }
}
