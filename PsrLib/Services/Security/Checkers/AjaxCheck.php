<?php

namespace PsrLib\Services\Security\Checkers;

use PsrLib\Services\Security\SecurityChecker;

class AjaxCheck extends AbstractCheck
{
    public function isGrantedAjax(): bool
    {
        return $this->securitychecker->isGranted(SecurityChecker::ACTION_USER_IS_AUTHENTICATED);
    }
}
