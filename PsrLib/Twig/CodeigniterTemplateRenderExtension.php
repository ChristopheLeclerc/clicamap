<?php

namespace PsrLib\Twig;

use CI_Loader;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CodeigniterTemplateRenderExtension extends AbstractExtension
{
    /**
     * @var CI_Loader
     */
    private $ciLoader;

    public function __construct(CI_Loader $ciLoader)
    {
        $this->ciLoader = $ciLoader;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('render_ci_template', [$this, 'renderCiTemplate'], ['is_safe' => ['html']]),
            new TwigFunction('render_ci_helper', [$this, 'renderCiHelper'], ['is_safe' => ['html']]),
        ];
    }

    public function renderCiTemplate(string $template, array $args = []): string
    {
        return $this->ciLoader->view($template, $args, true);
    }

    public function renderCiHelper(string $helper, array $args = []): string
    {
        return $helper($args);
    }
}
