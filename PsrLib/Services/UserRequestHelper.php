<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\FermeRegroupementRepository;
use PsrLib\ORM\Repository\PaysanRepository;

/**
 * Helper for user manipulation on multiple entities.
 */
class UserRequestHelper
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var AmapienRepository
     */
    protected $amapienRepo;

    /**
     * @var AmapRepository
     */
    protected $amapRepo;

    /**
     * @var PaysanRepository
     */
    protected $paysanRepo;

    /**
     * @var FermeRegroupementRepository
     */
    protected $fermeRegroupementRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->amapRepo = $this->em->getRepository(Amap::class);
        $this->amapienRepo = $this->em->getRepository(Amapien::class);
        $this->paysanRepo = $this->em->getRepository(Paysan::class);
        $this->fermeRegroupementRepo = $this->em->getRepository(FermeRegroupement::class);
    }

    public function getUserFromEmailStatut(string $email, string $statut): ?BaseUser
    {
        if ('amapien' === $statut) {
            $user = $this->amapRepo->findOneBy([
                'email' => $email,
            ]);
            if (null === $user) {
                $user = $this->amapienRepo->findOneBy([
                    'email' => $email,
                ]);
            }
        } elseif ('paysan' === $statut) {
            $user = $this->paysanRepo->findOneBy([
                'email' => $email,
            ]);
        } elseif ('regroupement' === $statut) {
            $user = $this->fermeRegroupementRepo->findOneBy([
                'email' => $email,
            ]);
        } else {
            return null;
        }

        return $user;
    }

    /**
     * Récupère un utilisateur depuis le token.
     * Renvoie null si aucun utlisateur trouvé ou si le token est expiré.
     *
     * @return null|BaseUser
     */
    public function getUserFromTokenMdpReinit(?string $token)
    {
        if (null === $token) {
            return null;
        }

        /** @var BaseUser[] $users */
        $users = array_merge(
            $this->paysanRepo->findByPasswordResetTokenString($token),
            $this->amapRepo->findByPasswordResetTokenString($token),
            $this->amapienRepo->findByPasswordResetTokenString($token)
        );
        if (!empty($users) && $users[0]->getPasswordResetToken()->isValid()) {
            return $users[0];
        }

        return null;
    }

    /**
     * Get all invalid emails on database.
     *
     * @return string[]
     */
    public function getInvalidEmails(): array
    {
        $this->amapienRepo->refreshConnection();

        $emails = $this
            ->em
            ->createNativeQuery('SELECT amap_email_public as email FROM ak_amap WHERE amap_email_invalid = 1
                    UNION
                    SELECT a_email as email FROM ak_amapien WHERE a_email_invalid = 1
                    UNION
                    SELECT pay_email as email FROM ak_paysan WHERE pay_email_invalid = 1;
            ', new ResultSetMapping())
            ->getArrayResult()
        ;

        return array_column($emails, 'email');
    }

    /**
     * @param array|string $email
     */
    public function isEmailInvalid($email): bool
    {
        $toChecks = is_array($email) ? $email : [$email];
        $invalidEmails = $this->getInvalidEmails();

        foreach ($toChecks as $toCheck) {
            if (in_array($toCheck, $invalidEmails, true)) {
                return true;
            }
        }

        return false;
    }
}
