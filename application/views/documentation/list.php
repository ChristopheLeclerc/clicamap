<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
$documentPermissionFormatter = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
    ->get(\PsrLib\Services\DocumentPermissionFormatter::class)
;

?>

<h3>Gestion des documents</h3>

<h5>
    <a href="<?= site_url('/portail'); ?>">Accueil</a>
    <i class="glyphicon glyphicon-menu-right"></i> Communication
    <i class="glyphicon glyphicon-menu-right"></i> Gestion des documents
</h5>

<p><a href="<?= site_url('/documentation/form'); ?>" class="btn btn-success" style="margin-right:5px;"><i class="glyphicon glyphicon-plus"></i> Créer un nouveau document</a></p>

<table class="table table-datatable" id="table-documents">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Type d'utilisateur</th>
            <th>Outils</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($documents as $document) {
    echo '<tr>';
    echo '<td>'.$document->getNom().'</td>';
    echo '<td>'.$documentPermissionFormatter->formatPermission($document).'</td>';
    echo '<td>
                    <a href="'.prep_url($document->getLien()).'" target="_blank"
                        title="Voir le document"
                    >
                        <i 
                            class="glyphicon glyphicon-eye-open"
                            data-toggle="tooltip"
                            title="Voir le document"
                            ></i>
                        </a>
                    <a 
                        href="'.site_url('/documentation/form/'.$document->getId()).'"
                        title="Modifier le document"
                        >
                        <i 
                            class="glyphicon glyphicon-edit"
                            data-toggle="tooltip"
                            title="Modifier le document"
                         ></i>
                        </a>
                    <a href="#" 
                        title="Supprimer le document"
                        data-toggle="modal"
                        data-target="#modal-remove"
                        data-document-id="'.$document->getId().'"
                        data-document-name="'.$document->getNom().'"
                        >
                            <i 
                                class="glyphicon glyphicon-remove"
                                data-toggle="tooltip"
                                title="Supprimer le document">
                                </i>
                    </a>
                 </td>';
    echo '</tr>';
}?>
    </tbody>
</table>

<div class="modal fade text-left" id="modal-remove" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Voulez-vous vraiment supprimer le document ? </h5>
            </div>
            <div class="modal-body">
                <p class="text-right">
                    <a
                        id="modal-remove-link"
                        href="#"
                       class="btn btn-danger btn-sm">OUI</a>
                    <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                </p>
            </div>
        </div>
    </div>
</div>

<script>
// Remove modal
  $('#modal-remove').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('document-id');
    var name = button.data('document-name');
    var modal = $(this);
    modal.find('.modal-title').text('Voulez-vous vraiment supprimer le document ' + name + ' ?' );
    modal.find('#modal-remove-link').attr('href', '/documentation/supprimer/' + id)
  });

</script>


