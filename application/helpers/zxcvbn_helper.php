<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('render_zxcvbn_progress_bar')) {
    /**
     * Fait le rendu d'une barre de progression qui donne la force d'un mot de passe.
     *
     * @param $passwordFieldName Nom du champ dans lequel est le mot de passe
     * @param $url Url sur laquel envoyer le mot de passe pour test
     */
    function render_zxcvbn_progress_bar($passwordFieldName, $url)
    {
        $CI = &get_instance();

        return $CI->load->view(
            'includes/zxcvbn_progress_bar',
            [
                'passwordFieldName' => $passwordFieldName,
                'url' => $url,
            ],
            true
        );
    }
}

if (!function_exists('generate_zxcvbn_response')) {
    /**
     * Génère une réponse à une requete ajax faite depuis une progress bar zxcvbn
     * Quitte à la fin de la fonction.
     */
    function generate_zxcvbn_response()
    {
        $CI = &get_instance();
        $password = $CI->input->post('zxcvbn_password');

        $strength = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\PsrLib\Services\Zxcvbn::class)
            ->estimateScore($password)
        ;

        header('Content-Type: application/json');
        echo json_encode([
            'strength' => $strength,
        ]);
    }
}
