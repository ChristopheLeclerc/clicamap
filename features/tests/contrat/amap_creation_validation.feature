# language: fr
Fonctionnalité: Comme amap, je doit avoir des messages d'erreurs en cas d'informations invalides

  @database
  Plan du Scénario: Validation étape 1
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion des contrats vierges"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Et que je suis "Créer un nouveau contrat"
    Et que je remplis "model_contrat_form1[nom]" avec "<nom>"
    Et que je remplis "model_contrat_form1[filiere]" avec "<filiere>"
    Et que je remplis "model_contrat_form1[livraisonLieu]" avec "<ll>"
    Et je presse "Étape suivante"
    Alors je devrais voir "<message>"
    Exemples:
      | nom | filiere | ll | message                        |
      | nom | filiere |    | Cette valeur n'est pas valide. |

  @database
  Plan du Scénario: Validation étape 2 - date de fin de souscription
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion des contrats vierges"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Et que je suis "Créer un nouveau contrat"
    Et que je remplis "model_contrat_form1[nom]" avec "nom"
    Et que je remplis "model_contrat_form1[filiere]" avec "filiere"
    Et que je remplis "model_contrat_form1[livraisonLieu]" avec "1"
    Et je presse "Étape suivante"
    Et que je presse "btn-plus"
    Et que je presse "btn-plus"
    Et que je remplis le datepicker "model_contrat_form2[forclusion]" avec "<fin_souscription>"
    Et que je remplis le datepicker "model_contrat_form2[dates][1][dateLivraison]" avec "<date_livraison_1>"
    Et que je remplis le datepicker "model_contrat_form2[dates][2][dateLivraison]" avec "<date_livraison_2>"
    Et je presse "Étape suivante"
    Alors je devrais voir "La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)"
    Exemples:
    | fin_souscription | date_livraison_1 | date_livraison_2 |
    | 2000-08-15       | 2000-08-15      | 2000-08-16        |
    | 2000-08-15       | 2000-08-16      | 2000-08-35        |

  @database
  Scénario: Validation étape 2 - génération des dates
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion des contrats vierges"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Et que je suis "Créer un nouveau contrat"
    Et que je remplis "model_contrat_form1[nom]" avec "nom"
    Et que je remplis "model_contrat_form1[filiere]" avec "filiere"
    Et que je remplis "model_contrat_form1[livraisonLieu]" avec "1"
    Et je presse "Étape suivante"
    Quand je remplis le datepicker "date_livraison_debut" avec "2000-01-01"
    Et que je remplis le datepicker "date_livraison_fin" avec "2000-02-01"
    Et que je presse "Générer des dates"
    Alors le champ "model_contrat_form2[dates][1][dateLivraison]" devrait contenir "2000-01-01"
    Alors le champ "model_contrat_form2[dates][2][dateLivraison]" devrait contenir "2000-01-08"
    Alors le champ "model_contrat_form2[dates][3][dateLivraison]" devrait contenir "2000-01-15"
    Alors le champ "model_contrat_form2[dates][4][dateLivraison]" devrait contenir "2000-01-22"
    Alors le champ "model_contrat_form2[dates][5][dateLivraison]" devrait contenir "2000-01-29"

  @database
  Scénario: Validation étape 3 - multiples produits identiques
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion des contrats vierges"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Et que je suis "Créer un nouveau contrat"
    Et que je remplis "model_contrat_form1[nom]" avec "nom"
    Et que je remplis "model_contrat_form1[filiere]" avec "filiere"
    Et que je remplis "model_contrat_form1[livraisonLieu]" avec "1"
    Et je presse "Étape suivante"
    Et que je remplis le datepicker "model_contrat_form2[forclusion]" avec "2000-01-01"
    Et que je remplis le datepicker "date_livraison_debut" avec "2000-02-01"
    Et que je remplis le datepicker "date_livraison_fin" avec "2000-03-01"
    Et que je presse "Générer des dates"
    Et je presse "Étape suivante"
    Quand je presse "btn-plus"
    Et je presse "Étape suivante"
    Alors je devrais voir "Impossible d'indiquer plusieurs fois le même produit"
    Et le champ "cat[0]" devrait contenir "autre_divers"
    Et le champ "prod[0]" devrait contenir "1"
    Et le champ "cat[2]" devrait contenir "autre_divers"
    Et le champ "prod[2]" devrait contenir "1"
