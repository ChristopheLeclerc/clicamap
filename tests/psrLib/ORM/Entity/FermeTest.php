<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;
use Test\ValidationViolationTrait;

/**
 * @internal
 * @coversNothing
 */
class FermeTest extends ContainerAwareTestCase
{
    use ValidationViolationTrait;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    /**
     * @dataProvider getPremiereDateLivrableAvecDelaisProvider
     */
    public function testGetPremiereDateLivrableAvecDelais(?int $delais, Carbon $expected)
    {
        $ferme = new Ferme();
        $ferme->setDelaiModifContrat($delais);

        $res = $ferme->getPremiereDateLivrableAvecDelais(new Carbon('01-01-2000'));
        self::assertTrue($expected->eq($res));
    }

    public function getPremiereDateLivrableAvecDelaisProvider()
    {
        return [
            [null, new Carbon('01-01-2000')],
            [-1, new Carbon('01-01-2000')],
            [0, new Carbon('01-01-2000')],
            [1, new Carbon('02-01-2000')],
            [2, new Carbon('03-01-2000')],
            [3, new Carbon('04-01-2000')],
        ];
    }

    public function testInRegroupementFalse()
    {
        $ferme = new Ferme();
        self::assertFalse($ferme->inRegroupement());
    }

    public function testInRegroupementTrue()
    {
        $ferme = new Ferme();
        $ferme->setRegroupement(new FermeRegroupement());

        self::assertTrue($ferme->inRegroupement());
    }

    public function testNomRequiredNotProvided()
    {
        $ferme = new Ferme();
        $ferme->setNom(null);

        $violations = $this->validator->validate($ferme);
        self::assertViolationContainsMessage($violations, 'This value should not be blank.', 'nom');
    }

    public function testNomRequiredProvided()
    {
        $ferme = new Ferme();
        $ferme->setNom('nom');

        $violations = $this->validator->validate($ferme);
        self::assertViolationNotContainsMessage($violations, 'This value should not be blank.', 'nom');
    }

    public function testSiretRequiredNoSiret()
    {
        $ferme = new Ferme();
        $ferme->setSiret(null);
        $violations = $this->validator->validate($ferme);
        self::assertViolationContainsMessage($violations, 'This value should not be blank.', 'siret');
    }

    public function testSiretRequiredSiret()
    {
        $ferme = new Ferme();
        $ferme->setSiret('1234');
        $violations = $this->validator->validate($ferme);
        self::assertViolationNotContainsMessage($violations, 'This value should not be blank.', 'siret');
    }

    public function testSiretRequiredRegroupementNoSiret()
    {
        $ferme = new Ferme();
        $ferme->setRegroupement(new FermeRegroupement());
        $violations = $this->validator->validate($ferme);
        self::assertViolationNotContainsMessage($violations, 'This value should not be blank.', 'siret');
    }
}
