<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\ORM\Entity\Files\Logo;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\DepartementRepository")
 * @ORM\Table(name="ak_departement")
 */
class Departement implements EntityWithLogoInterface, LocalisableEntity
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(name="dep_id", length=3)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="dep_nom", length=55, nullable=false)
     */
    private $nom;

    /**
     * @var null|Region
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Region", fetch="EAGER", inversedBy="departements")
     * @ORM\JoinColumn(name="dep_fk_reg_id", referencedColumnName="reg_id")
     */
    private $region;

    /**
     * @var ArrayCollection<Amapien>
     * @ORM\ManyToMany(targetEntity="PsrLib\ORM\Entity\Amapien", inversedBy="adminDepartments")
     * @JoinTable(name="ak_departement_administrateur",
     *      joinColumns={@JoinColumn(name="dep_adm_fk_departement_id", referencedColumnName="dep_id")},
     *      inverseJoinColumns={@JoinColumn(name="dep_adm_fk_amapien_id", referencedColumnName="a_id")}
     * )
     */
    private $admins;

    /**
     * @var null|Logo
     * @ORM\OneToOne(targetEntity="PsrLib\ORM\Entity\Files\Logo", cascade={"ALL"}, orphanRemoval=true)
     */
    private $logo;

    /**
     * @var null|string
     * @ORM\Column(name="dep_reseau_url", type="string", length=255, nullable=true)
     */
    private $url;

    public function __toString()
    {
        return (string) $this->getNom();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getDepartement(): ?Departement
    {
        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Departement
    {
        $this->nom = $nom;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): Departement
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Add admin.
     *
     * @return Departement
     */
    public function addAdmin(Amapien $admin)
    {
        $this->admins[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdmin(Amapien $admin)
    {
        return $this->admins->removeElement($admin);
    }

    /**
     * Get admins.
     *
     * @return ArrayCollection<Amapien>
     */
    public function getAdmins()
    {
        return $this->admins;
    }

    public function getLogo(): ?Logo
    {
        return $this->logo;
    }

    public function setLogo(?Logo $logo): Departement
    {
        $this->logo = $logo;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Departement
    {
        $this->url = $url;

        return $this;
    }
}
