#language: fr
Fonctionnalité: Inscription à une distribution en tant qu'amapien

  @database
  Scénario: Inscription à une distribution en tant qu'amapien
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Et que je suis "Mes distrib'AMAP"
    Et que je remplis le datepicker "search_distribution_amapien[period]" avec "01/01/2030 au 01/12/2030"
    Et que je devrais voir "test"
    Quand je presse "S'inscrire"
    Alors je devrais voir "0/1"
