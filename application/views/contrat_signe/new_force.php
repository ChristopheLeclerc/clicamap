<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
?>

<h3>Sélection de l'amapien</h3>

<div class="row">
    <form method="post">
    <div class="col-xs-6">
        <div class="form-group">
            <label>Amapien</label>
            <?=form_error('amapien'); ?>
            <select name="amapien" class="form-control js-select2">
                <option></option>
                <?php foreach ($amapiens as $amapien): ?>
                    <option value="<?=$amapien->getId(); ?>">
                        <?=sprintf(
    '%s %s - %s',
    $amapien->getNom(),
    $amapien->getPrenom(),
    $amapien->getEmail()
); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div class="col-xs-12">
        <button class="btn btn-success pull-right" style="margin-left: 10px;">Étape suivante</button>
        <a
            class="btn btn-default pull-right"
            href="<?=site_url('/contrat_signe/'); ?>"
        >Retour</a>

    </div>
    </form>
</div>
