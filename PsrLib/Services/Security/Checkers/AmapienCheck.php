<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Security\Checkers;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;

class AmapienCheck extends AbstractCheck
{
    public function isGrantedAmapienDisplayProfil(Amapien $amapien): bool
    {
        // Autorise les amapiens à éditer leur propre profil
        if ($this->getCurrentUser() instanceof Amapien) {
            if ($this->getCurrentUser()->getId() === $amapien->getId()) {
                return true;
            }
        }

        // Si on peut éditer le profil d'un amampien, on peut le visualiser
        if (true === $this->isGrantedAmapienEditExternal($amapien)) {
            return true;
        }

        return false;
    }

    public function isGrantedAmapienEditExternal(Amapien $amapien): bool
    {
        if ($this->getCurrentUser() instanceof Amapien) {
            // Le super admin peut éditer tous les profils
            if ($this->getCurrentUser()->isSuperAdmin()) {
                return true;
            }

            if ($this->getCurrentUser()->isAdminOf($amapien)) {
                return true;
            }
        }

        // Autorise les amaps a éditer le profil de leurs membres
        if ($this->getCurrentUser() instanceof Amap) {
            return $this->getCurrentUser()->getAmapiens()->contains($amapien);
        }

        return false;
    }

    public function isGrantedAmapienEditOwnProfile(Amapien $amapien): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->getId() === $amapien->getId()
        ;
    }

    public function isGrantedAmapienList(): bool
    {
        if ($this->getCurrentUser() instanceof Amap) {
            return true;
        }

        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isAdmin()
        ;
    }

    public function isGrantedAmapienCreateNoAmap(): bool
    {
        return $this->getCurrentUser() instanceof Amapien
            && $this->getCurrentUser()->isSuperAdmin()
        ;
    }
}
