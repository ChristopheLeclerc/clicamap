<?php

namespace PsrLib\Twig;

use PsrLib\Services\Security\SecurityChecker;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SecurityExtension extends AbstractExtension
{
    /**
     * @var SecurityChecker
     */
    private $securityChecker;

    public function __construct(SecurityChecker $securityChecker)
    {
        $this->securityChecker = $securityChecker;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('is_granted', [$this, 'isGranted']),
        ];
    }

    /**
     * @param null|mixed $subject
     */
    public function isGranted(string $action, $subject = null): bool
    {
        return $this->securityChecker->isGranted($action, $subject);
    }
}
