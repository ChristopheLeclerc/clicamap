<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\EntityWithLogoInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Url;

class EntityWithLogoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?EntityWithLogoInterface $entity */
        $entity = $options['data'];

        $imgContraints = [
            new Image([
                'maxHeight' => 1024,
                'maxWidth' => 1024,
                'mimeTypes' => ['image/jpeg', 'image/png'],
                'maxSize' => '2Mi',
            ]),
        ];

        if (null === $entity || null === $entity->getLogo()) {
            $imgContraints[] = new NotNull(['message' => 'Vous n\'avez pas sélectionné de fichier à envoyer.']);
        }

        $builder
            ->add('url', TextType::class, [
                'constraints' => [ // Constraint on form as entity invalid by default
                    new NotBlank(['message' => 'Le champ ne peut pas être vide.']),
                    new Url(['message' => 'L\'URL est invalide']),
                    new Length(['max' => 255]),
                ],
            ])
            ->add('img', FileType::class, [
                'mapped' => false,
                'constraints' => $imgContraints,
                'attr' => ['class' => 'hidden'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EntityWithLogoInterface::class,
        ]);
    }
}
