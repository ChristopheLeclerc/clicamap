<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


namespace Deployer;

require 'recipe/codeigniter.php';


// Project name
set('application', 'clicamap');

// Project repository
set('repository', 'git@gitlab.com:reseau-amap/clicamap.git');

// Disable git tty for CD
set('git_tty', false);

// Shared files/dirs between deploys 
add('shared_files', ['.env']);
add('shared_dirs', ['public/uploads', 'application/writable/session']);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts
host('doctrine')
    ->hostname('xz276.ftp.infomaniak.com')
    ->set('branch', 'doctrine')
    ->user('xz276_doctrine')
    ->set('deploy_path', '~/web/');

host('master')
    ->hostname('xz276.ftp.infomaniak.com')
    ->set('branch', 'master')
    ->user('xz276_doctrine')
    ->set('deploy_path', '~/sites/clicamap.org/');

host('preprod')
    ->hostname('xz276.ftp.infomaniak.com')
    ->set('branch', 'preprod')
    ->user('xz276_doctrine')
    ->set('deploy_path', '~/sites/preprod.clicamap.org/');

host('dev')
    ->hostname('xz276.ftp.infomaniak.com')
    ->set('branch', 'dev')
    ->user('xz276_doctrine')
    ->set('deploy_path', '~/sites/dev.clicamap.org/');

// Tasks
task(
    'deploy:doctrine:proxies',
    'cd {{release_path}} && export CI_ENV=production && php-7.4 vendor/bin/doctrine orm:generate-proxies'
);
task(
    'deploy:doctrine:migration',
    'cd {{release_path}} && export CI_ENV=production && php-7.4 vendor/bin/doctrine-migrations migrations:migrate -n'
)->onHosts(['master']);
before('deploy:symlink', 'deploy:doctrine:migration');
before('deploy:symlink', 'deploy:doctrine:proxies');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

task('deploy:writable', function () {});

set('bin/composer', function () {
    return '/opt/php/bin/composer2';
});
