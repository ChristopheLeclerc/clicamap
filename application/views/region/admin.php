<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Region $region */
?>

<script type="text/javascript">

  /*ALERTES & AUTOCOMPLÉTION CP / VILLE */
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 2500);

// ATTENTION, OUTIL D'AUTOCOMPLÉTION DU RÉSEAU !

    $(function () {
      $("#amapien").autocomplete({
        source: <?= '"'.site_url('ajax/get_amapien_suggestion"'); ?>,
        dataType: "json",
        minLength: 2,
        appendTo: "#container"
      });
    });

  });

</script>

<?php

echo '<h3>Les administrateurs de la région "'.$region->getNom().'"</h3>';

echo '<h5><a href="'.site_url('region').'">Les régions</a> <i class="glyphicon glyphicon-menu-right"></i> '.$region->getNom().'<i class="glyphicon glyphicon-menu-right"></i> Administrateurs</h5>';

echo '<div class="alert alert-info">';
echo 'Si vous voulez ajouter un administrateur à la région, utilisez le champ ci-dessous <strong>en vous aidant des suggestions proposées en cours de frappe</strong>.&nbsp;';
// echo 'L\'amapien choisi pour être administrateur doit faire partie de la région par l\'adresse de livraison de son AMAP.';
echo 'Dans le champ d\'ajout, il convient de commencer par le nom. L\'ajout se fera via l\'email afin d\'éviter tout problème en cas d\'homonyme. Pour ensuite retirer les droits d\'administration à l\'amapien, il suffit de cliquer sur l\'amapien à supprimer.';
echo '</div>';

echo '<form method="POST" action="'.site_url('region/add_reg_admin/'.$region->getId()).'">';

echo '<div class="row">';
echo '<div class="form-group col-sm-12">';
echo '<input type="text" class="form-control" id="amapien" name="amapien" value="" placeholder="Exemple : Dupont Jean"/>';
echo '<div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>';
echo form_error('amapien');
echo '</div>';
echo '<div class="form-group col-sm-12">';
echo '<input type="submit" value="Ajouter" class="btn btn-success pull-right"/>';
echo '</div>';
echo '</div>';
echo '</form>';

$currentUser = get_current_connected_user();
/** @var \PsrLib\ORM\Entity\Amapien $regAdm */
foreach ($region->getAdmins() as $regAdm) {
    if ($regAdm->getId() !== $currentUser->getId()) {
        echo '<a href="#" data-toggle="modal" data-target="#modal_'.$regAdm->getId().'" ><button class="btn btn-primary btn-sm" data-toggle="tooltip" title="Retirer les droits à l\'amapien">'.mb_strtoupper($regAdm->getNom()).' '.ucfirst($regAdm->getPrenom()).' <i class="glyphicon glyphicon-remove"></i></button></a>&nbsp;';
    } else {
        echo '<button class="btn btn-default btn-sm" disabled >'.mb_strtoupper($regAdm->getNom()).' '.ucfirst($regAdm->getPrenom()).'</button>&nbsp;';
    }

    echo '<div class="modal fade text-left" id="modal_'.$regAdm->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    echo '<div class="modal-dialog">';
    echo '<div class="modal-content">';
    echo '<div class="modal-header">';
    echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    echo '<h4 class="modal-title" id="myModalLabel">Région "'.$region->getNom().'"</h4>';
    echo '</div>';
    echo '<div class="modal-body">';
    echo '<h5>Voulez-vous vraiment retirer les droits d\'administration à "'.$regAdm->getNom().' '.$regAdm->getPrenom().'" ?</h5>';
    echo '<p class="text-right">';
    echo '<a href="'.site_url('region/remove_reg_admin/'.$region->getId().'/'.$regAdm->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
    echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
    echo '</p>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}

?>
