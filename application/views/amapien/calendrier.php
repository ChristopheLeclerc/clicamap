<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var string $calendrier */
/** @var array $livraisons */
/** @var \PsrLib\ORM\Entity\AmapLivraisonLieu[] $livraison_lieu */
$currentUser = get_current_connected_user();
?>

<h3>Calendrier des livraisons</h3>

<div class="alert alert-info">Une date en vert indique une livraison, cliquer dessus pour voir le détail.</div>

<?php

echo '<div class="table-responsive calendrier">';
echo $calendrier;
echo '</div>';

if (isset($livraisons)) {
    foreach ($livraisons as $key => $jour) {
        echo '<div class="modal fade" id="modal_'.$key.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Livraisons du '.$key.'/'.$mois.'/'.$an.'</h4>
    </div>
    <div class="modal-body">';

        if ($jour['mc']) {
            /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
            foreach ($jour['mc'] as $mc) {
                $ll = $mc->getLivraisonLieu();
                echo '<div class="well">';
                echo '<h3>'.ucfirst($mc->getNom()).'</h3>';
                echo '<h4>Lieu : '.ucfirst($ll->getNom()).' - '.$ll->getAdresse().', '.$ll->getVille()->getCpString().' '.$ll->getVille()->getNom().'</h4>';
                echo '<ul>';

                /** @var \PsrLib\ORM\Entity\ContratCellule $cellule */
                foreach ($jour['cellules'] as $cellule) {
                    if ($cellule->getModeleContratProduit()->getModeleContrat()->getId() === $mc->getId()) {
                        $produit = $cellule->getModeleContratProduit();
                        if (0.0 !== $cellule->getQuantite()) {
                            echo '<li>'.ucfirst($produit->getNom()).' ('.$produit->getConditionnement().') : <strong>'.$cellule->getQuantite().'</strong></li>';
                        }
                    }
                }

                echo '</ul>';
                echo '</div>';
            }
        } else {
            echo '<h5>Il n\'y a pas encore de commande pour cette date.</h5>';
        }

        echo '</div>
    </div>
    </div>
    </div>';
    }
}
