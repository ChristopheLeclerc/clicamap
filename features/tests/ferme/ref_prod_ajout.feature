# language: fr
Fonctionnalité: Ajout d'un référent produit

  @database
  Scénario: Ajouter un référent produit à l'AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis sur "/ferme"
    Et que je remplis "search_ferme[region]" avec "84"
    Et que je suis "Référents produits de la ferme"
    Lorsque je remplis "ferme_ref_produit[amapien]" avec "2"
    Et que je presse "Ajouter"
    Alors je devrais voir "amapien@test.amap-aura.org"

  @database
  Scénario: Masquer l'ajout de référent produit déjà référent
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis sur "/ferme"
    Et que je remplis "search_ferme[region]" avec "84"
    Et que je suis "Référents produits de la ferme" sur la ligne contenant "Paysan prénom PAYSAN NOM"
    Alors le selecteur "ferme_ref_produit[amapien]" devrait contenir l'option "2"
    Et le selecteur "ferme_ref_produit[amapien]" ne devrait pas contenir l'option "3"
    Et je suis sur "/ferme"
    Et je remplis "search_ferme[region]" avec "84"
    Et je suis "Référents produits de la ferme" sur la ligne contenant "Paysan prénom 2 PAYSAN NOM 2"
    Et le selecteur "ferme_ref_produit[amapien]" devrait contenir l'option "2"
    Et le selecteur "ferme_ref_produit[amapien]" devrait contenir l'option "3"
