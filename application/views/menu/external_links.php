<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<?php if (!empty($liens)) { ?>

<li class="dropdown dropdown-menu-right">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">www.AMAP&nbsp;<span class="caret"></span>&nbsp;</a>
    <ul class="dropdown-menu">
    <?php
    foreach ($liens as $lien) {
        echo '<li><a target="_blank" href="'.prep_url($lien['lien']).'">'.$lien['nom'].'</a></li>';
    }
    ?>
    </ul>
</li>

<?php } ?>
