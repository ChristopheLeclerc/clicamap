<?php

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AmapienRepository;

/**
 * @internal
 * @coversNothing
 */
class AmapienRepositoryTest extends RepositoryTest
{
    /**
     * @var AmapienRepository
     */
    private $amapienRepo;

    public function setUp()
    {
        parent::setUp();
        $this->amapienRepo = $this->em->getRepository(Amapien::class);
    }

    public function testDistributionAmapienAvailiable()
    {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $distribution = $this->recordDistribution($amap);

        $amapiens = $this->amapienRepo->qbDistributionAmapienAvailiable($distribution)->getQuery()->getResult();
        self::assertCount(2, $amapiens);
        self::assertSame('amapien@test.amap-aura.org', $amapiens[0]->getEmail());
        self::assertSame('amapienref@test.amap-aura.org', $amapiens[1]->getEmail());
    }

    public function testDistributionAmapienAvailiableInactive()
    {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $distribution = $this->recordDistribution($amap);

        $amapien = $this
            ->em
            ->getRepository(Amapien::class)
            ->findOneBy([
                'email' => 'amapien@test.amap-aura.org',
            ])
        ;
        $amapien->setEtat(Amapien::ETAT_INACTIF);
        $this->em->flush();

        $amapiens = $this->amapienRepo->qbDistributionAmapienAvailiable($distribution)->getQuery()->getResult();
        self::assertCount(1, $amapiens);
        self::assertSame('amapienref@test.amap-aura.org', $amapiens[0]->getEmail());
    }

    public function testDistributionAmapienAvailiableRecorded()
    {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $distribution = $this->recordDistribution($amap);

        $amapien = $this
            ->em
            ->getRepository(Amapien::class)
            ->findOneBy([
                'email' => 'amapien@test.amap-aura.org',
            ])
        ;
        $distribution->addAmapien($amapien);
        $this->em->flush();

        $amapiens = $this->amapienRepo->qbDistributionAmapienAvailiable($distribution)->getQuery()->getResult();
        self::assertCount(1, $amapiens);
        self::assertSame('amapienref@test.amap-aura.org', $amapiens[0]->getEmail());
    }

    private function recordDistribution(Amap $amap)
    {
        $distribution = new AmapDistribution(
            AmapDistributionDetail::create(new Time(17, 00), new Time(18, 00), 10, 'test'),
            $amap->getLivraisonLieux()->first(),
            new Carbon('2000-01-01')
        );
        $this->em->persist($distribution);
        $this->em->flush();

        return $distribution;
    }
}
