<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AkDocument.
 *
 * @ORM\Table(name="ak_document")
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @var null|int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255, nullable=false)
     */
    private $lien = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="permission_anonyme", type="boolean", nullable=false)
     */
    private $permissionAnonyme = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="permission_admin", type="boolean", nullable=false)
     */
    private $permissionAdmin = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="permission_paysan", type="boolean", nullable=false)
     */
    private $permissionPaysan = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="permission_amap", type="boolean", nullable=false)
     */
    private $permissionAmap = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="permission_amapienref", type="boolean", nullable=false)
     */
    private $permissionAmapienref = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="permission_amapien", type="boolean", nullable=false)
     */
    private $permissionAmapien = false;

    public function nbPermissionsSelected(): int
    {
        $nbSelected = 0;
        // @phpstan-ignore-next-line
        foreach ($this as $key => $value) { // Loop over all permissions properties
            if (0 === strpos($key, 'permission')
                && true === $this->{$key}) {
                ++$nbSelected;
            }
        }

        return $nbSelected;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Document
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLien(): string
    {
        return $this->lien;
    }

    public function setLien(string $lien): Document
    {
        $this->lien = $lien;

        return $this;
    }

    public function isPermissionAnonyme(): bool
    {
        return $this->permissionAnonyme;
    }

    public function setPermissionAnonyme(bool $permissionAnonyme): Document
    {
        $this->permissionAnonyme = $permissionAnonyme;

        return $this;
    }

    public function isPermissionAdmin(): bool
    {
        return $this->permissionAdmin;
    }

    public function setPermissionAdmin(bool $permissionAdmin): Document
    {
        $this->permissionAdmin = $permissionAdmin;

        return $this;
    }

    public function isPermissionPaysan(): bool
    {
        return $this->permissionPaysan;
    }

    public function setPermissionPaysan(bool $permissionPaysan): Document
    {
        $this->permissionPaysan = $permissionPaysan;

        return $this;
    }

    public function isPermissionAmap(): bool
    {
        return $this->permissionAmap;
    }

    public function setPermissionAmap(bool $permissionAmap): Document
    {
        $this->permissionAmap = $permissionAmap;

        return $this;
    }

    public function isPermissionAmapienref(): bool
    {
        return $this->permissionAmapienref;
    }

    public function setPermissionAmapienref(bool $permissionAmapienref): Document
    {
        $this->permissionAmapienref = $permissionAmapienref;

        return $this;
    }

    public function isPermissionAmapien(): bool
    {
        return $this->permissionAmapien;
    }

    public function setPermissionAmapien(bool $permissionAmapien): Document
    {
        $this->permissionAmapien = $permissionAmapien;

        return $this;
    }
}
