<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratDatesReglement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDatesReglement;
use PsrLib\Services\MoneyHelper;
use RuntimeException;

class ExcelGenerator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function generer_synthese_contrat_reglements(ModeleContrat $mc): string
    {
        if (2 !== $mc->getVersion()) {
            throw new RuntimeException('Compatible uniquement avec les contrats en V2');
        }

        $ll = $mc->getLivraisonLieu();
        $amap = $ll->getAmap();
        /** @var Contrat[] $contrats */
        $contrats = $mc->getContrats()->toArray();
        usort($contrats, function (Contrat $a, Contrat $b) {
            return strnatcmp(mb_strtolower($a->getAmapien()->getNom()), mb_strtolower($b->getAmapien()->getNom()));
        });

        $nbContrats = count($contrats);
        /** @var Amapien[] $amapiens */
        $amapiens = [];
        foreach ($contrats as $contrat) {
            $amapiens[] = $contrat->getAmapien();
        }
        /** @var ModeleContratDatesReglement[] $dateReglementModelContrat */
        $dateReglementModelContrat = $mc->getDateReglements()->toArray();
        $dateReglementContratSigne = [];
        foreach ($contrats as $contrat) {
            $dateReglementContratSigne[$contrat->getId()] = $contrat->getDatesReglements()->toArray();
        }

        // Extraction des règlements depuis les contrats pour ne prendre en compte que ceux qui sont reelement utilisés
        $reglements = [];
        foreach ($contrats as $contrat) {
            if (!in_array($contrat->getReglementType(), $reglements)) {
                $reglements[] = $contrat->getReglementType();
            }
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Reglage hauteur et largeur
        $sheet->getColumnDimension('A')->setWidth(24);
        $sheet->getColumnDimension('B')->setWidth(24);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getDefaultRowDimension()->setRowHeight(16);
        $sheet->getDefaultColumnDimension()->setWidth(12);
        $sheet->getRowDimension(5)->setRowHeight(30);
        $sheet->getRowDimension(7)->setRowHeight(30);
        $sheet->getRowDimension(8 + count($amapiens))->setRowHeight(30);
        $sheet->getRowDimension(9 + count($amapiens))->setRowHeight(30);

        // HEADER
        $sheet->setCellValue('A1', 'Synthèse des règlements du contrat : '.$mc->getNom());
        $sheet->setCellValue('A2', $amap->getNom());
        $sheet->setCellValue('A3', 'Extrait le '.Carbon::now()->format('d/m/Y H:i:s'));
        $sheet->getStyle('A1:A3')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);
        $sheet->setCellValue('A'.(8 + $nbContrats), 'Détail par type de paiement');
        $sheet->setCellValue('A'.(9 + $nbContrats), 'TOTAL par date de règlement');

        // LISTE DES AMAPIENS
        $sheet->setCellValue('A7', 'Nom');
        $sheet->setCellValue('B7', 'Prénom');
        foreach ($amapiens as $i => $amapien) {
            $line = 8 + $i;
            $sheet->setCellValue('A'.$line, $amapien->getNom());
            $sheet->setCellValue('B'.$line, $amapien->getPrenom());
        }

        // Dates de livraisons
        $maxCol = 4;
        foreach ($dateReglementModelContrat as $date) {
            $dateString = $date->getDate()->format('d/m/Y');
            $sheet->getCellByColumnAndRow($maxCol, 5)->setValue($dateString);
            $sheet->mergeCellsByColumnAndRow(
                $maxCol,
                5,
                $maxCol + count($reglements) - 1,
                5
            );
            foreach ($reglements as $reglement) {
                $sheet
                    ->getCellByColumnAndRow($maxCol, 7)
                    ->setValue(ModeleContrat::getModeLabel($reglement))
                ;
                $currentLine = 8;
                foreach ($contrats as $contrat) {
                    if ($reglement === $contrat->getReglementType()) {
                        /** @var ContratDatesReglement[] $dateReglement */
                        $dateReglement = array_values(array_filter(
                            $dateReglementContratSigne[$contrat->getId()],
                            function (ContratDatesReglement $contrat_signe_dates_reglement) use ($date) {
                                return $contrat_signe_dates_reglement->getModeleContratDatesReglement()->getId() === $date->getId();
                            }
                        ));

                        if (!empty($dateReglement)) {
                            $sheet
                                ->getCellByColumnAndRow($maxCol, $currentLine)
                                ->setValue(MoneyHelper::toStringDecimal($dateReglement[0]->getMontant()))
                            ;
                        }
                    }

                    ++$currentLine;
                }

                // Ligne "Détail par type de paiement"
                $colString = Coordinate::stringFromColumnIndex($maxCol);
                $sheet
                    ->getCellByColumnAndRow($maxCol, $currentLine)
                    ->setValue(
                        sprintf('=SUM(%s8:%s%d)', $colString, $colString, $currentLine - 1)
                    )
                ;

                ++$maxCol;
            }
        }

        $sheet->setCellValue('C5', 'Date de règlement');
        // Colonne "Total par AMAPien"
        $sheet->setCellValue('C7', 'Total par amapien');
        $colString = Coordinate::stringFromColumnIndex($maxCol - 1);
        foreach ($amapiens as $i => $amapien) {
            $line = 8 + $i;
            $sheet
                ->getCellByColumnAndRow(3, $line)
                ->setValue(
                    sprintf('=SUM(D%d:%s%d)', $line, $colString, $line)
                )
            ;
        }
        $sheet->setCellValue(
            'C'.(8 + $nbContrats),
            sprintf('=SUM(C8:C%d)', 7 + $nbContrats)
        );

        // Ligne "Total par date de règlement"
        $col = 4;
        $line = 9 + count($amapiens);
        foreach ($dateReglementModelContrat as $date) {
            $colEnd = $col + count($reglements) - 1;
            $colStartString = Coordinate::stringFromColumnIndex($col);
            $colEndString = Coordinate::stringFromColumnIndex($colEnd);
            $sheet
                ->getCellByColumnAndRow($col, $line)
                ->setValue(
                    sprintf('=SUM(%s%d:%s%d)', $colStartString, $line - 1, $colEndString, $line - 1)
                )
            ;
            $sheet->mergeCellsByColumnAndRow($col, $line, $colEnd, $line);
            $col = $colEnd + 1;
        }

        // Styles
        $sheet
            ->getStyleByColumnAndRow(3, 5, $maxCol - 1, 9 + count($amapiens))
            ->applyFromArray([
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
            ])
        ;

        $sheet
            ->getStyleByColumnAndRow(3, 8, $maxCol - 1, 7 + count($amapiens))
            ->applyFromArray([
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                ],
            ])
        ;

        $sheet
            ->getStyleByColumnAndRow(1, 8, 2, 7 + count($amapiens))
            ->applyFromArray([
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
            ])
        ;

        $outputFileName = tempnam(sys_get_temp_dir(), 'syntheseContrat_');
        $objWriter = new Xls($spreadsheet);
        $objWriter->save($outputFileName);

        return $outputFileName;
    }
}
