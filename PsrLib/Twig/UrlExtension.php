<?php

namespace PsrLib\Twig;

use CI_Config;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class UrlExtension extends AbstractExtension
{
    /**
     * @var CI_Config
     */
    private $ciConfig;

    public function __construct(CI_Config $ciConfig)
    {
        $this->ciConfig = $ciConfig;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('base_url', [$this, 'baseUrl'], ['is_safe' => ['html']]),
            new TwigFunction('site_url', [$this, 'siteUrl'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param string|string[] $uri URI string or an array of segments
     */
    public function baseUrl($uri = '', string $protocol = null): string
    {
        return $this->ciConfig->base_url($uri, $protocol);
    }

    /**
     * @param string|string[] $uri URI string or an array of segments
     */
    public function siteUrl($uri = '', string $protocol = null): string
    {
        return $this->ciConfig->site_url($uri, $protocol);
    }
}
