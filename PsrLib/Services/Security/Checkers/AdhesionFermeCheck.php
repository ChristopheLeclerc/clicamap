<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Security\Checkers;

use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Paysan;

class AdhesionFermeCheck extends AbstractCheck
{
    public function isGrantedList(): bool
    {
        return $this->getCurrentUser() instanceof Paysan;
    }

    public function isGrantedDownload(?AdhesionFerme $adhesionFerme): bool
    {
        if (null === $adhesionFerme) {
            return false;
        }

        if (null === $adhesionFerme->getVoucherFileName()) {
            return false;
        }

        if ($this->getCurrentUser() instanceof Amapien) {
            return $this->getCurrentUser()->isAdminOf($adhesionFerme->getFerme());
        }

        if ($this->getCurrentUser() instanceof Paysan) {
            return $this->getCurrentUser()->getFerme() === $adhesionFerme->getFerme();
        }

        return false;
    }
}
