<?php

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\AmapLivraisonLieu;

class AmapDistributionDetailMassEdit
{
    /**
     * @var AmapDistributionDetail
     */
    private $detail;

    /**
     * @var ?AmapLivraisonLieu
     */
    private $livraisonLieu;

    public function __construct(AmapDistributionDetail $detail, ?AmapLivraisonLieu $livraisonLieu)
    {
        $this->detail = $detail;
        $this->livraisonLieu = $livraisonLieu;
    }

    public function getDetail(): AmapDistributionDetail
    {
        return $this->detail;
    }

    public function setDetail(AmapDistributionDetail $detail): AmapDistributionDetailMassEdit
    {
        $this->detail = $detail;

        return $this;
    }

    public function getLivraisonLieu(): ?AmapLivraisonLieu
    {
        return $this->livraisonLieu;
    }

    public function setLivraisonLieu(?AmapLivraisonLieu $livraisonLieu): AmapDistributionDetailMassEdit
    {
        $this->livraisonLieu = $livraisonLieu;

        return $this;
    }
}
