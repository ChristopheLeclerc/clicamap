<?php

namespace PsrLib\Services;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use Symfony\Component\HttpFoundation\Session\Session;

class Authentification
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Session
     */
    private $session;

    public function __construct(EntityManagerInterface $em, Session $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * @return null|BaseUser
     */
    public function getCurrentUser()
    {
        if ('amapien' === $this->session->get('statut')) {
            /** @var null|Amapien */
            return $this->em->getReference(Amapien::class, $this->session->get('ID'));
        }
        if ('paysan' === $this->session->get('statut')) {
            /** @var null|Paysan */
            return $this->em->getReference(Paysan::class, $this->session->get('ID'));
        }
        if ('amap' === $this->session->get('statut')) {
            /** @var null|Amap */
            return $this->em->getReference(Amap::class, $this->session->get('ID'));
        }
        if ('regroupement' === $this->session->get('statut')) {
            /** @var null|Amap */
            return $this->em->getReference(FermeRegroupement::class, $this->session->get('ID'));
        }

        return null;
    }
}
