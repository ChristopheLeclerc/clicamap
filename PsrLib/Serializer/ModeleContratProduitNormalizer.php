<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\FermeProduit;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\TypeProduction;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ModeleContratProduitNormalizer implements DenormalizerAwareInterface, ContextAwareDenormalizerInterface
{
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        /** @var ModeleContratProduit $mcp */
        $mcp = $this->denormalizer->denormalize($data, ModeleContratProduit::class);

        $fermeProduitId = $data['fermeProduit']['id'] ?? null;
        if (null !== $fermeProduitId) {
            $mcp->setFermeProduit($this->em->getReference(FermeProduit::class, $fermeProduitId));
        }

        $tpId = $data['typeProduction']['id'] ?? null;
        if (null !== $tpId) {
            $mcp->setTypeProduction($this->em->getReference(TypeProduction::class, $tpId));
        }

        return $mcp;
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return ModeleContratProduit::class === $type
            && isset($context['groups'])
            && 'wizard' === $context['groups']
        ;
    }
}
