<?php

namespace PsrLib\Command;

use DI\Annotation\Inject;
use PsrLib\Services\FixtureLoader;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FixtureLoad extends BaseCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:fixtures:load';
    /**
     * @Inject
     *
     * @var FixtureLoader
     */
    private $loader;

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->write('Loading fixtures : ');
        $this->loader->loadFixtures();
        $io->writeln('ok');

        return 0;
    }
}
