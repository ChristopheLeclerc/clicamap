<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Services;

use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\Services\ModeleContratDeplacementReportFormater;

/**
 * @internal
 * @coversNothing
 */
class ModeleContratDeplacementReportFormatterTest extends TestCase
{
    /**
     * @dataProvider formatProvider
     *
     * @param int $reportNb
     */
    public function testFormat(bool $permissionDeplacement, bool $permissionReport, ?int $reportNb, string $expected)
    {
        $mc = new ModeleContrat();
        $mc->setAmapienPermissionDeplacementLivraison($permissionDeplacement);
        $mc->setAmapienPermissionReportLivraison($permissionReport);
        $mc->setAmapienReportNb($reportNb);

        self::assertSame(
            $expected,
            ModeleContratDeplacementReportFormater::formatModeleContratDeplacementReport($mc)
        );
    }

    public function formatProvider()
    {
        return [
            [false, false, null, 'NON'],
            [true, false, null, 'Oui sur une date sans livraison'],
            [false, true, 12, 'Oui, 12 sur une date avec livraison'],
            [true, true, 12, 'Oui sans livraison et 12 sur une date avec livraison'],
        ];
    }
}
