<?php

namespace PsrLib\DTO;

class SearchDistributionAmapState extends SearchDistributionBaseState
{
    /**
     * @var bool
     */
    private $complete = false;

    public function isComplete(): bool
    {
        return $this->complete;
    }

    public function setComplete(bool $complete): SearchDistributionAmapState
    {
        $this->complete = $complete;

        return $this;
    }
}
