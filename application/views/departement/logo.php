<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Departement $departement */
/** @var \Symfony\Component\Form\FormView $form */
$region = $departement->getRegion();
?>

<h3>Nouveau logo</h3>

<?= '<h5><a href="'.site_url('region').'">Les régions</a> <i class="glyphicon glyphicon-menu-right"></i> '.$region->getNom().' <i class="glyphicon glyphicon-menu-right"></i><a href="'.site_url('region/departement/'.$region->getId()).'"> Les départements</a> <i class="glyphicon glyphicon-menu-right"></i> '.$departement->getNom().'<i class="glyphicon glyphicon-menu-right"></i> Administrateurs</h5>';
?>

<?= form_open_multipart('', ['class' => 'form-horizontal']);
?>

<div class="form-group">
    <label class="col-sm-1 control-label" for="url">URL</label>
    <div class="col-sm-4">
        <?= render_form_text_widget($form['url']); ?>
    </div>
</div>
<?= form_error('url'); ?>

<div class="alert alert-info">
    Si le réseau ne possède pas de site internet, indiquer le site du Miramap http://miramap.org
</div>

<?php
if (null !== $departement->getLogo()) {
    echo '
    <div class="form-group">
        <label class="col-sm-1 control-label" for="logo" style="padding-top: 12px">Logo actuel</label>
        <div class="col-sm-4">
            <div class="logo-reseau">
                <img class="img-responsive" src="'.base_url($departement->getLogo()->getFileRelativePath()).'">
            </div>
        </div>

    </div>';
}?>

<div class="form-group">
    <label class="col-sm-1 control-label" for="logo" style="padding-top: 12px">Logo</label>
    <div class="col-sm-4">
        <div class="input-group" style="margin:5px 0;">
            <label class="input-group-btn">
                  <span class="btn btn-primary">
                  Parcourir&hellip; <?= render_form_file_widget($form['img']); ?>
                  </span>
            </label>
            <input type="text" class="form-control" readonly>
        </div>
        <p class="help-block">Si vous modifiez le lien, laissez vide pour garder le logo actuel. Seul les fichier .jpg et .png sont autorisés, avec un poids maximum de 2 Mo et une taille maximale de 1024x1024.</p>
    </div>

</div>

<?= upload_display_errors('<p class="text-danger">'); ?>

<button type="submit" class="btn btn-success pull-right" style="margin-left: .5em">Sauvegarder</button>
<a class="btn btn-default pull-right" href="<?= site_url('/region/departement/'.$departement->getRegion()->getId()); ?>">Annuler</a>

<?= form_close();
?>

<script type="text/javascript">
    /*PARCOURIR*/
    $(function() {

        // We can attach the `fileselect` event to all file inputs on the page
        $(document).on('change', ':file', function() {
            var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        // We can watch for our custom `fileselect` event like this
        $(document).ready( function() {
            $(':file').on('fileselect', function(event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });
        });

    });

</script>

