<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<h3>Pièces jointes</h3>

<?php

$mot = 'Création';

echo '<h5><a href="'.site_url('publipostage').'">Publipostage</a> <i class="glyphicon glyphicon-menu-right"></i> Pièces jointes</h5>';

echo form_open_multipart('publipostage/email_form_3', 'id="form3"');
// VARIABLES CACHÉES À LA CRÉATION
echo '<input type="hidden"  name="etape_pj" value="1" />';
// -----------------------------------------------------------------------------
if (set_value('reg')) {
    foreach (set_value('reg') as $reg) {
        echo '<input type="hidden"  name="reg[]" value="'.$reg.'" />';
    }
}

if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}

if (set_value('media')) {
    echo '<input type="hidden"  name="media" value="'.set_value('media').'" />';
}
if (set_value('qui')) {
    echo '<input type="hidden"  name="qui" value="'.set_value('qui').'" />';
}
if (set_value('en_recherche_partenariat')) {
    echo '<input type="hidden"  name="en_recherche_partenariat" value="'.set_value('en_recherche_partenariat').'" />';
}
if (set_value('adh_non')) {
    echo '<input type="hidden"  name="adh_non" value="'.set_value('adh_non').'" />';
}
if (set_value('adh_oui')) {
    echo '<input type="hidden"  name="adh_oui" value="'.set_value('adh_oui').'" />';
}
if (set_value('certification')) {
    echo '<input type="hidden"  name="certification" value="'.set_value('certification').'" />';
}
if (set_value('titre')) {
    echo '<input type="hidden"  name="titre" value="'.set_value('titre').'" />';
}
if (set_value('txt')) {
    echo '<input type="hidden"  name="txt" value="'.set_value('txt').'" />';
}
if (set_value('url')) {
    echo '<input type="hidden"  name="url" value="'.set_value('url').'" />';
}
// -----------------------------------------------------------------------------
?>

<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group col-md-12">
                <label for="pj" class="control-label">Pièces jointes (.pdf < 100 ko chacune) : </label>
                <input type="hidden" name="MAX_FILE_SIZE[]" value="100000"/>
                <input type="file" multiple class="form-control-file" id="pj" name="pj[]"/>
                <small id="fileHelp" class="form-text text-muted">Possibilité de télécharger une pièce jointe.</small>
                <?php
                if (isset($alerte_pj)) {
                    echo '<div class="alert alert-danger">'.$alerte_pj.'</div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <input type="submit" value="Sauvegarder" class="pull-right btn btn-success" style="margin-left:5px;"/>
</div>

</form>

<!-- ----------------------------------------------------------------------- -->
<!-- ÉTAPE PRÉCÉDENTE -->
<?php echo form_open_multipart('publipostage/email_form_2', 'id="form2"');
// VARIABLES CACHÉES
if (set_value('reg')) {
    foreach (set_value('reg') as $reg) {
        echo '<input type="hidden"  name="reg[]" value="'.$reg.'" />';
    }
}

if (set_value('dep')) {
    foreach (set_value('dep') as $dep) {
        echo '<input type="hidden"  name="dep[]" value="'.$dep.'" />';
    }
}

if (set_value('res')) {
    foreach (set_value('res') as $res) {
        echo '<input type="hidden"  name="res[]" value="'.$res.'" />';
    }
}

if (set_value('tp')) {
    foreach (set_value('tp') as $tp) {
        echo '<input type="hidden"  name="tp[]" value="'.$tp.'" />';
    }
}
// -----------------------------------------------------------------------------
if (set_value('media')) {
    echo '<input type="hidden"  name="media" value="'.set_value('media').'" />';
}
if (set_value('qui')) {
    echo '<input type="hidden"  name="qui" value="'.set_value('qui').'" />';
}
if (set_value('en_recherche_partenariat')) {
    echo '<input type="hidden"  name="en_recherche_partenariat" value="'.set_value('en_recherche_partenariat').'" />';
}
if (set_value('adh_non')) {
    echo '<input type="hidden"  name="adh_non" value="'.set_value('adh_non').'" />';
}
if (set_value('adh_oui')) {
    echo '<input type="hidden"  name="adh_oui" value="'.set_value('adh_oui').'" />';
}
if (set_value('certification')) {
    echo '<input type="hidden"  name="certification" value="'.set_value('certification').'" />';
}
if (set_value('titre')) {
    echo '<input type="hidden"  name="titre" value="'.set_value('titre').'" />';
}
if (set_value('txt')) {
    echo '<input type="hidden"  name="txt" value="'.set_value('txt').'" />';
}
if (set_value('url')) {
    echo '<input type="hidden"  name="url" value="'.set_value('url').'" />';
}

echo '<div class="form-group">';
echo '<input type="submit" value="Étape précédente" class="pull-right btn btn-default" style="margin-left:5px;"/>';
// echo '<a href="'.site_url('publipostage').'" class="pull-right btn btn-default">Annuler</a>';
echo '</div>';

echo '</form>';
