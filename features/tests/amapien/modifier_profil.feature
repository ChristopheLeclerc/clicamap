#language: fr
Fonctionnalité: Editer la fiche de l'amapien

  @database
  Plan du Scénario: Tester les messages d'erreur, profil Admin AURA et Admin Rhone
    Etant donné que je suis authentifié comme "amapien" avec le mail "<email>"
    Quand  je suis sur "/amapien"
    Et que je suis "Éditer la fiche de l'amapien"
    Alors  je devrais être sur "/amapien/informations_generales_edition/2"
    Et que je remplis "amapien[nom]" avec "<nom>"
    Et que je remplis "amapien[prenom]" avec "<prenom>"
    Et que je remplis "amapien[email]" avec "<mail>"
    Et que je remplis "amapien[newsletter]" avec "<news_radio>"
    Et que je remplis "amapien[ville]" avec "<ville>"
    Et que je presse "Sauvegarder"
    Alors  je devrais voir "<message>"

    Exemples:
      | email                          | nom         | prenom         | mail                       | news_radio | ville                          | message                                          |
      | admin.aura@test.amap-aura.org  |             | prenom amapien | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | admin.aura@test.amap-aura.org  | nom amapien |                | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | admin.aura@test.amap-aura.org  | nom amapien | prenom amapien | amapien@test.amap-aura.org |            | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être nulle.             |
      | admin.aura@test.amap-aura.org  | nom amapien | prenom amapien | amapientest.amap-aura.org  | oui        | 69001, LYON 1er Arrondissement | Cette valeur n'est pas une adresse email valide. |
      | admin.aura@test.amap-aura.org  | nom amapien | prenom amapien | amapien@test.amap-aura.org | oui        | 69001                          | Le champ \"Code postal, Ville\" est mal formaté. |
      | admin.rhone@test.amap-aura.org |             | prenom amapien | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | admin.rhone@test.amap-aura.org | nom amapien |                | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | admin.rhone@test.amap-aura.org | nom amapien | prenom amapien | amapien@test.amap-aura.org |            | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être nulle.             |
      | admin.rhone@test.amap-aura.org | nom amapien | prenom amapien | amapientest.amap-aura.org  | oui        | 69001, LYON 1er Arrondissement | Cette valeur n'est pas une adresse email valide. |
      | admin.rhone@test.amap-aura.org | nom amapien | prenom amapien | amapien@test.amap-aura.org | oui        | 69001                          | Le champ \"Code postal, Ville\" est mal formaté. |


  @database
  Plan du Scénario: Tester les messages d'erreur, profil Super Administrateur
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand  je suis sur "/amapien"
    Et que je remplis "search_amapien[region]" avec "84"
    Et que je suis "Éditer la fiche de l'amapien"
    Alors  je devrais être sur "/amapien/informations_generales_edition/2"
    Et que je remplis "amapien[nom]" avec "<nom>"
    Et que je remplis "amapien[prenom]" avec "<prenom>"
    Et que je remplis "amapien[email]" avec "<mail>"
    Et que je remplis "amapien[newsletter]" avec "<news_radio>"
    Et que je remplis "amapien[ville]" avec "<ville>"
    Et que je presse "Sauvegarder"
    Alors  je devrais voir "<message>"

    Exemples:
      | nom         | prenom         | mail                       | news_radio | ville                          | message                                          |
      |             | prenom amapien | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | nom amapien |                | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | nom amapien | prenom amapien | amapien@test.amap-aura.org |            | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être nulle.             |
      | nom amapien | prenom amapien | amapientest.amap-aura.org  | oui        | 69001, LYON 1er Arrondissement | Cette valeur n'est pas une adresse email valide. |
      | nom amapien | prenom amapien | amapien@test.amap-aura.org | oui        | 69001                          | Le champ \"Code postal, Ville\" est mal formaté. |


  @database
  Plan du Scénario: Tester les messages d'erreur, profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand  je suis sur "/amapien"
    Et que je suis "Éditer la fiche de l'amapien"
    Alors  je devrais être sur "/amapien/informations_generales_edition/2"
    Et que je remplis "amapien[nom]" avec "<nom>"
    Et que je remplis "amapien[prenom]" avec "<prenom>"
    Et que je remplis "amapien[email]" avec "<mail>"
    Et que je remplis "amapien[newsletter]" avec "<news_radio>"
    Et que je remplis "amapien[ville]" avec "<ville>"
    Et que je presse "Sauvegarder"
    Alors  je devrais voir "<message>"

    Exemples:
      | nom         | prenom         | mail                       | news_radio | ville                          | message                                          |
      |             | prenom amapien | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | nom amapien |                | amapien@test.amap-aura.org | oui        | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être vide.              |
      | nom amapien | prenom amapien | amapien@test.amap-aura.org |            | 69001, LYON 1er Arrondissement | Cette valeur ne doit pas être nulle.             |
      | nom amapien | prenom amapien | amapientest.amap-aura.org  | oui        | 69001, LYON 1er Arrondissement | Cette valeur n'est pas une adresse email valide. |
      | nom amapien | prenom amapien | amapien@test.amap-aura.org | oui        | 69001                          | Le champ \"Code postal, Ville\" est mal formaté. |


  @database
  Plan du Scénario: Tester l'édition du profil d'un amapien en tant qu'Admin AURA ou Admin Rhone ou AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "<email>"
    Quand  je suis sur "/amapien"
    Et que je suis "Éditer la fiche de l'amapien"
    Alors  je devrais être sur "/amapien/informations_generales_edition/2"
    Et que je remplis "amapien[nom]" avec "<nom>"
    Et que je remplis "amapien[prenom]" avec "<prenom>"
    Et que je remplis "amapien[email]" avec "<mail>"
    Et que je remplis "amapien[newsletter]" avec "<news_radio>"
    Et que je remplis "amapien[ville]" avec "<ville>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAPien a été mis à jour avec succès"

    Exemples:
      | email                          | nom         | prenom         | mail                       | news_radio | ville                          |
      | admin.aura@test.amap-aura.org  | nom amapien | prenom amapien | amapien@test.amap-aura.org | 1          | 69001, LYON 1er Arrondissement |
      | admin.rhone@test.amap-aura.org | nom amapien | prenom amapien | amapien@test.amap-aura.org | 1          | 69001, LYON 1er Arrondissement |
      | amap@test.amap-aura.org        | nom amapien | prenom amapien | amapien@test.amap-aura.org | 1          | 69001, LYON 1er Arrondissement |


  @database
  Plan du Scénario: Tester l'édition du profil d'un amapien en tant que Super Administrateur
    Etant donné que je suis authentifié comme "amapien" avec le mail "admin@test.amap-aura.org"
    Quand  je suis sur "/amapien"
    Et que je remplis "search_amapien[region]" avec "84"
    Et que je suis "Éditer la fiche de l'amapien"
    Alors  je devrais être sur "/amapien/informations_generales_edition/2"
    Et que je remplis "amapien[nom]" avec "<nom>"
    Et que je remplis "amapien[prenom]" avec "<prenom>"
    Et que je remplis "amapien[email]" avec "<mail>"
    Et que je remplis "amapien[newsletter]" avec "<news_radio>"
    Et que je remplis "amapien[ville]" avec "<ville>"
    Et que je presse "Sauvegarder"
    Alors je devrais voir "L'AMAPien a été mis à jour avec succès"

    Exemples:
      | nom         | prenom         | mail                       | news_radio | ville                          |
      | nom amapien | prenom amapien | amapien@test.amap-aura.org | 1          | 69001, LYON 1er Arrondissement |
