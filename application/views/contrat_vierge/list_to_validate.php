<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat[] $contrats */
/** @var \Symfony\Component\Form\FormView $searchForm */
?>

<h3>Gestion des contrats vierges</h3>


<div class="row">
    <div class="col-md-5">
        <form method="GET" action="/contrat_vierge" class="js-form-search">
            <div class="input-group">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button">AMAP</button>
                </span>
                <?= render_form_select_widget($searchForm['amap']); ?>
            </div>
        </form>
    </div>

    <div class="col-md-12">
        <?php if (0 === count($contrats)) : ?>
            <div class="alert alert-warning">Vous n'avez pas de contrats en attente de validation :-)</div>
        <?php else : ?>
            <table class="table table-hover">
	        <thead>
                <tr>
                    <th>Nom</th>
                    <th>Etat</th>
                    <th>Date de fin de souscription</th>
                    <th></th>
                </tr>
	        </thead>

                <?php foreach ($contrats as $contrat) : ?>
                    <tr>
                        <td>
                            <?=$contrat->getNom(); ?>
                        </td>
                        <td>
                            <?=\PsrLib\ORM\Entity\ModeleContrat::getEtatLabel($contrat->getEtat()); ?>
                        </td>
                        <td>
                            <?= $contrat->getForclusion()->format('d/m/Y'); ?>
                        </td>
                        <td>
			    <a href="<?=site_url('/contrat_vierge/v2_state_paysan_approval/'.$contrat->getId()); ?>" class="btn btn-success">Valider</a>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </table>
        <?php endif; ?>

    </div>
</div>
