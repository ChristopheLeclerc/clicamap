<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
/** @var \PsrLib\ORM\Entity\ModeleContratDate[] $mc_dates_livraison */
echo '<h3>Déplacer la date</h3>';

echo '<h5><a href="'.site_url('contrat_signe').'">Gestion des contrats signés</a> <i class="glyphicon glyphicon-menu-right"></i> '.$mc->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Déplacer une date de livraison <i class="glyphicon glyphicon-menu-right"></i> Étape 2</h5>';

echo '<form method="POST" action="'.site_url('contrat_signe/move_step_2/'.$mc->getId()).'">';
?>

<input type="hidden" name="step_2" value="1"/>

<div class="row">
    <div class="form-group">
        <label for="date_a_deplacer" class="col-md-12 control-label">La date à déplacer (Y-m-d)</label>
        <div class="col-md-12">

            <select class="form-control" id="date_a_deplacer" name="date_a_deplacer">

                <?php

                foreach ($mc_dates_livraison as $d_l) {
                    // La valeur est une construction avec l'ID et la date
                    // pour pouvoir récupérer les deux par la suite
                    echo '<option value="'.$d_l->getId().'">'.$d_l->getDateLivraison()->format('Y-m-d').'</option>';
                }

                ?>

            </select>

            <?php if (set_value('step_2')) {
                    echo form_error('date_a_deplacer');
                } ?>

        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <label for="nouvelle_date" class="col-md-12 control-label">La nouvelle date (Y-m-d)</label>
        <div class="col-md-12">

            <input type="text" data-date-format="yyyy-mm-dd" class="form-control datepicker" name="nouvelle_date"
                   value="<?= set_value('nouvelle_date'); ?>" placeholder="Cliquer pour choisir une nouvelle date ..."/>

            <?php

            if (set_value('step_2')) {
                echo form_error('nouvelle_date');
            }

            if ($alerte_date_existante) {
                echo '<div class="alert alert-danger">'.$alerte_date_existante.'</div>';
            }

            ?>
        </div>
    </div>
</div>

<div class="form-group">

    <input type="submit" value="Étape suivante" class="pull-right btn btn-success"/>

    <a href="<?= site_url('contrat_signe/accueil'); ?>" class="pull-right btn btn-default">Annuler</a>

</div>

</form>
