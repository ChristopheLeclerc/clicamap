<?php

namespace Test\Services\Exporters;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PHPUnit\Framework\TestCase;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\Services\Exporters\ExcelGeneratorDistributionAmap;

/**
 * @internal
 * @coversNothing
 */
class ExcelGeneratorDistributionAmapTest extends TestCase
{
    /**
     * @var ExcelGeneratorDistributionAmap
     */
    private $generator;

    public function setUp()
    {
        $this->generator = new ExcelGeneratorDistributionAmap();
    }

    public function testEmpty()
    {
        $file = $this->generator->genererExportDistributionInscrits([]);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        $this->headerTest($sheet);

        self::assertNull($sheet->getCell('A2')->getValue());
        self::assertNull($sheet->getCell('B2')->getValue());
        self::assertNull($sheet->getCell('C2')->getValue());
        self::assertNull($sheet->getCell('D2')->getValue());
        self::assertNull($sheet->getCell('E2')->getValue());
        self::assertNull($sheet->getCell('F2')->getValue());
        self::assertNull($sheet->getCell('G2')->getValue());
        self::assertNull($sheet->getCell('H2')->getValue());
    }

    public function testDistributionEmpty()
    {
        $dist1 = $this->buildDistribution(
            new Carbon('2000-02-01'),
            'Tache',
            new Time(5, 0),
            new Time(10, 0)
        );
        $file = $this->generator->genererExportDistributionInscrits([$dist1]);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        $this->headerTest($sheet);

        self::assertSame('01/02/2000', $sheet->getCell('A2')->getValue());
        self::assertSame('05:00', $sheet->getCell('B2')->getValue());
        self::assertSame('10:00', $sheet->getCell('C2')->getValue());
        self::assertSame('Tache', $sheet->getCell('D2')->getValue());
        self::assertNull($sheet->getCell('E2')->getValue());
        self::assertNull($sheet->getCell('F2')->getValue());
        self::assertNull($sheet->getCell('G2')->getValue());
        self::assertNull($sheet->getCell('H2')->getValue());

        self::assertNull($sheet->getCell('A3')->getValue());
    }

    public function testDistributionMultipeSortDate()
    {
        $dist1 = $this->buildDistribution(
            new Carbon('2000-02-02'),
            'Tache 2',
            new Time(6, 0),
            new Time(11, 0)
        );
        $dist2 = $this->buildDistribution(
            new Carbon('2000-02-01'),
            'Tache',
            new Time(5, 0),
            new Time(10, 0)
        );
        $file = $this->generator->genererExportDistributionInscrits([$dist1, $dist2]);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        $this->headerTest($sheet);

        self::assertSame('01/02/2000', $sheet->getCell('A2')->getValue());
        self::assertSame('05:00', $sheet->getCell('B2')->getValue());
        self::assertSame('10:00', $sheet->getCell('C2')->getValue());
        self::assertSame('Tache', $sheet->getCell('D2')->getValue());
        self::assertNull($sheet->getCell('E2')->getValue());
        self::assertNull($sheet->getCell('F2')->getValue());
        self::assertNull($sheet->getCell('G2')->getValue());
        self::assertNull($sheet->getCell('H2')->getValue());

        self::assertSame('02/02/2000', $sheet->getCell('A3')->getValue());
        self::assertSame('06:00', $sheet->getCell('B3')->getValue());
        self::assertSame('11:00', $sheet->getCell('C3')->getValue());
        self::assertSame('Tache 2', $sheet->getCell('D3')->getValue());
        self::assertNull($sheet->getCell('E3')->getValue());
        self::assertNull($sheet->getCell('F3')->getValue());
        self::assertNull($sheet->getCell('G3')->getValue());
        self::assertNull($sheet->getCell('H3')->getValue());
    }

    public function testDistributionMultipeSortHourStart()
    {
        $dist1 = $this->buildDistribution(
            new Carbon('2000-02-01'),
            'Tache 2',
            new Time(6, 0),
            new Time(11, 0)
        );
        $dist2 = $this->buildDistribution(
            new Carbon('2000-02-01'),
            'Tache',
            new Time(5, 0),
            new Time(10, 0)
        );
        $file = $this->generator->genererExportDistributionInscrits([$dist1, $dist2]);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        $this->headerTest($sheet);

        self::assertSame('01/02/2000', $sheet->getCell('A2')->getValue());
        self::assertSame('05:00', $sheet->getCell('B2')->getValue());
        self::assertSame('10:00', $sheet->getCell('C2')->getValue());
        self::assertSame('Tache', $sheet->getCell('D2')->getValue());
        self::assertNull($sheet->getCell('E2')->getValue());
        self::assertNull($sheet->getCell('F2')->getValue());
        self::assertNull($sheet->getCell('G2')->getValue());
        self::assertNull($sheet->getCell('H2')->getValue());

        self::assertSame('01/02/2000', $sheet->getCell('A3')->getValue());
        self::assertSame('06:00', $sheet->getCell('B3')->getValue());
        self::assertSame('11:00', $sheet->getCell('C3')->getValue());
        self::assertSame('Tache 2', $sheet->getCell('D3')->getValue());
        self::assertNull($sheet->getCell('E3')->getValue());
        self::assertNull($sheet->getCell('F3')->getValue());
        self::assertNull($sheet->getCell('G3')->getValue());
        self::assertNull($sheet->getCell('H3')->getValue());
    }

    public function testDistributionMultipeSortHourEnd()
    {
        $dist1 = $this->buildDistribution(
            new Carbon('2000-02-01'),
            'Tache 2',
            new Time(5, 0),
            new Time(11, 0)
        );
        $dist2 = $this->buildDistribution(
            new Carbon('2000-02-01'),
            'Tache',
            new Time(5, 0),
            new Time(10, 0)
        );
        $file = $this->generator->genererExportDistributionInscrits([$dist1, $dist2]);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        $this->headerTest($sheet);

        self::assertSame('01/02/2000', $sheet->getCell('A2')->getValue());
        self::assertSame('05:00', $sheet->getCell('B2')->getValue());
        self::assertSame('10:00', $sheet->getCell('C2')->getValue());
        self::assertSame('Tache', $sheet->getCell('D2')->getValue());
        self::assertNull($sheet->getCell('E2')->getValue());
        self::assertNull($sheet->getCell('F2')->getValue());
        self::assertNull($sheet->getCell('G2')->getValue());
        self::assertNull($sheet->getCell('H2')->getValue());

        self::assertSame('01/02/2000', $sheet->getCell('A3')->getValue());
        self::assertSame('05:00', $sheet->getCell('B3')->getValue());
        self::assertSame('11:00', $sheet->getCell('C3')->getValue());
        self::assertSame('Tache 2', $sheet->getCell('D3')->getValue());
        self::assertNull($sheet->getCell('E3')->getValue());
        self::assertNull($sheet->getCell('F3')->getValue());
        self::assertNull($sheet->getCell('G3')->getValue());
        self::assertNull($sheet->getCell('H3')->getValue());
    }

    public function testDistributionMultipeAmapiens()
    {
        $dist1 = $this->buildDistribution(
            new Carbon('2000-02-01'),
            'Tache',
            new Time(5, 0),
            new Time(10, 0)
        );

        $amapien1 = $this->buildAmapien(
            'nom 1',
            'prenom 1',
            'tel 1',
            'mail 1'
        );
        $amapien2 = $this->buildAmapien(
            'nom 2',
            'prenom 2',
            'tel 2',
            'mail 2'
        );
        $dist1->addAmapien($amapien1);
        $dist1->addAmapien($amapien2);

        $file = $this->generator->genererExportDistributionInscrits([$dist1]);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        $this->headerTest($sheet);

        self::assertSame('01/02/2000', $sheet->getCell('A2')->getValue());
        self::assertSame('05:00', $sheet->getCell('B2')->getValue());
        self::assertSame('10:00', $sheet->getCell('C2')->getValue());
        self::assertSame('Tache', $sheet->getCell('D2')->getValue());
        self::assertSame('nom 1', $sheet->getCell('E2')->getValue());
        self::assertSame('prenom 1', $sheet->getCell('F2')->getValue());
        self::assertSame('tel 1', $sheet->getCell('G2')->getValue());
        self::assertSame('mail 1', $sheet->getCell('H2')->getValue());

        self::assertSame('01/02/2000', $sheet->getCell('A3')->getValue());
        self::assertSame('05:00', $sheet->getCell('B3')->getValue());
        self::assertSame('10:00', $sheet->getCell('C3')->getValue());
        self::assertSame('Tache', $sheet->getCell('D3')->getValue());
        self::assertSame('nom 2', $sheet->getCell('E3')->getValue());
        self::assertSame('prenom 2', $sheet->getCell('F3')->getValue());
        self::assertSame('tel 2', $sheet->getCell('G3')->getValue());
        self::assertSame('mail 2', $sheet->getCell('H3')->getValue());
    }

    private function buildDistribution(Carbon $date, string $tache, Time $heureDebut, Time $heureFin)
    {
        return new AmapDistribution(
            AmapDistributionDetail::create($heureDebut, $heureFin, 1, $tache),
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            $date
        );
    }

    private function buildAmapien(string $nom, string $prenom, string $tel, string $mail)
    {
        $amapien = new Amapien();
        $amapien->setNom($nom);
        $amapien->setPrenom($prenom);
        $amapien->setNumTel1($tel);
        $amapien->setEmail($mail);

        return $amapien;
    }

    private function headerTest(Worksheet $sheet)
    {
        self::assertSame('Date', $sheet->getCell('A1')->getValue());
        self::assertSame('Heure de début', $sheet->getCell('B1')->getValue());
        self::assertSame('Heure de fin', $sheet->getCell('C1')->getValue());
        self::assertSame('Tâche', $sheet->getCell('D1')->getValue());
        self::assertSame('Nom de l\'amapien', $sheet->getCell('E1')->getValue());
        self::assertSame('Prénom de l\'amapien', $sheet->getCell('F1')->getValue());
        self::assertSame('Téléphone de l\'amapien', $sheet->getCell('G1')->getValue());
        self::assertSame('Email de l\'amapien', $sheet->getCell('H1')->getValue());
    }
}
