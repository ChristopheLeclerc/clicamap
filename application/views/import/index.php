<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
$currentUser = get_current_connected_user();
$isAmap = $currentUser instanceof \PsrLib\ORM\Entity\Amap;
?>

<script type="text/javascript">

  /*ALERTES*/
  $(document).ready(function () {

    /*window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(400, function(){
            $(this).remove();
        });
    }, 2000);*/

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 2500);

  });
  /*PARCOURIR*/
  $(function () {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function () {
      var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready(function () {
      $(':file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
          input.val(log);
        } else {
          if (log) alert(log);
        }

      });
    });

  });

</script>

<?php
// ALERTES
if ($amapiens_succes || $amap_succes || $paysans_succes) {
    if ($amap_succes) {
        $alerte = ' Les AMAP ont été ajoutées avec succès !';
    }
    if ($amapiens_succes) {
        $alerte = ' Les amapiens ont été ajoutés avec succès !';
    }
    if ($paysans_succes) {
        $alerte = ' Les paysans ont été ajoutés avec succès !';
    } ?>

    <div class="modal fade" id="alerte">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">
                    <p><i class="glyphicon glyphicon-ok" style="color:#449D44;"></i><?= $alerte; ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php
}

if (!$isAmap) {
    echo '<div class="btn btn-info pull-right" data-toggle="collapse" data-target="#info" style="margin-top:5px;"><i class="glyphicon glyphicon-info-sign"></i> Bulle info</div>';
}

echo '<h3>Outil d\'import de données en masse</h3>';

// MESSAGES --------------------------------------------------------------------
if (!$isAmap) {
    echo '<div class="alert alert-info collapse" id="info">Cet outil vous permet d\'importer en masse les AMAP, les amapiens et les paysans. Pour importer des éléments en masse, vous devez d\'abord remplir un fichier Excel au format <strong>XLS</strong>. Il est très important de respecter la structure du modèle mis à disposition en téléchargement dans chaque rubrique (nombre et nom des colonnes). Une fois que votre fichier Excel est prêt, vous pouvez le charger dans l\'application. Pour cela, cliquez sur le bouton "Parcourir", sélectionnez votre fichier, cliquez sur OK.
  </div>';
}

// FORMULAIRES -----------------------------------------------------------------
// AMAP ------------------------------------------------------------------------
if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_IMPORT_AMAP_PAYSAN)) {
    ?>

    <div class="well">

        <h4>Importation des AMAP</h4>

        <p>Pour avoir un exemple du fichier à remplir, merci de cliquer sur le lien :</p>
        <p><a href="<?= base_url('assets/import/amap.xls'); ?>">Télécharger un exemple de fichier pour charger
                les AMAP</a></p>

        <?php
        if ($amap_erreur) {
            echo '<div class="alert alert-danger">'.$amap_erreur.'</div>';
        }

    echo form_open_multipart('import/amap'); ?>

        <div class="input-group" style="margin:5px 0;">
            <label class="input-group-btn">
      <span class="btn btn-primary">
      Parcourir&hellip; <input type="file" name="amap" style="display: none;" multiple>
      </span>
            </label>
            <input type="text" class="form-control" readonly>
        </div>

        <div class="form-group" style="margin-bottom:40px;">
            <input type="submit" value="Importer" class="btn btn-default pull-right"/>
        </div>

        </form>

    </div>

    <?php
}

// AMAPIENS
if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_IMPORT)) {
    ?>

    <div class="well">

        <h4>Importation des amapiens</h4>

        <div class="alert alert-info">Pour importer des éléments, vous devez d'abord remplir un fichier Excel au format XLS. Aidez-vous de l'exemple mis à disposition en téléchargement.</div>


        <p>Pour avoir un exemple du fichier à remplir, merci de cliquer sur ce lien :</p>
        <p><a href="<?= base_url('assets/import/amapiens.xls'); ?>">Télécharger un exemple de fichier pour les AMAPiens</a></p>

        <?php
        if ($amapiens_erreur) {
            echo '<div class="alert alert-danger">'.$amapiens_erreur.'</div>';
        }

    echo form_open_multipart('import/amapiens'); ?>

        <div class="row">
            <div class="form-group">

                <?php
                if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien) {
                    echo '<label for="amap_id" class="col-md-12 control-label">Dans quelle Amap voulez-vous importez les amapiens ? </label>';
                } ?>

                <div class="col-md-12">
                    <?php if ($currentUser instanceof \PsrLib\ORM\Entity\Amap): ?>
                        <select class="form-control" readonly>*
                            <option><?=$currentUser; ?></option>
                        </select>
                    <?php else: ?>
                        <?= render_form_select_widget($form['amap']); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="input-group" style="margin:5px 0;">
            <label class="input-group-btn">
    <span class="btn btn-primary">
    Parcourir&hellip; <input type="file" name="amapiens" style="display: none;" multiple>
    </span>
            </label>
            <input type="text" class="form-control" readonly>

        </div>

        <div class="form-group" style="margin-bottom:40px;">
            <input type="submit" value="Importer" class="btn btn-default pull-right"/>
        </div>

        </form>

    </div>

    <?php
}
?>

<?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_IMPORT_AMAP_AMAPIEN_ADHESION)): ?>
    <div class="well">

        <h4>Reçus d'adhésion</h4>

        <div class="alert alert-info">
            Pour importer des éléments, vous devez d'abord remplir un fichier Excel au format XLS. Aidez-vous de l'exemple mis à disposition en téléchargement.
            Pour lancer la création des reçus, rendez-vous dans le menu "gestion des adhésions".
        </div>

        <p><a href="<?= base_url('assets/import/Import_adhesion_amap_amapien.xls'); ?>">Télécharger un exemple de fichier pour les reçus</a></p>

        <?php if (isset($amap_amapien_adhesions_errors) && is_array($amap_amapien_adhesions_errors)): ?>
            <?php foreach ($amap_amapien_adhesions_errors as $error): ?>
                <div class="alert alert-danger"><?=$error; ?></div>
            <?php endforeach; ?>
        <?php endif; ?>

        <?= form_open_multipart('import/amap_amapiens_adhesions'); ?>

        <div class="input-group" style="margin:5px 0;">
            <label class="input-group-btn">
    <span class="btn btn-primary">
    Parcourir&hellip; <input type="file" name="amap_amapien_adhesions" style="display: none;" multiple>
    </span>
            </label>
            <input type="text" class="form-control" readonly>

        </div>

        <div class="form-group" style="margin-bottom:40px;">
            <input type="submit" value="Importer" name="action_import_amap_amapien" class="btn btn-default pull-right"/>
        </div>

        <?= form_close(); ?>

    </div>
<?php endif; ?>


<?php
// PAYSANS
if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_IMPORT_AMAP_PAYSAN)) {
    ?>

    <div class="well">

        <h4>Importation des paysans et des fermes</h4>

        <p>Pour avoir un exemple du fichier à remplir, merci de cliquer sur ce lien :</p>
        <p><a href="<?= base_url('assets/import/paysans.xls'); ?>">Télécharger un exemple de fichier pour charger
                les paysans</a></p>

        <?php
        if ($paysans_erreur) {
            echo '<div class="alert alert-danger">'.$paysans_erreur.'</div>';
        }

    echo form_open_multipart('import/paysans'); ?>

        <div class="input-group" style="margin:5px 0;">
            <label class="input-group-btn">
    <span class="btn btn-primary">
    Parcourir&hellip; <input type="file" name="paysans" style="display: none;" multiple>
    </span>
            </label>
            <input type="text" class="form-control" readonly>

        </div>

        <div class="form-group" style="margin-bottom:40px;">
            <input type="submit" value="Importer" class="btn btn-default pull-right"/>
        </div>

        </form>

    </div>

    <?php
}

?>
