# language: fr
Fonctionnalité: Comme amapien référent, je doit pouvoir ajouter un contrat signé à un amapien

  @database
  Scénario: Ajouter un contrat signé à un amapien sans controle
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapienref@test.amap-aura.org"
    Et que je vais sur "/contrat_signe"
    Et que je remplis "search_contrat_signe_ref_produit[ferme]" avec "1"
    Et que je remplis "search_contrat_signe_ref_produit[mc]" avec "1"
    Et que je suis "Ajouter le contrat"
    Et que je remplis "amapien" avec "2"
    Et que je presse "Étape suivante"
    Et que je remplis "commande[1][1]" avec "1"
    Et que je remplis "commande[2][2]" avec "1"
    Et je devrais voir "Attention, avec ce module de gestion des contrats signés, les contrôles cités dans le bandeau bleu ne s'appliquent pas lors de la souscription d'un contrat."
    Et que je presse "Suivant"
    Et que je remplis "nb_reglements" avec "1"
    Et que je presse "Suivant"
    Et que je remplis "confirm-sign" avec "1"
    Et que je remplis "confirm-charter" avec "1"
    Et que je presse "Valider"
    Alors je devrais voir "NOM AMAPIEN"
