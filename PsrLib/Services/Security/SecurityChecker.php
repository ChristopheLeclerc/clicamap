<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Security;

use DI\Annotation\Injectable;
use LogicException;
use PsrLib\Services\Security\Checkers\AdhesionAmapAmapienCheck;
use PsrLib\Services\Security\Checkers\AdhesionAmapCheck;
use PsrLib\Services\Security\Checkers\AdhesionAmapienCheck;
use PsrLib\Services\Security\Checkers\AdhesionCheck;
use PsrLib\Services\Security\Checkers\AdhesionFermeCheck;
use PsrLib\Services\Security\Checkers\AjaxCheck;
use PsrLib\Services\Security\Checkers\AmapCheck;
use PsrLib\Services\Security\Checkers\AmapienCheck;
use PsrLib\Services\Security\Checkers\ContactCheck;
use PsrLib\Services\Security\Checkers\ContractModelCheck;
use PsrLib\Services\Security\Checkers\ContractSignedCheck;
use PsrLib\Services\Security\Checkers\DeliveryPlaceCheck;
use PsrLib\Services\Security\Checkers\DistributionCheck;
use PsrLib\Services\Security\Checkers\DocumentCheck;
use PsrLib\Services\Security\Checkers\EvenementCheck;
use PsrLib\Services\Security\Checkers\FermeCheck;
use PsrLib\Services\Security\Checkers\FermeRegroupementCheck;
use PsrLib\Services\Security\Checkers\ImportCheck;
use PsrLib\Services\Security\Checkers\MailShotCheck;
use PsrLib\Services\Security\Checkers\NetworkCheck;
use PsrLib\Services\Security\Checkers\PaysanCheck;
use PsrLib\Services\Security\Checkers\UserCheck;
use PsrLib\Services\Security\Checkers\UserTypeCheck;

/**
 * @Injectable(lazy=true)
 * Lazy loading to avoid circular references errors
 */
class SecurityChecker implements SecurityCheckerInterface
{
    public const ACTION_USER_CHANGE_PASSWORD = 'ACTION_USER_CHANGE_PASSWORD';
    public const ACTION_USER_ESTIMATE_PASSWORD_STRENGTH = 'ACTION_USER_ESTIMATE_PASSWORD_STRENGTH';
    public const ACTION_USER_IS_AUTHENTICATED = 'ACTION_USER_IS_AUTHENTICATED';

    public const ACTION_AMAPIEN_DISPLAY_PROFIL = 'ACTION_AMAPIEN_DISPLAY_PROFIL';
    public const ACTION_AMAPIEN_LIST = 'ACTION_AMAPIEN_LIST';
    public const ACTION_AMAPIEN_EDIT_EXTERNAL = 'ACTION_AMAPIEN_EDIT_EXTERNAL';
    public const ACTION_AMAPIEN_EDIT_OWN_PROFILE = 'ACTION_AMAPIEN_EDIT_OWN_PROFILE';
    public const ACTION_AMAPIEN_CHANGE_NAME = 'ACTION_AMAPIEN_CHANGE_NAME';
    public const ACTION_AMAPIEN_CREATE_NO_AMAP = 'ACTION_AMAPIEN_CREATE_NO_AMAP';

    public const ACTION_PAYSAN_DISPLAY_PROFIL = 'ACTION_PAYSAN_DISPLAY_PROFIL';
    public const ACTION_PAYSAN_CREATE = 'ACTION_PAYSAN_CREATE';
    public const ACTION_PAYSAN_DOWNLOAD_LIST = 'ACTION_PAYSAN_DOWNLOAD_LIST';
    public const ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT = 'ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT';
    public const ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL = 'ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL';
    public const ACTION_PAYSAN_SEARCH = 'ACTION_PAYSAN_SEARCH';
    public const ACTION_PAYSAN_EDIT_COMMENT = 'ACTION_PAYSAN_EDIT_COMMENT';
    public const ACTION_PAYSAN_CHANGE_NAME = 'ACTION_PAYSAN_CHANGE_NAME';
    public const ACTION_PAYSAN_CHANGE_ADHESION = 'ACTION_PAYSAN_CHANGE_ADHESION';
    public const ACTION_PAYSAN_CHANGE_CR = 'ACTION_PAYSAN_CHANGE_CR';

    public const ACTION_EVENEMENT_CREATE = 'ACTION_EVENEMENT_CREATE';
    public const ACTION_EVENEMENT_LIST = 'ACTION_EVENEMENT_LIST';
    public const ACTION_EVENEMENT_EDIT = 'ACTION_EVENEMENT_EDIT';
    public const ACTION_EVENEMENT_DISPLAY = 'ACTION_EVENEMENT_DISPLAY';

    public const ACTION_AMAP_LIST = 'ACTION_AMAP_LIST';
    public const ACTION_AMAP_DISPLAY = 'ACTION_AMAP_DISPLAY';
    public const ACTION_AMAP_CREATE = 'ACTION_AMAP_CREATE';
    public const ACTION_AMAP_CHANGE_STATE = 'ACTION_AMAP_CHANGE_STATE';
    public const ACTION_AMAP_SEND_PASSWORD = 'ACTION_AMAP_SEND_PASSWORD';
    public const ACTION_AMAP_COMMENT = 'ACTION_AMAP_COMMENT';
    public const ACTION_AMAP_SEARCH_PAYSAN = 'ACTION_AMAP_SEARCH_PAYSAN';
    public const ACTION_AMAP_SUBSCRIBE = 'ACTION_AMAP_SUBSCRIBE';
    public const ACTION_AMAP_CHANGE_PASSWORD = 'ACTION_AMAP_CHANGE_PASSWORD';
    public const ACTION_AMAP_SHOW_MAP = 'ACTION_AMAP_SHOW_MAP';
    public const ACTION_AMAP_MANAGE_ABS = 'ACTION_AMAP_MANAGE_ABS';
    public const ACTION_AMAP_CHANGE_ADHESION = 'ACTION_AMAP_CHANGE_ADHESION';

    public const ACTION_DELIVERY_PLACE_DELETE = 'ACTION_DELIVERY_PLACE_DELETE';

    public const ACTION_CONTACT_ADMIN = 'ACTION_CONTACT_ADMIN';
    public const ACTION_CONTACT_AMAP = 'ACTION_CONTACT_AMAP';
    public const ACTION_CONTACT_OWN_AMAP = 'ACTION_CONTACT_OWN_AMAP';

    public const ACTION_FERME_LIST = 'ACTION_FERME_LIST';
    public const ACTION_FERME_CREATE = 'ACTION_FERME_CREATE';
    public const ACTION_FERME_DOWNLOAD = 'ACTION_FERME_DOWNLOAD';
    public const ACTION_FERME_REF_PRODUITS = 'ACTION_FERME_REF_PRODUITS';
    public const ACTION_FERME_DISPLAY = 'ACTION_FERME_DISPLAY';
    public const ACTION_FERME_DISPLAY_OWN = 'ACTION_FERME_DISPLAY_OWN';
    public const ACTION_FERME_REMOVE = 'ACTION_FERME_REMOVE';
    public const ACTION_FERME_PRODUCT_DELETE = 'ACTION_FERME_PRODUCT_DELETE';
    public const ACTION_FERME_TYPE_PRODUCTION_MANAGE = 'ACTION_FERME_TYPE_PRODUCTION_MANAGE';
    public const ACTION_FERME_CHANGE_ADHESION = 'ACTION_FERME_CHANGE_ADHESION';

    public const ACTION_MAILSHOT = 'ACTION_MAILSHOT';

    public const ACTION_NETWORK_MANAGE = 'ACTION_NETWORK_MANAGE';

    public const ACTION_CONTRACT_SIGNED_LIST = 'ACTION_CONTRACT_SIGNED_LIST';
    public const ACTION_CONTRACT_SIGNED_DISPLAY = 'ACTION_CONTRACT_SIGNED_DISPLAY';
    public const ACTION_CONTRACT_SIGNED_EDIT = 'ACTION_CONTRACT_SIGNED_EDIT';
    public const ACTION_CONTRACT_SIGNED_EDIT_FORCE = 'ACTION_CONTRACT_SIGNED_EDIT_FORCE';
    public const ACTION_CONTRACT_SIGNED_DISPLAY_OWN = 'ACTION_CONTRACT_SIGNED_DISPLAY_OWN';
    public const ACTION_CONTRACT_SIGNED_EXPORT_PDF = 'ACTION_CONTRACT_SIGNED_EXPORT_PDF';
    public const ACTION_CONTRACT_SIGNED_MOVE_DATE = 'ACTION_CONTRACT_SIGNED_MOVE_DATE';
    public const ACTION_CONTRACT_SIGNED_LIST_ARCHIVED = 'ACTION_CONTRACT_SIGNED_LIST_ARCHIVED';

    public const ACTION_CONTRACT_MODEL_LIST = 'ACTION_CONTRACT_MODEL_LIST';
    public const ACTION_CONTRACT_MODEL_LIST_VALIDATE = 'ACTION_CONTRACT_MODEL_LIST_VALIDATE';
    public const ACTION_CONTRACT_MODEL_DISPLAY = 'ACTION_CONTRACT_MODEL_DISPLAY';
    public const ACTION_CONTRACT_MODEL_SUBSCRIBE = 'ACTION_CONTRACT_MODEL_SUBSCRIBE';
    public const ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE = 'ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE';
    public const ACTION_CONTRACT_MODEL_EDIT = 'ACTION_CONTRACT_MODEL_EDIT';
    public const ACTION_CONTRACT_MODEL_VALIDATE = 'ACTION_CONTRACT_MODEL_VALIDATE';
    public const ACTION_CONTRACT_MODEL_PREVIEW_PDF = 'ACTION_CONTRACT_MODEL_PREVIEW_PDF';
    public const ACTION_CONTRACT_MODEL_REMOVE = 'ACTION_CONTRACT_MODEL_REMOVE';
    public const ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY = 'ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY';
    public const ACTION_CONTRACT_MODEL_MOVE_DATE = 'ACTION_CONTRACT_MODEL_MOVE_DATE';

    public const ACTION_ADHESION_IMPORT = 'ACTION_ADHESION_IMPORT';
    public const ACTION_ADHESION_AMAP_LIST = 'ACTION_ADHESION_AMAP_LIST';
    public const ACTION_ADHESION_AMAP_LIST_SELF = 'ACTION_ADHESION_AMAP_LIST_SELF';
    public const ACTION_ADHESION_AMAP_DOWNLOAD = 'ACTION_ADHESION_AMAP_DOWNLOAD';
    public const ACTION_ADHESION_AMAP_DELETE = 'ACTION_ADHESION_AMAP_DELETE';
    public const ACTION_ADHESION_AMAP_GENERATE = 'ACTION_ADHESION_AMAP_GENERATE';

    public const ACTION_ADHESION_FERME_LIST = 'ACTION_ADHESION_FERME_LIST';
    public const ACTION_ADHESION_FERME_LIST_SELF = 'ACTION_ADHESION_FERME_LIST_SELF';
    public const ACTION_ADHESION_FERME_DOWNLOAD = 'ACTION_ADHESION_FERME_DOWNLOAD';
    public const ACTION_ADHESION_FERME_DELETE = 'ACTION_ADHESION_FERME_DELETE';
    public const ACTION_ADHESION_FERME_GENERATE = 'ACTION_ADHESION_FERME_GENERATE';

    public const ACTION_ADHESION_AMAPIEN_LIST = 'ACTION_ADHESION_AMAPIEN_LIST';
    public const ACTION_ADHESION_AMAPIEN_LIST_SELF = 'ACTION_ADHESION_AMAPIEN_LIST_SELF';
    public const ACTION_ADHESION_AMAPIEN_DOWNLOAD = 'ACTION_ADHESION_AMAPIEN_DOWNLOAD';
    public const ACTION_ADHESION_AMAPIEN_DELETE = 'ACTION_ADHESION_AMAPIEN_DELETE';
    public const ACTION_ADHESION_AMAPIEN_GENERATE = 'ACTION_ADHESION_AMAPIEN_GENERATE';

    public const ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD = 'ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD';
    public const ACTION_ADHESION_AMAP_AMAPIEN_DELETE = 'ACTION_ADHESION_AMAP_AMAPIEN_DELETE';
    public const ACTION_ADHESION_AMAP_AMAPIEN_GENERATE = 'ACTION_ADHESION_AMAP_AMAPIEN_GENERATE';

    public const ACTION_DOCUMENT_MANAGE = 'ACTION_DOCUMENT_MANAGE';
    public const ACTION_DOCUMENT_GETOWN = 'ACTION_DOCUMENT_GETOWN';

    public const ACTION_IMPORT = 'ACTION_IMPORT';
    public const ACTION_IMPORT_AMAP_PAYSAN = 'ACTION_IMPORT_AMAP_PAYSAN';
    public const ACTION_IMPORT_AMAP_AMAPIEN_ADHESION = 'ACTION_IMPORT_AMAP_AMAPIEN_ADHESION';

    public const ACTION_DISTRIBUTION_LIST = 'ACTION_DISTRIBUTION_LIST';
    public const ACTION_DISTRIBUTION_DELETE = 'ACTION_DISTRIBUTION_DELETE';
    public const ACTION_DISTRIBUTION_EDIT = 'ACTION_DISTRIBUTION_EDIT';
    public const ACTION_DISTRIBUTION_DETAIL = 'ACTION_DISTRIBUTION_DETAIL';
    public const ACTION_DISTRIBUTION_AMAPIEN_REGISTER = 'ACTION_DISTRIBUTION_AMAPIEN_REGISTER';
    public const ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER = 'ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER';
    public const ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN = 'ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN';

    public const ACTION_FERME_REGROUPEMENT_LIST = 'ACTION_FERME_REGROUPEMENT_LIST';
    public const ACTION_FERME_REGROUPEMENT_PROFIL = 'ACTION_FERME_REGROUPEMENT_PROFIL';

    public const ACTION_AJAX = 'ACTION_AJAX';

    public const ACTION_MIGRATE = 'ACTION_MIGRATE';

    public const USER_AMAP = 'USER_AMAP';
    public const USER_AMAPIEN = 'USER_AMAPIEN';
    public const USER_PAYSAN = 'USER_PAYSAN';

    /**
     * @var UserCheck
     */
    private $usercheck;

    /**
     * @var AmapienCheck
     */
    private $amapiencheck;

    /**
     * @var PaysanCheck
     */
    private $paysancheck;

    /**
     * @var AdhesionFermeCheck
     */
    private $adhesionfermecheck;

    /**
     * @var EvenementCheck
     */
    private $evenementcheck;

    /**
     * @var AmapCheck
     */
    private $amapcheck;

    /**
     * @var DeliveryPlaceCheck
     */
    private $deliveryplacecheck;

    /**
     * @var ContactCheck
     */
    private $contactcheck;

    /**
     * @var FermeCheck
     */
    private $fermecheck;

    /**
     * @var ContractSignedCheck
     */
    private $contractsignedcheck;

    /**
     * @var MailShotCheck
     */
    private $mailshotcheck;

    /**
     * @var NetworkCheck
     */
    private $networkcheck;

    /**
     * @var ContractModelCheck
     */
    private $contractmodelcheck;

    /**
     * @var ImportCheck
     */
    private $importcheck;

    /**
     * @var AjaxCheck
     */
    private $ajaxcheck;

    /**
     * @var DocumentCheck
     */
    private $documentcheck;

    /**
     * @var AdhesionCheck
     */
    private $adhesioncheck;

    /**
     * @var AdhesionAmapAmapienCheck
     */
    private $adhesionamapamapiencheck;

    /**
     * @var AdhesionAmapCheck
     */
    private $adhesionamapcheck;

    /**
     * @var AdhesionAmapienCheck
     */
    private $adhesionamapiencheck;

    /**
     * @var DistributionCheck
     */
    private $distributionCheck;

    /**
     * @var UserTypeCheck
     */
    private $userTypeCheck;

    /**
     * @var FermeRegroupementCheck
     */
    private $fermeRegroupementCheck;

    public function __construct(UserCheck $usercheck, AmapienCheck $amapiencheck, PaysanCheck $paysancheck, AdhesionFermeCheck $adhesionfermecheck, EvenementCheck $evenementcheck, AmapCheck $amapcheck, DeliveryPlaceCheck $deliveryplacecheck, ContactCheck $contactcheck, FermeCheck $fermecheck, ContractSignedCheck $contractsignedcheck, MailShotCheck $mailshotcheck, NetworkCheck $networkcheck, ContractModelCheck $contractmodelcheck, ImportCheck $importcheck, AjaxCheck $ajaxcheck, DocumentCheck $documentcheck, AdhesionCheck $adhesioncheck, AdhesionAmapAmapienCheck $adhesionamapamapiencheck, AdhesionAmapCheck $adhesionamapcheck, AdhesionAmapienCheck $adhesionamapiencheck, DistributionCheck $distributionCheck, FermeRegroupementCheck $fermeRegroupementCheck, UserTypeCheck $userTypeCheck)
    {
        $this->usercheck = $usercheck;
        $this->amapiencheck = $amapiencheck;
        $this->paysancheck = $paysancheck;
        $this->adhesionfermecheck = $adhesionfermecheck;
        $this->evenementcheck = $evenementcheck;
        $this->amapcheck = $amapcheck;
        $this->deliveryplacecheck = $deliveryplacecheck;
        $this->contactcheck = $contactcheck;
        $this->fermecheck = $fermecheck;
        $this->contractsignedcheck = $contractsignedcheck;
        $this->mailshotcheck = $mailshotcheck;
        $this->networkcheck = $networkcheck;
        $this->contractmodelcheck = $contractmodelcheck;
        $this->importcheck = $importcheck;
        $this->ajaxcheck = $ajaxcheck;
        $this->documentcheck = $documentcheck;
        $this->adhesioncheck = $adhesioncheck;
        $this->adhesionamapamapiencheck = $adhesionamapamapiencheck;
        $this->adhesionamapcheck = $adhesionamapcheck;
        $this->adhesionamapiencheck = $adhesionamapiencheck;
        $this->distributionCheck = $distributionCheck;
        $this->userTypeCheck = $userTypeCheck;
        $this->fermeRegroupementCheck = $fermeRegroupementCheck;
    }

    /**
     * @param mixed $subject
     */
    public function isGranted(string $action, $subject = null): bool
    {
        //TODO: catch in try ?
        switch ($action) {
            case self::ACTION_USER_ESTIMATE_PASSWORD_STRENGTH:
                return $this->usercheck->estimatePassworsStrength($subject);

            case self::ACTION_USER_CHANGE_PASSWORD:
                return $this->usercheck->isGrantedUserChangePassword($subject);

            case self::ACTION_USER_IS_AUTHENTICATED:
                return $this->usercheck->isUserAuthenticated();

            case self::ACTION_AMAPIEN_DISPLAY_PROFIL:
                return $this->amapiencheck->isGrantedAmapienDisplayProfil($subject);

            case self::ACTION_AMAPIEN_CREATE_NO_AMAP:
                return $this->amapiencheck->isGrantedAmapienCreateNoAmap();

            case self::ACTION_AMAPIEN_CHANGE_NAME:
            case self::ACTION_AMAPIEN_EDIT_EXTERNAL:
                return $this->amapiencheck->isGrantedAmapienEditExternal($subject);

            case self::ACTION_AMAPIEN_EDIT_OWN_PROFILE:
                return $this->amapiencheck->isGrantedAmapienEditOwnProfile($subject);

            case self::ACTION_AMAPIEN_LIST:
                return $this->amapiencheck->isGrantedAmapienList();

            case self::ACTION_PAYSAN_DISPLAY_PROFIL:
                return $this->paysancheck->isGrantedPaysanDisplayProfil($subject);

            case self::ACTION_PAYSAN_CHANGE_NAME:
            case self::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL:
                return $this->paysancheck->editProfilExternal($subject);

            case self::ACTION_PAYSAN_CREATE:
                return $this->paysancheck->isGrantedPaysanCreate();

            case self::ACTION_PAYSAN_DOWNLOAD_LIST:
                return $this->paysancheck->isGrantedPaysanDownloadList();

            case self::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT:
                return $this->paysancheck->isGrantedPaysanDownloadListRegroupement();

            case self::ACTION_PAYSAN_EDIT_COMMENT:
                return $this->paysancheck->isGrantedPaysanEditComment($subject);

            case self::ACTION_PAYSAN_CHANGE_ADHESION:
                return $this->paysancheck->isGrantedPaysanChangeAdhesion($subject);

            case self::ACTION_PAYSAN_CHANGE_CR:
                return $this->paysancheck->isGrantedPaysanChangeCR($subject);

            case self::ACTION_EVENEMENT_CREATE:
                return $this->evenementcheck->isGrantedEvenementCreate();

            case self::ACTION_EVENEMENT_EDIT:
                return $this->evenementcheck->isGrantedEvenementEdit($subject);

            case self::ACTION_EVENEMENT_LIST:
                return $this->evenementcheck->isGrantedEvenementList();

            case self::ACTION_EVENEMENT_DISPLAY:
                return $this->evenementcheck->isGrantedEvenementDisplay($subject);

            case self::ACTION_AMAP_LIST:
                return $this->amapcheck->isGrantedAmapList();

            case self::ACTION_AMAP_DISPLAY:
                return $this->amapcheck->isGrantedAmapDisplay($subject);

            case self::ACTION_AMAP_CREATE:
                return $this->amapcheck->isGrantedAmapCreate();

            case self::ACTION_AMAP_CHANGE_STATE:
            case self::ACTION_AMAP_COMMENT:
            case self::ACTION_AMAP_CHANGE_ADHESION:
                return $this->amapcheck->isGrantedMapChangeState($subject);

            case self::ACTION_AMAP_SEND_PASSWORD:
                return $this->amapcheck->isGrantedAmapSendPassword($subject);

            case self::ACTION_AMAP_SEARCH_PAYSAN:
                return $this->amapcheck->isGrantedAmapSearchPaysan();

            case self::ACTION_AMAP_SUBSCRIBE:
                return $this->amapcheck->isGrantedAmapSubscribe($subject);

            case self::ACTION_AMAP_CHANGE_PASSWORD:
                return $this->amapcheck->isGrantedAmapChangePassword($subject);

            case self::ACTION_AMAP_SHOW_MAP:
                return $this->amapcheck->isGrantedAmapShowMap($subject);

            case self::ACTION_AMAP_MANAGE_ABS:
                return $this->amapcheck->isGrantedAmapManageAbs($subject);

            case self::ACTION_DELIVERY_PLACE_DELETE:
                return $this->deliveryplacecheck->isGrantedDeliveryPlaceDelete($subject);

            case self::ACTION_CONTACT_ADMIN:
                return $this->contactcheck->isGrantedContactAdmin();

            case self::ACTION_CONTACT_AMAP:
                return $this->contactcheck->isGrantedContactAmap();

            case self::ACTION_CONTACT_OWN_AMAP:
                return $this->contactcheck->isGrantedContactOwnAmap();

            case self::ACTION_FERME_LIST:
                return $this->fermecheck->isGrantedFermeList();

            case self::ACTION_FERME_CREATE:
                return $this->fermecheck->isGrantedFermeCreate();

            case self::ACTION_FERME_DOWNLOAD:
                return $this->fermecheck->isGrantedFermeDownload();

            case self::ACTION_FERME_DISPLAY:
                return $this->fermecheck->isGrantedFermeDisplay($subject);

            case self::ACTION_FERME_REF_PRODUITS:
                return $this->fermecheck->isGrantedRefProduit($subject);

            case self::ACTION_FERME_DISPLAY_OWN:
                return $this->fermecheck->isGrantedFermeDisplayOwn();

            case self::ACTION_FERME_REMOVE:
                return $this->fermecheck->isGrantedFermeRemove();

            case self::ACTION_FERME_PRODUCT_DELETE:
                return $this->fermecheck->isGrantedFermeProductDelete($subject);

            case self::ACTION_FERME_TYPE_PRODUCTION_MANAGE:
                return $this->fermecheck->isGrantedTypeProductionManage($subject);

            case self::ACTION_FERME_CHANGE_ADHESION:
                return $this->fermecheck->isGrantedPaysanChangeAdhesion();

            case self::ACTION_MAILSHOT:
                return $this->mailshotcheck->isGrantedMailShot();

            case self::ACTION_NETWORK_MANAGE:
                return $this->networkcheck->isGrantedManageNetwork();

            case self::ACTION_CONTRACT_SIGNED_LIST:
                return $this->contractsignedcheck->isGrantedContractSignedList();

            case self::ACTION_CONTRACT_SIGNED_DISPLAY:
                return $this->contractsignedcheck->isGrantedContractSignedDisplay($subject);

            case self::ACTION_CONTRACT_SIGNED_EDIT:
                return $this->contractsignedcheck->isGrantedContractSignedEdit($subject);

            case self::ACTION_CONTRACT_SIGNED_EDIT_FORCE:
                return $this->contractsignedcheck->isGrantedContractSignedEditForce($subject);

            case self::ACTION_CONTRACT_SIGNED_DISPLAY_OWN:
                return $this->contractsignedcheck->isGrantedContractSignedDisplayOwn();

            case self::ACTION_CONTRACT_SIGNED_EXPORT_PDF:
                return $this->contractsignedcheck->isGrantedContractSignedExportPdf($subject);

            case self::ACTION_CONTRACT_SIGNED_MOVE_DATE:
                return $this->contractsignedcheck->isGrantedContractSignedMoveDate($subject);

            case self::ACTION_CONTRACT_SIGNED_LIST_ARCHIVED:
                return $this->contractsignedcheck->isGrantedContractSignedListArchived();

            case self::ACTION_CONTRACT_MODEL_LIST:
                return $this->contractmodelcheck->isGrantedContractModelList();

            case self::ACTION_CONTRACT_MODEL_LIST_VALIDATE:
                return $this->contractmodelcheck->isGrantedContractModelListValidate();

            case self::ACTION_CONTRACT_MODEL_DISPLAY:
                return $this->contractmodelcheck->isGrantedContractModelDisplay($subject);

            case self::ACTION_CONTRACT_MODEL_EDIT:
                return $this->contractmodelcheck->isGrantedContractModelEdit($subject);

            case self::ACTION_CONTRACT_MODEL_SUBSCRIBE:
                return $this->contractmodelcheck->isGrantedContractModelSubscribe($subject);

            case self::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE:
                return $this->contractmodelcheck->isGrantedContractModelSubscribeForce($subject);

            case self::ACTION_CONTRACT_MODEL_VALIDATE:
                return $this->contractmodelcheck->isGrantedContractModelValidate($subject);

            case self::ACTION_CONTRACT_MODEL_PREVIEW_PDF:
                return $this->contractmodelcheck->isGrantedContractModelPreviewPdf($subject);

            case self::ACTION_CONTRACT_MODEL_REMOVE:
                return $this->contractmodelcheck->isGrantedContractModelRemove($subject);

            case self::ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY:
                return $this->contractmodelcheck->isGrantedContractModelPaymentSummary($subject);

            case self::ACTION_CONTRACT_MODEL_MOVE_DATE:
                return $this->contractmodelcheck->isGrantedContractModelMoveDate($subject);

            case self::ACTION_IMPORT:
                return $this->importcheck->isGrantedImport();

            case self::ACTION_IMPORT_AMAP_PAYSAN:
                return $this->importcheck->isGrantedImportAmapPaysan();

            case self::ACTION_IMPORT_AMAP_AMAPIEN_ADHESION:
                return $this->importcheck->isGrantedImportAmapAmapienAdhesion();

            case self::ACTION_AJAX:
                return $this->ajaxcheck->isGrantedAjax();

            case self::ACTION_DOCUMENT_MANAGE:
                return $this->documentcheck->isGrantedManageDocument();

            case self::ACTION_DOCUMENT_GETOWN:
                return $this->documentcheck->isGrantedGetOwnDocument();

            case self::ACTION_ADHESION_AMAP_LIST:
            case self::ACTION_ADHESION_FERME_LIST:
            case self::ACTION_ADHESION_AMAPIEN_LIST:
                return $this->adhesioncheck->isGrantedList();

            case self::ACTION_ADHESION_AMAP_DELETE:
            case self::ACTION_ADHESION_AMAP_GENERATE:
            case self::ACTION_ADHESION_FERME_GENERATE:
            case self::ACTION_ADHESION_FERME_DELETE:
            case self::ACTION_ADHESION_AMAPIEN_GENERATE:
            case self::ACTION_ADHESION_AMAPIEN_DELETE:
                return $this->adhesioncheck->isGrantedDelete($subject);

            case self::ACTION_ADHESION_AMAP_AMAPIEN_DELETE:
            case self::ACTION_ADHESION_AMAP_AMAPIEN_GENERATE:
                return $this->adhesionamapamapiencheck->isGrantedDelete($subject);

            case self::ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD:
                return $this->adhesionamapamapiencheck->isGrantedDownload($subject);

            case self::ACTION_ADHESION_IMPORT:
                return $this->adhesioncheck->isGrantedImport();

            case self::ACTION_ADHESION_AMAP_DOWNLOAD:
                return $this->adhesionamapcheck->isGrantedDownload($subject);

            case self::ACTION_ADHESION_AMAP_LIST_SELF:
                return $this->adhesionamapcheck->isGrantedList();

            case self::ACTION_ADHESION_FERME_DOWNLOAD:
                return $this->adhesionfermecheck->isGrantedDownload($subject);

            case self::ACTION_ADHESION_FERME_LIST_SELF:
                return $this->adhesionfermecheck->isGrantedList();

            case self::ACTION_ADHESION_AMAPIEN_DOWNLOAD:
                return $this->adhesionamapiencheck->isGrantedDownload($subject);

            case self::ACTION_ADHESION_AMAPIEN_LIST_SELF:
                return $this->adhesionamapiencheck->isGrantedList();

            case self::ACTION_DISTRIBUTION_LIST:
                return $this->distributionCheck->isGrantedList();

            case self::ACTION_DISTRIBUTION_EDIT:
            case self::ACTION_DISTRIBUTION_DELETE:
                return $this->distributionCheck->isGrantedDelete($subject);

            case self::ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN:
                return $this->distributionCheck->isGrantedAmapienListOwn();

            case self::ACTION_DISTRIBUTION_AMAPIEN_REGISTER:
                return $this->distributionCheck->isGrantedAmapienRegister($subject);

            case self::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER:
                return $this->distributionCheck->isGrantedAmapienUnregister($subject);

            case self::ACTION_DISTRIBUTION_DETAIL:
                return $this->distributionCheck->isGrantedDetail($subject);

            case self::USER_AMAP:
                return $this->userTypeCheck->isGrantedUserIsAmap($subject);

            case self::USER_AMAPIEN:
                return $this->userTypeCheck->isGrantedUserIsAmapien($subject);

            case self::USER_PAYSAN:
                return $this->userTypeCheck->isGrantedUserIsPaysan($subject);

            case self::ACTION_FERME_REGROUPEMENT_LIST:
                return $this->fermeRegroupementCheck->isGrantedList();

            case self::ACTION_FERME_REGROUPEMENT_PROFIL:
                return $this->fermeRegroupementCheck->isGrantedProfil();
        }

        throw new LogicException('Unknow action');
    }
}
