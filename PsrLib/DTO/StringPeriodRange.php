<?php

namespace PsrLib\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class StringPeriodRange
{
    public const PERIOD_ALL = 'PERIOD_ALL';
    public const PERIOD_MONTH_CURRENT = 'PERIOD_MONTH_CURRENT';
    public const PERIOD_MONTH_NEXT = 'PERIOD_MONTH_NEXT';
    public const PERIOD_WEEK_CURRENT = 'PERIOD_WEEK_CURRENT';
    public const PERIOD_WEEK_NEXT = 'PERIOD_WEEK_NEXT';

    public const PERIODS = [
        'Toutes les dates' => self::PERIOD_ALL,
        'Ce mois' => self::PERIOD_MONTH_CURRENT,
        'Le mois suivant' => self::PERIOD_MONTH_NEXT,
        'Cette semaine' => self::PERIOD_WEEK_CURRENT,
        'La semaine suivante' => self::PERIOD_WEEK_NEXT,
    ];

    /**
     * @Assert\Choice(choices=StringPeriodRange::PERIODS)
     * @Assert\NotBlank()
     *
     * @var ?string
     */
    private $stringPeriod;

    public static function create(string $stringPeriod): self
    {
        $period = new self();
        $period->setStringPeriod($stringPeriod);

        return $period;
    }

    public function getStringPeriod(): ?string
    {
        return $this->stringPeriod;
    }

    public function setStringPeriod(?string $stringPeriod): StringPeriodRange
    {
        $this->stringPeriod = $stringPeriod;

        return $this;
    }
}
