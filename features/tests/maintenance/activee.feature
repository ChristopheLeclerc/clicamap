# language: fr
Fonctionnalité: En tant qu'utlisateur, je doit etre redirigé sur la page de maintenance si celle-ci est activée

  @environment
  Scénario: Accès à la page d'accueil
    Etant donné que je définit la variable environment "MAINTENANCE_ENABLED" avec la valeur "true"
    Lorsque je vais sur "/"
    Alors je devrais être sur "/maintenance"
