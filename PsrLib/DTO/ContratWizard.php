<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Contrat;
use Symfony\Component\Serializer\Annotation\Groups;

class ContratWizard
{
    /**
     * @var ?string
     * @Groups({"wizardContract"})
     */
    private $urlRetour;

    /**
     * @var ?Contrat
     * @Groups({"wizardContract"})
     */
    private $contrat;

    /**
     * @Groups({"wizardContract"})
     *
     * @var bool
     */
    private $bypassNbLibCheck = false;

    public function getUrlRetour(): ?string
    {
        return $this->urlRetour;
    }

    public function setUrlRetour(?string $urlRetour): ContratWizard
    {
        $this->urlRetour = $urlRetour;

        return $this;
    }

    public function getContrat(): ?Contrat
    {
        return $this->contrat;
    }

    public function setContrat(?Contrat $contrat): ContratWizard
    {
        $this->contrat = $contrat;

        return $this;
    }

    public function isBypassNbLibCheck(): bool
    {
        return $this->bypassNbLibCheck;
    }

    public function setBypassNbLibCheck(bool $bypassNbLibCheck): ContratWizard
    {
        $this->bypassNbLibCheck = $bypassNbLibCheck;

        return $this;
    }
}
