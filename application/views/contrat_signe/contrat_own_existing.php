<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
?>

<h3>Mes contrats existants</h3>

<table class="table table-datatable">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Date d'inscription</th>
        <th>Date de modification</th>
        <th>Paniers obligatoires</th>
        <th>Choix identiques</th>
        <th>Date de fin de souscription</th>
        <th>Outils</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($contrats as $contrat): ?>
        <?php $mc = $contrat->getModeleContrat(); ?>
        <tr>
            <td><?= $mc->getNom(); ?></td>
            <td><?= date_format_french_hour($contrat->getDateCreation()); ?></td>
            <td><?= date_format_french_hour($contrat->getDateModification()); ?></td>
            <td><?= $mc->getNblivPlancher(); ?></td>
            <td><?= print_bool($mc->getProduitsIdentiqueAmapien()); ?></td>
            <td><?= date_format_french($mc->getForclusion()); ?></td>
            <td class="text-right">
                <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_MOVE_DATE, $contrat)): ?>
                    <a title="Déplacer des livraisons"
                       href="<?= site_url('/contrat_signe/contrat_deplacement/'.$contrat->getId().'?ret='.urlencode(uri_string())); ?>">
                        <i class="glyphicon glyphicon-calendar" data-toggle="tooltip"
                           title="Déplacer des livraisons"></i>
                    </a>
                <?php endif; ?>

                <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EDIT, $contrat)): ?>
                    <a title="Modifier le contrat"
                       href="<?= site_url('/contrat_signe/contrat_edit/'.$contrat->getId()); ?>">
                        <i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier le contrat"></i>
                    </a>
                <?php endif; ?>

                <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EDIT, $contrat)): ?>
                    <a title="Supprimer le contrat" href="#" data-toggle="modal"
                       data-target="#modal_remove_<?= $contrat->getId(); ?>">
                        <i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le contrat"></i>
                    </a>

                    <!-- Modal remove -->
                    <div class="modal fade text-left" id="modal_remove_<?= $contrat->getId(); ?>" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer
                                        "<?= ucfirst($mc->getNom()); ?>"</h5>
                                </div>
                                <div class="modal-body">
                                    <p class="text-right">
                                        <a href="<?= site_url('contrat_signe/remove/'.$contrat->getId()); ?>"
                                           class="btn btn-danger btn-sm">OUI</a>
                                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (1 === $mc->getVersion()): ?>
                    <a title="Télécharger le contrat"
                       href="<?= site_url('/contrat_signe/telecharger_contrat/'.$contrat->getId()); ?>">
                        <i class="glyphicon glyphicon-list-alt" data-toggle="tooltip"
                           title="Télécharger le contrat"></i>
                    </a>
                <?php endif; ?>
                <?php if (2 === $mc->getVersion()): ?>
                    <a title="Télécharger le contrat"
                       href="<?= site_url('/contrat_signe/contrat_pdf/'.$contrat->getId()); ?>">
                        <i class="glyphicon glyphicon-file" data-toggle="tooltip" title="Télécharger le contrat"></i>
                    </a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

