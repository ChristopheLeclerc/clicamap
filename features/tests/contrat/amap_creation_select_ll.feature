# language: fr
Fonctionnalité: Comme amap, je doit pré selectionner automatiquement le lieu de livraison si il est unique dans l'AMAP

  @database
  Scénario: Un seul lieu de livraison par AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis "Gestion des contrats vierges"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Quand je suis "Créer un nouveau contrat"
    Alors le champ "model_contrat_form1[livraisonLieu]" devrait contenir "1"

  @database
  Scénario: Plusieurs lieux de livraison par AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que j'ajoute le lieu de livraison "ll amap test 2" a l'AMAP "amap@test.amap-aura.org"
    Et que je suis "Gestion des contrats vierges"
    Et que je remplis "search_contrat_vierge_amap[ferme]" avec "1"
    Quand je suis "Créer un nouveau contrat"
    Alors le champ "model_contrat_form1[livraisonLieu]" devrait contenir "Sélectionner"
