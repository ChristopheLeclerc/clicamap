<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210511144253 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE adhesion_amap_amapien (id INT AUTO_INCREMENT NOT NULL, amapien_id BIGINT NOT NULL, value_id INT NOT NULL, creator_id BIGINT NOT NULL, state VARCHAR(255) NOT NULL, generation_date DATETIME(6) DEFAULT NULL COMMENT \'(DC2Type:carbon)\', voucher_file_name VARCHAR(255) DEFAULT NULL, INDEX IDX_D86B8344BC7A2B78 (amapien_id), UNIQUE INDEX UNIQ_D86B8344F920BBA2 (value_id), INDEX IDX_D86B834461220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adhesion_amap_amapien ADD CONSTRAINT FK_D86B8344BC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE adhesion_amap_amapien ADD CONSTRAINT FK_D86B8344F920BBA2 FOREIGN KEY (value_id) REFERENCES adhesion_value (id)');
        $this->addSql('ALTER TABLE adhesion_amap_amapien ADD CONSTRAINT FK_D86B834461220EA6 FOREIGN KEY (creator_id) REFERENCES ak_amap (amap_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
