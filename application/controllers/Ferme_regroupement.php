<?php

use DI\Annotation\Inject;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class Ferme_regroupement extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordEncoder
     */
    public $passwordEncoder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordGenerator
     */
    public $passwordGenerator;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Email_Sender
     */
    private $email_sender;

    public function index()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST);

        $regroupements = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\FermeRegroupement::class)
            ->findAll()
        ;

        $this->twigDisplay('ferme_regroupement/index.html.twig', [
            'regroupements' => $regroupements,
        ]);
    }

    public function ajout()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST);

        $form = $this->formFactory->create(\PsrLib\Form\FermeRegroupementType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
            $regroupement = $form->getData();
            $this->em->persist($regroupement);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Regroupement ajouté'
            );

            redirect('/ferme_regroupement');
        }

        $this->twigDisplay('ferme_regroupement/ajout.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function supprimer($r_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST);
        $this->denyUnlessRequestMethod('POST');

        /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
        $regroupement = $this->findOrExit(\PsrLib\ORM\Entity\FermeRegroupement::class, $r_id);

        $this->em->remove($regroupement);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Regroupement supprimé'
        );

        redirect('/ferme_regroupement');
    }

    public function mdp($r_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REGROUPEMENT_LIST);
        $this->denyUnlessRequestMethod('POST');

        /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
        $regroupement = $this->findOrExit(\PsrLib\ORM\Entity\FermeRegroupement::class, $r_id);

        $password = $this->passwordGenerator->generatePassword();
        $pass_hash = $this->passwordEncoder->encodePassword($password);

        $regroupement->setPassword($pass_hash);
        $this->em->flush();

        $this->email_sender->envoyerMailMdp($regroupement, $password);

        $this->addFlash(
            'notice_success',
            'Mot de passe envoyé'
        );

        redirect('/ferme_regroupement');
    }

    public function profil()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_FERME_REGROUPEMENT_PROFIL);
        /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement Granted by security */
        $regroupement = $this->getUser();

        $form = $this->formFactory->create(\PsrLib\Form\FermeRegroupementProfilType::class, $regroupement);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newPassword = $form->get('password')->getData();
            if (null !== $newPassword) {
                $regroupement->setPassword(
                    $this->passwordEncoder->encodePassword($newPassword)
                );
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Informations mises à jour avec succès'
            );

            redirect('/ferme_regroupement/profil');
        }

        $this->twigDisplay('ferme_regroupement/profil.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
