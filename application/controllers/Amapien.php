<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use DI\Annotation\Inject;
use PsrLib\DTO\SearchAmapienState;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Amapien.
 */
class Amapien extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordEncoder
     */
    public $passwordEncoder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Password\PasswordGenerator
     */
    public $passwordGenerator;
    protected $data;

    /**
     * Amapien constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // -----------------------------------------------------------------------

        $this->data = ['livraisons' => [],
            'amapien_amap_all' => null,
            'amapien_all' => null,
            'reseau_all' => [],
            'reseau' => null,
            'alerte' => null,
            'remove' => null,
            'create' => null,
            'edit' => null,
            'key' => null,
            'key_all' => null,
            'amapien' => [],
            'amapien_id' => null,
            'gest' => null,
            'ref_res' => null,
            'ref_res_sec' => null,
            'villes' => [],
            'alerte_cp' => null,
            'annee_adhesion' => [],
            'c_all' => [], ];

        $this->load->helper('string');

        $this->load->library('email');
        $this->load->library('excel');
        $this->load->library('form_validation');

        $this
            ->form_validation
            ->set_error_delimiters('<div class="alert alert-danger">', '</div>')
        ;
    }

    public function index()
    {
        if ($this->getUser() instanceof \PsrLib\ORM\Entity\Amap) {
            $this->search_amap();

            return;
        }
        $this->search_admin();
    }

    /**
     * Accueil / Moteur de recherche.
     */
    public function search_amap()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_LIST);

        $this->redirectFromStoredGetParamIfExist('amapien');

        $amapienSearchState = new \PsrLib\DTO\SearchAmapienAmapState();
        $amapienSearchState->setAmap($this->getUser());

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienAmapType::class, $amapienSearchState, [
            'currentUser' => $this->getUser(),
        ]);
        $searchForm->handleRequest($this->request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $amapienSearchState = $searchForm->getData();
        }

        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->search($amapienSearchState)
        ;

        $this->loadViewWithTemplate('amapien/display_all', [
            'amapiens' => $amapiens,
            'searchForm' => $searchForm->createView(),
            'amapienSearchState' => $amapienSearchState,
        ]);
    }

    /**
     * Accueil / Moteur de recherche.
     */
    public function search_admin()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_LIST);

        $this->redirectFromStoredGetParamIfExist('amapien');
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $amapienSearchState = $searchForm->getData();
        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->search($amapienSearchState)
        ;

        $this->loadViewWithTemplate('amapien/display_all', [
            'amapiens' => $amapiens,
            'searchForm' => $searchForm->createView(),
            'amapienSearchState' => $amapienSearchState,
        ]);
    }

    /**
     * Afficher le détail de la fiche amapien.
     *
     * @param $amapien_id
     */
    public function display($amapien_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amap */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);

        // Sécurité
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_DISPLAY_PROFIL, $amapien);

        $this->loadViewWithTemplate('amapien/display', [
            'amapien' => $amapien,
        ]);
    }

    /**
     * Afficher le détail de la fiche amapien.
     *
     * @param $amapien_id
     */
    public function liste_attente($amapien_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);

        $form = $this->formFactory->create(\PsrLib\Form\AmapienListeAttente::class, $amapien);

        // Force submit on empty list
        if ('POST' === $this->request->getMethod()) {
            $form->submit($this->request->request->get('amapien_liste_attente', []));
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'La liste d\'attente de l\'AMAPien a été mise à jour avec succès'
            );

            redirect('/amapien');
        }

        /** @var \PsrLib\ORM\Entity\Ferme[] $amapFermes */
        $amapFermes = $this->em->getRepository(\PsrLib\ORM\Entity\Ferme::class)->getFromAmap($amapien->getAmap());
        $this->loadViewWithTemplate('amapien/liste_attente', [
            'form' => $form->createView(),
            'amapien' => $amapien,
            'amapFermes' => $amapFermes,
        ]);
    }

    /**
     * Calendrier de livraisons.
     *
     * @param $amapien_id
     * @param null $an
     * @param null $mois
     */
    public function calendrier($amapien_id, $an = null, $mois = null)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);

        // Sécurité
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_DISPLAY_PROFIL, $amapien);

        // PRÉFÉRENCES DU CALENDRIER -------------------------------------------
        $prefs = ['start_day' => 'monday',
            'day_type' => 'long',
            'show_next_prev' => true,
            'next_prev_url' => site_url('amapien/calendrier/'.$amapien_id), ]; // Le zéro est pour $date_id

        $prefs['template'] = ' 
      {table_open}<table class="table"  border="0" cellpadding="0" cellspacing="0">{/table_open}

      {heading_row_start}<tr>{/heading_row_start}

      {heading_previous_cell}<th><a href="{previous_url}"><i class="glyphicon glyphicon-menu-left"></i></a></th>{/heading_previous_cell}
      {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
      {heading_next_cell}<th><a href="{next_url}"><i class="glyphicon glyphicon-menu-right"></i></a></th>{/heading_next_cell}

      {heading_row_end}</tr>{/heading_row_end}

      {week_row_start}<tr>{/week_row_start}
      {week_day_cell}<td>{week_day}</td>{/week_day_cell}
      {week_row_end}</tr>{/week_row_end}

      {cal_row_start}<tr>{/cal_row_start}
      {cal_cell_start}<td>{/cal_cell_start}
      {cal_cell_start_today}<td>{/cal_cell_start_today}
      {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

      {cal_cell_content}<a href="{content}" data-toggle="modal" data-target="#modal_{day}"><button type=button class="btn btn-primary btn-xs">{day}</button></a>{/cal_cell_content}

      {cal_cell_no_content}{day}{/cal_cell_no_content}

      {cal_cell_blank}&nbsp;{/cal_cell_blank}

      {cal_cell_other}{day}{/cal_cel_other}

      {cal_cell_end}</td>{/cal_cell_end}
      {cal_cell_end_today}</td>{/cal_cell_end_today}
      {cal_cell_end_other}</td>{/cal_cell_end_other}
      {cal_row_end}</tr>{/cal_row_end}

      {table_close}</table>{/table_close}
      ';

        $this->load->library('calendar', $prefs);

        if (!$an) {
            $this->data['an'] = date('Y');
        } else {
            $this->data['an'] = $an;
        }
        if (!$mois) {
            $this->data['mois'] = date('m');
        } else {
            $this->data['mois'] = $mois;
        }

        $this->data['liens'] = null;

        // ---------------------------------------------------------------------
        // TOUTES LES DATES DE CONTRATS VIERGES (MC) LIÉS À UNE AMAPIEN --------
        $amap = $amapien->getAmap();
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $date_all */
        $date_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratDate::class)
            ->getByAmap($amap)
        ;
        if (count($date_all) > 0) {
            // On dédoublonne les dates ------------------------------------------
            $amapien_date_all = [];
            foreach ($date_all as $date) {
                $amapien_date_all[] = $date->getDateLivraison()->format('Y-m-d');
            }
            $amapien_date_all = array_unique($amapien_date_all);

            // Liens du calendrier et infos des Modals ---------------------------
            $this->data['liens'] = [];

            /** @var \PsrLib\ORM\Repository\ModeleContratRepository $modeleContratRepo */
            $modeleContratRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ;

            /** @var \PsrLib\ORM\Repository\ContratCelluleRepository $contratCelluleRepo */
            $contratCelluleRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ContratCellule::class)
            ;

            foreach ($amapien_date_all as $a_d) {
                // Année
                $a_d_an = substr($a_d, 0, 4);
                // Mois
                $a_d_mois = substr($a_d, 5, 2);

                // L'année et le mois du contrat correspondent avec l'année et le mois courant du calendrier
                if ($this->data['an'] == $a_d_an
                    && $this->data['mois'] == $a_d_mois
                ) {
                    $jour = substr($a_d, -2);
                    // On enlève le premier zéro du jour s'il y en a un --------------
                    if (0 == substr($jour, 0, 1)) {
                        $jour = substr($jour, 1, 2);
                    }

                    // On place un lien sur le jour du calendrier --------------------
                    $this->data['livraisons'][$jour] = [];

                    // Tableaux en fonction de l'AMAPien et de la date ---------------
                    // Tableau des contrats vierges : mc_id , mc_nom , lieu (nom ; adr ; cp ; ville)
                    $this->data['livraisons'][$jour]['mc'] = $modeleContratRepo
                        ->getFromAmapienDate($amapien, $a_d)
                    ;
                    $this->data['livraisons'][$jour]['cellules'] = $contratCelluleRepo
                        ->getFromAmapienDate($amapien, $a_d)
                    ;
                    if (count($this->data['livraisons'][$jour]['mc']) > 0
                        && count($this->data['livraisons'][$jour]['cellules']) > 0) {
                        $this->data['liens'][$jour] = '#';
                    }
                }
            }
        }

        $this->data['calendrier'] = $this
            ->calendar
            ->generate(
                $this->data['an'],
                $this->data['mois'],
                $this->data['liens']
            )
        ;
        $this->loadViewWithTemplate('amapien/calendrier', $this->data);
    }

    /**
     * Edition d'un amapien.
     *
     * @param null $amapien_id
     * @param bool $no_amap
     */
    public function informations_generales_edition_perso($amapien_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_OWN_PROFILE, $amapien);

        $formClass = \PsrLib\Form\AmapienOwnProfileType::class;
        if ($amapien->isAdminRegion() || $amapien->isAdminDepartment()) {
            $formClass = \PsrLib\Form\AmapienAdhesionInfoOwnProfileType::class;
        }
        $form = $this->formFactory->create($formClass, $amapien);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'AMAPien a été mis à jour avec succès'
            );

            redirect('/amapien/display/'.$amapien->getId());
        }

        $this->loadViewWithTemplate('amapien/informations_generales', [
            'form' => $form->createView(),
            'amapien' => $amapien,
        ]);
    }

    /**
     * Edition d'un amapien.
     *
     * @param null $amapien_id
     * @param bool $no_amap
     */
    public function informations_generales_edition($amapien_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);

        $form = $this->formFactory->create(\PsrLib\Form\AmapienType::class, $amapien);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'AMAPien a été mis à jour avec succès'
            );

            redirect('/amapien');
        }

        $this->loadViewWithTemplate('amapien/informations_generales', [
            'form' => $form->createView(),
            'amapien' => $amapien,
        ]);
    }

    /**
     * @param null|mixed $amap_id
     */
    public function informations_generales_creation($amap_id = null)
    {
        $amap = null;
        if (null === $amap_id) {
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_CREATE_NO_AMAP);
        } else {
            /** @var \PsrLib\ORM\Entity\Amap $amap */
            $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_LIST);
        }
        $amapien = new \PsrLib\ORM\Entity\Amapien();
        $amapien->setAmap($amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapienCreationType::class, $amapien);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($amapien);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'amapien(ne) a été créé(e) avec succès !'
            );

            if (true === $form->get('needWaiting')->getData()) {
                redirect('/amapien/liste_attente/'.$amapien->getId());
            }

            redirect('/amapien');
        }

        $this->loadViewWithTemplate('amapien/informations_generales', [
            'form' => $form->createView(),
            'amapien' => null,
        ]);
    }

    public function telecharger_amapiens_amap()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_LIST);

        $amapienSearchState = new \PsrLib\DTO\SearchAmapienAmapState();
        $amapienSearchState->setAmap($this->getUser());

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienAmapType::class, $amapienSearchState, [
            'currentUser' => $this->getUser(),
        ]);
        $searchForm->handleRequest($this->request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $amapienSearchState = $searchForm->getData();
        }

        /** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
        $amapiens = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->search($amapienSearchState);
        if (0 === count($amapiens)) {
            redirect('/amapien');
        }

        $this->_telecharger_amapien($amapiens, $searchForm->getData());
    }

    /**
     * Télécharger l'ensemble des amapiens de la session recherche.
     *
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function telecharger_amapiens()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_LIST);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        /** @var \PsrLib\DTO\SearchAmapienState $searchState */
        $searchState = $searchForm->getData();

        /** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
        $amapiens = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->search($searchState);
        if (0 === count($amapiens)) {
            redirect('/amapien');
        }

        $this->_telecharger_amapien($amapiens, $searchForm->getData());
    }

    /**
     * Créer un mot de passe pour un amapien et lui envoyer par email.
     *
     * @param $amapien_id
     */
    public function password($amapien_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);

        $password = $this->passwordGenerator->generatePassword();
        $pass_hash = $this->passwordEncoder->encodePassword($password);

        // EMAIL
        $amapien->setPassword($pass_hash);
        $amapien->setEtat(\PsrLib\ORM\Entity\Amapien::ETAT_ACTIF);
        $this->em->flush();

        $this->email->from(EMAIL, FROM);
        $this->email->to($amapien->getEmail());
        $this->email->subject('Bienvenue sur '.SITE_NAME);
        $this->email->message('<p>Bonjour, voici vos identifiants pour vous connecter à '.SITE_NAME.' :<br/>
        Adresse email : '.$amapien->getEmail().'<br/>
        Mot de passe : '.$password.'</p>
        <a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a>');

        if ($this->email->send()) {
            $this->addFlash(
                'notice_success',
                'Le mot de passe a été envoyé avec succès !'
            );
        } else {
            $this->addFlash(
                'notice_success',
                'Le mot de passe n\'a pas pu être envoyé'
            );
        }

        $this->email->clear();

        redirect('/amapien');
    }

    /**
     * Supprimer un amapien.
     *
     * @param $amapien_id
     */
    public function remove($amapien_id)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);

        $this->em->remove($amapien);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'amapien a été supprimé(e) avec succès'
        );

        redirect('/amapien');
    }

    /**
     * Activer / désactiver un amapien.
     *
     * @param $amapien_id
     * @param $etat
     */
    public function de_activate($amapien_id, $etat)
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);
        if (!in_array($etat, \PsrLib\ORM\Entity\Amapien::ETAT_AVALIABLE, true)) {
            redirect('/amapien');
        }

        $amapien->setEtat(\PsrLib\ORM\Entity\Amapien::ETAT_ACTIF === $etat ? \PsrLib\ORM\Entity\Amapien::ETAT_INACTIF : \PsrLib\ORM\Entity\Amapien::ETAT_ACTIF);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'état de l\'amapien a été modifié(e) avec succès'
        );

        redirect('/amapien');
    }

    /**
     * @param \PsrLib\ORM\Entity\Amapien[] $amapiens
     */
    private function _telecharger_amapien(
        $amapiens,
        PsrLib\DTO\SearchAmapienAmapState $searchAmapienAmapState
    ) {
        $time = 'extrait le '.date('d/m/Y');

        $nb_amapiens = count($amapiens);

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Amapiens');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Email');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Téléphone 1');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Téléphone 2');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Adresse');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Ville');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Adhérent année');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Abonné newsletter');
        $this->excel->getActiveSheet()->setCellValue('K4', 'liste d\'attente = oui/non');
        $this->excel->getActiveSheet()->setCellValue('L4', 'État');

        $this->excel->getActiveSheet()->getStyle('A4:L4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:L4')->getFont()->setBold(true);

        $i = 5;
        foreach ($amapiens as $amapien) {
            $annee_adhesion = implode(' | ', $amapien->getAnneeAdhesions()->toArray());

            $this->excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($amapien->getNom()));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($amapien->getPrenom()));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, $amapien->getEmail());
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('D'.$i, $amapien->getNumTel1(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('E'.$i, $amapien->getNumTel2(), PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('F'.$i, $amapien->getLibAdr1());
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('G'.$i, null !== $amapien->getVille() ? $amapien->getVille()->getCpString() : null, PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('H'.$i, strtoupper(null !== $amapien->getVille() ? $amapien->getVille()->getNom() : null));
            $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('I'.$i, $annee_adhesion);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('J'.$i, ucfirst(true === $amapien->getNewsletter() ? 'Oui' : 'Non'));
            $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            if (!$amapien->getFermeAttentes()->isEmpty()) {
                $this->excel->getActiveSheet()->setCellValue('K'.$i, 'OUI');
            } else {
                $this->excel->getActiveSheet()->setCellValue('K'.$i, 'NON');
            }

            $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('L'.$i, ucfirst($amapien->getEtat()));
            $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':L'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = null;

        if ($searchAmapienAmapState instanceof SearchAmapienState) {
            if (null !== $searchAmapienAmapState->getRegion()) {
                $titre = $searchAmapienAmapState->getRegion().' ; ';
            }

            if (null !== $searchAmapienAmapState->getDepartement()) {
                $titre .= $searchAmapienAmapState->getDepartement().' ; ';
            }

            if (null !== $searchAmapienAmapState->getReseau()) {
                $titre .= $searchAmapienAmapState->getReseau().' ; ';
            }
        }

        if (null !== $searchAmapienAmapState) {
            if (null !== $searchAmapienAmapState->getAmap()) {
                $titre .= $searchAmapienAmapState->getAmap().' ; ';
            }

            if (null !== $searchAmapienAmapState->getAdhesion()) {
                $titre .= 'Années d\'adhésion : '.$searchAmapienAmapState->getAdhesion().' ; ';
            }

            if (true === $searchAmapienAmapState->getNewsletter()) {
                $titre = $titre.'Abonnés à la newsletter'.' ; ';
            }

            if (true === $searchAmapienAmapState->getActive()) {
                $titre .= 'Actifs';
            }
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_amapiens_'.date('d_m_Y').'.xls';
        header('Content-Type: application/vnd.ms-excel'); // mime type
        header('Content-Disposition: attachment;filename="'.$nom_fichier.'"'); // Envoi du nom au navigateur
        header('Cache-Control: max-age=0'); // PAs de cache

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
}
