<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;

/**
 * @extends EntityRepository<Departement>
 */
class DepartementRepository extends EntityRepository
{
    /**
     * @var array
     */
    private $cachedUserDepartementMap = [];

    /**
     * @return Departement[]
     */
    public function findWithLogo()
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.logo IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Region[] $regions
     *
     * @return Departement[]
     */
    public function findByMultipleRegion($regions)
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.region in (:regions)')
            ->setParameter('regions', $regions)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get accessible region for user. Memory cache result to avoid recurring database requests.
     *
     * @return Departement[]
     */
    public function getDepartementsForUserCached(BaseUser $user, ?Region $region)
    {
        $userId = $user->getId();
        if (isset($this->cachedUserDepartementMap[$userId])) {
            return $this->cachedUserDepartementMap[$userId];
        }

        if ($user instanceof Amapien && $user->isAdminDepartment()) {
            $departements = $user->getAdminDepartments()->toArray();
        } else {
            $departements = $this->findBy(['region' => $region]);
        }

        $this->cachedUserDepartementMap[$userId] = $departements;

        return $departements;
    }
}
