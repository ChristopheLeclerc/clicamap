<?php

namespace PsrLib\Command;

use PsrLib\Services\PhpDiContrainerSingleton;
use Symfony\Component\Console\Command\Command;

abstract class BaseCommand extends Command
{
    public function __construct(string $name = null)
    {
        parent::__construct($name);
        $container = PhpDiContrainerSingleton::getContainer();
        $container->injectOn($this);
    }
}
