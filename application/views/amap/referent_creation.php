<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<script type="text/javascript">

  /*AUTOCOMPLÉTION*/
  $(document).ready(function () {

    $(function () {
      $("#ref_cp_ville").autocomplete({
        source: <?= '"'.site_url('ajax/get_cp_ville_suggestion"'); ?>,
        dataType: "json",
        minLength: 4,
        appendTo: "#container"
      });
    });

  });

</script>

<h3>Ajout d'un référent réseau AMAP</h3>

<h5><a href="<?= site_url('amap'); ?>">Gestion des AMAP</a> <i class="glyphicon glyphicon-menu-right"></i>
    Création d'une AMAP <i class="glyphicon glyphicon-menu-right"></i> Étape 7</h5>

<form method="POST" action="referent">

    <input type="hidden" name="step_ref" value="1"/>

    <?php

    // VARIABLES HIDDEN
    if (set_value('liv_lieu')) {
        echo '<input type="hidden" name="liv_lieu" value="'.set_value('liv_lieu').'" />';
    }

    if (set_value('liv_adr')) {
        echo '<input type="hidden" name="liv_adr" value="'.set_value('liv_adr').'" />';
    }

    if (set_value('liv_cp_ville')) {
        echo '<input type="hidden" name="liv_cp_ville" value="'.set_value('liv_cp_ville').'" />';
    }

    if (set_value('liv_gps_lat')) {
        echo '<input type="hidden" name="liv_gps_lat" value="'.set_value('liv_gps_lat').'" />';
    }

    if (set_value('liv_gps_long')) {
        echo '<input type="hidden" name="liv_gps_long" value="'.set_value('liv_gps_long').'" />';
    }

    if (set_value('liv_saison')) {
        echo '<input type="hidden" name="liv_saison" value="'.set_value('liv_saison').'" />';
    }

    if (set_value('liv_jour')) {
        echo '<input type="hidden" name="liv_jour" value="'.set_value('liv_jour').'" />';
    }

    if (set_value('liv_heure_deb')) {
        echo '<input type="hidden" name="liv_heure_deb" value="'.set_value('liv_heure_deb').'" />';
    }

    if (set_value('liv_heure_fin')) {
        echo '<input type="hidden" name="liv_heure_fin" value="'.set_value('liv_heure_fin').'" />';
    }

    if (set_value('nbr_amapiens')) {
        echo '<input type="hidden" name="nbr_amapiens" value="'.set_value('nbr_amapiens').'" />';
    }

    if (set_value('heure_deb')) {
        echo '<input type="hidden" name="heure_deb" value="'.set_value('heure_deb').'" />';
    }

    if (set_value('heure_fin')) {
        echo '<input type="hidden" name="heure_fin" value="'.set_value('heure_fin').'" />';
    }

    if (set_value('produits_proposes')) {
        foreach (set_value('produits_proposes') as $pp) {
            echo '<input type="hidden" name="produits_proposes[]" value="'.$pp.'" />';
        }
    }

    if (set_value('produits_recherches')) {
        foreach (set_value('produits_recherches') as $pr) {
            echo '<input type="hidden" name="produits_recherches[]" value="'.$pr.'" />';
        }
    }

    if (set_value('annee_adhesion')) {
        foreach (set_value('annee_adhesion') as $aa) {
            echo '<input type="hidden" name="annee_adhesion[]" value="'.$aa.'" />';
        }
    }

    if (set_value('nom')) {
        echo '<input type="hidden" name="nom" value="'.set_value('nom').'" />';
    }

    if (set_value('admin_lib_adr')) {
        echo '<input type="hidden" name="admin_lib_adr" value="'.set_value('admin_lib_adr').'" />';
    }

    if (set_value('admin_cp_ville')) {
        echo '<input type="hidden" name="admin_cp_ville" value="'.set_value('admin_cp_ville').'" />';
    }

    if (set_value('email_public')) {
        echo '<input type="hidden" name="email_public" value="'.set_value('email_public').'" />';
    }

    if (set_value('url')) {
        echo '<input type="hidden" name="url" value="'.set_value('url').'" />';
    }

    if (set_value('date_creation')) {
        echo '<input type="hidden" name="date_creation" value="'.set_value('date_creation').'" />';
    }

    if (set_value('nb_adherents')) {
        echo '<input type="hidden" name="nb_adherents" value="'.set_value('nb_adherents').'" />';
    }

    if (set_value('etat')) {
        echo '<input type="hidden" name="etat" value="'.set_value('etat').'" />';
    }

    if (set_value('ref_sec_prenom')) {
        echo '<input type="hidden" name="ref_sec_prenom" value="'.set_value('ref_sec_prenom').'" />';
    }

    if (set_value('ref_sec_nom')) {
        echo '<input type="hidden" name="ref_sec_nom" value="'.set_value('ref_sec_nom').'" />';
    }

    if (set_value('ref_sec_email')) {
        echo '<input type="hidden" name="ref_sec_email" value="'.set_value('ref_sec_email').'" />';
    }

    if (set_value('ref_sec_lib_adr_1')) {
        echo '<input type="hidden" name="ref_sec_lib_adr_1" value="'.set_value('ref_sec_lib_adr_1').'" />';
    }

    if (set_value('ref_sec_cp_ville')) {
        echo '<input type="hidden" name="ref_sec_cp_ville" value="'.set_value('ref_sec_cp_ville').'" />';
    }

    if (set_value('gest_prenom')) {
        echo '<input type="hidden" name="gest_prenom" value="'.set_value('gest_prenom').'" />';
    }

    if (set_value('gest_nom')) {
        echo '<input type="hidden" name="gest_nom" value="'.set_value('gest_nom').'" />';
    }

    if (set_value('gest_email')) {
        echo '<input type="hidden" name="gest_email" value="'.set_value('gest_email').'" />';
    }

    if (set_value('gest_lib_adr_1')) {
        echo '<input type="hidden" name="gest_lib_adr_1" value="'.set_value('gest_lib_adr_1').'" />';
    }

    if (set_value('gest_cp_ville')) {
        echo '<input type="hidden" name="gest_cp_ville" value="'.set_value('gest_cp_ville').'" />';
    }

    ?>

    <div class="alert alert-info">Un(e) référent(e) réseau doit être associé à cette AMAP. Vous pouvez créer l'amapien
        référent réseau en passant par les champs d'ajout rapide ci-dessous.
    </div>

    <div class="well">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-md-2">
                    <label for="ref_prenom" class="control-label">Prénom : </label>
                    <input type="text" class="form-control" name="ref_prenom"
                           value="<?php if (set_value('ref_prenom')) {
        echo set_value('ref_prenom');
    } ?>"/>
                    <?php if (set_value('step_ref')) {
        echo form_error('ref_prenom');
    } ?>
                </div>

                <div class="form-group col-md-2">
                    <label for="ref_nom" class="control-label">Nom : </label>
                    <input type="text" class="form-control" name="ref_nom" value="<?php if (set_value('ref_nom')) {
        echo set_value('ref_nom');
    } ?>"/>
                    <?php if (set_value('step_ref')) {
        echo form_error('ref_nom');
    } ?>
                </div>

                <div class="form-group col-md-2">
                    <label for="ref_email" class="control-label">Email : </label>
                    <input type="text" class="form-control" name="ref_email" value="<?php if (set_value('ref_email')) {
        echo set_value('ref_email');
    } ?>"/>
                    <?php if (set_value('step_ref')) {
        echo form_error('ref_email');
    } ?>
                </div>

                <div class="form-group col-md-2">
                    <label for="ref_lib_adr_1" class="control-label">Adresse : </label>
                    <input type="text" class="form-control" name="ref_lib_adr_1"
                           value="<?php if (set_value('ref_lib_adr_1')) {
        echo set_value('ref_lib_adr_1');
    } ?>"/>
                    <?php if (set_value('step_ref')) {
        echo form_error('ref_lib_adr_1');
    } ?>
                </div>

                <div class="form-group col-md-4">
                    <label for="ref_cp_ville" class="control-label">Code postal, Ville : </label>
                    <input type="text" class="form-control" id="ref_cp_ville" name="ref_cp_ville"
                           value="<?php if (set_value('ref_cp_ville')) {
        echo set_value('ref_cp_ville');
    } ?>"/>
                    <div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>
                    <?php if (set_value('step_ref')) {
        echo form_error('ref_cp_ville');
    } ?>
                </div>

            </div>
        </div>
    </div>

    <div class="form-group">

        <input type="submit" value="Étape suivante" class="pull-right btn btn-success"/>

</form>

<!-- ÉTAPE PRÉCÉDENTE -->
<form method="POST" action="<?= site_url('amap/produits'); ?>">

    <?php

    // VARIABLES HIDDEN
    if (set_value('liv_lieu')) {
        echo '<input type="hidden" name="liv_lieu" value="'.set_value('liv_lieu').'" />';
    }

    if (set_value('liv_adr')) {
        echo '<input type="hidden" name="liv_adr" value="'.set_value('liv_adr').'" />';
    }

    if (set_value('liv_cp_ville')) {
        echo '<input type="hidden" name="liv_cp_ville" value="'.set_value('liv_cp_ville').'" />';
    }

    if (set_value('liv_gps_lat')) {
        echo '<input type="hidden" name="liv_gps_lat" value="'.set_value('liv_gps_lat').'" />';
    }

    if (set_value('liv_gps_long')) {
        echo '<input type="hidden" name="liv_gps_long" value="'.set_value('liv_gps_long').'" />';
    }

    if (set_value('liv_saison')) {
        echo '<input type="hidden" name="liv_saison" value="'.set_value('liv_saison').'" />';
    }

    if (set_value('liv_jour')) {
        echo '<input type="hidden" name="liv_jour" value="'.set_value('liv_jour').'" />';
    }

    if (set_value('liv_heure_deb')) {
        echo '<input type="hidden" name="liv_heure_deb" value="'.set_value('liv_heure_deb').'" />';
    }

    if (set_value('liv_heure_fin')) {
        echo '<input type="hidden" name="liv_heure_fin" value="'.set_value('liv_heure_fin').'" />';
    }

    if (set_value('nbr_amapiens')) {
        echo '<input type="hidden" name="nbr_amapiens" value="'.set_value('nbr_amapiens').'" />';
    }

    if (set_value('heure_deb')) {
        echo '<input type="hidden" name="heure_deb" value="'.set_value('heure_deb').'" />';
    }

    if (set_value('heure_fin')) {
        echo '<input type="hidden" name="heure_fin" value="'.set_value('heure_fin').'" />';
    }

    if (set_value('produits_proposes')) {
        foreach (set_value('produits_proposes') as $pp) {
            echo '<input type="hidden" name="produits_proposes[]" value="'.$pp.'" />';
        }
    }

    if (set_value('produits_recherches')) {
        foreach (set_value('produits_recherches') as $pr) {
            echo '<input type="hidden" name="produits_recherches[]" value="'.$pr.'" />';
        }
    }

    if (set_value('annee_adhesion')) {
        foreach (set_value('annee_adhesion') as $aa) {
            echo '<input type="hidden" name="annee_adhesion[]" value="'.$aa.'" />';
        }
    }

    if (set_value('nom')) {
        echo '<input type="hidden" name="nom" value="'.set_value('nom').'" />';
    }

    if (set_value('admin_lib_adr')) {
        echo '<input type="hidden" name="admin_lib_adr" value="'.set_value('admin_lib_adr').'" />';
    }

    if (set_value('admin_cp_ville')) {
        echo '<input type="hidden" name="admin_cp_ville" value="'.set_value('admin_cp_ville').'" />';
    }

    if (set_value('email_public')) {
        echo '<input type="hidden" name="email_public" value="'.set_value('email_public').'" />';
    }

    if (set_value('url')) {
        echo '<input type="hidden" name="url" value="'.set_value('url').'" />';
    }

    if (set_value('date_creation')) {
        echo '<input type="hidden" name="date_creation" value="'.set_value('date_creation').'" />';
    }

    if (set_value('nb_adherents')) {
        echo '<input type="hidden" name="nb_adherents" value="'.set_value('nb_adherents').'" />';
    }

    if (set_value('etat')) {
        echo '<input type="hidden" name="etat" value="'.set_value('etat').'" />';
    }

    if (set_value('ref_prenom')) {
        echo '<input type="hidden" name="ref_prenom" value="'.set_value('ref_prenom').'" />';
    }

    if (set_value('ref_nom')) {
        echo '<input type="hidden" name="ref_nom" value="'.set_value('ref_nom').'" />';
    }

    if (set_value('ref_email')) {
        echo '<input type="hidden" name="ref_email" value="'.set_value('ref_email').'" />';
    }

    if (set_value('ref_lib_adr_1')) {
        echo '<input type="hidden" name="ref_lib_adr_1" value="'.set_value('ref_lib_adr_1').'" />';
    }

    if (set_value('ref_cp_ville')) {
        echo '<input type="hidden" name="ref_cp_ville" value="'.set_value('ref_cp_ville').'" />';
    }

    if (set_value('ref_sec_prenom')) {
        echo '<input type="hidden" name="ref_sec_prenom" value="'.set_value('ref_sec_prenom').'" />';
    }

    if (set_value('ref_sec_nom')) {
        echo '<input type="hidden" name="ref_sec_nom" value="'.set_value('ref_sec_nom').'" />';
    }

    if (set_value('ref_sec_email')) {
        echo '<input type="hidden" name="ref_sec_email" value="'.set_value('ref_sec_email').'" />';
    }

    if (set_value('ref_sec_lib_adr_1')) {
        echo '<input type="hidden" name="ref_sec_lib_adr_1" value="'.set_value('ref_sec_lib_adr_1').'" />';
    }

    if (set_value('ref_sec_cp_ville')) {
        echo '<input type="hidden" name="ref_sec_cp_ville" value="'.set_value('ref_sec_cp_ville').'" />';
    }

    if (set_value('gest_prenom')) {
        echo '<input type="hidden" name="gest_prenom" value="'.set_value('gest_prenom').'" />';
    }

    if (set_value('gest_nom')) {
        echo '<input type="hidden" name="gest_nom" value="'.set_value('gest_nom').'" />';
    }

    if (set_value('gest_email')) {
        echo '<input type="hidden" name="gest_email" value="'.set_value('gest_email').'" />';
    }

    if (set_value('gest_lib_adr_1')) {
        echo '<input type="hidden" name="gest_lib_adr_1" value="'.set_value('gest_lib_adr_1').'" />';
    }

    if (set_value('gest_cp_ville')) {
        echo '<input type="hidden" name="gest_cp_ville" value="'.set_value('gest_cp_ville').'" />';
    }

    ?>

    <input type="submit" value="Étape précédente" class="pull-right btn btn-default"/>
    <a href="<?= site_url('amap'); ?>" class="pull-right btn btn-default">Annuler</a>

    </div>

</form>
