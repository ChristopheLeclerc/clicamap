<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\FermeProduit[] $fermeProduits */
/** @var \PsrLib\ORM\Entity\ModeleContratProduit $produitsExistants */
/** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
?>

<h3><?= $contrat->getNom(); ?> : choix des produits et de leurs prix</h3>

<h5>
    <a href="<?= site_url('contrat_vierge'); ?>">Gestion des contrats vierges</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Création
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_1'); ?>">Étape 1</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    <a href="<?= site_url('contrat_vierge/v2_contrat_form_2'); ?>">Étape 2</a>
    <i class="glyphicon glyphicon-menu-right"></i>
    Étape 3
</h5>

<?php
    $cats = [];
    array_walk($fermeProduits, function (PsrLib\ORM\Entity\FermeProduit $fermeProduits) use (&$cats) {
        $cats[$fermeProduits->getTypeProduction()->getSlug()] = $fermeProduits->getTypeProduction()->getNom();
    });
?>

<div id="input-template" class="well well-sm hidden input-row">
    <div class="row">
        <div class="col-xs-3">
            <select class="form-control select-cat">
                <?php foreach ($cats as $key => $cat) {
    echo "<option value='{$key}'>{$cat}</option>";
}
                ?>
                </select>
        </div>

        <div class="col-xs-3">
            <select class="form-control select-prod">

            </select>
        </div>

        <div class="col-xs-3">
            <div class="input-group">
                <input type="number" class="form-control input-prix" step=".01" min="0" required/>
                <div class="input-group-addon">€</div>
            </div>

        </div>

        <div class="col-xs-2">
            <div class="checkbox">
                <label>
                    <input type="checkbox" class="check-regul"> Régularisation de poids
                </label>
            </div>
        </div>

        <div class="col-xs-1">
            <button type="button" class="btn btn-danger btn-remove"><i class="glyphicon glyphicon-minus"></i></button>
        </div>


    </div>
</div>

<script>
    var lineId = 0;
    $(document).ready(function () {

      // Gestion des produits
      var products = [
          <?php foreach ($fermeProduits as $fermeProduit) {
                    $nomEchappe = addslashes($fermeProduit->getNom());
                    $condEchappe = addslashes($fermeProduit->getConditionnement());
                    $regul = $fermeProduit->isRegulPds() ? '1' : '0';
                    echo "{
                cat: '{$fermeProduit->getTypeProduction()->getSlug()}' ,
                id: '{$fermeProduit->getId()}' ,
                nom: '{$nomEchappe}' ,
                cond: '{$condEchappe}',
                regul:'{$regul}',
                prix: '{$fermeProduit->getPrix()}',
                },";
                }?>
      ];

      // Gestion des changements de produits
      $('#form-produits').on('change', '.select-cat', function (e) {

        var valueSelected = this.value;

        var productSelect = $(this)
          .closest('.input-row')
          .find('.select-prod');

        productSelect
          .empty();

        products.forEach(function (product) {
          if(product.cat === valueSelected) {
            productSelect.append('<option value="' + product.id + '">' + product.nom + ' / ' + product.cond + '</option>');
          }
        });

        productSelect.trigger('change');
      });

      // Gestion des changements de produits
      $('#form-produits').on('change', '.select-prod', function (e) {

        var valueSelected = this.value;

        var regulCheck = $(this)
          .closest('.input-row')
          .find('.check-regul');
        var inputPrix = $(this)
          .closest('.input-row')
          .find('.input-prix');

        regulCheck.prop('checked', false);

        products.forEach(function (product) {
          if(product.id === valueSelected) {
            if(product.regul === '1') {
              regulCheck.prop('checked', true);
            }
            inputPrix.val(product.prix);

          }

        });
      });

      // Bouton ajout
      $('#btn-plus').on('click', function (e) {
        e.preventDefault();
        newLine();
      });

      // Bouton de suppression
      $('#form-produits').on('click', '.btn-remove', function (e) {
        e.preventDefault();

        $(this)
          .closest('.input-row')
          .remove();

        // Desactive bouton si il n'y a qu'une ligne
        if($('#products > div').length === 0) {
          $('input[type="submit"]').attr('disabled', true);
        }
      });

      // Ajoute les produits existants
      <?php $produitsNormalized = array_map(function (PsrLib\ORM\Entity\ModeleContratProduit $produit) {
                    return [
                        'tp' => $produit->getTypeProduction()->getSlug(),
                        'f' => $produit->getFermeProduit()->getId(),
                        'prix' => $produit->getPrix(),
                        'regul' => $produit->getRegulPds() ? '1' : '0',
                    ];
                }, $produitsExistants); ?>
      var existingProducts = JSON.parse('<?= json_encode($produitsNormalized, JSON_HEX_APOS); ?>');
      existingProducts.forEach(function (product) {
        newLine(product);
      });

      function newLine(product = null) {
        $('#products')
          .append($('#input-template').clone());

        var newRow = $('#products #input-template');

        newRow
          .find('.select-cat')
          .prop('name', 'cat[' + lineId + ']')
          .trigger('change');
        newRow
          .find('.select-prod')
          .prop('name', 'prod[' + lineId + ']');
        newRow
          .find('.input-prix')
          .prop('name', 'prix[' + lineId + ']');
        newRow
          .find('.check-regul')
          .prop('name', 'regul[' + lineId + ']');

        if(product !== null) {

            newRow
              .find('.select-cat')
              .val(product.tp)
              .trigger('change');
            newRow
              .find('.select-prod')
              .val(product.f);
            newRow
              .find('.input-prix')
              .val(product.prix);
            newRow
              .find('.check-regul')
              .prop('checked', parseInt(product.regul) === 1);

        }

        newRow
          .attr('id', '')
          .removeClass('hidden');


        lineId = lineId + 1;

        // Active bouton suivant
        $('input[type="submit"]').attr('disabled', false);
      }

    });

</script>

<form id="form-produits" method="POST" action="">
    <?= validation_errors(); ?>
    <div id="products">

    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-xs-12">
            <button
                type="button"
                id="btn-plus"
                class="btn btn-success pull-right">
                <i class="glyphicon glyphicon-plus"></i>
            </button>
        </div>
    </div>

    <input type="submit" value="Étape suivante" class="pull-right btn btn-success" style="margin-left:5px;" disabled/>
    <a href="<?= site_url('/contrat_vierge/v2_contrat_form_2'); ?>" class="pull-right btn btn-default" style="margin-left:5px;">Étape précédente</a>
    <a href="<?= site_url('contrat_vierge'); ?>" class="pull-right btn btn-default">Annuler</a>

</form>

