<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\BaseUser;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Repository\DepartementRepository;
use PsrLib\ORM\Repository\RegionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegionDepartementType extends AbstractType
{
    /**
     * @var RegionRepository
     */
    private $regionRepo;

    /**
     * @var DepartementRepository
     */
    private $departmentRepo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->regionRepo = $em
            ->getRepository(Region::class)
        ;
        $this->departmentRepo = $em
            ->getRepository(Departement::class)
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var null|BaseUser $currentUser */
        $currentUser = $options['currentUser'];
        if ($currentUser instanceof Amapien && $currentUser->isSuperAdmin()) {
            $regions = $this->regionRepo->findAllOrdered();
        } elseif ($currentUser instanceof Amapien && $currentUser->isAdminRegion()) {
            $regions = $currentUser->getAdminRegions()->toArray();
        } elseif ($currentUser instanceof Amapien && $currentUser->isAdminDepartment()) {
            $regions = $currentUser->getAdminDepartments()->map(function (Departement $departement) {
                return $departement->getRegion();
            })->toArray();
        } elseif ($currentUser instanceof Amap) {
            $regions = $this->regionRepo->findAllOrdered();
        } else {
            throw new LogicException('Invalid current user : '.$currentUser->getEmail());
        }
        $builder
            ->add('region', ChoiceType::class, [
                'choices' => $regions,
                'required' => false,
                'placeholder' => true,
                'choice_label' => function (?Region $region) {
                    return null === $region ? '' : $region->getNom();
                },
                'choice_value' => function (?Region $region) {
                    return null === $region ? '' : $region->getId();
                },
            ])
            ->add('departement', ChoiceType::class)
        ;

        $builder
            ->get('region')
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var null|Region $region */
                $region = $event->getForm()->getData();

                $event
                    ->getForm()
                    ->getParent()
                    ->add('departement', ChoiceType::class, [
                        'choices' => null === $region ? [] : $this->departmentRepo->findBy(['region' => $region]),
                        'required' => false,
                        'placeholder' => true,
                        'choice_label' => function (?Departement $departement) {
                            return null === $departement ? '' : $departement->getNom();
                        },
                        'choice_value' => function (?Departement $departement) {
                            return null === $departement ? '' : $departement->getId();
                        },
                    ])
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'currentUser' => null,
        ]);
    }
}
