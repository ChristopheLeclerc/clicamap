<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<title><?= SITE_NAME; ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="<?= base_url('assets/images/favicon.ico'); ?>" />

<!-- Sentry -->
<script src="https://polyfill.io/v3/polyfill.min.js?features=Promise%2CObject.assign%2CString.prototype.includes%2CNumber.isNaN"></script>
<script src="https://browser.sentry-cdn.com/5.6.3/bundle.min.js" integrity="sha384-/Cqa/8kaWn7emdqIBLk3AkFMAHBk0LObErtMhO+hr52CntkaurEnihPmqYj3uJho" crossorigin="anonymous">
</script>
<script>
  Sentry.init({ dsn: 'https://d05d1c4e7b53464c9e7be8676d20bce5@sentry.io/1765486' });
</script>

<link href="<?= base_url('assets/css/jquery-ui.css'); ?>" rel="stylesheet">

<link href="<?= base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/bootstrap/css/datepicker.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/css/fontawesome/css/all.min.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/plugins/slick/slick.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/plugins/slick/slick-theme-custom.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet"/>

<script type="text/javascript" src="<?= base_url('assets/jquery/jquery-2.2.3.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/jquery/jquery-ui.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/bootstrap/js/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/slick/slick.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/javascript/tableHeadFixer.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/jquery-debounce/jquery.ba-throttle-debounce.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/moment/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/jquery-mask/jquery.mask.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/select2/js/select2.min.js'); ?>"></script>


<script type="text/javascript" src="<?= base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
<link href="<?= base_url('assets/plugins/summernote/summernote.min.css'); ?>" rel="stylesheet">

<script type="text/javascript" src="<?= base_url('assets/javascript/custom.js'); ?>"></script>

<?= render_debug_bar_header(); ?>
