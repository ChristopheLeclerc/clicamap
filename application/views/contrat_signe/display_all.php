<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
/** @var \Symfony\Component\Form\FormView $searchForm */
/** @var \PsrLib\DTO\SearchContratSigneState $searchContratState */
$currentUser = get_current_connected_user();
?>

    <h3>Gestion des contrats signés</h3>

    <form method="GET" action="" class="js-form-search">
        <div class="well">
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Région</button>
                        </span>
                        <?= render_form_select_widget($searchForm['region']); ?>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Département</button>
                        </span>
                        <?= render_form_select_widget($searchForm['departement']); ?>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Réseau</button>
                        </span>
                        <?= render_form_select_widget($searchForm['reseau']); ?>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">AMAP</button>
                        </span>
                        <?= render_form_select_widget($searchForm['amapFerme']['amap']); ?>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Ferme</button>
                        </span>
                        <?= render_form_select_widget($searchForm['amapFerme']['ferme']); ?>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Contrat</button>
                        </span>
                        <?= render_form_select_widget($searchForm['mc']); ?>

                    </div>
                </div>
            </div>
	    <div class="row text-right">
		<a href="<?= site_url('contrat_signe?reset_search'); ?>" class="btn btn-primary" style="margin-right:5px;"><i class="glyphicon glyphicon-refresh"></i> Réinitialiser les critères de recherche</a>
	    </div>
        </div><!-- well -->
    </form>

<?php
require '_display_all_list.php';
?>
