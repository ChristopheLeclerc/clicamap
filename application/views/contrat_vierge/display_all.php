<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $searchForm */
/** @var \PsrLib\ORM\Entity\ModeleContrat[] $contrats */
/** @var \PsrLib\ORM\Entity\Ferme | null $fermeSelected */
/** @var \PsrLib\ORM\Entity\Amap | null $amapSelected */
?>


<h3>Gestion des contrats vierges</h3>

<form method="GET" action="/contrat_vierge" class="js-form-search">

<div class="well">
    <div class="row">
        <?php if ($searchForm->offsetExists('region')): ?>
            <div class="col-md-4">
                <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-primary"
                                type="button">Région</button>
                        </span>
                    <?= render_form_select_widget($searchForm['region']); ?>

                </div>
            </div>
        <?php endif; ?>

        <?php if ($searchForm->offsetExists('departement')): ?>
            <div class="col-md-4">
                <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary"
                                    type="button">Département</button>
                        </span>
                    <?= render_form_select_widget($searchForm['departement']); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($searchForm->offsetExists('reseau')): ?>
            <div class="col-md-4">
                <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button">Réseau</button>
                        </span>
                    <?= render_form_select_widget($searchForm['reseau']); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">AMAP</button>
                    </span>
                <?php if ($searchForm->offsetExists('amapFerme')): ?>
                    <?= render_form_select_widget($searchForm['amapFerme']['amap']); ?>
                <?php else: ?>
                    <?= render_form_select_widget($searchForm['amap']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Ferme</button>
                    </span>
                <?php if ($searchForm->offsetExists('amapFerme')): ?>
                    <?= render_form_select_widget($searchForm['amapFerme']['ferme']); ?>
                <?php else: ?>
                    <?= render_form_select_widget($searchForm['ferme']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row text-right">
	<a href="/contrat_vierge?reset_search" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> Réinitialiser les critères de recherche</a>
    </div>

</div><!-- well -->

</form>
<?php
if (null !== $fermeSelected) {
    echo '<p><a href="'.site_url('contrat_vierge/v2_contrat_form_1/'.$fermeSelected->getId().'/'.$amapSelected->getId()).'" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Créer un nouveau contrat</a>&nbsp;<a href="'.site_url('produit/accueil/'.$fermeSelected->getId()).'" class="btn btn-primary"><i class="glyphicon glyphicon-apple"></i> Produits proposés par la ferme</a></p>';
} else {
    echo '<p><a href="#" class="btn btn-success" disabled><i class="glyphicon glyphicon-plus"></i> Créer un nouveau contrat</a></p>';
}

if (count($contrats) >= 1) {
    ?>

    <div class="table-responsive">

        <table class="table table-hover">

            <thead class="thead-inverse">

            <tr>

                <th>Nom</th>

                <th>Etat</th>

                <th>Paniers<br/>obligatoires</th>

                <th>Choix<br/>identiques</th>

                <th>Date de fin de souscription</th>

                <th class="text-right">Outils</th>

            </tr>

            </thead>

            <tbody>

            <?php

            foreach ($contrats as $mc) {
                echo '<tr>';

                echo '<th scope="row">';
                echo ucfirst($mc->getNom());
                echo '</th>';

                echo '<td>';

                echo \PsrLib\ORM\Entity\ModeleContrat::getEtatLabel($mc->getEtat());

                echo '</td>';

                echo '<td>';
                echo ucfirst($mc->getNblivPlancher());
                echo '</td>';

                echo '<td>';
                if ($mc->getProduitsIdentiqueAmapien()) {
                    echo 'OUI';
                } else {
                    echo 'NON';
                }
                echo '</td>';

                echo '<td>';
                if ($mc->getForclusion()) {
                    echo $mc->getForclusion()->format('d/m/Y');
                } else {
                    echo 'Aucune';
                }
                echo '</td>';

                echo '<td class="text-right">'; ?>

                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_EDIT, $mc)) : ?>
                        <a title="Envoyer le contrat au paysan pour validation" href="#" data-toggle="modal" data-target="#modal_activate_<?= $mc->getId(); ?>" >
                            <i class="glyphicon glyphicon-send" data-toggle="tooltip" title="Envoyer le contrat au paysan pour validation"></i>
                        </a>

                        <a title="Tester le contrat" href="<?= site_url('contrat_vierge/v2_simulation/'.$mc->getId()); ?>">
                            <i class="glyphicon glyphicon-ok" data-toggle="tooltip" title="Tester le contrat"></i>
                        </a>

                        <a title="Modifier le contrat" href="<?= site_url('contrat_vierge/v2_contrat_edit/'.$mc->getId()); ?>">
                            <i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier le contrat"></i>
                        </a>
                        <a title="Supprimer le contrat" href="#" data-toggle="modal" data-target="#modal_remove_<?= $mc->getId(); ?>" >
                            <i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le contrat"></i>
                        </a>
                    <?php endif; ?>
                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_PREVIEW_PDF, $mc)): ?>
                        <a title="Prévisualiser le contrat" href="<?= site_url('contrat_vierge/v2_preview_pdf/'.$mc->getId()); ?>" target="_blank">
                            <i class="glyphicon glyphicon-file" data-toggle="tooltip" title="Prévisualiser le contrat"></i>
                        </a>
                    <?php endif; ?>

                    <!-- Modal remove -->
                    <div class="modal fade text-left" id="modal_remove_<?= $mc->getId(); ?>" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer
                                        "<?= ucfirst($mc->getNom()); ?>"</h5>
                                </div>
                                <div class="modal-body">
                                    <p class="text-right">
                                        <a href="<?= site_url('contrat_vierge/remove/'.$mc->getId()); ?>"
                                           class="btn btn-danger btn-sm">OUI</a>
                                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal activer -->
                    <div class="modal fade text-left" id="modal_activate_<?= $mc->getId(); ?>" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiment valider
                                        "<?= ucfirst($mc->getNom()); ?>" ?</h4>
                                    <h5 class="alert alert-danger">Avez-vous bien testé le contrat ? Une fois validé, vous ne pouvez plus modifier le contrat, sauf si le paysan le refuse</h5>
                                </div>
                                <div class="modal-body">
                                    <p class="text-right">
                                        <a href="<?= site_url('contrat_vierge/v2_state_to_validate/'.$mc->getId()); ?>"
                                           class="btn btn-danger btn-sm">OUI</a>
                                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    </td>
                </tr>

                <?php
            } ?>

            </tbody>
        </table>
    </div>

    <?php
} else {
                echo '<div class="alert alert-warning">Aucun contrat n\'a encore été créé.</div>';
            }

?>

