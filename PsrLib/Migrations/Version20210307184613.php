<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use PsrLib\ORM\Entity\Files\Logo;
use PsrLib\ORM\Entity\Region;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184613 extends AbstractMigration
{
    private $existingLogos = [];

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        $this->existingLogos = $this
            ->connection
            ->executeQuery('SELECT reg_id, reg_reseau_logo_filename FROM ak_region WHERE reg_reseau_logo_filename IS NOT NULL;')
            ->fetchAll()
        ;
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, original_file_name VARCHAR(255) DEFAULT NULL, file_name VARCHAR(255) NOT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_region ADD logo_id INT DEFAULT NULL, DROP reg_reseau_logo_filename');
    }

    public function postUp(Schema $schema): void
    {
        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        $regionRepo = $em->getRepository(Region::class);
        foreach ($this->existingLogos as $existingLogo) {
            $region = $regionRepo->findOneById($existingLogo['reg_id']);
            $logo = new Logo($existingLogo['reg_reseau_logo_filename']);
            $region->setLogo($logo);

            $em->persist($logo);
        }
        $em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
