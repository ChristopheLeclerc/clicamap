<?php

namespace Test\Services\EntityBuilder;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use PsrLib\DTO\AmapDistributionAjout;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Embeddable\Period;
use PsrLib\Services\EntityBuilder\AmapDistributionBuilder;

/**
 * @internal
 * @coversNothing
 */
class AmapDistributionBuilderTest extends TestCase
{
    /**
     * @var AmapDistributionBuilder
     */
    private $builder;

    public function setUp()
    {
        $this->builder = new AmapDistributionBuilder();
    }

    public function testRepAucune()
    {
        $dto = $this->buildDto(
            AmapDistributionAjout::REP_AUCUNE,
            // Date are equals if no repetition
            Period::buildFromDates(new Carbon('2000-01-01'), new Carbon('2000-01-01'))
        );

        $res = $this->builder->buildFromAjoutDTO($dto);
        self::assertCount(1, $res);
        self::assertSame('test ll', $res[0]->getAmapLivraisonLieu()->getNom());
        self::assertSame('test tache', $res[0]->getDetail()->getTache());
        self::assertTrue((new Carbon('2000-01-01'))->eq($res[0]->getDate()));
    }

    public function testRepSemaine()
    {
        $dto = $this->buildDto(
            AmapDistributionAjout::REP_SEMAINE,
            // Date are equals if no repetition
            Period::buildFromDates(new Carbon('2000-01-01'), new Carbon('2000-02-01'))
        );

        $res = $this->builder->buildFromAjoutDTO($dto);
        self::assertCount(5, $res);
        self::assertSame('test ll', $res[0]->getAmapLivraisonLieu()->getNom());
        self::assertSame('test tache', $res[0]->getDetail()->getTache());
        self::assertTrue((new Carbon('2000-01-01'))->eq($res[0]->getDate()));
        self::assertTrue((new Carbon('2000-01-08'))->eq($res[1]->getDate()));
        self::assertTrue((new Carbon('2000-01-15'))->eq($res[2]->getDate()));
        self::assertTrue((new Carbon('2000-01-22'))->eq($res[3]->getDate()));
        self::assertTrue((new Carbon('2000-01-29'))->eq($res[4]->getDate()));
    }

    public function testRep2Semaines()
    {
        $dto = $this->buildDto(
            AmapDistributionAjout::REP_2SEMAINES,
            // Date are equals if no repetition
            Period::buildFromDates(new Carbon('2000-01-01'), new Carbon('2000-02-01'))
        );

        $res = $this->builder->buildFromAjoutDTO($dto);
        self::assertCount(3, $res);
        self::assertSame('test ll', $res[0]->getAmapLivraisonLieu()->getNom());
        self::assertSame('test tache', $res[0]->getDetail()->getTache());
        self::assertTrue((new Carbon('2000-01-01'))->eq($res[0]->getDate()));
        self::assertTrue((new Carbon('2000-01-15'))->eq($res[1]->getDate()));
        self::assertTrue((new Carbon('2000-01-29'))->eq($res[2]->getDate()));
    }

    private function buildDto(string $repetition, Period $period): AmapDistributionAjout
    {
        $livLieu = new AmapLivraisonLieu();
        $livLieu->setNom('test ll');

        $detail = new AmapDistributionDetail();
        $detail->setTache('test tache');

        $ajout = new AmapDistributionAjout();
        $ajout->setDetail($detail);
        $ajout->setAmapLivraisonLieu($livLieu);
        $ajout->setPeriod($period);
        $ajout->setRepetition($repetition);

        return $ajout;
    }
}
