<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\MinkExtension\Context\RawMinkContext;

class MinkFailureContext extends RawMinkContext
{
    private $stepData = [];

    /**
     * @AfterStep
     *
     * Screenshot on failure for easier debugging
     */
    public function screenshotOnError(AfterStepScope $event)
    {
        // Disable from environmenet
        if (!(isset($_SERVER['TEST_DUMP_ON_FAILURE']) && 'true' === $_SERVER['TEST_DUMP_ON_FAILURE'])) {
            return;
        }

        // save step data
        $driver = $this->getSession()->getDriver();
        $this->stepData[] = [
            'screen' => $driver->getScreenshot(),
            'html' => $driver->getContent(),
        ];

        if (!$event->getTestResult()->isPassed()) {
            $filePath = '/var/www/html/features/screenshots/';
            if (!is_dir($filePath)) {
                mkdir($filePath);
            }

            $time = time();
            foreach ($this->stepData as $i => $data) {
                $destFile = $filePath.$time.'_'.$i;
                file_put_contents($destFile.'.png', $data['screen']);
                file_put_contents($destFile.'.html', $data['html']);
            }
            $this->stepData = [];
        }
    }

    /**
     * @AfterScenario
     */
    public function afterScenario()
    {
        $this->stepData = [];
    }
}
