<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use PsrLib\ORM\Entity\AdhesionValue;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class AdhesionValueTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testYearMin()
    {
        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'year',
            -1,
            'Default'
        );
        self::assertCount(1, $violations);
        self::assertSame('L\'année doit etre positive', $violations[0]->getMessage());

        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'year',
            1,
            'Default'
        );
        self::assertCount(0, $violations);
    }

    public function testFreeField1MaxLength()
    {
        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'freeField1',
            implode('', array_fill(0, 1001, 'a')),
            'Default'
        );
        self::assertCount(1, $violations);
        self::assertSame('Le champ libre 1 peut faire au maximum 1000 caractères.', $violations[0]->getMessage());

        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'freeField1',
            implode('', array_fill(0, 1000, 'a')),
            'Default'
        );
        self::assertCount(0, $violations);
    }

    public function testFreeField2MaxLength()
    {
        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'freeField2',
            implode('', array_fill(0, 1001, 'a')),
            'Default'
        );
        self::assertCount(1, $violations);
        self::assertSame('Le champ libre 2 peut faire au maximum 1000 caractères.', $violations[0]->getMessage());

        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'freeField2',
            implode('', array_fill(0, 1000, 'a')),
            'Default'
        );
        self::assertCount(0, $violations);
    }

    public function testVoucherNumberFormat()
    {
        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'voucherNumber',
            '123 aa',
            'Default'
        );
        self::assertCount(1, $violations);
        self::assertSame('Le numéro de recu doit contenir uniquement des lettres majuscule ou minuscule et des chiffres.', $violations[0]->getMessage());

        $violations = $this->validator->validatePropertyValue(
            AdhesionValue::class,
            'voucherNumber',
            '123aa',
            'Default'
        );
        self::assertCount(0, $violations);
    }
}
