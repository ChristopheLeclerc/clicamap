<?php

namespace Test\Services\Security;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class ContractSignedDisplayTest extends AbstractSecurityTest
{
    public function testAmapSameContract()
    {
        $amap = new Amap();
        $contract = $this->buildContractForAmap($amap);

        $this->mockAuthenticatedUser($amap);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contract));
    }

    public function testAmapDiffrentContract()
    {
        $contract = $this->buildContractForAmap(new Amap());

        $this->mockAuthenticatedUser(new Amap());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contract));
    }

    public function testPaysanSameContract()
    {
        $paysan = new Paysan();

        $ferme = new Ferme();
        $paysan->setFerme($ferme);

        $contract = $this->buildContractForFerme($ferme);

        $this->mockAuthenticatedUser($paysan);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contract));
    }

    public function testPaysanDiffrentContract()
    {
        $paysan = new Paysan();

        $ferme = new Ferme();
        $ferme->addPaysan($paysan);

        $contract = $this->buildContractForFerme($ferme);

        $this->mockAuthenticatedUser(new Paysan());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contract));
    }

    public function testFermeRegroupementSame()
    {
        $paysan = new Paysan();

        $ferme = new Ferme();
        $ferme->addPaysan($paysan);

        $regroupement = new FermeRegroupement();
        $regroupement->addFerme($ferme);

        $contract = $this->buildContractForFerme($ferme);

        $this->mockAuthenticatedUser($regroupement);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contract));
    }

    public function testFermeRegroupementDiffrent()
    {
        $paysan = new Paysan();

        $ferme = new Ferme();
        $ferme->addPaysan($paysan);

        $regroupement = new FermeRegroupement();
        $regroupement->addFerme($ferme);

        $contract = $this->buildContractForFerme($ferme);

        $this->mockAuthenticatedUser(new FermeRegroupement());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contract));
    }

    private function buildContractForAmap(Amap $amap)
    {
        $ll = new AmapLivraisonLieu();
        $ll->setAmap($amap);
        $mc = new ModeleContrat();
        $mc->setLivraisonLieu($ll);
        $contrat = new Contrat();
        $contrat->setModeleContrat($mc);

        return $contrat;
    }

    private function buildContractForFerme(Ferme $ferme)
    {
        $mc = new ModeleContrat();
        $mc->setFerme($ferme);
        $contract = new Contrat();
        $contract->setModeleContrat($mc);

        return $contract;
    }
}
