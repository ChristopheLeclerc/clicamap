# language: fr
Fonctionnalité: Comme amap, je ne doit pas pouvoir forcer la souscrition d'un amapien désactivé

  Scénario: Forcer la souscription possible si pas désactivé
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je vais sur "/contrat_signe/contrat_new_force/1"
    Alors le selecteur "amapien" devrait contenir l'option "amapien@test.amap-aura.org"

  @database
  Scénario: Forcer souscription impossible en cas de liste d'attente
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Et que je suis sur "/amapien"
    Et que je suis "Activation/desactivation de l'amapien"
    Et que je devrais voir "L'état de l'amapien a été modifié(e) avec succès"
    Quand  je vais sur "/contrat_signe/contrat_new_force/1"
    Alors le selecteur "amapien" ne devrait pas contenir l'option "amapien@test.amap-aura.org"
