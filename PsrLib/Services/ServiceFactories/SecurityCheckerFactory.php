<?php

namespace PsrLib\Services\ServiceFactories;

use Psr\Container\ContainerInterface;
use PsrLib\Services\Security\MockableSecurityChecker;
use PsrLib\Services\Security\SecurityChecker;
use PsrLib\Services\Security\SecurityCheckerInterface;

class SecurityCheckerFactory
{
    public function create(ContainerInterface $container): SecurityCheckerInterface
    {
        // Instanciate mockabe security checker on phpunit for unit testing
        if (defined('PHPUNIT_IS_RUNNING') && true === constant('PHPUNIT_IS_RUNNING')) {
            return $container->get(MockableSecurityChecker::class);
        }

        return $container->get(SecurityChecker::class);
    }
}
