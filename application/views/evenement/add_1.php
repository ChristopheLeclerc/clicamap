<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
$currentUser = get_current_connected_user();
/** @var ?\PsrLib\ORM\Entity\Evenement $ev */
?>

<h3>Informer</h3>

<?php

if (isset($ev_id)) {
    $mot = 'Modification';
} else {
    $mot = 'Création';
}

echo '<h5><a href="'.site_url('evenement').'">Événements</a> <i class="glyphicon glyphicon-menu-right"></i> '.$mot.' d\'un événement <i class="glyphicon glyphicon-menu-right"></i> Informer</h5>';

if (isset($ev_id)) {
    echo form_open_multipart('evenement/add_1/'.$ev_id, 'id="add1"');
} else {
    echo form_open_multipart('evenement/add_1', 'id="add1"');
}
// VARIABLES CACHÉES
// ------------------------------------------------------------------------
if (set_value('evt_nom')) {
    echo '<input type="hidden"  name="evt_nom" value="'.set_value('evt_nom').'" />';
}
if (set_value('date_deb')) {
    echo '<input type="hidden"  name="date_deb" value="'.set_value('date_deb').'" />';
}
if (set_value('date_fin')) {
    echo '<input type="hidden"  name="date_fin" value="'.set_value('date_fin').'" />';
}
if (set_value('hor_deb')) {
    echo '<input type="hidden"  name="hor_deb" value="'.set_value('hor_deb').'" />';
}
if (set_value('hor_fin')) {
    echo '<input type="hidden"  name="hor_fin" value="'.set_value('hor_fin').'" />';
}
if (set_value('url')) {
    echo '<input type="hidden"  name="url" value="'.set_value('url').'" />';
}
if (set_value('evt_txt')) {
    echo '<input type="hidden"  name="evt_txt" value="'.set_value('evt_txt').'" />';
}
// ------------------------------------------------------------------------

// SESSION AMAP OU RÉFÉRENT PRODUIT
if (($currentUser instanceof \PsrLib\ORM\Entity\Amap) || ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isRefProduit())) {
    ?>
    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-3">
                    <label for="amapiens" class="control-label">
                        <input type="checkbox" name="amapiens" id="amapiens" value="1"
                            <?php if (set_value('amapiens')) {
        echo set_checkbox('amapiens', '1');
    } elseif (isset($ev) && $ev->getInfoAmapiens()) {
        echo 'checked';
    } ?> />
                        Informer mes amapiens
                    </label>
                </div>
                <div class="form-group col-md-3">
                    <label for="reseau" class="control-label">
                        <input type="checkbox" name="reseau" id="reseau" value="1"
                            <?php if (set_value('reseau')) {
        echo set_checkbox('reseau', '1');
    } elseif (isset($ev) && $ev->getInfoCreateurReseau()) {
        echo 'checked';
    } ?> />
                        Informer mon réseau
                    </label>
                </div>
                <div class="input-group col-md-6">
            <span class="input-group-btn">
              <button class="btn btn-primary" type="button">Informer une ferme</button>
            </span>
                    <select class="form-control" id="ferme_id" name="ferme_id">
                        <option value=""></option>
                        <?php
                        foreach ($amap_ferme_all as $ferme) {
                            if (isset($ev) && null !== $ev->getInfoFerme() && $ev->getInfoFerme()->getId() === $ferme->getId()) {
                                echo '<option value="'.$ferme->getId().'" selected >'.$ferme->getNom().'</option>';
                            } elseif (set_value('ferme_id')) {
                                echo '<option value="'.$ferme->getId().'" '.set_select('ferme_id', $ferme->getId()).'>'.$ferme->getNom().'</option>';
                            } else {
                                echo '<option value="'.$ferme->getId().'">'.$ferme->getNom().'</option>';
                            }
                        } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <?php
} // SESSION ADMIN
elseif ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin()) {
    ?>

    <div class="alert alert-info">Avec les champs réseaux, vous pourrez circonscrire et sélectionner l'ensemble des AMAP
        et des fermes (ou des filières) que vous voulez informer de l'événement. Les filières prendront le pas sur la
        sélection d'une ou plusieurs fermes.
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="alert alert-info">Listes à choix multiple ; maintenez Shift ou Ctrl enfoncés pour sélectionner / désélectionner les éléments... Bla bla bla</div> -->
                <!-- ------------------------------------------------------------- -->
                <!-- RÉGIONS -->
                <?php
                if ($currentUser->isSuperAdmin()) {
                    echo '<div class="form-group col-md-4">';
                    echo '<p class="pull-right"><button type="button" onclick="reg_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="reg_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>'; ?>
                <h4>Régions</h4>
                <input type="hidden" name="reg_clear" id="reg_clear" value="0"/>
                <div id="regions" onclick="submit();"
                     style="height:200px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                    <?php
                    foreach ($region_all as $region) {
                        echo '<div class="checkbox"><input type="checkbox" name="reg[]" id="reg_'.$region->getId().'" value="'.$region->getId().'" '.set_checkbox('reg[]', $region->getId()).' onclick="submit();" /><label for="reg_'.$region->getId().'" class="control-label">'.$region.'</label></div>';
                    } ?>
                </div>
            </div>
            <?php
                } ?>
            <!-- ------------------------------------------------------------- -->
            <!-- DÉPARTEMENTS -->
            <?php
            if ($currentUser->isAdmin()) {
                if ($currentUser->isSuperAdmin()) {
                    echo '<div class="form-group col-md-4">';
                } else {
                    echo '<div class="form-group col-md-6">';
                }

                $n = 0;
                foreach ($departement_all as $departement) {
                    ++$n;
                }
                // Btn grisé
                if (0 == $n) {
                    echo '<p class="pull-right"><button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
                } else {
                    echo '<p class="pull-right"><button type="button" onclick="dep_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="dep_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
                } ?>
            <h4>Départements</h4>
            <input type="hidden" name="dep_clear" id="dep_clear" value="0"/>
            <div id="departements" onclick="submit();"
                 style="height:200px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                <?php
                foreach ($departement_all as $departement) {
                    if (set_value('dep[]')) {
                        echo '<div class="checkbox"><input type="checkbox" name="dep[]" id="dep_'.$departement->getId().'" value="'.$departement->getId().'" '.set_checkbox('dep[]', $departement->getId()).' onclick="submit();" /><label for="dep_'.$departement->getId().'" class="control-label">'.$departement.'</label></div>';
                    } elseif (isset($ev_info_dep) && in_array($departement, $ev_info_dep)) {
                        echo '<div class="checkbox"><input type="checkbox" name="dep[]" id="dep_'.$departement->getId().'" value="'.$departement->getId().'" checked onclick="submit();" /><label for="dep_'.$departement->getId().'" class="control-label">'.$departement.'</label></div>';
                    } else {
                        echo '<div class="checkbox"><input type="checkbox" name="dep[]" id="dep_'.$departement->getId().'" value="'.$departement->getId().'" onclick="submit();" /><label for="dep_'.$departement->getId().'" class="control-label">'.$departement.'</label></div>';
                    }
                } ?>
            </div>
        </div>
        <?php
            } ?>
        <!-- ------------------------------------------------------------- -->
        <!-- RÉSEAUX -->
        <?php
        if ($currentUser->isSuperAdmin()) {
            echo '<div class="form-group col-md-4">';
        } else {
            echo '<div class="form-group col-md-6">';
        }
    $n = 0;
    foreach ($dep_res as $reseau) {
        ++$n;
    }
    // Btn grisé
    if (0 == $n) {
        echo '<p class="pull-right"><button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } else {
        echo '<p class="pull-right"><button type="button" onclick="res_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="res_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } ?>
        <h4>Réseaux</h4>
        <input type="hidden" name="res_clear" id="res_clear" value="0"/>
        <div id="reseaux" onclick="submit();"
             style="height:200px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
            <?php
            $reseaux_id = []; // Pour dédoublonner
    foreach ($dep_res as $reseau) {
        if (!in_array($reseau->getId(), $reseaux_id)) {
            if (set_value('res[]')) {
                echo '<div class="checkbox"><input type="checkbox" name="res[]" id="res_'.$reseau->getId().'" value="'.$reseau->getId().'" '.set_checkbox('res[]', $reseau->getId()).' onclick="submit();" /><label for="res_'.$reseau->getId().'" class="control-label">'.$reseau.'</label></div>';
            } elseif (isset($ev_info_res) && in_array($reseau->getId(), $ev_info_res)) {
                echo '<div class="checkbox"><input type="checkbox" name="res[]" id="res_'.$reseau->getId().'" value="'.$reseau->getId().'" checked onclick="submit();" /><label for="res_'.$reseau->getId().'" class="control-label">'.$reseau.'</label></div>';
            } else {
                echo '<div class="checkbox"><input type="checkbox" name="res[]" id="res_'.$reseau->getId().'" value="'.$reseau->getId().'" onclick="submit();" /><label for="res_'.$reseau->getId().'" class="control-label">'.$reseau.'</label></div>';
            }
        }
        array_push($reseaux_id, $reseau->getId());
    } ?>
        </div>
    </div>
    </div>
    </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <!-- ------------------------------------------------------------- -->
                <!-- AMAP -->
                <?php
                echo '<div class="form-group col-md-4">';
    // Pour ensuite dédoublonner, ordre alphabétique et compter
    $n = 0;
    $amap_array_2 = [];
    foreach ($amap_array as $amap) {
        foreach ($amap as $a) {
            $amap_array_2[$a->getId()] = $a->getNom();
            ++$n;
        }
    }
    array_unique($amap_array_2);
    asort($amap_array_2);
    // Btn grisé
    if (0 == $n) {
        echo '<p class="pull-right"><button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } else {
        echo '<p class="pull-right"><button type="button" onclick="amap_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="amap_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } ?>
                <h4>AMAP</h4>
                <input type="hidden" name="amap_clear" id="amap_clear" value="0"/>
                <div id="amap_all" onclick="submit();"
                     style="height:200px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                    <?php
                    foreach ($amap_array_2 as $key => $amap) {
                        if (set_value('amap[]')) {
                            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="amap[]" id="amap_'.$key.'" value="'.$key.'" '.set_checkbox('amap[]', $key).' /><label for="amap_'.$key.'" class="control-label">'.ucfirst($amap).'</label></div>';
                        } elseif (isset($ev_info_amap) && in_array($amap, $ev_info_amap)) {
                            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="amap[]" id="amap_'.$key.'" value="'.$key.'" checked /><label for="amap_'.$key.'" class="control-label">'.ucfirst($amap).'</label></div>';
                        } else {
                            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="amap[]" id="amap_'.$key.'" value="'.$key.'" /><label for="amap_'.$key.'" class="control-label">'.ucfirst($amap).'</label></div>';
                        }
                    } ?>
                </div>
            </div>
            <!-- ------------------------------------------------------------- -->
            <!-- FERMES -->
            <div class="form-group col-md-4">
                <?php
                // Pour ensuite dédoublonner, ordre alphabétique et compter
                $n = 0;
    $ferme_array_2 = [];
    foreach ($ferme_array as $ferme) {
        foreach ($ferme as $f) {
            $ferme_array_2[$f->getId()] = $f->getNom();
            ++$n;
        }
    }
    array_unique($ferme_array_2);
    asort($ferme_array_2);
    // Btn grisé
    if (0 == $n) {
        echo '<p class="pull-right"><button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } else {
        echo '<p class="pull-right"><button type="button" onclick="ferme_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="ferme_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
    } ?>
                <h4>Fermes</h4>
                <input type="hidden" name="ferme_clear" id="ferme_clear" value="0"/>
                <div id="ferme_all" onclick="submit();"
                     style="height:200px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                    <?php
                    foreach ($ferme_array_2 as $key => $ferme) {
                        if (set_value('ferme[]')) {
                            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="ferme[]" id="ferme_'.$key.'" value="'.$key.'" '.set_checkbox('ferme[]', $key).' /><label for="ferme_'.$key.'" class="control-label">'.ucfirst($ferme).'</label></div>';
                        } elseif (isset($ev_info_ferme) && in_array($ferme, $ev_info_ferme)) {
                            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="ferme[]" id="ferme_'.$key.'" value="'.$key.'" checked /><label for="ferme_'.$key.'" class="control-label">'.ucfirst($ferme).'</label></div>';
                        } else {
                            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="ferme[]" id="ferme_'.$key.'" value="'.$key.'" /><label for="ferme_'.$key.'" class="control-label">'.ucfirst($ferme).'</label></div>';
                        }
                    } ?>
                </div>
            </div>
            <!-- ------------------------------------------------------------- -->
            <!-- TYPES DE PRODUCTION -->
            <div class="form-group col-md-4">
                <?php
                // Btn grisé
                if (!$type_prod) {
                    echo '<p class="pull-right"><button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" class="btn btn-default btn-sm disabled"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
                } else {
                    echo '<p class="pull-right"><button type="button" onclick="tp_check_all();" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i></button>&nbsp;<button type="button" onclick="tp_uncheck_all();" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-unchecked"></i></button></p>';
                } ?>
                <h4>Filières</h4>
                <input type="hidden" name="tp_clear" id="tp_clear" value="0"/>
                <div id="tp_all" onclick="submit();"
                     style="height:200px; width:100%; overflow:scroll; background-color:#fff; padding-left:30px;">
                    <?php
    foreach ($type_prod as $tp) {
        $tp_nom = $tp->getNomComplet();
        // -----------------------------------------------------------
        if (set_value('tp[]')) {
            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="tp[]" id="tp_'.$tp->getSlug().'" value="'.$tp->getSlug().'" '.set_checkbox('tp[]', $tp->getSlug()).' /><label for="tp_'.$tp->getSlug().'" class="control-label">'.$tp_nom.'</label></div>';
        } elseif (isset($ev_info_tp) && in_array($tp, $ev_info_tp)) {
            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="tp[]" id="tp_'.$tp->getSlug().'" value="'.$tp->getSlug().'" checked /><label for="tp_'.$tp->getSlug().'" class="control-label">'.$tp_nom.'</label></div>';
        } else {
            echo '<div class="checkbox"><input type="checkbox" onclick="submit()" name="tp[]" id="tp_'.$tp->getSlug().'" value="'.$tp->getSlug().'" /><label for="tp_'.$tp->getSlug().'" class="control-label">'.$tp_nom.'</label></div>';
        }
    } ?>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
} ?>

<div class="form-group">
    <?php
    if ($ev_id) {
        $mot = 'Valider';
    } else {
        $mot = 'Étape suivante';
    }

    // Création
    if (!empty($_POST)) {
        // Admin
        if ($currentUser instanceof \PsrLib\ORM\Entity\Amapien && $currentUser->isAdmin()) {
            // Champs obligatoires
            if (set_value('amap') || set_value('ferme') || set_value('tp')) {
                echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;" onclick="valide()"/><a href="'.site_url('evenement').'" class="pull-right btn btn-default">Annuler</a>';
            } else {
                echo '<input type="button" value="'.$mot.'" class="pull-right btn btn-default disabled" style="margin-left:5px;"/><a href="'.site_url('evenement').'" class="pull-right btn btn-default">Annuler</a>';
            }
        } else {
            echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;" onclick="valide()"/><a href="'.site_url('evenement').'" class="pull-right btn btn-default">Annuler</a>';
        }
    } // Gestionnaire AMAP & édition
    else {
        echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;" onclick="valide()"/><a href="'.site_url('evenement').'" class="pull-right btn btn-default">Annuler</a>';
    }
    // Édition
    // elseif($ev_id)
    //   echo '<input type="submit" value="'.$mot.'" class="pull-right btn btn-success" style="margin-left:5px;" onclick="valide()"/><a href="'.site_url('evenement').'" class="pull-right btn btn-default">Annuler</a>';
    ?>
    <script type="text/javascript">
      function valide() {
        $('#etape_validee').val(1);
        document.getElementById("add1").submit();
      }
    </script>
</div>
<input type="hidden" name="etape_validee" id="etape_validee" value="0"/>
</form>


<script type="text/javascript">

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
  });

  $(function () {
    $('.datepicker').datepicker();
  });

  function reg_check_all() {
    $("#regions input:checkbox").attr('checked', true);
    document.getElementById("add1").submit();
  }

  function reg_uncheck_all() {
    $('#reg_clear').val(1);
    $('#dep_clear').val(1);
    $('#res_clear').val(1);
    $('#amap_clear').val(1);
    $('#ferme_clear').val(1);
    $('#tp_clear').val(1);
    document.getElementById("add1").submit();
  }

  function dep_check_all() {
    $("#departements input:checkbox").attr('checked', true);
    document.getElementById("add1").submit();
  }

  function dep_uncheck_all() {
    $('#dep_clear').val(1);
    $('#res_clear').val(1);
    $('#amap_clear').val(1);
    $('#ferme_clear').val(1);
    $('#tp_clear').val(1);
    document.getElementById("add1").submit();
  }

  function res_check_all() {
    $("#reseaux input:checkbox").attr('checked', true);
    document.getElementById("add1").submit();
  }

  function res_uncheck_all() {
    $('#res_clear').val(1);
    $('#amap_clear').val(1);
    $('#ferme_clear').val(1);
    $('#tp_clear').val(1);
    document.getElementById("add1").submit();
  }

  function amap_check_all() {
    $("#amap_all input:checkbox").attr('checked', true);
    document.getElementById("add1").submit();
  }

  function amap_uncheck_all() {
    $('#amap_clear').val(1);
    // $('#ferme_clear').val(1);
    // $('#tp_clear').val(1);
    document.getElementById("add1").submit();
  }

  function ferme_check_all() {
    $("#ferme_all input:checkbox").attr('checked', true);
    document.getElementById("add1").submit();
  }

  function ferme_uncheck_all() {
    $('#ferme_clear').val(1);
    // $('#tp_clear').val(1);
    document.getElementById("add1").submit();
  }

  function tp_check_all() {
    $("#tp_all input:checkbox").attr('checked', true);
    document.getElementById("add1").submit();
  }

  function tp_uncheck_all() {
    $('#tp_clear').val(1);
    document.getElementById("add1").submit();
  }

</script>
