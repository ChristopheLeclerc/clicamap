<?php

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;

/**
 * @internal
 * @coversNothing
 */
class AmapDistributionTest extends TestCase
{
    public function testGetHeureDebut()
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 00),
                new Time(11, 00)
            )
        ;
        self::assertTrue(Carbon::parse('2000-01-01 10:00:00')->eq($distribution->getDateDebut()));
    }

    public function testGetHeureFin()
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 00),
                new Time(11, 00)
            )
        ;
        self::assertTrue(Carbon::parse('2000-01-01 11:00:00')->eq($distribution->getDateFin()));
    }

    public function testgetGetPlacesRestantesDefaut()
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 00),
                new Time(11, 00)
            )
        ;
        self::assertSame(10, $distribution->getPlacesRestantes());
    }

    public function testgetGetPlacesRestantesinscrit()
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 00),
                new Time(11, 00)
            )
        ;
        $distribution->addAmapien(new Amapien());
        self::assertSame(9, $distribution->getPlacesRestantes());
    }

    public function testIsCompletedFalse()
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 00),
                new Time(11, 00)
            )
        ;
        $distribution->addAmapien(new Amapien());
        self::assertFalse($distribution->isCompleted());
    }

    public function testIsCompletedTrue()
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 00),
                new Time(11, 00)
            )
        ;
        for ($i = 0; $i < 10; ++$i) {
            $distribution->addAmapien(new Amapien());
        }
        self::assertTrue($distribution->isCompleted());
    }

    private function buildDistribution(?Carbon $date, ?Time $heureDebut, ?Time $heureFin)
    {
        return new AmapDistribution(
            AmapDistributionDetail::create($heureDebut, $heureFin, 10, 'tache'),
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            $date
        );
    }
}
