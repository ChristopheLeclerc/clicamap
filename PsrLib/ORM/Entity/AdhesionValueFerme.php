<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class AdhesionValueFerme extends AdhesionValue
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("SIRET")
     *
     * @var ?string
     * @Assert\NotBlank(message="Le SIRET ne peut pas être vide.")
     */
    private $siret;

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): AdhesionValueFerme
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Bind amount membership to global amount. Used on pdf generation to force print membership related informations.
     */
    public function getAmountMembership(): ?Money
    {
        return $this->getAmount();
    }
}
