<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if ($_SESSION) {
    echo '<div class="alert alert-success">Bonjour';

    // INFO AMAPIEN
    if ($this->session->prenom) {
        echo ' <strong>'.ucfirst(strtolower($this->session->prenom)).' '.mb_strtoupper($this->session->nom).'</strong>';
    }

    echo ', bienvenue sur <strong>'.SITE_NAME.' !</strong> Vous êtes actuellement connecté en tant ';

    // SUPER ADMIN
    if ($this->session->sup_adm) {
        echo 'que <strong>Super Administrateur</strong>. Vous avez tous les droits.';
    } // ADMINISTRATEURS RÉSEAUX
    elseif ($this->session->adm_reg_active) {
        echo 'qu\'<strong>Administrateur Réseau</strong>. Vous avez tous les droits sur la région : <strong>';

        echo $this->session->adm_reg_active->reg_nom;

        echo '</strong>.';
    } elseif ($this->session->adm_dep) {
        echo 'qu\'<strong>Administrateur Réseau</strong>. Vous avez tous les droits sur le(s) département(s) : <strong>';

        $i = 1;
        $nb_dep = count($this->session->adm_dep);

        foreach ($this->session->adm_dep as $dep) {
            echo $dep->dep_nom;

            if ($i < $nb_dep) {
                echo ' ; ';
            }

            ++$i;
        }

        echo '</strong>.';
    } elseif ($this->session->adm_res_actif) {
        echo 'qu\'<strong>Administrateur Réseau</strong>. Vous avez tous les droits sur le(s) réseau(x) : <strong>';

        $i = 1;
        $nb_res = count($this->session->adm_res_actif);

        foreach ($this->session->adm_res_actif as $res) {
            echo $res->res_nom;

            if ($i < $nb_res) {
                echo ' ; ';
            }

            ++$i;
        }

        echo '</strong>.';
    } // GESTIONNAIRE
    elseif ($this->session->gest_amap_active) {
        echo 'qu\'<strong>AMAP</strong> : <strong>';

        echo $this->session->gest_amap_active->amap_nom;

        echo '</strong>.';
    } // RÉFÉRENT PRODUIT AVEC N FERME
    elseif ($this->session->ref_prod_n_ferme) {
        $n = count($this->session->ref_prod_n_ferme);
        $x = 1;

        echo 'que <strong>Référent produit</strong> auprès de : <strong>';

        foreach ($this->session->ref_prod_n_ferme as $ferme) {
            echo trim($ferme->f_nom);

            if ($x < $n) {
                echo ' ; ';
            } else {
                echo '.';
            }

            ++$x;
        }

        echo '</strong>';
    } // RÉFÉRENT PRODUIT AVEC 1 FERME
    elseif ($this->session->ref_prod && !$this->session->ref_prod_n_ferme) {
        echo 'que <strong>Référent produit</strong> auprès de : <strong>';

        echo trim($this->session->ref_prod->f_nom);

        echo '.';

        echo '</strong>';
    } // INFOS AMAPIEN AVEC AMAP
    elseif ($this->session->amapien && $this->session->mon_amap) {
        echo 'qu\'<strong>amapien</strong> de : <strong>';

        echo $this->session->mon_amap->amap_nom;

        echo '</strong>.';
    } // INFOS AMAPIEN SANS AMAP
    elseif ($this->session->amapien && !$this->session->mon_amap) {
        echo 'qu\'utilisateur sans AMAP ni privilège.';
    } // INFOS PAYSAN
    elseif ($this->session->paysan) {
        echo 'que <strong>paysan</strong> de la ferme <strong>';

        echo $this->session->f_nom;

        echo '</strong>.';
    }

    echo '</div>';
}

?>

<!--<div class="well well-lg">

<h3>Adhésion</h3>

<p>Amapiens, Paysans, pour utiliser <?= SITE_NAME; ?>, vous devez être adhérents du réseau et qu'un membre de votre AMAP s'occupe de gérer votre espace en ligne.</p>

<p>Vous n'êtes pas encore adhérent, téléchargez notre bulletin d'adhésion :</p>

<h4>Pour l'Isère :</h4>

<p>
<a href="<?= site_url('assets/bulletins/isere_bulletin_adhesion_paysan_2016.doc'); ?>"  class="btn btn-default">Bulletin d'adhésion paysan</a>
<a href="<?= site_url('assets/bulletins/isere_bulletin_adhesion_AMAP_2016.doc'); ?>"  class="btn btn-default">Bulletin d'adhésion AMAP</a>
</p>

<h4>Pour la Savoie :</h4>
<p><a href="https://mega.nz/#F!LBh1BAJI!dVNPeviNnXi3WOzsOOLfNw" target="_blanck" class="btn btn-default">Bulletins d'adhésion</a></p>

<h4>Pour les autres départements d'Auvergne-Rhônes-Alpes :</h4>

<p>
<a href="https://mega.nz/#!OQJGXA4L!UOIeF9Hf7uQn9IkH_0fwL9oGHii78LIhvf2Md1WWLZk" target="_blank" class="btn btn-default">Bulletin d'adhésion paysan</a>
<a href="https://mega.nz/#!6I4jDBDC!W1BS_TNtaiS46e_HoQx-Yln71kAVGg6J8sVmGZfw41o" target="_blanck" class="btn btn-default">Bulletin d'adhésion AMAP</a>
</p>

</div>-->

<!-- <div class="well">

<p>Vous souhaitez administrer <?= SITE_NAME; ?> pour votre AMAP, <a href="<?= site_url('contact'); ?>">contactez-nous</a>. Le réseau vous transmettra alors un identitfiant qui vous permettra de gérer votre AMAP et vos contrats en toute simplicité grâce à <?= SITE_NAME; ?>.</p>

</div> -->
