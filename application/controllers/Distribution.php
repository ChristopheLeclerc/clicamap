<?php

use Carbon\Carbon;
use DI\Annotation\Inject;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class Distribution extends AppController
{
    /**
     * @Inject
     *
     * @var \PsrLib\Services\EntityBuilder\AmapDistributionBuilder
     */
    private $amapDistributionPeriodBuilder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\EntityBuilder\AmapDistributionDetailMassEditDtoBuilder
     */
    private $amapDistributionDetailBuilder;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Email_Sender
     */
    private $email_sender;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Exporters\ExcelGeneratorDistributionAmap
     */
    private $excelGeneratorDistributionAmap;

    public function index()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_LIST);

        $this->redirectFromStoredGetParamIfExist('distribution_amap');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchDistributionAmapType::class, null, [
            'amap' => $this->getUser(),
        ]);

        //Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $distributions = [];
        if ($searchForm->isValid()) {
            $distributions = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\AmapDistribution::class)
                ->search($searchForm->getData())
            ;
        }

        $this
            ->twigDisplay('distribution/index.html.twig', [
                'distributions' => $distributions,
                'searchForm' => $searchForm->createView(),
            ])
        ;
    }

    public function detail($d_id)
    {
        /** @var \PsrLib\ORM\Entity\AmapDistribution $distribution */
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_DETAIL, $distribution);

        $form = $this->formFactory->create(\PsrLib\Form\AmapDistributionAmapAmapienInscription::class, null, [
            'distribution' => $distribution,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
            $amapiens = $form->get('amapiens')->getData();
            foreach ($amapiens as $amapien) {
                $distribution->addAmapien($amapien);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Amapiens ajoutés avec succès'
            );
            foreach ($amapiens as $amapien) {
                if ($amapien->estActif()) {
                    $this->email_sender->envoyerDistributionInscriptionAmapAmapien($distribution, $amapien);
                }
            }
            redirect('/distribution/detail/'.$distribution->getId());
        }

        $this->twigDisplay('distribution/detail.html.twig', [
            'distribution' => $distribution,
            'form' => $form->createView(),
        ]);
    }

    public function amap_desinscription($d_id)
    {
        $this->denyUnlessRequestMethod('POST');

        /** @var \PsrLib\ORM\Entity\AmapDistribution $distribution */
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_DETAIL, $distribution);

        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $this->request->request->get('amapien'));

        if (!$distribution->getAmapiens()->contains($amapien)) {
            $this->exit_error();
        }

        $distribution->removeAmapien($amapien);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Amapiens désinscrit avec succès'
        );

        if ($amapien->estActif()) {
            $this->email_sender->envoyerDistributionDesinscriptionAmapAmapien($distribution, $amapien);
        }

        redirect('/distribution/detail/'.$distribution->getId());
    }

    public function amapien()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN);
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->getUser();

        $this->redirectFromStoredGetParamIfExist('distribution_amapien');

        $searchState = new \PsrLib\DTO\SearchDistributionAmapienState($amapien);
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchDistributionAmapienType::class, $searchState, [
            'amap' => $amapien->getAmap(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $distributions = [];
        if ($searchForm->isValid()) {
            $distributions = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\AmapDistribution::class)
                ->searchAmapienState($searchState)
            ;
        }
        $this->twigDisplay('distribution/liste.twig', [
            'distributions' => $distributions,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    public function ajout()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_LIST);

        $form = $this->formFactory->create(\PsrLib\Form\AmapDistributionAjoutType::class, null, [
            'amap' => $this->getUser(),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\DTO\AmapDistributionAjout $dto */
            $dto = $form->getData();

            $distributions = $this->amapDistributionPeriodBuilder->buildFromAjoutDTO($dto);
            foreach ($distributions as $distribution) {
                $this->em->persist($distribution);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Les distributions ont été ajoutées.'
            );

            redirect('/distribution');
        }

        $this->twigDisplay('distribution/ajout.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function edit($id)
    {
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_EDIT, $distribution);

        $form = $this->formFactory->create(\PsrLib\Form\AmapDistributionEditType::class, $distribution, [
            'amap' => $this->getUser(),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'La distribution a été modifiées.'
            );

            redirect('/distribution');
        }

        $this->twigDisplay('distribution/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function supprimer($id)
    {
        $this->denyUnlessRequestMethod('POST');
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_DELETE, $distribution);

        $this->distributionMarkToRemove($distribution);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'La distribution a été supprimée avec succès'
        );

        redirect('/distribution');
    }

    public function supprimer_masse()
    {
        $distributions = $this->parseDistributionIdsFromRequest();
        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_DELETE, $distributions);

        $form = $this
            ->formFactory
            ->createBuilder()
            ->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                'label' => 'Valider',
            ])
            ->getForm()
        ;
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($distributions as $distribution) {
                $this->distributionMarkToRemove($distribution);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Les distributions ont été supprimées avec succès'
            );

            redirect('/distribution');
        }

        $this->twigDisplay('distribution/suppression_masse.html.twig', [
            'form' => $form->createView(),
            'distributions' => $distributions,
        ]);
    }

    public function edit_masse()
    {
        $distributions = $this->parseDistributionIdsFromRequest();
        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_EDIT, $distributions);

        $dto = $this
            ->amapDistributionDetailBuilder
            ->buildFromMultipleDistributions($distributions)
        ;
        $form = $this
            ->formFactory
            ->create(\PsrLib\Form\AmapDistributionEditMasseType::class, $dto, [
                'amap' => $this->getUser(), // granted by permission
            ])
        ;
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $detail = $dto->getDetail();
            $ll = $dto->getLivraisonLieu();

            foreach ($distributions as $distribution) {
                $distribution->setDetail($detail);
                $distribution->setAmapLivraisonLieu($ll);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Les distributions ont été modifiées avec succès'
            );

            redirect('/distribution');
        }

        $this->twigDisplay('distribution/edition_masse.html.twig', [
            'form' => $form->createView(),
            'distributions' => $distributions,
        ]);
    }

    public function telecharger_masse()
    {
        $distributions = $this->parseDistributionIdsFromRequest();
        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_DETAIL, $distributions);

        /** @var \PsrLib\ORM\Entity\Amap $currentUser Granted by permission check */
        $currentUser = $this->getUser();
        \Assert\Assertion::isInstanceOf($currentUser, \PsrLib\ORM\Entity\Amap::class);

        $this->load->helper('download');
        $outFileName = $this->excelGeneratorDistributionAmap->genererExportDistributionInscrits($distributions);
        force_download(
            sprintf('Distribution_%s_%s.xls', $currentUser->getNom(), Carbon::now()->format('Y_m_d')),
            file_get_contents($outFileName),
            true
        );
        unlink($outFileName);
    }

    public function inscription(string $d_id)
    {
        $this->denyUnlessRequestMethod('POST');

        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution);

        $amapien = $this->getUser();
        $distribution->addAmapien($amapien);
        $this->em->flush();

        redirect('/distribution/amapien');
    }

    public function desinscription(string $d_id)
    {
        $this->denyUnlessRequestMethod('POST');

        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER, $distribution);

        $amapien = $this->getUser();
        $distribution->removeAmapien($amapien);
        $this->em->flush();

        redirect('/distribution/amapien');
    }

    private function distributionMarkToRemove(PsrLib\ORM\Entity\AmapDistribution $distribution)
    {
        foreach ($distribution->getAmapiens() as $amapien) {
            $this->email_sender->envoyerDistributionDesinscriptionAmapAmapien($distribution, $amapien);
        }
        $this->em->remove($distribution);
    }

    /**
     * @return \PsrLib\ORM\Entity\AmapDistribution[]
     */
    private function parseDistributionIdsFromRequest(): array
    {
        $distributions = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapDistribution::class)
            ->findByMultipleIds(
                explode(',', $this->request->query->get('ids', ''))
            )
        ;
        if (0 === count($distributions)) {
            $this->exit_error();
        }

        return $distributions;
    }
}
