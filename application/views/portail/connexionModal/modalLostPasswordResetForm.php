<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Formulaire nouveau mot de passe */
if (isset($print_lost_password_reset_form) && true === $print_lost_password_reset_form) {
    ?>

    <div class="modal" tabindex="-1" role="dialog" id="lostpassword-reset-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <?= form_open('', ['class' => 'form-horizontal']); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Nouveau mot de passe</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="password_1" class="col-sm-3 control-label">Mot de passe *</label>
                        <div class="col-sm-9">
                            <?= form_password('password_1', set_value('password_1'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('password_1'); ?>

                    <div class="form-group">
                        <label for="lostpassword_email" class="col-sm-3 control-label">Confirmez le mot de passe *</label>
                        <div class="col-sm-9">
                            <?= form_password('password_2', set_value('password_2'), ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?= form_error('password_2'); ?>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Force du mot de passe</label>
                        <div class="col-sm-9">
                            <?= render_zxcvbn_progress_bar('password_1', site_url('portail/mdp_oublie_test_password/'.$token)); ?>
                            <p class="help-block">Le mot de passe doit avoir au moins une force de 2 sur 4 <br />
                                et au moins 8 caractères</p>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                </div>
                <?= form_close(); ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script>
      $('#lostpassword-reset-modal')
        .modal({
          show: true
        })
        // Redirect to connexion page on close
        .on('hidden.bs.modal', function () {
          window.location.href = '/portail/connexion';
        });


    </script>
    <?php
}
?>
