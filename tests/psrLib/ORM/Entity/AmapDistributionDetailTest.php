<?php

namespace Test\ORM\Entity;

use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 * @coversNothing
 */
class AmapDistributionDetailTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testValidateHourValidDiffrentHour()
    {
        $detail = AmapDistributionDetail::create(
            new Time(1, 10),
            new Time(2, 20),
            1,
            'test'
        );
        $violations = $this->validator->validate($detail);
        self::assertEmpty($violations);
    }

    public function testValidateHourValidSameHour()
    {
        $detail = AmapDistributionDetail::create(
            new Time(1, 10),
            new Time(1, 20),
            1,
            'test'
        );
        $violations = $this->validator->validate($detail);
        self::assertEmpty($violations);
    }

    public function testValidateHourInValidDiffrentHour()
    {
        $detail = AmapDistributionDetail::create(
            new Time(2, 10),
            new Time(1, 20),
            1,
            'test'
        );
        $violations = $this->validator->validate($detail);
        self::assertCount(1, $violations);
        self::assertSame('L\'heure de début doit être avant l\'heure de fin', $violations[0]->getMessage());
    }

    public function testValidateHourInvalidSameHour()
    {
        $detail = AmapDistributionDetail::create(
            new Time(1, 20),
            new Time(1, 10),
            1,
            'test'
        );
        $violations = $this->validator->validate($detail);
        self::assertCount(1, $violations);
        self::assertSame('L\'heure de début doit être avant l\'heure de fin', $violations[0]->getMessage());
    }
}
