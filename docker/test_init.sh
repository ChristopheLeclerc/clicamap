#!/usr/bin/env bash

chmod -R 777 $CI_PROJECT_DIR

composer install

ln -s $CI_PROJECT_DIR/* /var/www/html
ln -s $CI_PROJECT_DIR/.* /var/www/html

# Launch chrome
sudo -u chrome /opt/google/chrome/chrome --headless --disable-gpu --no-sandbox --window-size="1920,1080" --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222 http://localhost &

# Launch apache
apache2-foreground > /dev/null &
