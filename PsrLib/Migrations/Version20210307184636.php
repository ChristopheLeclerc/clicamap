<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184636 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_ville ENGINE InnoDb;');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63B3E7E790 FOREIGN KEY (adresse_admin_ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('ALTER TABLE ak_ferme ADD CONSTRAINT FK_AD7E64AEA73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('CREATE INDEX IDX_AD7E64AEA73F0036 ON ak_ferme (ville_id)');
        $this->addSql('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D860A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('CREATE INDEX IDX_CC11D860A73F0036 ON ak_paysan (ville_id)');
        $this->addSql('CREATE INDEX IDX_CC11D86018981132 ON ak_paysan (ferme_id)');
        $this->addSql('CREATE INDEX IDX_88EBF3AF72E220CB ON ak_ville (v_fk_dep_id)');

        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau MODIFY COLUMN pay_cr_fk_pay_id BIGINT;');
        $this->addSql('DELETE FROM ak_paysan_cadre_reseau WHERE pay_cr_fk_pay_id NOT IN (SELECT pay_id FROM ak_paysan)');
        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau ADD FOREIGN KEY (pay_cr_fk_pay_id) REFERENCES ak_paysan(pay_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
