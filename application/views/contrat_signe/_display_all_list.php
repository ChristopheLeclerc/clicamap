<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
/** @var \Symfony\Component\Form\FormView $searchForm */
/** @var \PsrLib\DTO\SearchContratSigneState $searchContratState */
$currentUser = get_current_connected_user();

if (!$searchContratState->isEmpty()) {
    ?>

    <p>

        <?php

        $mc = $searchContratState->getMc();
    echo '<div class="clearfix">';

    $modeleContrat = $mc;

    include '_info_modele_contrat.php';

    if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE, $mc)) {
        echo '<a href="'.site_url('contrat_signe/contrat_new_force/'.$mc->getId()).'" class="btn btn-success" title="Ajouter le contrat"><i class="glyphicon glyphicon-plus"></i> Ajouter le contrat d\'un amapien</a>&nbsp;';
    }

    if (count($contrats) > 0) {
        echo '<a href="#" class="btn btn-danger" disabled><i class="glyphicon glyphicon-remove-sign"></i> Supprimer le contrat vierge</a>&nbsp;';

        echo '<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_download"><i class="glyphicon glyphicon-download"></i> Télécharger</a>&nbsp;';

        if (!($currentUser instanceof \PsrLib\ORM\Entity\UserWithFermes)) {
            echo '<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_email"><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>&nbsp;';

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_MOVE_DATE, $mc)) {
                echo '<a href="'.site_url('contrat_signe/move/'.$mc->getId()).'" class="btn btn-primary"><i class="glyphicon glyphicon-calendar"></i> Déplacer une date de livraison</a>&nbsp;';
            }

            echo '<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_autre"><i class="glyphicon glyphicon-cog"></i> Autre</a>';
        }

        echo '</div>';
    } else {
        echo '<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal_remove_mc"><i class="glyphicon glyphicon-remove-sign"></i> Supprimer le contrat vierge</a>&nbsp;';

        // echo '<a href="#" class="btn btn-info" disabled><i class="glyphicon glyphicon-inf-sign"></i> Infos contrat vierge</a>&nbsp;';

        echo '<div class="clearfix">';

        echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-download"></i> Télécharger</a>&nbsp;';

        if (!($currentUser instanceof \PsrLib\ORM\Entity\UserWithFermes)) {
            echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>&nbsp;';
            echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-calendar"></i> Déplacer une date de livraison</a>&nbsp;';
            echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-cog"></i> Autre</a>';
        }

        echo '</div>';
    } ?>

    </p>

    <!-- Modal remove -->
    <div class="modal fade text-left" id="modal_remove_mc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer le contrat vierge ?</h5>
                </div>
                <div class="modal-body">
                    <p class="text-right">
                        <a href="<?= site_url('contrat_signe/mc_remove/'.$mc->getId()); ?>"
                           class="btn btn-danger btn-sm">OUI</a>
                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal autre -->
    <div class="modal fade" id="modal_autre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Autres actions sur les contrats signés</h4>
                </div>
                <div class="modal-body">
                    <h5>Veuillez indiquer ce que vous souhaitez modifier :</h5>
                    <!-- <p><a href="<?= site_url('contrat_signe/move'); ?>">Déplacer une date de livraison</a></p> -->
                    <p><a href="<?= site_url('contrat_signe/counter/'.$mc->getId()); ?>">Mettre à zéro les quantités commandées
                            sur une ou plusieurs dates de livraison</a></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal email -->
    <div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Comment envoyer un email à tous les amapiens de ce contrat
                        ?</h4>
                </div>
                <div class="modal-body">
                    <h5>Pour envoyer un email à tous les adhérents de ce contrat, vous devez :</h5>
                    <ul>
                        <li>Faire un copier de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Copier sur la zone bleue ci dessous
                        </li>
                        <li>Ouvrir votre outil favori pour l'envoi des mails (Thunderbird, Gmail, Outlook...)</li>
                        <li>Faire nouveau message</li>
                        <li>Faire un coller de toutes les adresses e-mail en faisant Ctrl+C ou en faisant clic droit +
                            Coller dans la liste des destinataires du message.
                        </li>
                    </ul>

                    <form>
                        <div class="form-group">
                            <textarea class="form-control" rows="5"><?php

                                $emails = [];
    foreach ($contrats as $contrat) {
        $emails[] = $contrat->getAmapien()->getEmail();
    }
    echo implode(PHP_EOL, $emails); ?> </textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal download -->
    <div class="modal fade" id="modal_download" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Télécharger</h4>
                </div>
                <div class="modal-body">
                    <h5>Veuillez cliquer sur le lien du fichier que vous souhaitez télécharger :</h5>

                    <?php

                    echo '<p><a href="'.site_url('contrat_signe/telecharger_feuilles_distribution/'.$mc->getId()).'">Toutes les feuilles de livraisons</a></p>';

    echo '<p><a href="'.site_url('contrat_signe/telecharger_liasse_contrats/'.$mc->getId()).'">La liasse des contrats signés</a></p>';

    if (!($currentUser instanceof \PsrLib\ORM\Entity\UserWithFermes)) {
        echo '<p><a href="'.site_url('contrat_signe/telecharger_souscripteurs/'.$mc->getId()).'">La liste des souscripteurs de ce contrat, avec leur email et téléphone</a></p>';
    }

    if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY, $mc)) {
        echo '<p><a href="'.site_url('contrat_signe/telecharger_synthese_contrat/'.$mc->getId()).'">Synthèse des paiements</a></p>';
    } ?>

                </div>
            </div>
        </div>
    </div>

    <?php

    if (count($contrats) > 0) {
        ?>

        <div class="table-responsive">
            <table class="table table-hover table-datatable">
                <thead class="thead-inverse">

                <tr>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Date de création</th>
                    <th>Date de modification</th>
                    <th class="text-right">Outils</th>
                </tr>
                </thead>

                <tbody>

                <?php

                $contrat_all_nbr = count($contrats);
        if (1 == $contrat_all_nbr) {
            echo '<div class="alert alert-success">1 contrat signé :</div>';
        } else {
            echo '<div class="alert alert-success">'.$contrat_all_nbr.' contrats signés pour cette recherche :</div>';
        }

        foreach ($contrats as $contrat) {
            echo '<tr>';

            echo '<th scope="row">';
            echo strtoupper($contrat->getAmapien()->getNom());
            echo '</th>';

            echo '<td>';
            echo ucfirst($contrat->getAmapien()->getPrenom());
            echo '</td>';

            echo '<td data-order="'.$contrat->getDateCreation()->format('U').'">';
            echo date_format_french_hour($contrat->getDateCreation());
            echo '</td>';

            echo '<td data-order="'.(null === $contrat->getDateModification() ? '' : $contrat->getDateModification()->format('U')).'">';
            if ($contrat->getDateModification()) {
                echo date_format_french_hour($contrat->getDateModification());
            } else {
                echo 'Le contrat n\'a pas encore été modifié';
            }
            echo '</td>';

            echo '<td class="text-right">';

            /*if($this->session->ref_prod)
                $redirection = '/1';

            else
                $redirection = "";*/

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_DISPLAY, $contrat)) {
                echo '<a href="'.site_url('/contrat_signe/telecharger_contrat/'.$contrat->getId()).'"><i class="glyphicon glyphicon-list-alt" data-toggle="tooltip" title="Télécharger le contrat (excel)"></i></a> ';
            }

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EXPORT_PDF, $contrat)) {
                echo '<a href="'.site_url('/contrat_signe/contrat_pdf/'.$contrat->getId()).'"><i class="glyphicon glyphicon-file" data-toggle="tooltip" title="Télécharger le contrat juridique (pdf)"></i></a>';
            } ?>

                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_MOVE_DATE, $contrat)): ?>
                        <a href="<?= site_url('/contrat_signe/contrat_deplacement/'.$contrat->getId()); ?>">
                            <i class="glyphicon glyphicon-calendar" data-toggle="tooltip" data-placement="top"
                               title="Déplacer des dates de livraison"></i>
                        </a>
                    <?php endif; ?>

                    <?php if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_CONTRACT_SIGNED_EDIT_FORCE, $contrat)) { ?>
                        <a href="<?= site_url('/contrat_signe/contrat_edit/'.$contrat->getId().'?force'); ?>">
                            <i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier le contrat"></i>
                        </a>

                        <a title="Supprimer le contrat" href="#" data-toggle="modal"
                           data-target="#modal_remove_<?= $contrat->getId(); ?>">
                            <i class="glyphicon glyphicon-remove" data-toggle="tooltip"
                               title="Supprimer le contrat"></i>
                        </a>

                        <!-- Modal remove -->
                        <div class="modal fade text-left" id="modal_remove_<?= $contrat->getId(); ?>" tabindex="-1"
                             role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer le
                                            contrat de
                                            "<?= strtoupper($contrat->getAmapien()->getPrenom()).' '.strtoupper($contrat->getAmapien()->getNom()); ?>
                                            "</h5>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-right">
                                            <a href="<?= site_url('contrat_signe/remove/'.$contrat->getId()); ?>"
                                               class="btn btn-danger btn-sm">OUI</a>
                                            <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    </td>
                    </tr>

                    <?php
        } ?>

                </tbody>
            </table>
        </div>

        <?php
    } else {
        echo '<div class="alert alert-warning">Aucun contrat n\'a encore été créé.</div></div>';
    }
} else {
    echo '<p>';

    // if( ! $this->session->paysan && ! $this->session->ref_prod)
    //   echo '<a href="'.site_url('contrat_signe/creation').'" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Ajouter le contrat d\'un amapien</a>&nbsp;';
    //
    // else

    // echo '<a href="#" class="btn btn-info" disabled><i class="glyphicon glyphicon-info-sign"></i> Infos contrat vierge</a>&nbsp;';

    echo '<div class="clearfix">';

    echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-download"></i> Télécharger</a>&nbsp;';

    if (!$this->session->paysan) {
        echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-envelope"></i> Emails de la liste</a>&nbsp;';
        echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-calendar"></i> Déplacer une date de livraison</a>&nbsp;';
        echo '<a href="#" class="btn btn-primary" disabled><i class="glyphicon glyphicon-cog"></i> Autre</a>';
    }

    echo '</div>';
    echo '</p>';
}
