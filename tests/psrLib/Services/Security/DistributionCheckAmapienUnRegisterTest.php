<?php

namespace Test\Services\Security;

use Carbon\Carbon;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class DistributionCheckAmapienUnRegisterTest extends AbstractSecurityTest
{
    public function testUnRegisterAsPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        $distribution = $this->prophesize(AmapDistribution::class)->reveal();
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER, $distribution));
    }

    public function testUnRegisterAsAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        $distribution = $this->prophesize(AmapDistribution::class)->reveal();
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER, $distribution));
    }

    public function testUnRegisterAsAmapienValid()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            10,
            new Carbon('2000-01-01')
        );
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->mockAuthenticatedUser($amapien);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER, $distribution));
    }

    public function testUnRegisterAsAmapienNotRegistered()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            10,
            new Carbon('2000-01-01')
        );
        $distribution->removeAmapien($amapien);

        $this->mockAuthenticatedUser($amapien);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER, $distribution));
    }

    public function testUnRegisterAsAmapienAfterStart()
    {
        $amapien = $this->buildAmapien();
        $distribution = $this->buildDistribution(
            $amapien,
            new Time(12, 00),
            new Time(13, 00),
            10,
            new Carbon('2000-01-01')
        );
        Carbon::setTestNow('2000-01-01 12:00:00');

        $this->mockAuthenticatedUser($amapien);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER, $distribution));
    }

    private function buildDistribution(Amapien $amapien, Time $heureDebut, Time $heureFin, int $nbPersonnes, Carbon $date)
    {
        $distribution = new AmapDistribution(
            AmapDistributionDetail::create($heureDebut, $heureFin, $nbPersonnes, 'tache'),
            $amapien->getAmap()->getLivraisonLieux()->first(),
            $date
        );
        $distribution->addAmapien($amapien);

        return $distribution;
    }

    private function buildAmapien()
    {
        $amapien = new Amapien();
        $ll = new AmapLivraisonLieu();
        $amap = new Amap();
        $amap->addLivraisonLieux($ll);
        $ll->setAmap($amap);
        $amapien->setAmap($amap);

        return $amapien;
    }
}
