<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class FermeCheckDisplayTest extends AbstractSecurityTest
{
    public function testDisplayRegroupementFermeValid()
    {
        $regroupement = new FermeRegroupement();
        $ferme1 = new Ferme();
        $ferme1->setRegroupement($regroupement);

        $this->mockAuthenticatedUser($regroupement);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_DISPLAY, $ferme1));
    }

    public function testDisplayRegroupementFermeInvalid()
    {
        $regroupement = new FermeRegroupement();
        $ferme1 = new Ferme();
        $ferme1->setRegroupement($regroupement);

        $ferme2 = new Ferme();

        $this->mockAuthenticatedUser($regroupement);
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_FERME_DISPLAY, $ferme2));
    }
}
