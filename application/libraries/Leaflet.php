<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * CodeIgniter Leaflet Js Class.
 *
 * @category	Libraries
 *
 * @author		anggriyulio (Anggri Yulio P)
 *
 * @see		http://anggriyulio.com/
 */
class Leaflet
{
    public $tileLayer = 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png';
    public $attribution = '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>';
    // Map State Options
    public $center = '-0.959, 100.39716';
    public $zoom = '13';
    public $layers = '';
    public $map_name = 'map';
    public $minZoom = '';
    public $maxZoom = '';
    public $user = '';
    public $crs = 'L.CRS.EPSG3857';
    // Interaction options
    public $dragging = true;
    public $touchZoom = true;
    public $scrollWheelZoom = true;
    public $doubleClickZoom = true;
    public $boxZoom = true;
    public $tap = true;
    public $tapTolerance = 15;
    public $trackResize = true;
    public $worldCopyJump = false;
    public $closePopupOnClick = true;
    public $bounceAtZoomLimits = true;
    // Control options
    public $zoomControl = true;
    public $attributionControl = true;
    // Events
    public $click = '';
    public $dblclick = '';
    public $mousedown = '';
    public $mouseup = '';
    public $mouseover = '';
    public $mouseout = '';
    public $mousemove = '';
    public $press = '';
    public $focus = '';
    public $blur = '';
    public $existing = '';
    public $load = '';
    public $unload = '';
    public $creating = '';
    public $movestart = '';
    public $move = '';
    public $moveend = '';
    public $dragstart = '';
    public $drag = '';
    public $dragend = '';
    public $zoomstart = '';
    public $zoomend = '';
    public $zoomlevelschange = '';
    public $resize = '';
    public $autopanstart = '';
    public $layeradd = '';
    public $layerremove = '';
    public $baselayerchange = '';
    public $overlayadd = '';
    public $overlayremove = '';
    public $locationfound = '';
    public $locationerror = '';
    public $popupopen = '';
    public $popupclose = '';
    public $customFunction = '';
    public $markers = [];
    protected $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    public function leaflet($config = [])
    {
        if (count($config) > 0) {
            $this->initialize($config);
        }
    }

    public function initialize($config = [])
    {
        foreach ($config as $key => $val) {
            if (isset($this->{$key})) {
                $this->{$key} = $val;
            }
        }
    }

    public function add_marker($params = [])
    {
        $marker = [];
        //$this->markersInfo['marker_'.count($this->markers)] = array();
        $marker['latlng'] = '-0.9583407792361563,100.3982162475586';
        $marker['icon'] = '';
        $marker['clickable'] = true;
        $marker['draggable'] = false;
        $marker['keyboard'] = true;
        $marker['title'] = '';
        $marker['alt'] = '';
        $marker['zIndexOffset'] = 0;
        $marker['opacity'] = 1.0;
        $marker['riseOnHover'] = false;
        $marker['riseOffset'] = 250;
        // Marker Event
        $marker['dragend'] = '';
        $marker['customFunction'] = '';
        $marker['iconColor'] = '';
        $marker['spin'] = false;
        $marker['extraClasses'] = '';
        $marker['popupContent'] = '';
        // Marker Icon
        $marker['customicon'] = false;
        $marker['iconUrl'] = '';
        $marker['iconRetinaUrl'] = '';
        $marker['iconSize'] = '[20,20]';
        $marker['iconAnchor'] = '';
        $marker['popupAnchor'] = '';
        $marker['shadowUrl'] = '';
        $marker['shadowRetinaUrl'] = '';
        $marker['shadowSize'] = '';
        $marker['shadowAnchor'] = '';
        $marker['className'] = 'icon-marker';
        $marker_output = '';
        foreach ($params as $key => $value) {
            if (isset($marker[$key])) {
                $marker[$key] = $value;
            }
        }
        // Create the marker
        $marker_output .= 'marker = new L.marker(['.$marker['latlng'].'],({';
        // Start of marker options
        if (!$marker['clickable']) {
            $marker_output .= 'clickable: false,';
        }
        if (false == !$marker['draggable']) {
            $marker_output .= 'draggable: true,';
        }
        if (!$marker['keyboard']) {
            $marker_output .= '"keyboard":false,';
        }
        if ($marker['title']) {
            $marker_output .= '"title":"'.$marker['title'].'",';
        }
        if ($marker['alt']) {
            $marker_output .= '"alt":"'.$marker['alt'].'",';
        }
        if ($marker['zIndexOffset']) {
            $marker_output .= '"zIndexOffset":'.$marker['zIndexOffset'].',';
        }
        if (!$marker['opacity']) {
            $marker_output .= '"opacity":'.$marker['opacity'].',';
        }
        if ($marker['riseOnHover']) {
            $marker_output .= '"riseOnHover":true,';
        }
        if (!$marker['riseOffset']) {
            $marker_output .= '"riseOffset":'.$marker['riseOffset'].',';
        }
        if ($marker['extraClasses']) {
            $marker_output .= '"extraClasses" : "'.$marker['extraClasses'].'",';
        }
        // Custom Marker Icon
        if (true == $marker['customicon']) {
            $marker_output .= 'icon: L.icon({';
            $marker_output .= 'iconUrl: "'.$marker['iconUrl'].'",';
            if ('' == !$marker['iconRetinaUrl']) {
                $marker_output .= 'iconRetinaUrl: "'.$marker['iconRetinaUrl'].'",';
            }
            if ('' == !$marker['iconSize']) {
                $marker_output .= 'iconSize: '.$marker['iconSize'].',';
            }
            if ('' == !$marker['iconAnchor']) {
                $marker_output .= 'iconAnchor: '.$marker['iconAnchor'].',';
            }
            if ('' == !$marker['popupAnchor']) {
                $marker_output .= 'popupAnchor: '.$marker['popupAnchor'].',';
            }
            if ('' == !$marker['shadowUrl']) {
                $marker_output .= 'shadowUrl: "'.$marker['shadowUrl'].'",';
            }
            if ('' == !$marker['shadowRetinaUrl']) {
                $marker_output .= 'shadowRetinaUrl: "'.$marker['shadowRetinaUrl'].'",';
            }
            if ('' == !$marker['shadowSize']) {
                $marker_output .= 'shadowSize: '.$marker['shadowSize'].',';
            }
            if ('' == !$marker['shadowAnchor']) {
                $marker_output .= 'shadowAnchor: '.$marker['shadowAnchor'].',';
            }
            if ('' == !$marker['className']) {
                $marker_output .= 'className: "'.$marker['className'].'",';
            }
            $marker_output .= '}),';
        }
        // End of Custom icon
        // End of marker options
        $marker_output .= '}))';
        if ('' != $marker['popupContent']) {
            $marker_output .= '.bindPopup("'.$marker['popupContent'].'")';
        }
        $marker_output .= '.addTo(map);';
        if ('' != $marker['dragend']) {
            $marker_output .= 'marker.on("dragend", '.$marker['dragend'].');';
        }
        if ('' != $marker['customFunction']) {
            $marker_output .= $marker['customFunction'];
        }
        array_push($this->markers, $marker_output);
    }

    public function create_map()
    {
        $this->output_js = '';
        $this->output_js_contents = '';
        $this->output_html = '';
        $this->output_html .= '<div id="map" style="width:100%; height:400px;"></div>';
        $this->output_js .= '
			<script type="text/javascript">
			 $(document).ready(function() {
			';
        $this->output_js_contents .= '
			var map = L.map("map",{
				center: ['.$this->center.'],
				zoom: '.$this->zoom.',
				dragging: '.$this->dragging.'
			})
			';
        $this->output_js_contents .= '
			L.tileLayer("'.$this->tileLayer.'", {';
        $this->output_js_contents .= "attribution: '{$this->attribution}'";
        $this->output_js_contents .= '
			}).addTo(map)
			';
        if ('' != $this->customFunction) {
            $this->output_js_contents .= $this->customFunction;
        }
        if ('' != $this->click) {
            $this->output_js_contents .= '
			 '.$this->map_name.'.on("click",'.$this->click.');
			';
        }
        /*
        * Add marker.
        * @uses add_marker
        */
        if (count($this->markers)) {
            foreach ($this->markers as $marker) {
                $this->output_js_contents .= $marker;
            }
        }
        $this->output_js .= $this->output_js_contents;
        $this->output_js .= '
		  	});
		';
        $this->output_js .= '</script>';

        return ['js' => $this->output_js, 'html' => $this->output_html];
    }
}
// End of file leaflet.php
// Location: ./application/libraries/leaflet.php
