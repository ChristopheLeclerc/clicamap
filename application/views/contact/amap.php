<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap[] $amaps */
/** @var \Symfony\Component\Form\FormView $form */
?>
<h3>Contacter une AMAP</h3>

<?php if (count($amaps) > 0) : ?>

    <div class="table-responsive">
        <table class="table">

            <tbody>

            <?php

            foreach ($amaps as $amap) {
                echo '<tr>';
                echo '<th>'.$amap->getNom().'</th>';
                echo '<td><a href="#" data-toggle="modal" data-target="#modal_envoi_'.$amap->getId().'" ><button type=button class="btn btn-primary btn-sm pull-right" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Email</button></a>'; ?>

                <div class="modal fade text-left" id="modal_envoi_<?= $amap->getId(); ?>" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title" id="myModalLabel">L'email sera envoyé uniquement à l'AMAP et au(x)
                                    référent(s) de la ferme.</h5>
                            </div>
                            <div class="modal-body">

                                <form method="POST" action="">

                                    <?= render_form_hidden_widget($form['targetId'], $amap->getId()); ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">

        <span class="input-group-btn">
        <button class="btn btn-primary" type="button">Objet</button>
        </span>

                                                <div class="form-group">
                                                    <?= render_form_text_widget($form['title']); ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <?= render_form_textarea_widget($form['content']); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="submit" value="Envoyer" class="pull-right btn btn-success"/>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                </td>
                </tr>

                <?php
            }

        echo '</tbody>';
        echo '</table>';
        echo '</div>';
        ?>
<?php else: ?>
        Aucun contact
<?php endif; ?>
