<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * AkContratDatesReglement.
 *
 * @ORM\Table(name="ak_contrat_dates_reglement")
 * @ORM\Entity
 */
class ContratDatesReglement
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"wizardContract"})
     */
    private $id;

    /**
     * @var Contrat | null
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Contrat", inversedBy="datesReglements")
     * @ORM\JoinColumn(name="fk_contrat", referencedColumnName="c_id")
     */
    private $contrat;

    /**
     * @var ModeleContratDatesReglement | null
     *
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\ModeleContratDatesReglement")
     * @ORM\JoinColumn(name="fk_modele_contrat_dates_reglement", referencedColumnName="id")
     * @Groups({"wizardContract"})
     */
    private $modeleContratDatesReglement;

    /**
     * @var ?Money
     *
     * @ORM\Column(name="montant", type="money")
     * @Groups({"wizardContract"})
     */
    private $montant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContrat(): ?Contrat
    {
        return $this->contrat;
    }

    public function setContrat(?Contrat $contrat): ContratDatesReglement
    {
        $this->contrat = $contrat;

        return $this;
    }

    public function getModeleContratDatesReglement(): ?ModeleContratDatesReglement
    {
        return $this->modeleContratDatesReglement;
    }

    public function setModeleContratDatesReglement(?ModeleContratDatesReglement $modeleContratDatesReglement): ContratDatesReglement
    {
        $this->modeleContratDatesReglement = $modeleContratDatesReglement;

        return $this;
    }

    public function getMontant(): ?Money
    {
        return $this->montant;
    }

    public function setMontant(?Money $montant): ContratDatesReglement
    {
        $this->montant = $montant;

        return $this;
    }
}
