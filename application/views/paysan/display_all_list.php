<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-inverse">
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Email</th>
            <th>Mot de passe</th>
            <th>Ferme</th>
            <th class="text-right">Outils</th>
        </tr>
        </thead>

        <tbody>

        <?php

        foreach ($paysans as $paysan) {
            ?>
            <tr>

                <th scope="row">

                    <?php
                    if (strlen($paysan->getNom()) > 25) {
                        echo substr(mb_strtoupper($paysan->getNom()), 0, 15).'...';
                    } else {
                        echo mb_strtoupper($paysan->getNom());
                    } ?>
                </th>

                <td>
                    <?php
                    if (strlen($paysan->getPrenom()) > 20) {
                        echo substr(mb_strtoupper($paysan->getPrenom()), 0, 15).'...';
                    } else {
                        echo mb_strtoupper($paysan->getPrenom());
                    }
            echo '</td>';

            echo '<td><a href="mailto:'.$paysan->getEmail().'" >';
            if (strlen($paysan->getEmail()) > 35) {
                echo substr(mb_strtolower($paysan->getEmail()), 0, 30).'...';
            } else {
                echo mb_strtolower($paysan->getEmail());
            }
            echo '</a></td>';

            echo '<td>';
            if (null !== $paysan->getPassword()) {
                echo 'Oui';
            } else {
                echo 'Non';
            }
            echo '</td>';

            echo '<td>';
            $ferme = $paysan->getFerme();
            if (null !== $ferme) {
                echo '<a href="'.base_url('ferme/display/'.$ferme->getId()).'" >';
                if (strlen($paysan->getNom()) > 40) {
                    echo substr(mb_strtolower($ferme->getNom()), 0, 35).'...';
                } else {
                    echo mb_strtolower($ferme->getNom());
                }
                echo '</a>';
            }
            echo '</td>';

            echo '<td class="text-right">';
            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan)) {
                if (\PsrLib\ORM\Entity\Paysan::ETAT_INACTIF === $paysan->getEtat()) {
                    echo '<a title="Activer le profil" href="'.site_url('paysan/de_activate_paysan/'.$paysan->getId()).'"><i class="glyphicon glyphicon-play" data-toggle="tooltip" title="Activer le profil"></i></a>&nbsp;';
                } else {
                    echo '<a title="Desactiver le profil" href="'.site_url('paysan/de_activate_paysan/'.$paysan->getId()).'"><i class="glyphicon glyphicon-pause" data-toggle="tooltip" title="Desactiver le profil"></i></a>&nbsp;';
                }
            }
            echo '<a title="Détails du profil" href="'.site_url('paysan/display/'.$paysan->getId()).'"><i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" title="Détails du profil"></i></a>&nbsp;';

            echo '<a title="Modifier le profil" href="'.site_url('paysan/informations_generales/'.$paysan->getId()).'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifier le profil"></i></a>&nbsp;';

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_CHANGE_CR, $paysan)) {
                echo '<a title="Recherche d\'AMAP" href="'.site_url('paysan/recherche_amap/'.$paysan->getId()).'"><i class="glyphicon glyphicon-search" data-toggle="tooltip" title="Recherche d\'AMAP"></i></a>&nbsp;';
            }

            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan)) {
                echo '<a title="Envoyer un mot de passe au paysan" href="#" data-toggle="modal" data-target="#modal_key_'.$paysan->getId().'" ><i class="glyphicon glyphicon-lock" data-toggle="tooltip" title="Envoyer un mot de passe au paysan"></i></a>&nbsp;';
            }

            // Pas d'accès pour les référents produits
            if (is_granted(\PsrLib\Services\Security\SecurityChecker::ACTION_PAYSAN_CHANGE_CR, $paysan)) {
                echo '<a title="Supprimer le profil" href="#" data-toggle="modal" data-target="#modal_'.$paysan->getId().'"><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Supprimer le profil"></i></a>&nbsp;';
            } ?>

                    <!-- Modal mot de passe -->
                    <div class="modal fade text-left" id="modal_key_<?= $paysan->getId(); ?>" tabindex="-1"
                         role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment envoyer un mot de
                                        passe
                                        au
                                        paysan <?= strtoupper($paysan->getNom()).' '.ucfirst($paysan->getPrenom()); ?>
                                        ?</h5>
                                </div>
                                <div class="modal-body">
                                    <p>Cette action créera un mot de passe au paysan (même s'il en a déjà un) qui
                                        lui sera
                                        envoyé par email ; de plus, son profil sera activé.</p>
                                    <p class="text-right">
                                        <a href="<?= site_url('paysan/password/'.$paysan->getId()); ?>"
                                           class="btn btn-danger btn-sm">OUI</a>
                                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal Remove -->
                    <div class="modal fade text-left" id="modal_<?= $paysan->getId(); ?>" tabindex="-1"
                         role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">Voulez-vous vraiment supprimer le
                                        profil de
                                        "<?= strtoupper($paysan->getNom()).' '.ucfirst($paysan->getPrenom()); ?>
                                        "</h5>
                                </div>
                                <div class="modal-body">
                                    <p class="text-right">
                                        <a href="<?= site_url('paysan/remove_paysan/'.$paysan->getId()); ?>"
                                           class="btn btn-danger btn-sm" title="supprimer le profil du paysan">OUI</a>
                                        <a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </td>
            </tr>

            <?php
        } ?>

        </tbody>
    </table>
</div>
