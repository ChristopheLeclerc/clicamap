<?php

namespace PsrLib\Services\ServiceFactories;

use PsrLib\ProjectLocation;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;

class TranslatorFactory
{
    public function create(): Translator
    {
        $translator = new Translator('fr');
        $translator->addLoader('xlf', new XliffFileLoader());
        $translator->addResource('xlf', ProjectLocation::PROJECT_ROOT.'vendor/symfony/validator/Resources/translations/validators.fr.xlf', 'fr');

        return $translator;
    }
}
