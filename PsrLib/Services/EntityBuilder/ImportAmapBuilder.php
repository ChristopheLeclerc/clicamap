<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapAnneeAdhesion;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonHoraire;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\ORM\Repository\VilleRepository;

class ImportAmapBuilder
{
    /**
     * @var VilleRepository
     */
    private $villeRepo;

    /**
     * @var AmapienRepository
     */
    private $amapienRepo;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        $this->villeRepo = $this->em->getRepository(Ville::class);
        $this->amapienRepo = $this->em->getRepository(Amapien::class);
    }

    /**
     * @return Amap[]
     */
    public function buildAmapsFromImport(array $entete, array $valeurs)
    {
        $amaps = [];
        foreach ($valeurs as $valeur) {
            $amap = new Amap();
            $amap->setEtat(Amap::ETAT_CREATION);
            $amap->setPossedeAssurance(false);
            $amap->setAmapEtudiante(false);

            $livraison_lieu = new AmapLivraisonLieu();
            $livraison_lieu->setAmap($amap);
            $amap->addLivraisonLieux($livraison_lieu);
            $contact_referent_1 = new Amapien();
            $contact_referent_2 = new Amapien();
            $livraison_horaireEte = new AmapLivraisonHoraire($livraison_lieu);
            $livraison_horaireHiver = new AmapLivraisonHoraire($livraison_lieu);
            $livraison_lieu->addLivraisonHoraire($livraison_horaireEte);
            $livraison_lieu->addLivraisonHoraire($livraison_horaireHiver);

            $llVille = $this->villeRepo->findOneBy([
                'cp' => trim($valeur['D']),
                'nom' => trim($valeur['E']),
            ]);
            $livraison_lieu->setVille($llVille);

            $ref1Ville = $this->villeRepo->findOneBy([
                'cp' => trim($valeur['BA']),
                'nom' => trim($valeur['BB']),
            ]);
            $contact_referent_1->setVille($ref1Ville);

            $amapVille = $this->villeRepo->findOneBy([
                'cp' => trim($valeur['BH']),
                'nom' => trim($valeur['BI']),
            ]);
            $amap->setAdresseAdminVille($amapVille);

            foreach ($valeur as $k => $v) {
                if ($v) {
                    $v = trim($v); // trim() supprime les espaces
                    // Nom
                    if ('A' == $k) {
                        $amap->setNom($v);
                    }

                    // LIVRAISON -------------------------------------------------------
                    // Lieu
                    if ('B' == $k) {
                        $livraison_lieu->setNom($v);
                    }
                    // Adresse
                    if ('C' == $k) {
                        $livraison_lieu->setAdresse($v);
                    }
                    // GPS Latitude
                    if ('lat' == $k) {
                        $livraison_lieu->setGpsLatitude($v);
                    }
                    // GPS Longitude
                    if ('long' == $k) {
                        $livraison_lieu->setGpsLongitude($v);
                    }

                    // HORAIRES --------------------------------------------------------
                    if ('F' == $k) {
                        $livraison_horaireEte->setJour($v);
                        $livraison_horaireHiver->setJour($v);
                    }
                    // Été : horaires
                    if ('G' == $k) {
                        $v = str_replace('–', '-', $v);
                        $heure = explode('-', $v);

                        // Heure de début
                        $h_deb = trim($heure[0]);
                        $h_deb = str_replace('h', ':', $h_deb);
                        if (3 == strlen($h_deb)) { // Exemple : 18: au lieu de 18:00
                            $h_deb = $h_deb.'00';
                        }
                        // Heure de fin
                        $h_fin = trim($heure[1]);
                        $h_fin = str_replace('h', ':', $h_fin);
                        if (3 == strlen($h_fin)) { // Exemple : 18: au lieu de 18:00
                            $h_fin = $h_fin.'00';
                        }

                        $livraison_horaireEte->setHeureDebut($h_deb);
                        $livraison_horaireEte->setHeureFin($h_fin);
                    }
                    // Hiver : horaires
                    if ('H' == $k) {
                        $v = str_replace('–', '-', $v);
                        $heure = explode('-', $v);

                        $h_deb = trim($heure[0]);
                        $h_deb = str_replace('h', ':', $h_deb);
                        if (3 == strlen($h_deb)) { // Exemple : 18: au lieu de 18:00
                            $h_deb = $h_deb.'00';
                        }

                        $h_fin = trim($heure[1]);
                        $h_fin = str_replace('h', ':', $h_fin);
                        if (3 == strlen($h_fin)) { // Exemple : 18: au lieu de 18:00
                            $h_fin = $h_fin.'00';
                        }

                        $livraison_horaireHiver->setHeureDebut($h_deb);
                        $livraison_horaireHiver->setHeureFin($h_fin);
                    }

                    // Email public
                    if ('I' == $k) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("mailto:', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0]);
                        }

                        $amap->setEmail($v);
                    }
                    // Adresse Site Web ou Blog
                    if ('J' == $k) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0]);
                        }
                        $amap->setUrl($v);
                    }

                    // PRODUITS PROPOSÉS
                    // Légumes
                    if ('K' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 1));
                    }
                    // Fruits
                    if ('L' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 2));
                    }
                    // Vache
                    if ('M' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 4));
                    }
                    // Chèvre
                    if ('N' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 5));
                    }
                    // Brebis
                    if ('O' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 6));
                    }
                    // Vin
                    if ('P' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 7));
                    }
                    // Œufs
                    if ('Q' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 8));
                    }
                    // Pain
                    if ('R' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 9));
                    }
                    // Pisciculture
                    if ('S' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 10));
                    }
                    // Produits de la mer
                    if ('T' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 11));
                    }
                    // Viande de porc
                    if ('U' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 13));
                    }
                    // Viande de volaille
                    if ('V' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 14));
                    }
                    // Viande d'agneau
                    if ('W' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 15));
                    }
                    // Viande bœuf
                    if ('X' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 16));
                    }
                    // Viande autre
                    if ('Y' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 17));
                    }
                    // Miel
                    if ('Z' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 18));
                    }
                    // Autre / divers
                    if ('AA' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpPropose($this->em->getReference(TypeProduction::class, 19));
                    }

                    // PRODUITS RECHERCHÉS
                    // Légumes
                    if ('AB' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 1));
                    }
                    // Fruits
                    if ('AC' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 2));
                    }
                    // Vache
                    if ('AD' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 4));
                    }
                    // Chèvre
                    if ('AE' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 5));
                    }
                    // Brebis
                    if ('AF' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 6));
                    }
                    // Vin
                    if ('AG' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 7));
                    }
                    // Œufs
                    if ('AH' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 8));
                    }
                    // Pain
                    if ('AI' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 9));
                    }
                    // Pisciculture
                    if ('AJ' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 10));
                    }
                    // Produits de la mer
                    if ('AK' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 11));
                    }
                    // Viande de porc
                    if ('AL' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 13));
                    }
                    // Viande de volaille
                    if ('AM' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 14));
                    }
                    // Viande d'agneau
                    if ('AN' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 15));
                    }
                    // Viande bœuf
                    if ('AO' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 16));
                    }
                    // Viande autre
                    if ('AP' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 17));
                    }
                    // Miel
                    if ('AQ' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 18));
                    }
                    // Autre / divers
                    if ('AR' == $k && ('o' == $v || 'O' == $v)) {
                        $amap->addTpRecherche($this->em->getReference(TypeProduction::class, 19));
                    }

                    // Date de Création
                    if ('AS' == $k) {
                        $amap->setAnneeCreation($v);
                    }
                    // Nombre d'Adhérents
                    if ('AT' == $k) {
                        $amap->setNbAdherents($v);
                    }
                    // Adhérent Année
                    if ('AU' == $k) {
                        $annee = explode('|', $v);
                        foreach ($annee as $a) {
                            $adhesion = new AmapAnneeAdhesion((int) trim($a), $amap);
                            $amap->addAnneeAdhesion($adhesion);
                        }
                    }
                    // CONTACT RÉFÉRENT RÉSEAU
                    // Prénom
                    if ('AV' == $k) {
                        $contact_referent_1->setPrenom($v);
                    }
                    // Nom
                    if ('AW' == $k) {
                        $contact_referent_1->setNom($v);
                    }

                    // Téléphone
                    if ('AX' == $k) {
                        if (strpos($v, '|')) {
                            $v_array = explode('|', $v);
                            $contact_referent_1->setNumTel1($v_array[0]);
                            $contact_referent_1->setNumTel1($v_array[1]);
                        } else {
                            $contact_referent_1->setNumTel1($v);
                        }
                    }
                    // Email
                    if ('AY' == $k) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("mailto:', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0]);
                        }
                        $contact_referent_1->setEmail($v);
                    }
                    // Adresse
                    if ('AZ' == $k) {
                        $contact_referent_1->setLibAdr1($v);
                    }

                    // CONTACT RÉFÉRENT RÉSEAU SECONDAIRE
                    // Prénom
                    if ('BC' == $k) {
                        $contact_referent_2->setPrenom($v);
                    }
                    // Nom
                    if ('BD' == $k) {
                        $contact_referent_2->setNom($v);
                    }
                    // Téléphone
                    if ('BE' == $k) {
                        $v_array = explode('|', $v);
                        // Si il y a plusieurs numéro
                        if (count($v_array) > 1) {
                            $contact_referent_2->setNumTel1(trim($v_array[0]));
                            $contact_referent_2->setNumTel2(trim($v_array[1]));
                        } else {
                            $contact_referent_2->setNumTel1($v);
                        }
                    }
                    // Email
                    if ('BF' == $k) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("mailto:', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0]);
                        }
                        $contact_referent_2->setLibAdr1($v);
                    }

                    // Adresse Administrative adresse
                    if ('BG' == $k) {
                        $amap->setAdresseAdmin($v);
                    }
                }
            }

            // CONTACT RÉFÉRENT RÉSEAU ---------------------------------------------
            // C-À-D != null
            if (null !== $contact_referent_1->getEmail()) {
                $amapien_1 = $this->amapienRepo->findOneBy(['email' => $contact_referent_1->getEmail()]);
                if (null !== $amapien_1) {
                    $amap->addAmapien($amapien_1);
                    $amap->setAmapienRefReseau($amapien_1);
                } else { // Sinon, on le crée
                    $amap->addAmapien($contact_referent_1);
                    $amap->setAmapienRefReseau($contact_referent_1);
                }
            }

            // CONTACT RÉFÉRENT RÉSEAU SECONDAIRE ----------------------------------
            // CÀD != null
            if (null !== $contact_referent_2->getEmail()) {
                $amapien_2 = $this->amapienRepo->findOneBy(['email' => $contact_referent_2->getEmail()]);
                if (null !== $amapien_2) {
                    $amap->addAmapien($amapien_2);
                    $amap->setAmapienRefReseauSecondaire($amapien_2);
                } else { // Sinon, on le crée
                    $amap->addAmapien($contact_referent_2);
                    $amap->setAmapienRefReseauSecondaire($contact_referent_2);
                }
            }

            $amaps[] = $amap;
        }

        return $amaps;
    }
}
