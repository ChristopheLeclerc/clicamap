<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use DI\Annotation\Inject;
use PsrLib\Services\UserRequestHelper;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'ContactTrait.php';

class Portail extends AppController
{
    use ContactTrait;

    /**
     * @Inject
     *
     * @var UserRequestHelper
     */
    public $userrequesthelper;

    /**
     * @var MY_Form_validation
     */
    public $form_validation;

    /**
     * @var ConnexionRules
     */
    public $connexionrules;

    /**
     * @var CI_Session
     */
    public $session;

    protected $data;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\Email_Sender
     */
    private $email_sender;

    /**
     * Portail constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->data = ['alerte_email' => null,
            'alerte_password' => null,
            'alerte_password_2' => null,
            'ref_prod_choix' => null,
            'edit' => null,
            'amapien' => [],
            'amap' => [],
            'paysan' => [], ];

        $this->load->helper('array');
        $this->load->helper('email');
        $this->load->helper('string');

        $this->load->library('email');

        $this->load->library('Form/Portail/ConnexionRules');
    }

    /**
     * Index.
     */
    public function index()
    {
        $currentUser = $this->getUser();
        if (null !== $currentUser) {
            redirect('evenement');
        }

        redirect('portail/connexion');
    }

    /**
     * Se déconnecter du site.
     */
    public function deconnexion()
    {
        $this->sfSession->invalidate();
        redirect('/');
    }

    /**
     * Se connecter au site.
     */
    public function connexion()
    {
        // Chargement des règles de validation
        $this->form_validation->set_rules('statut', '', 'required|regex_match[/amapien|paysan|regroupement/]');
        $this->form_validation->set_rules('email', 'Email', [
            'required',
            'valid_email',
            [
                ConnexionRules::EMAIL_EXISTE_ET_ACTIF_CALLABLE,
                [$this->connexionrules, 'email_existe_et_actif'],
            ],
        ], [
            'valid_email' => 'L\'Email n\'est pas valide.',
        ]);
        $this->form_validation->set_rules(
            'password',
            'Mot de passe',
            [
                'required',
                [
                    ConnexionRules::CONCORDANCE_EMAIL_PASSWORD_CALLABLE,
                    [$this->connexionrules, 'concordance_email_password'],
                ],
            ]
        );

        // Validation de la connexion
        if (!$this->form_validation->run()) {
            $this->loadViewWithTemplate(
                'portail/connexion',
                $this->data,
                'template',
                ['header_portail' => 1, 'menu_less' => 1]
            );

            return;
        }

        // Identifiants valides. On peut faire la connexion de l'utilisateur
        $email = $this->input->post('email');
        $statut = $this->input->post('statut');
        /** @var \PsrLib\ORM\Entity\BaseUser $user */
        $user = $this->userrequesthelper->getUserFromEmailStatut($email, $statut);

        switch (get_class($user)) {
            case \PsrLib\ORM\Entity\Amap::class:
            $this->sfSession->set('ID', $user->getId());
            $this->sfSession->set('statut', 'amap');

            break;

        case \PsrLib\ORM\Entity\Amapien::class:
            $this->sfSession->set('ID', $user->getId());
            $this->sfSession->set('statut', 'amapien');

            break;

        case \PsrLib\ORM\Entity\Paysan::class:
            $this->sfSession->set('ID', $user->getId());
            $this->sfSession->set('statut', 'paysan');

            break;

        case \PsrLib\ORM\Entity\FermeRegroupement::class:
            $this->sfSession->set('ID', $user->getId());
            $this->sfSession->set('statut', 'regroupement');

            break;

        default:
            redirect('/portail/connexion');
        }

        // Redirect to evenements
        redirect('evenement');
    }

    /**
     * Changer son mot de passe.
     *
     * @param $id
     * @param null $who
     */
    public function password($id, $who = null)
    {
        // Sécurité
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_USER_CHANGE_PASSWORD, (int) $id);

        $this->form_validation->set_rules('password_1', '"Nouveau mot de passe"', 'required|min_length[8]');
        $this->form_validation->set_rules('password_2', '"Confirmer le nouveau mot de passe"', 'required|min_length[8]');

        // VÉRIFICATION MANUELLE
        if ($this->form_validation->run()) {
            // Concordance Password 1 / Password 2
            $password1 = $this->input->post('password_1', false);
            $password2 = $this->input->post('password_2', false);
            if (!$this->concordance_passwords($password1, $password2)) {
                $this->loadViewWithTemplate(
                    'portail/password',
                    $this->data,
                    'template',
                    ['header_portail' => 1, 'menu_less' => 1]
                );
            } else {
                if ('a' == $who) {
                    /** @var \PsrLib\ORM\Entity\Amapien $amapien */
                    $amapien = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->findOneById($id);
                    $amapien->setPlainPassword($password1);
                    $this->em->flush();

                    redirect('/amapien/display/'.$amapien->getId());

                    return;
                }
                if ($who = 'pay') {
                    /** @var \PsrLib\ORM\Entity\Paysan $paysan */
                    $paysan = $this->em->getRepository(\PsrLib\ORM\Entity\Paysan::class)->findOneById($id);
                    $paysan->setPlainPassword($password1);
                    $this->em->flush();

                    redirect('/paysan/display/'.$paysan->getId());

                    return;
                }
            }
        } else { // Première visite || Formulaire non valide
            $this->loadViewWithTemplate(
                'portail/password',
                $this->data,
                'template',
                ['header_portail' => 1, 'menu_less' => 1]
            );
        }
    }

    public function contact()
    {
        $this->handleContactRequest('/portail/connexion');

        $this->data['print_contact_form'] = true;

        $this->loadViewWithTemplate(
            'portail/connexion',
            $this->data,
            'template',
            ['header_portail' => 1, 'menu_less' => 1]
        );
    }

    public function mdp_oublie()
    {
        $this->form_validation->set_rules('lostpassword_statut', '"Statut"', 'required|regex_match[/amapien|paysan|regroupement/]');
        $this->form_validation->set_rules('lostpassword_email', 'Email', [
            'required',
            'valid_email',
            [
                ConnexionRules::EMAIL_EXISTE_ET_ACTIF_CALLABLE,
                [$this->connexionrules, 'email_existe_et_actif'],
            ],
        ], [
            'valid_email' => 'L\'Email n\'est pas valide.',
        ]);

        if ($this->form_validation->run()) {
            // Genère un nouveau token
            $email = $this->input->post('lostpassword_email');
            $statut = $this->input->post('lostpassword_statut');
            /** @var ?\PsrLib\ORM\Entity\BaseUser $user */
            $user = $this->userrequesthelper->getUserFromEmailStatut($email, $statut);
            if (null === $user) {
                return redirect('/portail/connexion');
            }
            $token = new \PsrLib\ORM\Entity\Token();
            $user->setPasswordResetToken($token);
            $this->em->flush();

            // Envoyer l'email avec le lien de réintialisation
            $this->email_sender->envoyerMdpReinit($email, $user);

            // Message de confirmation pour l'utilisateur
            $this->addFlash(
                'flash_mdpreinit_mail_envoye',
                ' Un email contenant un lien vous permettant de générer un nouveau mot de passe vient de vous être envoyé, veuillez consulter votre boite mail'
            );

            return redirect('/portail/connexion');
        }

        $this->data['print_lost_password_form'] = true;

        $this->loadViewWithTemplate(
            'portail/connexion',
            $this->data,
            'template',
            ['header_portail' => 1, 'menu_less' => 1]
        );
    }

    /**
     * @param mixed $token
     */
    public function mdp_oublie_confirmation($token)
    {
        /** @var \PsrLib\ORM\Entity\BaseUser $user */
        $user = $this->userrequesthelper->getUserFromTokenMdpReinit($token);

        // Test si le lien est encore valide
        if (null === $user) {
            $this->addFlash(
                'flash_mdpreinit_confirmatio',
                'Le lien que vous avez suivi est invalide ou expiré. Merci de faire une nouvelle demande de reintialisation de mot de passe'
            );

            return redirect('portail/connexion');
        }

        $password1 = $this->input->post('password_1', false);
        $this->form_validation->set_rules('password_1', '"Mot de passe"', 'required|min_length[8]|password_strength_check', [
            'password_strength_check' => 'Mot de passe trop faible',
        ]);
        $this->form_validation->set_rules('password_2', '"Confirmez le mot de passe"', 'required|min_length[8]|password_check['.$password1.']', [
            'password_check' => 'Les deux mot de passe ne correspondent pas',
        ]);
        if ($this->form_validation->run()) {
            // Re initialise le mot de passe et supprime le token
            $user->setPlainPassword($password1);
            $user->setPasswordResetToken(null);
            $this->em->flush();

            $this->addFlash(
                'flash_mdpreinit_confirmatio',
                'Le mot de passe a été correctement mis à jour'
            );

            return redirect('portail/connexion');
        }

        $this->data['print_lost_password_reset_form'] = true;
        $this->data['token'] = $token;

        $this->loadViewWithTemplate(
            'portail/connexion',
            $this->data,
            'template',
            ['header_portail' => 1, 'menu_less' => 1]
        );
    }

    /**
     * Test la force du mot du mot de passe donné.
     *
     * @param mixed $tokenMdpOuli
     */
    public function mdp_oublie_test_password($tokenMdpOuli)
    {
        $user = $this->userrequesthelper->getUserFromTokenMdpReinit($tokenMdpOuli);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_USER_ESTIMATE_PASSWORD_STRENGTH, $user);

        generate_zxcvbn_response();
    }

    /**
     * Les deux mots de passe concordent-ils ?
     *
     * @param $password_1
     * @param $password_2
     *
     * @return bool
     */
    private function concordance_passwords($password_1, $password_2)
    {
        if ($password_1 != $password_2) {
            $this->data['alerte_password_2'] = 'Les mots de passe ne correspondent pas';

            return false;
        }

        return true;
    }
}
