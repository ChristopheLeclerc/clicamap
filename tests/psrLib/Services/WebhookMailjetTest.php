<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Services;

use PsrLib\Services\WebhookMailjet;

/**
 * @internal
 * @coversNothing
 */
class WebhookMailjetTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WebhookMailjet
     */
    private $webhook;

    public function setUp()
    {
        $this->webhook = new WebhookMailjet();
    }

    public function testInvalidJson()
    {
        $res = $this->webhook->getEmailFromRequest('invalid');

        self::assertNull($res);
    }

    public function testValidJsonNoField()
    {
        $res = $this->webhook->getEmailFromRequest('{}');

        self::assertNull($res);
    }

    public function testValidJsonSoftBounce()
    {
        $res = $this->webhook->getEmailFromRequest(file_get_contents(__DIR__.'/../../data/mailjet_request_soft_bounce.json'));

        self::assertNull($res);
    }

    public function testValidJsonHardBounce()
    {
        $res = $this->webhook->getEmailFromRequest(file_get_contents(__DIR__.'/../../data/mailjet_request_hard_bounce.json'));

        self::assertSame('bounce@mailjet.com', $res);
    }

    public function testValidJsonBlocked()
    {
        $res = $this->webhook->getEmailFromRequest(file_get_contents(__DIR__.'/../../data/mailjet_request_blocked.json'));

        self::assertSame('bounce@mailjet.com', $res);
    }

    public function testValidJsonSpam()
    {
        $res = $this->webhook->getEmailFromRequest(file_get_contents(__DIR__.'/../../data/mailjet_request_spam.json'));

        self::assertSame('bounce@mailjet.com', $res);
    }

    public function testValidJsonClick()
    {
        $res = $this->webhook->getEmailFromRequest(file_get_contents(__DIR__.'/../../data/mailjet_request_click.json'));

        self::assertNull($res);
    }
}
