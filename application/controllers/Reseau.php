<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class Reseau.
 */
class Reseau extends AppController
{
    /**
     * @var \PsrLib\ORM\Repository\ReseauRepository
     */
    private $reseauRepo;

    /**
     * Reseau constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->reseauRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Reseau::class);
    }

    /**
     * Index.
     */
    public function index()
    {
        $this->accueil();
    }

    public function accueil()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        $reseaux = $this->reseauRepo->findAll();
        $this->loadViewWithTemplate('reseau/index', [
            'reseaux' => $reseaux,
        ]);
    }

    /**
     * Création du réseau.
     *
     * @param null $reseau_id
     */
    public function creation($reseau_id = null)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        $rule = sprintf('required|is_unique[%s.%s]', \PsrLib\ORM\Entity\Reseau::class, 'nom');
        $this->form_validation->set_rules('reseau', '"Réseau"', $rule);
        if ($this->form_validation->run()) {
            if (null !== $reseau_id) {
                /** @var \PsrLib\ORM\Entity\Reseau $reseau */
                $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);
                $reseau->setNom($this->input->post('reseau'));
                $this->addFlash(
                    'notice_success',
                    'Le réseau a été modifié avec succès !'
                );
            } else { // CREATION
                $reseau = new \PsrLib\ORM\Entity\Reseau();
                $reseau->setNom($this->input->post('reseau'));
                $this->em->persist($reseau);

                $this->addFlash(
                    'notice_success',
                    'Le réseau a été créé avec succès !'
                );
            }
            $this->em->flush();
        }

        $this->loadViewWithTemplate('reseau/index', [
            'reseaux' => $this->reseauRepo->findAll(),
        ]);
    }

    /**
     * Supprimer un réseau.
     *
     * @param $reseau_id
     */
    public function remove($reseau_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);

        $this->em->remove($reseau);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le réseau a été supprimé avec succès !'
        );

        redirect('/reseau');
    }

    /**
     * Afficher les villes d'un réseau.
     *
     * @param $reseau_id
     */
    public function ville($reseau_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);

        $this->loadViewWithTemplate('reseau/ville', [
            'reseau' => $reseau,
        ]);
    }

    /**
     * Ajout d'une ville dans un réseau.
     *
     * @param $reseau_id
     */
    public function ville_ajout($reseau_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);

        $this->form_validation->set_rules('code_postal_ville', '"Code Postal, Ville"', 'required|cp_ville_check', ['required' => 'Le champ ne peut pas être vide.']);
        if ($this->form_validation->run()) {
            $lieu = explode(',', $this->input->post('code_postal_ville'));
            $cp = trim($lieu[0]);
            $nom = trim($lieu[1]);
            $ville = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Ville::class)
                ->findOneBy([
                    'cp' => $cp,
                    'nom' => $nom,
                ])
            ;
            $reseau->addVille($ville);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'La ville a été ajoutée avec succès'
            );

            redirect('/reseau/ville/'.$reseau->getId());
        }
        $this->loadViewWithTemplate('reseau/ville', [
            'reseau' => $reseau,
        ]);
    }

    /**
     * Suppression d'une ville dans un réseau.
     *
     * @param $reseau_id
     * @param $res_v_id
     */
    public function ville_remove($reseau_id, $res_v_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);
        /** @var \PsrLib\ORM\Entity\Ville $ville */
        $ville = $this->findOrExit(\PsrLib\ORM\Entity\Ville::class, $res_v_id);

        $reseau->removeVille($ville);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'La ville a été supprimée avec succès'
        );

        redirect('/reseau/ville/'.$reseau->getId());
    }

    /**
     * Afficher les Admin d'un réseau.
     *
     * @param $reseau_id
     */
    public function admin($reseau_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);

        $this->loadViewWithTemplate('reseau/admin', [
            'reseau' => $reseau,
        ]);
    }

    /**
     * @param $reseau_id
     */
    public function admin_ajout($reseau_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);

        $this->form_validation->set_rules('amapien', '"Amapien"', 'required|valid_email|amapien_check', ['required' => 'Le champ ne peut pas être vide.']);
        if ($this->form_validation->run()) {
            $amapien = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                ->findOneBy([
                    'email' => $this->input->post('amapien'),
                ])
            ;
            $reseau->addAdmin($amapien);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'admin a été ajouté avec succès'
            );

            redirect('/reseau/admin/'.$reseau->getId());
        }

        $this->loadViewWithTemplate('reseau/admin', [
            'reseau' => $reseau,
        ]);
    }

    /**
     * Suppression d'un Admin d'un réseau.
     *
     * @param $reseau_id
     * @param $res_adm_id
     */
    public function admin_remove($reseau_id, $res_adm_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);
        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $res_adm_id);

        $reseau->removeAdmin($amapien);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'admin a été supprimé avec succès'
        );

        redirect('/reseau/admin/'.$reseau->getId());
    }

    /**
     * @param string $reseau_id
     */
    public function reseau_logo($reseau_id)
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Reseau $reseau */
        $reseau = $this->findOrExit(\PsrLib\ORM\Entity\Reseau::class, $reseau_id);

        $form = $this->formFactory->create(\PsrLib\Form\EntityWithLogoType::class, $reseau);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedImg = $form->get('img')->getData();
            if (null !== $uploadedImg) {
                $logo = $this
                    ->uploader
                    ->uploadFile($uploadedImg, \PsrLib\ORM\Entity\Files\Logo::class)
                ;
                $reseau->setLogo($logo);
            }
            $this->em->flush();

            redirect('/reseau');
        }

        $this->loadViewWithTemplate('reseau/logo', [
            'reseau' => $reseau,
            'form' => $form->createView(),
        ]);
    }
}
