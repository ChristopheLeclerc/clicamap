<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class DistributionCheckDetailTest extends AbstractSecurityTest
{
    public function testDetailAsPaysan()
    {
        $distribution = $this->prophesize(AmapDistribution::class)->reveal();
        $this->mockAuthenticatedUser(new Paysan());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DETAIL, $distribution));
    }

    public function testDetailAsAmapien()
    {
        $distribution = $this->prophesize(AmapDistribution::class)->reveal();
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DETAIL, $distribution));
    }

    public function testDetailAsAmapSameLL()
    {
        $amap = new Amap();
        $ll = new AmapLivraisonLieu();
        $ll->setAmap($amap);

        $distribution = $this->prophesize(AmapDistribution::class);
        $distribution->getAmapLivraisonLieu()->willReturn($ll);

        $this->mockAuthenticatedUser($amap);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DETAIL, $distribution->reveal()));
    }

    public function testDetailAsAmapDiffrentLL()
    {
        $amap = new Amap();
        $ll = new AmapLivraisonLieu();
        $ll->setAmap($amap);

        $distribution = $this->prophesize(AmapDistribution::class);
        $distribution->getAmapLivraisonLieu()->willReturn($ll);

        $this->mockAuthenticatedUser(new Amap());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DETAIL, $distribution->reveal()));
    }
}
