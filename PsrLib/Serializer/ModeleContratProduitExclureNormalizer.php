<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Carbon\Carbon;
use DateTime;
use PsrLib\ORM\Entity\ModeleContratProduitExclure;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class ModeleContratProduitExclureNormalizer implements ContextAwareNormalizerInterface
{
    /**
     * @param ModeleContratProduitExclure $object
     * @param null|mixed                  $format
     *
     * @return array
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $dateLivraison = $object->getModeleContratDate()->getDateLivraison();
        // Force conversion from datetime instance
        if ($dateLivraison instanceof Carbon) {
            $dateLivraison = DateTime::createFromFormat('U', (string) $object->getModeleContratDate()->getDateLivraison()->getTimestamp());
        }

        return [
            'id' => $object->getId(),
            'date' => $dateLivraison,
            'produit' => $object->getModeleContratProduit()->getFermeProduit()->getId(),
        ];
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof ModeleContratProduitExclure
            && isset($context['groups'])
            && 'wizard' === $context['groups']
        ;
    }
}
