<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form\Type;

use PsrLib\ORM\Entity\AmapienAnneeAdhesion;
use PsrLib\Services\AdhesionRangeBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapienAnneeAdhesionType extends AbstractType
{
    /**
     * @var AdhesionRangeBuilder
     */
    private $adhesionRangeBuilder;

    public function __construct(AdhesionRangeBuilder $adhesionRangeBuilder)
    {
        $this->adhesionRangeBuilder = $adhesionRangeBuilder;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addModelTransformer(new CallbackTransformer(
                function ($amapAnneeAdhesions) {
                    $ret = [];
                    foreach ($amapAnneeAdhesions as $amapAnneeAdhesion) {
                        $ret[] = (string) $amapAnneeAdhesion;
                    }

                    return $ret;
                },
                function (array $annees) use ($options) {
                    $ret = [];
                    foreach ($annees as $annee) {
                        $ret[] = new AmapienAnneeAdhesion($annee, $options['amapien']);
                    }

                    return $ret;
                }
            ))
        ;
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => array_combine($this->adhesionRangeBuilder->buildAdhesionAvailiableRange(), $this->adhesionRangeBuilder->buildAdhesionAvailiableRange()),
            'amapien' => null,
            'multiple' => true,
        ]);
    }
}
