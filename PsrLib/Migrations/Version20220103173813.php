<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220103173813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        // Cleanup data
        $this->addSql('DELETE FROM ak_amap_type_production_propose WHERE amap_tpp_fk_amap_id NOT IN (SELECT amap_id FROM ak_amap);');
        $this->addSql('DELETE FROM ak_amap_type_production_recherche WHERE amap_tpr_fk_amap_id NOT IN (SELECT amap_id FROM ak_amap);');
        $this->addSql('DELETE FROM ak_amap_commentaire WHERE amap_com_fk_amap_id NOT IN (SELECT amap_id FROM ak_amap)');
        $this->addSql('DELETE FROM ak_amap_livraison_horaire WHERE amap_liv_hor_fk_amap_liv_lieu_id NOT IN (SELECT amap_liv_lieu_id FROM ak_amap_livraison_lieu);');
        $this->addSql('DELETE FROM ak_ferme_amapien_liste_attente WHERE f_a_la_fk_amapien_id NOT IN (SELECT a_id FROM ak_amapien);');
        $this->addSql('DELETE FROM ak_contrat WHERE c_fk_amapien_id NOT IN (SELECT a_id FROM ak_amapien);');
        $this->addSql('DELETE FROM ak_contrat_cellule WHERE c_c_fk_contrat_id NOT IN (SELECT c_id FROM ak_contrat)');
        $this->addSql('DELETE FROM ak_contrat_cellule WHERE c_c_fk_modele_contrat_produit_id NOT IN (SELECT mc_pro_id FROM ak_modele_contrat_produit);');
        $this->addSql('DELETE FROM ak_contrat_dates_reglement WHERE fk_contrat NOT IN (SELECT c_id FROM ak_contrat);');
        $this->addSql('DELETE FROM ak_evenement_info_amap WHERE ev_info_amap_fk_amap_id NOT IN (SELECT amap_id FROM ak_amap);');
        $this->addSql('DELETE FROM ak_evenement_info_ferme WHERE ev_info_f_fk_f_id NOT IN (SELECT f_id FROM ak_ferme);');
        $this->addSql('DELETE FROM ak_ferme_annee_adhesion WHERE ferme_aa_fk_f_id NOT IN (SELECT f_id FROM ak_ferme);');
        $this->addSql('UPDATE ak_modele_contrat SET mc_livraison_lieu=NULL WHERE mc_livraison_lieu NOT IN (SELECT amap_liv_lieu_id FROM ak_amap_livraison_lieu);');
        $this->addSql('UPDATE ak_modele_contrat SET mc_fk_ferme_id=NULL WHERE mc_fk_ferme_id NOT IN (SELECT f_id FROM ak_ferme);');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit MODIFY mc_pro_fk_ferme_produit_id bigint;');
        $this->addSql('UPDATE ak_modele_contrat_produit SET mc_pro_fk_ferme_produit_id=NULL WHERE mc_pro_fk_ferme_produit_id NOT IN (SELECT f_pro_id FROM ak_ferme_produit);');
        $this->addSql('DELETE FROM ak_modele_contrat_date WHERE mc_d_fk_modele_contrat_id NOT IN (SELECT mc_id FROM ak_modele_contrat);');
        $this->addSql('DELETE FROM ak_modele_contrat_dates_reglement WHERE fk_modele_contrat NOT IN (SELECT mc_id FROM ak_modele_contrat);');
        $this->addSql('DELETE FROM ak_contrat_cellule WHERE c_c_fk_modele_contrat_produit_id NOT IN (SELECT mc_pro_id FROM ak_modele_contrat_produit);');
        $this->addSql('DELETE FROM ak_modele_contrat_produit WHERE mc_pro_fk_modele_contrat_id NOT IN (SELECT mc_id FROM ak_modele_contrat);');
        $this->addSql('DELETE FROM ak_modele_contrat_produit_exclure WHERE mc_pro_ex_fk_modele_contrat_date_id NOT IN (SELECT mc_d_id FROM ak_modele_contrat_date);');
        $this->addSql('DELETE FROM ak_modele_contrat_produit_exclure WHERE mc_pro_ex_fk_modele_contrat_produit_id NOT IN (SELECT mc_pro_id FROM ak_modele_contrat_produit);');
        $this->addSql('DELETE FROM ak_paysan_commentaire WHERE pay_com_fk_pay_id NOT IN (SELECT pay_id FROM ak_paysan);');

        // Drop unused tables
        $this->addSql('DROP TABLE ak_amap_amapien');
        $this->addSql('DROP TABLE ak_amap_livraison');
        $this->addSql('DROP TABLE ak_amap_recherche_paysan_match');
        $this->addSql('DROP TABLE ak_ferme_paysan');
        $this->addSql('DROP TABLE ak_ferme_referent');
        $this->addSql('DROP TABLE ak_ferme_referent_modele_contrat');
        $this->addSql('DROP TABLE ak_paysan_annonce');
        $this->addSql('DROP TABLE ak_paysan_annonce_departement');
        $this->addSql('DROP TABLE ak_paysan_annonce_jour');
        $this->addSql('DROP TABLE ak_paysan_annonce_type_prod');
        $this->addSql('DROP TABLE ak_super_administrateur');
        $this->addSql('DROP TABLE migrations');

        // Remove old keys
        $this->addSql('ALTER TABLE adhesion_amap_amapien DROP FOREIGN KEY FK_D86B834461220EA6');
        $this->addSql('ALTER TABLE adhesion_amap_amapien DROP FOREIGN KEY FK_D86B8344BC7A2B78');
        $this->addSql('ALTER TABLE adhesion_amap_amapien DROP FOREIGN KEY FK_D86B8344F920BBA2');
        $this->addSql('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC633BC43DD2');
        $this->addSql('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC63A4B3F446');
        $this->addSql('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC63AA2E96A0');
        $this->addSql('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC63B3E7E790');
        $this->addSql('ALTER TABLE ak_amap_absence DROP FOREIGN KEY ak_amap_absence_ibfk_1');
        $this->addSql('ALTER TABLE ak_amap_distribution DROP FOREIGN KEY FK_C219B4A1857FEC30');
        $this->addSql('ALTER TABLE ak_amapien DROP FOREIGN KEY FK_2CDF7F8D52AA66E8');
        $this->addSql('ALTER TABLE ak_amapien DROP FOREIGN KEY FK_2CDF7F8DAA2E96A0');
        $this->addSql('ALTER TABLE ak_contrat DROP FOREIGN KEY FK_72C7BFC6511FC912');
        $this->addSql('ALTER TABLE ak_evenement DROP FOREIGN KEY FK_1EE75601C06A9F55');
        $this->addSql('ALTER TABLE ak_evenement DROP FOREIGN KEY FK_1EE75601F26B1C97');
        $this->addSql('ALTER TABLE ak_ferme DROP FOREIGN KEY FK_AD7E64AEA73F0036');
        $this->addSql('ALTER TABLE ak_ferme_produit DROP FOREIGN KEY FK_244CA784708B3AB7');
        $this->addSql('ALTER TABLE ak_ferme_produit DROP FOREIGN KEY FK_244CA7849C2BA34A');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose DROP FOREIGN KEY FK_DF57266059114409');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose DROP FOREIGN KEY FK_DF572660DA1F6AB5');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit DROP FOREIGN KEY FK_A15C0C389C2BA34A');
        $this->addSql('ALTER TABLE ak_paysan DROP FOREIGN KEY FK_CC11D86018981132');
        $this->addSql('ALTER TABLE ak_paysan DROP FOREIGN KEY FK_CC11D860A73F0036');
        $this->addSql('ALTER TABLE ak_paysan DROP FOREIGN KEY FK_CC11D860AA2E96A0');
        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau DROP FOREIGN KEY ak_paysan_cadre_reseau_ibfk_1');
        $this->addSql('ALTER TABLE amap_distribution_amapien DROP FOREIGN KEY FK_F041D04019E9F30A');
        $this->addSql('ALTER TABLE amap_distribution_amapien DROP FOREIGN KEY FK_F041D0408AEA9B1B');
        $this->addSql('ALTER TABLE ferme_amapien DROP FOREIGN KEY FK_AA769FCC19E9F30A');
        $this->addSql('ALTER TABLE ferme_amapien DROP FOREIGN KEY FK_AA769FCC48A4116C');

        // Change database structure
        $this->addSql('ALTER TABLE adhesion_amap CHANGE amap_id amap_id INT NOT NULL, CHANGE value_id value_id INT NOT NULL, CHANGE creator_id creator_id INT NOT NULL');
        $this->addSql('ALTER TABLE adhesion_amap_amapien CHANGE amapien_id amapien_id INT DEFAULT NULL, CHANGE creator_id creator_id INT NOT NULL');
        $this->addSql('ALTER TABLE adhesion_amapien CHANGE amapien_id amapien_id INT NOT NULL, CHANGE value_id value_id INT NOT NULL, CHANGE creator_id creator_id INT NOT NULL');
        $this->addSql('ALTER TABLE adhesion_ferme CHANGE ferme_id ferme_id INT NOT NULL, CHANGE value_id value_id INT NOT NULL, CHANGE creator_id creator_id INT NOT NULL');
        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap DROP amap_mdpreinit_token, DROP amap_mdpreinit_creationdate, CHANGE amap_id amap_id INT AUTO_INCREMENT NOT NULL, CHANGE amap_fk_contact_referent_reseau_id amap_fk_contact_referent_reseau_id INT DEFAULT NULL, CHANGE amap_fk_contact_referent_reseau_secondaire_id amap_fk_contact_referent_reseau_secondaire_id INT DEFAULT NULL, CHANGE possede_assurance possede_assurance TINYINT(1) DEFAULT NULL, CHANGE amap_etudiante amap_etudiante TINYINT(1) DEFAULT NULL, CHANGE amap_email_invalid amap_email_invalid TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_type_production_propose MODIFY amap_tpp_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_type_production_propose DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_amap_type_production_propose DROP amap_tpp_id, CHANGE amap_tpp_fk_amap_id amap_tpp_fk_amap_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_type_production_propose ADD PRIMARY KEY (amap_tpp_fk_amap_id, amap_tpp_fk_type_production_id)');
        $this->addSql('ALTER TABLE ak_amap_type_production_recherche MODIFY amap_tpr_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_type_production_recherche DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_amap_type_production_recherche DROP amap_tpr_id, CHANGE amap_tpr_fk_amap_id amap_tpr_fk_amap_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_type_production_recherche ADD PRIMARY KEY (amap_tpr_fk_amap_id, amap_tpr_fk_type_production_id)');
        $this->addSql('ALTER TABLE ak_amap_absence CHANGE absence_id absence_id INT AUTO_INCREMENT NOT NULL, CHANGE absence_titre absence_titre VARCHAR(255) DEFAULT NULL, CHANGE absence_fk_amap_id absence_fk_amap_id INT NOT NULL, CHANGE absence_debut absence_debut DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon)\', CHANGE absence_fin absence_fin DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon)\'');
        $this->addSql('ALTER TABLE ak_amap_annee_adhesion CHANGE amap_aa_id amap_aa_id INT AUTO_INCREMENT NOT NULL, CHANGE amap_aa_annee amap_aa_annee INT NOT NULL, CHANGE amap_aa_fk_amap_id amap_aa_fk_amap_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_collectif CHANGE amap_coll_id amap_coll_id INT AUTO_INCREMENT NOT NULL, CHANGE amap_coll_fk_amap_id amap_coll_fk_amap_id INT NOT NULL, CHANGE amap_coll_fk_amapien_id amap_coll_fk_amapien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_commentaire CHANGE amap_com_id amap_com_id INT AUTO_INCREMENT NOT NULL, CHANGE amap_com_fk_amap_id amap_com_fk_amap_id INT DEFAULT NULL, CHANGE amap_com_date amap_com_date DATE NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE amap_livraison_lieu_id amap_livraison_lieu_id INT NOT NULL, CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) NOT NULL, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE amap_distribution_amapien CHANGE amapien_a_id amapien_a_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_livraison_horaire CHANGE amap_liv_hor_id amap_liv_hor_id INT AUTO_INCREMENT NOT NULL, CHANGE amap_liv_hor_fk_amap_liv_lieu_id amap_liv_hor_fk_amap_liv_lieu_id INT NOT NULL, CHANGE amap_liv_hor_saison amap_liv_hor_saison VARCHAR(5) DEFAULT NULL, CHANGE amap_liv_hor_jour amap_liv_hor_jour VARCHAR(50) DEFAULT NULL, CHANGE amap_liv_hor_heure_debut amap_liv_hor_heure_debut VARCHAR(5) DEFAULT NULL, CHANGE amap_liv_hor_heure_fin amap_liv_hor_heure_fin VARCHAR(5) DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_livraison_lieu DROP amap_liv_lieu_cp, DROP amap_liv_lieu_ville, DROP amap_liv_lieu_fk_dep_id, DROP amap_liv_lieu_fk_reg_id, CHANGE amap_liv_lieu_id amap_liv_lieu_id INT AUTO_INCREMENT NOT NULL, CHANGE amap_liv_lieu_fk_amap_id amap_liv_lieu_fk_amap_id INT NOT NULL, CHANGE amap_liv_lieu amap_liv_lieu VARCHAR(255) NOT NULL, CHANGE amap_liv_lieu_lib_adr amap_liv_lieu_lib_adr VARCHAR(255) NOT NULL, CHANGE amap_liv_lieu_gps_latitude amap_liv_lieu_gps_latitude VARCHAR(255) NOT NULL, CHANGE amap_liv_lieu_gps_longitude amap_liv_lieu_gps_longitude VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_recherche_paysan CHANGE amap_r_pay_id amap_r_pay_id INT AUTO_INCREMENT NOT NULL, CHANGE amap_r_pay_debut amap_r_pay_debut DATE NOT NULL, CHANGE amap_r_pay_expiration amap_r_pay_expiration DATE NOT NULL, CHANGE amap_r_pay_fk_amap_id amap_r_pay_fk_amap_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amapien CHANGE a_id a_id INT AUTO_INCREMENT NOT NULL, CHANGE a_email_invalid a_email_invalid TINYINT(1) NOT NULL, CHANGE amap_id amap_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_ferme_amapien_liste_attente MODIFY f_a_la_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_amapien_liste_attente DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_ferme_amapien_liste_attente DROP f_a_la_id, CHANGE f_a_la_fk_ferme_id f_a_la_fk_ferme_id INT NOT NULL, CHANGE f_a_la_fk_amapien_id f_a_la_fk_amapien_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_amapien_liste_attente ADD PRIMARY KEY (f_a_la_fk_amapien_id, f_a_la_fk_ferme_id)');
        $this->addSql('ALTER TABLE ak_amapien_annee_adhesion CHANGE a_aa_id a_aa_id INT AUTO_INCREMENT NOT NULL, CHANGE a_aa_annee a_aa_annee INT NOT NULL, CHANGE a_aa_fk_amapien_id a_aa_fk_amapien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_contrat DROP c_montant_avoir, DROP c_contrat_fichier, CHANGE c_id c_id INT AUTO_INCREMENT NOT NULL, CHANGE c_fk_modele_contrat_id c_fk_modele_contrat_id INT DEFAULT NULL, CHANGE c_fk_amapien_id c_fk_amapien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_contrat_cellule CHANGE c_c_id c_c_id INT AUTO_INCREMENT NOT NULL, CHANGE c_c_fk_contrat_id c_c_fk_contrat_id INT DEFAULT NULL, CHANGE c_c_fk_modele_contrat_produit_id c_c_fk_modele_contrat_produit_id INT DEFAULT NULL, CHANGE c_c_fk_modele_contrat_date_id c_c_fk_modele_contrat_date_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE fk_contrat fk_contrat INT DEFAULT NULL, CHANGE fk_modele_contrat_dates_reglement fk_modele_contrat_dates_reglement INT DEFAULT NULL, CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_departement CHANGE dep_id dep_id VARCHAR(3) NOT NULL, CHANGE dep_fk_reg_id dep_fk_reg_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CD4D5D97F98F144A ON ak_departement (logo_id)');
        $this->addSql('ALTER TABLE ak_departement_administrateur MODIFY dep_adm_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_departement_administrateur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_departement_administrateur DROP dep_adm_id, CHANGE dep_adm_fk_amapien_id dep_adm_fk_amapien_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_departement_administrateur ADD PRIMARY KEY (dep_adm_fk_departement_id, dep_adm_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_evenement DROP INDEX FK_1EE75601F26B1C97, ADD UNIQUE INDEX UNIQ_1EE75601F26B1C97 (pj_id)');
        $this->addSql('ALTER TABLE ak_evenement DROP INDEX FK_1EE75601C06A9F55, ADD UNIQUE INDEX UNIQ_1EE75601C06A9F55 (img_id)');
        $this->addSql('ALTER TABLE ak_evenement CHANGE ev_id ev_id INT AUTO_INCREMENT NOT NULL, CHANGE ev_nom ev_nom VARCHAR(150) DEFAULT NULL, CHANGE ev_date_deb ev_date_deb DATE DEFAULT NULL, CHANGE ev_date_fin ev_date_fin DATE DEFAULT NULL, CHANGE ev_info_amap_ferme ev_info_amap_ferme INT DEFAULT NULL, CHANGE ev_fk_amap_id ev_fk_amap_id INT DEFAULT NULL, CHANGE ev_fk_amapien_id ev_fk_amapien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_amap MODIFY ev_info_amap_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_amap DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_evenement_info_amap DROP ev_info_amap_id, CHANGE ev_info_amap_fk_ev_id ev_info_amap_fk_ev_id INT NOT NULL, CHANGE ev_info_amap_fk_amap_id ev_info_amap_fk_amap_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_amap ADD PRIMARY KEY (ev_info_amap_fk_ev_id, ev_info_amap_fk_amap_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_departement MODIFY ev_info_dep_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_departement DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_evenement_info_departement DROP ev_info_dep_id, CHANGE ev_info_dep_fk_dep_id ev_info_dep_fk_dep_id VARCHAR(3) NOT NULL, CHANGE ev_info_dep_fk_ev_id ev_info_dep_fk_ev_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_departement ADD PRIMARY KEY (ev_info_dep_fk_ev_id, ev_info_dep_fk_dep_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_ferme MODIFY ev_info_f_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_ferme DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_evenement_info_ferme DROP ev_info_f_id, CHANGE ev_info_f_fk_ev_id ev_info_f_fk_ev_id INT NOT NULL, CHANGE ev_info_f_fk_f_id ev_info_f_fk_f_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_ferme ADD PRIMARY KEY (ev_info_f_fk_ev_id, ev_info_f_fk_f_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_reseau MODIFY ev_info_res_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_reseau DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_evenement_info_reseau DROP ev_info_res_id, CHANGE ev_info_res_fk_res_id ev_info_res_fk_res_id INT NOT NULL, CHANGE ev_info_res_fk_ev_id ev_info_res_fk_ev_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_reseau ADD PRIMARY KEY (ev_info_res_fk_ev_id, ev_info_res_fk_res_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_type_prod MODIFY ev_info_tp_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_type_prod DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_evenement_info_type_prod DROP ev_info_tp_id, CHANGE ev_info_tp_fk_ev_id ev_info_tp_fk_ev_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_evenement_info_type_prod ADD PRIMARY KEY (ev_info_tp_fk_ev_id, ev_info_tp_fk_tp_id)');
        $this->addSql('ALTER TABLE ak_ferme DROP f_code_postal, DROP f_ville, DROP f_fk_dep_id, DROP f_fk_reg_id, CHANGE f_id f_id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE ferme_amapien CHANGE ferme_f_id ferme_f_id INT NOT NULL, CHANGE amapien_a_id amapien_a_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_annee_adhesion CHANGE ferme_aa_id ferme_aa_id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_produit CHANGE f_pro_id f_pro_id INT AUTO_INCREMENT NOT NULL, CHANGE f_pro_nom f_pro_nom VARCHAR(255) DEFAULT NULL, CHANGE f_pro_conditionnement f_pro_conditionnement VARCHAR(255) DEFAULT NULL, CHANGE f_pro_fk_ferme_id f_pro_fk_ferme_id INT DEFAULT NULL, CHANGE f_pro_regul_pds f_pro_regul_pds TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose DROP INDEX FK_DF57266059114409, ADD UNIQUE INDEX UNIQ_DF57266059114409 (attestation_certification_id)');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose DROP ferme_tpp_lien_attestation_certification, CHANGE ferme_tpp_id ferme_tpp_id INT AUTO_INCREMENT NOT NULL, CHANGE ferme_tpp_fk_ferme_id ferme_tpp_fk_ferme_id INT NOT NULL, CHANGE ferme_tpp_nb_amap_livrees ferme_tpp_nb_amap_livrees NUMERIC(30, 2) DEFAULT NULL, CHANGE ferme_tpp_nb_contrats_total_amap ferme_tpp_nb_contrats_total_amap NUMERIC(30, 2) DEFAULT NULL, CHANGE ferme_tpp_ca_annuel_amap ferme_tpp_ca_annuel_amap NUMERIC(30, 2) DEFAULT NULL, CHANGE ferme_tpp_p_ca_total ferme_tpp_p_ca_total NUMERIC(30, 2) DEFAULT NULL, CHANGE type_production_id type_production_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat DROP mc_fk_ferme_referent_id, CHANGE mc_id mc_id INT AUTO_INCREMENT NOT NULL, CHANGE mc_livraison_lieu mc_livraison_lieu INT DEFAULT NULL, CHANGE mc_frequence mc_frequence INT DEFAULT NULL, CHANGE mc_modalites mc_modalites INT DEFAULT NULL, CHANGE mc_frequence_articles mc_frequence_articles INT DEFAULT NULL, CHANGE mc_nbliv_plancher mc_nbliv_plancher INT DEFAULT NULL, CHANGE mc_version mc_version INT NOT NULL, CHANGE mc_amapien_permission_report_livraison mc_amapien_permission_report_livraison TINYINT(1) DEFAULT NULL, CHANGE mc_nbliv_plancher_depassement mc_nbliv_plancher_depassement TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_date CHANGE mc_d_id mc_d_id INT AUTO_INCREMENT NOT NULL, CHANGE mc_d_fk_modele_contrat_id mc_d_fk_modele_contrat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_dates_reglement CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE date date DATE DEFAULT NULL, CHANGE fk_modele_contrat fk_modele_contrat INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit DROP mc_pro_fk_type_production_lib_lien, CHANGE mc_pro_id mc_pro_id INT AUTO_INCREMENT NOT NULL, CHANGE mc_pro_fk_ferme_produit_id mc_pro_fk_ferme_produit_id INT DEFAULT NULL, CHANGE mc_pro_fk_modele_contrat_id mc_pro_fk_modele_contrat_id INT DEFAULT NULL, CHANGE mc_pro_regul_pds mc_pro_regul_pds TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit_exclure DROP mc_pro_ex_fk_modele_contrat_id, CHANGE mc_pro_ex_id mc_pro_ex_id INT AUTO_INCREMENT NOT NULL, CHANGE mc_pro_ex_fk_modele_contrat_date_id mc_pro_ex_fk_modele_contrat_date_id INT DEFAULT NULL, CHANGE mc_pro_ex_fk_modele_contrat_produit_id mc_pro_ex_fk_modele_contrat_produit_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_paysan DROP pay_code_postal, DROP pay_ville, CHANGE pay_id pay_id INT AUTO_INCREMENT NOT NULL, CHANGE pay_email pay_email VARCHAR(255) DEFAULT NULL, CHANGE pay_email_invalid pay_email_invalid TINYINT(1) NOT NULL, CHANGE ferme_id ferme_id INT DEFAULT NULL, CHANGE newsletter newsletter TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau DROP INDEX pay_cr_fk_pay_id, ADD UNIQUE INDEX UNIQ_8148CA6D44643D9C (pay_cr_fk_pay_id)');
        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau CHANGE pay_cr_id pay_cr_id INT AUTO_INCREMENT NOT NULL, CHANGE pay_cr_fk_pay_id pay_cr_fk_pay_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_paysan_commentaire CHANGE pay_com_id pay_com_id INT AUTO_INCREMENT NOT NULL, CHANGE pay_com_fk_pay_id pay_com_fk_pay_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_region CHANGE reg_id reg_id INT NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4BADF42FF98F144A ON ak_region (logo_id)');
        $this->addSql('ALTER TABLE ak_region_administrateur MODIFY reg_adm_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_region_administrateur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_region_administrateur DROP reg_adm_id, CHANGE reg_adm_fk_region_id reg_adm_fk_region_id INT NOT NULL, CHANGE reg_adm_fk_amapien_id reg_adm_fk_amapien_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_region_administrateur ADD PRIMARY KEY (reg_adm_fk_region_id, reg_adm_fk_amapien_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_892A29E1F98F144A ON ak_reseau (logo_id)');
        $this->addSql('ALTER TABLE ak_reseau_administrateur MODIFY res_adm_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_reseau_administrateur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_reseau_administrateur DROP res_adm_id, CHANGE res_adm_fk_reseau_id res_adm_fk_reseau_id INT NOT NULL, CHANGE res_adm_fk_amapien_id res_adm_fk_amapien_id INT NOT NULL');
        $this->addSql('ALTER TABLE ak_reseau_administrateur ADD PRIMARY KEY (res_adm_fk_reseau_id, res_adm_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_reseau_villes MODIFY res_v_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_reseau_villes DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE ak_reseau_villes DROP res_v_id');
        $this->addSql('ALTER TABLE ak_reseau_villes ADD PRIMARY KEY (res_v_fk_reseau_id, res_v_fk_ville_id)');
        $this->addSql('DROP INDEX res_cp_fk_reseau_id ON ak_reseau_villes');
        $this->addSql('ALTER TABLE ak_type_production DROP tp_categorie_fille');
        $this->addSql('ALTER TABLE ak_ville CHANGE v_id v_id VARCHAR(5) NOT NULL, CHANGE v_cp v_cp INT NOT NULL, CHANGE v_fk_dep_id v_fk_dep_id VARCHAR(3) NOT NULL');

        // Restore keys
        $this->addSql('ALTER TABLE adhesion_amap ADD CONSTRAINT FK_3623071452AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE adhesion_amap ADD CONSTRAINT FK_36230714F920BBA2 FOREIGN KEY (value_id) REFERENCES adhesion_value (id)');
        $this->addSql('ALTER TABLE adhesion_amap ADD CONSTRAINT FK_3623071461220EA6 FOREIGN KEY (creator_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE adhesion_amap_amapien ADD CONSTRAINT FK_D86B8344BC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE adhesion_amap_amapien ADD CONSTRAINT FK_D86B8344F920BBA2 FOREIGN KEY (value_id) REFERENCES adhesion_value (id)');
        $this->addSql('ALTER TABLE adhesion_amap_amapien ADD CONSTRAINT FK_D86B834461220EA6 FOREIGN KEY (creator_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE adhesion_amapien ADD CONSTRAINT FK_FCCE162FBC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE adhesion_amapien ADD CONSTRAINT FK_FCCE162FF920BBA2 FOREIGN KEY (value_id) REFERENCES adhesion_value (id)');
        $this->addSql('ALTER TABLE adhesion_amapien ADD CONSTRAINT FK_FCCE162F61220EA6 FOREIGN KEY (creator_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE adhesion_ferme ADD CONSTRAINT FK_63AE08EA18981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE adhesion_ferme ADD CONSTRAINT FK_63AE08EAF920BBA2 FOREIGN KEY (value_id) REFERENCES adhesion_value (id)');
        $this->addSql('ALTER TABLE adhesion_ferme ADD CONSTRAINT FK_63AE08EA61220EA6 FOREIGN KEY (creator_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63AA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63A4B3F446 FOREIGN KEY (amap_fk_contact_referent_reseau_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC633BC43DD2 FOREIGN KEY (amap_fk_contact_referent_reseau_secondaire_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_87ABDC63B3E7E790 ON ak_amap (adresse_admin_ville_id)');
        $this->addSql('ALTER TABLE ak_amap_type_production_propose ADD CONSTRAINT FK_B785E7B144060735 FOREIGN KEY (amap_tpp_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_amap_type_production_propose ADD CONSTRAINT FK_B785E7B1DE8120EC FOREIGN KEY (amap_tpp_fk_type_production_id) REFERENCES ak_type_production (tp_id)');
        $this->addSql('CREATE INDEX IDX_B785E7B144060735 ON ak_amap_type_production_propose (amap_tpp_fk_amap_id)');
        $this->addSql('CREATE INDEX IDX_B785E7B1DE8120EC ON ak_amap_type_production_propose (amap_tpp_fk_type_production_id)');
        $this->addSql('ALTER TABLE ak_amap_type_production_recherche ADD CONSTRAINT FK_94B9C060A83D99AA FOREIGN KEY (amap_tpr_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_amap_type_production_recherche ADD CONSTRAINT FK_94B9C0603317F305 FOREIGN KEY (amap_tpr_fk_type_production_id) REFERENCES ak_type_production (tp_id)');
        $this->addSql('CREATE INDEX IDX_94B9C060A83D99AA ON ak_amap_type_production_recherche (amap_tpr_fk_amap_id)');
        $this->addSql('CREATE INDEX IDX_94B9C0603317F305 ON ak_amap_type_production_recherche (amap_tpr_fk_type_production_id)');
        $this->addSql('ALTER TABLE ak_amap_absence ADD CONSTRAINT FK_40084272A18BE7C7 FOREIGN KEY (absence_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_amap_annee_adhesion ADD CONSTRAINT FK_FA2ECD324E8C5DD FOREIGN KEY (amap_aa_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('CREATE INDEX IDX_FA2ECD324E8C5DD ON ak_amap_annee_adhesion (amap_aa_fk_amap_id)');
        $this->addSql('ALTER TABLE ak_amap_collectif ADD CONSTRAINT FK_BDB20552A9DD0970 FOREIGN KEY (amap_coll_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_amap_collectif ADD CONSTRAINT FK_BDB20552637E513C FOREIGN KEY (amap_coll_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('CREATE INDEX IDX_BDB20552A9DD0970 ON ak_amap_collectif (amap_coll_fk_amap_id)');
        $this->addSql('CREATE INDEX IDX_BDB20552637E513C ON ak_amap_collectif (amap_coll_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_amap_commentaire ADD CONSTRAINT FK_3048826646B90E64 FOREIGN KEY (amap_com_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('CREATE INDEX IDX_3048826646B90E64 ON ak_amap_commentaire (amap_com_fk_amap_id)');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) NOT NULL, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution ADD CONSTRAINT FK_C219B4A1857FEC30 FOREIGN KEY (amap_livraison_lieu_id) REFERENCES ak_amap_livraison_lieu (amap_liv_lieu_id)');
        $this->addSql('ALTER TABLE amap_distribution_amapien ADD CONSTRAINT FK_F041D0408AEA9B1B FOREIGN KEY (amap_distribution_id) REFERENCES ak_amap_distribution (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE amap_distribution_amapien ADD CONSTRAINT FK_F041D04019E9F30A FOREIGN KEY (amapien_a_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE ak_amap_livraison_horaire ADD CONSTRAINT FK_9BCAEDA3684C06ED FOREIGN KEY (amap_liv_hor_fk_amap_liv_lieu_id) REFERENCES ak_amap_livraison_lieu (amap_liv_lieu_id)');
        $this->addSql('CREATE INDEX IDX_9BCAEDA3684C06ED ON ak_amap_livraison_horaire (amap_liv_hor_fk_amap_liv_lieu_id)');
        $this->addSql('ALTER TABLE ak_amap_livraison_lieu ADD CONSTRAINT FK_65938E818340F746 FOREIGN KEY (amap_liv_lieu_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('CREATE INDEX IDX_65938E818340F746 ON ak_amap_livraison_lieu (amap_liv_lieu_fk_amap_id)');
        $this->addSql('CREATE INDEX IDX_65938E81A73F0036 ON ak_amap_livraison_lieu (ville_id)');
        $this->addSql('ALTER TABLE ak_amap_recherche_paysan ADD CONSTRAINT FK_9AE600E775463982 FOREIGN KEY (amap_r_pay_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('CREATE INDEX IDX_9AE600E775463982 ON ak_amap_recherche_paysan (amap_r_pay_fk_amap_id)');
        $this->addSql('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8DAA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->addSql('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8D52AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_2CDF7F8DA73F0036 ON ak_amapien (ville_id)');
        $this->addSql('ALTER TABLE ak_ferme_amapien_liste_attente ADD CONSTRAINT FK_29F98926A721E02 FOREIGN KEY (f_a_la_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE ak_ferme_amapien_liste_attente ADD CONSTRAINT FK_29F9892359474DF FOREIGN KEY (f_a_la_fk_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('CREATE INDEX IDX_29F98926A721E02 ON ak_ferme_amapien_liste_attente (f_a_la_fk_amapien_id)');
        $this->addSql('CREATE INDEX IDX_29F9892359474DF ON ak_ferme_amapien_liste_attente (f_a_la_fk_ferme_id)');
        $this->addSql('ALTER TABLE ak_amapien_annee_adhesion ADD CONSTRAINT FK_AF37F51BBD0424BF FOREIGN KEY (a_aa_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('CREATE INDEX IDX_AF37F51BBD0424BF ON ak_amapien_annee_adhesion (a_aa_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_contrat ADD CONSTRAINT FK_72C7BFC6A06790C FOREIGN KEY (c_fk_modele_contrat_id) REFERENCES ak_modele_contrat (mc_id)');
        $this->addSql('ALTER TABLE ak_contrat ADD CONSTRAINT FK_72C7BFC6B66FF47F FOREIGN KEY (c_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE ak_contrat ADD CONSTRAINT FK_72C7BFC6511FC912 FOREIGN KEY (pdf_id) REFERENCES file (id)');
        $this->addSql('CREATE INDEX IDX_72C7BFC6B66FF47F ON ak_contrat (c_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_contrat_cellule ADD CONSTRAINT FK_65D0D3F86AC68896 FOREIGN KEY (c_c_fk_contrat_id) REFERENCES ak_contrat (c_id)');
        $this->addSql('ALTER TABLE ak_contrat_cellule ADD CONSTRAINT FK_65D0D3F8367F2141 FOREIGN KEY (c_c_fk_modele_contrat_produit_id) REFERENCES ak_modele_contrat_produit (mc_pro_id)');
        $this->addSql('ALTER TABLE ak_contrat_cellule ADD CONSTRAINT FK_65D0D3F828C69F13 FOREIGN KEY (c_c_fk_modele_contrat_date_id) REFERENCES ak_modele_contrat_date (mc_d_id)');
        $this->addSql('CREATE INDEX IDX_65D0D3F86AC68896 ON ak_contrat_cellule (c_c_fk_contrat_id)');
        $this->addSql('CREATE INDEX IDX_65D0D3F8367F2141 ON ak_contrat_cellule (c_c_fk_modele_contrat_produit_id)');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement ADD CONSTRAINT FK_1103000A901BA4BF FOREIGN KEY (fk_contrat) REFERENCES ak_contrat (c_id)');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement ADD CONSTRAINT FK_1103000A7320B82C FOREIGN KEY (fk_modele_contrat_dates_reglement) REFERENCES ak_modele_contrat_dates_reglement (id)');
        $this->addSql('CREATE INDEX IDX_1103000A901BA4BF ON ak_contrat_dates_reglement (fk_contrat)');
        $this->addSql('CREATE INDEX IDX_1103000A7320B82C ON ak_contrat_dates_reglement (fk_modele_contrat_dates_reglement)');
        $this->addSql('CREATE INDEX IDX_CD4D5D97985E3896 ON ak_departement (dep_fk_reg_id)');
        $this->addSql('ALTER TABLE ak_departement_administrateur ADD CONSTRAINT FK_DB2DFC64BB39129D FOREIGN KEY (dep_adm_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('CREATE INDEX IDX_DB2DFC6443D2066E ON ak_departement_administrateur (dep_adm_fk_departement_id)');
        $this->addSql('CREATE INDEX IDX_DB2DFC64BB39129D ON ak_departement_administrateur (dep_adm_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE75601C06A9F55 FOREIGN KEY (img_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE75601F26B1C97 FOREIGN KEY (pj_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE75601B3099DAC FOREIGN KEY (ev_info_amap_ferme) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE756017D96118B FOREIGN KEY (ev_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE75601B9FACD98 FOREIGN KEY (ev_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('CREATE INDEX IDX_1EE75601B3099DAC ON ak_evenement (ev_info_amap_ferme)');
        $this->addSql('CREATE INDEX IDX_1EE756017D96118B ON ak_evenement (ev_fk_amap_id)');
        $this->addSql('CREATE INDEX IDX_1EE75601B9FACD98 ON ak_evenement (ev_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_amap ADD CONSTRAINT FK_8ABBD76992C1F371 FOREIGN KEY (ev_info_amap_fk_ev_id) REFERENCES ak_evenement (ev_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_amap ADD CONSTRAINT FK_8ABBD7692BC9DC48 FOREIGN KEY (ev_info_amap_fk_amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('CREATE INDEX IDX_8ABBD76992C1F371 ON ak_evenement_info_amap (ev_info_amap_fk_ev_id)');
        $this->addSql('CREATE INDEX IDX_8ABBD7692BC9DC48 ON ak_evenement_info_amap (ev_info_amap_fk_amap_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_departement ADD CONSTRAINT FK_9170BD45A6E10FA3 FOREIGN KEY (ev_info_dep_fk_ev_id) REFERENCES ak_evenement (ev_id)');
        $this->addSql('CREATE INDEX IDX_9170BD45A6E10FA3 ON ak_evenement_info_departement (ev_info_dep_fk_ev_id)');
        $this->addSql('CREATE INDEX IDX_9170BD4556A13F61 ON ak_evenement_info_departement (ev_info_dep_fk_dep_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_ferme ADD CONSTRAINT FK_4DA69DBB18FC0D2C FOREIGN KEY (ev_info_f_fk_ev_id) REFERENCES ak_evenement (ev_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_ferme ADD CONSTRAINT FK_4DA69DBB96A414DC FOREIGN KEY (ev_info_f_fk_f_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('CREATE INDEX IDX_4DA69DBB18FC0D2C ON ak_evenement_info_ferme (ev_info_f_fk_ev_id)');
        $this->addSql('CREATE INDEX IDX_4DA69DBB96A414DC ON ak_evenement_info_ferme (ev_info_f_fk_f_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_reseau ADD CONSTRAINT FK_E41715F3AEF3FCBC FOREIGN KEY (ev_info_res_fk_ev_id) REFERENCES ak_evenement (ev_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_reseau ADD CONSTRAINT FK_E41715F31C9B6E0F FOREIGN KEY (ev_info_res_fk_res_id) REFERENCES ak_reseau (res_id)');
        $this->addSql('CREATE INDEX IDX_E41715F3AEF3FCBC ON ak_evenement_info_reseau (ev_info_res_fk_ev_id)');
        $this->addSql('CREATE INDEX IDX_E41715F31C9B6E0F ON ak_evenement_info_reseau (ev_info_res_fk_res_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_type_prod ADD CONSTRAINT FK_4343A81D6DB9D32E FOREIGN KEY (ev_info_tp_fk_ev_id) REFERENCES ak_evenement (ev_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_type_prod ADD CONSTRAINT FK_4343A81D155232C0 FOREIGN KEY (ev_info_tp_fk_tp_id) REFERENCES ak_type_production (tp_id)');
        $this->addSql('CREATE INDEX IDX_4343A81D6DB9D32E ON ak_evenement_info_type_prod (ev_info_tp_fk_ev_id)');
        $this->addSql('CREATE INDEX IDX_4343A81D155232C0 ON ak_evenement_info_type_prod (ev_info_tp_fk_tp_id)');
        $this->addSql('ALTER TABLE ferme_amapien ADD CONSTRAINT FK_AA769FCC48A4116C FOREIGN KEY (ferme_f_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ferme_amapien ADD CONSTRAINT FK_AA769FCC19E9F30A FOREIGN KEY (amapien_a_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE ak_ferme_annee_adhesion ADD CONSTRAINT FK_2AE53295A0DBDA0E FOREIGN KEY (ferme_aa_fk_f_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('CREATE INDEX IDX_2AE53295A0DBDA0E ON ak_ferme_annee_adhesion (ferme_aa_fk_f_id)');
        $this->addSql('ALTER TABLE ak_ferme_produit ADD CONSTRAINT FK_244CA784708B3AB7 FOREIGN KEY (f_pro_fk_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_ferme_produit ADD CONSTRAINT FK_244CA7849C2BA34A FOREIGN KEY (type_production_id) REFERENCES ak_type_production (tp_id)');
        $this->addSql('CREATE INDEX IDX_244CA784708B3AB7 ON ak_ferme_produit (f_pro_fk_ferme_id)');
        $this->addSql('CREATE INDEX IDX_244CA7849C2BA34A ON ak_ferme_produit (type_production_id)');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose ADD CONSTRAINT FK_DF572660DA1F6AB5 FOREIGN KEY (ferme_tpp_fk_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose ADD CONSTRAINT FK_DF5726609C2BA34A FOREIGN KEY (type_production_id) REFERENCES ak_type_production (tp_id)');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose ADD CONSTRAINT FK_DF57266059114409 FOREIGN KEY (attestation_certification_id) REFERENCES file (id)');
        $this->addSql('CREATE INDEX IDX_DF572660DA1F6AB5 ON ak_ferme_type_production_propose (ferme_tpp_fk_ferme_id)');
        $this->addSql('CREATE INDEX IDX_DF5726609C2BA34A ON ak_ferme_type_production_propose (type_production_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat ADD CONSTRAINT FK_3025F6E337317C54 FOREIGN KEY (mc_livraison_lieu) REFERENCES ak_amap_livraison_lieu (amap_liv_lieu_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat ADD CONSTRAINT FK_3025F6E3567D1658 FOREIGN KEY (mc_fk_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('CREATE INDEX IDX_3025F6E337317C54 ON ak_modele_contrat (mc_livraison_lieu)');
        $this->addSql('CREATE INDEX IDX_3025F6E3567D1658 ON ak_modele_contrat (mc_fk_ferme_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat_date ADD CONSTRAINT FK_DA0201EDA5AB8D57 FOREIGN KEY (mc_d_fk_modele_contrat_id) REFERENCES ak_modele_contrat (mc_id)');
        $this->addSql('CREATE INDEX IDX_DA0201EDA5AB8D57 ON ak_modele_contrat_date (mc_d_fk_modele_contrat_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat_dates_reglement ADD CONSTRAINT FK_F3FD4AF510B78AA8 FOREIGN KEY (fk_modele_contrat) REFERENCES ak_modele_contrat (mc_id)');
        $this->addSql('CREATE INDEX IDX_F3FD4AF510B78AA8 ON ak_modele_contrat_dates_reglement (fk_modele_contrat)');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit ADD CONSTRAINT FK_A15C0C3833DEB7AF FOREIGN KEY (mc_pro_fk_ferme_produit_id) REFERENCES ak_ferme_produit (f_pro_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit ADD CONSTRAINT FK_A15C0C387A21A673 FOREIGN KEY (mc_pro_fk_modele_contrat_id) REFERENCES ak_modele_contrat (mc_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit ADD CONSTRAINT FK_A15C0C389C2BA34A FOREIGN KEY (type_production_id) REFERENCES ak_type_production (tp_id)');
        $this->addSql('CREATE INDEX IDX_A15C0C3833DEB7AF ON ak_modele_contrat_produit (mc_pro_fk_ferme_produit_id)');
        $this->addSql('CREATE INDEX IDX_A15C0C387A21A673 ON ak_modele_contrat_produit (mc_pro_fk_modele_contrat_id)');
        $this->addSql('CREATE INDEX IDX_A15C0C389C2BA34A ON ak_modele_contrat_produit (type_production_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit_exclure ADD CONSTRAINT FK_ADD1EFC5F072EC87 FOREIGN KEY (mc_pro_ex_fk_modele_contrat_date_id) REFERENCES ak_modele_contrat_date (mc_d_id)');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit_exclure ADD CONSTRAINT FK_ADD1EFC547B6C15 FOREIGN KEY (mc_pro_ex_fk_modele_contrat_produit_id) REFERENCES ak_modele_contrat_produit (mc_pro_id)');
        $this->addSql('CREATE INDEX IDX_ADD1EFC5F072EC87 ON ak_modele_contrat_produit_exclure (mc_pro_ex_fk_modele_contrat_date_id)');
        $this->addSql('CREATE INDEX IDX_ADD1EFC547B6C15 ON ak_modele_contrat_produit_exclure (mc_pro_ex_fk_modele_contrat_produit_id)');
        $this->addSql('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D860AA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->addSql('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D86018981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau ADD CONSTRAINT FK_8148CA6D44643D9C FOREIGN KEY (pay_cr_fk_pay_id) REFERENCES ak_paysan (pay_id)');
        $this->addSql('ALTER TABLE ak_paysan_commentaire ADD CONSTRAINT FK_321CA53E5641AFF6 FOREIGN KEY (pay_com_fk_pay_id) REFERENCES ak_paysan (pay_id)');
        $this->addSql('CREATE INDEX IDX_321CA53E5641AFF6 ON ak_paysan_commentaire (pay_com_fk_pay_id)');
        $this->addSql('ALTER TABLE ak_region_administrateur ADD CONSTRAINT FK_57A63CBA5F9CB5F6 FOREIGN KEY (reg_adm_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('CREATE INDEX IDX_57A63CBA1DB39DDD ON ak_region_administrateur (reg_adm_fk_region_id)');
        $this->addSql('CREATE INDEX IDX_57A63CBA5F9CB5F6 ON ak_region_administrateur (reg_adm_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_reseau ADD CONSTRAINT FK_892A29E1F98F144A FOREIGN KEY (logo_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_reseau_administrateur ADD CONSTRAINT FK_4CAD9EEDE4C3009A FOREIGN KEY (res_adm_fk_reseau_id) REFERENCES ak_reseau (res_id)');
        $this->addSql('ALTER TABLE ak_reseau_administrateur ADD CONSTRAINT FK_4CAD9EEDA5B6831E FOREIGN KEY (res_adm_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('CREATE INDEX IDX_4CAD9EEDE4C3009A ON ak_reseau_administrateur (res_adm_fk_reseau_id)');
        $this->addSql('CREATE INDEX IDX_4CAD9EEDA5B6831E ON ak_reseau_administrateur (res_adm_fk_amapien_id)');
        $this->addSql('ALTER TABLE ak_reseau_villes ADD CONSTRAINT FK_7E474F1262D5B704 FOREIGN KEY (res_v_fk_reseau_id) REFERENCES ak_reseau (res_id)');
        $this->addSql('CREATE INDEX IDX_7E474F1262D5B704 ON ak_reseau_villes (res_v_fk_reseau_id)');
        $this->addSql('CREATE INDEX IDX_7E474F12F6AFA406 ON ak_reseau_villes (res_v_fk_ville_id)');
        $this->addSql('ALTER TABLE ak_type_production ADD CONSTRAINT FK_F6633994FB73058D FOREIGN KEY (tp_categorie_mere) REFERENCES ak_type_production (tp_id)');
        $this->addSql('CREATE INDEX IDX_F6633994FB73058D ON ak_type_production (tp_categorie_mere)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
