<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \Symfony\Component\Form\FormView $form */
/** @var \PsrLib\ORM\Entity\Ferme $ferme */
$currentUser = get_current_connected_user();
?>

<script type="text/javascript">

  /* AUTOCOMPLÉTION CP / VILLE */
  $(document).ready(function () {
    $("#f_autocomplete").autocomplete({
      source: <?= '"'.site_url('ajax/geocode_get_suggestion').'"'; ?>,
      dataType: "json",
      minLength: 4,
      appendTo: "#container_f_autocomplete",
      select: function (event, ui) {

        // Maj des informations sur la page
        $("#f_autocomplete").val(ui.item.label);
        $('input[name="ferme[libAdr]"]').val(ui.item.value.address);
        $('input[name="ferme[gpsLatitude]"]').val(ui.item.value.lat.toString());
        $('input[name="ferme[gpsLongitude]"]').val(ui.item.value.long.toString());
        $('input[name="ferme[ville]"]').val(
          ui.item.value.cp.toString() + ', ' + ui.item.value.ville.toString()
        );

        // Stop la propagation de l'évènement
        return false;
      }
    });


  });

</script>

<?php

if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) {
    echo '<h3>Informations générales de la ferme</h3>';
    echo '<h5><a href="'.site_url('paysan/display/'.$currentUser->getId()).'">Profil</a> <i class="glyphicon glyphicon-menu-right"></i> '.$ferme->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Édition</h5>';
} else {
    echo '<h3>Informations générales</h3>';
    echo '<h5>';

    if (isset($ferme)) {
        echo '<a href="'.site_url('ferme/display/'.$ferme->getId()).'">'.ucfirst($ferme->getNom()).'</a> <i class="glyphicon glyphicon-menu-right"></i> Édition</h5>';
    } else {
        echo '<a href="'.site_url('ferme/index').'">Gestion des fermes</a> <i class="glyphicon glyphicon-menu-right"></i> Création</h5>';
    }
}

?>
<form method="POST" action="">
    <?php if ($form->offsetExists('regroupement')): ?>
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group col-sm-12">
                        <label>Regroupement</label>
                        <?= render_form_select_widget($form['regroupement']); ?>
                    </div>

                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-sm-6">
                    <label for="getNom()" class="control-label">Nom : </label>
                    <?= render_form_text_widget($form['nom']); ?>
                </div>

                <div class="form-group col-sm-6">
                    <label for="f_siret" class="control-label">SIRET (cette information est disponible auprès de votre
                        paysan et sur internet) : </label>
                    <?= render_form_number_widget($form['siret']); ?>
                </div>

            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-md-6">
                    <label for="f_description" class="control-label">Description : </label>
                    <?= render_form_textarea_widget($form['description']); ?>
                </div>

                <div class="col-md-6">

                    <div class="form-group ">

                        <label for="f_delai_modif_contrat" class="control-label">Délai en jours nécessaire à la ferme
                            pour
                            préparer au mieux la livraison : </label>
                        <?= render_form_number_widget($form['delaiModifContrat']); ?>
                        <div class="alert alert-info" style="margin-top: 1em">
                            Si les livraisons ont lieu le jeudi et que vous inscrivez 2 dans ce champ : <br/>
                            1/ Le/La paysan.ne sera informé par mail sur la livraison du jeudi que le mardi à 2h00 du
                            matin
                            <br/>
                            2/ L'amapien.ne ne pourra pas déplacer une livraison les 2 jours précédents celle-ci <br/>
                            3/ Un nouvel arrivant ne pourra pas contractualiser sur la livraison du jeudi à venir lors
                            des 2
                            jours prédédents celle-ci. Le nouvel arrivant peut toutefois contractualiser sur les dates
                            ultérieurs de livraisons du contrat.
                        </div>
                    </div>

                </div>


            </div>


        </div>

    </div>

    <div class="entry">
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-xs-12">
                        <label for="f_autocomplete" class="control-label">Adresse de la ferme</label>
                    </div>

                    <div class="col-xs-12">
                        <div class="alert alert-info">
                            Utilisez le champ ci-dessous pour compléter l'adresse de manière automatique grâce aux
                            suggestions proposées.
                            Il est possible que la rue ou le numéro ne soit pas connu de l'outil. Dans ce cas, vous
                            pouvez
                            soit modifier
                            manuellement le champ N° et rue ci-dessus, soit contribuer au projet <a target="_blank"
                                                                                                    href="https://wiki.openstreetmap.org/wiki/FR:Guide_du_d%C3%A9butant">Open
                                Street Map</a>
                            pour que votre adresse soit précisément localisée.
                        </div>
                    </div>

                    <div class="form-group col-xs-12">

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                            <input type="text" class="form-control" id="f_autocomplete" name="f_autocomplete"
                                   placeholder="Veuillez taper ici l'adresse et sélectionner une suggestion"/>
                        </div>

                        <div id="container_f_autocomplete" class="ui-autocomplete"
                             style="width:100%;font-size:11px;"></div>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="f_lib_adr" class="control-label">N°, voie : </label>
                        <?= render_form_text_widget($form['libAdr']); ?>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="f_code_postal_ville" class="control-label">Code postal, Ville : </label>
                        <?= render_form_text_widget($form['ville']); ?>

                        <div id="container" class="ui-autocomplete" style="width:100%;font-size:11px;"></div>
                    </div>


                    <div class="form-group col-sm-6">
                        <label for="f_gps_lat" class="control-label">GPS Latitude (ex : 45.75) : </label>
                        <?= render_form_text_widget($form['gpsLatitude']); ?>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="f_gps_long" class="control-label">GPS Longitude (ex : 4.85) : </label>
                        <?= render_form_text_widget($form['gpsLongitude']); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">

                <label for="f_tva" class="col-md-12 control-label">Assujetti à la TVA :</label>

                <?= render_form_radio_widget($form['tva'], 'col-md-2'); ?>

                <div class="form-group col-sm-4">
                    <label for="f_tva_taux" class="control-label">Taux de TVA applicable : </label>
                    <?= render_form_text_widget($form['tvaTaux']); ?>
                </div>

                <div class="form-group col-md-4">
                    <label for="f_tva_date" class="control-label">Date du passage de non assujetti à assujetti
                        : </label>
                    <?= render_form_datepicker_widget($form['tvaDate']); ?>
                </div>

            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-6">

                <label for="f_surface" class="control-label">Surface de la ferme (ha) :</label>
                <?= render_form_number_widget($form['surface'], '0', '.01'); ?>

            </div>

            <div class="col-md-6">

                <label for="f_uta" class="control-label">Nombre d'UTA :</label>
                <?= render_form_number_widget($form['uta'], '0', '.01'); ?>
            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="f_certification">Certification :</label>
                    <?= render_form_select_widget($form['certification']); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="f_url">Site internet :</label>
                    <?= render_form_text_widget($form['url']); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            <div class="col-md-12">
                <?= render_form_multiselect_widget($form['anneeAdhesions']); ?>

            </div>
        </div>
    </div>

    <div class="form-group">

        <input type="submit" value="Sauvegarder" class="pull-right btn btn-success" style="margin-left:5px;"/>
        <?php if ($currentUser instanceof \PsrLib\ORM\Entity\Paysan) { ?>
            <a href="<?= site_url('/ferme/display/'.$ferme->getId()); ?>" class="pull-right btn btn-default">Annuler</a>
        <?php } else { ?>
            <a href="<?= site_url('ferme'); ?>" class="pull-right btn btn-default">Annuler</a>
        <?php } ?>


    </div>

</form>
