<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Ferme[] $fermes */
/** @var \PsrLib\ORM\Entity\Amapien[] $refProduits */
/** @var \Symfony\Component\Form\FormView $form */
?>


<h3>Contacter un / des référent(s) produit</h3>

<?php if (count($fermes) > 0): ?>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-inverse">
            <tr>
                <th>Ferme(s)</th>
                <th>Paysan(s)</th>
                <th>Référent(s) produit</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <?php

            echo '<tbody>';

    foreach ($fermes as $ferme) {
        echo '<tr>';

        echo '<th scope="row">';
        echo ucfirst(strtolower($ferme->getNom()));
        echo '</th>';

        echo '<td>';
        echo implode(';', $ferme->getPaysans()->toArray());
        echo '</td>';

        echo '<td>';
        echo implode(';', $refProduits[$ferme->getId()]);
        echo '</td>';

        echo '<td class="text-right">';

        echo '<a href="#" data-toggle="modal" data-target="#modal_envoi_'.$ferme->getId().'" ><button type=button class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-envelope glyphicon-lg" ></i> Email référents</button></a>'; ?>

                <div class="modal fade text-left" id="modal_envoi_<?= $ferme->getId(); ?>" tabindex="-1"
                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Un email sera envoyé uniquement à tous les
                                    référents de cette ferme et copie email de l'AMAP.</h4>
                            </div>
                            <div class="modal-body">

                                <form method="POST" action="">
                                    <?= render_form_hidden_widget($form['targetId'], $ferme->getId()); ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">

                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Objet</button>
                    </span>

                                                <div class="form-group">
                                                    <?= render_form_text_widget($form['title']); ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <?= render_form_textarea_widget($form['content']); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="submit" value="Envoyer"
                                                       class="pull-right btn btn-success"/>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                </td>
                </tr>

                <?php
    } ?>

            </tbody>
        </table>
    </div>

<?php endif; ?>
