<?php

namespace Test\Services\Security;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class DistributionCheckDeleteTest extends AbstractSecurityTest
{
    public function buildDistribution(Amap $amap)
    {
        $ll = new AmapLivraisonLieu();
        $ll->setAmap($amap);

        return new AmapDistribution(
            $this->prophesize(AmapDistributionDetail::class)->reveal(),
            $ll,
            new Carbon()
        );
    }

    public function testDeleteAsPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DELETE, $this->buildDistribution(new Amap())));
    }

    public function testDeleteAsAmapien()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DELETE, $this->buildDistribution(new Amap())));
    }

    public function testDeleteAsAmapDiffrent()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DELETE, $this->buildDistribution(new Amap())));
    }

    public function testDeleteAsAmapSame()
    {
        $amap = new Amap();
        $this->mockAuthenticatedUser($amap);
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_DELETE, $this->buildDistribution($amap)));
    }
}
