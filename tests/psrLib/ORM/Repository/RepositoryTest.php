<?php

namespace Test\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Test\ContainerAwareTestCase;
use Test\DatabaseTrait;

abstract class RepositoryTest extends ContainerAwareTestCase
{
    use DatabaseTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->em = self::$container->get(EntityManagerInterface::class);
        $this->initDb();
        $this->em->clear();
    }
}
