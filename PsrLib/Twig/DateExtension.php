<?php

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DateExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('date_format_french', [$this, 'dateFormatFrench']),
            new TwigFunction('date_format_french_hour', [$this, 'dateFormatFrenchFour']),
        ];
    }

    public function dateFormatFrench(\DateTimeInterface $input = null): string
    {
        if (null === $input) {
            return '';
        }

        return $input->format('d/m/Y');
    }

    public function date_format_french_hour(\DateTimeInterface $input = null): string
    {
        if (null === $input) {
            return '';
        }

        return $input->format('d/m/Y H:i:s');
    }
}
