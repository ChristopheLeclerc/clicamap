<?php

namespace Test;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait ValidationViolationTrait
{
    public static function assertViolationContainsMessage(ConstraintViolationListInterface $violations, string $message, ?string $path = null)
    {
        $violationMessages = self::filterViolations($violations, $message, $path);

        self::assertNotEmpty($violationMessages);
    }

    public static function assertViolationNotContainsMessage(ConstraintViolationListInterface $violations, string $message, ?string $path = null)
    {
        $violationMessages = self::filterViolations($violations, $message, $path);

        self::assertEmpty($violationMessages);
    }

    private static function filterViolations(ConstraintViolationListInterface $violations, string $message, ?string $path = null)
    {
        return array_filter(iterator_to_array($violations), function (ConstraintViolationInterface $violation) use ($message, $path) {
            return $violation->getMessageTemplate() === $message
                && (null === $path || $violation->getPropertyPath() === $path);
        });
    }
}
