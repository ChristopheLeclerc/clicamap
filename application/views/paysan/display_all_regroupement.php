<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
include 'modal_email.php';

?>
<h3>Gestion des paysans</h3>

<a href="/paysan/informations_generales_creation" class="btn btn-success"><i
            class="glyphicon glyphicon-plus"></i> Créer un profil paysan</a>

<?php if (count($paysans) > 0): ?>

    <a href="/paysan/telecharger_paysans_regroupement"
       class="btn btn-primary" style="margin-left:5px;"><i class="glyphicon glyphicon-download"></i> Télécharger
    la liste</a>
    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_email" data-dismiss="modal"
       aria-hidden="true" style="margin-left:5px;"><i class="glyphicon glyphicon-envelope"></i> Emails de la
    liste</a>

    <?=modal_email($paysans); ?>
<?php endif; ?>

<?php
include 'display_all_list.php';
?>
