<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\DTO\SearchPaysanState;
use PsrLib\Form\Type\AnneeAdhesionGenericType;
use PsrLib\Form\Type\ReseauType;
use PsrLib\Form\Type\TypeProductionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchPaysanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reseau', ReseauType::class, [
                'disabled' => true,
            ])
            ->add('adhesion', AnneeAdhesionGenericType::class)
            ->add('typeProduction', TypeProductionType::class, [
                'multiple' => false,
                'required' => false,
                'placeholder' => true,
            ])
            ->add('commercialisation', AnneeAdhesionGenericType::class)
            ->add('suiviSPG', AnneeAdhesionGenericType::class)
            ->add('keyword', TextType::class, [
                'required' => false,
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Profil actif',
            ])
            ->add('paysansSansFerme', CheckboxType::class, [
                'label' => 'Paysans sans ferme',
            ])
        ;
    }

    public function getParent()
    {
        return SearchBaseRegionDepartementType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'restrict_to_user' => false,
            'data_class' => SearchPaysanState::class,
            'empty_data' => function (FormInterface $form) {
                $state = new SearchPaysanState();
                $state->setRegion($form->get('region')->getData());
                $state->setDepartement($form->get('departement')->getData());

                return $state;
            },
        ]);
    }
}
