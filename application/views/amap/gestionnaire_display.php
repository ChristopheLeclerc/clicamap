<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Amap $amap */
/** @var \Symfony\Component\Form\FormView $form */
?>

<script type="text/javascript">

  /*ALERTES & AUTOCOMPLÉTION CP / VILLE */
  $(document).ready(function () {

    $('#alerte').modal('show');

    setTimeout(function () {
      $('#alerte').modal('hide')
    }, 2500);

    $(function () {
      $("#amapien").autocomplete({
        source: <?= '"'.site_url('ajax/get_amapien_suggestion/'.$amap->getId().'"'); ?>,
        dataType: "json",
        minLength: 2,
        appendTo: "#container"
      });
    });

  });

</script>

<?php

if (get_current_connected_user() instanceof \PsrLib\ORM\Entity\Amap) {
    $retour = 'Gestion de mon AMAP';
} else {
    $retour = 'Gestion des AMAP';
}

echo '<h3>Le(s) correspondant(s) Clic\'AMAP</h3>';

echo '<h5><a href="'.site_url('amap').'">'.$retour.'</a> <i class="glyphicon glyphicon-menu-right"></i> '.$amap->getNom().'</h5>';

echo '<div class="alert alert-info">Si vous voulez ajouter un ou plusieurs correspondants Clic\'AMAP à l\'AMAP, utilisez le menu déroulant ci-dessous. L\'amapien choisi pour être correspondant Clic\'AMAP doit faire partie de l\'AMAP. Pour ensuite retirer les droits d\'administration à l\'amapien, il suffit de cliquer sur l\'amapien à supprimer. Vous ne pouvez pas retirez vos propres droits d\'administration.</div>';

echo '<form method="POST">';

echo '<div class="row">';
echo '<div class="form-group col-sm-12">';
echo render_form_select_widget($form['referent']);
echo '</div>';
echo '<div class="form-group col-sm-12">';
echo '<input type="submit" value="Ajouter" class="btn btn-success pull-right"/>';
echo '</div>';
echo '</div>';
echo '</form>';

$gestionnaires = $amap->getAmapiens()->matching(\PsrLib\ORM\Repository\AmapienRepository::criteriaGestionaire());
if (!$gestionnaires->isEmpty()) {
    /** @var \PsrLib\ORM\Entity\Amapien $gest */
    foreach ($gestionnaires as $gest) {
        echo '<a href="#" data-toggle="modal" data-target="#modal_'.$gest->getId().'" ><button class="btn btn-primary btn-sm" data-toggle="tooltip" title="Retirer les droits à l\'amapien">'.mb_strtoupper($gest->getNom()).' '.ucfirst($gest->getPrenom()).' <i class="glyphicon glyphicon-remove"></i></button></a>&nbsp;';

        echo '<div class="modal fade text-left" id="modal_'.$gest->getId().'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';
        echo '<div class="modal-header">';
        echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        echo '<h4 class="modal-title" id="myModalLabel">AMAP "'.$amap->getNom().'"</h4>';
        echo '</div>';
        echo '<div class="modal-body">';
        echo '<h5>Voulez-vous vraiment retirer le rôle de correspondant Clic\'AMAP à "'.$gest->getNom().' '.$gest->getPrenom().'" ?</h5>';
        echo '<p class="text-right">';
        echo '<a href="'.site_url('amap/gestionnaire_remove/'.$gest->getId()).'" class="btn btn-danger btn-sm">OUI</a>&nbsp;';
        echo '<a href="#" class="btn btn-success btn-sm" data-dismiss="modal">NON</a>';
        echo '</p>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
}

?>
