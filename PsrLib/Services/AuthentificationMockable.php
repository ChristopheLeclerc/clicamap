<?php

namespace PsrLib\Services;

use Psr\Container\ContainerInterface;
use PsrLib\ORM\Entity\BaseUser;

/**
 * Mockable authentification used for unit testing.
 */
class AuthentificationMockable
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ?BaseUser
     */
    private $mockedUser;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function mockUser(?BaseUser $user = null): void
    {
        $this->mockedUser = $user;
    }

    /**
     * @return null|BaseUser
     */
    public function getCurrentUser()
    {
        if (null !== $this->mockedUser) {
            return $this->mockedUser;
        }

        return $this->container->get(Authentification::class)->getCurrentUser();
    }
}
