# language: fr
Fonctionnalité: En tant qu'utlisateur, je ne doit pas pouvoir accéder à la page de maintenance si celle-ci est désactivée

  Scénario: Accès à la page de maintenance
    Lorsque je vais sur "/maintenance"
    Alors je devrais être sur "/"
