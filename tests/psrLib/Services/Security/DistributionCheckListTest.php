<?php

namespace Test\Services\Security;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\Services\Security\SecurityChecker;

/**
 * @internal
 * @coversNothing
 */
class DistributionCheckListTest extends AbstractSecurityTest
{
    public function testListAsPaysan()
    {
        $this->mockAuthenticatedUser(new Paysan());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_LIST));
    }

    public function testListAsAmapien()
    {
        $this->mockAuthenticatedUser(new Amapien());
        self::assertFalse($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_LIST));
    }

    public function testListAsAmap()
    {
        $this->mockAuthenticatedUser(new Amap());
        self::assertTrue($this->securityChecker->isGranted(SecurityChecker::ACTION_DISTRIBUTION_LIST));
    }
}
