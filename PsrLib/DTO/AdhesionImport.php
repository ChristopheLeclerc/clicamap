<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Adhesion;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\Amap;
use PsrLib\Services\PhpDiContrainerSingleton;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Adhesion import DTO. Used to validate unicity of voucher number by creator
 * Class AdhesionImport.
 */
class AdhesionImport
{
    /**
     * @var Adhesion[]
     */
    private $adhesions;

    /**
     * AdhesionImport constructor.
     *
     * @param Adhesion[] $adhesions
     */
    public function __construct($adhesions)
    {
        $this->adhesions = $adhesions;
    }

    /**
     * @Assert\Callback(groups={"import"})
     */
    public function validate(ExecutionContextInterface $context): void
    {
        /** @phpstan-ignore-next-line */
        $em = PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);

        if (0 === count($this->adhesions)) {
            return;
        }

        $creator = null;
        foreach ($this->adhesions as $adhesion) {
            $adhesionCreator = $adhesion->getCreator();
            if (null === $creator) {
                $creator = $adhesionCreator;
            }

            if ($creator->getId() !== $adhesionCreator->getId()) {
                $context->buildViolation('Toutes les adhésions doivent avoir le meme createur.')
                    ->addViolation()
                ;

                return;
            }
        }

        // Test duplicate in import
        $voucherNumbersImport = [];
        foreach ($this->adhesions as $adhesion) {
            $voucherNumber = $adhesion->getValue()->getVoucherNumber();
            if (in_array($voucherNumber, $voucherNumbersImport, true)) {
                $context->buildViolation(
                    sprintf('Duplication du numéro de reçus "%s" dans le fichier.', $voucherNumber)
                )
                    ->addViolation()
                ;

                return;
            }
            $voucherNumbersImport[] = $voucherNumber;
        }

        // Test duplicate in database
        if ($creator instanceof Amap) {
            $adhesionBdd = $em->getRepository(AdhesionAmapAmapien::class)->getVoucherNumbers($creator);
        } else {
            $adhesionRepoAmap = $em->getRepository(AdhesionAmap::class);
            $adhesionRepoFerme = $em->getRepository(AdhesionFerme::class);
            $adhesionRepoAmapien = $em->getRepository(AdhesionAmapien::class);
            $adhesionBdd = array_merge(
                $adhesionRepoAmap->getVoucherNumbers($creator),
                $adhesionRepoFerme->getVoucherNumbers($creator),
                $adhesionRepoAmapien->getVoucherNumbers($creator)
            );
        }

        foreach ($this->adhesions as $adhesion) {
            $voucherNumber = $adhesion->getValue()->getVoucherNumber();
            if (in_array($voucherNumber, $adhesionBdd, true)) {
                $context->buildViolation(
                    sprintf('Le numéro de reçus "%s" existe déjà dans la base de donnée.', $voucherNumber)
                )
                    ->addViolation()
                ;
            }
        }
    }

    /**
     * @return Adhesion[]
     */
    public function getAdhesions(): array
    {
        return $this->adhesions;
    }
}
