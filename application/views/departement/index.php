<?php
/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @var \PsrLib\ORM\Entity\Region $region */
/** @var \PsrLib\ORM\Entity\Departement[] $departement_all */
echo '<h3>Les départements de la région "'.$region->getNom().'"</h3>';

echo '<h5><a href="'.site_url('region').'">Les régions</a> <i class="glyphicon glyphicon-menu-right"></i> '.$region->getNom().' <i class="glyphicon glyphicon-menu-right"></i> Départements</h5>';

if (count($departement_all) > 0) {
    echo '<div class="alert alert-info">À l\'aide des outils de cette liste, vous pourrez ajouter des administrateurs aux départements.</div>';

    echo '<div class="table-responsive">';
    echo '<table class="table table-hover">';
    echo '<thead class="thead-inverse">';
    echo '<tr>';
    echo '<th>Départements</th>';
    echo '<th class="text-right">Outils</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    foreach ($departement_all as $departement) {
        echo '<tr>';
        echo '<th scope="row">';
        echo ucfirst($departement->getNom());
        echo '</th>';

        echo '<td class="text-right">';
        // Logo du département
        echo '<a title="Icone du réseau" href="'.site_url('region/departement_logo/'.$departement->getId()).'"><i class="glyphicon glyphicon-picture" data-toggle="tooltip" title="Icone du réseau"></i></a>&nbsp;';

        // Administrateurs du département
        echo '<a title="Administrateurs du département" href="'.site_url('region/dep_admin/'.$departement->getId()).'"><i class="glyphicon glyphicon-education" data-toggle="tooltip" title="Administrateurs du département"></i></a>&nbsp;';

        echo '</td>';
        echo '</tr>';
    }

    echo '</tbody>';
    echo '</table>';
    echo '</div>';
}
