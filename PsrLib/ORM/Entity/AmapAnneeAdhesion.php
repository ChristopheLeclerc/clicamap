<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="PsrLib\ORM\Repository\AmapAnneeAdhesionRepository")
 * @ORM\Table(name="ak_amap_annee_adhesion")
 */
class AmapAnneeAdhesion
{
    /**
     * @var ?int
     * @ORM\Column(type="integer", name="amap_aa_id")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", name="amap_aa_annee")
     */
    private $annee;

    /**
     * @var null|Amap
     * @ORM\ManyToOne(targetEntity="PsrLib\ORM\Entity\Amap", inversedBy="anneeAdhesions")
     * @ORM\JoinColumn(name="amap_aa_fk_amap_id", referencedColumnName="amap_id")
     */
    private $amap;

    /**
     * AmapAnneeAdhesion constructor.
     */
    public function __construct(int $annee, Amap $amap)
    {
        $this->annee = $annee;
        $this->amap = $amap;
    }

    public function __toString()
    {
        return (string) $this->annee;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): AmapAnneeAdhesion
    {
        $this->annee = $annee;

        return $this;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    /**
     * @param Amap $amap
     */
    public function setAmap(Amap $amap = null): AmapAnneeAdhesion
    {
        $this->amap = $amap;

        return $this;
    }
}
