<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;
use DI\Annotation\Inject;
use PsrLib\Exception\AdhesionImportException;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\AdhesionValueAmapien;
use PsrLib\ORM\Entity\AdhesionValueFerme;
use PsrLib\Services\AdhesionImport;
use PsrLib\Services\EntityBuilder\AdhesionBuilderAmap;
use PsrLib\Services\EntityBuilder\AdhesionBuilderAmapien;
use PsrLib\Services\EntityBuilder\AdhesionBuilderFerme;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Adhesion extends AppController
{
    /**
     * @Inject
     *
     * @var AdhesionImport
     */
    public $adhesionimport;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\MPdfGeneration
     */
    public $mpdfgeneration;

    /**
     * @Inject
     *
     * @var \PsrLib\Services\LocationSearchFilter
     */
    public $locationsearchfilter;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');

        $this->load->helper('download');
    }

    public function import()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_IMPORT);
        $errors = [];

        $action = null;
        if (isset($_FILES['import_amap'])) {
            $action = 'amap';

            try {
                $this->adhesionimport->doImport(
                    $_FILES['import_amap'],
                    AdhesionValueAmap::class,
                    $this->container->get(AdhesionBuilderAmap::class)
                );
            } catch (AdhesionImportException $e) {
                $this->loadViewWithTemplate('adhesion/import', [
                    'errors' => $e->getErrors(),
                    'action' => $action,
                ]);

                return;
            }

            $this->addFlash(
                'notice_success',
                'Import validé'
            );
            redirect('/adhesion/import');
        }

        if (isset($_FILES['import_paysan'])) {
            $action = 'paysan';

            try {
                $this->adhesionimport->doImport(
                    $_FILES['import_paysan'],
                    AdhesionValueFerme::class,
                    $this->container->get(AdhesionBuilderFerme::class)
                );
            } catch (AdhesionImportException $e) {
                $this->loadViewWithTemplate('adhesion/import', [
                    'errors' => $e->getErrors(),
                    'action' => $action,
                ]);

                return;
            }

            $this->addFlash(
                'notice_success',
                'Import validé'
            );
            redirect('/adhesion/import');
        }

        if (isset($_FILES['import_amapien'])) {
            $action = 'amapien';

            try {
                $this->adhesionimport->doImport(
                    $_FILES['import_amapien'],
                    AdhesionValueAmapien::class,
                    $this->container->get(AdhesionBuilderAmapien::class)
                );
            } catch (AdhesionImportException $e) {
                $this->loadViewWithTemplate('adhesion/import', [
                    'errors' => $e->getErrors(),
                    'action' => $action,
                ]);

                return;
            }

            $this->addFlash(
                'notice_success',
                'Import validé'
            );
            redirect('/adhesion/import');
        }

        $this->loadViewWithTemplate('adhesion/import', [
            'errors' => $errors,
            'action' => $action,
        ]);
    }

    public function amap()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_LIST);

        [$regions, $departments, $selectedRegion, $selectedDepartment] = $this
            ->locationsearchfilter
            ->getCurrentLocationSearch('search_region', 'search_department')
        ;

        /** @var \PsrLib\ORM\Repository\AdhesionAmapRepository $adhesionRepo */
        $adhesionRepo = $this->em->getRepository(AdhesionAmap::class);
        $adhesionYears = $adhesionRepo->getAllYears();
        $selectedYear = $this->input->get('search_year');
        if (null === $selectedYear) {
            $selectedYear = -1;
        }
        $selectedYear = (int) $selectedYear;

        $selectedIsStudent = $this->input->get('search_student');
        if ('' === $selectedIsStudent || null === $selectedIsStudent) {
            $selectedIsStudent = null;
        } else {
            $selectedIsStudent = '1' === $selectedIsStudent;
        }

        $selectedSearchKeyword = $this->input->get('search_keyword') ?? '';

        $adhesions = [];
        if (null !== $selectedRegion) {
            $adhesions = $this
                ->em
                ->getRepository(AdhesionAmap::class)
                ->search($selectedRegion, $selectedDepartment, $selectedYear, $selectedIsStudent, $selectedSearchKeyword, $this->sfSession->get('sup_adm') ? null : $this->getUser())
            ;
        }

        $this->loadViewWithTemplate('adhesion/amap', [
            'regions' => $regions,
            'departments' => $departments,
            'selectedRegion' => $selectedRegion,
            'selectedDepartment' => $selectedDepartment,
            'selectedIsStudent' => $selectedIsStudent,
            'adhesions' => $adhesions,
            'adhesionYears' => $adhesionYears,
            'selectedYear' => $selectedYear,
            'selectedSearchKeyword' => $selectedSearchKeyword,
        ]);
    }

    public function amap_pdf(int $id)
    {
        /** @var AdhesionAmap $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmap::class)->findOneById($id);

        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DOWNLOAD, $adhesion);

        $adhesion->setGenerationDate(\Carbon\Carbon::now());
        $this->mpdfgeneration->genererAdhesion($adhesion);
    }

    public function amap_download($id)
    {
        /** @var null|AdhesionAmap $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmap::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DOWNLOAD, $adhesion);

        force_download(
            sprintf(
                'recus_amap_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            ),
            file_get_contents($this->config->item('adhesion_path').'/'.$adhesion->getVoucherFileName()),
            true
        );
    }

    public function amap_delete($id)
    {
        if ('POST' !== $this->input->method(true)) {
            throw new \Exception('Unsupported method');
        }

        /** @var null|AdhesionAmap $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmap::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DELETE, $adhesion);

        $this->em->remove($adhesion);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Adhésion supprimée'
        );

        $this->redirect->redirectToReferer('/adhesion/amap');
    }

    public function amap_action()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_LIST);

        if (null !== $this->input->post('action-delete')) {
            $this->_bulk_delete(AdhesionAmap::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DELETE);
        }

        if (null !== $this->input->post('action-generate')) {
            $this->_bulk_generate(AdhesionAmap::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_GENERATE);
        }

        if (null !== $this->input->post('action-download')) {
            $this->_bulk_download(AdhesionAmap::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DOWNLOAD);
        }

        $this->redirect->redirectToReferer('/adhesion/amap');
    }

    public function paysan()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_LIST);

        [$regions, $departments, $selectedRegion, $selectedDepartment] = $this
            ->locationsearchfilter
            ->getCurrentLocationSearch('search_region', 'search_department')
        ;

        /** @var \PsrLib\ORM\Repository\AdhesionFermeRepository $adhesionRepo */
        $adhesionRepo = $this->em->getRepository(AdhesionFerme::class);
        $adhesionYears = $adhesionRepo->getAllYears();
        $selectedYear = $this->input->get('search_year');
        if (null === $selectedYear) {
            $selectedYear = -1;
        }
        $selectedYear = (int) $selectedYear;

        $selectedSearchKeyword = $this->input->get('search_keyword') ?? '';

        $adhesions = [];
        if (null !== $selectedRegion) {
            $adhesions = $this
                ->em
                ->getRepository(AdhesionFerme::class)
                ->search($selectedRegion, $selectedDepartment, $selectedYear, $selectedSearchKeyword, $this->sfSession->get('sup_adm') ? null : $this->getUser())
            ;
        }

        $this->loadViewWithTemplate('adhesion/ferme', [
            'regions' => $regions,
            'departments' => $departments,
            'selectedRegion' => $selectedRegion,
            'selectedDepartment' => $selectedDepartment,
            'adhesions' => $adhesions,
            'adhesionYears' => $adhesionYears,
            'selectedYear' => $selectedYear,
            'selectedSearchKeyword' => $selectedSearchKeyword,
        ]);
    }

    public function paysan_action()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_LIST);

        if (null !== $this->input->post('action-delete')) {
            $this->_bulk_delete(AdhesionFerme::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_DELETE);
        }

        if (null !== $this->input->post('action-generate')) {
            $this->_bulk_generate(AdhesionFerme::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_GENERATE);
        }

        if (null !== $this->input->post('action-download')) {
            $this->_bulk_download(AdhesionFerme::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_DOWNLOAD);
        }

        $this->redirect->redirectToReferer('/adhesion/amap');
    }

    public function paysan_download($id)
    {
        /** @var null|AdhesionFerme $adhesion */
        $adhesion = $this->em->getRepository(AdhesionFerme::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_DOWNLOAD, $adhesion);

        force_download(
            sprintf(
                'recus_ferme_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            ),
            file_get_contents($this->config->item('adhesion_path').'/'.$adhesion->getVoucherFileName()),
            true
        );
    }

    public function paysan_delete($id)
    {
        if ('POST' !== $this->input->method(true)) {
            throw new \Exception('Unsupported method');
        }

        /** @var null|AdhesionAmap $adhesion */
        $adhesion = $this->em->getRepository(AdhesionFerme::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_DELETE, $adhesion);

        $this->em->remove($adhesion);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Adhésion supprimée'
        );

        $this->redirect->redirectToReferer('/adhesion/paysan');
    }

    public function amapien()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_LIST);

        [$regions, $departments, $selectedRegion, $selectedDepartment] = $this
            ->locationsearchfilter
            ->getCurrentLocationSearch('search_region', 'search_department')
        ;

        /** @var \PsrLib\ORM\Repository\AdhesionFermeRepository $adhesionRepo */
        $adhesionRepo = $this->em->getRepository(AdhesionAmapien::class);
        $adhesionYears = $adhesionRepo->getAllYears();
        $selectedYear = $this->input->get('search_year');
        if (null === $selectedYear) {
            $selectedYear = -1;
        }
        $selectedYear = (int) $selectedYear;

        $selectedSearchKeyword = $this->input->get('search_keyword') ?? '';

        $adhesions = [];
        if (null !== $selectedRegion) {
            $adhesions = $this
                ->em
                ->getRepository(AdhesionAmapien::class)
                ->search($selectedRegion, $selectedDepartment, $selectedYear, $selectedSearchKeyword, $this->sfSession->get('sup_adm') ? null : $this->getUser())
            ;
        }

        $this->loadViewWithTemplate('adhesion/amapien', [
            'regions' => $regions,
            'departments' => $departments,
            'selectedRegion' => $selectedRegion,
            'selectedDepartment' => $selectedDepartment,
            'adhesions' => $adhesions,
            'adhesionYears' => $adhesionYears,
            'selectedYear' => $selectedYear,
            'selectedSearchKeyword' => $selectedSearchKeyword,
        ]);
    }

    public function amapien_action()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_LIST);

        if (null !== $this->input->post('action-delete')) {
            $this->_bulk_delete(AdhesionAmapien::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_DELETE);
        }

        if (null !== $this->input->post('action-generate')) {
            $this->_bulk_generate(AdhesionAmapien::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_GENERATE);
        }

        if (null !== $this->input->post('action-download')) {
            $this->_bulk_download(AdhesionAmapien::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_DOWNLOAD);
        }

        $this->redirect->redirectToReferer('/adhesion/amapien');
    }

    public function amapien_download($id)
    {
        /** @var null|AdhesionAmapien $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmapien::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_DOWNLOAD, $adhesion);

        force_download(
            sprintf(
                'recus_amapien_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            ),
            file_get_contents($this->config->item('adhesion_path').'/'.$adhesion->getVoucherFileName()),
            true
        );
    }

    public function amapien_delete($id)
    {
        if ('POST' !== $this->input->method(true)) {
            throw new \Exception('Unsupported method');
        }

        /** @var null|AdhesionAmap $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmapien::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_DELETE, $adhesion);

        $this->em->remove($adhesion);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Adhésion supprimée'
        );

        $this->redirect->redirectToReferer('/adhesion/amapien');
    }

    public function amap_amapien()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_IMPORT_AMAP_AMAPIEN_ADHESION);

        /** @var \PsrLib\ORM\Repository\AdhesionFermeRepository $adhesionRepo */
        $adhesionRepo = $this->em->getRepository(\PsrLib\ORM\Entity\AdhesionAmapAmapien::class);
        $adhesionYears = $adhesionRepo->getAllYears();
        $selectedYear = $this->input->get('search_year');
        if (null === $selectedYear) {
            $selectedYear = -1;
        }
        $selectedYear = (int) $selectedYear;

        $selectedSearchKeyword = $this->input->get('search_keyword') ?? '';

        $adhesions = $adhesionRepo
            ->search($selectedYear, $selectedSearchKeyword, $this->getUser())
        ;

        $this->loadViewWithTemplate('adhesion/amap_amapien', [
            'adhesions' => $adhesions,
            'adhesionYears' => $adhesionYears,
            'selectedYear' => $selectedYear,
            'selectedSearchKeyword' => $selectedSearchKeyword,
        ]);
    }

    public function amap_amapien_action()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_IMPORT_AMAP_AMAPIEN_ADHESION);

        if (null !== $this->input->post('action-delete')) {
            $this->_bulk_delete(AdhesionAmapAmapien::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_AMAPIEN_DELETE);
        }

        if (null !== $this->input->post('action-generate')) {
            $this->_bulk_generate(AdhesionAmapAmapien::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_AMAPIEN_GENERATE);
        }

        if (null !== $this->input->post('action-download')) {
            $this->_bulk_download(AdhesionAmapAmapien::class, \PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD);
        }

        $this->redirect->redirectToReferer('/adhesion/amap_amapien');
    }

    public function amap_amapien_download($id)
    {
        /** @var null|AdhesionAmapAmapien $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmapAmapien::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD, $adhesion);

        force_download(
            sprintf(
                'recus_amapien_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            ),
            file_get_contents($this->config->item('adhesion_path').'/'.$adhesion->getVoucherFileName()),
            true
        );
    }

    public function amap_amapien_delete($id)
    {
        if ('POST' !== $this->input->method(true)) {
            throw new \Exception('Unsupported method');
        }

        /** @var null|AdhesionAmapAmapien $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmapAmapien::class)->findOneById($id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_AMAPIEN_DELETE, $adhesion);

        $this->em->remove($adhesion);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Adhésion supprimée'
        );

        $this->redirect->redirectToReferer('/adhesion/amap_amapien');
    }

    public function mes_recus_amap()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAP_LIST_SELF);

        /** @var \PsrLib\ORM\Entity\Amap $currentUser */
        $currentUser = $this->getUser();

        $adhesions = $this
            ->em
            ->getRepository(AdhesionAmap::class)
            ->findBy([
                'amap' => $currentUser,
            ])
        ;

        $this->loadViewWithTemplate('adhesion/amap_self', [
            'adhesions' => $adhesions,
        ]);
    }

    public function mes_recus_ferme()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_FERME_LIST_SELF);

        /** @var \PsrLib\ORM\Entity\Paysan $currentUser */
        $currentUser = $this->getUser();

        $adhesions = $this
            ->em
            ->getRepository(AdhesionFerme::class)
            ->findByPaysan($currentUser)
        ;

        $this->loadViewWithTemplate('adhesion/ferme_self', [
            'adhesions' => $adhesions,
        ]);
    }

    public function mes_recus_amapien()
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\SecurityChecker::ACTION_ADHESION_AMAPIEN_LIST_SELF);

        /** @var \PsrLib\ORM\Entity\Amapien $currentUser */
        $currentUser = $this->getUser();

        $adhesions = $this
            ->em
            ->getRepository(AdhesionAmapien::class)
            ->findBy([
                'amapien' => $currentUser,
            ])
        ;
        $adhesions_amap_amapien = $this
            ->em
            ->getRepository(AdhesionAmapAmapien::class)
            ->findBy([
                'amapien' => $currentUser,
            ])
        ;

        $this->loadViewWithTemplate('adhesion/amapien_self', [
            'adhesions' => array_merge($adhesions, $adhesions_amap_amapien),
        ]);
    }

    private function _get_selected_ids()
    {
        $selectedIds = $this->input->post('selected');
        if (null === $selectedIds) {
            $selectedIds = [];
        } else {
            $selectedIds = array_keys($selectedIds);
        }

        return $selectedIds;
    }

    private function _bulk_delete(string $adhesionClass, string $permission)
    {
        $selectedIds = $this->_get_selected_ids();

        $adhesions = $this->em->getRepository($adhesionClass)->findByMultipleIds($selectedIds);
        if (0 === count($adhesions)) {
            $this->addFlash(
                'notice_success',
                'Aucune adhésion sélectionnée'
            );

            return;
        }

        $this->denyAccessUnlessGrantedMultiple($permission, $adhesions);

        foreach ($adhesions as $adhesion) {
            $this->em->remove($adhesion);
        }

        $this->em->flush();
        $this->addFlash(
            'notice_success',
            sprintf('%d adhésions supprimées avec succès', count($adhesions))
        );
    }

    private function _bulk_generate(string $adhesionClass, string $permission)
    {
        $evm = $this->em->getEventManager();

        $selectedIds = $this->_get_selected_ids();

        /** @var Adhesion[] $adhesions */
        $adhesions = $this
            ->em
            ->getRepository($adhesionClass)
            ->findByMultipleIds($selectedIds, \PsrLib\ORM\Entity\Adhesion::STATE_DRAFT)
        ;

        if (0 === count($adhesions)) {
            $this->addFlash(
                'notice_success',
                'Aucune adhésion en attente : aucun reçu ne sera généré'
            );

            return;
        }

        $this->denyAccessUnlessGrantedMultiple($permission, $adhesions);

        foreach ($adhesions as $adhesion) {
            $adhesion->setGenerationDate(Carbon::now());

            try {
                $filename = $this->mpdfgeneration->genererAdhesion($adhesion);
            } catch (\PsrLib\Exception\AdhesionGenerationCreatorNoCityExecption $e) {
                $this->addFlash(
                    'notice_error',
                    $e->getMessage()
                );

                return;
            }

            $adhesion->setVoucherFileName($filename);
            $adhesion->setState(\PsrLib\ORM\Entity\Adhesion::STATE_GENERATED);

            $evm->dispatchEvent(
                \PsrLib\ORM\EventSubscriber\Events::ADHESION_POST_GENERATE,
                new \PsrLib\ORM\EventSubscriber\AdhesionPostGenerateEventArgs($adhesion)
            );
        }

        $this->em->flush();

        $this->addFlash(
            'notice_success',
            sprintf('%d adhésions générée', count($adhesions))
        );
    }

    private function _bulk_download(string $adhesionClass, string $permission)
    {
        $selectedIds = $this->_get_selected_ids();

        /** @var AdhesionAmap[] $adhesions */
        $adhesions = $this->em->getRepository($adhesionClass)->findByMultipleIds($selectedIds, \PsrLib\ORM\Entity\Adhesion::STATE_GENERATED);
        if (0 === count($adhesions)) {
            $this->addFlash(
                'notice_success',
                'Aucune adhésion sélectionnée'
            );

            return;
        }

        $this->denyAccessUnlessGrantedMultiple($permission, $adhesions);

        $zip = new ZipArchive();
        $tmpFileName = sprintf('%s/bulk_download_%s.zip', sys_get_temp_dir(), \Ramsey\Uuid\Uuid::uuid4());
        if (true !== $zip->open($tmpFileName, ZipArchive::CREATE)) {
            throw new \RuntimeException('Unable to create zip file'.$tmpFileName);
        }

        foreach ($adhesions as $adhesion) {
            $zip->addFile($this->config->item('adhesion_path').'/'.$adhesion->getVoucherFileName(), $adhesion->getVoucherFileName());
        }
        $zip->close();
        force_download(
            'recus.zip',
            file_get_contents($tmpFileName),
            true
        );

        unlink($tmpFileName);
    }
}
