# language: fr
Fonctionnalité: Edition de son profil personnel

  Scénario: Masquage des champs information adhésion pour les amapiens non admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "amapien@test.amap-aura.org"
    Quand je suis "Mon compte"
    Et je suis "Mon profil"
    Et je suis "Éditer le profil"
    Alors je ne devrais pas voir "Nom du réseau"

  Plan du Scénario: Affichage des champs information adhésion pour les admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis "Mon compte"
    Et je suis "Mon profil"
    Et je suis "Éditer le profil"
    Alors je devrais voir "Nom du réseau"

    Exemples:
      | mail                            |
      | admin.aura@test.amap-aura.org   |
      | admin.rhone@test.amap-aura.org  |

