<?php

namespace PsrLib\Services\ServiceFactories;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\ServiceFactories;

use Carbon\Doctrine\CarbonImmutableType;
use Carbon\Doctrine\CarbonType;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Cache\PhpFileCache;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Tools\Setup;
use PsrLib\ORM\EventSubscriber\AdhesionAmapienPostGenerateSubscriber;
use PsrLib\ORM\EventSubscriber\AdhesionAmapPostGenerateSubscriber;
use PsrLib\ORM\EventSubscriber\AdhesionFermePostGenerateSubscriber;
use PsrLib\ORM\Type\MoneyType;
use PsrLib\ORM\Type\TimeType;
use PsrLib\ProjectLocation;

class DoctrineFactory
{
    public function create(
        AdhesionAmapPostGenerateSubscriber $adhesionAmapPostGenerateSubscriber,
        AdhesionFermePostGenerateSubscriber $adhesionFermePostGenerateSubscriber,
        AdhesionAmapienPostGenerateSubscriber $adhesionAmapienPostGenerateSubscriber
    ): EntityManagerInterface {
        if (!Type::hasType('money')) {
            Type::addType('money', MoneyType::class);
        }
        if (!Type::hasType('carbon')) {
            Type::addType('carbon', CarbonType::class);
        }
        if (!Type::hasType('carbon_immutable')) {
            Type::addType('carbon_immutable', CarbonImmutableType::class);
        }
        if (!Type::hasType('time_custom')) {
            Type::addType('time_custom', TimeType::class);
        }

        AnnotationRegistry::registerLoader('class_exists');

        // Database connection information
        $connectionOptions = [
            'driver' => 'pdo_mysql',
            'user' => $_ENV['DB_USERNAME'],
            'password' => $_ENV['DB_PASSWORD'],
            'host' => $_ENV['DB_HOSTNAME'],
            'dbname' => $_ENV['DB_DATABASE'],
            'charset' => 'UTF8',
        ];

        $paths = [ProjectLocation::PROJECT_ROOT.'/PsrLib/ORM/Entity'];

        // Use environment variable to get correct value on command line
        $isProductionSetup = 'production' === getenv('CI_ENV') || 'true' === getenv('FORCE_DOCTRINE_PRODUCTION');
        $baseCacheFolder = ProjectLocation::PROJECT_ROOT.'/application/writable/cache/doctrine';
        $config = Setup::createAnnotationMetadataConfiguration(
            $paths,
            !$isProductionSetup,
            $baseCacheFolder.'/proxy',
            $isProductionSetup ? new PhpFileCache($baseCacheFolder.'/cache') : null,
            false
        );

        $namingStrategy = new UnderscoreNamingStrategy(CASE_LOWER);
        $config->setNamingStrategy($namingStrategy);

        $em = EntityManager::create($connectionOptions, $config);

        $evm = $em->getEventManager();
        $evm->addEventSubscriber($adhesionAmapPostGenerateSubscriber);
        $evm->addEventSubscriber($adhesionFermePostGenerateSubscriber);
        $evm->addEventSubscriber($adhesionAmapienPostGenerateSubscriber);

        return $em;
    }
}
