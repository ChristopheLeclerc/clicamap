<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Form;

use PsrLib\DTO\SearchPaysanState;
use PsrLib\Form\SearchPaysanType;
use PsrLib\ORM\Entity\Amapien;
use Test\DatabaseTrait;

/**
 * @internal
 * @coversNothing
 */
class SearchPaysanTypeTest extends AbstractFormTest
{
    use DatabaseTrait;

    public function setUp()
    {
        parent::setUp();
        $this->initDb();
    }

    public function testAdminDepartementEmptyTest()
    {
        $form = $this->formFactory->create(SearchPaysanType::class, null, [
            'currentUser' => $this->getAdminDepartement(),
        ]);

        $form->submit([]);
        $this->assertTrue($form->isSynchronized());
        $this->assertEquals(new SearchPaysanState(), $form->getData());
        $this->assertCount(17, $form->createView()['region']->vars['choices']);
        $this->assertCount(0, $form->createView()['departement']->vars['choices']);
    }

    public function testAdminDepartementSelectRegionTest()
    {
        $form = $this->formFactory->create(SearchPaysanType::class, null, [
            'currentUser' => $this->getAdminDepartement(),
        ]);

        $form->submit([
            'region' => 84,
        ]);
        $this->assertTrue($form->isSynchronized());
        $this->assertEquals(84, $form->getData()->getRegion()->getId());
        $this->assertCount(17, $form->createView()['region']->vars['choices']);
        $this->assertCount(12, $form->createView()['departement']->vars['choices']);
    }

    private function getAdminDepartement()
    {
        return $this
            ->em
            ->getRepository(Amapien::class)
            ->findOneBy([
                'email' => 'admin.rhone@test.amap-aura.org',
            ])
            ;
    }
}
