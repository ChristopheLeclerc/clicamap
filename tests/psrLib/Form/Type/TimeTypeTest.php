<?php

namespace Test\Form\Type;

use PsrLib\DTO\Time;
use PsrLib\Form\Type\TimeType;
use Test\Form\AbstractFormTest;

/**
 * @internal
 * @coversNothing
 */
class TimeTypeTest extends AbstractFormTest
{
    public function testFormValid()
    {
        $form = $this->formFactory->create(TimeType::class);
        $form->submit('1:23');

        $time = $form->getData();
        self::assertInstanceOf(Time::class, $time);
        self::assertSame(1, $time->getHour());
        self::assertSame(23, $time->getMinute());
    }

    /**
     * @dataProvider invalidInputProvider
     */
    public function testFormInvalid(string $input)
    {
        $form = $this->formFactory->create(TimeType::class);
        $form->submit($input);

        $time = $form->getData();
        self::assertNull($time);
        self::assertCount(1, $form->getErrors());
        self::assertSame('Merci d\'utiliser le format HH:MM', $form->getErrors()[0]->getMessage());
    }

    public function invalidInputProvider()
    {
        return [
            ['abc'],
            ['a:12'],
            ['123:12'],
            ['1h12'],
        ];
    }
}
