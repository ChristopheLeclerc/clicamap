# language: fr
Fonctionnalité: Visionner le profil d'une AMAP avec tous les profils possibles

  @database
  Plan du Scénario: Visualiser le profil de l'AMAP, profils AMAPien et AMAPien référent
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/evenement"
    Et que je suis "Contact"
    Et que je suis "Mon AMAP"
    Alors je devrais être sur "/contact/mon_amap"

    Exemples:
      | mail                          |
      | amapien@test.amap-aura.org    |
      | amapienref@test.amap-aura.org |

  @database
  Scénario: Visualiser le profil de l'AMAP, profil AMAP
    Etant donné que je suis authentifié comme "amapien" avec le mail "amap@test.amap-aura.org"
    Quand je suis sur "/evenement"
    Et que je suis "Gestionnaire AMAP"
    Et que je suis "Gestion de mon AMAP"
    Alors je devrais être sur "/amap"
    Et que je suis "Détails de l'AMAP"
    Alors je devrais être sur "/amap/display/1"
    Et je devrais voir "Gestion de mon AMAP"

  @database
  Plan du Scénario: Visualiser le profil d'une AMAP, profils réseau et super-admin
    Etant donné que je suis authentifié comme "amapien" avec le mail "<mail>"
    Quand je suis sur "/evenement"
    Et que je suis "Gestionnaire AMAP"
    Et que je suis "Gestion des AMAP"
    Alors je devrais être sur "/amap"
    Et que je remplis "search_amap[region]" avec "84"
    Et que je suis "Détails de l'AMAP"
    Alors je devrais être sur "/amap/display/1"
    Et je devrais voir "Gestion des AMAP"

    Exemples:
      | mail                          |
      | admin.aura@test.amap-aura.org |
      | admin@test.amap-aura.org      |
